<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once('simple_html_dom.php');
/**
 * Handle login request on m.bca
 *
 * no long description
 *
 * @author     kadekjayak <kadekjayak@yahoo.co.id>
 * #MODIFIER   Robith Ritz <robithritz@gmail.com>
 * @copyright  2016 kadekjayak
 * @license   http://www.apache.org/licenses/LICENSE-2.0  Apache License 2.0
 * @version    0.1
 */
define('MANDIRI_PARSER_DEBUG', false);
class Mandiribot {
	
	private $company_id;
	private $username;
	private $password;
	
	public $curlHandle;

	public $_defaultTargets = [
		'loginAction' => 'https://mib.bankmandiri.co.id/sme/common/login.do?action=loginSME',
		'initialMenu' => 'https://mib.bankmandiri.co.id/sme/common/login.do?action=doSMEMainFrame',
		'topMenuFrame' => 'https://mib.bankmandiri.co.id/sme/common/login.do?action=topRequestSME',
		'sideMenuFrame' => 'https://mib.bankmandiri.co.id/sme/common/login.do?action=menuWithNameRequest',
		'mutasiFrame' => 'https://mib.bankmandiri.co.id/sme/front/transactioninquiry.do?action=transactionByDateRequest&menuCode=MNU_GCME_040201',
		'getMutasi' => 'https://mib.bankmandiri.co.id/sme/front/transactioninquiry.do?action=doCheckValidityAndShow&type=show&',
		'logoutAction' => 'https://mib.bankmandiri.co.id/sme/common/login.do?action=logoutSME'
	];
	public $isLoggedIn = false;
	
	public $ipAddress;

	public $last_output = array();
	
	public $_defaultHeaders = array(
		'Host: mib.bankmandiri.co.id',
		'Connection: keep-alive',
		'Cache-Control: max-age=0',
		'Upgrade-Insecure-Requests: 1',
		'User-Agent: Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.76 Mobile Safari/537.36',
		'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
		// 'Accept-Encoding: gzip, deflate, br',
		'Accept-Language: en-US,en;q=0.8,id;q=0.6,fr;q=0.4'
	);
	
	/**
	* The Constructor
	* this class will make login request to BCA when initialized
	*
	* @param string $username
	* @param string $password
	*/
	public function __construct($params)
	{
		$company_id = $params['company_id'];
		$username = $params['username'];
		$password = $params['password'];
		if( MANDIRI_PARSER_DEBUG == true ) error_reporting(E_ALL);
		$this->company_id = $company_id;
		$this->username = $username;
		$this->password = $password;
		$this->curlHandle = curl_init();
		$this->setupCurl();
		// $this->login($this->company_id, $this->username, $this->password);
	}
	
	/**
	* Get ip address, required on login parameters
	*
	* @return String;
	*/
	public function getIpAddress()
	{
		if($this->ipAddress !== null) $this->ipAddress = json_decode( file_get_contents( 'http://myjsonip.appspot.com/' ) )->ip;
		return $this->ipAddress;
	}
	
	/**
	* Execute the CURL and return result
	*
	* @return curl result
	*/
	public function exec()
	{
		$result = curl_exec($this->curlHandle);
		if( MANDIRI_PARSER_DEBUG == true ) {
			$http_code = curl_getinfo($this->curlHandle, CURLINFO_HTTP_CODE);
			// print_r($result);
			/**
			* Perlu diwapadai jangan melakukan pengecekan dengan interval waktu dibawah 10 menit ! 
			*/
			/*if($http_code == 302) {
				echo 'HALAMAN DIREDIRECT, harap tunggu beberapa menit ( biasanya 10 Menit! )';
				exit;
			}*/
			if($http_code != 200){
				return -1;
			}
		}
		$this->last_output[] = array('length' => strlen($result), 'htmlvalue' => $result);
		return $result;
	}
	
	/**
	* Register default CURL parameters
	*/
	public function setupCurl()
	{
		curl_setopt( $this->curlHandle, CURLOPT_POST, 0 );
		curl_setopt( $this->curlHandle, CURLOPT_HTTPGET, 1 );
		curl_setopt( $this->curlHandle, CURLOPT_HTTPHEADER, $this->_defaultHeaders);
		curl_setopt( $this->curlHandle, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $this->curlHandle, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $this->curlHandle, CURLOPT_COOKIEFILE,'cookie' );
		curl_setopt( $this->curlHandle, CURLOPT_COOKIEJAR, 'cookiejar' );
	}
	
	/**
	* Set request method on CURL to GET 
	*/
	public function curlSetGet()
	{
		curl_setopt( $this->curlHandle, CURLOPT_POST, 0 );
		curl_setopt( $this->curlHandle, CURLOPT_HTTPGET, 1 );
	}
	
	/**
	* Set request method on CURL to POST 
	*/
	public function curlSetPost()
	{
		curl_setopt( $this->curlHandle, CURLOPT_POST, 1 );
		curl_setopt( $this->curlHandle, CURLOPT_HTTPGET, 0 );
	}
	
	/**
	* Login to BCA
	*/
	public function login()
	{
		
		//Sending Login Info
		// $this->getIpAddress();

		$password = sha1(md5($this->password));
		
		$params = array(
			"corpId={$this->company_id}",
			"userName={$this->username}",
			'passwordEncryption=',
			'language=fr_FR',
			"password={$password}",
			"sessionId="
		);
		$params = implode( '&', $params );
		$this->curlSetPost();
		curl_setopt( $this->curlHandle, CURLOPT_URL, $this->_defaultTargets['loginAction'] );
		// curl_setopt( $this->curlHandle, CURLOPT_REFERER, $this->_defaultTargets['loginUrl'] );
		curl_setopt( $this->curlHandle, CURLOPT_POSTFIELDS, $params );
		$result = $this->exec();

		if( strpos($result, 'location.href = "/sme/common/login.do?action=doSMEMainFrame";') !== false || strpos($result, 'Harap membuka browser lain untuk membuka Company Prime Cash Module') !== false){
			
			curl_setopt( $this->curlHandle, CURLOPT_URL, $this->_defaultTargets['initialMenu'] );
			$this->curlSetGet();
			$this->exec();

			curl_setopt( $this->curlHandle, CURLOPT_URL, $this->_defaultTargets['topMenuFrame'] );
			$this->exec();

			$this->isLoggedIn = true;
		}else{
			$this->isLoggedIn = false;
		}
	}
	
	/**
	* Get mutasi rekening pages
	*
	* @param string $from 'Y-m-d'
	* @param string $to 'Y-m-d'
	* @return string
	*/
	public function getMutasiRekening($from, $to)
	{
		if( !$this->isLoggedIn ) $this->login($this->company_id, $this->username, $this->password );
		
		$this->curlSetGet();
		curl_setopt( $this->curlHandle, CURLOPT_URL, $this->_defaultTargets['sideMenuFrame'] );
		$this->exec();

		curl_setopt( $this->curlHandle, CURLOPT_URL, $this->_defaultTargets['mutasiFrame'] );
		$this->exec();

		$this->curlSetPost();
		
		$params = array( 
				'dlAccountNo=1370014152082^MEETAZA PRAWIRA MEDI^IDR^IDR^13700^S^KC Yogyakarta Sudirman',
				'accountType=S',
				'accountHierarchy=',
				'customFile=',
				'processAccountIndividually=Y',
				'archiveFlag=N',
				'screenState=TRX_DATE',
				'checkDate=Y',
				'timeLength=31',
				'showTimeLength=31',
				'accountNumber=1370014152082',
				'accountNm=MEETAZA PRAWIRA MEDI',
				'currDisplay=IDR',
				'curr=IDR',
				'frOrganizationUnit=13700',
				'accountTypeCode=D',
				'language=FR'
				);
		$params_get = 'day1='.date('j', strtotime($from)).'&mon1='.date('n', strtotime($from)).'&year1='.date('Y', strtotime($from)).'&day2='.date('j', strtotime($to)).'&mon2='.date('n', strtotime($to)).'&year2='.date('Y', strtotime($to)).'&accountNumber=1370014152082&accountType=D&frOrganizationUnitNm=&currDisplay=IDR&trxFilter=credit';
		$params = implode( '&', $params );

		curl_setopt( $this->curlHandle, CURLOPT_URL, $this->_defaultTargets['getMutasi'].$params_get );

		curl_setopt( $this->curlHandle, CURLOPT_REFERER, $this->_defaultTargets['mutasiFrame'] );
		curl_setopt( $this->curlHandle, CURLOPT_POSTFIELDS, $params );
		$html = $this->exec();
		return $this->getMutasiRekeningTable($html);
		// return $this->_defaultTargets['getMutasi'].$params_get."<br>".$params;
	}
	
	/**
	* Parse the pages on mutasi rekening
	* this method will return only elements on <table> tag that contain only list of transaction
	*
	* @param string $html
	* @return string
	*/
	private function getMutasiRekeningTable($html)
	{
		/*$dom = new DOMDocument();
	
		if ( MANDIRI_PARSER_DEBUG ) {
			$dom->loadHTML($html);	
		} else {
			@$dom->loadHTML($html);	
		}
		
		$dom->getElementById('pagebody');
		
		$table = $dom->getElementsByTagName('table');
		$table = $table->item(4);
		return $dom->saveHTML($table);*/

		$html = str_get_html($html);
		$new_arr = array();

		$total_data = 0;
		$field_data = ['TanggalDanWaktu', 'ValueDate', 'Deskripsi', 'Debit', 'Kredit', 'Saldo'];
		foreach ($html->find('table[class="clsFormTrxStatus"] tr[class="clsEven"]') as $key => $tr) {
			$td = $tr->find('td');
			if(count($td) == 6){
				for($i=0; $i<count($td); $i++) {
					// $new_arr[] = array('name' => $value->innertext, 'url' => $base_url.$value->href);
					// echo $state." - ".$base_url.$value->href."<br>";
					$show = str_replace(array('\t', '<br>'), '', $td[$i]->innertext);
					if($i > 2){
						$show = str_replace('.00', '', $show);
					}
					$show = preg_replace('/\t/', '', $show);
					$show = addslashes($show);
					$new_arr[$total_data][$field_data[$i]] = trim(htmlentities($show));
				}
				$total_data++;
			}
		}
		return $new_arr;
	}
	private function getArrayValues($html)
	{
		$dom = new DOMDocument();
		$dom->loadHTML($html);
		$table = $dom->getElementsByTagName('table');
		$rows = $dom->getElementsByTagName('tr');
		$datas = [];
		for ($i = 0; $i < $rows->length; $i++) {
			if($i== 0 ) continue;
		    $cols = $rows->item($i)->getElementsbyTagName("td");
		    $date = $cols->item(0)->nodeValue;
		    $date = explode('/', $date);
		    $date = date('Y') . '-' . $date[1] . '-' . $date[0];
		    $description = $cols->item(1);
		    $flows = $cols->item(2)->nodeValue;
		    $descriptionText = $dom->saveHTML($description);
		    $descriptionText = str_replace('<td>', '', $descriptionText);
		    $descriptionText = str_replace('</td>', '', $descriptionText);
		    $description = explode('<br>', $descriptionText);
		    $data = compact('date','description', 'flows');
		    $datas[] = $data;
		}
		return $datas;
	}
	/**
	* Ambil daftar transaksi pada janga waktu tertentu
	*
	*
	* @param string $from 'Y-m-d'
	* @param string $to 'Y-m-d'
	* @return array
	**/
	public function getListTransaksi($from, $to)
	{
		$result = $this->getMutasiRekening($from, $to);
		$result = $this->getArrayValues($result);
		return $result;
	}
	/**
	* getTransaksiCredit
	*
	* Ambil semua list transaksi credit (kas Masuk)
	*
	* @param string $from 'Y-m-d'
	* @param string $to 'Y-m-d'
	* @return array
	*/
	public function getTransaksiCredit($from, $to)
	{
		$result = $this->getListTransaksi($from, $to);
		$result = array_filter($results, function($row){
			return $row['flows'] == 'CR';
		});
		return $result;
	}
	/**
	* getTransaksiDebit
	*
	* Ambil semua list transaksi debit (kas Keluar)
	* Struktur data tidak konsisten !, tergantung dari jenis transaksi
	*
	* @param string $from 'Y-m-d'
	* @param string $to 'Y-m-d'
	* @return array
	*/
	public function getTransaksiDebit($from, $to)
	{
		$result = $this->getListTransaksi($from, $to);
		$result = array_filter($result, function($row){
			return $row['flows'] == 'DB';
		});
		return $result;
	}
	/**
	* Logout
	* 
	* Logout from KlikBca website
	* Lakukan logout setiap transaksi berakhir!
	*
	* @return string
	*/
	public function logout()
	{
		$this->curlSetGet();
		curl_setopt( $this->curlHandle, CURLOPT_URL, $this->_defaultTargets['logoutAction'] );
		return $this->exec();
	}
}