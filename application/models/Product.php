<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Model{
    function __construct() {
        $this->proTable = 'products';
        $this->transTable = 'payments';
        $this->packageTable = 'master_package';
    }
    
    // Fetch and return product data
    public function getOrder($id = ''){
        $this->db->select('*');
        $this->db->from('tbl_order');
        if($id){
            $this->db->where('order_id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            $this->db->order_by('created_at', 'asc');
            $query = $this->db->get();
            $result = $query->result_array();
        }
        return !empty($result)?$result:false;
    }
    
    // Insert transaction data
    public function insertTransaction($data = array()){
        $user_id = $data['user_id'];
        $product_id = $data['product_id'];
        $txn_id = $data['txn_id'];
        $payment_gross = $data['payment_gross'];
        $currency_code = $data['currency_code'];
        $payer_email = $data['payer_email'];
        $payment_status = $data['payment_status'];
        if ($payment_status == "Completed") {
            
            $insert = $this->db->query("INSERT INTO payments (user_id, product_id, txn_id, payment_gross, currency_code, payer_email, payment_status) VALUES ('$user_id','$product_id','$txn_id','$payment_gross','$currency_code','$payer_email','$payment_status')");
            // $insert = $this->db->insert($this->transTable,$data);

        }
        else if($payment_status == "Pending"){
            $insert = $this->db->query("INSERT INTO payments (user_id, product_id, txn_id, payment_gross, currency_code, payer_email, payment_status) VALUES ('$user_id','$product_id','$txn_id','$payment_gross','$currency_code','$payer_email','$payment_status')");
            // $insert = $this->db->insert($this->transTable,$data);
        }
        else
        {
            // $this->db->query("INSERT INTO payments (user_id, product_id, txn_id, payment_gross, currency_code, payer_email, payment_status) VALUES ('$user_id','$product_id','$txn_id','$payment_gross','$currency_code','$payer_email','$payment_status')");
            // $insert = $this->db->insert($this->transTable,$data);
        }        
        return $insert?true:false;
    }
}