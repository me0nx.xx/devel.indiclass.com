<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_model extends CI_Model {

	function getTblUser(){
		$id_user = $this->session->userdata('id_user_kids');
		if ($id_user == null || $id_user =="") {
			$id_user = $this->session->userdata('id_user');
		}

		// $id_user = $this->session->userdata('id_user');

		$select_all_data = $this->db->query("SELECT *FROM tbl_user WHERE id_user='$id_user'")->row_array();

		return $select_all_data;
	}

	function getTblUserAndProfile(){
		$id_user = $this->session->userdata('id_user_kids');
		if ($id_user == null || $id_user =="") {
			$id_user = $this->session->userdata('id_user');
		}
		// $id_user = $this->session->userdata('id_user');

		$select_all_data = $this->db->query("SELECT tbl_user. * , tpt.*, master_jenjang. * 
			FROM tbl_user LEFT JOIN ( tbl_profile_student as tpt LEFT JOIN master_jenjang ON tpt.jenjang_id=master_jenjang.jenjang_id ) ON tpt.student_id=tbl_user.id_user WHERE tbl_user.id_user =  '$id_user'")->row_array();

		return $select_all_data;
	}

	function getTblUserAndProfileTutor(){
		$id_user = $this->session->userdata('id_user');

		$select_all_data = $this->db->query("SELECT tbl_user. * , tbl_profile_tutor. * 
			FROM tbl_user, tbl_profile_tutor
			WHERE tbl_user.id_user =  tbl_profile_tutor.tutor_id
			AND tbl_user.id_user =  '$id_user'")->row_array();

		if($select_all_data['education_background'] != ''){
			$select_all_data['education_background'] = unserialize($select_all_data['education_background']);
		}

		if ($select_all_data['teaching_experience'] != '') {
			$select_all_data['teaching_experience'] = unserialize($select_all_data['teaching_experience']);
		}

		return $select_all_data;
	}

	
	function send_session()
	{
		$suserid = $this->session->userdata('id_user');		
		$scek = $this->db->query('select temp_session from temp_session where id_user = "'.$suserid.'"');
		if (empty($scek)) {
			return 0;
		}
		
		foreach ($scek->result_array() as $d) {
			$temp_session = $d['temp_session'];
			return $temp_session;
		}
	}

	function jenjangname()
	{
		// $this->db->group_by('jenjang_name');
		// $this->db->order_by('jenjang_id','ASC');
		// $level= $this->db->get('master_jenjang');
		$level = $this->db->query("SELECT distinct(jenjang_name) FROM master_jenjang");
		// $levels = $this->db->query("SELECT * FROM `master_jenjang`");

		return $level->result_array();
	}


	function jenjangnamelevel()
	{
		/*$this->db->group_by('jenjanglevel_name');
		$this->db->order_by('jenjanglevel_id','ASC');
		$level= $this->db->get('master_jenjanglevel');*/
		$levels = $this->db->query("SELECT distinct(jenjanglevel_name) FROM master_jenjanglevel");

		return $levels->result_array();
	}

	function jenjang_id($jname = '')
	{
		// $this->db->group_by('jenjang_name');
		// $this->db->order_by('jenjang_id','ASC');
		// $level= $this->db->get('master_jenjang');
		$level = $this->db->query("SELECT * FROM `master_jenjang` where jenjang_name='$jname'");

		return $level->result_array();
	}

	function levels($name)
	{
		$leves="<option value='0'>SELECT GRADE LEVEL</option>";

		// $this->db->order_by('jenjang_level','ASC');
		// $kec= $this->db->get_where('master_jenjang',array('jenjang_name'=>$levName));

		$lev = $this->db->query('select * from master_jenjang where jenjang_name = "'.$name.'"');

		foreach ($lev->result_array() as $data ){
			$jenjanglevel.= "<option value='$data[jenjang_id]'>$data[jenjang_level]</option>";
		}

		return $jenjanglevel;
	}

	function jenjangnameregister()
	{
		/*$this->db->group_by('jenjang_name');
		$this->db->order_by('jenjang_id','ASC');
		$level= $this->db->get('master_jenjang');*/
		$level = $this->db->query("SELECT distinct(jenjang_name) from master_jenjang");

		return $level->result_array();
	}

	function levelsregister($name)
	{
		$leves="<option disabled>SELECT GRADE LEVEL</option>";

		// $this->db->order_by('jenjang_level','ASC');
		// $kec= $this->db->get_where('master_jenjang',array('jenjang_name'=>$levName));

		$lev = $this->db->query('select * from master_jenjang where jenjang_name = "'.$name.'"');

		foreach ($lev->result_array() as $data ){
			$jenjang_level.= "<option value='$data[jenjang_id]'>$data[jenjang_level]</option>";
		}

		return $jenjang_level;
	}

	function getJenjangID(){
		$id_user = $this->session->userdata('id_user');

		$jenjang_id = $this->db->query("SELECT *FROM master_jenjang WHERE");
		
	}

	function provinsi($id_prov = '')
	{
		// $this->db->order_by('provinsi_name','ASC');
		// $provinsi= $this->db->get('master_provinsi');
		if($id_prov == ''){
			$provinsi = $this->db->query("SELECT * FROM master_provinsi order by provinsi_name ASC")->result_array();
		}else{
			$provinsi = $this->db->query("SELECT * FROM master_provinsi WHERE provinsi_id=$id_prov")->row_array();
		}
		

		return $provinsi;
	}

	function getAllKabupaten($id_kab = '', $id_prov=''){

		if($id_prov == ''){
			if($id_kab == ''){
				$kabupaten = $this->db->query("SELECT * FROM master_kabupaten order by kabupaten_name ASC")->result_array();
			}else{
				$kabupaten = $this->db->query("SELECT * FROM master_kabupaten WHERE kabupaten_id=$id_kab ")->row_array();
			}
			
		}else{
			if($id_kab == ''){
				$kabupaten = $this->db->query("SELECT * FROM master_kabupaten WHERE provinsi_id=$id_prov order by kabupaten_name ASC")->result_array();
			}else{
				$kabupaten = $this->db->query("SELECT * FROM master_kabupaten WHERE provinsi_id=$id_prov AND kabupaten_id=$id_kab ")->row_array();
			}
		}

		return $kabupaten;
	}

	function getAllKecamatan($id_kec='',$id_kab=''){

		if($id_kab == ''){
			if($id_kec == ''){
				$kecamatan = $this->db->query("SELECT * FROM master_kecamatan order by kecamatan_name ASC")->result_array();
			}else{
				$kecamatan = $this->db->query("SELECT * FROM master_kecamatan WHERE kecamatan_id=$id_kec ")->row_array();
			}
			
		}else{
			if($id_kec == ''){
				$kecamatan = $this->db->query("SELECT * FROM master_kecamatan WHERE kabupaten_id=$id_kab order by kecamatan_name ASC")->result_array();
			}else{
				$kecamatan = $this->db->query("SELECT * FROM master_kecamatan WHERE kabupaten_id=$id_kab AND kecamatan_id=$id_kec ")->row_array();
			}
		}
		/*$id_user = $this->session->userdata('id_user');
		$kabupaten_id = $this->db->query("SELECT id_kabupaten FROM tbl_user WHERE id_user='$id_user'")->row_array()['id_kabupaten'];
		$kecamatan = $this->db->query("SELECT *FROM master_kecamatan WHERE kabupaten_id='$kabupaten_id'")->result_array();*/

		return $kecamatan;
	}

	function getAllKelurahan($id_des='',$id_kec=''){

		if($id_kec == ''){
			if($id_des == ''){
				$kelurahan = $this->db->query("SELECT * FROM master_desa order by desa_name ASC")->result_array();
			}else{
				$kelurahan = $this->db->query("SELECT * FROM master_desa WHERE desa_id=$id_des ")->row_array();
			}
			
		}else{
			if($id_des == ''){
				$kelurahan = $this->db->query("SELECT * FROM master_desa WHERE kecamatan_id=$id_kec order by desa_name ASC")->result_array();
			}else{
				$kelurahan = $this->db->query("SELECT * FROM master_desa WHERE kecamatan_id=$id_kec AND desa_id=$id_des ")->row_array();
			}
		}
/*		$id_user = $this->session->userdata('id_user');
		$kecamatan_id = $this->db->query("SELECT id_kecamatan FROM tbl_user WHERE id_user='$id_user'")->row_array()['id_kecamatan'];
		$kelurahan = $this->db->query("SELECT *FROM master_desa WHERE kecamatan_id='$kecamatan_id'")->result_array();*/

		return $kelurahan;
	}

	function school_provinsi($d_provinsi = '')
	{
		// $this->db->order_by('provinsi_name','ASC');
		// $provinsi= $this->db->get('master_provinsi');
		if($d_provinsi == ''){
			$provinsi = $this->db->query("SELECT provinsi FROM master_school GROUP by provinsi")->result_array();
		}else{
			$provinsi = $this->db->query("SELECT * FROM master_school WHERE provinsi='$d_provinsi'")->row_array();
		}
		

		return $provinsi;
	}

	function school_getAllKabupaten($d_kabupaten = '', $d_provinsi=''){

		if($d_provinsi == ''){
			if($d_kabupaten == ''){
				$kabupaten = $this->db->query("SELECT kabupaten FROM master_school GROUP by kabupaten ASC")->result_array();
			}else{
				$kabupaten = $this->db->query("SELECT * FROM master_school WHERE kabupaten='$d_kabupaten'")->row_array();
			}
			
		}else{
		 	if($d_kabupaten == ''){
				$kabupaten = $this->db->query("SELECT kabupaten FROM master_school WHERE provinsi='$d_provinsi' GROUP by kabupaten ASC")->result_array();
		 	}else{
		 		$kabupaten = $this->db->query("SELECT * FROM master_school WHERE provinsi='$d_provinsi' AND kabupaten_id='$d_kabupaten'")->row_array();
			}
		}

		return $kabupaten;
	}

	function school_getAllKecamatan($d_kecamatan='',$d_kabupaten=''){

		if($d_kabupaten == ''){
			if($d_kecamatan == ''){
				$kecamatan = $this->db->query("SELECT kecamatan FROM master_school WHERE kabupaten='$d_kabupaten' GROUP by kecamatan ASC")->result_array();
			}else{
				$kecamatan = $this->db->query("SELECT * FROM master_school WHERE kecamatan='$d_kecamatan'")->row_array();
			}
			
		}else{
			if($d_kecamatan == ''){
				$kecamatan = $this->db->query("SELECT kecamatan FROM master_school WHERE kabupaten='$d_kabupaten' GROUP by kecamatan ASC")->result_array();
			}else{
				$kecamatan = $this->db->query("SELECT * FROM master_school WHERE kabupaten='$d_kabupaten' AND kecamatan='$d_kecamatan'")->row_array();
			}
		}
		/*$id_user = $this->session->userdata('id_user');
		$kabupaten_id = $this->db->query("SELECT id_kabupaten FROM tbl_user WHERE id_user='$id_user'")->row_array()['id_kabupaten'];
		$kecamatan = $this->db->query("SELECT *FROM master_kecamatan WHERE kabupaten_id='$kabupaten_id'")->result_array();*/
		return $kecamatan;
	}

	function school_getAllKelurahan($d_kelurahan='',$d_kecamatan=''){

		if($d_kecamatan == ''){
			if($d_kelurahan == ''){
				$kelurahan = $this->db->query("SELECT kelurahan FROM master_school group by kelurahan ASC")->result_array();
			}else{
				$kelurahan = $this->db->query("SELECT * FROM master_school WHERE kelurahan='$d_kelurahan' ")->row_array();
			}
			
		}else{
			if($d_kelurahan == ''){
				$kelurahan = $this->db->query("SELECT kelurahan FROM master_school WHERE kecamatan='$d_kecamatan' group by kelurahan ASC")->result_array();
			}else{
				$kelurahan = $this->db->query("SELECT * FROM master_school WHERE kecamatan='$d_kecamatan' AND kelurahan='$d_kelurahan'")->row_array();
			}
		}
/*		$id_user = $this->session->userdata('id_user');
		$kecamatan_id = $this->db->query("SELECT id_kecamatan FROM tbl_user WHERE id_user='$id_user'")->row_array()['id_kecamatan'];
		$kelurahan = $this->db->query("SELECT *FROM master_desa WHERE kecamatan_id='$kecamatan_id'")->result_array();*/

		return $kelurahan;
	}

	function school_getName($d_schoolname='',$d_kelurahan=''){		
		// if($d_kelurahan == ''){
		// 	if($d_schoolname == ''){	
		// 		$school_name = $this->db->query("SELECT school_name FROM master_school group by school_name ASC")->result_array();
		// 	}else{
		// 		$school_name = $this->db->query("SELECT * FROM master_school WHERE school_name='$d_schoolname' ")->row_array();
		// 	}
			
		// }else{
		// echo $d_schoolname;
		// echo $d_kelurahan;
		
			if($d_schoolname == ''){
				$school_name = $this->db->query("SELECT school_name FROM master_school WHERE kelurahan='$d_kelurahan' group by school_name ASC")->result_array();
			}else{
				$school_name = $this->db->query("SELECT * FROM master_school WHERE school_name='$d_schoolname' AND kelurahan='$d_kelurahan' ")->row_array();
			}
		// }

		return $school_name;
	}

	public function getSchoolAddress($d_schoolname = null)
	{
		$d_schoolname = str_replace('%20', ' ', $d_schoolname);
		// echo "SELECT school_name FROM master_school WHERE school_name='$d_schoolname'";
		return $this->db->query("SELECT school_address FROM master_school WHERE school_name='$d_schoolname'")->row_array()['school_address'];
	}
	public function getSchoolID($d_schoolname = null)
	{
		$d_schoolname = str_replace('%20', ' ', $d_schoolname);
		// echo "SELECT school_name FROM master_school WHERE school_name='$d_schoolname'";
		return $this->db->query("SELECT school_id FROM master_school WHERE school_name='$d_schoolname'")->row_array()['school_id'];
	}

	public function getMasterJenjang($jenjang_id = '')
	{
		if($jenjang_id == ''){
			$jenjang = $this->db->query("SELECT * FROM master_jenjang")->result_array();
		}else{
			$jenjang = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id=$jenjang_id")->row_array();
		}
		return $jenjang;
	}

	function kabupaten($provProvinsi_id){
		$langg = $this->lang->line('selectkabupaten');
		$kabupaten="<option disabled value='0'>".$langg."</option>";

		$this->db->order_by('kabupaten_name','ASC');
		$kab= $this->db->get_where('master_kabupaten',array('provinsi_id'=> $provProvinsi_id));

		foreach ($kab->result_array() as $data ){
			$kabupaten.= "<option value='$data[kabupaten_id]'>$data[kabupaten_name]</option>";
		}

		return $kabupaten;

	}

	function kecamatan($kabId)
	{
		$langg = $this->lang->line('selectkecamatan');
		$kecamatan="<option disabled value='0'>".$langg."</option>";

		$this->db->order_by('kecamatan_name','ASC');
		$kec= $this->db->get_where('master_kecamatan',array('kabupaten_id'=>$kabId));

		foreach ($kec->result_array() as $data ){
			$kecamatan.= "<option value='$data[kecamatan_id]'>$data[kecamatan_name]</option>";
		}

		return $kecamatan;

	}

	function kelurahan($kelId)
	{
		$langg = $this->lang->line('selectkelurahan');
		$kelurahan = "<option disabled value='0'>".$langg."</option>";

		$this->db->order_by('desa_name','ASC');
		$kel = $this->db->get_where('master_desa', array('kecamatan_id'=>$kelId));

		foreach ($kel->result_array() as $data) {
			$kelurahan.= "<option value='$data[desa_id]'>$data[desa_name]</option>";
		}
		return $kelurahan;
	}

	function getDataJenjangname() { 
		$query = 'select distinct(jenjang_name) from master_jenjang '; 
		$q = $this->db->query($query); 
		if ($q->num_rows() > 0) { 
			foreach ($q->result() as $row) { $data[]=$row; } return $data; 
		} 
	}

	function getDataJenjanglevel($id_jenjang)
	{
		$query = 'select jenjang_level from master_jenjang where id_jenjang = "'.$id_jenjang.'"';
		$q = $this->db->query($query);
		if ($q->num_rows() > 0) {
			foreach ($q->result() as $row) {
				$data[]=$row;
			}
			return $data;
		}
	}

	function getSubject(){
		// $jenjang_id = $this->session->userdata('jenjang_id');
		// $get = $this->db->query("SELECT * FROM master_subject")->result_array();
		// $q_jenjang = "";
		// foreach ($get as $key => $v) {
		// 	$jenjangid = json_decode($v['jenjang_id'], TRUE);
		// 	if ($jenjangid != NULL) {
		// 		if(is_array($jenjangid)){
		// 			foreach ($jenjangid as $key => $value) {
		// 				if ($jenjang_id==$value) {
		// 					$getAllSubject = $this->db->query("SELECT subject_id FROM master_subject WHERE jenjang_id = '$value'")->row_array()['subject_id'];	
		// 					$q_jenjang.= " OR subject_id='".$getAllSubject."' ";
		// 				}						
		// 			}
		// 		}
		// 		else
		// 		{
		// 			$getAllSubject = $this->db->query("SELECT * FROM master_subject WHERE jenjang_id = '$jenjang_id'")->result_array();			
		// 		}
		// 	}
		// }

		$jenjangid = $this->session->userdata('jenjang_id_kids');
		if ($jenjangid == null || $jenjangid == "") {
			$jenjangid = $this->session->userdata('jenjang_id');
		}
		

		$getAllSubject = $this->db->query("SELECT * FROM master_subject WHERE jenjang_id LIKE '%\"$jenjangid\"%' OR jenjang_id='$jenjangid'")->result_array();
		return $getAllSubject;	
	}

	function getMyTutor(){
		$subject_id = $this->session->userdata('subject_id');
		$getAllTutor = $this->db->query("SELECT *FROM tbl_booking WHERE subject_id = '$subject_id'");

		$tutor_id = $getAllTutor->row_array()['id_user'];

		$tutor_profile = $this->db->query("SELECT tbl_user.user_name, tbl_profile_tutor. * FROM tbl_user, tbl_profile_tutor WHERE tbl_user.id_user = tbl_profile_tutor.tutor_id")->result();

		return $tutor_profile;
		
	}

	function save_booking($table, $where){
		// $select_dulu = $this->db->query("SELECT *FROM tbl_booking WHERE id_user='$where[id_user]' AND subject_id='$where[subject_id]'")->result_array();
		// if (!empty($select_dulu)) {
		// 	$return=0;
		// }else{
		$res = $this->db->simple_query("INSERT INTO tbl_booking (id_user, subject_id, tutor_id) VALUES ('$where[id_user]','$where[subject_id]','$where[tutor_id]')");
		if ($res) {
			// $action = "Memilih Subject ".$where[subject_name];
			$log = $this->db->query("INSERT INTO log_user (action, id_user) VALUES ('Memilih Subject $where[subject_name]','$where[id_user]')");
			if ($log) {
				$return=1;
			}			
		}else{
			$return=3;
		}


		return $return;
		
	}

	function unsave_booking($table, $where){
		
		$res = $this->db->query("DELETE FROM tbl_booking WHERE id_user='$where[id_user]' AND subject_id='$where[subject_id]' AND tutor_id='$where[tutor_id]'");
		if ($res) {
			$return =1;
		}
		else
		{
			$return = 0;
		}
		return $return;
	}

	function choose_sub($table, $where){
		$select_dulu = $this->db->query("SELECT * FROM tbl_booking WHERE id_user='$where[id_user]' AND subject_id='$where[subject_id]'")->row_array();
		if (empty($select_dulu)) {
			$res = $this->db->simple_query("INSERT INTO tbl_booking (id_user, subject_id) VALUES ('$where[id_user]','$where[subject_id]')");
			if ($res) {
				$return=1;

			}else{
				$return=0;
			}
		}else{
			$return=00;
		}

		return $return;
	}

	function unchoose_sub($table, $where){
		$select_dulu = $this->db->query("SELECT *FROM tbl_booking WHERE id_user='$where[id_user]' AND subject_id='$where[subject_id]'")->row_array();
		if (!empty($select_dulu)) {
			// $res = $this->db->simple_query("INSERT INTO tbl_booking (id_user, subject_id) VALUES ('$where[id_user]','$where[subject_id]')");
			$res = $this->db->simple_query("DELETE FROM tbl_booking WHERE subject_id='$where[subject_id]' AND id_user='$where[id_user]'");
			if ($res) {
				$return=1;

			}else{
				$return=0;
			}
		}else{
			$return=00;
		}

		return $return;
	}

	function request_sub($table, $where){
		$select_dulu = $this->db->query("SELECT *FROM tbl_booking WHERE id_user='$where[id_user]' AND subject_id='$where[subject_id]'")->row_array();
		if (!empty($select_dulu)) {
			// $res = $this->db->simple_query("INSERT INTO tbl_booking (id_user, subject_id) VALUES ('$where[id_user]','$where[subject_id]')");
			$res = $this->db->simple_query("UPDATE tbl_booking SET status='requested' WHERE subject_id='$where[subject_id]' AND id_user='$where[id_user]'");
			if ($res) {
				$return=1;

			}else{
				$return=0;
			}
		}else{
			$return=00;
		}

		return $return;
	}

	function getProfileTutor(){
		$tutor_id = $this->session->userdata('id_user');
		$ProfileTutor = $this->db->query("SELECT *FROM tbl_profile_tutor WHERE tutor_id= '$tutor_id'")->result_array();
		return $ProfileTutor;
	}


	public function set_availability($data,$tutor_id)
	{
		$stime = 0;
		for($x=0;$x<7;$x++){
			for ($i=0; $i < 24; $i++) {
				$take_time[$x][$i] = $x."_".$stime;
				$stime+=3600;
			}
			$stime = 0;
		}
		$arrDay = null;
		foreach ($take_time as $key => $value) {
			$array_privs[$key] = array();

			for($x=0;$x<24;$x++) {
				if(isset($data[$value[$x]])){
					$data[$value[$x]] = "off";
					echo $value[$x]." ada "."<br>";
					$st = substr($value[$x], 2); 
					$stp = $st+3600;
					// echo $st."<br>";
					$_1 = $this->db->query("SELECT * FROM tbl_avtime WHERE tutor_id =$tutor_id AND date='".$data['date_'.$key]."' AND start_time = $st")->row_array();
					if(empty($_1)){
						$this->db->simple_query("INSERT INTO tbl_avtime(tutor_id,date,start_time,stop_time,available) VALUES($tutor_id,'".$data['date_'.$key]."',".$st.",".$stp.",1)");
						// echo "Aefaef";
					}
					// $arrDay[$day][$hour]['time_name'] = $value['time_name'];
					// $arrDay[$day][$hour]['time'] = $value['time'];
					// $arrDay[$day][$hour]['available'] = 1;

				}else{
					$st = substr($value[$x], 2); 
					$stp = $st+3600;

					$_1 = $this->db->query("SELECT * FROM tbl_avtime WHERE tutor_id =$tutor_id AND date='".$data['date_'.$key]."' AND start_time = $st")->row_array();
					if(!empty($_1)){
						$this->db->simple_query("DELETE FROM tbl_avtime WHERE uid='".$_1['uid']."'");
					}
					// $arrDay[$day][$hour]['time_name'] = $value['time_name'];
					// $arrDay[$day][$hour]['time'] = $value['time'];
					// $arrDay[$day][$hour]['available'] = 0;
				}
				// echo $vas;

			}
			
		}
		// return null;
		/*$_0 = $this->db->query("SELECT * FROM tbl_avtime WHERE tutor_id =$tutor_id AND date='".$data['date_0']."'")->row_array();
		if(empty($_0)){
			$this->db->simple_query("INSERT INTO tbl_avtime(tutor_id,date,available) VALUES($tutor_id,'".$data['date_0']."','".serialize($arrDay[0])."')");
		}else{
			$this->db->simple_query("UPDATE tbl_avtime SET tutor_id=$tutor_id,date='".$data['date_0']."',available='".serialize($arrDay[0])."' WHERE uid=".$_0['uid']."");
		}
		$_0 = $this->db->query("SELECT * FROM tbl_avtime WHERE tutor_id =$tutor_id AND date='".$data['date_1']."'")->row_array();
		if(empty($_0)){
			$this->db->simple_query("INSERT INTO tbl_avtime(tutor_id,date,available) VALUES($tutor_id,'".$data['date_1']."','".serialize($arrDay[1])."')");
		}else{
			$this->db->simple_query("UPDATE tbl_avtime SET tutor_id=$tutor_id,date='".$data['date_1']."',available='".serialize($arrDay[1])."' WHERE uid=".$_0['uid']."");
		}
		$_0 = $this->db->query("SELECT * FROM tbl_avtime WHERE tutor_id =$tutor_id AND date='".$data['date_2']."'")->row_array();
		if(empty($_0)){
			$this->db->simple_query("INSERT INTO tbl_avtime(tutor_id,date,available) VALUES($tutor_id,'".$data['date_2']."','".serialize($arrDay[2])."')");
		}else{
			$this->db->simple_query("UPDATE tbl_avtime SET tutor_id=$tutor_id,date='".$data['date_2']."',available='".serialize($arrDay[2])."' WHERE uid=".$_0['uid']."");
		}
		$_0 = $this->db->query("SELECT * FROM tbl_avtime WHERE tutor_id =$tutor_id AND date='".$data['date_3']."'")->row_array();
		if(empty($_0)){
			$this->db->simple_query("INSERT INTO tbl_avtime(tutor_id,date,available) VALUES($tutor_id,'".$data['date_3']."','".serialize($arrDay[3])."')");
		}else{
			$this->db->simple_query("UPDATE tbl_avtime SET tutor_id=$tutor_id,date='".$data['date_3']."',available='".serialize($arrDay[3])."' WHERE uid=".$_0['uid']."");
		}
		$_0 = $this->db->query("SELECT * FROM tbl_avtime WHERE tutor_id =$tutor_id AND date='".$data['date_4']."'")->row_array();
		if(empty($_0)){
			$this->db->simple_query("INSERT INTO tbl_avtime(tutor_id,date,available) VALUES($tutor_id,'".$data['date_4']."','".serialize($arrDay[4])."')");
		}else{
			$this->db->simple_query("UPDATE tbl_avtime SET tutor_id=$tutor_id,date='".$data['date_4']."',available='".serialize($arrDay[4])."' WHERE uid=".$_0['uid']."");
		}
		$_0 = $this->db->query("SELECT * FROM tbl_avtime WHERE tutor_id =$tutor_id AND date='".$data['date_5']."'")->row_array();
		if(empty($_0)){
			$this->db->simple_query("INSERT INTO tbl_avtime(tutor_id,date,available) VALUES($tutor_id,'".$data['date_5']."','".serialize($arrDay[5])."')");
		}else{
			$this->db->simple_query("UPDATE tbl_avtime SET tutor_id=$tutor_id,date='".$data['date_5']."',available='".serialize($arrDay[5])."' WHERE uid=".$_0['uid']."");
		}
		$_0 = $this->db->query("SELECT * FROM tbl_avtime WHERE tutor_id =$tutor_id AND date='".$data['date_6']."'")->row_array();
		if(empty($_0)){
			$this->db->simple_query("INSERT INTO tbl_avtime(tutor_id,date,available) VALUES($tutor_id,'".$data['date_6']."','".serialize($arrDay[6])."')");
		}else{
			$this->db->simple_query("UPDATE tbl_avtime SET tutor_id=$tutor_id,date='".$data['date_6']."',available='".serialize($arrDay[6])."' WHERE uid=".$_0['uid']."");
		}*/
		$load = $this->lang->line('settingdone');
		$res['mes_alert'] = 'success';
		$res['mes_display'] = 'block';
		$res['mes_message'] = $load;
		return $res;

	}
	public function getMTime()
	{
		$res = $this->db->query("SELECT time_name,time FROM master_time order by uid;")->result_array();
		return $res;
	}
	public function reg_email($all_data)
	{
		//print_r($all_data);
		$uname = $all_data['user_name'];
		$fname = $all_data['first_name'];
		$lname = $all_data['last_name'];
		$email = $all_data['email'];
		$kode_area = $all_data['kode_area'];
		$phone = $all_data['no_hape'];
		// $password = md5($all_data['password']);
		$password = password_hash($all_data['password'],PASSWORD_DEFAULT);
		$bdate = $all_data['user_birthdate'];
		$samplee = $all_data['sample'];
		$jenjang_id = $all_data['id_jenjang'];
		$age = date_diff(date_create($bdate), date_create('today'))->y;
		$gender = $all_data['user_gender'];
		$utype_id = $all_data['usertype_id'];
		$id_user = $this->Rumus->gen_id_user();
		$r_key = $this->M_login->randomKey();
		if($utype_id == 'student'){
			$status = 1;	
		}else{
			$status = 3;
		}	
		// echo "INSERT INTO tbl_user(id_user,user_name, email, password, user_birthdate, user_age, user_gender, user_image, user_callnum, status, st_tour, usertype_id) VALUES('$id_user','$uname','$email','$password','$bdate','$age','$gender','empty.jpg','$phone',$status,'0','$utype_id')";
		// echo "<br>";
		// echo $id_user;
		// return 1;

		if($this->db->simple_query("INSERT INTO tbl_user(id_user,user_name, first_name, last_name, email, password, user_birthdate, user_age, user_gender, user_image, user_countrycode, user_callnum, status, st_tour, usertype_id) VALUES('$id_user','$uname','$fname','$lname','$email','$password','$bdate','$age','$gender','".base64_encode('user/empty.jpg')."','$kode_area','$phone','1','0','$utype_id')")){
			$last_id = $this->db->query("SELECT LAST_INSERT_ID();")->row_array()['LAST_INSERT_ID()'];
			$this->db->simple_query("INSERT INTO tbl_profile_student(student_id,jenjang_id) VALUES('".$id_user."','".$jenjang_id."')");
			// echo "INSERT INTO tbl_profile_student(student_id,jenjang_id) VALUES('".$id_user."','".$jenjang_id."')";
			// echo "<br>";
			// echo $last_id;
			// return 1;
			if($all_data['user_image'] != ''){
				// $path = $all_data['user_image'];
				// $imagedata = file_get_contents($path);
             	// alternatively specify an URL, if PHP settings allow
				// $base64 = base64_encode($imagedata);
				file_put_contents("./aset/img/user/".$last_id.".jpg",file_get_contents($all_data['user_image']));

				$config['image_library'] = 'gd2';
				$config['source_image']	= "../aset/img/user/".$last_id.".jpg";
				$config['width']	= 250;
				$config['maintain_ratio'] = FALSE;
				$config['height']	= 250;

				$this->image_lib->initialize($config); 

				$this->image_lib->resize();
				// $this->db->simple_query("UPDATE tbl_user SET user_image='".$last_id.".jpg' WHERE id_user='".$last_id."'");
				$this->db->simple_query("UPDATE tbl_user SET user_image='".$last_id.".jpg' WHERE id_user='".$last_id."'");				

				$load = $this->lang->line('successfullyregis');
				$this->session->set_flashdata('mes_alert','success');
				$this->session->set_flashdata('mes_display','block'); 
				$this->session->set_flashdata('mes_message',$load);	

				$this->session->set_userdata('email_register',$all_data['email']);
				$this->session->set_userdata('password_register',$all_data['password']);
				return 1;							
			}
			// else
			// {
			// 	$this->db->simple_query("UPDATE tbl_user SET user_image='empty.jpg' WHERE id_user='".$last_id."'");				

			// 	$load = $this->lang->line('successfullyregis');
			// 	$this->session->set_flashdata('mes_alert','success');
			// 	$this->session->set_flashdata('mes_display','block'); 
			// 	$this->session->set_flashdata('mes_message',$load);	

			// 	$this->session->set_userdata('email_register',$all_data['email']);
			// 	$this->session->set_userdata('password_register',$all_data['password']);
			// 	return 1;	
			// }
		}else{
			$load = $this->lang->line('successfullyregis');
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block'); 
			$this->session->set_flashdata('mes_message',$load);
			
			$this->session->set_userdata('email_register',$all_data['email']);
			$this->session->set_userdata('password_register',$all_data['password']);			
			return 1;
		}
		return -1;
	}

	public function getMySes(){
		$id_user = $this->session->userdata('id_user');
		$select_subject_id = $this->db->query("SELECT subject_id FROM tbl_booking WHERE id_user='$id_user'")->row_array();

		$select_strtime = $this->db->query("SELECT start_time FROM tbl_session");

		return $select_subject_id;
	}

	public function saveEdit_Accounttutor($table, $where){
		if ($where['oldpass'] != '' && $where['newpass']!= '' && $where['conpass'] != '') {
			$cek_pass = $this->db->query("SELECT *FROM tbl_user WHERE id_user='$where[id_user]'")->row_array();
			if (empty($cek_pass) || !password_verify($where['oldpass'], $cek_pass['password'])) {
				//kembalikan password lama tidak ada
				$load = $this->lang->line('oldpasswordfailed');
				$res['mes_alert'] = 'danger';
				$res['mes_display'] = 'block';
				$res['mes_message'] = $load;
				return $res;
			}else{
				if ($where['newpass'] != $where['conpass']) 
				{
						//kembalikan konpas dan password baru gak sama
					$load = $this->lang->line('failedpassword');
					$res['mes_alert'] = 'danger';
					$res['mes_display'] = 'block';
					$res['mes_message'] = $load;
					return $res;
				}
				else
				{
					$passbaru = password_hash($where['conpass'],PASSWORD_DEFAULT);
					$update_w_pass = $this->db->simple_query("UPDATE tbl_user SET password='$passbaru' WHERE id_user='$where[id_user]'");

					if ($update_w_pass) {

						$emailuser 		= $this->session->userdata('email');
						$user_name_user = $this->session->userdata('user_name');
						// Open connection
				        $ch = curl_init();

				        // Set the url, number of POST vars, POST data
				        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
				        curl_setopt($ch, CURLOPT_POST, true);
				        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_editAccountTutor','content' => array("user_name_user" => "$user_name_user", "emailuser" => "$emailuser") )));

				        // Execute post
				        $result = curl_exec($ch);
				        curl_close($ch);
				        // echo $result;
			            if ($result) {
		                 	$load = $this->lang->line('successfullyupdate');
							$res['mes_alert'] = 'success';
							$res['mes_display'] = 'block';
							$res['mes_message'] = $load;
							return $res;
			            }else{
		                  	$load = $this->lang->line('failedupdate');
							$res['mes_alert'] = 'danger';
							$res['mes_display'] = 'block';
							$res['mes_message'] = $load;
							return $res;   
			            }

					}else{
							//kembalian gagal update password
						$load = $this->lang->line('failedupdate');
						$res['mes_alert'] = 'danger';
						$res['mes_display'] = 'block';
						$res['mes_message'] = $load;
						return $res;
					}
				}
			}
			
		}
		$birthdate = $where['tahun_lahir']."-".$where['bulan_lahir']."-".$where['tanggal_lahir'];
		$umur = $birthdate;
		$umur = date_diff(date_create($umur), date_create('today'))->y;
		$user_name = $where['first_name'].' '.$where['last_name'];

		$update_wo_pass = $this->db->simple_query("UPDATE tbl_user SET user_name='$user_name', first_name='$where[first_name]', last_name='$where[last_name]', email='$where[email]', user_birthplace='$where[user_birthplace]', user_birthdate='$birthdate', user_countrycode='$where[kode_area]', user_callnum='$where[user_callnum]', user_gender='$where[user_gender]', user_religion='$where[user_religion]',  user_ktp='$where[user_ktp]', user_address='$where[user_address]', kodepos='$where[kodepos]', user_age='$umur' WHERE id_user='$where[id_user]'");
		if ($update_wo_pass) {
				//kembalian berhasil
			$load = $this->lang->line('successfullyupdate');
			$res['mes_alert'] = 'success';
			$res['mes_display'] = 'block';
			$res['mes_message'] = $load;
			$this->session->set_userdata('nama_lengkap',$where['user_name']);
			$this->session->set_userdata('no_hp',$where['user_callnum']);
			$this->session->set_userdata('user_ktp',$where['user_ktp']);
			$this->session->set_userdata('email',$where['email']);
			$this->session->set_userdata('address',$where['user_address']);
			$this->session->set_userdata('tanggal_lahir',$birthdate);
			$this->session->set_userdata('umur',$umur);
			$this->session->set_userdata('jenis_kelamin',$where['user_gender']);
			$this->session->set_userdata('agama',$where['user_religion']);
			return $res;
		}else{
			$load = $this->lang->line('failedupdate');
			$res['mes_alert'] = 'danger';
			$res['mes_display'] = 'block';
			$res['mes_message'] = $load;
			return $res;
		}

		return $res;
	}
	public function Edit_PasswordKids($table, $where){
		//cek kondisi change password
		// print_r($where);
		// return 0;

			$device= $where['device'];
			$id_user= $where['id_user'];
			// $id_user = $this->input->post('id_user');
			if ($device == 'web') {
				
				$newpass = $this->input->post('newpass');
				$conpass = $this->input->post('conpass');
				
			}
			else{
				$newpass = $this->input->post('newpass_'.$id_user);
				$conpass = $this->input->post('conpass_'.$id_user);		
			}
			

		if ($newpass != '' && $conpass!= '') {
			$cek_pass = $this->db->query("SELECT *FROM tbl_user WHERE id_user='$id_user'")->row_array();
			$lang = $this->session->userdata('lang');
			if (empty($cek_pass)) {
				//kembalikan password lama tidak ada
				$load = $this->lang->line('oldpasswordfailed');
				$res['mes_alert'] = 'danger';
				$res['mes_display'] = 'block';
				$res['mes_message'] = $load;
				$this->session->set_userdata('code',"1233");
				return $res;
			}else{
				if ($newpass != $conpass) {
						//kembalikan konpas dan password baru gak sama
					$load = $this->lang->line('failedpassword');
					$res['mes_alert'] = 'danger';
					$res['mes_display'] = 'block';
					$res['mes_message'] = $load;
					return $res;
					$this->session->set_userdata('code',"1233");
				}
				else
				{
					$passbaru = password_hash($conpass,PASSWORD_DEFAULT);
					$update_w_pass = $this->db->simple_query("UPDATE tbl_user SET password='$passbaru' WHERE id_user='$id_user'");

					if ($update_w_pass) {
						$emailuser 		= $this->session->userdata('email');
						$user_name_user = $this->session->userdata('user_name');
						       
		                // Open connection
				        $ch = curl_init();

				        // Set the url, number of POST vars, POST data
				        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
				        curl_setopt($ch, CURLOPT_POST, true);
				        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_editPasswordKids','content' => array("emailuser" => "$emailuser", "user_name_user" => "$user_name_user") )));

				        // Execute post
				        $result = curl_exec($ch);
				        curl_close($ch);
				        // echo $result;


			            if ($result) {
			                  	$load = $this->lang->line('successfullyupdate');
								$res['mes_alert'] = 'success';
								$res['mes_display'] = 'block';
								$res['mes_message'] = $load;
								return $res;
			            }else{
		                  	$load = $this->lang->line('failedupdate');
							$res['mes_alert'] = 'danger';
							$res['mes_display'] = 'block';
							$res['mes_message'] = $load;
							return $res;					
							$this->session->set_userdata('code',"1233"); 
			            }

					}else{
							//kembalian gagal update password
						$load = $this->lang->line('failedupdate');
						$res['mes_alert'] = 'danger';
						$res['mes_display'] = 'block';
						$res['mes_message'] = $load;
						
						return $res;
						$this->session->set_userdata('code',"1233");
					}
				}
			}
			
		}

			// $update_wo_pass = $this->db->simple_query("UPDATE tbl_user SET user_name='$where[user_name]', email='$where[email], user_callnum='$where[user_callnum], password=md5('$where[conpass]')");
		//$update_user = $this->db->simple_query
	}

	public function saveEdit_Accountstudent($table, $where){
		//cek kondisi change password
		// print_r($where);
		
		if ($where['newpass']!= '' && $where['conpass'] != '') {
			$cek_pass = $this->db->query("SELECT *FROM tbl_user WHERE id_user='$where[id_user]'")->row_array();
			$lang = $this->session->userdata('lang');
			if (empty($cek_pass)) {
				//kembalikan password lama tidak ada
				$load = $this->lang->line('oldpasswordfailed');
				$res['mes_alert'] = 'danger';
				$res['mes_display'] = 'block';
				$res['mes_message'] = $load;
				$this->session->set_userdata('code',"9999");
				return $res;
			}else{
				if ($where['newpass'] != $where['conpass']) {
						//kembalikan konpas dan password baru gak sama
					$load = $this->lang->line('failedpassword');
					$res['mes_alert'] = 'danger';
					$res['mes_display'] = 'block';
					$res['mes_message'] = $load;
					$this->session->set_userdata('code',"9898");
					return $res;
				}
				else
				{
					$passbaru = password_hash($where['conpass'],PASSWORD_DEFAULT);
					$update_w_pass = $this->db->simple_query("UPDATE tbl_user SET password='$passbaru'  WHERE id_user='$where[id_user]'");

					if ($update_w_pass) {
						$emailuser 		= $this->session->userdata('email');
						$user_name_user = $this->session->userdata('user_name');

						// Open connection
				        $ch = curl_init();

				        // Set the url, number of POST vars, POST data
				        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
				        curl_setopt($ch, CURLOPT_POST, true);
				        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_editAccountStudent','content' => array("user_name_user" => "$user_name_user", "emailuser" => "$emailuser") )));

				        // Execute post
				        $result = curl_exec($ch);
				        curl_close($ch);
				        // echo $result;
			            if ($result) {
		                 	$load = $this->lang->line('successfullyupdate');
							$res['mes_alert'] = 'success';
							$res['mes_display'] = 'block';
							$res['mes_message'] = $load;
							return $res;
							$this->session->set_userdata('ubah_pass',"9696");
							$this->session->set_userdata('code_cek',"0");	
			            }else{
		                  	$load = $this->lang->line('failedupdate');
							$res['mes_alert'] = 'danger';
							$res['mes_display'] = 'block';
							$res['mes_message'] = $load;
							return $res;		
							$this->session->set_userdata('ubah_pass',"9797");	 
							$this->session->set_userdata('code',"9797");	 
			            }


					}else{
							//kembalian gagal update password
						$load = $this->lang->line('failedupdate');
						$res['mes_alert'] = 'danger';
						$res['mes_display'] = 'block';
						$res['mes_message'] = $load;
						$this->session->set_userdata('code',"9797");
						return $res;
					}
				}
			}
			
		}
		else if ($where['tahun_lahir'] != '' && $where['bulan_lahir'] != '' && $where['tanggal_lahir'] != '') { 			
			$birthdate = $where['tahun_lahir']."-".$where['bulan_lahir']."-".$where['tanggal_lahir'];
			// $birthplace = $where['user_birthplace'];
			$user_callnum = $where['user_callnum'];
			$nomerhp = $where['kode_area']."".$where['user_callnum'];
			$umur = $birthdate;
			$umur = date_diff(date_create($umur), date_create('today'))->y;
			$username = $where['first_name'].' '.$where['last_name'];
			$update_wo_pass = $this->db->simple_query("UPDATE tbl_user SET user_name='$username', first_name='$where[first_name]', last_name='$where[last_name]', email='$where[email]', user_countrycode='$where[kode_area]',user_nationality='$where[user_nationality]',user_gender='$where[user_gender]', user_callnum='$user_callnum', user_birthdate='$birthdate', user_address='$where[user_address]', user_age=$umur WHERE id_user='$where[id_user]'");

			if ($update_wo_pass) {
				//kembalian berhasil
				$load = $this->lang->line('successfullyupdate');
				$res['mes_alert'] = 'success';
				$res['mes_display'] = 'block';
				$res['mes_message'] = $load;
				$this->session->set_userdata('jenis_kelamin',$where['user_gender']);
				$this->session->set_userdata('nama_lengkap',$username);
				$this->session->set_userdata('username',$username);
				$this->session->set_userdata('no_hp',$where['user_callnum']);
				$this->session->set_userdata('email',$where['email']);
				$this->session->set_userdata('tanggal_lahir',$birthdate);
				$this->session->set_userdata('code', '9696');
				$this->session->set_userdata('code_cek',"0");	
				return $res;
					//$this->session->set_userdata('no_hp',$w['user_callnum']);
			}else{
				$load = $this->lang->line('failedupdate');
				$res['mes_alert'] = 'danger';
				$res['mes_display'] = 'block';
				$res['mes_message'] = $load;
				$this->session->set_userdata('code',"9797");
				return $res;
			}

			return $res;	
		}
		else
		{					
			$username = $where['first_name'].' '.$where['last_name'];
			$update_wo_pass = $this->db->simple_query("UPDATE tbl_user SET user_name='$username', first_name='$where[first_name]', last_name='$where[last_name]', email='$where[email]', user_countrycode='$where[kode_area]' ,user_nationality='$where[user_nationality]',user_address='$where[user_address]', user_gender='$where[user_gender]', user_callnum='$where[user_callnum]' WHERE id_user='$where[id_user]'");

			if ($update_wo_pass) {
				//kembalian berhasil
				$load = $this->lang->line('successfullyupdate');
				$res['mes_alert'] = 'success';
				$res['mes_display'] = 'block';
				$res['mes_message'] = $load;
				$this->session->set_userdata('nama_lengkap',$where['user_name']);
				$this->session->set_userdata('no_hp',$where['user_callnum']);
				$this->session->set_userdata('email',$where['email']);
				$this->session->set_userdata('tanggal_lahir',$birthdate);
				$this->session->set_userdata('code', '9696');
					//$this->session->set_userdata('no_hp',$w['user_callnum']);
				return $res;
			}else{
				$load = $this->lang->line('failedupdate');
				$res['mes_alert'] = 'danger';
				$res['mes_display'] = 'block';
				$res['mes_message'] = $load;
				$this->session->set_userdata('code',"9797");
				return $res;
			}
			return $res;
		}	
		// return false;
		// return 0;
			// $update_wo_pass = $this->db->simple_query("UPDATE tbl_user SET user_name='$where[user_name]', email='$where[email], user_callnum='$where[user_callnum], password=md5('$where[conpass]')");
		//$update_user = $this->db->simple_query
	}

	public function saveEdit_AccountstudentBCU($table, $where){
		//cek kondisi change password
		print_r($where);
		
		if ($where['newpass']!= '' && $where['conpass'] != '') {
			$cek_pass = $this->db->query("SELECT *FROM tbl_user WHERE id_user='$where[id_user]'")->row_array();
			$lang = $this->session->userdata('lang');
			if (empty($cek_pass)) {
				//kembalikan password lama tidak ada
				$load = $this->lang->line('oldpasswordfailed');
				$res['mes_alert'] = 'danger';
				$res['mes_display'] = 'block';
				$res['mes_message'] = $load;
				$this->session->set_userdata('code',"9999");
				return $res;
			}else{
				if ($where['newpass'] != $where['conpass']) {
						//kembalikan konpas dan password baru gak sama
					$load = $this->lang->line('failedpassword');
					$res['mes_alert'] = 'danger';
					$res['mes_display'] = 'block';
					$res['mes_message'] = $load;
					$this->session->set_userdata('code',"9898");
					return $res;
				}
				else
				{
					$passbaru = password_hash($where['conpass'],PASSWORD_DEFAULT);
					$update_w_pass = $this->db->simple_query("UPDATE tbl_user SET password='$passbaru'  WHERE id_user='$where[id_user]'");

					if ($update_w_pass) {
						$emailuser 		= $this->session->userdata('email');
						$user_name_user = $this->session->userdata('user_name');

						// Open connection
				        $ch = curl_init();

				        // Set the url, number of POST vars, POST data
				        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
				        curl_setopt($ch, CURLOPT_POST, true);
				        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_editAccountStudent','content' => array("user_name_user" => "$user_name_user", "emailuser" => "$emailuser") )));

				        // Execute post
				        $result = curl_exec($ch);
				        curl_close($ch);
				        // echo $result;
			            if ($result) {
		                 	$load = $this->lang->line('successfullyupdate');
							$res['mes_alert'] = 'success';
							$res['mes_display'] = 'block';
							$res['mes_message'] = $load;
							return $res;
							$this->session->set_userdata('ubah_pass',"9696");
							$this->session->set_userdata('code_cek',"0");	
			            }else{
		                  	$load = $this->lang->line('failedupdate');
							$res['mes_alert'] = 'danger';
							$res['mes_display'] = 'block';
							$res['mes_message'] = $load;
							return $res;		
							$this->session->set_userdata('ubah_pass',"9797");	 
							$this->session->set_userdata('code',"9797");	 
			            }


					}else{
							//kembalian gagal update password
						$load = $this->lang->line('failedupdate');
						$res['mes_alert'] = 'danger';
						$res['mes_display'] = 'block';
						$res['mes_message'] = $load;
						$this->session->set_userdata('code',"9797");
						return $res;
					}
				}
			}
			
		}
		else if ($where['tahun_lahir'] != '' && $where['bulan_lahir'] != '' && $where['tanggal_lahir'] != '') { 
			echo "masuk ini";
			$birthdate = $where['tahun_lahir']."-".$where['bulan_lahir']."-".$where['tanggal_lahir'];
			$birthplace = $where['user_birthplace'];
			$user_callnum = $where['user_callnum'];
			$nomerhp = $where['kode_area']."".$where['user_callnum'];
			$umur = $birthdate;
			$umur = date_diff(date_create($umur), date_create('today'))->y;
			$username = $where['username'];
			$update_wo_pass = $this->db->simple_query("UPDATE tbl_user SET username='$username', first_name='$where[first_name]', last_name='$where[last_name]', email='$where[email]', user_countrycode='$where[kode_area]',user_nationality='$where[user_nationality]',user_gender='$where[user_gender]', user_callnum='$user_callnum', user_birthdate='$birthdate',user_birthplace='$birthplace', user_address='$where[user_address]',user_religion='$where[user_religion]', user_age=$umur WHERE id_user='$where[id_user]'");

			if ($update_wo_pass) {
				//kembalian berhasil
				$load = $this->lang->line('successfullyupdate');
				$res['mes_alert'] = 'success';
				$res['mes_display'] = 'block';
				$res['mes_message'] = $load;
				$this->session->set_userdata('jenis_kelamin',$where['user_gender']);
				$this->session->set_userdata('nama_lengkap',$username);
				$this->session->set_userdata('username',$username);
				$this->session->set_userdata('no_hp',$where['user_callnum']);
				$this->session->set_userdata('email',$where['email']);
				$this->session->set_userdata('tanggal_lahir',$birthdate);
				$this->session->set_userdata('code', '9696');
				$this->session->set_userdata('code_cek',"0");	
				return $res;
					//$this->session->set_userdata('no_hp',$w['user_callnum']);
			}else{
				$load = $this->lang->line('failedupdate');
				$res['mes_alert'] = 'danger';
				$res['mes_display'] = 'block';
				$res['mes_message'] = $load;
				$this->session->set_userdata('code',"9797");
				return $res;
			}

			return $res;	
		}
		else
		{		
		echo "masuk sono"							;
			$username = $where['username'];
			$update_wo_pass = $this->db->simple_query("UPDATE tbl_user SET username='$username', first_name='$where[first_name]', last_name='$where[last_name]', email='$where[email]', user_countrycode='$where[kode_area]' ,user_nationality='$where[user_nationality]',user_address='$where[user_address]',user_religion='$where[user_religion]', user_gender='$where[user_gender]', user_callnum='$where[user_callnum]' WHERE id_user='$where[id_user]'");

			if ($update_wo_pass) {
				//kembalian berhasil
				$load = $this->lang->line('successfullyupdate');
				$res['mes_alert'] = 'success';
				$res['mes_display'] = 'block';
				$res['mes_message'] = $load;
				$this->session->set_userdata('nama_lengkap',$where['user_name']);
				$this->session->set_userdata('no_hp',$where['user_callnum']);
				$this->session->set_userdata('email',$where['email']);
				$this->session->set_userdata('tanggal_lahir',$birthdate);
				$this->session->set_userdata('code', '9696');
					//$this->session->set_userdata('no_hp',$w['user_callnum']);
				return $res;
			}else{
				$load = $this->lang->line('failedupdate');
				$res['mes_alert'] = 'danger';
				$res['mes_display'] = 'block';
				$res['mes_message'] = $load;
				$this->session->set_userdata('code',"9797");
				return $res;
			}
			return $res;
		}	
		return false;
		return 0;
			// $update_wo_pass = $this->db->simple_query("UPDATE tbl_user SET user_name='$where[user_name]', email='$where[email], user_callnum='$where[user_callnum], password=md5('$where[conpass]')");
		//$update_user = $this->db->simple_query
	}


	function saveEdit_ProfileStudent($table,$where){
		$update_tbl_user = $this->db->simple_query("UPDATE tbl_user SET user_birthplace='$where[user_birthplace]', user_address='$where[user_address]', user_religion='$where[user_religion]' WHERE id_user='$where[id_user]'");

		if ($update_tbl_user) {
			// $this->session->set_userdata('jenis_kelamin',$where['user_gender']);
			// $this->session->set_userdata('nama_lengkap',$res['user_name']);
			$this->session->set_userdata('agama',$where['user_religion']);
			// $this->session->set_userdata('usertype_id', $res['usertype_id']);
			// $this->session->set_userdata('jenjang_id', $where['jenjang_id']);
			$this->session->set_userdata('address', $where['user_address']);
			$this->session->set_userdata('code', '9696');
			$this->session->set_userdata('code_cek','0');

			return $res;
		}else{
			$load = $this->lang->line('failedupdate');
			$res['mes_alert'] = 'danger';
			$res['mes_display'] = 'block';
			$res['mes_message'] = $load;
			$this->session->set_userdata('code', '9797');

			return $res;	
		}
	}


	function saveEdit_SchoolStudent($table,$where){

			$school_name= $where['school_name'];
			$school_address= $where['school_address'];
			$provinsi_id = $where['school_provinsi'];
			$kabupaten_id= $where['school_kabupaten'];
			$kecamatan_id= $where['school_kecamatan'];
			$school_id 		= $this->Rumus->gen_id_school();

			$id_user = $this->session->userdata('id_user');
			$cek_school_id = $this->db->query("SELECT school_id FROM `master_school` where school_name='$school_name' AND school_address='$school_address'")->row_array()['school_id'];
			// echo $cek_school_id;

			if ($cek_school_id) {
				$update_tbl_profile_student = $this->db->simple_query("UPDATE tbl_profile_student SET school_id = '$cek_school_id' WHERE student_id='$id_user'");
				$load = $this->lang->line('successfullyupdate');
				$res['mes_alert'] = 'success';
				$res['mes_display'] = 'block';
				$res['mes_message'] = $load;

				return $res;
			}else{
				$add_school = $this->db->simple_query("INSERT INTO master_school_temp(school_id, school_name, school_address, provinsi_id, kabupaten_id, kecamatan_id) VALUES('{$school_id}','{$school_name}','{$school_address}','{$provinsi_id}','{$kabupaten_id}','{$kecamatan_id}')");
				if ($add_school) {
					$update_tbl_profile_student = $this->db->simple_query("UPDATE tbl_profile_student SET school_id = '$school_id' WHERE student_id='$id_user'");
					$load = $this->lang->line('successfullyupdate');
					$res['mes_alert'] = 'success';
					$res['mes_display'] = 'block';
					$res['mes_message'] = $load;
				}
				else{

					$load = $this->lang->line('failedupdate');
					$res['mes_alert'] = 'danger';
					$res['mes_display'] = 'block';
					$res['mes_message'] = $load;

					return $res;
				}
			}

	}


	function saveEditChannel($table,$where){

		$channel_link = $where['channel_link'];
		$channel_id = $where['channel_id'];
		$channel_logo = $where['channel_logo'];
		$cek = $this->db->query("SELECT *FROM master_channel WHERE channel_link='$channel_link' and channel_id != '$channel_id'")->row_array();
		$cek_logo = $this->db->query("SELECT *FROM master_channel WHERE channel_logo='$channel_logo'")->row_array();

		if (empty($cek)) {
			$update_master_channel = $this->db->simple_query("UPDATE master_channel SET channel_name='$where[channel_name]', channel_description='$where[channel_description]', channel_about='$where[channel_about]', channel_country_code='$where[channel_country_code]', channel_callnum='$where[channel_callnum]', channel_email='$where[channel_email]', channel_address='$where[channel_address]', channel_link='$where[channel_link]' WHERE channel_id='$where[channel_id]'");
			$myip = $_SERVER['REMOTE_ADDR'];
			$plogc_id = $this->Rumus->getLogChnTrxID();
			$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Update setting data channel',NULL,'$myip',now())");

			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Berhasil Mengubah Data');
			redirect('/Channel/setting');
		}
		else 
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Nama channel link sudah ada, silakan ganti dengan yang lain');
			redirect('/Channel/setting');
			return 0;
		}
		

	}
	public function getPos($desa_id = null)
	{
		return $this->db->query("SELECT kodepos FROM master_desa  WHERE desa_id='$desa_id'")->row_array()['kodepos'];
	}

	function send_EmailStatus($where){
		// $get_id_user = $this->db->query("SELECT *FROM tbl_user WHERE email='$where[emailaddress]'")->row_array()['id_user'];
		// $get_gen_auth = $this->db->query("SELECT *FROM gen_auth WHERE id_user='$get_id_user'")->row_array()['random_key'];

		// return $get_gen_auth;
	}

	function resend_Email($where){
		$get_id_user = $this->db->query("SELECT *FROM tbl_user WHERE email='$where[emailaddress]'")->row_array()['id_user'];
		$get_gen_auth = $this->db->query("SELECT *FROM gen_auth WHERE id_user='$get_id_user'")->row_array()['random_key'];

		return $get_gen_auth;
	}

	function resend_Emaill($where){
		// $get_id_user = $this->db->query("SELECT *FROM tbl_user WHERE email='$where[emailaddress]'")->row_array()['id_user'];
		$get_gen_auth = $this->db->query("SELECT *FROM gen_auth WHERE id_user='$where[id_user]'")->row_array()['random_key'];

		return $get_gen_auth;
	}


	//INI ADMIN COY
	function getPendingApproval(){

		$select_all_data = $this->db->query("SELECT ts.* , tpt.* FROM tbl_profile_tutor AS tpt INNER JOIN tbl_user AS ts ON tpt.tutor_id=ts.id_user WHERE ts.usertype_id ='tutor' AND ts.status='2' ORDER BY ts.created_at DESC")->result_array();
		return $select_all_data;
	}

	function getAllTutor(){

		$select_all_data = $this->db->query("SELECT ts.* , tpt.* FROM tbl_profile_tutor AS tpt INNER JOIN tbl_user AS ts ON tpt.tutor_id=ts.id_user WHERE ts.usertype_id ='tutor' AND ts.status=1 ORDER BY ts.created_at DESC")->result_array();
		return $select_all_data;
	}

	public function getListTutorPayment()
	{
		$select_data = $this->db->query("SELECT DISTINCT tcr.id_user, tu.user_name, tu.user_image, tc.class_id, tcr.class_ack, tcr.flag, tcr.notes, tc.name, tc.tutor_id, tc.subject_id, tc.description, tc.class_type, tc.start_time, tc.finish_time FROM `tbl_class_rating` as tcr INNER JOIN tbl_class as tc ON tcr.class_id=tc.class_id INNER JOIN tbl_user as tu ON tu.id_user=tc.tutor_id WHERE tcr.class_ack=1 and (tc.class_type='multicast_paid' OR tc.class_type='private' OR tc.class_type='group')")->result_array();
		return $select_data;
	}

	public function tutordesc()
	{
		$tutor_id = $this->input->get('id_user');
		$year_experience = 0;
		$last_education = null;
		$nama_lembaga = null;
		$edu_score = 0;
		if($tutor_id != ''){
			$data  = $this->db->query("SELECT * FROM tbl_user as tu LEFT JOIN tbl_profile_tutor as tpt ON tu.id_user=tpt.tutor_id WHERE tu.id_user='".$tutor_id."' AND tu.usertype_id='tutor'")->row_array();
			if(!empty($data)){
				if($data['education_background'] != ''){
					$data['education_background'] = unserialize($data['education_background']);
					foreach ($data['education_background'] as $key => $value) {
						$a = $this->db->query("SELECT jenjanglevel_score FROM master_jenjanglevel WHERE jenjanglevel_name='".$value['educational_level']."'")->row_array();
						if(!empty($a)){
							$a = $a['jenjanglevel_score'];
						}else{
							$a = 0;
						}
						if($a > $edu_score){
							$edu_score = $a;
							$data['nama_lembaga'] = $value['educational_institution'];
							$last_education = $value['educational_level'];
						}
					}
					if($edu_score >=4 ){
						$data['last_education'] = $last_education;
					}else{
						$data['last_education'] = $last_education;
					}
				}
				// if($data['educational_institution'] != ''){
				// 	$data['educational_institution'] = unserialize($data['educational_institution']);
				// 	$deb = $data['educational_institution'];
				// 	foreach ($deb as $key => $value) {
				// 		$a = $this->db->query("SELECT jenjanglevel_score FROM master_jenjanglevel WHERE jenjanglevel_name='".$value['educational_level']."'")->row_array();
				// 		if(!empty($a)){
				// 			$a = $a['jenjanglevel_score'];
				// 		}else{
				// 			$a = 0;
				// 		}
				// 		if($a > $edu_score){
				// 			$edu_score = $a;
				// 			$last_instituion = $value['educational_institution'];
				// 		}
				// 	}
				// 	if($edu_score >=4 ){
				// 		$data['educational_institution'] = $last_instituion;
				// 	}else{
				// 		$data['educational_institution'] = $last_instituion;
				// 	}
				// }
				if($data['teaching_experience'] != ''){
					$data['teaching_experience'] = unserialize($data['teaching_experience']);
					$dte = $data['teaching_experience'];
					foreach ($dte as $key => $value) {
						$a = $value['year_experience2']-$value['year_experience1'];
						$year_experience+=$a;
					}
				}
				$data['year_experience'] = $year_experience;
				$competency = $this->db->query("SELECT tb.id_user, tb.subject_id ,ms.* FROM `tbl_booking` as tb INNER JOIN master_subject as ms ON tb.subject_id=ms.subject_id WHERE tb.id_user=".$tutor_id)->result_array();
				$data['competency'] = [];
				foreach ($competency as $key => $value) {
					$jenjangid = $value['jenjang_id'];
					$selectjenjang = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();
					if ($this->session->userdata('lang') == 'indonesia') {						
						$data['competency'][$key]['subject_name'] = $value['indonesia'];
						$data['competency'][$key]['jenjang_name'] = $selectjenjang['jenjang_name'];
						$data['competency'][$key]['jenjang_level'] = $selectjenjang['jenjang_level'];
					}
					else
					{										
						$data['competency'][$key]['subject_name'] = $value['english'];
						$data['competency'][$key]['jenjang_name'] = $selectjenjang['jenjang_name'];
						$data['competency'][$key]['jenjang_level'] = $selectjenjang['jenjang_level'];
					}
					
				}
				
				$rets['status'] = true;
				$rets['message'] = "Tutor found";
				$rets['data'] = $data;
				return $data;
			}else{
				$rets['status'] = true;
				$rets['message'] = "Tutor not found";
				$rets['data'] = null;
				return $data;
			}
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		return $data;
	}

	public function load_history(){
		
	}

	public function ambildataphoto()
	{
		$iduser = $this->session->userdata('id_user');
		$get_image = $this->db->query("SELECT * FROM tbl_user WHERE id_user='".$iduser."'")->row_array();
		return $get_image;
	}	

	function changeAvatar() {
        $post = isset($_POST) ? $_POST: array();
        $max_width = "500"; 
        $userId = isset($post['hdn-profile-id']) ? intval($post['hdn-profile-id']) : 0;
        $path ='./aset/img/user';

        $valid_formats = array("jpg", "png", "gif", "bmp","jpeg");
        $name = $_FILES['photoimg']['name'];
        $size = $_FILES['photoimg']['size'];
        if(strlen($name))
        {
        list($txt, $ext) = explode(".", $name);
        if(in_array($ext,$valid_formats))
        {
        if($size<(1024*1024)) // Image size max 1 MB
        {
        $actual_image_name = $name;
        $filePath = $path .'/'.$actual_image_name;
        $tmp = $_FILES['photoimg']['tmp_name'];
        
        if(move_uploaded_file($tmp, $filePath))
        {
        $width = getWidth($filePath);
            $height = getHeight($filePath);
            //Scale the image if it is greater than the width set above
            if ($width > $max_width){
                $scale = $max_width/$width;
                $uploaded = resizeImage($filePath,$width,$height,$scale);
            }else{
                $scale = 1;
                $uploaded = resizeImage($filePath,$width,$height,$scale);
            }
        /*$res = saveAvatar(array(
                        'userId' => isset($userId) ? intval($userId) : 0,
                                                'avatar' => isset($actual_image_name) ? $actual_image_name : '',
                        ));*/
                        
        //mysql_query("UPDATE users SET profile_image='$actual_image_name' WHERE uid='$session_id'");
        echo "<img id='photo' file-name='".$actual_image_name."' class='' src='".$filePath.'?'.time()."' class='preview'/>";
        }
        else
        echo "failed";
        }
        else
        echo "Image file size max 1 MB"; 
        }
        else
        echo "Invalid file format.."; 
        }
        else
        echo "Please select image..!";
        exit;
        
        
    }
    /*********************************************************************
     Purpose            : update image.
     Parameters         : null
     Returns            : integer
     ***********************************************************************/
     function saveAvatarTmp($act = '') {
        $post = isset($_POST) ? $_POST: array();
        $userId = isset($post['id']) ? intval($post['id']) : 0;
        $path ='./aset/img/user/';
        $t_width = 300; // Maximum thumbnail width
        $t_height = 300;    // Maximum thumbnail height
		
    if(isset($_POST['t']) and $_POST['t'] == "ajax")
    {
        extract($_POST);
        
        //$img = get_user_meta($userId, 'user_avatar', true);
        $imagePath = './aset/img/user/'.$_POST['image_name'];
        $ratio = ($t_width/$w1); 
        $nw = ceil($w1 * $ratio);
        $nh = ceil($h1 * $ratio);
        $nimg = imagecreatetruecolor($nw,$nh);
        $im_src = imagecreatefromjpeg($imagePath);
        imagecopyresampled($nimg,$im_src,0,0,$x1,$y1,$nw,$nh,$w1,$h1);
        imagejpeg($nimg,$imagePath,90);
        
    }
    echo $imagePath.'?'.time();;
    exit(0);    
    }
    
    /*********************************************************************
     Purpose            : resize image.
     Parameters         : null
     Returns            : image
     ***********************************************************************/
    function resizeImage($image,$width,$height,$scale) {
    $newImageWidth = ceil($width * $scale);
    $newImageHeight = ceil($height * $scale);
    $newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
    $source = imagecreatefromjpeg($image);
    imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);
    imagejpeg($newImage,$image,90);
    chmod($image, 0777);
    return $image;
}
/*********************************************************************
     Purpose            : get image height.
     Parameters         : null
     Returns            : height
     ***********************************************************************/
function getHeight($image) {
    $sizes = getimagesize($image);
    $height = $sizes[1];
    return $height;
}
/*********************************************************************
     Purpose            : get image width.
     Parameters         : null
     Returns            : width
     ***********************************************************************/
function getWidth($image) {
    $sizes = getimagesize($image);
    $width = $sizes[0];
    return $width;
}

}