<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Kidsmod extends CI_Model{	

	public function reg_kids($data='',$parent_id='')
	{
		$email = $data['email'];
		$username = $data['username'];
		$first_name = $data['first_name'];
		$last_name = $data['last_name'];
		$user_name = $first_name." ".$last_name;
		$id_user = $this->Rumus->gen_id_user();
		$password = password_hash($data['password'],PASSWORD_DEFAULT);
		$user_birthdate = $data['user_birthdate'];
		$user_birthplace = $data['user_birthplace'];
		$user_gender = $data['user_gender'];
		$usertype_id = 'student kid';
		$user_callnum = $this->session->userdata('no_hp');
		$user_countrycode = $this->session->userdata('kode_area');

		$school_name = $data['school_name'];
		$school_address = $data['school_address'];
		$jenjang_id = $data['jenjang_id'];

		if($this->db->simple_query("INSERT INTO tbl_user(id_user, email, username, user_name, first_name, last_name, password, user_birthplace, user_birthdate, user_countrycode, user_callnum, user_image, user_gender, usertype_id, status) VALUES($id_user, '$email', '$username', '$user_name', '$first_name', '$last_name', '$password', '$user_birthplace', '$user_birthdate', '$user_countrycode', '$user_callnum', '".base64_encode('user/empty.jpg')."', '$user_gender', '$usertype_id', 1)")){

			$school_id = $this->db->query("SELECT school_id FROM `master_school` where school_name='$school_name' AND school_address='$school_address'")->row_array()['school_id'];
			$this->db->simple_query("INSERT INTO tbl_profile_kid(kid_id, school_id, school_name, school_address, jenjang_id, parent_id) VALUES($id_user, '$school_id', '$school_name', '$school_address', $jenjang_id, $parent_id)");
			return 1;
		}else{
			$error = $this->db->error();
			if(strpos($error['message'], 'username') !== false){
				return -2; // USERNAME DUPLICATE
			}
		}
		return -1;// ANOTHER ERROR OCCURED

	}
	function cek_cokeis($table,$where){		
		// check user ada apa gak di database
		$res = $this->db->query("SELECT ts.*, tps.jenjang_id FROM tbl_user as ts LEFT JOIN tbl_profile_student as tps ON ts.id_user=tps.student_id WHERE ( email='$where[email]' || username='$where[email]' )")->row_array();
		if(empty($res) ){
			return 0;			
		}
				
		// check session generated nya masih login atau udh logout
		// $ids = $res['id_user'];
		// $query = $this->db->query("SELECT *FROM temp_session WHERE id_user='$ids'")->row_array();
		// if (!empty($query)) {
		// 	return 0;
		// }

		// $nm_lengkap = $res['first_name']+$res['last_name'];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://classmiles.com/Rest/googleAuthGetJWT");
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$res);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS,4000); 

		$curl_rets = curl_exec($ch);
		// if(curl_errno($ch)){
    		// echo 'Curl error: ' . curl_error($ch);
		// }
		$access_token = json_decode($curl_rets,true);
		
		$this->session->set_userdata('access_token_jwt',$access_token['access_token']);
		
		$this->session->set_userdata('id_user',$res['id_user']);
		$this->session->set_userdata('email',$res['email']);
		$this->session->set_userdata('kode_area',$res['user_countrycode']);
		$this->session->set_userdata('no_hp',$res['user_callnum']);
		$this->session->set_userdata('jenis_kelamin',$res['user_gender']);
		$this->session->set_userdata('user_name', $res['user_name']);
		$this->session->set_userdata('tanggal_lahir',$res['user_birthdate']);
		$this->session->set_userdata('umur',$res['user_age']);
		$this->session->set_userdata('agama',$res['user_religion']);
		$this->session->set_userdata('usertype_id', $res['usertype_id']);
		$this->session->set_userdata('jenjang_id', $res['jenjang_id']);	
		$this->session->set_userdata('user_image', $res['user_image']);
		$this->session->set_userdata('status', $res['status']);		
		$this->session->set_userdata('address', $res['user_address']);
		$this->session->set_userdata('number', $res['password']);
		// $this->session->set_userdata('lang','indonesia');
		$this->session->set_userdata('color','blue');		
		// $this->session->set_userdata('lang','english');
		//$this->session->set_userdata('self_description', $res['self_description']);			

		// if ($res['usertype_id'] == "tutor") {
		// 	$select_session = $this->db->query("SELECT session_id FROM tbl_session WHERE tutor_id='$res[id_user]'")->row_array();
		// 	$this->session->set_userdata('temp_session', $select_session['session_id']);
		// }else if($res['usertype_id'] == "student"){
			// $select_session = $this->db->query("SELECT session_id FROM tbl_participant_session WHERE id_user='$res[id_user]'")->row_array();
			// $this->session->set_userdata('temp_session', $select_session['id']);

		/*$select_subject_id = $this->db->query("SELECT subject_id FROM master_subject WHERE jenjang_id = '$select_jenjang_id[jenjang_id]'")->row_array();
		$subject_id = $this->session->set_userdata('subject_id', $select_subject_id['subject_id']);*/

		//$show_tutor = $this->db->query("SELECT *FROM tbl_booking WHERE subject_id = '$select_subject_id[subject_id]'")->result();
		//print_r($show_tutor);
		
		// $this->M_Login->createSess();
		// $session_auth = $this->gen_session();
		// $ids = $res['id_user'];
		// $save = $this->db->simple_query("INSERT INTO temp_session (id_user, temp_session) VALUES ('$ids','$session_auth')");
		// if ($save) {
		// 	return 1;
		// }else{
		// 	return $save;
		// }

		return 1;
		
		//Create session login 

		
		
	}


	function google_login($email = ''){		
		// check user ada apa gak di database
		$res = $this->db->query("SELECT ts.*, tps.jenjang_id FROM tbl_user as ts LEFT JOIN tbl_profile_student as tps ON ts.id_user=tps.student_id WHERE email='".$email."' ")->row_array();
		if(empty($res) ){
			return 0;			
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://classmiles.com/Rest/googleAuthGetJWT");
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$res);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS,4000); 

		$curl_rets = curl_exec($ch);
		// if(curl_errno($ch)){
    		// echo 'Curl error: ' . curl_error($ch);
		// }
		$access_token = json_decode($curl_rets,true);
		$this->session->set_userdata('access_token_jwt',$access_token['access_token']);
		$this->session->set_userdata('id_user',$res['id_user']);
		$this->session->set_userdata('email',$res['email']);
		$this->session->set_userdata('kode_area',$res['user_countrycode']);
		$this->session->set_userdata('no_hp',$res['user_callnum']);
		$this->session->set_userdata('jenis_kelamin',$res['user_gender']);
		$this->session->set_userdata('nama_lengkap',$res['user_name']);
		$this->session->set_userdata('tanggal_lahir',$res['user_birthdate']);
		$this->session->set_userdata('umur',$res['user_age']);
		$this->session->set_userdata('agama',$res['user_religion']);
		$this->session->set_userdata('usertype_id', $res['usertype_id']);
		$this->session->set_userdata('jenjang_id', $res['jenjang_id']);	
		$this->session->set_userdata('user_image', $res['user_image']);
		$this->session->set_userdata('status', $res['status']);		
		$this->session->set_userdata('address', $res['user_address']);
		$this->session->set_userdata('number', $res['password']);
		$this->session->set_userdata('color','blue');
		if ($this->session->userdata('usertype_id') == 'student') {
			if ($this->session->userdata('status')==0) {
				$load = $this->lang->line('checkemail');
				$rets['mes_alert'] ='warning';
				$rets['mes_display'] ='block';
				$rets['mes_message'] =$load;
				$rets['redirect_path'] = 'Subjects';
				return null;
			}
			$rets['redirect_path'] = '/';
			return null;
		}else if($this->session->userdata('usertype_id') == 'tutor'){
			if ($this->session->userdata('status')==3) {
				$load = $this->lang->line('completetutorapproval');
				$rets['mes_alert'] ='warning';
				$rets['mes_display'] ='block';
				$rets['mes_message'] =$load;
				$rets['redirect_path'] = '/tutor/approval';
				return $rets;
			}else if($this->session->userdata('status')==2){
				$load = $this->lang->line('requestsent');
				$rets['mes_alert'] ='info';
				$rets['mes_display'] ='block';
				$rets['mes_message'] =$load;
				$rets['redirect_path'] = '/';
				return $rets;
			}else if($this->session->userdata('status')==0){
				$load = $this->lang->line('checkemail');
				$rets['mes_alert'] ='warning';
				$rets['mes_display'] ='block';
				$rets['mes_message'] =$load;
				$rets['redirect_path'] = '/';
				return $rets;
			}
		}else if($this->session->userdata('usertype_id') == 'admin'){
			$rets['redirect_path'] = '/admin';
			return $rets;
		}else{
			$rets['redirect_path'] = '/moderator';
			return $rets;
		}

			
		return 1;
		
	}

	function createSess($id_user, $email){
		//Create session login 
		//alert('Heii');
		$session_auth = $this->gen_session($email);
		$ids = $id_user;
		$save = $this->db->simple_query("INSERT INTO temp_session (id_user, temp_session, roomNumber) VALUES ('$id_user','$session_auth','1234567')");
		if ($save) {
			return $session_auth;
		}else{
			return $save;
		}
	}

	function randomKey(){

			$digit = 32;
			$karakter = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+{}|:<>?';,./[]";
			srand((double)microtime()*1000000);
			$i = 0;
			$pass = "";
			while ($i <= $digit-1)
			{
			$num = rand() % 32;
			$tmp = substr($karakter,$num,1);
			$pass = $pass.$tmp;
			$i++;
			}
			return $pass;
	}



	function daftar_baru($table,$where){
		//$pass = md5($where['kata_sandi']);
		$email = $where['email'];
		$umur = $where['tanggal_lahir'];
		$skrg = date_diff(date_create($umur), date_create('today'))->y;
		$cek = $this->db->query("SELECT *FROM tbl_user WHERE email='$email'")->row_array();
		if (empty($cek)) {
			$id_user = $this->Rumus->gen_id_user();
			// echo "INSERT INTO tbl_user (id_user, user_name, first_name, last_name, email, password, user_birthdate, user_callnum, user_gender, usertype_id, user_image, st_tour, user_age) VALUES ('$id_user','$where[user_name]','$where[first_name]','$where[last_name]','$where[email]','$where[kata_sandi]','$where[tanggal_lahir]','$where[no_hp_murid]','$where[jenis_kelamin]','student','empty.jpg','0','$skrg')";
			// return 1;
			$res = $this->db->simple_query("INSERT INTO tbl_user (id_user, user_name, first_name, last_name, email, password, user_birthdate, user_countrycode, user_callnum, user_gender, usertype_id, user_image, st_tour, user_age) VALUES ('$id_user','$where[user_name]','$where[first_name]','$where[last_name]','$where[email]','$where[kata_sandi]','$where[tanggal_lahir]','$where[user_countrycode]','$where[no_hp_murid]','$where[jenis_kelamin]','student','empty.jpg','0','$skrg')");
				if ($res) {
					$return['status'] = true;
					$return['message'] = "succeed";
					$r_key = $this->M_login->randomKey();

					//TAMBAH NOTIF
					$notif_message = json_encode( array("code" => 33));
					$link = "https://classmiles.com/master/resendEmaill";
					$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','$link','0')");

					$simpan = $this->db->simple_query("INSERT INTO gen_auth (random_key, id_user) VALUES ('$r_key','$id_user')");
					$simpan_profile_student = $this->db->simple_query("INSERT INTO tbl_profile_student (student_id, jenjang_id) VALUES ('$id_user','$where[jenjang_id]')");

					// $simpan_notif = $this->Rumus->create_notif($notif='Please, check your email to verify this account',$notif_mobile='Please, check your email to verify this account',$notif_type='single',$id_user=$id_user,$link='');
					// $simpan_notif = $this->db->simple_query("INSERT INTO tbl_notif (notif,notif_type,id_user,link,status) VALUES ('Please, check your email to verify this account','register','$id_user','','0')");
					$cekcek = "1";
					//$simpan_prof_stu = $this->db->simple_query("INSERT INTO tbl_profile_student () VALUES ()");
					$return['email'] = $email;
					// $return['first_name'] = $where['first_name'];
					$return['kata_sandi'] = $where['kata_sandi'];
					$return['id_user'] = $id_user;
					$return['r_key'] = $r_key;
					$return['cekcek'] = $cekcek;
					$return['key'] = 0;										
					return $return;
				}
				else
				{
					return 0;
				}
		}else if(!empty($cek)){
			$last_data = $this->db->query("SELECT * FROM tbl_user WHERE email='$email'")->row_array();
			$cek_status = $last_data['status'];
			$id_user = $last_data['id_user'];
			$email = $last_data['email'];
			$cekcek = "2";
			$r_key = $this->M_login->randomKey();
			if ($cek_status == 0) {
				//key 2 =  email sudah ada
				$return['email'] = $email;				
				$return['id_user'] = $id_user;
				$return['r_key'] = $r_key;
				$return['key'] = 2;
				$return['cekcek'] = $cekcek;
				return $return;
			}else{
				//key 3 email ada dan status bukan nol
				$return['email'] = $email;				
				$return['id_user'] = $id_user;
				$return['r_key'] = $r_key;
				$return['key'] = 3;
				$return['cekcek'] = $cekcek;
				return $return;
			}
		}
		$load = $this->lang->line('failed');
		$return['status'] = false;
		$return['message'] = $load;
		return $return;
	}

	function daftar_tutor($table,$where)
	{
		$email = $where['email'];
		$umur = $where['tanggal_lahir'];
		$skrg = date_diff(date_create($umur), date_create('today'))->y;
		$cek = $this->db->query("SELECT *FROM tbl_user WHERE email='$email'")->row_array();
		
		if (empty($cek)) {
			$id_user = $this->Rumus->gen_id_user();
			$res = $this->db->simple_query("INSERT INTO tbl_user (id_user, user_name, first_name, last_name, email, password, user_birthdate, user_countrycode, user_callnum, user_gender, usertype_id, user_image, user_age) VALUES ('$id_user','$where[nama_lengkap]','$where[first_name]','$where[last_name]','$where[email]','$where[kata_sandi]','$where[tanggal_lahir]','$where[user_countrycode]','$where[no_hp_tutor]','$where[jenis_kelamin]','tutor','empty.jpg',$skrg)");
			if ($res) {
				$return['status'] = true;
				$return['message'] = "succeed";		 	
				$r_key = $this->M_login->randomKey();
				$simpan = $this->db->simple_query("INSERT INTO gen_auth (random_key, id_user) VALUES ('$r_key','$id_user')");
				$simpan_profile_tutor = $this->db->simple_query("INSERT INTO tbl_profile_tutor (tutor_id) VALUES ('$id_user')");

				//NOTIF
				$notif_message = json_encode( array("code" => 42));					
					$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','','0')");

				// $simpan_notif = $this->db->simple_query("INSERT INTO tbl_notif (notif,notif_type,id_user,link,read_status) VALUES ('Successfully registered, please check your email for verification process.','tutor register','$id_user','','0')");
				$return['email'] = $email;
				$return['first_name'] = $where['first_name'];
				$return['kata_sandi'] = $where['kata_sandi'];
				$return['id_user'] = $id_user;
				$return['r_key'] = $r_key;
				$return['key'] = 0;
				return $return;

				

			}
		}else if(!empty($cek)){
			$cek_status = $this->db->query("SELECT status FROM tbl_user WHERE email='$email'")->row_array()['status'];
			if ($cek_status == 0) {
				//key 2 =  email sudah ada
				$return['key'] = 2;
				return $return;
			}else{
				//key 3 email ada dan status bukan nol
				$return['key'] = 3;
				return $return;
			}
		}

		$return['status'] = false;
		$return['message'] = "failed";
		return $return;
	}

	function getUser($unix = null){
		if ($unix == null) {
			$res = $this->db->query("SELECT *FROM tbl_user")->result_array();
			return $res;
		}else{
			$res = $this->db->query("SELECT *FROM tbl_user WHERE id_user='$unix'")->row_array();
			return $res;
		}
	}

	function getAuth($r_key = null){
		if ($r_key == null) {
			$res = $this->db->query("SELECT *FROM gen_auth")->result_array();
			return $res;
		}else{
			$res = $this->db->query("SELECT *FROM gen_auth WHERE random_key='$r_key'")->row_array();
			return $res;
		}
	}

	function delete_session($table,$where){
		//id yang sekarang lagi login(session)
		$id_user = $this->session->userdata('id_user');
		$del = $this->db->simple_query("DELETE FROM temp_session WHERE id_user='$id_user'");
		if ($del) {
			return 1;
		}else{
			return $del;
		}
	}
	

}