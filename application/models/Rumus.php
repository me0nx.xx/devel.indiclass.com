<?php 
/*require APPPATH."/libraries/jwt/JWT.php";
require APPPATH."/libraries/jwt/BeforeValidException.php";
require APPPATH."/libraries/jwt/ExpiredException.php";
require APPPATH."/libraries/jwt/SignatureInvalidException.php";*/

class Rumus extends CI_Model{

	function RandomString($length = 7) {
		$randstr = '';
		srand((double) microtime(TRUE) * 1000000);
    //our array add all letters and numbers if you wish
		$chars = array(
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'p',
			'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5',
			'6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 
			'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

		for ($rand = 0; $rand <= $length; $rand++) {
			$random = rand(0, count($chars) - 1);
			$randstr .= $chars[$random];
		}
		return $randstr;
	}
	function limit_words($string, $word_limit){
	    $words = explode(" ",$string);
	    return implode(" ",array_splice($words,0,$word_limit));
	}

	function RandomUnixCode($length = 3) {
		$randstr = '';
		srand((double) microtime(TRUE) * 1000000);
    //our array add all letters and numbers if you wish
		$chars = array(
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9');

		for ($rand = 0; $rand < $length; $rand++) {
			$random = rand(0, count($chars) - 1);
			while($rand == 0 && $random == 0){
				$random = rand(0, count($chars) - 1);
			}
			$randstr .= $chars[$random];
		}
		return (int)$randstr;
	}
	public function get_new_request_id($table='private')
	{
		if($table == 'program'){
			$d = $this->db->query("SELECT * FROM tbl_request_program ORDER BY created_at DESC limit 0,1")->row_array();
			if(!empty($d)){
				$d = $d['request_id'];
				$latest = substr($d, 1, strlen($d));
				$latest+= 1;
			}else{
				$latest = 1;
			}
			return '6'.$latest;
		}
		else if($table == 'private'){
			$d = $this->db->query("SELECT * FROM tbl_request ORDER BY datetime DESC limit 0,1")->row_array();
			if(!empty($d)){
				$d = $d['request_id'];
				$latest = substr($d, 1, strlen($d));
				$latest+= 1;
			}else{
				$latest = 1;
			}
			return '7'.$latest;
		}else if($table == 'group'){
			$d = $this->db->query("SELECT * FROM tbl_request_grup ORDER BY datetime DESC limit 0,1")->row_array();
			if(!empty($d)){
				$d = $d['request_id'];
				$latest = substr($d, 1, strlen($d));
				$latest+= 1;
			}else{
				$latest = 1;
			}
			return '8'.$latest;
		}else if($table == 'multicast'){
			$d = $this->db->query("SELECT * FROM tbl_request_multicast ORDER BY created_at DESC limit 0,1")->row_array();
			if(!empty($d)){
				$d = $d['request_id'];
				$latest = substr($d, 1, strlen($d));
				$latest+= 1;
			}else{
				$latest = 1;
			}
			return '9'.$latest;
		}
	}
	public function ftp_send($value='')
	{
		// set up basic connection
		$conn_id = ftp_connect('103.30.245.254');

		// login with username and password
		$login_result = ftp_login($conn_id, 'root', 'AdminCl3v4'); 

		// check connection
		if ((!$conn_id) || (!$login_result)) { 
		    echo "FTP connection has failed!";
		    // echo "Attempted to connect to $ftp_server for user $ftp_user_name"; 
		    exit; 
		} else {
		    echo "Connected to sto, for user root";
		}

		// upload the file
		// $upload = ftp_put($conn_id, '/home/uploaded/32.jpg', file_get_contents('aset/img/user/32.jpg'), FTP_BINARY); 

		// check upload status
		// if (!$upload) { 
		//     echo "FTP upload has failed!";
		// } else {
		//     echo "Uploaded $source_file to sto as $destination_file";
		// }

		// close the FTP stream 
		ftp_close($conn_id); 
	}
	public function tokenRefresher($value='')
	{
		$privateKey = "-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC9AuZYmkgLOCAV
Pxay9FjkqZfTX8Xd1n2vnI/tHV24hJniMzxcJ8zgka5+uWbMa7x8o+sYPqSianwz
0nnAOTzKLfJLa0OJdp/LUn5XJS1sC4viMY3WEKLcV39blgb9e28bjUkENiCJnlVb
h3W70sS0kwDEm5/cYMrv5vriEzu253tqHtct3y1oYO/jnW9yJW5+JTkwkT6fmiy9
opoBiWTkjp5EI3PM8nDe4LrFlNSpr8uOpk+51FRQ/sc8YAeZZ97BrVl8PZIGpCzY
B4naRCnnFKkJ+EuBlf+KWUVY/LTpKyfMQIkWYuh9L2Ofwn1x0Vi3TGRVCYlbk6SO
uK0KhpyXAgMBAAECggEAOaM1vRUnHQy7c98uO9oZdXlmDBYrj4+F+lRi62rGFquR
BZKcOHoGlwC11n0RJQtBijyuR1FrAQA56c+oQv7xU7IZLfiCutuKtQTt9AMpS8Zl
nM+BsiKWl1yzQKmKbigC5ML73iXnXDAFVYkEVQdb5rjhRhMy95AosmmcXe6Bii+c
nSHTC/rJUQ26KsScHtGfMvLvd4JCKy8J5zoRlhJ1SqXui8sNL4iEwwQG6bBHHh7Z
f2rW+MsFZAba6+RpzqV0P6v2o96nwjs///fPy+UXGeNSpt0FdA/OoECrOUPxm2Si
5dQJDv+opwbazWwO2SGafFPVf2DfS+qQEFtbcBgrEQKBgQDfcZfYRn8bgPVCiD2M
fUaMyS24IY0xhhxFlEXhEjTkVqtLM9xYA/wbGNrI9TRCJbE27lqPtcFuvH4aih0F
Ry79OksOcBJ9V697nJj9wsXeKjIxMaV7Eyfg51VFNG0cPHxutVCWvv48R15GHgeT
z9e7A0GV4jV50FJ7XDourqTEzwKBgQDYjPws+4Oc4KGGo9QcCx/M72pS+hAVYbj3
iqoRt2pjnbEyrzYzO15e84ACHIclAEyfIGnL4d9MHtFPzeaNnLGjc64YrqrMHlKL
m4grYgv1FMQTq+dqFN/2cIDp60Lrcib9GkgNdXlM9kZTSm7OqoNmrJHzNZtEX50v
dlDKomQtuQKBgQCkabJAescGluJg/VzEpl2mNFYG2fFJ0pO1AwBUN5BhwAA0yslE
nlIAIk7CdeLpFVELyWErw6K8d8gWgTAPdSx3OlCGJCMGxeGjBDxy5MGx0ryyBATu
xc6fJ7lPvd7XWw54a8QL2Ce8dQtCbtBGjDYCLsgwoI2Wv5ohmWBafzlQcwKBgQCK
Px9y3DL+wQ19tjNgn91yaNoZFRmVfOin5/eBDfY2mGKat+7DMECkH5H2Rx4kChfg
csH37kAvCXGZIBrzBzkE34IiscfKPgV2qRl6kMKPeO1gnZtERwVgABzcuVvkkeDA
LroXFYsWDheBbwBkocj3kG4wtWVldiEUPtV/N8L6yQKBgD3YpFXFQ3uTKjvXBsjD
DhrqCQoZijzPKfjndI75XeM069MSLhkS9Twe1Vd+k1YspBLUE8EvCMWwHEmRX580
1MBAkRtRVXZgGHsCg5CNEW1oq48LwAxGMpBAhhB7fX8ZMZy97F4WtgBK7SN/Mocp
uTaDxAmurQJA5HQpjOqsTx6k
-----END PRIVATE KEY-----";
		$token = array(
		    "iss" => "cm-storage@classmiles-web.iam.gserviceaccount.com",
		    "scope"=> "https://www.googleapis.com/auth/devstorage.full_control",
		    "aud" => "https://www.googleapis.com/oauth2/v4/token",
		    "exp" => time()+3600,
		    "iat" => time(),
		);

		$jwt = JWT::encode($token, $privateKey, 'RS256');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/oauth2/v4/token");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
		// curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('grant_type' => 'urn:ietf:params:oauth:grant-type:jwt-bearer', 'assertion' => $jwt)));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000); 

		$curl_rets = curl_exec($ch);

		$new_token = json_decode($curl_rets, TRUE);
		if(isset($new_token['error'])){
			return "-2";
		}else{
			$file = fopen('gstore_key/gstore_act.key',"wa+");
			fwrite($file,$new_token['access_token']);
			fclose($file);
			return 1;
		}
		// $file = fopen($savePath,"wa+");
	}
	public function sendToCDN($type='user', $target_filename='', $resource=null, $file_path=null)
	{
		// type = user, channel, class_icon, buktipembayaran
		$myfile = fopen('gstore_key/gstore_act.key', "r");
		$gstore_act = fread($myfile,filesize("gstore_key/gstore_act.key"));
		fclose($myfile);

		if($type == 'user'){
			if($resource != null && $file_path == null){
				$len=strlen(bin2hex($resource))/2;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/upload/storage/v1/b/cm-bucket/o?uploadType=media&predefinedAcl=publicRead&name=usercontent%2fuser%2f".$target_filename);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/jpg", "Content-Length : ".$len));
				curl_setopt($ch, CURLOPT_POST,1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,$resource);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000); 

				$curl_rets = curl_exec($ch);
				$curl_rets = json_decode($curl_rets, TRUE);
				if(isset($curl_rets['error']) && $curl_rets['error']['code'] == 401){
					$gstore_act = $this->Gstore->tokenRefresher();
					curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/jpg", "Content-Length : ".$len));
					$curl_rets = curl_exec($ch);
				}
				return $curl_rets;
			}else{
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/upload/storage/v1/b/cm-bucket/o?uploadType=media&predefinedAcl=publicRead&name=usercontent%2fuser%2f".$target_filename);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/jpg", "Content-Length : ".filesize($file_path)));
				curl_setopt($ch, CURLOPT_POST,1);
				curl_setopt($ch, CURLOPT_POSTFIELDS,file_get_contents($file_path));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000); 

				$curl_rets = curl_exec($ch);
				$curl_rets = json_decode($curl_rets, TRUE);
				if(isset($curl_rets['error']) && $curl_rets['error']['code'] == 401){
					$gstore_act = $this->Gstore->tokenRefresher();
					curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/jpg", "Content-Length : ".filesize($file_path)));
					$curl_rets = curl_exec($ch);
				}
				unlink($file_path);
				return $curl_rets;
			}
		}else if($type == 'class_icon'){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/upload/storage/v1/b/cm-bucket/o?uploadType=media&predefinedAcl=publicRead&name=static%2fclass_icon%2f".$target_filename);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/png", "Content-Length : ".filesize($file_path)));
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,file_get_contents($file_path));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000); 

			$curl_rets = curl_exec($ch);
			$curl_rets = json_decode($curl_rets, TRUE);
			if(isset($curl_rets['error']) && $curl_rets['error']['code'] == 401){
				$gstore_act = $this->Gstore->tokenRefresher();
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/png", "Content-Length : ".filesize($file_path)));
				$curl_rets = curl_exec($ch);
			}
			unlink($file_path);
			return $curl_rets;
		}else if($type == 'channel'){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/upload/storage/v1/b/cm-bucket/o?uploadType=media&predefinedAcl=publicRead&name=usercontent%2fchannel%2f".$target_filename);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/png", "Content-Length : ".filesize($file_path)));
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,file_get_contents($file_path));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000); 

			$curl_rets = curl_exec($ch);
			$curl_rets = json_decode($curl_rets, TRUE);
			if(isset($curl_rets['error']) && $curl_rets['error']['code'] == 401){
				$gstore_act = $this->Gstore->tokenRefresher();
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/png", "Content-Length : ".filesize($file_path)));
				$curl_rets = curl_exec($ch);
			}
			unlink($file_path);
			return $curl_rets;
		}else if($type == 'banksoal'){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/upload/storage/v1/b/cm-bucket/o?uploadType=media&predefinedAcl=publicRead&name=usercontent%2fbanksoal%2f".$target_filename);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/png", "Content-Length : ".filesize($file_path)));
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,file_get_contents($file_path));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000); 

			$curl_rets = curl_exec($ch);
			$curl_rets = json_decode($curl_rets, TRUE);
			if(isset($curl_rets['error']) && $curl_rets['error']['code'] == 401){
				$gstore_act = $this->Gstore->tokenRefresher();
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/png", "Content-Length : ".filesize($file_path)));
				$curl_rets = curl_exec($ch);
			}
			unlink($file_path);
			return $curl_rets;
		}else if($type == 'banner'){
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/upload/storage/v1/b/cm-bucket/o?uploadType=media&predefinedAcl=publicRead&name=usercontent%2fbanner%2f".$target_filename);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/png", "Content-Length : ".filesize($file_path)));
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,file_get_contents($file_path));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000); 

			$curl_rets = curl_exec($ch);
			$curl_rets = json_decode($curl_rets, TRUE);
			if(isset($curl_rets['error']) && $curl_rets['error']['code'] == 401){
				$gstore_act = $this->Gstore->tokenRefresher();
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/png", "Content-Length : ".filesize($file_path)));
				$curl_rets = curl_exec($ch);
			}
			unlink($file_path);
			return $curl_rets;
		}

		// SSH Host 
	    /*$ssh_host = $url;
	    if($ssh_host == CDN2_BASEURL){
	    	$ssh_port = 22; 
		    $ssh_auth_user = 'root'; 
		    $ssh_auth_pass = 'AdminCl3v4';
	    }else{
		    $ssh_port = 24; 
		    $ssh_server_fp = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'; 
		    $ssh_auth_user = 'hermi'; 
		    $ssh_auth_pub = '/etc/ssl/cm_ssl/GCP_Public_Key_SSH_05072017.pub'; 
		    $ssh_auth_priv = '/etc/ssl/cm_ssl/GCP_Private_Key_SSH_05072017_converted.ppk'; 
		    $ssh_methods = array(
		    'hostkey'=>'ssh-rsa,ssh-dss',
		//    'kex' => 'diffie-hellman-group-exchange-sha256',
		    'client_to_server' => array(
		        'crypt' => 'aes256-ctr,aes192-ctr,aes128-ctr,aes256-cbc,aes192-cbc,aes128-cbc,3des-cbc,blowfish-cbc',
		        'comp' => 'none'),
		    'server_to_client' => array(
		        'crypt' => 'aes256-ctr,aes192-ctr,aes128-ctr,aes256-cbc,aes192-cbc,aes128-cbc,3des-cbc,blowfish-cbc',
		        'comp' => 'none'));
		    $ssh_auth_pass = 'T0l4kB4l4'; 
		}
		$connection;
	    if (!($connection = ssh2_connect($ssh_host, $ssh_port))) { 
            return json_encode(array('status' => false, 'message' => 'Cannot connect to server')); 
        }*/

        //AUTH THROUGH PUBKEY
        /*$fingerprint = ssh2_fingerprint($this->connection, SSH2_FINGERPRINT_MD5 | SSH2_FINGERPRINT_HEX); 
        if (strcmp($this->ssh_server_fp, $fingerprint) !== 0) { 
            die('Unable to verify server identity!'); 
        } */
        /*if (!ssh2_auth_pubkey_file($this->connection, $this->ssh_auth_user, $this->ssh_auth_pub, $this->ssh_auth_priv, $this->ssh_auth_pass)) { 
            die('Autentication rejected by server'); 
        }*/


        //AUTH THROUGH PLAIN PASSWORD
        /*if (!ssh2_auth_password($connection, $ssh_auth_user, $ssh_auth_pass)) {
        	return json_encode(array('status' => false, 'message' => 'Autentication rejected by server')); 
		}
		if($resource != null && $file_path == null){
			file_put_contents("aset/_temp_images/".$target_filename, file_get_contents($resource));
			if(!ssh2_scp_send($connection, "aset/_temp_images/".$target_filename, $target_dir."/".$target_filename, 0775)){
				ssh2_exec($connection, 'exit');
				return json_encode(array('status' => false, 'message' => 'File transmition failure')); 
			}
			ssh2_exec($connection, 'exit');
			unlink('aset/_temp_images/'.$target_filename);
		}else{
			if(!ssh2_scp_send($connection, $file_path, $target_dir."/".$target_filename, 0775)){
				ssh2_exec($connection, 'exit');
				return json_encode(array('status' => false, 'message' => 'File transmition failure')); 
			}
			ssh2_exec($connection, 'exit');
			unlink($file_path);
		}*/
		

		return json_encode(array('status' => true, 'message' => 'File transmition successful')); 

		/*if (!($stream = ssh2_exec($this->connection, 'ls -l'))) { 
            throw new Exception('SSH command failed'); 
        } 
        stream_set_blocking($stream, true); 
        $data = ""; 
        while ($buf = fread($stream, 4096)) {
            $data .= $buf; 
        } 
        fclose($stream); 
        print_r($data);*/
		
	}
	public function classDrive_listFolders($tutor_id='')
	{
		// LIST FOLDER YANG DI MILIKI TUTOR , JIKA TUTOR BARU PERTAMA AKSES CLASSDRIVE MAKA AUTO CREATE FOLDER BUAT DIA
		// type = user, channel, class_icon, buktipembayaran
		$myfile = fopen('gstore_key/gstore_act.key', "r");
		$gstore_act = fread($myfile,filesize("gstore_key/gstore_act.key"));
		fclose($myfile);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/storage/v1/b/cm-bucket/o?delimiter=%2f&prefix=ClassDrive%2findiclass%2f".$tutor_id."%2f");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act));
		// curl_setopt($ch, CURLOPT_POST,1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS,$resource);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000);

		$curl_rets = curl_exec($ch);
		$curl_rets = json_decode($curl_rets, TRUE);
		if(isset($curl_rets['error']) && $curl_rets['error']['code'] == 401){
			$gstore_act = $this->Gstore->tokenRefresher();
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act));
			$curl_rets = curl_exec($ch);
			$curl_rets = json_decode($curl_rets, TRUE);
		}
		$list_folders = array();
		if(!isset($curl_rets['items'])){
			$cm = curl_init();
			curl_setopt($cm, CURLOPT_URL, "https://www.googleapis.com/upload/storage/v1/b/cm-bucket/o?uploadType=media&name=ClassDrive%2findiclass%2f".$tutor_id."%2f");
			curl_setopt($cm, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: application/x-www-form-urlencoded;charset=UTF-8", "Content-Length: 0"));
			curl_setopt($cm, CURLOPT_POST,1);
			// curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query(array("storageClass" => "MULTI_REGIONAL")));
			curl_setopt($cm, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($cm, CURLOPT_TIMEOUT_MS,10000);

			$curl_rets_cm = curl_exec($cm);
			// $curl_rets_cm = json_decode($curl_rets_cm, TRUE);

			return array();
		}
		if(isset($curl_rets['prefixes'])){
			$list_folders = $curl_rets['prefixes'];
			$delimiter = strpos($list_folders[0], "/".$tutor_id."/");
			$delimiter += strlen($tutor_id."/") + 1;
			foreach ($list_folders as $key => $value) {
				$list_folders[$key] = substr($value, $delimiter);
			}
		}
		return $list_folders;

	}
	public function classDrive_listImages($tutor_id='', $folder='')
	{
		$myfile = fopen('gstore_key/gstore_act.key', "r");
		$gstore_act = fread($myfile,filesize("gstore_key/gstore_act.key"));
		fclose($myfile);

		$folder_persen2f = str_replace(' ', '%20', $folder);
		$folder_persen2f = str_replace('/', '%2f', $folder_persen2f);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/storage/v1/b/cm-bucket/o?delimiter=%2f&prefix=ClassDrive%2findiclass%2f".$tutor_id."%2f".$folder_persen2f);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act));
		// curl_setopt($ch, CURLOPT_POST,1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS,$resource);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000);

		$curl_rets = curl_exec($ch);
		$curl_rets = json_decode($curl_rets, TRUE);
		if(isset($curl_rets['error']) && $curl_rets['error']['code'] == 401){
			$gstore_act = $this->Gstore->tokenRefresher();
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act));
			$curl_rets = curl_exec($ch);
			$curl_rets = json_decode($curl_rets, TRUE);
		}

		$list_images = array();
		if(isset($curl_rets['items'])){
			$delimiter = strpos($curl_rets['items'][0]['name'], "/".$tutor_id."/".$folder);
			$delimiter += strlen($tutor_id."/".$folder) + 1;

			foreach ($curl_rets['items'] as $key => $value) {
				if($value['size'] != "0"){
					array_push($list_images, substr($value['name'], $delimiter));
				}
			}
		}
		return $list_images;
	}
	public function classDrive_createFolder($tutor_id='',$new_folder_name='')
	{
		$myfile = fopen('gstore_key/gstore_act.key', "r");
		$gstore_act = fread($myfile,filesize("gstore_key/gstore_act.key"));
		fclose($myfile);

		$folder_persen2f = str_replace(' ', '%20', $new_folder_name);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/upload/storage/v1/b/cm-bucket/o?uploadType=media&name=ClassDrive%2findiclass%2f".$tutor_id."%2f".$folder_persen2f."%2f");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: application/x-www-form-urlencoded;charset=UTF-8", "Content-Length: 0"));
		curl_setopt($ch, CURLOPT_POST,1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS,$resource);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000);

		$curl_rets = curl_exec($ch);
		$curl_rets = json_decode($curl_rets, TRUE);
		if(isset($curl_rets['error']) && $curl_rets['error']['code'] == 401){
			$gstore_act = $this->Gstore->tokenRefresher();
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: application/x-www-form-urlencoded;charset=UTF-8", "Content-Length: 0"));
			$curl_rets = curl_exec($ch);
			$curl_rets = json_decode($curl_rets, TRUE);
		}
		return json_encode(array("status" => true, "message" => "ClassDrive folder creating successful."));
	}
	public function classDrive_deleteFolder($tutor_id='',$delete_folder_name='')
	{
		$myfile = fopen('gstore_key/gstore_act.key', "r");
		$gstore_act = fread($myfile,filesize("gstore_key/gstore_act.key"));
		fclose($myfile);

		$all_content_inside = $this->classDrive_listImages($tutor_id,$delete_folder_name);

		$folder_persen2f = str_replace(' ', '%20', $delete_folder_name);
		$folder_persen2f = str_replace('/', '%2f', $folder_persen2f);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/storage/v1/b/cm-bucket/o/ClassDrive%2findiclass%2f".$tutor_id."%2f".$folder_persen2f);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000);

		$curl_rets = curl_exec($ch);
		// $curl_rets = json_decode($curl_rets, TRUE);
		if(isset($curl_rets['error']) && $curl_rets['error']['code'] == 401){
			$gstore_act = $this->Gstore->tokenRefresher();
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act));
			$curl_rets = curl_exec($ch);
			$curl_rets = json_decode($curl_rets, TRUE);
		}
		foreach ($all_content_inside as $key => $value) {
			$value = str_replace(' ', '%20', $value);
			curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/storage/v1/b/cm-bucket/o/ClassDrive%2findiclass%2f".$tutor_id."%2f".$folder_persen2f.$value);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act));
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000);

			$curl_rets = curl_exec($ch);
		}
		return $curl_rets;
	}
	public function classDrive_uploadImage($tutor_id='', $folder='', $target_filename='', $resource='', $file_path='')
	{
		$myfile = fopen('gstore_key/gstore_act.key', "r");
		$gstore_act = fread($myfile,filesize("gstore_key/gstore_act.key"));
		fclose($myfile);

		$folder_persen2f = str_replace(' ', '%20', $folder);
		$folder_persen2f = str_replace('/', '%2f', $folder_persen2f);

		$target_filename = str_replace(' ', '%20', $target_filename);

		if($resource != null && $file_path == null){
			$len=strlen(bin2hex($resource))/2;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/upload/storage/v1/b/cm-bucket/o?uploadType=media&predefinedAcl=publicRead&name=ClassDrive%2findiclass%2f".$tutor_id."%2f".$folder_persen2f.$target_filename);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/jpg", "Content-Length : ".$len));
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$resource);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000); 

			$curl_rets = curl_exec($ch);
			$curl_rets = json_decode($curl_rets, TRUE);
			if(isset($curl_rets['error']) && $curl_rets['error']['code'] == 401){
				$gstore_act = $this->Gstore->tokenRefresher();
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/jpg", "Content-Length : ".$len));
				$curl_rets = curl_exec($ch);
			}
			return $curl_rets;
		}else{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/upload/storage/v1/b/cm-bucket/o?uploadType=media&predefinedAcl=publicRead&name=ClassDrive%2findiclass%2f".$tutor_id."%2f".$folder_persen2f.$target_filename);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/jpg", "Content-Length : ".filesize($file_path)));
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,file_get_contents($file_path));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000); 

			$curl_rets = curl_exec($ch);
			// $curl_rets = json_decode($curl_rets, TRUE);
			if(isset($curl_rets['error']) && $curl_rets['error']['code'] == 401){
				$gstore_act = $this->Gstore->tokenRefresher();
				curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act, "Content-Type: image/jpg", "Content-Length : ".filesize($file_path)));
				$curl_rets = curl_exec($ch);
			}
			// unlink($file_path);
			return $curl_rets;
		}

		return json_encode(array("status" => true, "message" => "ClassDrive uploading successful."));
	}
	public function classDrive_deleteImage($tutor_id='',$folder_name='',$delete_image_filename='')
	{
		$myfile = fopen('gstore_key/gstore_act.key', "r");
		$gstore_act = fread($myfile,filesize("gstore_key/gstore_act.key"));
		fclose($myfile);

		$folder_persen2f = str_replace(' ', '%20', $folder_name);
		$folder_persen2f = str_replace('/', '%2f', $folder_persen2f);

		$delete_image_filename = str_replace(' ', '%20', $delete_image_filename);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/storage/v1/b/cm-bucket/o/ClassDrive%2findiclass%2f".$tutor_id."%2f".$folder_persen2f.$delete_image_filename);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000);

		$curl_rets = curl_exec($ch);
		// $curl_rets = json_decode($curl_rets, TRUE);
		if(isset($curl_rets['error']) && $curl_rets['error']['code'] == 401){
			$gstore_act = $this->Gstore->tokenRefresher();
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer ".$gstore_act));
			$curl_rets = curl_exec($ch);
			$curl_rets = json_decode($curl_rets, TRUE);
		}
		return $curl_rets;
	}
	
	public function getGMTOffset($type='minute', $tm='')
	{
		if($tm == ''){
				$tm = date('O');
		}
		$hour = substr($tm, 1,2);
		$minute = substr($tm, 3,2);
		if($type=='minute'){
			$total = (intval(($hour)*60)+intval($minute));
			return intval(substr($tm,0,1).$total);
		}
	}
	public function getTrxID($value='')
	{
		$prefix = date('ymd');
		$last_digit = $this->db->query("SELECT RIGHT(trx_id,6) as last_digit FROM log_class_transaction WHERE LEFT(trx_id, 6) ='$prefix' ORDER BY created_at DESC limit 1 ")->row_array();

		if(empty($last_digit)){
			$last_digit = 0;
		}else{
			$last_digit = $last_digit['last_digit'];
		}
		$new_digit = intval($last_digit)+1;
		$result = $prefix.sprintf('%06d',$new_digit);
		return $result;
	}
	public function getTrxIDPurchase($value='')
	{
		$prefix = date('ymd');
		$last_digit = $this->db->query("SELECT RIGHT(trx_id,6) as last_digit FROM tbl_purchaseclass WHERE LEFT(trx_id, 6) ='$prefix' ORDER BY created_at DESC limit 1 ")->row_array();

		if(empty($last_digit)){
			$last_digit = 0;
		}else{
			$last_digit = $last_digit['last_digit'];
		}
		$new_digit = intval($last_digit)+1;
		$result = $prefix.sprintf('%06d',$new_digit);
		return $result;
	}
	public function getChnPointTrxID($value='')
	{
		$prefix = date('ymd');
		$last_digit = $this->db->query("SELECT RIGHT(ptrx_id,6) as last_digit FROM log_channel_point_transaction WHERE LEFT(ptrx_id, 6) ='$prefix' ORDER BY created_at DESC limit 1 ")->row_array();

		if(empty($last_digit)){
			$last_digit = 0;
		}else{
			$last_digit = $last_digit['last_digit'];
		}
		$new_digit = intval($last_digit)+1;
		$result = $prefix.sprintf('%06d',$new_digit);
		return $result;
	}
	public function getLogChnTrxID($value='')
	{
		$prefix = date('ymd');
		$last_digit = $this->db->query("SELECT RIGHT(plogc_id,6) as last_digit FROM log_channel WHERE LEFT(plogc_id, 6) ='$prefix' ORDER BY created_at DESC limit 1 ")->row_array();

		if(empty($last_digit)){
			$last_digit = 0;
		}else{
			$last_digit = $last_digit['last_digit'];
		}
		$new_digit = intval($last_digit)+1;
		$result = $prefix.sprintf('%06d',$new_digit);
		return $result;
	}
	public function getMyAvailabilityTime($id_user='',$start_date='', $end_date='', $user_utc='')
	{
		// $user_utc = $this->get('user_utc');
		// $duration = $this->get('duration') * 60;

		/*$user_device = $this->post('user_device') != '' ? $this->post('user_device') : "mobile";
		if($user_device == 'mobile'){
			$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		}*/

		$server_utc = $this->Rumus->getGMTOffset();
		$interval = $user_utc - $server_utc;
		$start_date_asked = DateTime::createFromFormat('Y-m-d H:i:s', $start_date." 00:00:00" );
		$start_date_asked->modify("-".$interval ." minutes");
		$start_date_asked = $start_date_asked->format('Y-m-d H:i:s');

		$end_date_asked = DateTime::createFromFormat('Y-m-d H:i:s', $end_date." 23:59:59" );
		$end_date_asked->modify("-".$interval ." minutes");
		$end_date_asked = $end_date_asked->format('Y-m-d H:i:s');



		$res = $this->db->query("SELECT DATE_ADD(start_time, INTERVAL $interval MINUTE) as start_time, DATE_ADD(finish_time, INTERVAL $interval MINUTE) as finish_time FROM tbl_class WHERE tutor_id=$id_user AND start_time>='$start_date_asked' AND finish_time<='$end_date_asked' ")->result_array();
		return $res;
	}
	public function gen_id_user($value='000')
	{
		$new_id = "";
		$last_id = $this->db->query("SELECT id_user FROM tbl_user ORDER BY id_user DESC limit 1")->row_array();
		if(empty($last_id)){
			$new_id = "1".$value;
		}else{
			$last_id = $last_id['id_user'];
			$new_id = strlen($last_id) > 3 ? intval(substr($last_id, 0, strlen($last_id)-3)+1).$value : intval($last_id+1).$value;
		}
		return $new_id;
	}
	public function gen_id_school($value='')
	{
		$new_id = "";
		$last_id = $this->db->query("SELECT id FROM master_school_temp ORDER BY id DESC limit 1")->row_array();
		if(empty($last_id)){
			$new_id = "1".$value;
		}else{
			$last_id = $last_id['id'];
			$new_id = strlen($last_id) > 3 ? intval(substr($last_id, 0, strlen($last_id)-3)+1).$value : intval($last_id+1).$value;
		}
		return "SI".$new_id;
	}
	public function get_subdesc($subject_id='',$id_user='',$start_weekdate='')
	{
		$data = $this->db->query("SELECT description FROM tbl_subdesc WHERE subject_id='{$subject_id}' AND id_user='{$id_user}' AND start_weekdate='{$start_weekdate}'")->row_array();
		if(!empty($data)){
			return $data['description'];
		}
		return "";
	}
	
	public function get_subjectbyid($subject_id='')
	{
		$data = $this->db->query("SELECT subject_id, subject_name, indonesia, english FROM master_subject WHERE subject_id='{$subject_id}'")->result_array();

		return $data;
	}

	function getSubject(){
		$id_user = $this->session->userdata('id_user');

		$getAllSubject = $this->db->query("SELECT tb.subject_id, ms.subject_name, ms.indonesia, ms.english FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id=ms.subject_id  WHERE tb.id_user='{$id_user}'")->result_array();
		$umum = $this->getUmum();
		$tots = 0;
		$new_arr = array();
		foreach ($getAllSubject as $key => $value) {

			if ($this->session->userdata('lang') == 'english') {
				$new_arr[$tots]['subject_id'] = $value['subject_id'];
				$new_arr[$tots++]['english'] = $value['english'];
			}
			if ($this->session->userdata('lang') == 'indonesia')
			{
				$new_arr[$tots]['subject_id'] = $value['subject_id'];
				$new_arr[$tots++]['indonesia'] = $value['indonesia'];
			}
			
		}

		foreach ($umum as $key => $value) {
			if ($this->session->userdata('lang') == 'english') {
				$new_arr[$tots]['subject_id'] = $value['subject_id'];
				$new_arr[$tots++]['english'] = $value['english'];
			}
			if ($this->session->userdata('lang') == 'indonesia')
			{
				$new_arr[$tots]['subject_id'] = $value['subject_id'];
				$new_arr[$tots++]['indonesia'] = $value['indonesia'];
			}
		}

		return $new_arr;
	}
	public function subdMe($tutor_id='',$start_date='')
	{
		$data = $this->db->query("SELECT tb.*, ms.subject_name, ms.indonesia, ms.english, mj.jenjang_name, mj.jenjang_level FROM tbl_booking as tb INNER JOIN (master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id)  ON tb.subject_id=ms.subject_id WHERE id_user='{$tutor_id}'")->result_array();
		$result = array();
		foreach ($data as $key => $value) {
			$sid = $value['subject_id'];
			$subd = $this->db->query("SELECT * FROM log_subdiscussion WHERE tutor_id='{$tutor_id}' AND subject_id='{$sid}' AND fdate_week='{$start_date}'")->row_array();
			$result[$key]['subject_id'] = $sid;
			$result[$key]['subject_name'] = $this->lang->line('subject_'.$sid)." - ".$value['jenjang_level']." ".$value['jenjang_name'];
			
			$result[$key]['date'] = $start_date;
			if(!empty($subd)){
				$result[$key]['isi'] = $subd['subdiscussion'];	
			}else{
				$result[$key]['isi'] = "";	
			}
		}
		return $result;
	}
	public function lavtimeMe($tutor_id='',$start_date='',$stop_date='')
	{
		$result = $this->db->query("SELECT lavtime_id, date, start_time, stop_time, setted FROM log_avtime WHERE tutor_id='{$tutor_id}' AND date>='{$start_date}' AND date<='{$stop_date}'")->result_array();
		if(empty($result)){
			/*for ($i=0; $i < 7; $i++) { 
				$result[$i]['start_time'] = 0;
				$result[$i]['stop_time'] = 15;
				$result[$i]['setted'] = 0;
			}*/
		}else{
			foreach ($result as $key => $value) {
				$result[$key]['start_time'] = sprintf('%02d',($value['start_time']/3600)).":".sprintf('%02d',(($value['start_time']%3600)/60));
				$result[$key]['stop_time'] = sprintf('%02d',($value['stop_time']/3600)).":".sprintf('%02d',(($value['stop_time']%3600)/60));
			}
		}
		return $result;
	}
	public function getUmum()
	{
		$umum = $this->db->query("SELECT * FROM master_subject WHERE jenjang_id=15")->result_array();
		return $umum;
	}

	public function create_notif($notif='',$notif_mobile='',$notif_type='',$id_user='',$link='')
	{
		// $this->db->query("CALL CREATE_NOTIF('{$notif}','{$notif_mobile}','{$notif_type}','{$id_user}','{$link}');");
		return 1;
	}
	public function update_notif($notif_id='',$id_user='')
	{
		$this->db->simple_query("UPDATE tbl_notif SET status=1 WHERE notif_id='{$notif_id}' AND id_user='{$id_user}'");
		return 1;
	}
	public function gen_order_id()
	{
		$a = str_replace('0.','',microtime());
		$a = str_replace(' ', '', $a);
		$tm = intval($a);

		$oid = pow($tm, 1/3);
		$oid = str_replace('.', '', $oid);
		if(strlen($oid) > 10){
			$oid = substr($oid, 0,10);
		}
		return $oid;
	}
	public function gen_invoice_id($order_id='')
	{
		$date = date('ymd');
		$prefix = "INV/".$date;
		$last_invoice_id = $this->db->query("SELECT invoice_id FROM tbl_invoice WHERE LEFT(invoice_id, 10) = '$prefix' ORDER BY created_at DESC limit 1 ")->row_array();
		if(!empty($last_invoice_id)){
			$last_invoice_id = substr($last_invoice_id['invoice_id'], 11, 7);
		}else{
			$last_invoice_id = 0;
		}
		$invoice_counter = intval($last_invoice_id)+1;
		$invoice_counter = sprintf('%07d', strval($invoice_counter));
		$invoice_id = $prefix."/".$invoice_counter."/".$order_id;
		return $invoice_id;
	}
	public function my_ended_class()
	{		
        $at_least_one = 0;
  		$id_userr = $this->session->userdata('id_user');
        $now = date('Y-m-d H:i:s');
        // $query_sche = $this->db->query("SELECT tc.class_id, tc.participant, tc.subject_id, tc.name, tc.description, tc.tutor_id, tu.first_name, tu.user_image, tc.start_time, tc.finish_time FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user where finish_time < '$now' && break_pos=0 && break_state = 0 ORDER BY finish_time ASC")->result_array();
        $query_sche = $this->db->query("SELECT tc.class_id, tc.participant, tc.subject_id, tc.name, tc.description, tc.tutor_id, tu.first_name, tu.user_image, tc.start_time, tc.finish_time FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user where finish_time < '$now' && break_pos=0 && break_state = 0 ORDER BY finish_time ASC")->result_array();
        $all_schedule = array();

        foreach ($query_sche as $key => $value) {
            $participant = json_decode($value['participant'],true);
            $in = 0;
            if($participant['visible'] == "followers"){
                $tutor_id = $value['tutor_id'];
                $subject_id = $value['subject_id'];
                $real = $this->db->query("SELECT * FROM tbl_booking WHERE tutor_id='{$tutor_id}' AND id_user='{$id_userr}' AND subject_id='{$subject_id}'")->row_array();
                if(!empty($real)){
                    $in = 1;
                }
            }
            else if($participant['visible'] == "all"){
                $in = 1;
            }
            else if($participant['visible'] == "private"){
                foreach ($participant['participant'] as $p => $vp) {
                    if($vp['id_user'] == $id_userr){
                        $in = 1;
                    }
                }
            }
            if($in == 1){
            	$all_schedule[] = $value;
            }
            
        }
        return $all_schedule;
	}
	public function get_class_info($class_id='')
	{
		$class = $this->db->query("SELECT tc.class_id, tc.participant, tc.subject_id, tc.name, tc.description, tc.tutor_id, tu.first_name, tu.user_image, tc.start_time, tc.finish_time, lc.status as convert_status, lc.type as convert_type FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user LEFT JOIN log_convert as lc ON tc.class_id=lc.class_id where tc.class_id='{$class_id}'")->row_array();
		if(!empty($class)){
			return $class;
		}
		return null;
	}
	public function notif_extra_class($id_tujuan='',$fcm_token='',$student_name='',$subject_id='',$typeclass='')
	{
		if($fcm_token != NULL && substr($fcm_token, 0,6) != 'kosong'){
			$subject = $this->db->query("SELECT * from master_subject where subject_id = '{$subject_id}'")->row_array();
			$json_notif = array("to" => $fcm_token, "data" => array("data" => array("code" => "32","title" => "Classmiles", "text" => "", "payload" => "")));
			$json_notif['data']['data']['payload'][] = array("student_name" => $student_name, "indonesia" => $subject['indonesia'], "english" =>$subject['english']);
			$headers = array(
				'Authorization: key=' . FIREBASE_API_KEY,
				'Content-Type: application/json'
				);
	    	// Open connection
			$ch = curl_init();

	    	// Set the url, number of POST vars, POST data
			curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

	    	// Execute post
			$result = curl_exec($ch);
			curl_close($ch);
			// echo $result;
		}
		if ($typeclass=="private") {
			$link = base_url().'tutor/approval_ondemand';	
		}
		else if($typeclass=="group")
		{
			$link = base_url().'tutor/approval_ondemand_group';
		}
		else
		{
			$link = base_url().'tutor/approval_ondemand';	
		}
		
		$notif = array("code"=> 32, "student_name"=> $student_name, "subject_id" => $subject_id);
		$notif = json_encode($notif);
		$this->db->simple_query("INSERT INTO tbl_notif(notif, id_user, link) VALUES('$notif','$id_tujuan','$link')");

		
	}

}