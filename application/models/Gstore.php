<?php 
require APPPATH."/libraries/jwt/JWT.php";
require APPPATH."/libraries/jwt/BeforeValidException.php";
require APPPATH."/libraries/jwt/ExpiredException.php";
require APPPATH."/libraries/jwt/SignatureInvalidException.php";

use \Firebase\JWT\JWT;
class Gstore extends CI_Model{


	public function tokenRefresher($value='')
	{
		$privateKey = "-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC9AuZYmkgLOCAV
Pxay9FjkqZfTX8Xd1n2vnI/tHV24hJniMzxcJ8zgka5+uWbMa7x8o+sYPqSianwz
0nnAOTzKLfJLa0OJdp/LUn5XJS1sC4viMY3WEKLcV39blgb9e28bjUkENiCJnlVb
h3W70sS0kwDEm5/cYMrv5vriEzu253tqHtct3y1oYO/jnW9yJW5+JTkwkT6fmiy9
opoBiWTkjp5EI3PM8nDe4LrFlNSpr8uOpk+51FRQ/sc8YAeZZ97BrVl8PZIGpCzY
B4naRCnnFKkJ+EuBlf+KWUVY/LTpKyfMQIkWYuh9L2Ofwn1x0Vi3TGRVCYlbk6SO
uK0KhpyXAgMBAAECggEAOaM1vRUnHQy7c98uO9oZdXlmDBYrj4+F+lRi62rGFquR
BZKcOHoGlwC11n0RJQtBijyuR1FrAQA56c+oQv7xU7IZLfiCutuKtQTt9AMpS8Zl
nM+BsiKWl1yzQKmKbigC5ML73iXnXDAFVYkEVQdb5rjhRhMy95AosmmcXe6Bii+c
nSHTC/rJUQ26KsScHtGfMvLvd4JCKy8J5zoRlhJ1SqXui8sNL4iEwwQG6bBHHh7Z
f2rW+MsFZAba6+RpzqV0P6v2o96nwjs///fPy+UXGeNSpt0FdA/OoECrOUPxm2Si
5dQJDv+opwbazWwO2SGafFPVf2DfS+qQEFtbcBgrEQKBgQDfcZfYRn8bgPVCiD2M
fUaMyS24IY0xhhxFlEXhEjTkVqtLM9xYA/wbGNrI9TRCJbE27lqPtcFuvH4aih0F
Ry79OksOcBJ9V697nJj9wsXeKjIxMaV7Eyfg51VFNG0cPHxutVCWvv48R15GHgeT
z9e7A0GV4jV50FJ7XDourqTEzwKBgQDYjPws+4Oc4KGGo9QcCx/M72pS+hAVYbj3
iqoRt2pjnbEyrzYzO15e84ACHIclAEyfIGnL4d9MHtFPzeaNnLGjc64YrqrMHlKL
m4grYgv1FMQTq+dqFN/2cIDp60Lrcib9GkgNdXlM9kZTSm7OqoNmrJHzNZtEX50v
dlDKomQtuQKBgQCkabJAescGluJg/VzEpl2mNFYG2fFJ0pO1AwBUN5BhwAA0yslE
nlIAIk7CdeLpFVELyWErw6K8d8gWgTAPdSx3OlCGJCMGxeGjBDxy5MGx0ryyBATu
xc6fJ7lPvd7XWw54a8QL2Ce8dQtCbtBGjDYCLsgwoI2Wv5ohmWBafzlQcwKBgQCK
Px9y3DL+wQ19tjNgn91yaNoZFRmVfOin5/eBDfY2mGKat+7DMECkH5H2Rx4kChfg
csH37kAvCXGZIBrzBzkE34IiscfKPgV2qRl6kMKPeO1gnZtERwVgABzcuVvkkeDA
LroXFYsWDheBbwBkocj3kG4wtWVldiEUPtV/N8L6yQKBgD3YpFXFQ3uTKjvXBsjD
DhrqCQoZijzPKfjndI75XeM069MSLhkS9Twe1Vd+k1YspBLUE8EvCMWwHEmRX580
1MBAkRtRVXZgGHsCg5CNEW1oq48LwAxGMpBAhhB7fX8ZMZy97F4WtgBK7SN/Mocp
uTaDxAmurQJA5HQpjOqsTx6k
-----END PRIVATE KEY-----";
		$token = array(
		    "iss" => "cm-storage@classmiles-web.iam.gserviceaccount.com",
		    "scope"=> "https://www.googleapis.com/auth/devstorage.full_control",
		    "aud" => "https://www.googleapis.com/oauth2/v4/token",
		    "exp" => time()+3600,
		    "iat" => time(),
		);

		$jwt = JWT::encode($token, $privateKey, 'RS256');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/oauth2/v4/token");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
		// curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('grant_type' => 'urn:ietf:params:oauth:grant-type:jwt-bearer', 'assertion' => $jwt)));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS,10000); 

		$curl_rets = curl_exec($ch);

		$new_token = json_decode($curl_rets, TRUE);
		if(isset($new_token['error'])){
			return "-2";
		}else{
			$file = fopen('gstore_key/gstore_act.key',"wa+");
			fwrite($file,$new_token['access_token']);
			fclose($file);
			return $new_token['access_token'];
		}
		// $file = fopen($savePath,"wa+");
	}
}