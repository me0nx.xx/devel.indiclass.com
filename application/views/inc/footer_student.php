    <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--jQuery [ REQUIRED ]-->
    

    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="../assets/indiclass/js/bootstrap.min.js"></script>

    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="../assets/indiclass/js/nifty.min.js"></script>

    <!--=================================================-->
    <!--Demo script [ DEMONSTRATION ]-->
    <script src="../assets/indiclass/js/demo/nifty-demo.min.js"></script>
    
    <!--Flot Chart [ OPTIONAL ]-->
    <script src="../assets/indiclass/plugins/flot-charts/jquery.flot.min.js"></script>
    <script src="../assets/indiclass/plugins/flot-charts/jquery.flot.resize.min.js"></script>
    <script src="../assets/indiclass/plugins/flot-charts/jquery.flot.tooltip.min.js"></script>

    <!--Sparkline [ OPTIONAL ]-->
    <script src="../assets/indiclass/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!--Specify page [ SAMPLE ]-->
    <script src="../assets/indiclass/js/demo/dashboard.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.js"></script>
</body>
</html>
