
<div class="pm-overview c-overflow">

    <div class="pmo-pic"  style="cursor:pointer;">
        <div class="p-relative" data-target="#modalColorKids" data-toggle="modal">
            <?php
                $id_user_kids = $this->session->userdata('id_user_kids');
                $parent_id  = $this->session->userdata('id_user');
                $iduser = $this->session->userdata('id_user_kids');
                $imguser  = $this->session->userdata('img_user_kids');

                if ($iduser == null) {
                    $imguser = $this->session->userdata('img_user_kids');
                    $iduser  = $this->session->userdata('id_user');
                }
                // $iduser = $this->session->userdata('id_user');
                $get_image = $this->db->query("SELECT * FROM tbl_user WHERE id_user='".$iduser."'")->row_array();
            ?>
            <a>             
                <img onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" class="img-responsive" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$get_image['user_image'];?>" > 
            </a>

            <div class="pmop-edit" style="cursor:pointer;">
                <i class="zmdi zmdi-camera"></i> <span class="hidden-xs" ><?php echo $this->lang->line('upd_profpic');?></span>
            </div>
        </div>        
        
    </div>

    <div class="pmo-block pmo-contact hidden-xs">
        <h2><?php echo $this->lang->line('profil'); ?></h2>

        <ul>
            <li style="overflow:hidden; word-wrap: break-word;"><i class="zmdi zmdi-account"></i> 
                <?php 
                    $id_user_kids = $this->session->userdata('id_user_kids');
                    $get_data_anak = $this->db->query("SELECT ts.*, tpk.parent_id, tpk.jenjang_id FROM tbl_user as ts LEFT JOIN tbl_profile_kid as tpk ON ts.id_user=tpk.kid_id WHERE id_user='$id_user_kids'")->row_array();
                    $username_kids = $this->session->userdata('username_kids');
                    $parent_id  = $this->session->userdata('id_user');
                    $id_user_kids = $this->session->userdata('id_user_kids');
                    $birth_date = $get_data_anak['user_birthdate'];
                    $birthdate = date("d M Y", strtotime($birth_date));
                    if ($parent_id != $id_user_kids) {
                        
                    }
                    else{

                    }
                    echo $get_data_anak['user_name'];    
                ?>
            </li>
            <li style="overflow:hidden; word-wrap: break-word;"><i class="zmdi zmdi-calendar"></i> 
                <?php 
                    
                    echo $get_data_anak['user_birthplace'].', '.$birthdate; 
                ?>
            </li>
            <li style="overflow:hidden; word-wrap: break-word;">
                <i class="zmdi zmdi-male-female"></i>
                <address class="m-b-0 ng-binding">
                    <?php 
                        if ($get_data_anak['user_gender'] == "") {
                            echo $this->lang->line('nogender');
                        }
                        if ($get_data_anak['user_gender'] == "Male"){
                            echo $this->lang->line('male');
                        }
                        if($get_data_anak['user_gender'] == "Female"){
                            echo $this->lang->line('women');
                        }
                    ?>   
                </address>
            </li>            
        </ul>

    </div>

    <div id="btn-color-targetsKids">
     <div class="modal fade" data-modal-color="red"  data-backdrop="static" id="modal_fotoKids" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-body" style="margin-top: 75%;" > 
                        <br>
                        <div class="alert alert-danger" style="display: none; ?>" id="alert_ukuranKids"><center>Maksimal file 2 MB,<br>Ukuran foto harap 512 x 512 pixels atau lebih</center>
                        </div>
                        <div class="alert alert-danger" style="display: none; ?>" id="alert_wajahKids"><center><?php echo $this->lang->line('fotowajah'); ?></center>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" data-target="#modalColorKids" style="">OK</button>
                    </div>
                </div>
            </div>
        </div>
    <div class="modal fade" data-modal-color="" id="modalColorKids" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <form id="form1Kids" method="post" action="<?php echo base_url('Master/update_photo_kids'); ?>" enctype="multipart/form-data">
            <div class="modal-header">
                <center>
                	<h4 class="modal-title" style="color:gray; margin-bottom:-20px;"><?php echo $this->lang->line('uploadphoto');?></h4>                
                </center>
            </div>
            <hr>
            <div class="modal-body">                    
                        <input type="file" required name="inputFileKids" id="inputFileKids" class="filestyle" accept="image/x-png,image/gif,image/jpeg" data-placeholder="No Selected" style="margin-left:10px;">  
                        <!-- <img id="loading_imageKids" style="display: none; position:absolute; z-index: 3; margin-top: 25%; margin-left: 25%; width: 111px; height: 111px;" src="http://wallpaperget.com/assets/img/loading.gif">               -->
                        <center>
                        <img onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$get_image['user_image'];?>" style='z-index: 1; width: 256px; height: 256px;' id="viewgambarKids" class="m-t-20">
                        </center>
                    
                    <hr>
                    <h4 class="c-gray">Note: </h4>
                    <ul class="c-red">
                        <li>Maksimal file 2 MB.</li>
                        <li>Ukuran foto harap 512 x 512 pixels atau lebih.</li>                   
                    </ul>
                    
                    <script>
                        function readURL(input) {
                            var file = document.getElementById('inputFileKids').files[0];
                            var access_token_jwt = "<?php echo $this->session->userdata('access_token_jwt')?>";
                            // console.warn(access_token_jwt);
                            var iduser  = "<?php echo $this->session->userdata('id_user');?>";
                            var d = new Date();
                            var n = d.getTime();
                            var profil_img = "";
                            var reader = new FileReader();
                            if (input.files && input.files[0]) {
                                var reader = new FileReader();
                                if(file && file.size < 2000000) { // 10 MB (this size is in bytes)
                                    // alert('kecil');
                                    reader.onload = function (e) {
                                        $("body").css("cursor", "progress");
                                        // $('#viewgambarKids').attr('src', e.target.result);
                                        $("#loading_imageKids").css('display','block');
                                        profil_img = e.target.result;
                                        profil_img = profil_img.split(";base64,")[1];
                                        $.ajax({
                                            url: '<?php echo base_url(); ?>V1.0.0/face_exist/access_token/'+access_token_jwt,
                                            type: 'POST',
                                            data: {
                                                image: profil_img
                                                
                                            },

                                            success: function(response)
                                            {      
                                            $("#loading_imageKids").css('display','none');
                                            $("body").css("cursor", "default");                 
                                                console.warn("test 0 class "+response['message']);
                                                // alert("success");
                                                if (response['message'] == "face existed") {
                                                    var reader = new FileReader();
                                                    reader.onload = function(e){
                                                    $('#viewgambarKids').attr('src', e.target.result);
                                                    $('#saveKids').removeClass('disabled');
                                                  }  
                                                  reader.readAsDataURL(input.files[0]); 
                                                }
                                                else{
                                                    $("#loading_imageKids").css('display','none');
                                                    $("body").css("cursor", "default"); 
                                                    var $el = $('#inputFileKids');
                                                    $el.wrap('<form>').closest('form').get(0).reset();
                                                    $el.unwrap();
                                                    $("#modal_fotoKids").modal("show"); 
                                                    $("#modalColorKids").modal("hide");
                                                    $('#alert_wajahKids').css('display','block');
                                                    $('#alert_ukuranKids').css('display','none'); 
                                                    $('#saveKids').addClass('btn btn-info upload disabled'); 
                                                    
                                                    
                                                }    
                                            }

                                        }); 
                                    }
                                }
                                else{
                                $("#loading_imageKids").css('display','none');
                                $("body").css("cursor", "default"); 
                                $("#modal_fotoKids").modal("show"); 
                                $("#modalColorKids").modal("hide");
                                $('#alert_wajahKids').css('display','none');
                                $('#alert_ukuranKids').css('display','block');    
                                $('#saveKids').addClass('btn btn-info upload disabled');
                                }
                                reader.readAsDataURL(input.files[0]);
                            }
                        }

                        $("#inputFileKids").change(function () {
                            readURL(this);
                        });

                        $(":file").filestyle({placeholder: "No file"});                        
                    </script>   
            </div>
            <div class="progress progress-striped active m-t-10" style="height:18px;">
                <div class="progress-bar" style="width:0%; height:150px;"></div>
            </div>
            <div class="modal-footer bgm-teal">
                <button type="submit" class="btn btn-info upload disabled" id="saveKids" name="saveKids"><?php echo $this->lang->line('upload');?></button>                
                <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo $this->lang->line('cancel');?></button>
            </div>
            </form>
        </div>
    </div>
    </div>
    </div>

</div>

<script type="text/javascript">
    
    jQuery(document).ready(function(){

        function proccess()
        {
            $(document).on('submit','form',function(e){
                var inp = $("#inputFileKids");                
                if (inp.val().length > 0) {
                    e.preventDefault();
                    $form = $(this);
                    uploadImage($form);            
                }          
            });
        }

        // PROGRESS BAR
        document.forms[0].addEventListener('submit', function( evt ) {
            var file = document.getElementById('inputFileKids').files[0];
            var inp = $("#inputFileKids");

            if(file && file.size < 2000000) { // 10 MB (this size is in bytes)
                //Submit form
                // alert('sip bro');
                proccess(); 
            
                // if (inp.val().length > 0) {

                //     e.preventDefault();

                //     $form = $(this);

                //     uploadImage($form);
                    
                // }   
                
            } else {
                //Prevent default and display error
                // alert('terlalu besar bro');
                $.ajax({
                    url: '<?php echo base_url();?>Master/sizeistobig',
                    type: 'POST',
                    success: function(response)
                    { 
                        location.reload();
                    }
                });
                evt.preventDefault();
                
            }
        }, false);

        function uploadImage($form){
            $form.find('.progress-bar').removeClass('progress-bar-success')
                                        .removeClass('progress-bar-danger');

            var formdata = new FormData($form[0]); //formelement
            var request = new XMLHttpRequest();

            //progress event...
            request.upload.addEventListener('progress',function(e){
                var percent = Math.round(e.loaded/e.total * 100);
                $form.find('.progress-bar').width(percent+'%').html(percent+'%');
            });

            //progress completed load event
            request.addEventListener('load',function(e){
                
                $form.find('.progress-bar').addClass('progress-bar-success').html("<?php if ($this->session->userdata('lang') == 'indonesia') { echo "Unggah Selesai...";} else { echo "Uploading Completed...";} ?>");
                // alert('Profile Completed Change');
                setTimeout(function(){
                    window.location.reload();
                }, 1000);
            });

            request.open('post', '<?php echo base_url(); ?>Master/update_photo');            
            request.send(formdata);

            $form.on('click','.cancel',function(){
                request.abort();

                $form.find('.progress-bar')
                    .addClass('progress-bar-danger')
                    .removeClass('progress-bar-success')
                    .html('upload aborted...');
            });

        }

        $('body').on('click', '#btn-color-targetsKids > .btn', function(){
            var color = $(this).data('target-color');
            $('#modalColorKids').attr('data-modal-color', color);
        });

        jQuery('#change-pic').on('click', function(e){
            jQuery('#changePic').show();
            jQuery('#change-pic').hide();
            
        });
        
        jQuery('#photoimg').on('change', function()   
        { 
            jQuery("#preview-avatar-profileKids").html('');
            jQuery("#preview-avatar-profileKids").html('Uploading....');
            jQuery("#cropimage").ajaxForm(
            {
            target: '#preview-avatar-profileKids',
            success:    function() { 
                    jQuery('img#photo').imgAreaSelect({
                    aspectRatio: '1:1',
                    onSelectEnd: getSizes,
                });
                jQuery('#image_name').val(jQuery('#photo').attr('file-name'));
                }
            }).submit();

        });
    });

</script>
