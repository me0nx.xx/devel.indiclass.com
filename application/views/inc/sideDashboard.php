<div class="profile-menu">
    <a href="">
        <?php
            $id_user_kids = $this->session->userdata('id_user_kids');
            $parent_id  = $this->session->userdata('id_user');
            $iduser = $this->session->userdata('id_user_kids');
            $imguser  = $this->session->userdata('img_user_kids');

            if ($iduser == null) {
                $imguser = $this->session->userdata('img_user_kids');
                $iduser  = $this->session->userdata('id_user');
            }
            // $iduser = $this->session->userdata('id_user');
            $get_image = $this->db->query("SELECT * FROM tbl_user WHERE id_user='".$iduser."'")->row_array();
        ?>
        <div class="profile-pic" style="margin-bottom: -20px;">
            <img style="width: 70px; height: 70px;" onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$get_image['user_image'];?>" alt="">
        </div>

        <div class="profile-info">
            <?php echo $get_image['user_name'];
            ?>  
            <!-- <i class="zmdi zmdi-caret-down"></i> -->
        </div>
    </a>  
</div>

    <?php
        if ($this->session->userdata('status')==0) {
    ?>
        <ul class="main-menu">
                 <li class="<?php if($sideactive=="subject"){echo "active";}else{

            } ?>"><a href="<?php echo base_url(); ?>first/subjects"><i class="zmdi zmdi-desktop-windows"></i> <?php echo $this->lang->line('subject'); ?></a></li>
                                           
            <li class="<?php if($sideactive=="profile"){echo "active";}else{

            } ?>"><a href="<?php echo base_url(); ?>first/about"><i class="zmdi zmdi-account-circle"></i> <?php echo $this->lang->line('profil'); ?></a></li>
        </ul>
    <?php
        }
        else
        {
            $iduser = $this->session->userdata('id_user');
            $getbalance = $this->db->query("SELECT active_balance FROM uangsaku WHERE id_user='$iduser'")->row_array();
            $hasil = number_format($getbalance['active_balance'], 0, ".", ".");
            $usertype_id = $this->session->userdata('usertype_id');
    ?>

    <div class="card" style="background: #e5e5e5; height: 3px;"></div>

    <ul class="main-menu">
        
        <?php 
        if ($usertype_id == "student kid") {
            ?>            
                <li class="<?php if($sideactive=="mykids"){ echo "active";} else{} ?>"> 
                    <a href="<?php echo base_url(); ?>Kids"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/myclass2-01.svg" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('mykids_class'); ?></a>
                </li>   
                <li>
                    <a href="<?php echo base_url('/logout'); ?>"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/keluar1-01.svg" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('logout'); ?></a>
                </li>             
            <?php
        }
        else
        {
            $code_cek = $this->session->userdata('code_cek');
            if ($code_cek=='888' || $code_cek=='889') {
            ?>
                <li style="margin-top: -6%;" class="<?php if($sideactive=="home"){ echo "active";} else{} ?>"> 
                    <a href="#"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/ic_home_black.png" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('home'); ?></a>
                </li>
                <li style="margin-top: -6%;" class="<?php if($sideactive=="myclass"){ echo "active";} else{} ?>"> 
                    <a href="#"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/myclass2-01.svg" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('myclass'); ?></a>
                </li>
                <li class="<?php if($sideactive=="subject"){echo "active";}else{
                } ?>">
                    <a href="#"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/subject-01.svg" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> Tutor</a>
                </li>
                <li class="sub-menu <?php if($sideactive=="ondemand"){echo "active";}else{} ?>">
                    <a href="#"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/extraclass-01.svg" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('private'); ?></a>  
                    <ul>
                        <li><a href="#"><?php echo $this->lang->line('ondemand'); ?></a></li>
                        <li><a href="#">Data <?php echo $this->lang->line('ondemand'); ?></a></li>
                    </ul>
                </li>
                <!-- <li style="margin-top: -6%;" class="<?php if($sideactive=="quizside"){ echo "active";} else{} ?>"> 
                    <a href="#"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/logo_baru/Quiz.png" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('sidequiz'); ?></a>
                </li> -->
                <li class="sub-menu <?php if($sideactive=="quizside"){echo "active";}else{} ?>">
                    <a href="#"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/logo_baru/Quiz.png" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('sidequiz'); ?></a>
                    <ul>
                        <li><a href="#"><?php echo $this->lang->line('quizlist'); ?></a></li>                
                    </ul>
                </li>
                <li class="<?php if($sideactive=="profile"){echo "active";}else{} ?>">
                    <a href="#"><i class="zmdi zmdi-account"></i> <?php echo $this->lang->line('profil'); ?></a>
                </li>
                <!-- <li class="sub-menu <?php if($sideactive=="setting"){echo "active";}else{} ?>">
                    <a href="#"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/logo_baru/Akun.png" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('tab_account'); ?></a>
                    <ul>
                        <li class="<?php if($sideactive=="mykids"){ echo "sub-menu active";} else if ($sideactive=="mykids_subject"){ echo "sub-menu active";} else if($sideactive=="mykids_ondemand"){ echo "sub-menu active";} else if($sideactive=="mykids_dataondemand"){ echo "sub-menu active";} else if($sideactive=="mykids_registrasion"){ echo "sub-menu active";} else{ echo "sub-menu";} ?>">
                            <a href=""></i>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('mykids'); ?></a>
                            <ul>
                                <li class="<?php if($sideactive=="mykids"){ echo "active";} else{} ?>"> 
                                    <a href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('mykids_class'); ?></a>
                                </li>
                                <li>
                                    <a class="<?php if($sideactive=="mykids_registrasion"){ echo "active"; }else { echo "";} ?>" href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('mykids_registrasion'); ?></a>
                                </li>
                                <li>
                                    <a class="<?php if($sideactive=="mykids_subject"){ echo "active"; }else { echo "";} ?>" href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('mykids_subject'); ?></a>
                                </li>
                                <li>
                                    <a class="<?php if($sideactive=="mykids_ondemand"){ echo "active"; }else { echo "";} ?>" href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('mykids_demand'); ?></a>
                                </li>
                                <li>
                                    <a class="<?php if($sideactive=="mykids_dataondemand"){ echo "active"; }else { echo "";} ?>" href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $this->lang->line('mykids_ondemand'); ?></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li> -->
                <li class="<?php if($sideactive=="reputation"){echo "active";}else{
                } ?>">
                    <a href="#"><i class="tm-icon zmdi zmdi-comment-edit"></i> Ulasan</a>
                </li>
                <li>
                    <a href="<?php echo base_url('/logout'); ?>"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/keluar1-01.svg" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('logout'); ?></a>
                </li>
            <?php
            }
            else
            {
            ?>
                <li style="margin-top: -6%;" class="<?php if($sideactive=="home"){ echo "active";} else{} ?>"> 
                    <a href="<?php echo BASE_URL(); ?>"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/ic_home_black.png" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('home'); ?></a>
                </li>
                <li class="<?php if($sideactive=="myclass"){ echo "active";} else{} ?>"> 
                    <a href="<?php echo base_url(); ?>MyClass"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/myclass2-01.svg" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('myclass'); ?></a>
                </li>
                
                <li class="<?php if($sideactive=="subject"){echo "active";}else{
                } ?>">
                    <a href="<?php echo base_url(); ?>Subjects"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/subject-01.svg" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> Tutor</a>
                </li>
                
                <!-- <li class="sub-menu <?php if($sideactive=="ondemand"){echo "active";}else{} ?>">
                    <a href="<?php echo base_url(); ?>Ondemand"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/extraclass-01.svg" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('private'); ?></a>
                    
                    <ul>                    
                        <li><a href="<?php echo base_url(); ?>Dataondemand">Data <?php echo $this->lang->line('ondemand'); ?></a></li>
                    </ul>
                </li> -->

                <li class="<?php if($sideactive=="ondemand"){ echo "active";} else{} ?>"> 
                    <a href="<?php echo base_url(); ?>Dataondemand"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/extraclass-01.svg" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;">Data <?php echo $this->lang->line('ondemand'); ?></a>
                </li>

                <!-- <li class="<?php if($sideactive=="quizside"){ echo "active";} else{} ?>"> 
                    <a href="<?php echo base_url(); ?>Quiz"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/logo_baru/Quiz.png" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('sidequiz'); ?></a>
                </li> -->

                <li class="sub-menu <?php if($sideactive=="quizside"){echo "active";}else{} ?>">
                    <a href="<?php echo base_url(); ?>Quiz"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/logo_baru/Quiz.png" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('sidequiz'); ?></a>
                    <ul>
                        <li><a href="<?php echo base_url(); ?>Quiz"><?php echo $this->lang->line('quizlist'); ?></a></li>                
                    </ul>
                </li>

                <!-- <li class="<?php if($sideactive=="uangsaku"){echo "sub-menu active toggled";}else{
                echo "sub-menu";
            } 
            ?>">
            <a href="<?php echo base_url(); ?>first/uangsaku"><i class="zmdi zmdi-money-box"></i> <?php echo $this->lang->line('uangsaku'); ?></a>

            <ul>
            <li><a class="active" href="<?php echo base_url(); ?>first/uangsaku"><?php echo $this->lang->line('transactionlist'); ?></a></li>
            <li><a href="<?php echo base_url(); ?>first/uangsaku">Daftar Rekening</a></li>
            <li><a href="<?php echo base_url(); ?>first/uangsaku">Top Up</a></li>
            <li><a href="<?php echo base_url(); ?>first/uangsaku">Cara Top Up</a></li>
            </ul>
            </li>  --> 
            <li class="<?php if($sideactive=="profile"){echo "active";}else{} ?>">
                <a href="<?php echo base_url(); ?>About"><i class="zmdi zmdi-account"></i> <?php echo $this->lang->line('profil'); ?></a>
            </li>
            
            <li class="<?php if($sideactive=="reputation"){echo "active";}else{
            } ?>">
                <a href="<?php echo base_url();?>Reputation"><i class="tm-icon zmdi zmdi-comment-edit"></i> Ulasan</a>
            </li>

            <li>
                <a href="<?php echo base_url('/logout'); ?>"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/keluar1-01.svg" style="margin-left: -17%; margin-right: 6%; height: 23px; width: 23px;"> <?php echo $this->lang->line('logout'); ?></a>
            </li>
            <?php
            }
            }
        ?>
    </ul>
    <?php
    }
?>


