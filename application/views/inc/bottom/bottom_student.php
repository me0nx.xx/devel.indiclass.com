<!-- Visible when footer positions are fixed -->
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<div class="show-fixed pad-rgt pull-right">
    <a href="#" class="text-main"><span class="badge badge-danger"></span></a>
</div>

<p class="pad-lft">© 2018 Indiclass</p>