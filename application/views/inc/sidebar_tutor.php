<div class="mdk-drawer js-mdk-drawer" id="default-drawer" style="background-color: #FFFFFF;">
    <div class="mdk-drawer__content ">
        <div class="sidebar sidebar-left sidebar-light sidebar-transparent-sm-up o-hidden">
            <div class="sidebar-p-y" data-simplebar data-simplebar-force-enabled="true">
                
                <div class="sidebar-heading">Tutor</div>
                <ul class="sidebar-menu">
                    
                    <li class="sidebar-menu-item <?php if($sideactive=="classes_index"){ echo "active";} else{} ?>">
                        <a class="sidebar-menu-button" href="<?php echo BASE_URL();?>Tutor/KelasSaya">
                          <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">import_contacts</i> Jadwal Kelas
                        </a>
                    </li>
                    <!-- <li class="sidebar-menu-item <?php if($sideactive=="classes"){ echo "active";} else{} ?>">
                        <a class="sidebar-menu-button" href="<?php echo BASE_URL();?>Tutor/Classes">
                          <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">playlist_add</i> Classes
                        </a>
                    </li> -->
                    <!-- <li class="sidebar-menu-item <?php if($sideactive=="manage"){ echo "active";} else{} ?>">
                        <a class="sidebar-menu-button" href="<?php echo BASE_URL();?>Tutor/BankDetail">
                          <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">class</i> Bank Detail                           
                        </a>
                    </li>
                    <li class="sidebar-menu-item <?php if($sideactive=="request"){ echo "active";} else{} ?>">
                        <a class="sidebar-menu-button" href="<?php echo BASE_URL();?>Tutor/Approve">
                          <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">assessment</i> Approve Request Class                           
                        </a>
                    </li>  -->                                       
                </ul>
                <div class="sidebar-heading">Data Diri</div>
                <ul class="sidebar-menu">
                    <li class="sidebar-menu-item <?php if($sideactive=="personal"){ echo "active";} else{} ?>">
                        <a class="sidebar-menu-button" href="<?php echo BASE_URL();?>Tutor/DataDiri">
                          <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">account_box</i> Data Diri
                        </a>
                    </li>
                    <li class="sidebar-menu-item">
                        <a class="sidebar-menu-button" href="<?php echo BASE_URL();?>Admin/Logout">
                          <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">lock_open</i> Keluar
                        </a>
                    </li>
                </ul>
                
            </div>
        </div>
    </div>
</div>