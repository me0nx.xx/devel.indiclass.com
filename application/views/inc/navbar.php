<header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #db1f00;">
    <ul class="header-inner" >

        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>


        <li class="hidden-xs">

            <?php if ($this->session->userdata('status')==1) { ?>
                <a href="<?php echo base_url(); ?>" class="m-l-10"><img src="<?php echo BASE_URL();?>aset/img/indiLogo2.png" style="width: 130px; height: auto; margin-top: 3%;" alt=""></a>            
            <?php }?>
            <?php if ($this->session->userdata('status')== 0) { ?>
                <a href="#" class="m-l-10"><img src="<?php echo BASE_URL();?>aset/img/indiLogo2.png" style="width: 130px; height: auto; margin-top: 3%;" alt=""></a>            
            <?php }?>
            <?php if ($this->session->userdata('status')== 2) { ?>
                <a href="#" class="m-l-10"><img src="<?php echo BASE_URL();?>aset/img/indiLogo2.png" style="width: 130px; height: auto; margin-top: 3%;" alt=""></a>            
            <?php }?>
            <?php if ($this->session->userdata('status')== 3) { ?>
                <a href="#" class="m-l-10"><img src="<?php echo BASE_URL();?>aset/img/indiLogo2.png" style="width: 130px; height: auto; margin-top: 3%;" alt=""></a>            
            <?php }?>    
            <?php 
                    
                $channel_logo = $this->session->userdata('channel_logo');
                if ($channel_logo != null) {
                ?>
                <img style="width: auto; height: 35px; padding-left: 5px;" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$channel_logo ?>" alt="">
                <?php
                }
            ?>    
            
        </li>

        <li class="pull-right">
            <ul class="top-menu">                
                <li class="dropdown">
                    <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-more-vert"></i></a>
                    <ul class="dropdown-menu dm-icon pull-right">                    
                        <li>
                            <a onclick="signOut()" data-action="" href="<?php echo base_url('/logout'); ?>"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>side/keluar1-01.svg" style="margin-left: -7%; margin-right: 6%; height: 23px; width: 23px;">  <?php echo $this->lang->line('logout'); ?></a>
                        </li>
                    </ul>
                </li>
                
               <!--  <li class="hidden-xs" id="chat-trigger" data-trigger="#chat">
                    <a href=""><i class="tm-icon zmdi zmdi-comment-alt-text"></i></a>
                </li> -->
            
            </ul>
        </li>
    </ul>
    <!-- Top Search Content -->
    <!-- <div id="top-search-wrap">
        <div class="tsw-inner">
            <i id="top-search-close" class="zmdi zmdi-arrow-left"></i>
            <input type="text">
        </div>
    </div> -->
</header>
<script type="text/javascript">
    var id_user = "<?php echo $this->session->userdata('id_user')?>";
    var user_utc = new Date().getTimezoneOffset();
    user_utc = -1 * user_utc;  
    $(document).ready(function(){

        $('#btn_setindonesia').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
        });
        $('#btn_setenglish').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/english'); ?>',function(){ location.reload(); });
        });

        $('#lightblue').click(function(e){
        	e.preventDefault();
        	$.get('<?php $this->session->set_userdata('color','lightblue'); ?>',function(){  });
		});
		$('#bluegray').click(function(e){
			e.preventDefault();
			$.get('<?php $this->session->set_userdata('color','bluegray'); ?>',function(){ location.reload(); });						
		});
		$('#cyan').click(function(e){	
			e.preventDefault();
			$.get('<?php $this->session->set_userdata('color','cyan'); ?>',function(){ location.reload(); });			
		});
		$('#teal').click(function(e){
			e.preventDefault();
			$.get('<?php $this->session->set_userdata('color','teal'); ?>',function(){ location.reload(); });			
		});
		$('#orange').click(function(e){
			e.preventDefault();
			$.get('<?php $this->session->set_userdata('color','orange'); ?>',function(){ location.reload(); });			
		});
		$('#blue').click(function(e){
			e.preventDefault();
			$.get('<?php $this->session->set_userdata('color','blue'); ?>',function(){ location.reload(); });			
		});
    });
    
</script>