<!DOCTYPE html>
<html ng-app="cmApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-signin-client_id" content="947006145421-ubsui7grgk3j9bhi7icjmgjqsr3c1l7r.apps.googleusercontent.com">
    <meta name="theme-color" content="#008080"> 
    <meta property="og:url"           content="https://www.indiclass.id" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Indiclass" />
    <meta property="og:description"   content="Fitur belajar online interaktif yang lengkap serta banyak pilihan guru atau tutor handal dan menyenangkan" />
    <meta property="og:image"         content="<?php echo base_url();?>aset/img/playstore-icon.png" />

    <title>Indiclass</title>

    <!-- ________________________________________________________START DIPAKAI_______________________________________________________________________________________ -->
    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">    
    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">  
    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/chosen/chosen.min.css" rel="stylesheet">  
    <link href="<?php echo base_url(); ?>aset/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>aset/css/app.min.1.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/css/app.min.2.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/vendors/summernote/dist/summernote.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">

    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <?php
        $iduser = $this->session->userdata('id_user');
        $querycek = $this->db->query("SELECT count(*) as jumlah FROM tbl_notif WHERE id_user='$iduser' AND read_status=0")->row_array();
        $jumlahnotif = $querycek['jumlah'];
    ?>
    <link rel="icon" href="../aset/img/logo_atas.png" type="image/gif">
    <link href="<?php echo base_url(); ?>aset/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>aset/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>aset/css/css.css" rel="stylesheet">
    <!-- LOAD ANGULAR -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>aset/js/anjeondemand.js"></script> -->
    <script src="<?php echo base_url(); ?>aset/js/purchase.js"></script>
    <script src="<?php echo base_url(); ?>aset/js/tester.js"></script>
    <!-- <script src="<?php echo base_url(); ?>aset/js/anjenavbar.js"></script> -->
    <script src="<?php echo base_url(); ?>aset/js/ng-infinite-scroll.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/dhtmlxscheduler/dhtmlxscheduler_flat.css" type="text/css" media="screen" title="no title"
        charset="utf-8">
    <style type="text/css">
      ::-webkit-scrollbar {
          width: 9px;
      }
      ::-webkit-scrollbar-track {
          -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
          -webkit-border-radius: 7px;
          border-radius: 7px;
      }
      ::-webkit-scrollbar-thumb {
          -webkit-border-radius: 7px;
          border-radius: 7px;
          background: #a6a5a5;
          -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
      }
      ::-webkit-scrollbar-thumb:window-inactive {
          background: rgba(0,0,0,0.4); 
      }
      .modal-content  {
          -webkit-border-radius: 0px !important;
          -moz-border-radius: 0px !important;
          border-radius: 10px !important; 
      }
    </style>
    <style type="text/css">
        .toggle-switch .ts-label {
            min-width: 130px;
        }
        html, body {
            overflow: auto;
        }     
    </style>
    <style type="text/css">
        .ribbon {
           position: absolute;
           right: 8px; top: -1.8px;
           z-index: 1;
           overflow: hidden;
           width: 75px; height: 75px; 
           text-align: right;
        }
        .ribbon span {
           font-size: 10px;
           color: #fff; 
           text-transform: uppercase; 
           text-align: center;
           font-weight: bold; line-height: 20px;
           transform: rotate(45deg);
           width: 100px; display: block;
           background: #79A70A;
           background: linear-gradient(#9BC90D 0%, #79A70A 100%);
           box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
           position: absolute;
           top: 16px; right: -23px;
        }
        .ribbon span::before {
           content: '';
           position: absolute; 
           left: 0px; top: 100%;
           z-index: -1;
           border-left: 3px solid #79A70A;
           border-right: 3px solid transparent;
           border-bottom: 3px solid transparent;
           border-top: 3px solid #79A70A;
        }
        .ribbon span::after {
           content: '';
           position: absolute; 
           right: 0%; top: 100%;
           z-index: -1;
           border-right: 3px solid #79A70A;
           border-left: 3px solid transparent;
           border-bottom: 3px solid transparent;
           border-top: 3px solid #79A70A;
        }
        .ribbon {
           position: absolute;
           right: 8px; top: -1.8px;
           z-index: 1;
           overflow: hidden;
           width: 75px; height: 75px; 
           text-align: right;
        }
        .ribbon span {
           font-size: 10px;
           color: #fff; 
           text-transform: uppercase; 
           text-align: center;
           font-weight: bold; line-height: 20px;
           transform: rotate(45deg);
           width: 100px; display: block;
           background: #79A70A;
           background: linear-gradient(#9BC90D 0%, #79A70A 100%);
           box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
           position: absolute;
           top: 16px; right: -23px;
        }
        .ribbon span::before {
           content: '';
           position: absolute; 
           left: 0px; top: 100%;
           z-index: -1;
           border-left: 3px solid #79A70A;
           border-right: 3px solid transparent;
           border-bottom: 3px solid transparent;
           border-top: 3px solid #79A70A;
        }
        .ribbon span::after {
           content: '';
           position: absolute; 
           right: 0%; top: 100%;
           z-index: -1;
           border-right: 3px solid #79A70A;
           border-left: 3px solid transparent;
           border-bottom: 3px solid transparent;
           border-top: 3px solid #79A70A;
        }

        .ribbonn {
           position: absolute;
           right: -3px; top: -1.8px;
           z-index: 1;
           overflow: hidden;
           width: 180px; height: 100px; 
           text-align: right;
        }
        .ribbonn span {
           font-size: 10px;
           color: #fff; 
           text-transform: uppercase; 
           text-align: center;
           font-weight: bold; line-height: 20px;
           transform: rotate(45deg);
           width: 100px; display: block;
           background: #79A70A;
           background: linear-gradient(#9BC90D 0%, #79A70A 100%);
           box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
           position: absolute;
           top: 16px; right: -23px;
        }
        .ribbonn span::before {
           content: '';
           position: absolute; 
           left: 0px; top: 100%;
           z-index: -1;
           border-left: 3px solid #79A70A;
           border-right: 3px solid transparent;
           border-bottom: 3px solid transparent;
           border-top: 3px solid #79A70A;
        }
        .ribbonn span::after {
           content: '';
           position: absolute; 
           right: 0%; top: 100%;
           z-index: -1;
           border-right: 3px solid #79A70A;
           border-left: 3px solid transparent;
           border-bottom: 3px solid transparent;
           border-top: 3px solid #79A70A;
        }
        .red span {background: linear-gradient(#F70505 0%, #8F0808 100%);}
        .red span::before {border-left-color: #8F0808; border-top-color: #8F0808;}
        .red span::after {border-right-color: #8F0808; border-top-color: #8F0808;}

        .blue span {background: linear-gradient(#2989d8 0%, #1e5799 100%);}
        .blue span::before {border-left-color: #1e5799; border-top-color: #1e5799;}
        .blue span::after {border-right-color: #1e5799; border-top-color: #1e5799;}
    </style> 
</head>

<body <?php if(isset($sat)){ echo $sat; } ?> ng-controller="ModelController as ondemand">
  <div class=" modal fade" data-modal-color="blue" id="complete_modal" tabindex="-1" data-backdrop="static" role="dialog">
      <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content" style="margin-top: 75%;" >
              <div class="modal-header">
                  <h4 class="modal-title"><?php echo $this->lang->line('completeprofiletitle');?></h4>
              </div>
                  <center>
                      <div id='modal_approv' class="modal-body" >
                          <div class="alert alert-info" style="display: none; ?>" id="alertcompletedata">
                              <?php echo $this->lang->line('alertcompletedata');?>
                          </div>
                          <div class="alert alert-info" style="display: none; ?>" id="alertcomplete_school">
                                <?php echo $this->lang->line('alertcompleteschool');?>
                            </div>
                      </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                  </div>
              </center>
          </div>
      </div>
  </div>
  <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="modalrjjt" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header" style="height: 10px;">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
                  <h4 class="modal-title c-black">Tolak</h4>
              </div> 
              <div class="modal-body">
                  <hr>
                  <div class="alert alert-success alert-dismissible" role="alert" id="alertsuccessrjjt" style="display: none;">
                      Anda telah tolak permintaan group class dari teman anda.
                  </div>
                  <div class="alert alert-danger alert-dismissible" role="alert" id="alertfailedrjjt" style="display: none;">
                      gagal tolak permintaan group !!!
                  </div>
                  <div class="alert alert-warning alert-dismissible" role="alert" id="alertinforjjt" style="display: none;">
                      Anda sudah tolak permintaan group class ini.
                  </div>
                  <blockquote class="m-b-25">
                      <p class="c-gray">Berikut adalah detail kelas tersebut :</p>
                  </blockquote>                    
                  <div class="table table-responsive">
                      <table class="table table-striped">
                          <thead>
                              <tr>
                                  <th>Nama Pengajak</th>
                                  <th>Nama Tutor</th>
                                  <th>Mata Pelajaran</th>
                                  <th>Waktu Permintaan</th>
                                  <th>Durasi Kelas</th>
                              </tr>
                          </thead>
                          <tbody id="kotakdetailkelasrjjt" class="c-gray">

                          </tbody>
                      </table>
                  </div>
              </div>                                               
              <div class="modal-footer">
                  <button type="button" reqid="" ids="" id="btnrjjt" class="ckrjt btn bgm-red">Tolak</button>
              </div>                        
          </div>
      </div>
  </div>
  <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="modalappr" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header" style="height: 10px;">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
                  <h4 class="modal-title c-black">Persetujuan</h4>
              </div> 
              <div class="modal-body">
                  <hr>
                  <div class="alert alert-success alert-dismissible" role="alert" id="alertsuccessappr" style="display: none;">
                      Anda telah mensetujui permintaan group class dari teman anda.
                  </div>
                  <div class="alert alert-danger alert-dismissible" role="alert" id="alertfailedappr" style="display: none;">
                      gagal mensetujui permintaan group !!!
                  </div>
                  <div class="alert alert-warning alert-dismissible" role="alert" id="alertinfoappr" style="display: none;">
                      Anda sudah mensetujui permintaan group class ini.
                  </div>
                  <div class="alert alert-danger alert-dismissible" role="alert" id="alertinforejectappr" style="display: none;">
                      Anda sudah menolak permintaan group class ini.
                  </div>
                  <blockquote class="m-b-25">
                      <p class="c-gray">Berikut adalah detail kelas tersebut :</p>
                  </blockquote>                    
                  <div class="table table-responsive">
                      <table class="table table-striped">
                          <thead>
                              <tr>
                                  <th>Nama Pengajak</th>
                                  <th>Nama Tutor</th>
                                  <th>Mata Pelajaran</th>
                                  <th>Waktu Permintaan</th>
                                  <th>Durasi Kelas</th>
                              </tr>
                          </thead>
                          <tbody id="kotakdetailkelas" class="c-gray">

                          </tbody>
                      </table>
                  </div>
              </div>                                               
              <div class="modal-footer">
                  <button type="button" reqid="" ids="" id="btnappr" class="ckstj btn bgm-green">Setuju</button>
              </div>                        
          </div>
      </div>
  </div>
  <div class=" modal fade"  id="modal_alert" tabindex="-1" data-backdrop="static" role="dialog">
      <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content" id="modal_konten">
            <center>
                    <div id='' class="modal-body" style="margin-top: 75%;" ><br>
                       <div class="" style="color: white; display: block; ?>" id="alertbody">
                            <label id="text_modal">Halloo</label>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button  id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                </div>
            </center>
          </div>
      </div>
  </div>
  <div class=" modal fade" data-modal-color="blue" id="upd_jenjang_modal" tabindex="-1" data-backdrop="static" role="dialog">
      <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
                  <center>
                      <div id='modal_approv' class="modal-body" style="margin-top: 50%;" ><br>
                         <div class="alert alert-info" style="display: block; ?>" id="alertupdatejenjang">
                              Memasuki tahun ajaran baru, silakan perbaharui data sekolah kamu.
                          </div>
                      </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-link btn_updatejenjang" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                  </div>
              </center>
          </div>
      </div>
  </div>
  <div class=" modal fade" data-modal-color="blue" id="alert_updatejenjang" tabindex="-1" data-backdrop="static" role="dialog">
      <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
                  <center>
                      <div id='modal_approv' class="modal-body" style="margin-top: 50%;" ><br>
                         <div class="alert alert-info" style="display: block; ?>" id="">
                              Silakan perbaharui data sekolah Anda dibawah ini.
                          </div>
                      </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                  </div>
              </center>
          </div>
      </div>
  </div>
  <div class=" modal fade" data-modal-color="blue" id="tambah_ortu" tabindex="-1" data-backdrop="static" role="dialog" style="margin-top: 150px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <center>
                    <div id='' class="modal-body" style="" ><br>
                          <label for='nama'>Silakan masukkan email Orang Tua dibawah ini</label>
                           <input type="email" name="email" id="emailOrtu" required class="input-sm form-control fg-input">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btnOkeOrtu btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                    </div>
                </center>
            </div>
        </div>
    </div>
  <div class=" modal fade" data-modal-color="green" id="mdl_profil_success" tabindex="-1" data-backdrop="static" role="dialog">
      <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content" style="margin-top: 50%;">

                <center>
                    <div id='modal_approv' class="modal-body" style="" ><br>
                       <h4 class="c-white">
                         <?php echo $this->lang->line('successfullyupdate'); ?>
                       </h4>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn_oke" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                </div>
              </center>
          </div>
      </div>
  </div>
  <div class=" modal fade" data-modal-color="red" id="mdl_password_failed" tabindex="-1" data-backdrop="static" role="dialog">
      <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content" style="margin-top: 50%;">

                <center>
                    <div id='modal_approv' class="modal-body" style="" ><br>
                       <h4 class="c-white">
                         <?php echo $this->lang->line('failedpassword'); ?>
                       </h4>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn_oke" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                </div>
              </center>
          </div>
      </div>
  </div>
  <div class=" modal fade" data-modal-color="red" id="mdl_oldpassword_failed" tabindex="-1" data-backdrop="static" role="dialog">
      <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content" style="margin-top: 50%;">

                <center>
                    <div id='modal_approv' class="modal-body" style="" ><br>
                       <h4 class="c-white">
                         <?php echo $this->lang->line('oldpasswordfailed'); ?>
                       </h4>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn_oke" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                </div>
              </center>
          </div>
      </div>
  </div>
  <div class=" modal fade" data-modal-color="red" id="mdl_profil_failed" tabindex="-1" data-backdrop="static" role="dialog">
      <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content" style="margin-top: 50%;">

                <center>
                    <div id='modal_approv' class="modal-body" style="" ><br>
                       <h4 class="c-white">
                         <?php echo $this->lang->line('failedpassword'); ?>
                       </h4>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn_oke" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                </div>
              </center>
          </div>
      </div>
  </div>
  <div class=" modal fade" data-modal-color="green" id="mdl_upd_success" tabindex="-1" data-backdrop="static" role="dialog">
      <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content" style="margin-top: 50%;">
                <center>
                    <div id='modal_approv' class="modal-body" style="" ><br>
                       Terima kasih telah memperbaharui jenjang sekolah Anda
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn_oke" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                </div>
              </center>
          </div>
      </div>
  </div>
  <div class=" modal fade" data-modal-color="red" id="mdl_upd_failed" tabindex="-1" data-backdrop="static" role="dialog">
      <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
                  <center>
                      <div id='modal_approv' class="modal-body" style="margin-top: 50%;" ><br>
                         Gagal memperbaharui jenjang sekolah Anda, silakan ulangi kembali.
                      </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-link btn_oke" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                  </div>
              </center>
          </div>
      </div>
  </div>

    <div class='modal fade' style=' margin-left: 13%; margin-right: 13%;' id='modalShowClassApproved' >
        <div class='modal-dialog'>
            <div class='modal-content'>
                <div class='modal-header' style='height: 10px;'>
                    <h4 class='m-b-25 modal-title c-black'>Kelas Tambahan <b id='txt_type_class'></b> Anda disetujui oleh tutor </h4>
                </div> 
                <hr>
                <div class='modal-body'>
                    <div class='row'>                        
                        <div class='col-md-12 c-black'>
                            <div class='col-md-3'>
                                <img class='img-rounded m-t-20' id='txt_image_tutor' onerror='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=' style='height: 120px; width: 120px; background-size: 100% 100%; background-color: #ECECEC;'>
                            </div>
                            
                            <div class='col-md-9'>
                                <table class='table table-responsive p-10 table-bordered'>
                                    <tbody>
                                        <tr>
                                            <td ><i class=" zmdi zmdi-account"></i><label style="margin-left: 10px;" id='txt_name_tutor'></label></td>
                                        </tr>
                                        <tr>
                                            <td ><i class=" zmdi zmdi-time"></i><label style="margin-left: 10px;" id='txt_req_time'></label></td>
                                        </tr>
                                        <tr>
                                            <td ><i class=" zmdi zmdi-book"></i><label style="margin-left: 10px;" id='txt_subject'></label></td>
                                        </tr>
                                        <tr>
                                            <td ><i class=" zmdi zmdi-comment-text-alt"></i><label style="margin-left: 10px;" id='txt_topic'></label></td>
                                        </tr>
                                    </tbody>
                                </table>
                              </div>
                        </div>
                    </div>
                </div>                                               
                <div class='modal-footer'>
                    <button type='button' data-dismiss='modal' aria-label='Close' class='btn bgm-white c-gray'>Bayar Nanti</button>
                    <a id='gotoinvoice' href="">
                       <button type='button' class='btn_goto_invoice btn bgm-green'  >Bayar Sekarang</button>
                    </a>
                </div>                        
            </div>
        </div>
    </div>
    <div class="modal fade" style=" margin-left: 13%; margin-right: 13%;" id="modal_approve_tutor" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="height: 10px;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
                    <h4 class="modal-title c-black">Persetujuan</h4>
                </div> 
                <div class="modal-body">
                    <div class="row">
                        <hr>
                        <div class="header" align="center">
                            <label class="c-black f-17">Anda mendapat permintaan kelas dari Channel <b id='nama_channel'></b></label><br><br>  
                        </div>
                        <div class="col-md-12 c-black" style="">
                            <div class="col-md-3">
                                <img class="img-rounded" id='logo_channel' onerror="<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=" style="height: 120px; width: 120px; background-color: #008080;">
                            </div>
                            <div class="col-md-3" style="padding-right: 0;">
                                <label>Nama Program</label>
                                <label>Waktu Permintaan</label><br>
                                <label>Mata Pelajaran</label><br>
                                <label>Topik</label><br>
                                <label>Durasi Kelas</label><br>
                            </div>
                            <div class="col-md-5" style="padding: 0;">
                                <b>: </b><label id="nama_program"></label><br>
                                <b>: </b><label id="waktu_permintaan"></label><br>
                                <b>: </b><label id="mata_pelajaran"></label><br>
                                <b>: </b><label id="topik"></label><br>
                                <b>: </b><label id="durasi_kelas"></label><br>
                            </div>
                        </div>
                    </div>
                </div>                                               
                <div class="modal-footer">
                    <button type="button" reqid="" ids="" id="" class="btn btn-link reject_req c-red">Tolak</button>
                    <button type="button" reqid="" ids="" id="" class="btn approve_req bgm-green">Setuju</button>
                </div>                        
            </div>
        </div>
    </div>

    <div class="modal fade" style=" margin-left: 13%; margin-right: 13%;" id="modalApproveToTutor" data-modal-color="">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="height: 10px;">
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button> -->
                    <h4 class="modal-title c-black">Persetujuan</h4>
                </div> 
                <div class="modal-body">
                    <div class="row">
                        <hr>
                        <div class="header" align="center">
                          <label class="c-black f-17">Anda mendapat permintaan kelas tambahan <b id='app_typerequest'></b></label><br><br>  
                        </div>
                        <div class="col-md-12 c-black" style="">
                            <div class="col-md-3">
                                <img class="img-rounded m-t-20" id='app_user_image' onerror="<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=" style="height: 120px; width: 120px; background-size: 100% 100%; background-color: #ECECEC;">
                            </div>
                            
                            <div class="col-md-9">
                                <table class="table table-responsive p-10 table-bordered">
                                  <tbody>
                                        <tr>
                                            <td ><i class=" zmdi zmdi-account"></i><label style="margin-left: 10px;" id='app_studentname'></label></td>
                                        </tr>
                                        <tr>
                                            <td ><i class=" zmdi zmdi-calendar"></i><label style="margin-left: 10px;" id='app_timerequest'></label></td>
                                        </tr>
                                        <tr>
                                            <td ><i class=" zmdi zmdi-book"></i><label style="margin-left: 10px;" id='app_subject'></label></td>
                                        </tr>
                                        <tr>
                                            <td ><i class=" zmdi zmdi-comment-text-alt"></i><label style="margin-left: 10px;" id='app_topik'></label></td>
                                        </tr>
                                        <tr>
                                            <td ><i class=" zmdi zmdi-time"></i><label style="margin-left: 10px;" id='app_durasi'></label></td>
                                        </tr>
                                    </tbody>
                                </table>
                              </div>
                        </div>
                    </div>
                </div>                                               
                <div class="modal-footer">
                    <button type="button" class="btn-reject-demand btn bgm-white c-gray" demand-link="">Tolak</button>
                    <button type="button" class="btn-choose-demand btn bgm-green" id="confrimbro" demand-link="">Setujui</button>
                </div>                        
            </div>
        </div>
    </div>

    <!-- Javascript Libraries -->
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/js/jquery-ui.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/alertify/alertify.js "></script> -->
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>  
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/moment/min/moment.min.js"></script> 
    <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>   -->
    <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/moment/min/moment.min.js"></script> -->
    <script src="<?php echo base_url(); ?>aset/date/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/chosen/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js "></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bootgrid/jquery.bootgrid.updated.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/js/functions.js"></script>  
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/Waves/dist/waves.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/fileinput/fileinput.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/vendors/input-mask/input-mask.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/js/scripts.js"></script>
    <script src="<?php echo base_url(); ?>aset/js/jquery.smartWizard.js"></script>
    <script src="<?php echo base_url(); ?>aset/js/custom.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/js/jquery.maskMoney.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>aset/camerajs/webcam.js"></script> 
    <!-- <script src="https://apis.google.com/js/api:client.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script> -->    
    <script src="<?php echo base_url(); ?>aset/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/mediaelement/build/mediaelement-and-player.min.js"></script> -->
    <script>
      var FileAPI = {
          debug: true
        , media: true
        , staticPath: './FileAPI/'
      };
    </script>
    <script src="<?php echo base_url(); ?>aset/FileAPI/FileAPI.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/FileAPI/FileAPI.exif.js"></script>
    <script src="<?php echo base_url(); ?>aset/jquery.fileapi.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/js/bootstrap-filestyle.min.js"> </script>
    <script src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/dhtmlxscheduler.js" type="text/javascript" charset="utf-8"></script>
    <script src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/dhtmlxscheduler_editors.js" type="text/javascript" charset="utf-8"></script>
    <script src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/dhtmlxscheduler_minical.js" type="text/javascript" charset="utf-8"></script>
    <script src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/<?php if ($this->session->userdata('lang') == 'indonesia') { echo 'locale_id'; } else if ($this->session->userdata('lang') == 'english'){ echo 'locale_en';} ?>.js" type="text/javascript" charset="utf-8"></script>
    <script src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/dhtmlxscheduler_serialize.js" type="text/javascript" charset="utf-8"></script>
    <script src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/dhtmlxscheduler_collision.js" type="text/javascript" charset="utf-8"></script>

    <!-- UNTUK DELETE -->
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/backbone.js"></script>-->
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/underscore.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/dhtmlxscheduler_mvc.js?!"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/js/jquery.hotkeys.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>aset/dhtmlxscheduler/js/dhtmlxscheduler_readonly.js" type="text/javascript" charset="utf-8"></script> -->

    <!-- UNTUK VIDEO5HTML -->
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/swfobject.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.dotdotdot-1.5.1.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.address.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.selectbox-0.2.js"></script>
    <script type="text/javascript" src="http://www.youtube.com/player_api"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.apPlaylistManager.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.apYoutubePlayer.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.vg.settings_scroll.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.func.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.vg.func.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/video5/js/jquery.videoGallery.min.js"></script> -->

    <!-- TIDAK DI PAKAI -->
    <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script> -->
    <script src="<?php echo base_url(); ?>aset/vendors/summernote/dist/summernote-updated.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/flot/jquery.flot.js"></script> -->
    <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/flot/jquery.flot.resize.js"></script> -->
    <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/flot.curvedlines/curvedLines.js"></script> -->
    <!-- <script src="<?php echo base_url(); ?>aset/vendors/sparklines/jquery.sparkline.min.js"></script> -->
    <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>      -->
    <!-- <script src="<?php echo base_url(); ?>aset/js/flot-charts/curved-line-chart.js"></script> -->
    <!-- <script src="<?php echo base_url(); ?>aset/js/flot-charts/line-chart.js"></script> -->
    <!-- <script src="<?php echo base_url(); ?>aset/js/charts.js"></script>     -->
    <!-- <script src="<?php echo base_url(); ?>aset/vendors/farbtastic/farbtastic.min.js"></script>   -->
    <!-- <script src="<?php echo base_url(); ?>aset/js/jquery.backstretch.js"></script> -->
    <!-- <script src="<?php echo base_url(); ?>aset/js/jquery.backstretch.min.js"></script> -->
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>aset/imgareaselect/scripts/jquery.imgareaselect.pack.js"></script> -->
    <!-- <script src="<?php echo base_url(); ?>aset/js/cropbox.js"></script>   -->
    <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script> -->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-91410789-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script>
        jQuery(function ($){
            var $blind = $('.splash__blind');
            $('.splash').mouseenter(function (){
                $('.splash__blind', this).animate({ top: -10 }, 'fast', 'easeInQuad').animate({ top: 0 }, 'slow', 'easeOutBounce');
            }).click(function (){
                if( !FileAPI.support.media ){
                    $blind.animate({ top: -$(this).height() }, 'slow', 'easeOutQuart');
                }
                FileAPI.Camera.publish($('.splash__cam'), function (err, cam){
                    if( err ){
                        // alert("Unfortunately, your browser does not support webcam.");
                    } else {
                        $('.splash').off();
                        $blind.animate({ top: -$(this).height() }, 'slow', 'easeOutQuart');
                    }
                });
            });   
            $('body').on('click', '[data-tab]', function (evt){
                evt.preventDefault();
                var el = evt.currentTarget;
                var tab = $.attr(el, 'data-tab');
                var $example = $(el).closest('.example');
                $example
                .find('[data-tab]')
                .removeClass('active')
                .filter('[data-tab="'+tab+'"]')
                .addClass('active')
                .end()
                .end()
                .find('[data-code]')
                .hide()
                .filter('[data-code="'+tab+'"]').show();
            });
            function _getCode(node, all){
                var code = FileAPI.filter($(node).prop('innerHTML').split('\n'), function (str){ return !!str; });
                if( !all ){
                  code = code.slice(1, -2);
                }
                var tabSize = (code[0].match(/^\t+/) || [''])[0].length;
                return $('<div/>').text($.map(code, function (line){
                return line.substr(tabSize).replace(/\t/g, '   ');
                }).join('\n'))
                .prop('innerHTML')
                .replace(/ disabled=""/g, '')
                .replace(/&amp;lt;%/g, '<%')
                .replace(/%&amp;gt;/g, '%>');
            }
        });
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-16483888-3', 'rubaxa.github.io');
        ga('send', 'pageview');
    </script>
  
    <div id="fb-root"></div>
    <script type="text/javascript">
      $(".btnOkeOrtu").click(function(){
      var emailOrtu = $("#emailOrtu").val();
      var id_kid = "<?php echo $this->session->userdata('id_user'); ?>";
        $.ajax({
            url: '<?php echo base_url(); ?>Rest/addParentKids/',
            type: 'POST',
            data: {                
                email : emailOrtu,
                kid_id: id_kid
            },      
            success: function(data)
            {              
                if (data['status']) {
                  swal("Success add Parents", "", "success")

                  // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Success Join to Channel');
                  setTimeout(function(){
                      location.reload();
                  },3000);
                }
                else if (data['code'] == -101) {
                  swal("Email tersebut sudah menjadi parrent Anda", "", "info")
                }
                else{
                  swal("error", "", "error")

                  // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Success Join to Channel');
                  setTimeout(function(){
                      location.reload();
                  },3000);
                }
            }
        });
      });
    </script>
    <script type="text/javascript">        
        window.fbAsyncInit = function() {
            FB.init({
              appId      : '346921169470280',
              xfbml      : true,
              version    : 'v2.8'
            });
            FB.AppEvents.logPageView();
            FB.getLoginStatus(function (response) {
            })
        };
        (function(d, s, id){
           var js, fjs = d.getElementsByTagName(s)[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement(s); js.id = id;
           js.src = "//connect.facebook.net/en_US/sdk.js";
           fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();
            // alert(profile.getName());
            console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            console.log('Name: ' + profile.getName());
            // console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail());
        }
        function signOut() {
          FB.logout(function(response) {
            // alert(JSON.stringify(response));
          });
          var auth2 = gapi.auth2.getAuthInstance();
          auth2.signOut().then(function () {
            console.log('User signed out.');
          }); 
        }
        function onLoad() {
          gapi.load('auth2', function() {
            auth2 = gapi.auth2.init({
              client_id: '947006145421-ubsui7grgk3j9bhi7icjmgjqsr3c1l7r.apps.googleusercontent.com',
                // scope: 'profile email'
              });
          });
        }
        var pfp = "BBBB";
        var pfka = "AAA";
        var pfkec = "AAA";
        var jname = '';
        var jid = "BBBB";
        $(document).ready(function() {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            $.ajaxSetup({
                type:"POST",
                url: "<?php echo base_url()?>First/ambil_data",
                cache: false,
            });
            $('select.selectALL').each(function(){
                if($(this).attr('id') == 'jenjang_level'){
                    $(this).select2({
                        delay: 2000,
                        ajax: {
                            dataType: 'json',
                            type: 'GET',
                            url: '<?php echo base_url(); ?>ajaxer/ajax_jenjang_level_ALL/',
                            data: function (params) {
                                return {
                                    term: params.term,
                                    page: params.page || 1,
                                    jname: jname,
                                };
                            },
                            processResults: function(data){
                                return {
                                    results: data.results,
                                    pagination: {
                                        more: data.more
                                    }
                                };
                            }
                        }
                    });
                }
                else{
                  $(this).select2();
                }
            });
             $('select.selectSD').each(function(){
                if($(this).attr('id') == 'jenjang_level'){
                    $(this).select2({
                        delay: 2000,
                        ajax: {
                            dataType: 'json',
                            type: 'GET',
                            url: '<?php echo base_url(); ?>ajaxer/ajax_jenjang_level_SD/',
                            data: function (params) {
                                return {
                                    term: params.term,
                                    page: params.page || 1,
                                    jname: jname,
                                };
                            },
                            processResults: function(data){
                                return {
                                    results: data.results,
                                    pagination: {
                                        more: data.more
                                    }
                                };
                            }
                        }
                    });
                }
                else{
                  $(this).select2();
                }
            });
            $('select.select2').each(function(){
                if($(this).attr('id') == 'jenjang_name'){
                    $(this).select2({
                        delay: 2000,
                        ajax: {
                            dataType: 'json',
                            type: 'GET',
                            url: '<?php echo base_url(); ?>ajaxer/ajax_jenjang_name/',
                            data: function (params) {
                                return {
                                    term: params.term,
                                    page: params.page || 1,
                                    jname: jname,
                                };
                            },
                            processResults: function(data){
                                return {
                                    results: data.results,
                                    pagination: {
                                        more: data.more
                                    }
                                };
                            }
                        }
                    });
                }else if($(this).attr('id') == 'jenjang_level'){
                    $(this).select2({
                        // minimumInputLength: 3,
                        delay: 2000,
                        ajax: {
                            dataType: 'json',
                            type: 'GET',
                            url: '<?php echo base_url(); ?>ajaxer/ajax_jenjang_level/',
                            data: function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1,
                                jname: jname,
                            };
                        },
                        processResults: function(data){
                            return {
                                results: data.results,
                                pagination: {
                                    more: data.more
                                }
                            };
                        }
                    }
                    });
                }else if($(this).attr('id') == 'provinsi_id' || $(this).attr('id') == 'kecamatan_id' || $(this).attr('id') == 'kabupaten_id' || $(this).attr('id') == 'desa_id'){
                    var a = null;
                    if ($(this).attr('id') == 'provinsi_id') { a = "<?php echo $this->lang->line('chooseyourpropinsi'); ?>";} if ($(this).attr('id') == 'kecamatan_id') { a = "<?php echo $this->lang->line('chooseyourkecamatan'); ?>";} if ($(this).attr('id') == 'kabupaten_id') { a = "<?php echo $this->lang->line('chooseyourkabupaten'); ?>";} if ($(this).attr('id') == 'desa_id') { a = "<?php echo $this->lang->line('chooseyourkelurahan'); ?>";}
                    $(this).select2({
                        minimumInputLength: 1,
                        placeholder: a,
                        delay: 2000,
                        ajax: {
                            dataType: 'json',
                            type: 'GET',
                            url: '<?php echo base_url(); ?>ajaxer/ajax_indonesia/' + $(this).attr('id'),
                            data: function (params) {
                                return {
                                    term: params.term,
                                    page: params.page || 1,
                                    pfp: pfp,
                                    pfka: pfka,
                                    pfkec: pfkec,
                                    jid:jid
                                };
                            },
                            processResults: function(data){
                                return {
                                    results: data.results,
                                    pagination: {
                                        more: data.more
                                    }
                                };
                            }
                        }
                    });
                }
                else if($(this).attr('name') == "educational_level"){
                    $(this).select2({
                        delay: 2000,
                        ajax: {
                            dataType: 'json',
                            type: 'GET',
                            url: '<?php echo base_url(); ?>ajaxer/getJenjangLevel',
                            data: function (params) {
                              return {
                                term: params.term,
                                page: params.page || 1
                              };
                            },
                            processResults: function(data){
                              return {
                                results: data.results,
                                pagination: {
                                  more: data.more
                                }
                              };
                            }
                        }
                    });
                }
                else{
                  $(this).select2();
                }
            });          
            $("#jenjang_name").change(function(){
                jname = $(this).val();
                $('#jenjang_level.select2').select2("val", "");
            });
            $('#provinsi_id').change(function(){
                var prov = $(this).val();
                pfp = prov;
                $('#kabupaten_id.select2').select2("val","");
            });
            $('#kabupaten_id').change(function(){
                var prov = $(this).val();
                pfka = prov;
                $('#kecamatan_id.select2').select2("val","");
            });
            $('#kecamatan_id').change(function(){
                var prov = $(this).val();
                pfkec = prov;
                $('#desa_id.select2').select2("val","");
            });
            $('#desa_id').change(function(){
                var prov = $(this).val();
                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url(); ?>ajaxer/getPos/'+prov,
                    success:function(datis){
                        $('#kodepos').val(datis);
                    }
                });
            });
            $('#wizard').smartWizard({
                enableFinishButton: false,
                hideButtonsOnDisabled: true,
                labelFinish:'<?php echo $this->lang->line('submitapproval'); ?>',
                labelNext:'<?php echo $this->lang->line('next'); ?>',
                labelBack:'<?php echo $this->lang->line('back'); ?>',
                reverseButtonsOrder: false,
                onLeaveStep:leaveAStepCallback,
                onFinish:validationFinishCallback
            });
            $('#wizard_verticle').smartWizard({
                transitionEffect: 'slide'
            });
            $('.buttonNext').addClass('btn btn-success');
            $('.buttonPrevious').addClass('btn btn-primary');
            $('.buttonFinish').addClass('btn btn-warning disabled');
            $('.buttonPrevious').click(function(){
                $('.buttonFinish').show('none');
            });
            $('.buttonNext').click(function(){
                $('.buttonFinish').show('block');
            });
            $('#btn_submit_ok').click(function(){
                $('#modal_loading').modal('show');
                $('.buttonFinish').addClass('btn btn-warning disabled');
                // setTimeout(function(){
                //     $("#modal_loading").modal('hide');
                //     getpollingdata(parameter);
                // },5000);
                location.href='<?php echo base_url(); ?>tutor';
            });

            $('.buttonFinish').click(function(){
                $('#modal_loading').modal('show');
                $('.buttonFinish').addClass('btn btn-warning disabled');
                setTimeout(function(){
                    $("#modal_loading").modal('hide');
                    getpollingdata(parameter);
                },3000);
            });
           

          

            function leaveAStepCallback(obj, context){
                return validateSteps(); // return false to stay on step and true to continue navigation 
            }
            function validateSteps(stepnumber){
                var isStepValid = true;
                // validate step 1   
                if($('#imgfile').val() == '' && $("#cek_foto").val()=="2"){
                    $("#modal_foto").modal('show');
                    $('#alert_wajah').css('display','none');
                    $('#alert_ukuran').css('display','none');
                    $('#alert_wajah').css('display','block');
                    return false;                       
                }       
                if($('#user_birthplace').val() == ''){
                    $('#user_birthplace').focus();
                    $("#valid_modal").modal('show');
                    $('#cek_tempat').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
                    return false;                       
                }
                if($('#user_religion').val() == '') {
                    $('#cek_agama').focus();
                    $("#valid_modal").modal('show');
                    $('#cek_agama').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
                    return false;
                }
                if($('#user_address').val() == '') {
                    $('#user_address').focus();
                    $("#valid_modal").modal('show');
                    $('#cek_alamat').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
                    return false;
                }
                if($('#provinsi').val() == '') {
                    $('#cek_provinsi').focus();
                    $("#valid_modal").modal('show');
                    $('#cek_provinsi').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
                    return false;
                }
                if($('#kabupaten').val() == '') {
                    $('#cek_kabupaten').focus();
                    $("#valid_modal").modal('show');
                    $('#cek_kabupaten').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
                    return false;
                }
                if($('#kecamatan').val() == '') {
                    $('#cek_kecamatan').focus();
                    $("#valid_modal").modal('show');
                    $('#cek_kecamatan').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
                    return false;
                }
                if($('#kelurahan').val() == '') {
                    $('#cek_keluarahan').focus();
                    $("#valid_modal").modal('show');
                    $('#cek_keluarahan').html('<?php echo $this->lang->line('noempty'); ?>').css('color', 'red');
                    return false;
                }
                var o = {};
                var a = $('#approvalForm_account').serializeArray();
                $.each(a, function() {
                    if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                var y = {};
                var b = $('#approvalForm_profile').serializeArray();
                $.each(b, function() {
                    if (y[this.name] !== undefined) {
                        if (!y[this.name].push) {
                            y[this.name] = [y[this.name]];
                        }
                        y[this.name].push(this.value || '');
                    } else {
                        y[this.name] = this.value || '';
                    }
                });
                $.ajax({
                    url: '<?php echo base_url(); ?>master/tutor_tempApproval/<?php echo $this->session->userdata('id_user'); ?>',
                    type: 'POST',
                    data: {
                        'form_account': JSON.stringify(o),
                        'form_profile': JSON.stringify(y)
                    },
                    success: function(hasil){
                        // location.href='<?php echo base_url(); ?>tutor';
                    }
                });                            
                return true;   
            }
            function validationFinishCallback(argument) {
              var isStepValid = true;
              // validate step 1
              
              if($('#educational_level1').val() == ''){
               $("#valid_modal").modal('show');
                return false;                       
              }
              if($('#educational_competence1').val() == '') {
                $("#valid_modal").modal('show');
                return false;
              }
              if($('#educational_institution1').val() == '') {
                $("#valid_modal").modal('show');
                return false;
              }
              if($('#institution_address1').val() == '') {
                $("#valid_modal").modal('show');
                return false;
              }
              if($('#graduation_year1').val() == '') {
                $("#valid_modal").modal('show');
                return false;
              }
              if($('#description_experience1').val() == '') {
                $("#valid_modal").modal('show');
                return false;
              }
              if($('#year_experience1').val() == '') {
                $("#valid_modal").modal('show');
                return false;
              }
              if($('#year_experience2').val() == '') {
                $("#valid_modal").modal('show');
                return false;
              }  
              if($('#place_experience1').val() == '') {
                $("#valid_modal").modal('show');
                return false;
              }  
              if($('#information_experience1').val() == '') {
                $("#valid_modal").modal('show');
                return false;
              }
              

              onFinishCallback(argument);
            }
            function onFinishCallback(argument) {
              // $('#approvalForm_account').submit();
              var o = {};
              var a = $('#approvalForm_account').serializeArray();
              $.each(a, function() {
                if (o[this.name] !== undefined) {
                  if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                  }
                  o[this.name].push(this.value || '');
                } else {
                  o[this.name] = this.value || '';
                }
              });
              var y = {};
              var b = $('#approvalForm_profile').serializeArray();
              $.each(b, function() {
                if (y[this.name] !== undefined) {
                  if (!y[this.name].push) {
                    y[this.name] = [y[this.name]];
                  }
                  y[this.name].push(this.value || '');
                } else {
                  y[this.name] = this.value || '';
                }
              });
              $.ajax({
                url: '<?php echo base_url(); ?>master/tutor_submitApproval/<?php echo $this->session->userdata('id_user'); ?>',
                type: 'POST',
                data: {
                  'form_account': JSON.stringify(o),
                  'form_profile': JSON.stringify(y)
                },
                error: function(jqXHR) { 
                    if(jqXHR.status==0) {
                        $("#nointernet_modal").modal('show');
                        $('.buttonFinish').removeClass('disabled');
                    }
                },
                success: function(hasil){
                    if (hasil!=null) {
                    $("#modal_submit").modal('show');        
                    }
                    else{
                    $("#valid_modal").modal('show');   
                    }
                
                }
              });
            
            }
            $("#provinsi").change(function(){
                var value=$(this).val();                
                if(value>0){
                    $.ajax({                    
                        data:{modul:'kabupaten',id:value},
                        success: function(respond){
                            $("#kabupaten").html(respond);
                        }
                    })
                }
            });
            $("#kabupaten").change(function(){
                var value=$(this).val();                
                if(value>0){
                    $.ajax({
                        data:{modul:'kecamatan',id:value},
                        success: function(respond){
                            $("#kecamatan").html(respond);
                        }
                    })
                }
            });
            $("#kecamatan").change(function(){
                var value=$(this).val();
                if (value>0) {
                    $.ajax({
                        data:{modul:'kelurahan',id:value},
                        success: function(respond){
                            $("#kelurahan").html(respond);
                        }
                    })
                }
            });
            $("#jenjangname").change(function(){
                var name = $("#jenjangname").val();              
                $.ajax({
                    type:"POST",
                    url: "<?php echo base_url()?>First/getlevels",
                    cache: false,
                    data:{"name":name},
                    success: function(respond){
                        $("#jenjanglevel").html(respond);
                    }
                })
            });
            var cId = $('#calendar'); //Change the name if you want. I'm also using thsi add button for more actions
            $('#buttonSession').click(function(){
                event.preventDefault();
                var url = $(this).attr('href');
                location.href=url;
                var session = '';
            });            
            $('#tanggal_lahir').on('keypress',function(e){
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
            $('#tahun_lahir').on('keypress',function(e){
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
            $('#no_hape').on('keypress',function(e){
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
            $('#no_hp_ortu').on('keypress',function(e){
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
            $('#user_callnum').on('keypress',function(e){
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
            $("#data-table-basic").bootgrid({
                css: {
                    icon: 'zmdi icon',
                    iconColumns: 'zmdi-view-module',
                    iconDown: 'zmdi-expand-more',
                    iconRefresh: 'zmdi-refresh',
                    iconUp: 'zmdi-expand-less'
                },
            });                
            $("#data-table-selection").bootgrid({
                css: {
                    icon: 'zmdi icon',
                    iconColumns: 'zmdi-view-module',
                    iconDown: 'zmdi-expand-more',
                    iconRefresh: 'zmdi-refresh',
                    iconUp: 'zmdi-expand-less'
                },
                selection: true,
                multiSelect: true,
                rowSelect: true,
                keepSelection: true
            });
                
            //Generate the Calendar
            cId.fullCalendar({
                header: {
                  right: '',
                  center: 'prev, title, next',
                  left: ''
                },
                theme: true, //Do not remove this as it ruin the design
                selectable: true,
                selectHelper: true,
                editable: true,
                //Add Events
                events: [
                    {
                        title: 'Matematika',
                        start: new Date(y, m, 1),
                        allDay: true,
                        className: 'bgm-cyan'
                    },
                    {
                        title: 'Meeting with client',
                        start: new Date(y, m, 10),
                        allDay: true,
                        className: 'bgm-orange'
                    },
                    {
                        title: 'Repeat Event',
                        start: new Date(y, m, 18),
                        allDay: true,
                        className: 'bgm-amber'
                    },
                    {
                        title: 'Semester Exam',
                        start: new Date(y, m, 20),
                        allDay: true,
                        className: 'bgm-green'
                    },
                    {
                        title: 'Soccor match',
                        start: new Date(y, m, 5),
                        allDay: true,
                        className: 'bgm-lightblue'
                    },
                    {
                        title: 'Coffee time',
                        start: new Date(y, m, 21),
                        allDay: true,
                        className: 'bgm-orange'
                    },
                    {
                        title: 'Job Interview',
                        start: new Date(y, m, 5),
                        allDay: true,
                        className: 'bgm-amber'
                    },
                    {
                        title: 'IT Meeting',
                        start: new Date(y, m, 5),
                        allDay: true,
                        className: 'bgm-green'
                    },
                    {
                        title: 'Brunch at Beach',
                        start: new Date(y, m, 1),
                        allDay: true,
                        className: 'bgm-lightblue'
                    },
                    {
                        title: 'Live TV Show',
                        start: new Date(y, m, 15),
                        allDay: true,
                        className: 'bgm-cyan'
                    },
                    {
                        title: 'Software Conference',
                        start: new Date(y, m, 25),
                        allDay: true,
                        className: 'bgm-lightblue'
                    },
                    {
                        title: 'Coffee time',
                        start: new Date(y, m, 30),
                        allDay: true,
                        className: 'bgm-orange'
                    },
                    {
                        title: 'Job Interview',
                        start: new Date(y, m, 30),
                        allDay: true,
                        className: 'bgm-green'
                    },
                ],
                //On Day Select
                select: function(start, end, allDay) {
                    $('#addNew-event').modal('show');   
                    $('#addNew-event input:text').val('');
                    $('#getStart').val(start);
                    $('#getEnd').val(end);
                }
            });

            $('#gunakanCamera').click(function(){
                $(this).attr('class','btn btn-info active');
                $('#unggahFoto').attr('class','btn btn-default');
                Webcam.attach( '#my_camera' );
                $('#take_pics').prop('disabled',false);
                $('#retake_pics').prop('disabled',true);
                $('#my_camera').css('display','block');                      
                $('#imgprev').css('display','none');
                $('#pics_div').css('display','block');
                $('#imgprev').css('visibility','visible');
                $('#upload_foto').css('display','none');
                $('#cek_foto').val('1');
                $('#imgfile').val('');              
                $('.buttonNext').addClass('btn btn-success disabled');
                // $('.buttonNext').removeClass('disabled');
            });
            $('#unggahFoto').click(function(){
                $(this).attr('class','btn btn-info active');
                $('#gunakanCamera').attr('class','btn btn-default');
                Webcam.reset();
                $('#imgfile').css('display','block');
                $('#my_camera').css('display','none');
                $('#cek_foto').val('2');
                $('#pics_div').css('display','none'); 
                $('#imgprev').css('display','block');
                $('#imgprev').attr('src','<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=');
                $('#imgprev').css('visibility','inherit');
                $('#upload_foto').css('display','block');
                $('.buttonNext').removeClass('disabled');
            });
            $('#take_pics').click(function(){
                Webcam.snap( function(data_uri) {
                  $('#imgdata').html(data_uri);
                    console.warn(data_uri);
                    var profil_img = "";
                    var access_token_jwt = "<?php echo $this->session->userdata('access_token_jwt')?>";
                    // console.warn(access_token_jwt);
                    var iduser  = "<?php echo $this->session->userdata('id_user');?>";
                    profil_img = data_uri;
                    profil_img = profil_img.split(";base64,")[1];
                   $.ajax({
                            url: '<?php echo base_url(); ?>V1.0.0/face_exist/access_token/'+access_token_jwt,
                            type: 'POST',
                            data: {
                                image: profil_img
                                
                            },
                            success: function(response)
                            {                       
                                console.warn("test 0 class "+response['message']);
                                // alert("success");
                                if (response['message'] == "face existed") {

                                    $('.buttonNext').addClass('btn btn-success');
                                    $('.buttonNext').removeClass('disabled');
                                }
                                else{
                                    $('#imgdata').html('');
                                    Webcam.unfreeze();
                                    $(this).prop('disabled',true);
                                    $('#take_pics').prop('disabled',false);
                                    $("#modal_foto").modal("show"); 
                                    $('#alert_wajah').css('display','block');
                                    $('#alert_ukuran').css('display','none');
                                    $('.buttonNext').addClass('btn btn-success disabled');
                                    $('.buttonFinish').addClass('btn btn-warning disabled'); 
                                    
                                }    
                            }

                        }); 
                });
                Webcam.freeze();
                $(this).prop('disabled',true);
                $('#retake_pics').prop('disabled',false);
                $('my_camera').css('display','none');
                $('my_camera').css('display','block');
            });
            $('#retake_pics').click(function(){
                $('#imgdata').html('');
                Webcam.unfreeze();
                $(this).prop('disabled',true);
                $('#take_pics').prop('disabled',false);
                $('.buttonNext').addClass('btn btn-success disabled');
            });
            $('#imgfile').change(function(){
                readURL(this);
            });
            /*$('.buttonNext').click(function(){
                if ($('#cek_foto').val()=="1") {
                    alert('Harusnya berhasil');
                $('.buttonFinish').addClass('btn btn-warning disabled');
                return true;     
                }
                else if ($("#imgfile").val() !="" && $('#cek_foto').val()=="2") {
                $("#modal_foto").modal("show"); 
                $('#alert_wajah').css('display','block');
                $('#alert_ukuran').css('display','none');
                $('.buttonNext').addClass('btn btn-success disabled');
                $('.buttonFinish').addClass('btn btn-warning disabled');        
                }
                
            });*/
            function readURL(input){
                var file = document.getElementById('imgfile').files[0];
                var inp = $("#imgfile");


                if(file && file.size < 10000000) { // 10 MB (this size is in bytes)
                    var access_token_jwt = "<?php echo $this->session->userdata('access_token_jwt')?>";
                    // console.warn(access_token_jwt);
                    var iduser  = "<?php echo $this->session->userdata('id_user');?>";
                    var d = new Date();
                    var n = d.getTime();
                    var profil_img = "";
                    var reader = new FileReader();
                    reader.onload = function(e){
                        // $('#imgprev').attr('src', e.target.result);
                        $('#imgdata').html(e.target.result);
                        profil_img = e.target.result;
                        profil_img = profil_img.split(";base64,")[1];
                        // console.warn(profil_img);
                        $.ajax({
                            url: '<?php echo base_url(); ?>V1.0.0/face_exist/access_token/'+access_token_jwt,
                            type: 'POST',
                            data: {
                                image: profil_img
                                
                            },
                            success: function(response)
                            {                       
                                console.warn("test 0 class "+response['message']);
                                // alert("success");
                                if (response['message'] == "face existed") {
                                    var reader = new FileReader();
                                    reader.onload = function(e){
                                    $('#imgprev').attr('src', e.target.result);
                                    $('#imgdata').html(e.target.result);
                                    $('#imgprev').css('visibility','visible');
                                    $('#imgprev').css('display','block');
                                    $('.buttonNext').addClass('btn btn-success');
                                    $('.buttonNext').removeClass('disabled');
                                  }  
                                  reader.readAsDataURL(input.files[0]); 
                                }
                                else{
                                    var $el = $('#imgfile');
                                    $el.wrap('<form>').closest('form').get(0).reset();
                                    $el.unwrap();
                                    $("#modal_foto").modal("show"); 
                                    $('#alert_wajah').css('display','block');
                                    $('#alert_ukuran').css('display','none');
                                    $('.buttonNext').addClass('btn btn-success disabled');
                                    $('.buttonFinish').addClass('btn btn-warning disabled'); 
                                    
                                }    
                            }

                        }); 
                        // alert("gagal");
                        $('#imgprev').css('visibility','visible');
                        $('#imgprev').css('display','block');
                    }  
                    reader.readAsDataURL(input.files[0]); 
                    
                        
                } else {
                    //Prevent default and display error                      
                    var $el = $('#imgfile');
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();
                    $("#modal_foto").modal("show");
                    $('#alert_wajah').css('display','none');
                    $('#alert_ukuran').css('display','block'); 

                }
            }
            //Create and ddd Action button with dropdown in Calendar header. 
            var actionMenu = '<ul class="actions actions-alt" id="fc-actions">' +
            '<li class="dropdown">' +
            '<a href="" data-toggle="dropdown"><i class="zmdi zmdi-more-vert"></i></a>' +
            '<ul class="dropdown-menu dropdown-menu-right">' +
            '<li class="active">' +
            '<a data-view="month" href="">Month View</a>' +
            '</li>' +                                            
            '<li>' +
            '<a data-view="basicWeek" href="">Week View</a>' +
            '</li>' +
            '<li>' +                                            
            '<a data-view="agendaWeek" href="">Agenda Week View</a>' +
            '</li>' +                                            
            '<li>' +
            '<a data-view="basicDay" href="">Day View</a>' +
            '</li>' +
            '<li>' +
            '<a data-view="agendaDay" href="">Agenda Day View</a>' +
            '</li>' +
            '</ul>' +
            '</div>' +
            '</li>';
            cId.find('.fc-toolbar').append(actionMenu);
            //Event Tag Selector
            (function(){
                $('body').on('click', '.event-tag > span', function(){
                  $('.event-tag > span').removeClass('selected');
                  $(this).addClass('selected');
                });
            })();

            //Add new Event
            $('body').on('click', '#addEvent', function(){
                var eventName = $('#eventName').val();
                var tagColor = $('.event-tag > span.selected').attr('data-tag');
                if (eventName != '') {
                    //Render Event
                    $('#calendar').fullCalendar('renderEvent',{
                        title: eventName,
                        start: $('#getStart').val(),
                        end:  $('#getEnd').val(),
                        className: tagColor
                    },true ); //Stick the event
                    $('#addNew-event form')[0].reset()
                    $('#addNew-event').modal('hide');
                }
                else {
                    $('#eventName').closest('.form-group').addClass('has-error');
                }
            });   
            //Calendar views
            $('body').on('click', '#fc-actions [data-view]', function(e){
                e.preventDefault();
                var dataView = $(this).attr('data-view');
                $('#fc-actions li').removeClass('active');
                $(this).parent().addClass('active');
                cId.fullCalendar('changeView', dataView);  
            });
            //Basic Example
            $("#data-table-basic").bootgrid({
                css: {
                    icon: 'zmdi icon',
                    iconColumns: 'zmdi-view-module',
                    iconDown: 'zmdi-expand-more',
                    iconRefresh: 'zmdi-refresh',
                    iconUp: 'zmdi-expand-less'
                },
            });     
            //Selection
            $("#data-table-selection").bootgrid({
                css: {
                    icon: 'zmdi icon',
                    iconColumns: 'zmdi-view-module',
                    iconDown: 'zmdi-expand-more',
                    iconRefresh: 'zmdi-refresh',
                    iconUp: 'zmdi-expand-less'
                },
                selection: true,
                multiSelect: true,
                rowSelect: true,
                keepSelection: true
            });    
            //Command Buttons
            $("#data-table-command").bootgrid({
              css: {
                  icon: 'zmdi icon',
                  iconColumns: 'zmdi-view-module',
                  iconDown: 'zmdi-expand-more',
                  iconRefresh: 'zmdi-refresh',
                  iconUp: 'zmdi-expand-less'
              },
              formatters: {
                  "commands": function(column, row) {
                    return "<button type=\"button\" class=\"btn btn-icon command-edit waves-effect waves-circle\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-edit\"></span></button> " + 
                    "<button type=\"button\" class=\"btn btn-icon command-delete waves-effect waves-circle\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-delete\"></span></button>";
                  }
              }
            });
        });
    </script>
    <script>
        function notify(from, align, icon, type, animIn, animOut, message){
            $.growl({
                icon: icon,
                title: ' ',
                message: message,
                url: ''
            },{
                element: 'body',
                type: type,
                allow_dismiss: true,
                placement: {
                from: from,
                align: align
            },
            offset: {
                x: 20,
                y: 85
            },
            spacing: 10,
            z_index: 1031,
            delay: 5000,
            timer: 1000,
            url_target: '_blank',
            mouse_over: false,
            animate: {
                enter: animIn,
                exit: animOut
            },
            icon_type: 'class',
            template: '<div data-growl="container" class="alert" role="alert" style="z-index:99;">' +
            '<button type="button" class="close" data-growl="dismiss">' +
            '<span aria-hidden="true">&times;</span>' +
            '<span class="sr-only">Close</span>' +
            '</button>' +
            '<span data-growl="icon"></span>' +
            '<span data-growl="title"></span>' +
            '<span data-growl="message"></span>' +
            '<a href="#" data-growl="url"></a>' +
            '</div>'
            });
        };
    </script>

    <script type="text/javascript">
        $.extend({
            getUrlVars: function(){
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for(var i = 0; i < hashes.length; i++)
                {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                return vars;
                },
                getUrlVar: function(name){
                return $.getUrlVars()[name];
            }
        });

        $(document).on("click", ".ckstj", function () { 
            var request_id = $(".ckstj").attr('reqid');
            var id_user = $(".ckstj").attr('ids');
            var tokenjwt = "no";
            $.ajax({
                url: '<?php echo base_url();?>Rest/approve_ondemandgroup/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    request_id: request_id,
                    id_user : id_user
                },               
                success: function(data)
                {                    
                    $("#btnappr").css('display','none');
                    if (data['status'] == true) 
                    {
                        $("#alertsuccessappr").css('display','block');
                        setTimeout(function(){
                            $("#modalappr").modal('hide');
                            window.location.replace("<?php echo base_url();?>");
                        },5000);                            
                    }
                    else
                    {                            
                        $("#alertfailedappr").css('display','block');
                        setTimeout(function(){
                            $("#modalappr").modal('hide');
                            window.location.replace("<?php echo base_url();?>");
                        },5000);   
                    }
                }
            });
        });

        $(document).on("click", ".ckrjt", function () { 
            var request_id = $(".ckrjt").attr('reqid');
            var id_user = $(".ckrjt").attr('ids');
            var tokenjwt = "no";
            $.ajax({
                url: '<?php echo base_url();?>Rest/approve_ondemandgroup/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    request_id: requestid,
                    id_user : id_user,
                    answer: "t"
                },               
                success: function(data)
                {                  

                    if (data['status'] == true) 
                    {
                        $("#alertinforjjt").css('display','none');
                        $("#alertfailedrjjt").css('display','none');
                        $("#alertsuccessrjjt").css('display','block');
                        setTimeout(function(){
                            $("#modalappr").modal('hide');
                            window.location.replace("<?php echo base_url();?>");
                        },5000); 
                    }
                    else
                    {
                        $("#alertinforjjt").css('display','none');
                        $("#alertsuccessrjjt").css('display','none');
                        $("#alertfailedrjjt").css('display','block');
                        setTimeout(function(){
                            $("#modalappr").modal('hide');
                            window.location.replace("<?php echo base_url();?>");
                        },5000);
                    }
                }
            });
        });

        if ($.getUrlVar("ca") != null) {
            var code        = $.getUrlVar("ca");
            var requestid   = $.getUrlVar("reqid");
            var id_user     = $.getUrlVar("ids");
            var type        = $.getUrlVar("type");
            var user_utc = new Date().getTimezoneOffset();
            user_utc = -1 * user_utc;
            if (code == "appr") {             
                $.ajax({
                    url: '<?php echo base_url();?>Process/showDetailClass',
                    type: 'POST',
                    data: {
                        request_id: requestid,
                        type:type,
                        id_user:id_user,
                        user_utc:user_utc
                    },
                    success: function(response)
                    {                       
                        var response = JSON.parse(response);
                        if (response['approve'] == 0) {                                  
                            if (response['statusapprove'] == 0) {                          
                                var name_tutor   = response['name_tutor'];
                                var nama_pengajak= response['nama_pengajak'];
                                var subject_name= response['subject_name'];
                                var waktu       = response['waktu'];
                                var durasi      = response['durasi']+" jam";
                                var kotak       = "<tr><td>"+nama_pengajak+"</td><td>"+name_tutor+"</td><td>"+subject_name+"</td><td>"+waktu+"</td><td>"+durasi+"</td></tr>";
                                $("#kotakdetailkelas").append(kotak);
                                $(".ckstj").attr('reqid',requestid);
                                $(".ckstj").attr('ids',id_user);
                                $("#modalappr").modal('show');    
                            }                                
                            else
                            {
                                var name_tutor   = response['name_tutor'];
                                var nama_pengajak= response['nama_pengajak'];
                                var subject_name= response['subject_name'];
                                var waktu       = response['waktu'];
                                var durasi      = response['durasi']+" jam";
                                var kotak       = "<tr><td>"+nama_pengajak+"</td><td>"+name_tutor+"</td><td>"+subject_name+"</td><td>"+waktu+"</td><td>"+durasi+"</td></tr>";
                                $("#kotakdetailkelas").append(kotak);
                                $("#btnappr").css('display','none');
                                $("#alertfailedappr").css('display','none');
                                $("#alertsuccessappr").css('display','none');
                                $("#alertinfoappr").css('display','block');                                    
                                $("#modalappr").modal('show');  
                                setTimeout(function(){
                                    $("#modalappr").modal('hide');
                                    window.location.replace("<?php echo base_url();?>");
                                },5000);    
                            }
                        }
                        else if (response['approve'] == -1) {                          
                            var name_tutor   = response['name_tutor'];
                            var nama_pengajak= response['nama_pengajak'];
                            var subject_name= response['subject_name'];
                            var waktu       = response['waktu'];
                            var durasi      = response['durasi']+" jam";
                            var kotak       = "<tr><td>"+nama_pengajak+"</td><td>"+name_tutor+"</td><td>"+subject_name+"</td><td>"+waktu+"</td><td>"+durasi+"</td></tr>";
                            $("#kotakdetailkelas").append(kotak);
                            $(".ckstj").css('display','none');
                            $("#alertfailedappr").css('display','none');
                            $("#alertsuccessappr").css('display','none');
                            $("#alertinfoappr").css('display','none');
                            $("#alertinforejectappr").css('display','block');
                            $("#modalappr").modal('show');    
                        }
                        else
                        {
                            $("#modalappr").modal('hide');
                        }
                    }
                });    
                
            }
            else if (code == "rjjt") {
                $.ajax({
                    url: '<?php echo base_url();?>Process/showDetailClass',
                    type: 'POST',
                    data: {
                        request_id: requestid,
                        type:type,
                        user_utc:user_utc
                    },
                    success: function(response)
                    { 
                        var response = JSON.parse(response);
                        if (response['status'] == 1) {
                            if (response['approve'] == 0) {                            
                                var name_tutor   = response['name_tutor'];
                                var nama_pengajak= response['nama_pengajak'];
                                var subject_name= response['subject_name'];
                                var waktu       = response['waktu'];
                                var durasi      = response['durasi']+" jam";
                                var kotak       = "<tr><td>"+nama_pengajak+"</td><td>"+name_tutor+"</td><td>"+subject_name+"</td><td>"+waktu+"</td><td>"+durasi+"</td></tr>";
                                $("#kotakdetailkelasrjjt").append(kotak);
                                $(".ckrjt").attr('reqid',requestid);
                                $(".ckrjt").attr('ids',id_user);
                                $("#modalrjjt").modal('show');    
                            }
                            else{        
                                var name_tutor   = response['name_tutor'];
                                var nama_pengajak= response['nama_pengajak'];
                                var subject_name= response['subject_name'];
                                var waktu       = response['waktu'];
                                var durasi      = response['durasi']+" jam";
                                var kotak       = "<tr><td>"+nama_pengajak+"</td><td>"+name_tutor+"</td><td>"+subject_name+"</td><td>"+waktu+"</td><td>"+durasi+"</td></tr>";
                                $("#kotakdetailkelasrjjt").append(kotak);
                                $("#alertfailedrjjt").css('display','none');
                                $("#alertsuccessrjjt").css('display','none');
                                $("#alertinforjjt").css('display','block');
                                $("#btnappr").css('display','none');
                                $("#modalrjjt").modal('show'); 
                                setTimeout(function(){
                                    window.location.replace("<?php echo base_url();?>");
                                },5000);    
                            }
                        }
                        else
                        {
                            var name_tutor   = response['name_tutor'];
                            var nama_pengajak= response['nama_pengajak'];
                            var subject_name= response['subject_name'];
                            var waktu       = response['waktu'];
                            var durasi      = response['durasi']+" jam";
                            var kotak       = "<tr><td>"+nama_pengajak+"</td><td>"+name_tutor+"</td><td>"+subject_name+"</td><td>"+waktu+"</td><td>"+durasi+"</td></tr>";
                            $("#kotakdetailkelasrjjt").append(kotak);
                            $(".ckrjt").attr('reqid',requestid);
                            $(".ckrjt").attr('ids',id_user);
                            $("#alertinforjjt").css('display','none');
                            $("#alertsuccessrjjt").css('display','none');
                            $("#alertfailedrjjt").css('display','block');
                            $(".ckrjt").css('display','none');
                            $("#modalrjjt").modal('show'); 
                            setTimeout(function(){
                                window.location.replace("<?php echo base_url();?>");
                            },5000);    
                        }
                    }
                });  
            }
            else
            {
                setTimeout(function(){                        
                    window.location.replace("<?php echo base_url();?>");
                },3000); 
            }
        }
        else
        {

        }
    </script>   

    <script type="text/javascript">
        <?php
          $myip = $_SERVER['REMOTE_ADDR'];
        ?>
        var ip = "<?php echo $myip;?>";    
        // alert(myip);
        var geo_locationn = "";
        var ceklang = "<?php echo $this->session->userdata('lang');?>";    

        // var ip = '125.161.131.185';
        var access_key = '2705cc5551f25dc021c878caf93f1449';
          $.ajax({
              url: 'https://api.ipstack.com/' + ip + '?access_key=' + access_key,   
              dataType: 'jsonp',
              success: function(geo) {
                  var a = JSON.stringify(geo); 
                  // alert(a);
                  // output the "capital" object inside "location"
                  // alert(geo['country_name']);
                  geo_locationn = geo['country_code'];
                  // alert(geo_locationn);
                  // alert(ceklang);

                  // buka();
                  
                  
              }
          });  
            // ip_info("Visitor", "Country Code");
            var width = $(window).width();
            

            var ceklanguage = "<?php echo $this->session->userdata('ceklanguage');?>";
            var ceklocation = "<?php echo $this->session->userdata('get_location');?>";
            // alert(ceklocation);
            // alert(geo_location);    
            // $.get("http://www.geoplugin.net/json.gp", function (response) {
                // geo_location = geoplugin_countryCode();          
    </script>
    

    <?php
        if($this->session->userdata('usertype_id') == 'student'){
            // $this->load->view('inc/mdl_rating');
        }
    ?>

    <script type="text/javascript">
      	$(document).ready(function() {
	        var id_user = "<?php echo $this->session->userdata('id_user');?>";
	        var user_utc = new Date().getTimezoneOffset();
	            user_utc = -1 * user_utc;
	        var program_id    = null;
	        var channel_id    = null;
	        var program_name  = null;
	        var status        = null;
	        var created_at    = null;
	        var eventDate     = null;
	        var eventTime     = null;
	        var eventDurasi   = null;
	        var tutor_id      = null;
	        var subject_id    = null;
	        var topik         = null;
	        var eventstatus   = null;
	        var jenjangnamelevel = null;
	        var subject_name  = null;
	        var channel_name  = null;
	        var channel_logo  = null;
	        $.ajax({
	            url: 'https://classmiles.com/air/v1/requestClassTutor/',
	            type: 'POST',       
	            data: {
	                tutor_id: id_user
	            },         
	            success: function(response)
	            {           
		            console.log(response);
		            if (response.data == null) {
		            }
		            else
		            {
		              	if (response.data['tutor_id'] == id_user) {
		                    $("#modal_approve_tutor").modal('show');                                        
		                  
		                    var program_id = response.data['program_id'];
		                    var channel_id = response.data['channel_id'];
		                    var program_name = response.data['program_name'];
		                    var status = response.data['status'];
		                    var created_at = response.data['created_at'];
		                    var eventDate = response.data['eventDate'];
		                    var eventTime = response.data['eventTime'];
		                    var eventDurasi = response.data['eventDurasi'];
		                    var hours = parseInt( eventDurasi / 3600 );
		                    var minutes = parseInt( (eventDurasi - (hours * 3600)) / 60 );
		                    var seconds = Math.floor((eventDurasi - ((hours * 3600) + (minutes * 60))));
		                    if (hours == 0) {
		                      	var durasi = (minutes < 10 ? "0" + minutes : minutes) + " Menit";   
		                    }
		                    else if (minutes == 0) {
		                      	var durasi = (hours < 10 ? "" + hours : hours) + " Jam ";
		                    }
		                    else{
		                      	var durasi = (hours < 10 ? "" + hours : hours) + " Jam " + (minutes < 10 ? "0" + minutes : minutes) + " Menit"; 
		                    }
		                     
		                    var tutor_id = response.data['tutor_id'];
		                    var subject_id = response.data['subject_id'];
		                    var topik = response.data['topik'];
		                    var eventstatus = response.data['eventstatus'];
		                    var jenjangnamelevel = response.data['jenjangnamelevel'];
		                    var subject_name = response.data['subject_name'];
		                    var channel_name = response.data['channel_name'];
		                    var channel_logo = response.data['channel_logo'];                        

		                    $("#nama_program").html(program_name);
		                    $("#nama_channel").html(channel_name);
		                    $("#waktu_permintaan").html(eventDate+" "+eventTime);
		                    $("#mata_pelajaran").html(subject_name);
		                    $("#topik").html(topik);
		                    $("#durasi_kelas").html(durasi);
		                    $("#logo_channel").attr('src',"<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+channel_logo);

		                    $('.reject_req').click(function(){
		                        $.ajax({
		                            url: 'https://classmiles.com/air/v1/reject_RequestFromTutor/',
		                            type: 'POST',       
		                            data: {
            										tutor_id: tutor_id,
            										program_id: program_id,
            										channel_id: channel_id,
            										eventDate: eventDate,
            										eventTime: eventTime,
            										eventDurasi: eventDurasi
		                            },         
		                            success: function(response)
		                            {
		                                var a = JSON.stringify(response); 
		                                // alert(a);
		                                // alert("Berhasil reject");
		                                $("#modal_approve_tutor").modal('hide');                
		                                $("#modal_alert").modal('show');
		                                $("#modal_konten").css('background-color','#32c787');
		                                $("#text_modal").html("Sukses, Anda telah MENOLAK permintaan kelas, dan akan kami sampaikan kepada Admin Channel");
		                                $("#button_ok").click( function(){
		                                    location.reload();
		                                });
		                            }
		                        });
		                    });

                        $('.approve_req').click(function(){                              
                            $.ajax({
                                url: 'https://classmiles.com/air/v1/accept_RequestFromTutor/',
                                type: 'POST',       
                                data: {
                                    tutor_id: tutor_id,
                                    program_id: program_id,
                                    channel_id: channel_id,
                                    subject_id: subject_id,
                                    eventDate: eventDate,
                                    eventTime: eventTime,
                                    eventDurasi: eventDurasi,
                                    user_utc: user_utc

                                },         
                                success: function(response)
                                {
              										var a = JSON.stringify(response); 
              										if (response['code'] == "200") {
              											$("#modal_approve_tutor").modal('hide');
              											$("#modal_alert").modal('show');
              											$("#modal_konten").css('background-color','#32c787');
              											$("#text_modal").html("Sukses, persetujuan Anda akan kami sampaikan kepada Admin Channel");
              											$("#button_ok").click( function(){
              												location.reload();
              											});
              										}
              										else if(response['code'] == "403"){
              											$("#modal_approve_tutor").modal('hide');
              											$("#modal_alert").modal('show');
              											$("#modal_konten").css('background-color','red');
              											$("#text_modal").html("Mohon maaf, kelas tersebut bentrok dengan jadwal anda yang lain.");
              											$("#button_ok").click( function(){
              												location.reload();
              											});
              										}
              										else if(response['code'] == "-100"){
              											$("#modal_approve_tutor").modal('hide');
              											$("#modal_alert").modal('show');
              											$("#modal_konten").css('background-color','red');
              											$("#text_modal").html("Mohon maaf, terjadi kesalahan.");
              											$("#button_ok").click( function(){
              												location.reload();
              											});   
              										}
              										else if(response['code'] == "-400"){
              											$("#modal_approve_tutor").modal('hide');
              											$("#modal_alert").modal('show');
              											$("#modal_konten").css('background-color','red');
              											$("#text_modal").html("Mohon maaf, terjadi kesalahan.");
              											$("#button_ok").click( function(){
              												location.reload();
              											}); 
              										}
              										else
              										{
              											$("#modal_approve_tutor").modal('hide');
              											$("#modal_alert").modal('show');
              											$("#modal_konten").css('background-color','red');
              											$("#text_modal").html("Mohon maaf, terjadi kesalahan.");
              											$("#button_ok").click( function(){
              												location.reload();
              											}); 
              										}
		                            }
		                        });
		                    });
		                }
		            }
		          }
	     	  });

  	        setTimeout(function(){

  	        },5000); 
      	});

      	$(document).ready(function() {
          $('ul.list_username .anak').click(function(e) {

              var id_user = this.id;
              // alert(id_user);
              // return false;
              // $("."+id_user).css('display','block');
             
              // alert(new_id_user);
              $('#id_user').val(id_user);
              $.ajax({
                  url: '<?php echo base_url(); ?>Master/change_user',
                  type: 'POST',
                  data: {
                      id_user : id_user
                  },               
                  success: function(data){                    
                      location.href = "<?php base_url();?>/"
                  }
              });
          });
      });
      var code_cek = "<?php echo $this->session->userdata('code_cek');?>";    
      
      var code = "<?php echo $this->session->userdata('code');?>";  
      var ubah_pass = "<?php echo $this->session->userdata('ubah_pass');?>";  
      var id_user_kids = "<?php echo $this->session->userdata('id_user_kids');?>";     
      var id_user = "<?php echo $this->session->userdata('id_user');?>";     
      var jenjang_id = "<?php echo $this->session->userdata('jenjang_id');?>";     
      var jenjang_id_kids = "<?php echo $this->session->userdata('jenjang_id_kids');?>";     
      var pathname = window.location.pathname;        
      var cek_update = "<?php echo $this->session->userdata('update_jenjang');?>";

      if (id_user_kids != id_user) {
      }
      else{

      //   if (jenjang_id == 15 && pathname == '/Profile-School'  && jenjang_id_kids =="") {
      //     window.location.replace('<?php echo base_url();?>');
      //   }
      //   if(code_cek=='777' && pathname != '/Profile-School'){
      //     $("#upd_jenjang_modal").modal('show');      
      //   }
      //   else if (code_cek=='888' && pathname != '/Profile-School') {
      //       window.location.replace('<?php echo base_url();?>Profile-School');
      //   }
      //   else if(code_cek=='889' && pathname != '/About'){
      //       window.location.replace('<?php echo base_url();?>About');     

      //   }
      // }

      $(document.body).on('click', '.btn_updatejenjang' ,function(e){
          window.location.replace('<?php echo base_url();?>Profile-School');
      });
      if (cek_update == 1) {
          $("#mdl_upd_failed").modal('hide');
          $("#mdl_upd_success").modal('show');
          $("#alert_updatejenjang").modal('hide');
      }
      else  if (cek_update == 0) {
          // $("#mdl_upd_failed").modal('show');
          // $("#mdl_upd_success").modal('hide');
          // $("#alert_updatejenjang").modal('hide');
      }
      if (code == "9696" ) {
        $("#mdl_profil_success").modal('show');
      }else if (code == "9797" ) {
        $("#mdl_profil_failed").modal('show');
      }else if (code == "9898" ) {
        $("#mdl_password_failed").modal('show');
      }else if (code == "9999" ) {
        $("#mdl_oldpassword_failed").modal('show');
      }

      $(document.body).on('click', '.btn_oke' ,function(e){
          $.ajax({
              url: '<?php echo base_url(); ?>Rest/clearsession',
              type: 'POST',
              success: function(response)
              { 
                  console.warn(response);
              }
          });
      });
    </script>    

    <!-- GET TUTOR APPROVAL REQUEST -->
    <script type="text/javascript">
        
        var tutor_id = "<?php echo $this->session->userdata('id_user');?>";
        var usertype = "<?php echo $this->session->userdata('usertype_id');?>";
        var user_utc = new Date().getTimezoneOffset();
            user_utc = -1 * user_utc;
        
        if (usertype == 'tutor' ) {
            $(document).ready(function() {

                function capitalizeFirstLetter(string) {
                    return string.charAt(0).toUpperCase() + string.slice(1);
                }

                function secondsToTime(secs)
      			    {
      			        var hours = Math.floor(secs / (60 * 60));
      			       
      			        var divisor_for_minutes = secs % (60 * 60);
      			        var minutes = Math.floor(divisor_for_minutes / 60);
      			     
      			        var divisor_for_seconds = divisor_for_minutes % 60;
      			        var seconds = Math.ceil(divisor_for_seconds);
      			       
      			        var obj = {
      			            "h": hours,
      			            "m": minutes,
      			            "s": seconds
      			        };
      			        return obj;
      			    }
                $.ajax({
                        url: '<?php echo BASE_URL();?>Rest/listRequestApproval_toTutor',
                        type: 'POST',
                        data: {
                            tutor_id : tutor_id,
                            user_utc : user_utc
                        },
                        success: function(response)
                        {
                            if (response['code'] == 200) {
                                var request_id        = response['data']['request_id'];
                                var user_image        = "<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+response['data']['user_image'];
                                var user_name         = response['data']['user_name'];
                                var id_user_requester = response['data']['id_user_requester'];
                                var subject_id        = response['data']['subject_id'];
                                var topic             = response['data']['topic'];
                                var date_requested    = response['data']['date_requested'];
                                var duration_requested= response['data']['duration_requested'];
                                var type              = response['data']['type'];
                                var subject_name      = response['data']['subject_name'];
                                var jenjang_name      = response['data']['jenjang_name'];
                                var jenjang_level     = response['data']['jenjang_level'];
                                var icon              = "<?php echo CDN_URL.STATIC_IMAGE_CDN_URL.'class_icon/'?>"+response['data']['icon'];
                                var a = secondsToTime(duration_requested);
                                var b = a['h'];
                                if (b == 0) {
                                  var jam ="";
                                }
                                else{
                                  var jam =b+" Jam ";
                                }
                                var c = a['m'];
                                if(c == 0){
                                  var menit ="";
                                }
                                else{
                                  var menit = c+" Menit ";
                                }
                                var d = a['s'];
                                if (d == 0){
                                  var detik ="";
                                }
                                else{
                                  var detik =d+" Menit ";
                                }
                                $("#app_user_image").attr('src',user_image);
                                $("#app_typerequest").html(capitalizeFirstLetter(type));
                                $("#app_studentname").html(capitalizeFirstLetter(user_name));
                                $("#app_timerequest").html(date_requested);
                                $("#app_subject").html(subject_name+' '+jenjang_name+' '+jenjang_level);
                                $("#app_topik").html(topic);
                                $("#app_durasi").html(jam+''+menit+''+detik);
                                if (type == "private") {
                                  $(".btn-choose-demand").attr('demand-link','<?php echo BASE_URL();?>process/approve_demand?request_id='+request_id);
                                  $(".btn-reject-demand").attr('demand-link','<?php echo BASE_URL();?>process/reject_demand?request_id='+request_id);  
                                }
                                else if(type == "group"){
                                  $(".btn-choose-demand").attr('demand-link','<?php echo BASE_URL();?>process/approve_demand_group?request_id='+request_id);
                                  $(".btn-reject-demand").attr('demand-link','<?php echo BASE_URL();?>process/reject_demand_group?request_id='+request_id);  
                                }
                                else{
                                  $(".btn-choose-demand").attr('');
                                  $(".btn-reject-demand").attr('');  
                                }
                                
                                $("#modalApproveToTutor").modal("show");
                            }
                            else
                            {

                            }                        
                        }
                    });	
                function cekRequest(){
                    $.ajax({
                        url: '<?php echo BASE_URL();?>Rest/listRequestApproval_toTutor',
                        type: 'POST',
                        data: {
                            tutor_id : tutor_id,
                            user_utc : user_utc
                        },
                        success: function(response)
                        {
                            if (response['code'] == 200) {
                                var request_id        = response['data']['request_id'];
                                var user_image        = "<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+response['data']['user_image'];
                                var user_name         = response['data']['user_name'];
                                var id_user_requester = response['data']['id_user_requester'];
                                var subject_id        = response['data']['subject_id'];
                                var topic             = response['data']['topic'];
                                var date_requested    = response['data']['date_requested'];
                                var duration_requested= response['data']['duration_requested'];
                                var type              = response['data']['type'];
                                var subject_name      = response['data']['subject_name'];
                                var jenjang_name      = response['data']['jenjang_name'];
                                var jenjang_level     = response['data']['jenjang_level'];
                                var icon              = "<?php echo CDN_URL.STATIC_IMAGE_CDN_URL.'class_icon/'?>"+response['data']['icon'];
                                var a = secondsToTime(duration_requested);
                                var b = a['h'];
                                if (b == 0) {
                                  var jam ="";
                                }
                                else{
                                  var jam =b+" Jam ";
                                }
                                var c = a['m'];
                                if(c == 0){
                                  var menit ="";
                                }
                                else{
                                  var menit = c+" Menit ";
                                }
                                var d = a['s'];
                                if (d == 0){
                                  var detik ="";
                                }
                                else{
                                  var detik =d+" Menit ";
                                }
                                $("#app_user_image").attr('src',user_image);
                                $("#app_typerequest").html(capitalizeFirstLetter(type));
                                $("#app_studentname").html(capitalizeFirstLetter(user_name));
                                $("#app_timerequest").html(date_requested);
                                $("#app_subject").html(subject_name+' '+jenjang_name+' '+jenjang_level);
                                $("#app_topik").html(topic);
                                $("#app_durasi").html(jam+''+menit+''+detik);
                                if (type == "private") {
                                  $(".btn-choose-demand").attr('demand-link','<?php echo BASE_URL();?>process/approve_demand?request_id='+request_id);
                                  $(".btn-reject-demand").attr('demand-link','<?php echo BASE_URL();?>process/reject_demand?request_id='+request_id);  
                                }
                                else if(type == "group"){
                                  $(".btn-choose-demand").attr('demand-link','<?php echo BASE_URL();?>process/approve_demand_group?request_id='+request_id);
                                  $(".btn-reject-demand").attr('demand-link','<?php echo BASE_URL();?>process/reject_demand_group?request_id='+request_id);  
                                }
                                else{
                                  $(".btn-choose-demand").attr('');
                                  $(".btn-reject-demand").attr('');  
                                }
                                
                                $("#modalApproveToTutor").modal("show");
                            }
                            else
                            {

                            }                        
                        }
                    });

                };	    

                


                $(".btn-choose-demand").click(function(){
                  $("#modalApproveToTutor").modal("hide");
                  
                	setTimeout(function(){
                		cekRequest();
                	},2000);
                });

                $(".btn-reject-demand").click(function(){
                  $("#modalApproveToTutor").modal("hide");
                  
                  setTimeout(function(){
                    cekRequest();
                  },2000);
                });
            });
        }
        else if(usertype == 'student' || usertype == 'student kid'){
          var id_user = "<?php echo $this->session->userdata('id_user');?>";
          var user_utc = new Date().getTimezoneOffset();  
              user_utc = -1 * user_utc;
          var access_token_jwt = "<?php echo $this->session->userdata('access_token_jwt')?>";
          var count=null;
          $.ajax({
              url: '<?php echo base_url(); ?>Rest/showInvoiceToStudent/access_token/'+access_token_jwt,
              type: 'POST',
              data: {
                  id_user: id_user,
                  user_utc : user_utc
              },
              success: function (response) {
                var pathname = window.location.pathname;
                if (pathname != "/DetailOrder") {
                  console.warn(response);
                  if (response['status'] == false){
                    $(".shownotif").css('display','none');
                  }
                  else{
                    for (var i = 0; i < response.data.length; i++) {
                      count++;
                      $("#modalShowClassApproved").modal('show');
                      var order_id      = response['data'][i]['order_id'];
                      var buyer_id      = response['data'][i]['buyer_id'];
                      var invoice_id      = response['data'][i]['invoice_id'];
                      var order_status      = response['data'][i]['order_status'];
                      var payment_due      = response['data'][i]['payment_due'];

                      var order_detail      = response['data'][i]['order_detail'];
                      var link = window.location.origin+"/DetailOrder?order_id="+order_id;
                      for (var x = 0; x < order_detail.length; x++) {
                          var tutor_name = order_detail[x]['tutor_name'];
                          var subject_name = order_detail[x]['subject_name'];
                          var subject_name = order_detail[x]['subject_name'];
                          var date_requested = order_detail[x]['date_requested'];
                          var tutor_image = order_detail[x]['tutor_image'];
                          var topic = order_detail[x]['topic'];
                          var type = order_detail[x]['type'];
                      }
                      $("#txt_type_class").html(type);
                      $("#txt_name_tutor").html(tutor_name);
                      $("#txt_req_time").html(date_requested);
                      $("#txt_subject").html(subject_name);
                      $("#txt_topic").html(topic);
                      $("#gotoinvoice").attr('href', link);
                      $("#txt_image_tutor").attr('src','<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>'+tutor_image);
                  }
                  $(".shownotif").css('display','block');
                  $(".shownotif").text(count);
                  }
                }
              }
          });
          $.ajax({
              url: '<?php echo base_url(); ?>Rest/myInvoice/access_token/'+access_token_jwt,
              type: 'POST',
              data: {
                  id_user: id_user,
                  user_utc : user_utc
              },
              success: function (response) {
                
              }
          });
        }
        

    </script>
<script type="text/javascript">
    $(document).ready(function(){
            var code_cek = null;
            var status_login = "<?php echo $this->session->userdata('status_user');?>";
            var cekk = setInterval(function(){
                code_cek = "<?php echo $this->session->userdata('code_cek');?>";
                
                cek();
            },500);
            // alert('aasdfsd');
            function cek(){
                // if (code_cek == "889") {
                //     $("#complete_modal").modal('show');
                //     $("#alertcompletedata").css('display','block');
                //     $("#alertcomplete_school").css('display','none');
                //     $(".edit_profile").css('display','block');
                //     $(".show_profile").css('display','none');
                //     $("#btn_edit_username").css('display','none');
                //     $("#btn_save_username").css('display','none');
                    
                //     code_cek == null;
                //     clearInterval(cekk);
                //     $.ajax({
                //         url: '<?php echo base_url(); ?>Rest/clearsession',
                //         type: 'POST',
                //         data: {
                //             cek_code: code
                //         },
                //         success: function(response)
                //         { 
                //             console.warn(response);
                //         }
                //     });
                // }
                // if (code_cek == "888") {
                //     $("#complete_modal").modal('show');
                //     $("#alertcomplete_school").css('display','block');
                //     $('#alertcompletedata').css('display','hide');
                //     code_cek == null;
                //     clearInterval(cekk);
                //     $.ajax({
                //         url: '<?php echo base_url(); ?>Rest/clearsession',
                //         type: 'POST',
                //         data: {
                //             cek_code: code
                //         },
                //         success: function(response)
                //         { 
                //             console.warn(response);
                //         }
                //     });
                // }
                // console.warn(code_cek);
                // console.warn(status_login);
            }

            
        });
</script>


