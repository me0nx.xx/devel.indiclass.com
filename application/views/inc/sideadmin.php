<div class="profile-menu">
    <a href="">
        <div class="profile-pic">
            <img src="<?php echo $this->session->userdata('user_image');?>" > 
        </div>

        <div class="profile-info">

            <?php 
                $idadmin = $this->session->userdata('id_user');
                $cek     = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$idadmin'")->result_array();
                foreach ($cek as $row => $a) {
                    echo $a['user_name'];
                }
            ?>        
        
            <i class="zmdi zmdi-caret-down"></i>
        </div>
    </a>

    <ul class="main-menu">
        <li>
            <a href="<?php echo base_url('/logout'); ?>"><i class="zmdi zmdi-time-restore"></i> <?php echo $this->lang->line('logout'); ?></a>
        </li>
    </ul>
</div>
<ul class="main-menu">
    <?php
        $usertype = $this->session->userdata('usertype_id');
        if ($usertype == "tutor_approval") {
            ?>
            <li class="    
                <?php if($sideactive=="homeadmin"){ echo "active";} else{            
                } ?>"> <a href="<?php echo base_url(); ?>admin"><i class="zmdi zmdi-home"></i>  <?php echo $this->lang->line('homeadminn'); ?></a>
            </li>
            <li class="    
                <?php if($sideactive=="approval_tutor"){ echo "active";} else{            
                } ?>"> <a href="<?php echo base_url(); ?>admin/tutor_approval"><i class="zmdi zmdi-accounts-list"></i>  <?php echo $this->lang->line('approval_tutor'); ?></a>
            </li>
            <?php
        }
        else if ($usertype == "assessor") {
            ?>
            <li class="    
                <?php if($sideactive=="approval_tutor"){ echo "active";} else{            
                } ?>"> <a href="<?php echo base_url(); ?>admin/tutor_approval"><i class="zmdi zmdi-assignment-account"></i> <?php echo $this->lang->line('approval_tutor'); ?></a>
            </li>
            <!-- <li class="    
                <?php if($sideactive=="assessor"){ echo "active";} else{            
                } ?>"> <a href="<?php echo base_url(); ?>admin/Assessor"><i class="zmdi zmdi-assignment-check"></i> <?php echo $this->lang->line('verify_subject'); ?></a>
            </li> -->
            <li class="<?php if($sideactive=="assessor"){ echo "sub-menu active";} else if($sideactive=="unassessor"){ echo "sub-menu active";} else{ echo "sub-menu";           
                } ?>">
                <a href=""><i class="zmdi zmdi-code"></i> <?php echo $this->lang->line('verify_subject'); ?></a>
                <ul>
                    <li class="<?php if($sideactive=="assessor"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/Assessor"><?php echo $this->lang->line('verify_subject'); ?></a></li>
                    <li class="<?php if($sideactive=="unassessor"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/UnAssessor"> <?php echo $this->lang->line('unverify_subject'); ?></a></li>
                </ul>
            </li>
            <?php
        }
        else if ($usertype == "evaluator") {
            ?>
            <li class="    
                <?php if($sideactive=="evaluator_tutor"){ echo "active";} else{            
                } ?>"> <a href="<?php echo base_url(); ?>admin/Evaluator"><i class="zmdi zmdi-accounts-list"></i> Evaluator</a>
            </li>
            <?php
        }
        else
        {
        ?>
        <li class="    
            <?php if($sideactive=="homeadmin"){ echo "active";} else{            
            } ?>"> <a href="<?php echo base_url(); ?>admin"><i class="zmdi zmdi-home"></i>  <?php echo $this->lang->line('homeadminn'); ?></a>
        </li>
        
        <li class="    
            <?php if($sideactive=="enggineadmin"){ echo "active";} else{            
            } ?>"> <a href="<?php echo base_url(); ?>admin/Engine"><i class="zmdi zmdi-assignment-o"></i>  <?php echo $this->lang->line('schedule_engine'); ?></a>
        </li>
        
        <li class="    
            <?php if($sideactive=="addsubject"){ echo "active";} else{            
            } ?>"> <a href="<?php echo base_url(); ?>admin/Subjectadd"><i class="zmdi zmdi-plus-square"></i>  <?php echo $this->lang->line('adminaddsubject'); ?></a>
        </li>

        <li class="<?php if($sideactive=="alltutor"){ echo "sub-menu active";} else if($sideactive=="kuotakelas"){ echo "sub-menu active";} else if($sideactive=="tambahkuotakelas"){ echo "sub-menu active";} else{ echo "sub-menu";           
            } ?>">
            <a href="#"><i class="zmdi zmdi-accounts-list"></i> Tutor</a>
            <ul>
                <li class="<?php if($sideactive=="alltutor"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/allTutor"><?php echo $this->lang->line('adminalltutor'); ?></a></li>                
                <li class="<?php if($sideactive=="kuotakelas"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/ApprovalKuota"><?php echo $this->lang->line('adminkuotakelas'); ?></a></li>
                <li class="<?php if($sideactive=="tambahkuotakelas"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/kuotaKelas"><?php echo $this->lang->line('tambahadminkuotakelas'); ?></a></li>
                <!-- <li class="<?php if($sideactive=="approval_tutor"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/tutor_approval"> <?php echo $this->lang->line('approval_tutor'); ?></a></li> -->
                <!-- <li class="<?php if($sideactive=="assessor"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/Assessor"> Assessor</a></li>
                <li class="<?php if($sideactive=="evaluator_tutor"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/Evaluator"> Evaluator</a></li> -->
                <!-- <li class="<?php if($sideactive=="reschedule"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/rescheduleClass"> Reschedule Class</a></li> -->
            </ul>
        </li>

        <li class="<?php if($sideactive=="addreferral"){ echo "sub-menu active";} else if($sideactive=="usereferral"){ echo "sub-menu active";} else{ echo "sub-menu";           
            } ?>">
            <a href=""><i class="zmdi zmdi-code"></i> <?php echo $this->lang->line('adminreferralcode'); ?></a>
            <ul>
                <li class="<?php if($sideactive=="addreferral"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/addReferral"><?php echo $this->lang->line('adminaddreferral'); ?></a></li>
                <li class="<?php if($sideactive=="usereferral"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/useReferral"> <?php echo $this->lang->line('adminusereferral'); ?></a></li>
            </ul>
        </li>

        <li style="display: none; " class="<?php if($sideactive=="paymentmulticast"){ echo "sub-menu active";} else if($sideactive=="paymentprivate"){ echo "sub-menu active";} else if($sideactive=="paymentgroup"){ echo "sub-menu active";} else{ echo "sub-menu";           
            } ?>">
            <a href=""><i class="zmdi zmdi-money-box"></i> <?php echo $this->lang->line('adminpayment'); ?></a>
            <ul>
                <li class="<?php if($sideactive=="paymentmulticast"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/PaymentMulticast"> <?php echo $this->lang->line('adminclassmulticast'); ?></a></li>
                <li class="<?php if($sideactive=="paymentprivate"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/PaymentPrivate"> <?php echo $this->lang->line('adminclassprivate'); ?></a></li>
                <li class="<?php if($sideactive=="paymentgroup"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/PaymentGroup"> <?php echo $this->lang->line('adminclassgroup'); ?></a></li>
            </ul>
        </li>

        <li class="<?php if($sideactive=="registerchannel"){ echo "sub-menu active";} else if ($sideactive=="pointchannel"){ echo "sub-menu active";} else if($sideactive=="usepointchannel"){ echo "sub-menu active";} else if($sideactive=="activitychannel"){ echo "sub-menu active";} else{ echo "sub-menu";           
            } ?>">
            <a href=""><i class="zmdi zmdi-view-list-alt"></i> <?php echo $this->lang->line('adminchannel'); ?></a>
            <ul>
                <li class="<?php if($sideactive=="registerchannel"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/Register"> <?php echo $this->lang->line('adminregisterchannel'); ?></a></li>
                <li><a class="<?php if($sideactive=="pointchannel"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('admin/PointChannel'); ?>"><?php echo $this->lang->line('adminpointchannel'); ?></a></li>
                <li><a class="<?php if($sideactive=="quotachannel"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('admin/QuotaChannel'); ?>">Quota Channel</a></li>
                <li><a class="<?php if($sideactive=="usepointchannel"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('admin/UsePointChannel'); ?>"> <?php echo $this->lang->line('adminusepointchannel'); ?></a></li>            
                <li><a class="<?php if($sideactive=="activitychannel"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('admin/ActivityChannel'); ?>"> <?php echo $this->lang->line('adminactivitychannel'); ?></a></li>
            </ul>
        </li>

        <li class="<?php if($sideactive=="datauangsaku"){ echo "sub-menu active";} else if ($sideactive=="confrimuangsaku"){ echo "sub-menu active";} else{ echo "sub-menu";           
            } ?>">
            <a href=""><i class="zmdi zmdi-view-list-alt"></i> <?php echo $this->lang->line('adminpocketmoney'); ?></a>
            <ul>
                <li><a class="<?php if($sideactive=="datauangsaku"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('admin/Data_transaction'); ?>"><?php echo $this->lang->line('admindatatransaction'); ?></a></li>
                <li><a class="<?php if($sideactive=="confrimuangsaku"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('admin/Confrim_transaction'); ?>"><?php echo $this->lang->line('adminconfirmtransaction'); ?></a></li>            
            </ul>
        </li>

        <li hidden class="<?php if($sideactive=="profile"){ echo "active"; } else{
            } ?>"> <a href="<?php echo base_url(); ?>admin/about"><i class="zmdi zmdi-account-circle"></i> <?php echo $this->lang->line('profil'); ?></a>
        </li>

        <li class="    
            <?php if($sideactive=="announcement"){ echo "active";} else{            
            } ?>"> <a href="<?php echo base_url(); ?>admin/Announcement"><i class="zmdi zmdi-notifications-active"></i>  <?php echo $this->lang->line('adminannouncement'); ?></a>
        </li>
        <li class="    
            <?php if($sideactive=="support"){ echo "active";} else{            
            } ?>"> <a href="<?php echo base_url(); ?>admin/Support"><i class="zmdi zmdi-folder-person"></i>  <?php echo $this->lang->line('adminsupport'); ?></a>
        </li>
        <?php
        }
    ?>
</ul>
