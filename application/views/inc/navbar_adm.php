 <div class="navigation-trigger hidden-xl-up" data-ma-action="aside-open" data-ma-target=".sidebar">
        <div class="navigation-trigger__inner">
            <i class="navigation-trigger__line"></i>
            <i class="navigation-trigger__line"></i>
            <i class="navigation-trigger__line"></i>
        </div>
    </div>

    <div class="header__logo hidden-sm-down">
        <h1><a href="#"><img src="" id="viewLogoChannelTop" alt="" style="width: auto; height: 50px;"> </a></h1>
    </div>

    <ul class="top-nav">        
        <li class="dropdown top-nav__notifications">
            <a href="" data-toggle="dropdown" class="top-nav__notify">
                <i class="zmdi zmdi-notifications"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                <div class="listview listview--hover">
                    <div class="listview__header">
                        Notifications

                        <div class="actions">
                            <a href="" class="actions__item zmdi zmdi-check-all" data-ma-action="notifications-clear"></a>
                        </div>
                    </div>

                    <div class="listview__scroll scrollbar-inner">                        
                        <!-- <a href="" class="listview__item">
                            <img src="https://cdn.classmiles.com/usercontent/dXNlci9STTJxS0dJci5qcGc=" class="listview__img" alt="">

                            <div class="listview__content">
                                <div class="listview__heading">Fredric Mitchell Jr.</div>
                                <p>Phasellus a ante et est ornare accumsan at vel magnauis blandit turpis at augue ultricies</p>
                            </div>
                        </a> -->
                    </div>

                    <div class="p-1"></div>
                </div>
            </div>
        </li>

        <li class="dropdown hidden-xs-down">
            <a href="" data-toggle="dropdown"><i class="zmdi zmdi-more-vert"></i></a>

            <div class="dropdown-menu dropdown-menu-right">                               
                <a href="<?php echo base_url();?>logout" class="dropdown-item">Logout</a>
            </div>
        </li>

    </ul>
<script type="text/javascript">    
    var access_token = "<?php echo $this->session->userdata('access_token');?>";
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";

    $.ajax({
        url: '<?php echo AIR_API;?>channel_getTentang/access_token/'+access_token,
        type: 'POST',
        data: {
            channel_id: channel_id
        },
        success: function(response)
        {
            var code    = response['code'];

            if (code == 200) {
                var channel_logo            = response['data']['channel_logo'];
                var channel_banner          = response['data']['channel_banner'];                
                var imgLogo                 = "<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+channel_logo;
                var imgBanner               = "<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+channel_banner;
                $("#viewLogoChannelTop").attr('src',imgLogo);
                $(".user__img").attr('src',imgLogo);
            }
            else
            {
                window.location.href="<?php echo BASE_URL();?>logout";
            }
        }
    });
</script>