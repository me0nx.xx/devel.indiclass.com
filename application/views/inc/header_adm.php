<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Dashboard - Channel</title>
        <!-- Vendor styles -->
        <link rel="stylesheet" href="../aset/channel/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">        
        <link rel="stylesheet" href="../aset/channel/vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="../aset/channel/vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">
        <link rel="stylesheet" href="../aset/channel/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css">
        <link rel="icon" href="https://cdn.classmiles.com/sccontent/icon.ico" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="https://infra.clarin.eu/content/libs/DataTables-1.10.4/media/css/jquery.dataTables.css">
        <!-- <script type="text/javascript" src="https://infra.clarin.eu/content/libs/DataTables-1.10.4/media/js/jquery.js"></script> -->
        <link rel="stylesheet" href="../aset/channel/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <!-- App styles -->
        <link rel="stylesheet" href="../aset/channel/css/app.min.css">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/jquery/dist/jquery.min.js"></script> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
        <link rel="stylesheet" type="text/css" href="../aset/channel/vendors/bower_components/flatpickr/dist/flatpickr.min.css" />
        <link rel="stylesheet" type="text/css" href="../aset/channel/css/bootstrap-material-datetimepicker.css">
        <style type="text/css">
            .modal-open .select2-dropdown {
                z-index: 10060;
            }

            .modal-open .select2-close-mask {
                z-index: 10055;
            }
        </style>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    </head>

    <body data-ma-theme="green">
        
        <div class="page-loader" style="background-color: rgba(0,0,0,0.3);">
            <div class="page-loader__spinner">
                <svg viewBox="25 25 50 50">
                    <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                </svg>
            </div>
        </div>