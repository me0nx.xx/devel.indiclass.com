<div id="mainnav">

    <!--Menu-->
    <!--================================-->
    <div id="mainnav-menu-wrap">
        <div class="nano has-scrollbar">
            <div class="nano-content" tabindex="0" style="right: -17px;">

                <!--Profile Widget-->
                <!--================================-->
                <div id="mainnav-profile" class="mainnav-profile">
                    <div class="profile-wrap text-center">
                        <div class="pad-btm">
                            <img class="img-circle img-md" src="../assets/indiclass/img/profile-photos/1.png" alt="Profile Picture">
                        </div>
                        <a href="#" class="box-block" aria-expanded="false">                            
                            <p class="mnp-name"><?php echo $this->session->userdata('user_name');?></p>
                            <span class="mnp-desc"><?php echo $this->session->userdata('email');?></span>
                        </a>
                    </div>
                </div>

                <!--Shortcut buttons-->
                <!--================================-->
                <div id="mainnav-shortcut" class="hidden">
                    <ul class="list-unstyled shortcut-wrap">
                        <li class="col-xs-3" data-content="My Profile" data-original-title="" title="">
                            <a class="shortcut-grid" href="#">
                                <div class="icon-wrap icon-wrap-sm icon-circle bg-mint">
                                <i class="demo-pli-male"></i>
                                </div>
                            </a>
                        </li>
                        <li class="col-xs-3" data-content="Messages" data-original-title="" title="">
                            <a class="shortcut-grid" href="#">
                                <div class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                                <i class="demo-pli-speech-bubble-3"></i>
                                </div>
                            </a>
                        </li>
                        <li class="col-xs-3" data-content="Activity" data-original-title="" title="">
                            <a class="shortcut-grid" href="#">
                                <div class="icon-wrap icon-wrap-sm icon-circle bg-success">
                                <i class="demo-pli-thunder"></i>
                                </div>
                            </a>
                        </li>
                        <li class="col-xs-3" data-content="Lock Screen" data-original-title="" title="">
                            <a class="shortcut-grid" href="#">
                                <div class="icon-wrap icon-wrap-sm icon-circle bg-purple">
                                <i class="demo-pli-lock-2"></i>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <!--================================-->
                <!--End shortcut buttons-->


                <ul id="mainnav-menu" class="list-group">
        
                    <!--Category name-->
                    <li class="list-header">Menu</li>
        
                    <!--Menu list item-->
                    <li class="<?php if($sideactive == 'classes_index'){ echo 'active-sub active'; } else { ' '; } ?>">
                        <a href="<?php echo BASE_URL();?>Siswa/KelasSaya">
                            <i class="fa fa-home"></i>
                            <span class="menu-title">Kelas Saya</span>
                        </a>                            
                    </li>
        
                    <li class="<?php if($sideactive == 'classes_pengajar'){ echo 'active-sub active'; } else { ' '; } ?>">
                        <a href="<?php echo BASE_URL();?>Siswa/Pengajar">
                            <i class="fa fa-users"></i>
                            <span class="menu-title">Pengajar</span>
                        </a>                            
                    </li>

                    <li class="<?php if($sideactive == 'classes_request'){ echo 'active-sub active'; } else { ' '; } ?>">
                        <a href="<?php echo BASE_URL();?>Siswa/Permintaan">
                            <i class="fa fa-vcard-o"></i>
                            <span class="menu-title">Permintaan Kelas</span>
                        </a>                            
                    </li>
                    
                    <!-- <li class="<?php if($sideactive == 'classes_datadiri'){ echo 'active-sub active'; } else { ' '; } ?>">
                        <a href="<?php echo BASE_URL();?>Siswa/DataDiri">
                            <i class="fa fa-user-circle"></i>
                            <span class="menu-title">Data Diri</span>
                        </a>                            
                    </li> -->

                    <li>
                        <a href="<?php echo BASE_URL();?>Siswa/logout">
                            <i class="fa fa-sign-out"></i>
                            <span class="menu-title">Keluar</span>
                        </a>                            
                    </li>

                    <li class="list-divider"></li>
                            
                </ul>

            </div>
        <div class="nano-pane" style="display: none;"><div class="nano-slider" style="height: 1782px; transform: translate(0px, 0px);"></div></div></div>
    </div>
    <!--================================-->
    <!--End menu-->

</div>