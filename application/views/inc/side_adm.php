<div class="scrollbar-inner">
    <div class="user">
        <div class="user__info" data-toggle="dropdown">
            <img class="user__img" src="" alt="">
            <div>
                <div class="user__name">
                    <?php 
                        $kalimat = $this->session->userdata('user_name');
                        // $jumlahkarakter=10;
                        // $cetak = substr($kalimat,$jumlahkarakter,1);
                        // if($cetak !=" "){
                        //     while($cetak !=" "){
                        //         $i=1;
                        //         $jumlahkarakter=$jumlahkarakter+$i;                                
                        //         $cetak = substr($kalimat,$jumlahkarakter,1);
                        //     }
                        // }
                        // $cetak = substr($kalimat,0,$jumlahkarakter);
                        echo $kalimat;
                        ?></div>
                <div class="user__email"><?php echo $this->session->userdata('email');?></div>
            </div>
        </div>

        <div class="dropdown-menu">
            <a class="dropdown-item" href="<?php echo base_url();?>Channel/Logout">Logout</a>
        </div>
    </div>

    <ul class="navigation">
        <li class="<?php if($sideactive=="Home"){ echo "navigation__active";} else{} ?>">
            <a href="<?php echo base_url();?>Channel"><i class="zmdi zmdi-home"></i> Home</a>
        </li>
        <li class="<?php if($sideactive=="Purchase"){ echo "navigation__active";} else{} ?>">
            <a href="<?php echo base_url();?>Channel/Purchase"><i class="zmdi zmdi-shopping-cart"></i> Purchase</a>
        </li>
        <li class="<?php if($sideactive=='Listclass'){ echo 'navigation__sub navigation__sub--active navigation__sub--toggled'; } else if($sideactive=='Tutor'){ echo 'navigation__sub navigation__sub--active navigation__sub--toggled'; } else if($sideactive=='Tutor'){ echo 'navigation__sub navigation__sub--active navigation__sub--toggled'; } else{ echo 'navigation__sub';}?>">
            <a href=""><i class="zmdi zmdi-collection-bookmark"></i> Program</a>

            <ul style="display: none;">
                <li class="<?php if($sideactive=="Listprogram"){ echo "navigation__active";} else{} ?>"><a href="<?php echo base_url();?>Channel/ListProgram">Program List</a></li>
                <li class="<?php if($sideactive=="Listclass"){ echo "navigation__active";} else{} ?>"><a href="<?php echo base_url();?>Channel/ListClass">Session</a></li>
                <li class="<?php if($sideactive=="Tutor"){ echo "navigation__active";} else{} ?>"><a href="<?php echo base_url();?>Channel/TutorList">Tutor</a></li>                
            </ul>
        </li>

        <li class="<?php if($sideactive=='Student'){ echo 'navigation__sub navigation__sub--active'; } else if($sideactive=='StudentJoin'){ echo 'navigation__sub navigation__sub--active navigation__sub--toggled'; } else if($sideactive=='Group'){ echo 'navigation__sub navigation__sub--active navigation__sub--toggled'; }else{ echo 'navigation__sub';}?>">
            <a href=""><i class="zmdi zmdi-account"></i> Participant</a>

            <ul style="display: none;">
                <li class="<?php if($sideactive=="Student"){ echo "navigation__active";} else{} ?>"><a href="<?php echo base_url();?>Channel/StudentList">Participant List</a></li>
                <li class="<?php if($sideactive=="StudentJoin"){ echo "navigation__active";} else{} ?>"><a href="<?php echo base_url();?>Channel/StudentListJoin">Participant Request Join</a></li>
                <li class="<?php if($sideactive=="Group"){ echo "navigation__active";} else{} ?>"><a href="<?php echo base_url();?>Channel/Group">Group</a></li>                
            </ul>
        </li>
        <li class="<?php if($sideactive=='ReportPoint'){ echo 'navigation__sub navigation__sub--active'; } else if($sideactive=='ReportPayment'){ echo 'navigation__sub navigation__sub--active navigation__sub--toggled'; }else{ echo 'navigation__sub';}?>">
            <a href=""><i class="zmdi zmdi-assignment"></i> Report</a>

            <ul style="display: none;">
            	<li class="<?php if($sideactive=="ReportPayment"){ echo "navigation__active";} else{} ?>"><a href="<?php echo base_url();?>Channel/ReportPayment">Payment</a></li>
                <li class="<?php if($sideactive=="ReportPoint"){ echo "navigation__active";} else{} ?>"><a href="<?php echo base_url();?>Channel/ReportPoint">Point</a></li>                
            </ul>
        </li>
        <!-- <li class="<?php if($sideactive=="Report"){ echo "navigation__active";} else{} ?>">
            <a href="<?php echo base_url();?>Channel/Report"><i class="zmdi zmdi-assignment"></i> Report</a>
        </li> -->
        <li class="<?php if($sideactive=="SettingPackage"){ echo "navigation__active";} else{} ?>">
            <a href="<?php echo base_url();?>Channel/SettingPackage"><i class="zmdi zmdi-storage"></i> Package</a>
        </li>
        <li class="<?php if($sideactive=="Setting"){ echo "navigation__active";} else{} ?>">
            <a href="<?php echo base_url();?>Channel/Settings"><i class="zmdi zmdi-settings"></i> Setting Channel</a>
        </li>
        
    </ul>
</div>