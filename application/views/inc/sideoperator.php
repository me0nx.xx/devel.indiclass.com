<div class="profile-menu">
    <a href="">
        <div class="profile-pic">
            <img src="<?php echo $this->session->userdata('user_image');?>" > 
        </div>

        <div class="profile-info">
            <?php echo $this->session->userdata('user_name'); ?>
        
            <i class="zmdi zmdi-caret-down"></i>
        </div>
    </a>
</div>
<ul class="main-menu">

    <li class="<?php if($sideactive=="home"){echo "active";}else{

    } ?>"><a href="#"><i class="zmdi zmdi-home"></i>Modify Class</a></li>   
    <!-- <li class="    
        <?php if($sideactive=="datauangsaku"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>operator/DataTransaction"><i class="zmdi zmdi-accounts-list"></i>  Data Transaction</a>
    </li> -->
    <!-- <li  class="<?php if($sideactive=="datauangsaku"){ echo "sub-menu active";} else if($sideactive=="confrimpending"){ echo "sub-menu active";} else if($sideactive=="listinvoice"){ echo "sub-menu active";} else if($sideactive=="confrimationpayment"){ echo "sub-menu active";} else if($sideactive=="listpaymenttutor"){ echo "sub-menu active";} else{ echo "sub-menu";           
            } ?>">
        <a href=""><i class="zmdi zmdi-assignment-o"></i> Finance</a>
        <ul>
            <li style="display: none;" class="<?php if($sideactive=="datauangsaku"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>operator/DataTransaction">Data Transaction</a></li>
            <li class="<?php if($sideactive=="listinvoice"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>operator/ListInvoice">List Invoice Active</a></li>
            <li class="<?php if($sideactive=="invoiceexpired"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>operator/InvoiceExpired">List Invoice Expired</a></li>
            <li class="<?php if($sideactive=="listpackageinvoice"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>operator/ListPackageInvoice">List Package Invoice</a></li>
            <li class="<?php if($sideactive=="confrimationpayment"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>operator/ConfrimationPayment">List Confirmation Payment</a></li>
            <li class="<?php if($sideactive=="confrimpending"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>operator/ConfrimPending">Transaction Pending</a></li>  
            <li class="<?php if($sideactive=="listpaymenttutor"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>operator/paymentTutor">Payment Tutor</a></li>          
        </ul>
    </li> -->

    <!-- <li class="<?php if($sideactive=="confrimuangsaku"){ echo "sub-menu active";} else if($sideactive=="confrimondemand"){ echo "sub-menu active";} else if($sideactive=="confrimmulticast"){ echo "sub-menu active";} else{ echo "sub-menu";           
            } ?>">
        <a href=""><i class="zmdi zmdi-assignment-o"></i> Class Confrimation</a>
        <ul>
            <li class="<?php if($sideactive=="confrimuangsaku"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>operator/ConfrimTransaction">Topup Saldo</a></li>
            <li class="<?php if($sideactive=="confrimmulticast"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>operator/ConfrimMulticast"> Multicast Paid</a></li>
            <li class="<?php if($sideactive=="confrimondemand"){ echo "active";} else{ } ?>"> <a href="<?php echo base_url(); ?>operator/ConfrimOnDemand"> Private / Group</a></li>
        </ul>
    </li> -->
    <!-- <li class="    
        <?php if($sideactive=="confrimuangsaku"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>operator/ConfrimTransaction"><i class="zmdi zmdi-assignment-o"></i>  Confrim Top Up</a>
    </li>
    <li class="    
        <?php if($sideactive=="confrimondemand"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>operator/ConfrimOnDemand"><i class="zmdi zmdi-assignment-o"></i>  Confrim On Demand </a>
    </li> -->
    <li style="display: none;" class="    
        <?php if($sideactive=="alltutor"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>operator/ListTutor"><i class="zmdi zmdi-accounts-list"></i>  List Tutor</a>
    </li>
    <li style="display: none;">
        <a href="<?php echo base_url('/logout'); ?>"><i class="zmdi zmdi-time-restore"></i> <?php echo $this->lang->line('logout'); ?></a>
    </li>  

</ul>