<nav class="navbar navbar-expand navbar-dark m-0" id="desc_colorChannel">

    <!-- Toggle sidebar -->
    <button class="navbar-toggler d-block" data-toggle="sidebar" type="button">
        <span class="material-icons">menu</span>
    </button>

    <!-- Brand -->
    <a href="<?php echo BASE_URL();?>Student/MyClass" class="navbar-brand"><img src="" id="desc_imageChannel" style="height: 30px; width: 100px; margin-right: 5px;"> <label id="desc_nameChannel"></label></a>

    <div class="navbar-spacer"></div>

    <!-- Menu -->
    <ul class="nav navbar-nav d-none d-md-flex">        
        <li class="nav-item active">
            <a class="nav-link" href="#"> <span id="nav_nametutor"> </span></a>
        </li>
    </ul>

    <!-- Menu -->
    <ul class="nav navbar-nav">

        <!-- User dropdown -->
        <li class="nav-item dropdown">
            <a class="nav-link active dropdown-toggle" data-toggle="dropdown" href="#" role="button">
                <img onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" src="" alt="Avatar" id="nav_imagetutor" class="rounded-circle" width="40">
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="<?php echo BASE_URL();?>Tutor/Personal">
                    <i class="material-icons">person</i> Profile
                </a>
                <a class="dropdown-item" href="<?php echo BASE_URL();?>Admin/Logout">
                    <i class="material-icons">lock</i> Logout
                </a>
            </div>
        </li>
        <!-- // END User dropdown -->

    </ul>
    <!-- // END Menu -->

    
</nav>

<div class="modal  fade" id="modal_session_expired" style="margin-top: 10%;"  data-backdrop="static" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title pull-left" style="color: #000;"><b>Fisioterapi</b></h3>                
            </div>
            <div class="modal-body">
                <label>Sesi anda telah habis<br>Silahkan masuk kembali</label>
            </div>                
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {

        var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc; 
        var access_token = "<?php echo $this->session->userdata('access_token');?>";
        var iduser = "<?php echo $this->session->userdata('id_user');?>";
        var channel_id = "<?php echo $this->session->userdata('channel_id');?>";

        if (iduser!="") {
            $.ajax({
                url: '<?php echo AIR_API;?>channel_getTentang/access_token/'+access_token,
                type: 'POST',
                data: {
                    channel_id : channel_id
                },
                success: function(response)
                {
                    if (response['code'] == -400) {
                        window.location.href='<?php echo base_url();?>Admin/Logout';
                    }
                    var a = JSON.stringify(response);
                    // alert(response['data']['user_name']);

                    var channel_name    = response['data']['channel_name'];
                    var channel_logo    = response['data']['channel_logo'];
                    var channel_color   = response['data']['channel_color'];
                    var kotak_image     = "<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+channel_logo;
                    // $("#desc_nameChannel").html(channel_name);
                    $("#desc_imageChannel").attr('src','../assets/img/logos.png');
                    $("#desc_colorChannel").css('background-color', '#f95a60');

                }
            });

            $.ajax({
                url: '<?php echo AIR_API;?>detail_student/access_token/'+access_token,
                type: 'POST',
                data: {
                    channel_id : '46',
                    id_user : iduser
                },
                success: function(response)
                {
                    if (response['code'] == -400) {
                        window.location.href='<?php echo base_url();?>Admin/Logout';
                    }
                    var a = JSON.stringify(response);
                    // alert(response['data']['user_name']);

                    var user_name       = response['data']['user_name'];
                    var user_image      = response['data']['user_image'];
                    var kotak_image     = "<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+user_image;
                    $("#nav_nametutor").html(user_name);
                    $("#nav_imagetutor").attr('src',kotak_image);

                }
            });

        }
    });
</script>