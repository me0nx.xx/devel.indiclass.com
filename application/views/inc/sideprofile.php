<style>
      .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 5px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 250px;
        height: 250px;
      }

      .cropit-preview-image-container {
        cursor: move;
      }

      .cropit-preview-background {
        opacity: .2;
        cursor: auto;
      }

      .image-size-label {
        margin-top: 10px;
      }

    </style>

<div class="pm-overview c-overflow">


    <div class="pmo-pic"  style="cursor:pointer;">
        <div class="p-relative" data-target="#modalColor" data-toggle="modal">
            <?php
                $id_user_kids = $this->session->userdata('id_user_kids');
                $parent_id  = $this->session->userdata('id_user');
                $iduser = $this->session->userdata('id_user_kids');
                $imguser  = $this->session->userdata('img_user_kids');

                if ($iduser == null) {
                    $imguser = $this->session->userdata('img_user_kids');
                    $iduser  = $this->session->userdata('id_user');
                }
                // $iduser = $this->session->userdata('id_user');
                $get_image = $this->db->query("SELECT * FROM tbl_user WHERE id_user='".$iduser."'")->row_array();
            ?>
            <a>             
                <img  class="img-responsive" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$get_image['user_image'];?>" > 
            </a>

            <div class="pmop-edit" style="cursor:pointer;">
                <i class="zmdi zmdi-camera"></i> <span class="hidden-xs" ><?php echo $this->lang->line('upd_profpic');?></span>
            </div>
        </div>        
        
    </div>

    <div id="kontak_nonkids" class="pmo-block pmo-contact hidden-xs">
        <ul>
            <li style="overflow:hidden; word-wrap: break-word;"><i class="zmdi zmdi-account"></i> 
                <?php 
                    $id_user = $this->session->userdata('id_user');
                    $id_user_kids = $this->session->userdata('id_user_kids');
                    $username_kids = $this->session->userdata('username_kids');
                    $parent_id  = $this->session->userdata('id_user');
                    $id_user_kids = $this->session->userdata('id_user_kids');
                    $get_data_anak = $this->db->query("SELECT ts.*, tpk.parent_id, tpk.jenjang_id FROM tbl_user as ts LEFT JOIN tbl_profile_kid as tpk ON ts.id_user=tpk.kid_id WHERE id_user='$id_user'")->row_array();
                    $get_jenjang_anak = $this->db->query("SELECT tpk.parent_id, tpk.jenjang_id, tu.jenjang_level, tu.jenjang_name from tbl_profile_kid as tpk INNER JOIN master_jenjang as tu where tu.jenjang_id = tpk.jenjang_id and tpk.parent_id='$parent_id'")->row_array();                        
                    $username_kids = $this->session->userdata('username_kids');
                    $parent_id  = $this->session->userdata('id_user');
                    $id_user_kids = $this->session->userdata('id_user_kids');
                    $birth_date = $get_data_anak['user_birthdate'];
                    $birthdate = date("d M Y", strtotime($birth_date));
                    if ($parent_id != $id_user_kids) {
                        
                    }
                    else{

                    }
                    echo $get_data_anak['user_name'];    
                ?>
            </li>
            <li style="overflow:hidden; word-wrap: break-word;"><i class="zmdi zmdi-graduation-cap"></i> 
                <?php 
                    $jenjang_id   = $this->session->userdata('jenjang_id_kids'); 
                    if ($jenjang_id == null) {
                        $jenjang_id   = $this->session->userdata('jenjang_id'); 
                    }
                    $get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$jenjang_id."'")->row_array()['jenjang_name'];
                    $get_jenjanglevel = $this->db->query("SELECT jenjang_level  FROM master_jenjang where jenjang_id='".$jenjang_id."'")->row_array()['jenjang_level'];
                    if ($get_jenjanglevel=="0") {
                        $kelas = "";
                    }
                    else{
                        $kelas = "Kelas ".$get_jenjanglevel.",";   
                    }
                    echo $kelas." ". $get_jenjangtype; 
                ?>
            </li>
            <li style="overflow:hidden; word-wrap: break-word;"><i class="zmdi zmdi-cake"></i> 
                <?php 
                    
                    echo $get_data_anak['user_age']." ". $this->lang->line('yearsold'); 
                ?>
            </li>
             
            <li style="overflow:hidden; word-wrap: break-word;">
                <i class="zmdi zmdi-male-female"></i>
                <address class="m-b-0 ng-binding">
                    <?php 
                        if ($get_data_anak['user_gender'] == "") {
                            echo $this->lang->line('nogender');
                        }
                        if ($get_data_anak['user_gender'] == "Male"){
                            echo $this->lang->line('male');
                        }
                        if($get_data_anak['user_gender'] == "Female"){
                            echo $this->lang->line('women');
                        }
                    ?>   
                </address>
            </li>           
        </ul>
    </div>

    <div id="btn-color-targets">

        <div class="modal fade" data-modal-color="red"  data-backdrop="static" id="modal_foto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-body" style="margin-top: 75%;" > 
                        <br>
                        <div class="alert alert-danger" style="display: none; ?>" id="alert_ukuran"><center>Maksimal file 10 MB,<br>Ukuran foto harap 512 x 512 pixels atau lebih</center>
                        </div>
                        <div class="alert alert-danger" style="display: none; ?>" id="alert_wajah"><center><?php echo $this->lang->line('fotowajah'); ?></center>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" data-target="#modalColor" style="">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" data-modal-color="" id="modalColor" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <center>
                            <h4 class="modal-title" style="color:gray; margin-bottom:-20px;"><?php echo $this->lang->line('uploadphoto');?></h4>                
                        </center>
                    </div>
                    <hr>
                    <div class="modal-body">
                        <div class="image-editor">
                          <input accept="image/x-png,image/gif,image/jpeg"  type="file" required name="inputFile" id="inputFile" data-placeholder="No Selected" class="filestyle cropit-image-input">
                          <input type="text" name="image_new" id="image_new" hidden>

                          <center>
                              <div class="cropit-preview"></div>
                          </center>
                          <div class="image-size-label">
                            <label class="c-gray uploadImage">Resize image</label> 
                          </div>
                          <input type="range" class="cropit-image-zoom-input">
                        </div> 
                    </div>
                    <div class="progress progress-striped active m-t-10" style="height:18px;">
                        <div class="progress-bar" style="width:0%; height:150px;"></div>
                    </div>
                    <div class="modal-footer bgm-teal">
                        <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo $this->lang->line('cancel');?></button>
                        <button type="" class="uploadImage btn btn-info upload" id="save" name="save"><?php echo $this->lang->line('upload');?></button>                
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div id="kotak_loading" class="page-loader" style="background-color:black; opacity: 0.5;">
    <div class="preloader pl-lg pls-white">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p style="margin-left: -18px;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
    </div>
</div>

<script type="text/javascript">
    var id_user_kids = "<?php echo $this->session->userdata('id_user_kids'); ?>";
    var id_user = "<?php echo $this->session->userdata('id_user'); ?>";

    if (id_user == id_user_kids) {
        $("#kontak_kids").css('display','none');
        $("#kontak_nonkids").css('display','block');
    }
    else{
        $("#kontak_kids").css('display','block');
        $("#kontak_nonkids").css('display','none');

    }
</script>

<script type="text/javascript">
    
    jQuery(document).ready(function(){

        function proccess()
        {
            $(document).on('submit','form',function(e){
                var inp = $("#inputFile");                
                if (inp.val().length > 0) {
                    e.preventDefault();
                    $form = $(this);
                    uploadImage($form);            
                }          
            });
        }        

        function uploadImage($form){
            $form.find('.progress-bar').removeClass('progress-bar-success')
                                        .removeClass('progress-bar-danger');

            var formdata = new FormData($form[0]); //formelement
            var request = new XMLHttpRequest();

            //progress event...
            request.upload.addEventListener('progress',function(e){
                var percent = Math.round(e.loaded/e.total * 100);
                $form.find('.progress-bar').width(percent+'%').html(percent+'%');
            });

            //progress completed load event
            request.addEventListener('load',function(e){
                
                $form.find('.progress-bar').addClass('progress-bar-success').html("<?php if ($this->session->userdata('lang') == 'indonesia') { echo "Unggah Selesai...";} else { echo "Uploading Completed...";} ?>");
                // alert('Profile Completed Change');
                setTimeout(function(){
                    window.location.reload();
                }, 1000);
            });

            request.open('post', '<?php echo base_url(); ?>Master/update_photo');            
            request.send(formdata);

            $form.on('click','.cancel',function(){
                request.abort();

                $form.find('.progress-bar')
                    .addClass('progress-bar-danger')
                    .removeClass('progress-bar-success')
                    .html('upload aborted...');
            });

        }

        $('body').on('click', '#btn-color-targets > .btn', function(){
            var color = $(this).data('target-color');
            $('#modalColor').attr('data-modal-color', color);
        });

        jQuery('#change-pic').on('click', function(e){
            jQuery('#changePic').show();
            jQuery('#change-pic').hide();
            
        });
        
        jQuery('#photoimg').on('change', function()   
        { 
            jQuery("#preview-avatar-profile").html('');
            jQuery("#preview-avatar-profile").html('Uploading....');
            jQuery("#cropimage").ajaxForm(
            {
            target: '#preview-avatar-profile',
            success:    function() { 
                    jQuery('img#photo').imgAreaSelect({
                    aspectRatio: '1:1',
                    onSelectEnd: getSizes,
                });
                jQuery('#image_name').val(jQuery('#photo').attr('file-name'));
                }
            }).submit();

        });
    });
</script>
<script src="<?php echo base_url();?>aset/Cropit/dist/jquery.cropit.js"></script>
<script>
  $(function() {
    $('.image-editor').cropit({
      exportZoom: 1.25,
      imageBackground: true,
      imageBackgroundBorderWidth: 20,
    });
    $('.rotate-cw').click(function() {
      $('.image-editor').cropit('rotateCW');
    });
    $('.rotate-ccw').click(function() {
      $('.image-editor').cropit('rotateCCW');
    });

    $('.uploadImage').click(function() {
        $("#loading_image").css('display','block');
        var access_token_jwt = "<?php echo $this->session->userdata('access_token_jwt')?>";
        var iduser  = "<?php echo $this->session->userdata('id_user');?>";
        var d = new Date();
        var n = d.getTime();
        var imageData = $('.image-editor').cropit('export');
        $("#image_new").val(imageData);
        var profil_img = imageData.split(";base64,")[1];
        $("#modalColor").modal("hide");
        $("#kotak_loading").css('display','block');
        // angular.element(document.getElementById('save')).scope().loadings();
        // console.log(profil_img);
        // return false;

        // FaceDetection Not active
        /*var image_new = $("#image_new").val();
        $.ajax({
            url: '<?php echo base_url(); ?>Master/update_photo_new',
            type: 'POST',
            data: {
                image_new: image_new
            },
            success: function(response)
            {   
                window.location.reload();
            }
        });*/

        // FaceDetection active
        $.ajax({
            // url: 'https://10.148.0.9/FaceDetection/public/index.php/api/face_detection',
            // url: 'https://classmiles.com/engine/face_detection',
            url: '<?php echo base_url(); ?>Rest/face_detection',
            type: 'POST',
            data: {
                image: profil_img
            },
            success: function(response)
            {   
                $("#loading_image").css('display','none');
                $("body").css("cursor", "default");                 
                console.warn("test 0 class "+response['face']);
                if (response['face'] == true) {
                    var image_new = $("#image_new").val();
                    $.ajax({
                        url: '<?php echo base_url(); ?>Master/update_photo_new',
                        type: 'POST',
                        data: {
                            image_new: image_new
                        },
                        success: function(response)
                        {   
                            $("#kotak_loading").css('display','none');
                            window.location.reload();
                        }
                    });
                }
                else{
                    $("#loading_image").css('display','none');
                    $("body").css("cursor", "default"); 
                    var $el = $('#inputFile');
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();
                    $("#modal_foto").modal("show"); 
                    $("#modalColor").modal("hide");
                    $('#alert_wajah').css('display','block');
                    $('#alert_ukuran').css('display','none'); 
                    // $('#save').addClass('btn btn-info upload disabled'); 
                }    
            }
        });
    });
  });
</script>
