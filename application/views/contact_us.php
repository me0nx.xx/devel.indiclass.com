<div class="site_preloader flex_center">
    <div class="site_preloader_inner">
      <div class="rect1"></div>
      <div class="rect2"></div>
      <div class="rect3"></div>
      <div class="rect4"></div>
      <div class="rect5"></div>
    </div>
</div>

<div class="styler">
    <div class="single_styler clearfix sticky_header">
        <h4>Header style</h4>
        <select class="wide">
            <option value="static">Enable header top</option>
            <option value="diseable_ht">Diseable header top</option>
        </select>
    </div>
    <div class="single_styler primary_color clearfix">
        <h4>Primary Color</h4>
        <div class="clearfix">
            <input id="primary_clr" type="text" class="form-control color_input" value="#f9bf3b" />
            <label class="form-control" for="primary_clr"><i class="fa fa-angle-down"></i></label>
        </div>
    </div>
    <div class="single_styler secound_color clearfix">
        <h4>Secoundary Color</h4>
        <div class="clearfix">
            <input id="secound_clr" type="text" class="form-control color_input" value="#19b5fe" />
            <label class="form-control" for="secound_clr"><i class="fa fa-angle-down"></i></label>
        </div>
    </div>
    <div class="single_styler layout_mode clearfix">
        <h4>Layout Mode</h4>
        <select class="wide">
            <option value="wide">Wide</option>
            <option value="boxed">Boxed</option>
            <option value="wide_box">Wide Boxed</option>
        </select>
    </div>
    <div class="layout_mode_dep">
        <div class="single_styler body_bg_style clearfix">
            <h4>Body Background style</h4>
            <select class="wide">
                <option value="img_bg">Image Background</option>
                <option value="solid">Solid Color</option>
            </select>
        </div>
        <div class="single_styler body_solid_bg clearfix">
            <h4>Select Background Color</h4>
            <div class="clearfix">
                <input id="body_bg_clr" type="text" class="form-control color_input" value="#d3d3d3" />
                <label class="form-control" for="body_bg_clr"><i class="fa fa-angle-down"></i></label>
            </div>
        </div>
        <!-- <div class="single_styler body_bg_img clearfix">
            <h4>Select Background Image</h4>
            <div class="clearfix">
                <div class="single_bg" data-bg="assets/img/styler/07.jpg"></div>
                <div class="single_bg" data-bg="assets/img/styler/08.jpg"></div>
                <div class="single_bg" data-bg="assets/img/styler/1.png"></div>
                <div class="single_bg" data-bg="assets/img/styler/2.png"></div>
                <div class="single_bg" data-bg="assets/img/styler/3.png"></div>
                <div class="single_bg" data-bg="assets/img/styler/4.png"></div>
                <div class="single_bg" data-bg="assets/img/styler/5.png"></div>
                <div class="single_bg" data-bg="assets/img/styler/6.png"></div>
            </div>
        </div> -->
    </div>
</div>
   
<div class="main_wrap">
    <?php $this->load->view('inc/navbar');?>

    <!-- 15. breadcrumb_area -->
    <div class="breadcrumb_area">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>Kontak kami</h1>
                    <!-- <ul class="brc">
                        <li><a href="#">Home</a></li>
                        <li><span>Contact</span></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
    <!-- 15. /breadcrumb_area -->
    
    <!-- 10. form_area -->
    <div class="form_area sp single_page">
        <div class="container">
            <div class="row">
                <div class="col-md-5 form_h">
                    <div class="contact_info">
                        <div class="single_contact">
                            <p>Hubungi Kami</p>
                            <span>(65) 81035356</span>
                            <span>(62) 81388451201</span>
                        </div>
                        <div class="single_contact">
                            <p>Email</p>
                            <span>info@fisipro.com.sg</span>                            
                        </div>
                        <div class="single_contact">
                            <p>Jam Buka Layanan Pelanggan:</p>
                            <span>Mon to Fri : 10 am to 8 pm</span>
                            <span>Sat : 10 am to 6 pm</span>
                            <span>Sun : 11 am to 8 pm</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 form_h">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.090641984956!2d110.3414610649213!3d-7.780213679350158!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb94e114a9cee9bf8!2sClassmiles!5e0!3m2!1sen!2sid!4v1536036258689" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- 10. /form_area -->

    <!-- 04. cta_area -->
    <div class="cta_area wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="cta">
                        <h2>Mulai kelas di Fisipro</h2>
                    </div>
                </div>
                <div class="col-md-3 text-right">
                    <a href="#" class="button hvr-bounce-to-right pbg"><i class="fa fa-long-arrow-right"></i> Daftar Sekarang</a>
                </div>
            </div>
        </div>
    </div>
    <!-- 04. /cta_area -->
    
</div>