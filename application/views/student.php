<div class="site_preloader flex_center">
    <div class="site_preloader_inner">
      <div class="rect1"></div>
      <div class="rect2"></div>
      <div class="rect3"></div>
      <div class="rect4"></div>
      <div class="rect5"></div>
    </div>
</div>

<div class="styler">
    <div class="single_styler clearfix sticky_header">
        <h4>Header style</h4>
        <select class="wide">
            <option value="static">Enable header top</option>
            <option value="diseable_ht">Diseable header top</option>
        </select>
    </div>
    <div class="single_styler primary_color clearfix">
        <h4>Primary Color</h4>
        <div class="clearfix">
            <input id="primary_clr" type="text" class="form-control color_input" value="#f9bf3b" />
            <label class="form-control" for="primary_clr"><i class="fa fa-angle-down"></i></label>
        </div>
    </div>
    <div class="single_styler secound_color clearfix">
        <h4>Secoundary Color</h4>
        <div class="clearfix">
            <input id="secound_clr" type="text" class="form-control color_input" value="#19b5fe" />
            <label class="form-control" for="secound_clr"><i class="fa fa-angle-down"></i></label>
        </div>
    </div>
    <div class="single_styler layout_mode clearfix">
        <h4>Layout Mode</h4>
        <select class="wide">
            <option value="wide">Wide</option>
            <option value="boxed">Boxed</option>
            <option value="wide_box">Wide Boxed</option>
        </select>
    </div>
    <div class="layout_mode_dep">
        <div class="single_styler body_bg_style clearfix">
            <h4>Body Background style</h4>
            <select class="wide">
                <option value="img_bg">Image Background</option>
                <option value="solid">Solid Color</option>
            </select>
        </div>
        <div class="single_styler body_solid_bg clearfix">
            <h4>Select Background Color</h4>
            <div class="clearfix">
                <input id="body_bg_clr" type="text" class="form-control color_input" value="#d3d3d3" />
                <label class="form-control" for="body_bg_clr"><i class="fa fa-angle-down"></i></label>
            </div>
        </div>
        <!-- <div class="single_styler body_bg_img clearfix">
            <h4>Select Background Image</h4>
            <div class="clearfix">
                <div class="single_bg" data-bg="assets/img/styler/07.jpg"></div>
                <div class="single_bg" data-bg="assets/img/styler/08.jpg"></div>
                <div class="single_bg" data-bg="assets/img/styler/1.png"></div>
                <div class="single_bg" data-bg="assets/img/styler/2.png"></div>
                <div class="single_bg" data-bg="assets/img/styler/3.png"></div>
                <div class="single_bg" data-bg="assets/img/styler/4.png"></div>
                <div class="single_bg" data-bg="assets/img/styler/5.png"></div>
                <div class="single_bg" data-bg="assets/img/styler/6.png"></div>
            </div>
        </div> -->
    </div>
</div>
   
<div class="main_wrap">
    <?php $this->load->view('inc/navbar');?>

    <!-- 15. breadcrumb_area -->
    <div class="breadcrumb_area">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>Siswa</h1>
                    <!-- <ul class="brc">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li><a href="http://rltclass.com.sg">Home</a></li>
                        <li><a href="#">Student</a></li>
                        <li><span>Select preferred package</span></li>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
    <!-- 15. /breadcrumb_area -->

    <!-- 20. s_service_area -->
    <div class="s_service_area sp">
        <div class="container">
            <div class="row">
                <aside class="col-md-4">
                    <div class="widget widget_service wow fadeInUp">
                        <ul>
                            <li class="active clickHow" id="tab1" data-step="1"><a href="#">1. Daftar</a></li>
                            <li class="clickHow" id="tab2" data-step="2"><a href="#">2. Terima kasih telah mendaftar</a></li>
                            <li class="clickHow" id="tab3" data-step="3"><a href="#">3. Siswa Login</a></li>
                            <li class="clickHow" id="tab4" data-step="4"><a href="#">4. Portal Siswa (siswa dashboard)</a></li>
                        </ul>
                    </div>
                </aside>
                
                <div id="step1">
                    <div class="col-md-8">
                        <h4>1. Daftar</h4>
                        <br>
                    </div>
                    <div class="col-md-8">
                        <div class="service_content wow fadeInUp">
                            <div>
                                <h4>Bagaimana cara mendaftar ?</h4>
                                <br>
                                <p>Mengunjungi halaman daftar fisipro, lalu mengisi data diri hingga mendapatkan email verifikasi dari pihak kami.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="step2" style="display: none;">
                    <div class="col-md-8">
                        <h4>2. Terima kasih telah mendaftar</h4>
                        <br>
                    </div>
                    <div class="col-md-8">
                        <p>Duis autem vel eum iriure dolor in hendrerit in vulput ate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum</p>
                    </div>
                </div>

                <div id="step3" style="display: none;">
                    <div class="col-md-8">
                        <h4>3. Siswa Login</h4>
                        <br>
                    </div>
                    <div class="col-md-8">
                        <p>Duis autem vel eum iriure dolor in hendrerit in vulput ate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit</p>
                    </div>
                </div>

                <div id="step4" style="display: none;">
                    <div class="col-md-8">
                        <h4>4. Portal Siswa (siswa dashboard)</h4>
                        <br>
                    </div>
                    <div class="col-md-8">
                        <p>You’ll need to activate your account by clicking on the link in the verification email sent. Once you’re activated, you will have access to the members portal to browse and select your classes.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal  fade" id="modal_buypackage" style="margin-top: 10%;" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title pull-left" style="color: #000;"><b>The Rote Less Travelled</b></h3>
                </div>
                <div class="modal-body">
                    <!-- <div class='row col-md-12'>
                        <div class="col-md-3">
                            <label>Price</label>
                            <label>Total Session</label>
                        </div>
                        <div class="col-md-9">
                            <label>: $ <b id="detail_price">3</b></label><br>
                            <label>: <b id="detail_session">20</b></label>
                        </div>
                    </div> -->
                    <label>Are You sure you want to buy this package?</label>
                </div>
                <div class="modal-footer" style="background-color: #e5e5e5;">
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                    <button type="button" data-dismiss="modal" class="btn btn-success waves-effect" id="act_buy_package" data-id="" rtp="">Yes</button>
                </div>
            </div>
        </div>
    </div>
    <div class="animated fadeIn modal fade" style="margin-top: 15%; color: white;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" >
            <div class="modal-content" id="modal_konten">
                <div class="modal-body" align="center">
                    <label id="text_modal">Halloo</label><br>
                    <button   id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                </div>
            </div>
        </div>
    </div>
    <div class="animated fadeIn modal fade" style="color: white; margin-top: 15%;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm" >
            <div class="modal-content" id="modal_konten">
                <div class="modal-body" align="center">
                    <label id="text_modal">Halloo</label><br>
                    <button   id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                </div>
            </div>
        </div>
    </div>
    <!-- 20. /s_service_area -->

    <script type="text/javascript">
        var id_user = "<?php echo $this->session->userdata('id_user');?>";
        var total_session = $(".btn_buy").attr('tot_ses');
        var price_package = $(".btn_buy").attr('price');
        $(document.body).on('click', '.clickHow' ,function(e){
            var step = $(this).data('step');
            var arrayStep = ['1','2','3','4','5','6'];
            for (var i = 0; i < arrayStep.length ; i++) {
                $("#tab"+arrayStep[i]).removeClass();
                if (arrayStep[i] == step) {
                    $("#step"+arrayStep[i]).css('display','block');                    
                    $("#tab"+arrayStep[i]).addClass('active clickHow');
                }
                else
                {
                    $("#tab"+arrayStep[i]).addClass('clickHow');                    
                    $("#step"+arrayStep[i]).css('display','none');
                }                
            }
        });
        $(document).on('click', '#act_goto_login', function(){
             window.location.href='<?php echo base_url();?>Login?tlnk=std';    
        });
        $(document).on('click', '.btn_buy', function(){
            if (id_user == null || id_user =='') {
                $("#modal_alert_login").modal('show');
                // $("#modal_buypackage").modal('show');
            }
            else
            {
                $("#modal_buypackage").modal('show');
            }
        });
        $(document).on('click', '#act_buy_package', function(){
            // alert("id "+id_user);
            // alert("total_session "+total_session);
            // alert("price "+price_package);
            $("#modal_package").modal('hide');
            $("#modal_alert").modal('show');
            $("#modal_konten").css('background-color','#26c04c');
            $("#text_modal").html("Successfully Buy this Package");
            $("#button_ok").click( function(){
                location.reload();
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $(".select2").select2();
            var tgl = new Date(); 
            var user_utc = new Date().getTimezoneOffset();
            user_utc = -1 * user_utc;             
            var channel_id = "46";
            $("#list_credit").empty();
            // var status_member = null;

            $("#btn_back").click(function(){
                $("#page_add_credits").css('display','none');
                $("#page_select_member").css('display','block');
            });

            $.ajax({
                url: '<?php echo base_url();?>Rest/list_package',
                type: 'POST',
                data: {
                    channel_id: channel_id
                },
                success: function(response)
                {
                    var a = JSON.stringify(response);  
                    var code = response['code'];                
                    if (code == 200) {
                        for (var i = 0; i < response.data.length; i++) {
                            var id_package = response['data'][i]['id_package'];
                            var name_package = response['data'][i]['name_package'];
                            var credit_package = response['data'][i]['credit_package'];
                            var price_package_basic = response['data'][i]['price_package_basic'];
                            var price_package_premium = response['data'][i]['price_package_premium'];

                            var kotak_credit = "<div class='col-md-4 col-sm-6 single_job_tile wow fadeInUp' style='visibility: visible; animation-name: fadeInUp; margin-bottom:15px;'>"+
                                "<div>"+
                                    "<span>"+name_package+"</span>"+
                                    "<div class='col-md-12' style='margin-top:15px; height:100px;'>"+
                                    "<div class='col-md-6 col-sm-6'>"+
                                    "<h4 style='font-weight:normal;'>Price Basic</h4>"+
                                    "<h3 class='price_package' style='font-weight:normal;'>$ "+price_package_basic+"</h3>"+
                                    "</div>"+
                                    "<div class='col-md-6 col-sm-6'>"+
                                    "<h4><b>Price Premium</b></h4>"+
                                    "<h3 class='price_package'>$ "+price_package_premium+"</h3>"+
                                    "</div>"+
                                    "<hr></div>"+
                                    "<span class='date' style='font-size:16px;'>Total "+credit_package+" Credits</span>"+
                                    "<a href='#' data-credit='"+credit_package+"' data-price='"+price_package_basic+"' data-idpackage='"+id_package+"' data-namepackage='"+name_package+"' class='button-3 btn_buy  hvr-bounce-to-right'>Choose Package</a>"+
                                "</div>"+
                            "</div>";

                            $("#list_credit").append(kotak_credit);
                        }
                    }
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>Admin/Logout';
                    }
                }
            });

            $(document).on("click", ".btn_buy", function () { 
                $("#modal_alert").modal("show");
                $("#modal_konten").css('background-color','#F22613');
                $("#text_modal").html("Please login to continue buy");
                $("#button_ok").click( function(){
                    window.location.href='<?php echo base_url();?>Login';
                });            
            });
        });
    </script>
</div>