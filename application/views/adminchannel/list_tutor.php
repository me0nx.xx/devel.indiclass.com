<main class="main">
    
    <header class="header">
       	<?php $this->load->view("inc/navbar_adm"); error_reporting(0); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <header class="content__title">
            <h1>List Tutor</h1>
            <small>Detail list of tutors listed on your channel</small>

            <div class="actions">
                <a data-toggle="modal" href="#modalAddTutor"><button class="btn btn-success">+ Add Tutor</button></a>
            </div>
        </header>

        <div class="row">
            <div class="card col-md-12 col-xs-12">
                <div class="card-block">
                    <div class="row table-responsive">
                        <div id="tables_listTutor"></div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="animated fadeIn modal fade" style="color: white;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" >
                <div class="modal-content" id="modal_konten">
                    <div class="modal-body" align="center">
                        <label id="text_modal">Halloo</label><br>
                        <button  id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Default -->  
        <div class="modal fade show" id="modalAddTutor">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">                        
                        <h4 class="modal-title c-white pull-left">Invite Tutor</h4>
                        <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                    </div>
                    <div class="modal-body m-t-20">
                        <form id="frmdata">
                            <div class="col-md-12" style="padding: 0;">
                                <div class="col-md-12">
                                    <label>Search Tutor</label><br>
                                    <!-- <input name='ms' class="col-md-8 m-t-5" style="height: 35px;" id="ms"> -->
                                    <div class="col-md-12 m-t-10">                                          
                                        <select required name="tagorang[]" id="tagorang" multiple class="select2 tagorang" style="width: 100%;">                                                
                                        </select>                                           
                                    </div>
                                </div>
                                <div class="col-md-1"></div>  
                                <div class="col-md-12"><br><br>
                                    <p class="c-black">Noted</p>                            
                                    <ol>
                                        <li>Make sure Add a tutor listed on your Channel</li>
                                    </ol>                                    
                                </div>                                  
                            </div>
                        </form>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button"  class="btn btn-success btn-block m-t-15" id="savetutor">Add</button>                            
                    </div>
                </div>
            </div>
        </div>
        <div class="animated fadeIn modal fade" style="color: white;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" >
                <div class="modal-content" id="modal_konten">
                    <div class="modal-body" align="center">
                        <label id="text_modal">Halloo</label><br>
                        <button  id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade show" id="modalDeleteTutor" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>Delete Tutor</b></h3>
                    </div>
                    <div class="modal-body">                        
                        <label class="c-gray f-15">Are you sure you want to remove the tutor from your channel ?</label>                         
                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                        <button type="button" data-dismiss="modal" class="btn btn-danger waves-effect" id="proses_hapustutor" data-id="" rtp="">Yes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalStatusTutor" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>Change Status</b></h3>
                    </div>
                    <div class="modal-body" id="body_status">                  
                        

                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>        
<script type="text/javascript">
    var access_token = "<?php echo $this->session->userdata('access_token');?>";
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
    var dataSet = [];

    $(document).ready(function() {
        $.ajax({
            url: '<?php echo AIR_API;?>listTutorChannel/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id
            },
            success: function(response)
            {
                var a = JSON.stringify(response);  
                var code = response['code'];                
                if (code == 200) {
                    for (var i = 0; i < response.data.length; i++) {
                        var uid = response['data'][i]['uid'];
                        var channel_id = response['data'][i]['channel_id'];
                        var tutor_id = response['data'][i]['tutor_id'];
                        var created_at = response['data'][i]['created_at'];
                        var user_name = response['data'][i]['user_name'];
                        var email = response['data'][i]['email'];
                        var user_callnum = response['data'][i]['user_callnum'];
                        var user_gender = response['data'][i]['user_gender'];
                        var user_address = response['data'][i]['user_address'];  
                        var status = response['data'][i]['status'];
                        if (status =="inactive"){
                            status = "<label style='color:red;'>INCATIVE</label>";
                        }
                        else if (status =="active") {
                            status = "<label style='color:green;'>ACTIVE</label>";
                        }                      
                        var action = "<button class='change_status btn waves-effect' style='background-color: #607D8B;' data-iduser='"+tutor_id+"'><i class='zmdi zmdi-settings' style='color:#fff;'></i></button> <button class='actionbtn btn waves-effect' style='background-color: #607D8B;' data-tutorid='"+tutor_id+"'><i class='zmdi zmdi-delete' style='color:#fff;'></i></button>";
                        dataSet.push([i+1, user_name, email, user_callnum, user_gender, user_address, status, action ],);
                    }

                    $('#tables_listTutor').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="tutorList"></table>' );
             
                    $('#tutorList').dataTable( {
                        "data": dataSet,
                        "columns": [
                            { "title": "No"},
                            { "title": "Name of Tutor"},
                            { "title": "Email"},
                            { "title": "No Telp"},
                            { "title": "Gender"},
                            { "title": "Address"},
                            { "title": "Status"},
                            { "title": "Action" }
                        ]
                    });
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>logout';
                }
                else
                {
                    $('#tables_listTutor').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="tutorList"></table>' );
             
                    $('#tutorList').dataTable( {                        
                        "columns": [
                            { "title": "No"},
                            { "title": "Name of Tutor"},
                            { "title": "Email"},
                            { "title": "No Telp"},
                            { "title": "Gender"},
                            { "title": "Address"},
                            { "title": "Status"},
                            { "title": "Action" }
                        ]
                    });
                }
            }
        }); 
    
       	$(document).on('click', '.actionbtn', function(){
            var tutor_id    = $(this).data('tutorid');
            $('#proses_hapustutor').attr('rtp',tutor_id);
            $("#modalDeleteTutor").modal("show");
        }); 

        $('#proses_hapustutor').click(function(e){
            var rtp = $(this).attr('rtp');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo AIR_API;?>deleteTutor/access_token/"+access_token,
                type:"post",
                data: {
                    rtp: rtp,
                    channel_id: channel_id
                },
                success: function(response){   
                	var code = response['code'];
                	if (code == 200) {
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#text_modal").html("Successfully deleted Tutor");
                        $("#button_ok").click( function(){
                            window.location.reload();
                        });
	                    /*notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Successfully deleted Tutor");
	                    $('#modalDeleteTutor').modal('hide');
	                    setTimeout(function(){
	                    	window.location.reload();
	                    },2000);*/
                    }
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>logout';
                    }   
                    else{  
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#ff3333');
                        $("#text_modal").html("There is an error");
                        $("#button_ok").click( function(){
                            window.location.reload();
                        });                  	
	                    $('#modalDeleteTutor').modal('hide');
	                    /*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"There is an error!!!");
	                    setTimeout(function(){
	                    	window.location.reload();
	                    },2000);*/
                    }                  
                } 
            });
        });

        $(document).on('click', '.change_status', function(){
            $("#body_status").empty();
            var iduser    = $(this).data('iduser');
            $("#modalStatusTutor").modal("show");
            // alert(iduser);
            $.ajax({
                url: '<?php echo AIR_API;?>listTutorChannel/access_token/'+access_token,
                type: 'POST',
                data: {
                    channel_id: channel_id
                },
                success: function(response)
                {
                    var a = JSON.stringify(response);  
                    var code = response['code'];                
                    if (code == 200) {
                        for (var i = 0; i < response.data.length; i++) {
                            var uid = response['data'][i]['uid'];
                            var channel_id = response['data'][i]['channel_id'];
                            var tutor_id = response['data'][i]['tutor_id'];
                            var status = response['data'][i]['status'];
                            if (status=="inactive" && tutor_id == iduser) {
                                var button_status = "<center>"+
                                "<div class='row col-md-12' style='margin-top: 5%;'>"+
                                    "<div class='col-md-6'>"+
                                        "<button type='button' class='btn btn-lg btn-success waves-effect' id='activate' iduser='"+tutor_id+"'>ACTIVATE</button>"+
                                    "</div>"+
                                    "<div class='col-md-6'>"+
                                        "<button type='button' disabled='true' class='btn btn-lg btn-danger waves-effect' id='inactive' iduser='"+tutor_id+"' >NOT ACTIVATE</button>"+
                                    "</div>"+
                                "</div>"+
                            "</center> ";
                                $("#body_status").append(button_status);
                            }
                            else if (status=="active" && tutor_id == iduser) {
                                var button_status = "<center>"+
                                "<div class='row col-md-12' style='margin-top: 5%;'>"+
                                    "<div class='col-md-6'>"+
                                        "<button type='button' disabled='true' class='btn btn-lg btn-success waves-effect' id='activate' iduser='"+tutor_id+"'>ACTIVATE</button>"+
                                    "</div>"+
                                    "<div class='col-md-6'>"+
                                        "<button type='button' class='btn btn-lg btn-danger waves-effect' id='inactive' iduser='"+tutor_id+"' >NOT ACTIVATE</button>"+
                                    "</div>"+
                                "</div>"+
                            "</center> ";
                                $("#body_status").append(button_status);
                            }
                            else{
                                
                            }
                        }

                      
                    }
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>logout';
                    }
                    else
                    {
                       
                    }
                }
            }); 
        });
        $(document).on('click', '#activate', function(){
            var tutor_id = $(this).attr('iduser');
             $.ajax({
                url: '<?php echo AIR_API;?>change_actIn_tutor/access_token/'+access_token,
                type: 'POST',
                data: {
                    tutor_id: tutor_id,
                    channel_id: channel_id
                },
                success: function(response)
                {   
                    var a = JSON.stringify(response);  
                    var code = response['code'];
                    // alert(a);
                    $("#body_status").empty();
                    var button_status = "<center>"+
                        "<div class='row col-md-12' style='margin-top: 5%;'>"+
                            "<div class='col-md-6'>"+
                                "<button type='button' disabled='true' class='btn btn-lg btn-success waves-effect' id='activate' iduser='"+tutor_id+"'>ACTIVATE</button>"+
                            "</div>"+
                            "<div class='col-md-6'>"+
                                "<button type='button'  class='btn btn-lg btn-danger waves-effect' id='inactive' iduser='"+tutor_id+"' >NOT ACTIVATE</button>"+
                            "</div>"+
                        "</div>"+
                    "</center> ";
                        $("#body_status").append(button_status);
                        
                        $("#modalStatusTutor").modal("hide");
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#text_modal").html("Successfully activate");
                        $("#button_ok").click( function(){
                            location.reload();
                        });

                }
            });
        });
        $(document).on('click', '#inactive', function(){
            var tutor_id = $(this).attr('iduser');
            $.ajax({
                url: '<?php echo AIR_API;?>change_actIn_tutor/access_token/'+access_token,
                type: 'POST',
                data: {
                    tutor_id: tutor_id,
                    channel_id: channel_id
                },
                success: function(response)
                    {
                        var a = JSON.stringify(response);  
                        var code = response['code'];
                        $("#body_status").empty();
                        var button_status = "<center>"+
                            "<div class='row col-md-12' style='margin-top: 5%;'>"+
                                "<div class='col-md-6'>"+
                                    "<button type='button'  class='btn btn-lg btn-success waves-effect' id='activate' iduser='"+tutor_id+"'>ACTIVATE</button>"+
                                "</div>"+
                                "<div class='col-md-6'>"+
                                    "<button type='button' disabled='true'  class='btn btn-lg btn-danger waves-effect' id='inactive' iduser='"+tutor_id+"' >NOT ACTIVATE</button>"+
                                "</div>"+
                            "</div>"+
                        "</center> ";
                            $("#body_status").append(button_status);
                            
                            $("#modal_alert").modal('show');
                            $("#modalStatusTutor").modal("hide");
                            $("#modal_konten").css('background-color','#32c787');
                            $("#text_modal").html("Successfully deactivate");
                            $("#button_ok").click( function(){
                                location.reload();
                            });

                    }
            });
        });

        $("select.select2").select2({
            delay: 2000,                    
            maximumSelectionLength: 1,                  
            ajax: {
                dataType: 'json',
                type: 'GET',
                url: '<?php echo AIR_API;?>getTutorName/access_token/'+access_token+'?channel_id='+channel_id,
                data: function (params) {
                    return {
                      term: params.term,
                      page: params.page || 1
                    };
                },
                processResults: function(data){
                    return {
                        results: data.results,
                        pagination: {
                            more: data.more
                        }                       
                    };
                }                   
            }
        });                                         
        
        $('#savetutor').click(function(e){           
            var idtage = $("#tagorang").val()+ '';
            $.ajax({
                url : '<?php echo AIR_API;?>addTutorChannel/access_token/'+access_token,
                type:"post",
                data: {
                    channel_id: channel_id,
                    tutor_id: idtage
                },
                success: function(response){          
                	var code = response['code'];
                	if (code == 200) {
	                    $('#modalAddTutor').modal('hide');
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#text_modal").html("Successfully adds Tutor");
                        $("#button_ok").click( function(){
                            window.location.reload();
                        });
	                    /*notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Berhasil menambah Tutor");
	                    setTimeout(function(){
	                    	window.location.reload();
	                    },2000);*/
                    }
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>logout';
                    }
                    else
                    {
                    	$('#modalAddTutor').modal('hide');
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#ff3333');
                        $("#text_modal").html("There is an error!!!");
                        $("#button_ok").click( function(){
                            window.location.reload();
                        });
	                    /*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"There is an error!!!");
	                    setTimeout(function(){
	                    	window.location.reload();
	                    },2000);*/
                    }
                    
                } 
            });
        });
        
    });
</script>