<style type="text/css">
	::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
	    color: white;
	    opacity: 1; /* Firefox */
	}
</style>
<main class="main">
    <header class="header">
       	<?php $this->load->view("inc/navbar_adm"); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <header class="content__title">
            <h1>Purchase of The Packages</h1>
            <small>Every single package purchased is for 10 members</small>
             <div class="actions">
                <h6>Members on your channel : <strong id="total_member"> -</strong></h6>
            </div>
        </header>  
		<div class="row">
            <div class="col-sm-6 col-md-6 ">
	            <div class="card" style="color: white; background-color: #2196F3">
	                <div class="card-header" align="center">
	                    <h4 class="card-title" style="color: white;"><i>RECOMENDED PACKAGE</i></h4><br>
	                    <label style="font-size: 10vh;" id="paket_available">-</label>
	                    <br>
	                    <a href="#"><button class="buy_package_r btn btn-success">PURCHASE</button></a>
	                </div>
	            </div>                
	        </div>
         	<div class="col-sm-6 col-md-6">
	            <div class="card" style="color: white; background-color: #FF5722">
	                <div class="card-header" align="center">
	                    <h4 class="card-title" style="color: white;"><i>OTHER OPTIONS</i></h4><br>
	                    <input id="count_package" placeholder="..." style="color: white; background-color:#FF5722; border: none;  width: 100px; font-size: 8.5vh; text-align-last: center;" type="number" min="1" step="1" name="count_package" align="center"><br>
	                    <a href="#"><button class="buy_package_m btn btn-success">PURCHASE</button></a><br>
	                    <small>* Please enter the number of packages you wish to purchase in the blank section above</small>

	                </div>

	            </div>                
	        </div>
        </div>
    	<div class="modal " id="modal_package" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h5 class="modal-title">Please choose your Month Session</h5>
                    </div>
                    <div class="modal-body">
                        <form class="new-event__form">
	                    	<div>Start Date :</div>
	                        <div class="input-group">
	                            <div class="form-group">
	                                <input type="text" id="startMonth" class="form-control date-picker floating-label new-event__title"   placeholder="Choose Start Date">
	                                <i class="form-group__bar"></i>
	                            </div>
	                        </div> 
	                        <br>
	                        <div>End Date :</div>
	                        <div class="input-group">
	                            <div class="form-group">
	                                <input type="text" id="endMonth" class="form-control date-picker floating-label new-event__title" placeholder="Choose End Date">
	                                <i class="form-group__bar"></i>
	                            </div>
	                        </div>
                        </form>
                    </div>

                    <div class="modal-footer" style="background-color: #e5e5e5;">                        
                        <button type="button" class="btn btn-link" data-dismiss="modal" style="margin-top: 3%;">Cancel</button>
                        <button type="submit" class="btn btn-success buy_package_ok" style="margin-top: 3%;">PURCHASE</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="animated fadeIn modal fade" style="color: white;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" >
                <div class="modal-content" id="modal_konten">
                    <div class="modal-body" align="center">
                        <label id="text_modal">Halloo</label><br>
                        <button   id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>      
<script type="text/javascript">
	$(document).ready(function() {     
		var student_member = null;
		var tutor_member = null;
		var total_member = null;
		var cek_paket = null;
		var total_paket= null;
		var kuota = null;
		var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
		var access_token = "<?php echo $this->session->userdata('access_token');?>";    
	    $.ajax({
	        url: '<?php echo AIR_API;?>listStudentChannel/access_token/'+access_token,
	        type: 'POST',
	        data: {
	            channel_id: channel_id
	        },
	        success: function(response)
	        {
	            var a = JSON.stringify(response);  
	            var code = response['code'];                
	            if (code == 200) {
	                for (var i = 0; i < response.data.length; i++) {
	                	student_member++;
	                }
	                $.ajax({
			            url: '<?php echo AIR_API;?>listTutorChannel/access_token/'+access_token,
			            type: 'POST',
			            data: {
			                channel_id: channel_id
			            },
			            success: function(response)
			            {
			                var a = JSON.stringify(response);  
			                var codes = response['code'];      
			                // alert(codes);          
			                if (codes == 200) {
			                    for (var i = 0; i < response.data.length; i++) {         
			                       tutor_member++;
			                    }
			                    total_member = student_member+tutor_member
			                    cek_paket = total_member/10;
								total_paket = Math.ceil(cek_paket);
								$("#total_member").text(" "+total_member);
			                    $("#paket_available").text(total_paket);
			                }
			                else if (codes == -400) {
			                    window.location.href='<?php echo base_url();?>logout';
			                }			                 	
			            }
			        });
	            }
	            else if (code == -400) {
	                window.location.href='<?php echo base_url();?>logout';
	            }
	            else
	            {
	            }  	
	        }
	    });    
		$('#pilih_bulan').select2();
	 	$(".buy_package_r").click( function(){
	        $("#modal_package").modal('show');
	        kuota = total_paket
	    });
	    $(".buy_package_m").click( function(){
	    	kuota = $("#count_package").val();
	    	if (kuota == null || kuota == ""){
	       		$("#modal_alert").modal('show');
	            $("#modal_konten").css('background-color','#ff3333');
	            $("#text_modal").html("can not be empty !!!");
	             $("#button_ok").click( function(){
	                $("#modal_alert").modal('hide');
	                $("#modal_package").modal('hide');
	            });	
	    	}
	    	else{
	    		$("#modal_package").modal('show');
	    	}
	    	// alert(kuota);
	    	// return false;
	    });
		$('#startMonth').datetimepicker
	    	({
	            sideBySide: true, 
	            showClose: true,                   
	            format: 'YYYY-MM', 
	            viewMode: "months", 
	            stepping: 15,
	            icons: {
	                time: 'fa fa-clock-o',
	                date: 'fa fa-calendar',
	                up: 'fa fa-angle-up',
	                down: 'fa fa-angle-down',
	                previous: 'fa fa-angle-left',
	                next: 'fa fa-angle-right',
	                today: 'fa fa-dot-circle-o',
	                clear: 'fa fa-trash',
	                close: 'fa fa-times'
	            }
	    });
	    $('#endMonth').datetimepicker
	    	({
	            sideBySide: true, 
	            showClose: true,                   
	            format: 'YYYY-MM',
	            stepping: 15,
	            icons: {
	                time: 'fa fa-clock-o',
	                date: 'fa fa-calendar',
	                up: 'fa fa-angle-up',
	                down: 'fa fa-angle-down',
	                previous: 'fa fa-angle-left',
	                next: 'fa fa-angle-right',
	                today: 'fa fa-dot-circle-o',
	                clear: 'fa fa-trash',
	                close: 'fa fa-times'
	            }
	    });
		$(".buy_package_ok").click( function(){
		
			var date = new Date();
			var bln_ini = moment(date).format("YYYY-MM");
	        var startMonth = $("#startMonth").val();
	        var startDate = moment(startMonth).format("YYYY-MM");
	        var endMonth = $("#endMonth").val();
	        var endDate = moment(endMonth).format("YYYY-MM");
	        // alert(startDate);
	        if (startMonth == "" || endMonth == "") {
	        	$("#modal_package").modal('hide');
	       		$("#modal_alert").modal('show');
	            $("#modal_konten").css('background-color','#ff3333');
	            $("#text_modal").html("Please fill in the date");
	            $("#button_ok").click( function(){
	                $("#modal_package").modal('show');
	            });	
	        }
	        else if (startDate < bln_ini) {
	        	$("#modal_package").modal('hide');
	       		$("#modal_alert").modal('show');
	            $("#modal_konten").css('background-color','#ff3333');
	            $("#text_modal").html("Your Start Date is Invalid");
	            $("#button_ok").click( function(){
	                $("#modal_package").modal('show');
	            });
	        }
	        else if (startDate > endDate) {
	        	$("#modal_package").modal('hide');
	       		$("#modal_alert").modal('show');
	            $("#modal_konten").css('background-color','#ff3333');
	            $("#text_modal").html("Your End Date is later than the Start Date");
	            $("#button_ok").click( function(){
	                $("#modal_package").modal('show');
	            });
	        }
	        else{
	    		$.ajax({
	                url : '<?php echo AIR_API;?>channel_addPackage/access_token/'+access_token,
	                type:"post",
	                data: {
	                	channel_id: channel_id,
						kuota: kuota*10,
						start_date:startDate,
						end_date:endDate
	                },
	                success: function(response){             
	                    var code = response['code'];
	                   	if (code == 200) {
	                   		$("#modal_package").modal('hide');
	                   		$("#modal_alert").modal('show');
	                        $("#modal_konten").css('background-color','#32c787');
	                        $("#text_modal").html("Successfully Buy this Package");
	                        $("#button_ok").click( function(){
	                            location.reload();
	                        });
	                   	}
	                   	else if (code == -400) {
	                        window.location.href='<?php echo base_url();?>logout';
	                    }
	                    else if (code == -100) {
	                   		$("#modal_package").modal('hide');
	                   		$("#modal_alert").modal('show');
	                        $("#modal_konten").css('background-color','#ff3333');
	                        $("#text_modal").html("Can not purchase this package. There're active packages for this month");
	                        $("#button_ok").click( function(){
	                            $("#modal_package").modal('show');
	                   			$("#modal_alert").modal('hide');
	                        });
	                   	}
	                    else{                        
	                        $("#modal_alert").modal('show');
	                        $("#modal_package").modal('hide');
	                        $("#text_modal").html("There is an error!!!");
	                        $("#modal_konten").css('background-color','#ff3333');
	                        // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','There is an error!!!');
	                        $("#button_ok").click( function(){
	                            location.reload();
	                        });

	                    } 
	                }
	            }); 
	        }
	    });
	});
</script>
