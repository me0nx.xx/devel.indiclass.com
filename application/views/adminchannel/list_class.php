<main class="main">

    <header class="header">
       	<?php $this->load->view("inc/navbar_adm"); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <header class="content__title">
            <h1>List Class</h1>
            <small>Details of the class list in your channel</small>            
        </header>

        <div class="row">
            <div class="card col-md-12 col-xs-12">
                <div class="card-block">
                    <div class="row table-responsive">
                        <div id="tables_listClass"></div>
                    </div>
                </div>
            </div>
        
        </div>

        <div class="modal fade show" id="modalChange" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>Change Type</b></h3>
                    </div>
                    <div class="modal-body">                        
                        <label class="c-gray f-15">Please select Status for this class:</label>                        
                        <center>
                            <div class="row col-md-12" style="margin-top: 5%;">
                                <div class="cc-selector">
                                    <input id="onlyinvite" type="radio" name="choose_status" value="1" />
                                    <label class="drinkcard-cc onlyinvite" for="onlyinvite">Registered only</label>
                                    <input id="everyonejoin" type="radio" name="choose_status" value="2" />
                                    <label class="drinkcard-cc everyonejoin" style="color: white;" for="everyonejoin">Everyone can join</label>                                
                                </div>
                            </div>
                        </center>      
                        <style type="text/css">
                            .cc-selector input{
                                margin:0;padding:0;
                                -webkit-appearance:none;
                                   -moz-appearance:none;
                                        appearance:none;
                            }
                            .onlyinvite{background-image:url(https://cdn.classmiles.com/sccontent/baru/logo_baru/tutor_registeronly.png);}
                            .everyonejoin{background-image:url(https://cdn.classmiles.com/sccontent/baru/logo_baru/tutor_everyonecanjoin.png);}

                            .cc-selector input:active +.drinkcard-cc{opacity: .9;}
                            .cc-selector input:checked +.drinkcard-cc{
                                -webkit-filter: none;
                                   -moz-filter: none;
                                        filter: none;
                            }
                            .drinkcard-cc{
                                cursor:pointer;
                                background-size:contain;
                                background-repeat:no-repeat;
                                display:inline-block;
                                width:190px;height:190px;
                                -webkit-transition: all 100ms ease-in;
                                   -moz-transition: all 100ms ease-in;
                                        transition: all 100ms ease-in;
                                -webkit-filter: brightness(1.8) grayscale(1) opacity(.7);
                                   -moz-filter: brightness(1.8) grayscale(1) opacity(.7);
                                        filter: brightness(1.8) grayscale(1) opacity(.7);
                            }
                            .drinkcard-cc:hover{
                                -webkit-filter: brightness(1.2) grayscale(.5) opacity(.9);
                                   -moz-filter: brightness(1.2) grayscale(.5) opacity(.9);
                                        filter: brightness(1.2) grayscale(.5) opacity(.9);
                            }

                            /* Extras */
                            a:visited{color:#888}
                            a{color:#444;text-decoration:none;}
                            p{margin-bottom:.3em;}
                        </style> 
                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success waves-effect" id="proses_choosestatus" data-id="" rtp="">Change</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal CHANGE TUTOR -->  
        <div class="modal fade show" id="modalChangeTutor">
            <div class="modal-dialog" style="z-index: 1050;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title c-black"><b>Change Tutor</b></h3>
                    </div>
                    <div class="modal-body">
                        <p><label class="c-gray f-15">Select Tutor</label></p>                        
                        <select class="select2 form-control" required id="select_listtutor" style="width: 100%;">
                            <option disabled="disabled" selected="" value="0">Select Tutor</option>                            
                        </select>    
                        <!-- <select class="form-control" id="select_listtutor">
                        </select>                     -->
                    </div>                                                
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-link c-gray" style="margin-top: 2%;" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-success" data-id="" id="sbm_changetutor" rtp="" style="margin-top: 2%;">Select</button>
                        
                    </div>                        
                </div>
            </div>
        </div>

    </section>
</main>      

<script type="text/javascript">
    var access_token = "<?php echo $this->session->userdata('access_token');?>";
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
    var dataSet = [];
    var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc;

    $(document).ready(function() {

        $('#select_listtutor').select2();

        $.ajax({
            url: '<?php echo AIR_API;?>channel_classList/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id,
                user_utc: user_utc
            },
            success: function(response)
            {
                var a = JSON.stringify(response);                 
                var code = response['code'];                  
                if (code == 200) {
                    for (var i = 0; i < response.data.length; i++) {
                        var user_name           = response['data'][i]['user_name'];                        
                        var email               = response['data'][i]['email'];
                        var name                = response['data'][i]['name'];
                        var description         = response['data'][i]['description'];
                        var class_type          = response['data'][i]['class_type'];                        
                        var start_time          = response['data'][i]['start_time'];                        
                        var finish_time         = response['data'][i]['finish_time'];
                        var statustype          = response['data'][i]['channel_status'];
                        var tutor_id            = response['data'][i]['tutor_id'];
                        var class_id            = response['data'][i]['class_id'];
                        if (statustype == 1) {
                            textStatusType  = "Registered Student only";
                            buttonAksi      = "";
                        }
                        else if(statustype == 2)
                        {
                            textStatusType  = "Everyone can join";
                            buttonAksi      = "";
                        }
                        else
                        {
                            textStatusType  = "not selected yet";
                            buttonAksi      = "<button class='changeStatusClass btn waves-effect' style='background-color: #ff9800;' data-classid='"+class_id+"'><i class='zmdi zmdi-settings zmdi-hc-fw' style='color:#fff;'></i></button><br><br>";
                        }

                        dataSet.push([i+1, user_name, email, name, description, class_type, start_time, finish_time, textStatusType, buttonAksi+"<button class='changeTutor btn btn-info waves-effect' data-classid='"+class_id+"' data-tutorid='"+tutor_id+"'><img src='https://cdn.classmiles.com/sccontent/baru/logo_baru/change_tutor.png' style='height: 15px; width: 15px;''></button>"]);                        
                    }                        

                    $('#tables_listClass').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="listClass"></table>' );
             
                    $('#listClass').dataTable( {
                        "data": dataSet,
                        "columns": [
                            { "title": "No"},
                            { "title": "Tutor"},
                            { "title": "Email"},
                            { "title": "Subjek Kelas"},
                            { "title": "Deskripsi"},
                            { "title": "Tipe Kelas"},                            
                            { "title": "Kelas Mulai"},
                            { "title": "Kelas Berakhir"},
                            { "title": "Status Kelas"},
                            { "title": "Aksi"}
                        ]
                    });
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>logout';
                }
                else
                {
                    $('#tables_listClass').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="listClass"></table>' );
             
                    $('#listClass').dataTable( {                        
                        "columns": [
                            { "title": "No"},
                            { "title": "Tutor"},
                            { "title": "Email"},
                            { "title": "Subjek Kelas"},
                            { "title": "Deskripsi"},
                            { "title": "Tipe Kelas"},                            
                            { "title": "Kelas Mulai"},
                            { "title": "Kelas Berakhir"},
                            { "title": "Tipe Kelas"},
                            { "title": "Aksi"}
                        ]
                    });
                }
            }
        }); 
    
        $.ajax({
            url: '<?php echo AIR_API;?>listTutor/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id
            },
            success: function(response)
            {
                var a = JSON.stringify(response); 
                if (response['code'] == -400) {
                    window.location.href='<?php echo base_url();?>logout'
                }
                for (var i = 0; i < response.data.length; i++) {
                    var email = response['data'][i]['email'];
                    var user_name = response['data'][i]['user_name'];
                    var tutor_id = response['data'][i]['tutor_id'];
                    // echo '<option value="'.$v['tutor_id'].'">'.$v['user_name'].' - '.$v['email'].'</option>';                    
                    $("#select_listtutor").append("<option value='"+tutor_id+"'>"+user_name+" - "+email+"</option>");
                }
            }
        });

        $(document).on('click', '.changeStatusClass', function(){
            var class_id    = $(this).data('classid');            
            $('#tagorang').attr('channel_id',channel_id);
            $("#channel_id").val(channel_id);
            $('#modal_class_id').val(class_id);        
            $("#proses_choosestatus").attr('rtp',class_id);
            $("#proses_choosestatus").attr('channelid',channel_id);
            $("#modalChange").modal("show");    
        });

        $(document).on('click', '.changeTutor', function(){
            var class_id    = $(this).data('classid');                        
          
            $('#sbm_changetutor').attr('channel_id',channel_id);       
            $("#sbm_changetutor").attr('rtp',class_id);
            $("#modalChangeTutor").modal("show");    
        });

        $("#proses_choosestatus").click(function(){
            var pilihan     = $('input[name=choose_status]:checked').val();
            var rtp         = $(this).attr('rtp');
            var channelid   = $(this).attr('channelid');
            if (pilihan == undefined) {            
                notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Please choose first");
            }       
            else
            {
                $.ajax({
                    url :"<?php echo AIR_API;?>changeStatusClass/access_token/"+access_token,
                    type:"post",
                    data: {
                        pilihan: pilihan,
                        class_id: rtp,
                        channel_id: channelid
                    },
                    success: function(response){             
                        var a = JSON.parse(response);
                        if (a['code'] == 200) {
                            $("#modalChange").modal("hide");
                            if (pilihan == 2) {
                                notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"You have successfully selected 'EVERYONE CAN JOIN' status in this class.");
                            }
                            else
                            {
                                notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"You have successfully selected the status of 'REGISTERED BY CHANNEL' in this class.");   
                            }                            
                            setTimeout(function(){
                                location.reload();
                            },2500);
                        }
                        else if (a['code'] == -400) {
                            window.location.href='<?php echo base_url();?>logout'
                        }
                        else
                        {
                            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"There is an error!!!");
                            $("#modalChange").modal("hide");
                            setTimeout(function(){
                                location.reload();
                            },1000);
                        }
                    }
                });
            }
        });
        
        $(document).on("click", "#sbm_changetutor", function(){
            $('#modalChangeTutor').modal('hide');            
            var tutor_select = $("#select_listtutor").val();
            var rtp           = $(this).attr('rtp');
            var channel_id    = $(this).attr('channel_id');   
            $.ajax({
                url : '<?php echo AIR_API;?>changeTutorClassChannel/access_token/'+access_token,
                type:"post",
                data: {
                    class_id: rtp,
                    tutor_select: tutor_select,
                    channel_id: channel_id
                },
                success: function(response){             
                    var code = response['code'];
                    if (code == 200) {
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Successfully changed Tutor');
                        setTimeout(function(){                            
                            location.reload();
                        },2000);
                    }
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>logout';
                    }
                    else
                    {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','There is an error!!!');
                        setTimeout(function(){
                            location.reload();
                        },2000);
                    }
                }
            });
        });

    });
</script>  