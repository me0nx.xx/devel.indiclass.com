<div class="container">

    <div class="col-sm-4">
    </div>    
    <div class="col-sm-4" style="margin-top:17%;">

        <div class="card brd-2">

            <div class="listview">

                <div class="lv-header bgm-teal">
                    <div class="c-white f-17 m-t-10 m-b-10" style="font-family:sans-serif;"><?php echo $this->lang->line('resendaccount'); ?></div>
                </div>

                <form role="form" action="<?php echo base_url('Master/resendEmail'); ?>" method="post">
                    <div class="lv-body">
                        <div class="lv-item">
                        <?php if($this->session->flashdata('mes_alert')){ ?>
                        <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <?php echo $this->session->flashdata('mes_message'); ?>
                        </div>
                        <?php }else{ ?>
                            <div class="text-center lv-title"><?php echo $this->lang->line('enteremail'); ?></div>
                        <?php } ?>
                        </div>
                        <div class="lv-item">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                                <div class="fg-line">
                                    <input type="email" name="emailaddress" required class="form-control" placeholder="Enter your email address">
                                </div>
                            </div>                                            
                        </div>
                        <div class=" lv-item">                     
                            <button class="pull-right btn bgm-teal" name="sndmail" id="sndmail"><?php echo $this->lang->line('sendemail'); ?></button>                                            
                        </div>
                        <DIV><br></DIV>
                        <div class="lv-item input-group">
                            <a href="<?php echo base_url(); ?>" style="cursor:wait;"><label class="fg-label f-14 m-r-20"><?php echo $this->lang->line('backlogin'); ?></label></a>                    
                        </div>
                    </div>    
                </form>                                                            

            </div>

        </div>

    </div>
    <div class="col-sm-4">                                                 
    </div>  

</div><!-- akhir container -->
<!-- </section> -->

<!-- </section> -->

<!-- Page Loader -->
<div class="page-loader">
    <div class="preloader pls-blue">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p>Please wait...</p>
    </div>
</div>
