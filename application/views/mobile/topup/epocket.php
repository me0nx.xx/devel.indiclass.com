<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('topup/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="margin-top: 3.5%; z-index: 1; position: fixed;">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content" style="margin-top: 5%;">
        <div class="container">            
            <div class="block-header" style="margin-bottom: 50px;">
                <h2 style="margin-left: -2px;"><?php echo $this->lang->line('uangsaku'); ?></h2>
            </div>
            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>
            
            <div class="card m-t-20">
                <div class="card-header">
                    
                    <h2><?php echo $this->lang->line('transactionlist'); ?></h2>
                    <ul class="tab-nav pull-right" role="tablist">                                              
                        <li role="presentation" class="pull-right" style="margin-right: 3%;">
                            <a class="col-xs-4" href="#tab-3" aria-controls="tab-3" role="tab" data-toggle="tab">
                                <?php echo $this->lang->line('paymentconfirmation');?>
                            </a>
                        </li> 
                        <li role="presentation" class="pull-right">
                            <a class="col-xs-4" href="#tab-2" aria-controls="tab-2" role="tab" data-toggle="tab">
                                <?php echo $this->lang->line('menu3'); ?>
                            </a>
                        </li> 
                        <li role="presentation" class="active pull-right">
                            <a class="col-sx-4" href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab">
                               <?php echo $this->lang->line('usebalance'); ?>
                            </a>
                        </li>                       
                    </ul>
                    <hr style="margin-top: 2%;">
                </div>


                <div class="card-body card-padding" style="margin-top: -2%;">                   
                    
                    <div class="tab-content p-20">
                        <div role="tabpanel" class="tab-pane animated fadeIn in active" id="tab-1">
                            <div class="card-body card-padding table-responsive">
                            
                            <label class="f-15" style="margin-top: -3%; margin-bottom: 1%;"><?php echo $this->lang->line('detailusages'); ?></label>
                            <table class="display table table-striped table-bordered data">
                                <thead>
                                    <tr>
                                        <th class="bgm-teal c-white" style="width: 10px">No</th>
                                        <th class="bgm-teal c-white" style="width: 15px"><?php echo $this->lang->line('tanggal'); ?></th>
                                        <th class="bgm-teal c-white" style="width: 30px"><?php echo $this->lang->line('usebalance'); ?></th>
                                        <th class="bgm-teal c-white" style="width: 20px"><?php echo $this->lang->line('saldoawal'); ?></th>
                                        <th class="bgm-teal c-white" style="width: 20px">Harga</th>
                                        <th class="bgm-teal c-white" style="width: 20px"><?php echo $this->lang->line('saldoakhir'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 1;
                                        $user = $this->session->userdata('id_user');
                                        // $query2 = $this->db->query("SELECT lm.payment_type, lm.status_message, lo.* FROM log_midtrans_transaction as lm INNER JOIN log_order as lo ON lm.order_id=lo.order_id WHERE lo.id_user='$user' ORDER BY lo.timestamp DESC")->result_array();
                                        $query2 = $this->db->query("SELECT lct.*, tc.* FROM log_class_transaction as lct INNER JOIN tbl_class as tc ON lct.class_id=tc.class_id WHERE id_user='$user' ORDER BY lct.created_at DESC")->result_array();
                                        if (empty($query2)) {
                                            ?>
                                        <tr>
                                            <td colspan='8'><center><?php echo $this->lang->line('notransaction'); ?></center></td>
                                        </tr>
                                        <?php
                                        }
                                        else
                                        {     
                                        foreach ($query2 as $row => $v) {
                                            $last_balance = number_format($v['last_balance'], 0, ".", ".");
                                            $amount = number_format($v['amount'], 0, ".", ".");
                                            $new_balance = number_format($v['new_balance'], 0, ".", ".");
                                         
                                            $date = date_create($v['created_at']);
                                            $datee = date_format($date, 'd/m/y');
                                            $hari = date_format($date, 'd');
                                            $tahun = date_format($date, 'Y');
                                                                                    
                                            $date = $datee;
                                            $sepparator = '/';
                                            $parts = explode($sepparator, $date);
                                            $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));
                                        ?>
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $hari.' '. $bulan. ' '.$tahun ?></td>
                                            <td>Membeli pelajaran <?php echo $v['class_type']." ".$v['name']; ?>.</td>
                                            <td>Rp. <?php echo $last_balance; ?></td>
                                            <td>Rp. <?php echo $amount; ?></td>
                                            <td>Rp. <?php echo $new_balance; ?></td>
                                        </tr>                                    
                                        <?php
                                            $no++;
                                            }                                    
                                        } 
                                    ?>
                                </tbody>
                            </table>
                          </div>  
                        </div>
                        
                        <div role="tabpanel" class="tab-pane animated fadeIn" id="tab-2">
                            <div class="card-body card-padding table-responsive">

                              <label class="f-15" style="margin-top: -3%; margin-bottom: 1%;"><?php echo $this->lang->line('detailtopup'); ?></label>
                              <table class="display table table-striped table-bordered data">
                                <thead>
                                    <tr>
                                        <th class="bgm-teal c-white" style="width: 10px">No</th>
                                        <th class="bgm-teal c-white" style="width: 10px">No Transaksi</th>
                                        <th class="bgm-teal c-white text-center" style="width: 20px"><?php echo $this->lang->line('tanggal'); ?></th>
                                        <th class="bgm-teal c-white text-center" style="width: 15px"><?php echo $this->lang->line('jumlah'); ?></th>
                                        <th class="bgm-teal c-white text-center" style="width: 30px"><?php echo $this->lang->line('keterangan'); ?></th>
                                        <th class="bgm-teal c-white text-center" style="width: 30px"><?php echo $this->lang->line('metodepembayaran'); ?></th>
                                        <th class="bgm-teal c-white text-center" style="width: 20px">Status</th>
                                        <th class="bgm-teal c-white text-center" style="width: 20px"><?php echo $this->lang->line('aksi'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $no = 1;
                                        $user = $this->session->userdata('id_user');
                                        // $query2 = $this->db->query("SELECT lm.payment_type, lm.status_message, lo.* FROM log_midtrans_transaction as lm INNER JOIN log_order as lo ON lm.order_id=lo.order_id WHERE lo.id_user='$user' ORDER BY lo.timestamp DESC")->result_array();
                                        $query2 = $this->db->query("SELECT * FROM log_transaction WHERE id_user='$user' ORDER BY timestamp DESC")->result_array();
                                        if (empty($query2)) {
                                            ?>
                                        <tr>
                                            <td colspan='8'><center><?php echo $this->lang->line('notransaction'); ?></center></td>
                                        </tr>
                                        <?php
                                        }
                                        else
                                        {     
                                        foreach ($query2 as $row => $v) {
                                            $hasilkredit = number_format($v['credit'], 0, ".", ".");

                                          
                                        $date = date_create($v['timestamp']);
                                        $datee = date_format($date, 'd/m/y');
                                        $hari = date_format($date, 'd');
                                        $tahun = date_format($date, 'Y');
                                                                                
                                        $date = $datee;
                                        $sepparator = '/';
                                        $parts = explode($sepparator, $date);
                                        $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));

                                    ?>
                                    <tr>
                                        <td><?php echo($no); ?></td>
                                        <td><?php echo($v['trx_id']); ?></td>
                                        <td><?php echo $hari.' '. $bulan. ' '.$tahun ?></td>
                                        <td>Rp. <?php echo($hasilkredit); ?></td>
                                        <td><?php echo($v['keterangan']); ?></td>
                                        <td><?php 
                                            if($v['payment_method'] == 'credit_card')
                                            { 
                                                echo 'Credit Card' ;
                                            }
                                            else 
                                            { 
                                                echo 'Transfer Bank';
                                            } 
                                            ?>
                                        </td>
                                        <td>
                                            <?php 
                                                if ($v['flag'] == '1')
                                                {
                                                    echo "<label class='c-green'>".$this->lang->line('suksestransaction')."</label>"; 
                                                }
                                                else
                                                {
                                                    echo "<label class='c-red'>".$this->lang->line('pendingtransaction')."</label>";
                                                }    
                                            ?>
                                        </td> 
                                        <td>
                                            <center>                                                           
                                                <?php
                                                    if ($v['flag'] == '1')
                                                    {
                                                        ?>
                                                        <a href="<?php echo base_url('Sukses?order_id='.$v['trx_id']); ?>">
                                                            <button class="btn bgm-bluegray m-l-5 col-md-12 btn-sm"><?php echo $this->lang->line('menu1'); ?></button>
                                                        </a>

                                                        <?php
                                                    }
                                                    else
                                                    {
                                                        ?>
                                                        <a href="<?php echo base_url('Sukses?order_id='.$v['trx_id']); ?>">
                                                            <button class="btn bgm-bluegray m-l-5 col-md-12 btn-sm"><?php echo $this->lang->line('menu1'); ?></button>
                                                        </a>
                                                        <?php
                                                    }
                                                ?>                                                
                                            </center>
                                        </td>                             
                                    </tr>
                                    <?php
                                        $no++;
                                        }
                                         
                                    } 
                                    ?>
                                </tbody>
                              </table>
                          </div>                          
                        </div>  

                        <div role="tabpanel" class="tab-pane animated fadeIn" id="tab-3">
                            <div class="card-body card-padding table-responsive">

                              <label class="f-15" style="margin-top: -3%; margin-bottom: 1%;"><?php echo $this->lang->line('paymentconfirmation');?></label>    
                              <table class="display table table-striped table-bordered data">
                                <thead>
                                    <tr>
                                        <th class="bgm-teal c-white" style="width: 10px">No</th>
                                        <th class="bgm-teal c-white text-center" style="width: 10px"><?php echo $this->lang->line('tanggal'); ?></th>
                                        <th class="bgm-teal c-white text-center" style="width: 15px"><?php echo $this->lang->line('jumlah'); ?></th>
                                        <th class="bgm-teal c-white text-center" style="width: 30px"><?php echo $this->lang->line('keterangan'); ?></th>
                                        <th class="bgm-teal c-white text-center" style="width: 20px">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    $no = 1;
                                    $user = $this->session->userdata('id_user');
                                    $data = $this->db->query("SELECT *, timestamp + INTERVAL 2 DAY as timelimit FROM log_transaction WHERE id_user='$user' AND flag='0' AND (timestamp + INTERVAL 2 DAY)>NOW()")->result_array();
                                    if (empty($data)) {
                                        ?>
                                        <tr>
                                            <td colspan='8'><center><?php echo $this->lang->line('tidakadadata'); ?></center></td>
                                        </tr>
                                        <?php
                                    }
                                    else
                                    {
                                        foreach ($data as $row => $d) {

                                            $hasiljumlah = number_format($d['credit'], 0, ".", ".");
                                            $date = date_create($d['timestamp']);
                                            $datee = date_format($date, 'd/m/y');
                                            $hari = date_format($date, 'd');
                                            $tahun = date_format($date, 'Y');
                                                                                    
                                            $date = $datee;
                                            $sepparator = '/';
                                            $parts = explode($sepparator, $date);
                                            $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));

                                            //untuk timelimit 
                                            $datelimit = date_create($d['timelimit']);
                                            $dateelimit = date_format($datelimit, 'd/m/y');
                                            $harilimit = date_format($datelimit, 'd');
                                            $tahunlimit = date_format($datelimit, 'Y');
                                            $waktulimit = date_format($datelimit, 'H:s');
                                                                                    
                                            $datelimit = $dateelimit;
                                            $sepparatorlimit = '/';
                                            $partslimit = explode($sepparatorlimit, $datelimit);
                                            $bulanlimit = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

                                            $cekudhconfrimbelum = $this->db->query("SELECT * FROM log_trf_confirmation WHERE trx_id='$d[trx_id]' AND id_user='$d[id_user]'")->result_array();                                           
                                ?>
                                    <tr>
                                        <td><?php echo($no); ?></td>
                                        <td><?php echo $hari.' '. $bulan. ' '.$tahun ?></td>
                                        <td>Rp. <?php echo($hasiljumlah); ?></td>
                                        <td>Mohon transfer sebelum tanggal <?php echo $harilimit.' '. $bulanlimit. ' '.$tahunlimit. ' pada pukul '.$waktulimit ?></td>
                                        <td>
                                        <?php
                                            if ($cekudhconfrimbelum) {                                                
                                                ?>
                                                <center><a href="<?php echo base_url('Konfirmasi?transaksiid='.$d['trx_id'].'&trans=edt'); ?>"><button class="btn bgm-bluegray m-l-5 col-md-6 btn-sm" id="editkonfirmasi"><?php echo $this->lang->line('ubahkonfirmasi'); ?></button></a>
                                            <?php
                                            }
                                            else
                                            {                                               
                                        ?>
                                            <center><a href="<?php echo base_url('Konfirmasi?transaksiid='.$d['trx_id'].'&trans=knf'); ?>"><button class="btn bgm-bluegray m-l-5 col-md-6 btn-sm" id="konfirmasi"><?php echo $this->lang->line('konfirmasi'); ?></button></a>
                                        <?php 
                                            }
                                        ?>
                                        <a href="<?php echo base_url('Details?transaksiid='.$d['trx_id']); ?>"><button class="btn bgm-bluegray m-l-5 col-md-5 btn-sm"><?php echo $this->lang->line('menu1'); ?></button></a></center></td>
                                    </tr>                                    
                                </tbody>
                                <?php 
                                    $no++;
                                    }
                                }
                                ?>
                              </table>                          
                          </div>
                        </div>

                    </div>
                </div>                      

            </div>
        </div>            

    </div>
  </section>
</section>

<footer id="footer">
  <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    
    // $('#pencarian').on('change', function() {
    //     var value = this.value;   
    //     if (value == 'topup') {     
    //       $("#penggunaan").css('display','none');
    //       $("#topup").css('display','block');
    //     }
    //     else if (value == 'penggunaan'){
    //       $("#topup").css('display','none');
    //       $("#penggunaan").css('display','block');
    //     }
    // });
    $('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable();
</script>
<script type="text/javascript">
    
</script>