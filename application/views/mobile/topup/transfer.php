<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('topup/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="margin-top: 3.5%; z-index: 1; position: fixed;">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content" style="margin-top: 5%;">
        <div class="container invoice">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2><?php echo $this->lang->line('detailtopup'); ?></h2>
                <!-- <ul class="actions">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><a href="<?php echo base_url();?>first/topup">Top up</a></li>
                        <li><?php echo $this->lang->line('detailtopup'); ?></li>
                    </ol>                    
                </ul> -->
            </div>
<!-- 
            <div class="card m-t-20">
                <div class="card-header">
                    <h2>Rincian Top up Saldo</h2><small class="m-t-15">Transaksi tanggal 16-12-2016</small>
                    <hr>               
                </div>

                <div class="card-header" style="margin-top: -3%;">
                    
                    <div class="row">
                        
                    </div>
                    <hr>
                    <div class="pull-left" style="margin-top: -10px;">
                    	  <button class="btn btn-md c-black btn-block m-t-5 m-r-20" style="background: #f9f9f9; border: 1px solid gray;">Kembali</button>                 
                    </div>
                </div>


                <hr>
            </div>  -->          

            <?php 
                $orderid = $this->session->userdata('idorder');

                $getdatatransaksi = $this->db->query("SELECT * FROM log_transaction WHERE trx_id='$orderid'")->result_array();
                foreach ($getdatatransaksi as $row => $v) {
                    $hasiljumlah = number_format($v['credit'], 0, ".", ".");

                    $datelimit = date_create($v['timelimit']);
                    $dateelimit = date_format($datelimit, 'd/m/y');
                    $harilimit = date_format($datelimit, 'd');
                    $hari = date_format($datelimit, 'l');
                    $tahunlimit = date_format($datelimit, 'Y');
                    $waktulimit = date_format($datelimit, 'H:s');
                                                            
                    $datelimit = $dateelimit;
                    $sepparatorlimit = '/';
                    $partslimit = explode($sepparatorlimit, $datelimit);
                    $bulanlimit = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
            ?>
            <div class="card">
                <div class="card-header ch-alt">
                    <div class="pull-left f-14">No <?php echo $this->lang->line('transaksi'); ?> <?php echo $v['trx_id']; ?></div>
                    <div class="pull-right f-14"><?php echo $this->lang->line('transaksi'); ?> <?php echo $this->lang->line('tanggal'); ?> <?php echo $v['timestamp']; ?></div><br>
                </div>
                
                <div class="card-body card-padding" style="background: white;">
                    <div class="row m-b-25 text-center">
                        
                        <div class="col-xs-12">
                            <div class="i-to">
                                <p class="c-gray"><?php echo $this->lang->line('segera'); ?></p>

                                <div style="background: #edecec; width: 30%; height: 50px; margin-left: 35%;">
                                    <!-- <label style="margin-top: 5%;" class="f-14">Kamis, 08 Desember 2016 Pukul 10:00 WIB</label> -->
                                    <label style="margin-top: 5%;" class="f-14"><?php echo $hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit. ' Pukul '.$waktulimit ?></label>
                                </div>                                
                                
                                <h6 class="c-gray m-t-15"><?php echo $this->lang->line('totalpayment'); ?></h6>
                                <h1>Rp. <?php echo $hasiljumlah; ?></h1>
                                
                                <span class="text-muted">
                                    <address>
                                        <?php echo $this->lang->line('bedatransfer'); ?>
                                    </address>        
                                </span>

                                <!-- <form action="<?php echo site_url()?>/vtweb/vtweb_checkout" method="POST" id="payment-form"> -->
                                    <a href="<?php echo base_url('Konfirmasi?transaksiid='.$v['trx_id'].'&trans=knf');?>"><button class="btn btn-lg bgm-blue"><?php echo $this->lang->line('konfirmasipembayaran'); ?></button></a>
                                    <!-- <button class="btn btn-lg bgm-blue" type="submit">Konfirmasi Pembayaran</button> -->
                                <!-- </form> -->
                            </div>
                        </div>
                        
                    </div>
                    <hr>
                    <p class="c-gray text-center"><?php echo $this->lang->line('pembayaranmelalui'); ?></p>
                    <div class="clearfix"></div>
                    
                    <div class="row m-t-25 p-0 m-b-25">

                    	<div class="col-xs-1"></div>
                        <div class="col-xs-2">
                            <div class="brd-2 p-15 text-center" style="margin-top: -10%;">
                                <div class="c-white m-b-5"><img style="margin-left: -20%;" src="<?php echo base_url();?>aset/images/banklogo/bca1.png"></div>
                                <h2 class="m-0 c-gray f-12">Bank BCA, Jakarta
                                <h2 class="m-0 c-gray f-12">254 689 2003 </h2>
                                <h2 class="m-0 c-gray f-12">a.n. Classmiles.com</h2>
                            </div>
                        </div>
                        
                        <div class="col-xs-2">
                            <div class="brd-2 p-15 text-center" style="margin-top: -10%;">
                                <div class="c-white m-b-5"><img style="margin-left: -20%;" src="<?php echo base_url();?>aset/images/banklogo/bni1.png"></div>
                                <h2 class="m-0 c-gray f-12">Bank BNI, Jakarta
                                <h2 class="m-0 c-gray f-12">0381486211 </h2>
                                <h2 class="m-0 c-gray f-12">a.n. Classmiles.com</h2>
                            </div>
                        </div>
                        
                        <div class="col-xs-2">
                            <div class="brd-2 p-15 text-center" style="margin-top: -10%;">
                                <div class="c-white m-b-5"><img style="margin-left: -20%;" src="<?php echo base_url();?>aset/images/banklogo/mandiri1.png"></div>
                                <h2 class="m-0 c-gray f-12">Bank Mandiri, Jakarta
                                <h2 class="m-0 c-gray f-12">1290010901359 </h2>
                                <h2 class="m-0 c-gray f-12">a.n. Classmiles.com</h2>
                            </div>
                        </div>
                        
                        <div class="col-xs-2">
                            <div class="brd-2 p-15 text-center" style="margin-top: -10%;">
                                <div class="c-white m-b-5"><img style="margin-left: -25%;" src="<?php echo base_url();?>aset/images/banklogo/bri1.png"></div>
                                <h2 class="m-0 c-gray f-12">Bank BRI, Jakarta
                                <h2 class="m-0 c-gray f-12">580 475 900 684 136 </h2>
                                <h2 class="m-0 c-gray f-12">a.n. Classmiles.com</h2>
                            </div>
                        </div>

                        <div class="col-xs-2">
                            <div class="brd-2 p-15 text-center" style="margin-top: -10%;">
                                <div class="c-white m-b-5"><img style="margin-left: -25%;" src="<?php echo base_url();?>aset/images/banklogo/cimb1.png"></div>
                                <h2 class="m-0 c-gray f-12">Bank CIMB, Jakarta
                                <h2 class="m-0 c-gray f-12">5454 2212 3333 </h2>
                                <h2 class="m-0 c-gray f-12">a.n. Classmiles.com</h2>
                            </div>
                        </div>
                        <div class="col-xs-1"></div>

                    </div>                  
                    <hr>
                    <div class="clearfix"></div>
                    
                    <div class="p-25">
                        <h4 class="c-green f-400"><?php echo $this->lang->line('notes'); ?> :</h4>                        
                        <blockquote class="m-t-5">
                            <p class="c-gray f-12"><?php echo $this->lang->line('notesdes'); ?> <?php echo $waktulimit; ?> WIB <?php echo $this->lang->line('hari'); ?> <?php echo $hari; ?>, <?php echo $harilimit.' '. $bulanlimit. ' '.$tahunlimit ?> <?php echo $this->lang->line('notpaid'); ?></p>
                        </blockquote>                                                                        
                    </div>
                </div>
                
                <footer class="p-20">
                    <a href="<?php echo base_url();?>Topup"><button class="btn btn-md c-black btn-block m-t-5 m-r-20" style="background: #f9f9f9; border: 1px solid gray;"><?php echo $this->lang->line('kembali'); ?></button></a>
                </footer>
            </div>
            <?php 
            }
            ?>

        </div>
    </section>

</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>