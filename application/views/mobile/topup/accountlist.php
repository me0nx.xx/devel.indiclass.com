<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('topup/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="margin-top: 3.5%; z-index: 1; position: fixed;">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content" style="margin-top: 5%;">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2><?php echo $this->lang->line('accountlist'); ?></h2>
                <!-- <ul class="actions hidden-xs">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><?php echo $this->lang->line('accountlist'); ?></li>
                    </ol>                    
                </ul> -->
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="card m-t-20 p-20" style="">
                <div class="card-header">
                    <div class="pull-left"><h2>List Rekening</h2></div>
                    <div class="pull-right"><a data-toggle="modal" href="#modalTambah"><button class="btn btn-success">+ Tambah Rekening Bank</button></a></div>
                <br><br>
                <hr>

                <div class="row" style="margin-top: 2%; overflow-y: auto; height: 85vh;">
                <?php
                    $user = $this->session->userdata('id_user');
                    $getrekening = $this->db->query("SELECT mb.bank_logo, tr.* FROM tbl_rekening as tr LEFT JOIN master_bank as mb ON tr.bank_id=mb.bank_id WHERE id_user='$user'")->result_array();
                    if (empty($getrekening)) {
                        ?>
                    <center>Tidak ada Data Rekening</center>
                    <?php
                    }
                    else
                    {
                        foreach ($getrekening as $row => $v) {
                            $logobank = $v['bank_logo'];
                            $getbankid = $v['bank_id'];
                        ?>
                    <div class="col-md-12" >
                        <div class="card-body card-padding" style="background-color:#EEEEEE;"> 
                            <div class="pull-left">
                                <img src="<?php echo base_url(); ?>aset/images/banklogo/<?php echo $logobank; ?>" width="130" height="40">
                            </div>                           
                            <div class="pull-right">
                                <a href="<?php echo base_url(); ?>EditBank?idrn=<?php echo $v['id_rekening'] ?>"><button class="btn bgm-bluegray" title="Edit Rekening"><i class="zmdi zmdi-edit"></i></button></a>
                                <a data-toggle="modal" data-id="<?php echo $v['id_rekening']; ?>" class="hapusrekening" data-target-color="green" href="#modalDelete"><button class="btn bgm-bluegray" title="Hapus Rekening"><i class="zmdi zmdi-delete"></i></button></a>
                            </div>
                            <br><br>
                            <table class="table table-inner table-vmiddle m-t-10">
                                <thead>
                                    <tr>
                                        <th class="bgm-teal c-white" style="width: 10px">Nama Pemilik Rekening</th>
                                        <th class="bgm-teal c-white" style="width: 15px">No Rekening</th>
                                        <th class="bgm-teal c-white" style="width: 30px">Nama Bank</th>
                                        <th class="bgm-teal c-white" style="width: 20px">Cabang</th>                                        
                                    </tr>
                                </thead>
                                <tbody>                                        
                                    <tr>
                                        <td><?php echo $v['nama_akun'] ?></td>
                                        <td><?php echo $v['nomer_rekening'] ?></td>
                                        <td><?php echo $v['nama_bank'] ?></td>
                                        <td><?php echo $v['cabang'] ?></td>
                                    </tr>                                             
                                </tbody>
                            </table>                            
                        </div>
                    </div>
                    <?php
                            }
                        }
                    ?>                                 

                </div> 
                                            
            </div> 

            <!-- Modal TAMBAH -->  
            <div class="modal fade" id="modalTambah" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Tambah Akun Rekening</h4>
                            <hr>
                        </div>                        
                        <div class="modal-body">
                            <div class="card">
                            <div class="row p-15">                                 
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Nama Akun</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <input type="text" name="namaakun" id="namaakun" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Nomer Rekening</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <input type="text" name="norekening" id="norekening" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Nama Bank</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <div class="select">
                                                    <select required name="namabank" id="namabank" class="selectpicker" data-live-search="true" style="z-index: 10;">
                                                    <option disabled selected>Pilih Bank</option>   
                                                    <?php
                                                       $allbank = $this->db->query("SELECT * FROM master_bank")->result_array();
                                                       foreach ($allbank as $row => $v) {
                                                            echo '<option value="'.$v['bank_id'].'">'.$v['bank_name'].'</option>';
                                                        }
                                                    ?>                                                    
                                                    </select>
                                                </div>

                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Cabang</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <input type="text" name="cabangbank" id="cabangbank" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn bgm-green" id="simpandatarekening">Simpan</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>                              

            <!-- Modal DELETE -->  
            <div class="modal fade" id="modalDelete" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Hapus Akun Bank</h4>
                            <hr>
                            <p><label class="c-gray f-15">Apakah anda yakin ingin menghapus</label></p>
                            <input type="text" name="id_rekeningg" id="id_rekeningg" class="c-black" value="" hidden />
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" id="hapusdatarekening">Ya</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>
            
        </div>
    </section>

</section>
<script type="text/javascript">

    // $('#simpandatarekening').click(function(){
    //     alert('test');
    // });
    $(document).ready(function(){
        $('#simpandatarekening').click(function(e){
            var namaakun = $("#namaakun").val();
            var norekening = $("#norekening").val();
            var namabank = $("#namabank").val();
            var cabangbank = $("#cabangbank").val();
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/addrekening",
                type:"post",
                data: "namaakun="+namaakun+"&norekening="+norekening+"&namabank="+namabank+"&cabangbank="+cabangbank,
                success: function(html){             
                    alert("Berhasil Menambahkan akun rekening bank");
                    $('#modalTambah').modal('hide');
                    location.reload();                     
                } 
            });
         });

        $('#hapusdatarekening').click(function(e){
            var id_rekening = $("#id_rekeningg").val();
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/deleterekening",
                type:"post",
                data: "id_rekening="+id_rekening,
                success: function(html){             
                    alert("Berhasil menghapus data rekening");
                    $('#modalDelete').modal('hide');
                    location.reload();                     
                } 
            });
        });
    });
    $(document).on("click", ".hapusrekening", function () {
         var myBookId = $(this).data('id');
         $(".modal-header #id_rekeningg").val( myBookId );
    });   
</script>
