<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('topup/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="margin-top: 3.5%; z-index: 1; position: fixed;">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content" style="margin-top: 5%;">
        <div class="container invoice">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2><?php echo $this->lang->line('detailtopup'); ?></h2>
                <!-- <ul class="actions">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><a href="<?php echo base_url();?>first/topup">Top up</a></li>
                        <li><?php echo $this->lang->line('detailtopup'); ?></li>
                    </ol>                    
                </ul> -->
            </div>
<!-- 
            <div class="card m-t-20">
                <div class="card-header">
                    <h2>Rincian Top up Saldo</h2><small class="m-t-15">Transaksi tanggal 16-12-2016</small>
                    <hr>               
                </div>

                <div class="card-header" style="margin-top: -3%;">
                    
                    <div class="row">
                        
                    </div>
                    <hr>
                    <div class="pull-left" style="margin-top: -10px;">
                          <button class="btn btn-md c-black btn-block m-t-5 m-r-20" style="background: #f9f9f9; border: 1px solid gray;">Kembali</button>                 
                    </div>
                </div>


                <hr>
            </div>  -->          
            <?php 
                $orderid = $_GET['order_id'];

                // $getdatatransaksi = $this->db->query("SELECT lt.*, lmt.pdf_url FROM log_transaction as lt INNER JOIN log_midtrans_transaction AS lmt ON lt.trx_id=lmt.order_id WHERE lt.trx_id='$orderid'")->result_array();
                $getdatatransaksi = $this->db->query("SELECT * FROM log_transaction WHERE trx_id='$orderid'")->result_array();
                foreach ($getdatatransaksi as $row => $v) {
                    $hasiljumlah = number_format($v['credit'], 0, ".", ".");

                    $datelimit = date_create($v['timelimit']);
                    $dateelimit = date_format($datelimit, 'd/m/y');
                    $harilimit = date_format($datelimit, 'd');
                    $hari = date_format($datelimit, 'l');
                    $tahunlimit = date_format($datelimit, 'Y');
                    $waktulimit = date_format($datelimit, 'H:s');
                                                            
                    $datelimit = $dateelimit;
                    $sepparatorlimit = '/';
                    $partslimit = explode($sepparatorlimit, $datelimit);
                    $bulanlimit = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
            ?>
            <div class="card">
                <div class="card-header ch-alt">
                    <div class="col-md-4 f-14" style="text-align: left;">
                        No <?php echo $this->lang->line('transaksi'); ?> <?php echo $v['trx_id']; ?>
                    </div>
                    <div class="col-md-4 f-20" style="text-align: center;">
                        <?php 
                            if ($v['flag'] == 1) {
                                ?>
                                <?php echo $this->lang->line('suksestransaction'); ?>
                                <?php
                            }
                            else
                            {
                        ?>
                            <?php echo $this->lang->line('pendingtransaction'); ?>                       
                        <?php 
                            }
                        ?>
                    </div>
                    <div class="col-md-4 f-14" style="text-align: right;">
                        <?php echo $this->lang->line('transaksi'); ?> <?php echo $this->lang->line('tanggal'); ?> <?php echo $v['timestamp']; ?>
                    </div>
                    <br>
                </div>
                
                <div class="card-body card-padding" style="background: white;">
                    <div class="row m-b-25 text-center">
                        
                        <div class="col-xs-12">
                            <div class="i-to">
                                <?php 
                                    if ($v['payment_method'] == "credit_card") {
                                    ?>
                                    <h5 class="c-gray m-t-15"><?php echo $this->lang->line('totalpayment'); ?></h5>
                                    <?php
                                    }
                                    else
                                    {
                                ?>
                                    <p class="c-gray"><?php echo $this->lang->line('segera'); ?></p> 
                                    <div style="background: #edecec; width: 30%; height: 50px; margin-left: 35%;">
                                        <label style="margin-top: 5%;" class="f-14"><?php echo $hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit. ' Pukul '.$waktulimit ?></label>
                                    </div>                   
                                
                                                                                        
                                <h6 class="c-gray m-t-15"><?php echo $this->lang->line('totalpayment'); ?></h6>
                                <?php 
                                    }
                                ?>
                                <h1>Rp. <?php echo $hasiljumlah; ?></h1>
                                
                                <?php 
                                    if ($v['payment_method'] == "credit_card") {                                        
                                    }
                                    else
                                    {
                                ?>
                                    <span class="text-muted">
                                        <address>
                                            <?php echo $this->lang->line('bedatransfer'); ?>
                                        </address>        
                                    </span>                 
                                <?php 
                                    }
                                ?>                                

                                <?php
                                    if ($v['payment_method'] == "credit_card") {
                                        
                                    }
                                    else
                                    {
                                ?>
                                    <a target="_blank" href="<?php echo $v['pdf_url']; ?>" ><button class="btn btn-lg bgm-blue">Panduan Pembayaran</button></a>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        
                    </div>
                                                        
                    <hr>
                    <div class="clearfix"></div>
                    
                    <?php 
                        if ($v['payment_method'] == "credit_card") {
                        ?>
                        <div class="p-25">
                            <h4 class="c-green f-400"><?php echo $this->lang->line('notes'); ?> :</h4>                        
                            <blockquote class="m-t-5">
                                <p class="c-gray f-12">Pembayaran anda berhasil, Total saldo akan secara otomatis bertambah. </p>
                            </blockquote>                                                                        
                        </div>
                        <?php                                        
                        }
                        else
                        {
                        ?>
                        <div class="p-25">
                            <h4 class="c-green f-400"><?php echo $this->lang->line('notes'); ?> :</h4>                        
                            <blockquote class="m-t-5">
                                <p class="c-gray f-12">Mohon untuk melakukan pembayaran dalam 1x24 Jam. Sistem akan otomatis membatalkan transaksi anda jika sudah lewat dari batas waktu. </p>
                            </blockquote>                                                                        
                        </div>
                        <?php 
                        }
                    ?>
                </div>
                
                <footer class="p-20">
                    <a href="<?php echo base_url();?>Epocket"><button class="btn btn-md c-black btn-block m-t-5 m-r-20" style="background: #f9f9f9; border: 1px solid gray;"><?php echo $this->lang->line('kembali'); ?></button></a>
                </footer>
            </div>
            <?php 
            }
            ?>

        </div>
    </section>

</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<!-- <meta http-equiv="refresh" content="15; url=https://classmiles.com/first/epocket" /> -->
<!-- <section id="main">

    <section id="content">
        <div class="container">
            
            <div class="card m-t-20 m-b-20">
                
                <div class="card-header">
                
                </div>

                <div class="card-body card-padding">
                    <center>
                        <img style="margin-top: 8%;" src="<?php echo base_url();?>aset/images/transaksisukses.png" width="150px" height="150px"><br><br>
                        <label class="text-center" style="margin-top: 3%; font-size: 26px; margin-bottom: 10%;"><?php echo $this->lang->line('suksestransaction'); ?><br></label>
                    </center>
                </div>

            </div>            

        </div>
    </section>

</section>
 -->
