<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>


<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">

        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="container">        
 
            <div class="card m-b-10 z-depth-4">
                <div class="page-header card-header">
                
                    <div class="pull-left">                         
                        <h2><i class="glyphicon glyphicon-time m-r-10"></i>History</h2>
                    </div>
                    <div class="pull-right" style="margin-top:-10px;" data-toggle="tooltip" data-placement="right" title="Refresh">
                    <!-- <input type="text" id="userInput"=>give me input</input> -->
                        <button class="btn btn-icon"  onclick="test()"><i class="zmdi zmdi-refresh"></i></button>
                        <!-- <div style="float:right;" id="btn m-l-10">
                            <button class="btn btn-primary">Sort</button>
                        </div>
                        <div style="float:right; margin-right:15px; width:150px;">
                            <div class="col-sm-12">
                                <input type='text' class="form-control time-picker" placeholder="Search Time...">
                            </div>
                        </div>
                        <div style="float:right; margin-right:0px; width:150px;">
                            <div class="col-sm-12">
                                <input type='text' class="form-control date-picker" data-date-format="DD-MM-YYYY" placeholder="Search Date...">
                            </div>
                        </div> -->
                    </div>
                <br>
            </div>
            <ul class="timeline" style="margin:20px;">
            <?php 

                $query2 = $this->db->query("SELECT tbl_user. * , log_user. * FROM tbl_user, log_user WHERE tbl_user.id_user =  log_user.id_user")->result_array();
                $no = 0;
                foreach ($query2 as $row => $h) {

                    // $cekjumlah = $this->db->query("SELECT count(*) as jmlhh from log_user")->row_array();                                        
                    $no++;
                    if ($no %2 == 0) {
                        ?>
                        <li class="timeline-inverted">
                            <div class="timeline-badge bgm-teal"><i class="glyphicon glyphicon-credit-card"></i>
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">

                                    <h4 class="timeline-title"><?php echo $h['user_name'] ?></h4>

                                    <p><small class="text-muted"><i class="glyphicon glyphicon-time m-r-5"></i><?php echo $h['datetime']; ?></small>
                                    </p>
                                </div>
                                <div class="timeline-body">
                                    <div class="pull-left">
                                    <img style="margin-right:10px; margin-top:3px;" src="<?php echo base_url('aset/img/user/'.$h['user_image']); ?>" alt="" width="130" height="130">
                                    </div>
                                    <div class="m-l-20">
                                    <p style="text-align:justify;"><?php echo $h['action'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php
                    }
                    else
                    {
                        ?>                        
                        <li>
                            <div class="timeline-badge bgm-teal"><i class="glyphicon glyphicon-credit-card"></i>
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">

                                    <h4 class="timeline-title"><?php echo $h['user_name'] ?></h4>

                                    <p><small class="text-muted"><i class="glyphicon glyphicon-time m-r-5"></i><?php echo $h['datetime']; ?></small>
                                    </p>
                                </div>
                                <div class="timeline-body">
                                    <div class="pull-left">
                                    <img style="margin-right:10px; margin-top:3px;" src="<?php echo base_url('aset/img/user/'.$h['user_image']); ?>" alt="" width="130" height="130">
                                    </div>
                                    <div class="m-l-20">
                                    <p style="text-align:justify;"><?php echo $h['action'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </li>               
                        <?php
                    }
                    } 
                    ?>                
                    
                <!-- 
                <li>
                    <div class="timeline-badge bgm-teal"><i class="glyphicon glyphicon-credit-card"></i>
                    </div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">

                            <h4 class="timeline-title"><?php echo $h['user_name'] ?></h4>

                            <p><small class="text-muted"><i class="glyphicon glyphicon-time m-r-5"></i><?php echo $h['datetime']; ?></small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <div class="pull-left">
                            <img style="margin-right:10px; margin-top:3px;" src="<?php echo base_url('aset/img/user/'.$h['user_image']); ?>" alt="" width="130" height="130">
                            </div>
                            <div class="m-l-20">
                            <p style="text-align:justify;"><?php echo $h['action'] ?></p>
                            </div>
                        </div>
                    </div>
                </li>                 -->

                
               <!--  <li class="timeline-inverted">
                    <div class="timeline-badge warning"><i class="glyphicon glyphicon-credit-card"></i>
                    </div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">

                            <h4 class="timeline-title">Subject</h4>

                            <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 2 hours ago, 3 Aug 2016.</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p style="text-align:justify;">Memilih Subject bahasa inggris.</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="timeline-badge warning"><i class="glyphicon glyphicon-credit-card"></i>
                    </div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">

                            <h4 class="timeline-title">Subject</h4>

                            <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 3 hours ago, 3 Aug 2016.</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p style="text-align:justify;">Memilih Subject bahasa indonesia.</p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-badge bgm-teal"><i class="glyphicon glyphicon-credit-card"></i>
                    </div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">

                            <h4 class="timeline-title">Enter class Iqro - Hermawan Eko Nugroho</h4>

                            <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 6 hours ago, 3 Aug 2016.</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <div class="pull-left">
                            <img style="margin-right:10px; margin-top:3px;" src="<?php echo base_url('aset/img/user/69.jpg')?>" alt="" width="130" height="130">
                            </div>
                            <div class="m-l-20">
                            <p style="text-align:justify;">Mengajar iqro.</p>
                            </div>
                        </div>
                    </div>
                </li>    
                <li>
                    <div class="timeline-badge bgm-teal"><i class="glyphicon glyphicon-credit-card"></i>
                    </div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">

                            <h4 class="timeline-title">Enter class Komputer - Robith S. Islam</h4>

                            <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 8 hours ago, 3 Aug 2016.</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <div class="pull-left">
                            <img style="margin-right:10px; margin-top:3px;" src="<?php echo base_url('aset/img/user/32.jpg') ?>" alt="" width="130" height="130">
                            </div>
                            <div class="m-l-20">
                            <p style="text-align:justify;">Mengajar Biner.</p>                            
                            </div>
                        </div>
                    </div>
                </li> -->                
            </ul>
        </div>       

        </div>
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script>
$(document).ready(function () {
    $('#demo').awesomeCropper({ 
    width: 150, 
    height: 150, 
    debug: true
    });
});

</script>
