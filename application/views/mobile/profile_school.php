<style type="text/css">
    body{
        background-color: white;
    }
</style>
<header id="header" class="clearfix" style="background-color: #008080;">
    <div class="col-xs-1"  style="margin-top: 7%" >
        <a href="<?php echo base_url('About'); ?>" id="back_home" type="button" class="loading c-white f-19" style="cursor: pointer;">
        <i class="zmdi zmdi-arrow-left"></i>
        </a>
    </div>
    <div class=" c-white f-17 col-xs-10" style="margin-top: 7%">
        <center><?php echo $this->lang->line('settings'); ?></center>
    </div>

</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <section id="content">
        <div class="container" style="position:relative;">
            <div class=" modal fade" data-modal-color="blue" id="complete_modal" tabindex="-1" data-backdrop="static" role="dialog">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content" style="margin-top: 45%;">
                        <div class="modal-header">
                            <h4 class="modal-title"><?php echo $this->lang->line('completeschooltitle');?></h4>
                        </div>
                            <center>
                                <div id='modal_approv' class="modal-body"  ><br>
                                   <div class="alert alert-info" style="display: block; ?>" id="alertcomplete_school">
                                        <?php echo $this->lang->line('alertcompleteschool');?>
                                    </div>
                                </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
            <?php if(isset($mes_alert)){ ?>
            <div class="alert alert-<?php echo $mes_alert; ?>" style="display: <?php echo $mes_display; ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $mes_message; ?>
            </div>
            <?php } ?>
            
            <div class="block-header">
                <!-- <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                            <li><?php echo $this->lang->line('profil'); ?></li>
                            <li class="active"><?php echo $this->lang->line('tab_about'); ?></li>
                        </ol>
                    </li>
                </ul> -->
            </div><!-- akhir block header --> 
            <div class="alert alert-success" style="display: none; ?>" id="alertsuccessupdate">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                Foto akun berhasil diubah
            </div>
            <div class="alert alert-danger" style="display: none; ?>" id="alertsizebesar">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                Upload gagal, Mohon ukuran file tidak melebihi 2 MB.
            </div>
            
            
            <div class="" style="">
                <div class="" id="profile-main">    
                    <div class="">                
                        <div class="">
                            <blockquote class="m-b-5">
                                <p>Data <?php echo $this->lang->line('tab_ps'); ?></p>
                            </blockquote> 
                            <?php
                                $id_user   = $this->session->userdata('id_user'); 
                                $jenjang_id   = $this->session->userdata('jenjang_id'); 
                                $get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$jenjang_id."'")->row_array()['jenjang_name'];
                                // echo $get_jenjangtype;
                                $getdata = $this->db->query("SELECT * FROM tbl_profile_student WHERE student_id='$id_user'")->row_array();
                                $get_school_id = $this->db->query("SELECT school_id FROM `tbl_profile_student` where student_id='$id_user'")->row_array()['school_id'];
                                $cek_school_id = $this->db->query("SELECT school_id FROM `master_school` where school_id='$get_school_id'")->row_array()['school_id'];
                            if ($cek_school_id) {
                                $get_school_name= $this->db->query("SELECT school_name FROM `master_school` where school_id='$cek_school_id'")->row_array()['school_name'];
                                $get_provinsi= $this->db->query("SELECT provinsi FROM `master_school` where school_id='$cek_school_id'")->row_array()['provinsi'];
                                $get_kabupaten= $this->db->query("SELECT kabupaten FROM `master_school` where school_id='$cek_school_id'")->row_array()['kabupaten'];
                                $get_kecamatan= $this->db->query("SELECT kecamatan FROM `master_school` where school_id='$cek_school_id'")->row_array()['kecamatan'];
                                // $get_kelurahan= $this->db->query("SELECT kelurahan FROM `master_school` where school_id='$cek_school_id'")->row_array()['kelurahan'];
                                 $get_school_address= $this->db->query("SELECT school_address FROM `master_school` where school_id='$cek_school_id'")->row_array()['school_address'];
                                 $type = substr($get_jenjangtype, 0, 3);
                            }
                            else{
                                $cek_school_id = $this->db->query("SELECT school_id FROM `master_school_temp` where school_id='$get_school_id'")->row_array()['school_id'];
                                $get_school_name= $this->db->query("SELECT school_name FROM `master_school_temp` where school_id='$cek_school_id'")->row_array()['school_name'];
                                $get_provinsi= $this->db->query("SELECT provinsi_id FROM `master_school_temp` where school_id='$cek_school_id'")->row_array()['provinsi_id'];
                                $get_kabupaten= $this->db->query("SELECT kabupaten_id FROM `master_school_temp` where school_id='$cek_school_id'")->row_array()['kabupaten_id'];
                                $get_kecamatan= $this->db->query("SELECT kecamatan_id FROM `master_school_temp` where school_id='$cek_school_id'")->row_array()['kecamatan_id'];
                                // $get_kelurahan= $this->db->query("SELECT kelurahan FROM `master_school_temp` where school_id='$cek_school_id'")->row_array()['kelurahan'];
                                 $get_school_address= $this->db->query("SELECT school_address FROM `master_school_temp` where school_id='$cek_school_id'")->row_array()['school_address'];
                                 $type = substr($get_jenjangtype, 0, 3);

                            }
                             ?>
                             <?php
                             if ($cek_school_id=='0' || $cek_school_id== null){
                                ?>
                               <div id="editSchool">
                                    <form action="<?php echo base_url('/master/saveEditSchoolStudentMobile') ?>" method="post">                             
                                        <?php 
                                        
                                        // echo $get_school_address;


                                            $provinsi = "";
                                            $d_kabupaten = "";
                                            $d_kecamatan = "";
                                            $d_schoolname = "";
                                            
                                        ?>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label f-12">Provinsi Sekolah</label>
                                            <!-- <label><?php echo $get_jenjangtype; ?></label> -->
                                            <div class="col-sm-9">
                                                <div class="fg-line">                                        
                                                    <select id='school_provinsi' class="select2 form-control" name="school_provinsi" data-placeholder="<?php echo $this->lang->line('selectprovinsii'); ?>">                                            
                                                    <?php
                                                        
                                                        $prv = $this->Master_model->school_provinsi($provinsi);                                                
                                                        echo '<option disabled="disabled" selected="" value="0">Provinsi</option>'; 
                                                        foreach ($prv as $row => $v) {                                            
                                                            echo '<option value="'.$v['provinsi'].'">'.$v['provinsi'].'</option>';
                                                        }
                                                    ?>
                                                    </select>
                                                </div>                                           
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label f-12">Kabupaten Sekolah</label>
                                            <div class="col-sm-9">
                                                <div class="fg-line" id="boxkabupaten">                                        
                                                    <select disabled id='school_kabupaten' class="select2 form-control" name="school_kabupaten" data-placeholder="<?php echo $this->lang->line('selectkabupaten'); ?>">
                                                        <option value=""></option>
                                                        <?php
                                                            
                                                            $kbp = $this->Master_model->school_getAllKabupaten($d_kabupaten,$provinsi);
                                                            if($kbp != ''){
                                                                foreach ($kbp as $key => $value) {
                                                                    echo "<option value='".$value['kabupaten']."'>".$value['kabupaten']."</option>";
                                                                }
                                                                // echo "<script>pfka = '".$kbp['kabupaten']."'</script>";
                                                            }

                                                        ?>
                                                    </select>                                            
                                                </div>
                                            </div>                                    
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label f-12">Kecamatan Sekolah</label>
                                            <div class="col-sm-9">
                                                <div class="fg-line">                                       
                                                    <select  disabled id='school_kecamatan' class="select2 form-control" data-placeholder="<?php echo $this->lang->line('selectkecamatann'); ?>" name="id_kecamatan">
                                                        <option value=''></option>
                                                        <?php
                                                            $kcm = $this->Master_model->school_getAllKecamatan($d_kecamatan,$d_kabupaten);
                                                            if($kcm != ''){
                                                                echo "<option value='".$kcm['kecamatan']."' selected='true'>".$kcm['kecamatan']."</option>";
                                                                echo "<script>pfkec = '".$kcm['kecamatan']."'</script>";

                                                            }

                                                        ?>
                                                    </select>
                                                    <small id="cek_kecamatan" style="color: red;"></small>
                                                </div>
                                            </div>
                                        </div> 
                                        <br>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label f-12">Nama Sekolah</label>
                                            <div class="col-sm-9">
                                                <div class="fg-line">
                                                    <select disabled id='school_name' name="school_name" required class="select2 form-control" data-placeholder="" name="id_desa">
                                                        <option value=''></option>
                                                         <?php
                                                                // $name = $this->Master_model->school_getName($d_schoolname,$d_kelurahan);
                                                                if($name == ''){
                                                                    echo "<option value='".$name['school_name']."' selected='true'>".$name['school_name']."</option>";
                                                                    echo "<script>pfname = '".$name['school_name']."'</script>";
                                                                }

                                                            ?>
                                                    </select>
                                                    <small id="cek_schoolname" style="color: red;"></small>
                                                </div>
                                            </div>
                                        </div>  
                                        <br>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('school_address'); ?></label>
                                            <div class="col-sm-9">
                                                <div class="fg-line">
                                                    <textarea name="school_address" id="school_address" rows="3" required class="input-sm form-control fg-input" placeholder="<?php echo $this->lang->line('typeschooladdress'); ?>" value="<?php echo $get_school_address; ?>"><?php echo $get_school_address; ?></textarea>
                                                </div>
                                            </div>
                                        </div>      
                                        <br>
                                        <br><br>
                                        <div class="card-padding card-body">                                                                           
                                            <button type="submit" name="simpanedit" class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); ?></button>                    
                                        </div>
                                    </form>
                                </div>
                                <?php
                             }
                             else if ($getdata['flag'] == 0){
                                ?>
                                    <div id="updateSchool">
                                    
                                            <?php 
                                            
                                            // echo $get_school_address;


                                                $provinsi = "";
                                                $d_kabupaten = "";
                                                $d_kecamatan = "";
                                                $d_schoolname = "";
                                                
                                            ?>
                                            <h4>Pilih Jenjangmu Sekarang</h4>
                                            <div class="form-group">
                                                <label for="inputEmail3" class="col-sm-3 control-label f-12"><?php echo $this->lang->line('school_jenjang'); ?></label>
                                                <div class="col-sm-9">
                                                    <div class="fg-line" id="kotak_jenjang">
                                                        <select required name="jenjang_id"  class="selectALL form-control" id='jenjang_level' data-placeholder="<?php echo $this->lang->line('select_class'); ?>">
                                                            <option value=""></option>
                                                            <?php
                                                                $jen = $this->Master_model->getMasterJenjang();
                                                                if($jen != ''){
                                                                    echo "<option value='".$jen['jenjang_id']."' selected='true'>".$jen['jenjang_name'].' '.$jen['jenjang_level']."</option>";
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <label style="cursor: pointer; color: #2196F3">
                                                        <input type="checkbox" value="15" id="check_umum">
                                                        Sudah Lulus (Mahasiswa / UMUM)
                                                    </label>
                                                </div>
                                            </div><br><br>
                                            <div id="kotak_sekolah">
                                                <h4>Pilih Alamat Sekolahmu Sekarang</h4>
                                                <div class="pmb-block old_alamat" style="display: none;">
                                                    <div class="pmbb-body">
                                                        <div class="pmbb-view">
                                                            <dl class="dl-horizontal">
                                                                <dt><?php echo $this->lang->line('school_name'); ?></dt>
                                                                <dd>
                                                                    <?php 
                                                                        if ($get_school_name == "") {
                                                                            echo $this->lang->line('nousername');
                                                                        }
                                                                        echo $get_school_name; 
                                                                    ?>                                            
                                                                </dd>
                                                            </dl>
                                                            <dl class="dl-horizontal">
                                                                <dt><?php echo $this->lang->line('school_address'); ?></dt>
                                                                <dd>
                                                                    <?php 
                                                                        if ($get_school_address == "") {
                                                                            echo $this->lang->line('nousername');
                                                                        }
                                                                        echo $get_school_address; 
                                                                    ?>                                            
                                                                </dd>
                                                            </dl>
                                                            
                                                            <dl class="dl-horizontal">
                                                                <dt><?php echo $this->lang->line('selectkecamatann'); ?></dt>
                                                                <dd>
                                                                    <?php 
                                                                        if ($get_school_name == "") {
                                                                            echo $this->lang->line('nousername');
                                                                        }
                                                                        echo $get_kecamatan; 
                                                                    ?>                                            
                                                                </dd>
                                                            </dl> 
                                                            <?php
                                                                if ($get_provinsi != "Luar Negeri") {
                                                                    ?>
                                                                    <dl class="dl-horizontal">
                                                                        <dt><?php echo $this->lang->line('selectprovinsii'); ?></dt>
                                                                        <dd>
                                                                            <?php 
                                                                                if ($get_school_name == "") {
                                                                                    echo $this->lang->line('nousername');
                                                                                }
                                                                                echo $get_provinsi; 
                                                                            ?>                                            
                                                                        </dd>
                                                                    </dl>     
                                                                            <?php
                                                                        }
                                                            ?>                                                       
                                                        </div>                            
                                                    </div>
                                                </div>
                                                <div class="new_alamat" style="display: block;">
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label f-12">Provinsi Sekolah</label>
                                                        <!-- <label><?php echo $get_jenjangtype; ?></label> -->
                                                        <div class="col-sm-9">
                                                            <div class="fg-line">                                        
                                                                <select id='school_provinsi' class="select2 form-control" name="school_provinsi" data-placeholder="<?php echo $this->lang->line('selectprovinsii'); ?>">                                            
                                                                <?php
                                                                    
                                                                    $prv = $this->Master_model->school_provinsi($provinsi);                                                
                                                                    echo '<option disabled="disabled" selected="" value="0">Provinsi</option>'; 
                                                                    foreach ($prv as $row => $v) {                                            
                                                                        echo '<option value="'.$v['provinsi'].'">'.$v['provinsi'].'</option>';
                                                                    }
                                                                ?>
                                                                </select>
                                                            </div>                                           
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label f-12">Kabupaten Sekolah</label>
                                                        <div class="col-sm-9">
                                                            <div class="fg-line" id="boxkabupaten">                                        
                                                                <select disabled id='school_kabupaten' class="select2 form-control" name="school_kabupaten" data-placeholder="<?php echo $this->lang->line('selectkabupaten'); ?>">
                                                                    <option value=""></option>
                                                                    <?php
                                                                        
                                                                        $kbp = $this->Master_model->school_getAllKabupaten($d_kabupaten,$provinsi);
                                                                        if($kbp != ''){
                                                                            foreach ($kbp as $key => $value) {
                                                                                echo "<option value='".$value['kabupaten']."'>".$value['kabupaten']."</option>";
                                                                            }
                                                                            // echo "<script>pfka = '".$kbp['kabupaten']."'</script>";
                                                                        }

                                                                    ?>
                                                                </select>                                            
                                                            </div>
                                                        </div>                                    
                                                    </div>
                                                    <br>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label f-12">Kecamatan Sekolah</label>
                                                        <div class="col-sm-9">
                                                            <div class="fg-line">                                       
                                                                <select  disabled id='school_kecamatan' class="select2 form-control" data-placeholder="<?php echo $this->lang->line('selectkecamatann'); ?>" name="id_kecamatan">
                                                                    <option value=''></option>
                                                                    <?php
                                                                        $kcm = $this->Master_model->school_getAllKecamatan($d_kecamatan,$d_kabupaten);
                                                                        if($kcm != ''){
                                                                            echo "<option value='".$kcm['kecamatan']."' selected='true'>".$kcm['kecamatan']."</option>";
                                                                            echo "<script>pfkec = '".$kcm['kecamatan']."'</script>";

                                                                        }

                                                                    ?>
                                                                </select>
                                                                <small id="cek_kecamatan" style="color: red;"></small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label f-12">Nama Sekolah</label>
                                                        <div class="col-sm-9">
                                                            <div class="fg-line">
                                                                <select disabled id='school_name' name="school_name" required class="select2 form-control" data-placeholder="" name="id_desa">
                                                                    <option value=''></option>
                                                                     <?php
                                                                            // $name = $this->Master_model->school_getName($d_schoolname,$d_kelurahan);
                                                                            if($name == ''){
                                                                                echo "<option value='".$name['school_name']."' selected='true'>".$name['school_name']."</option>";
                                                                                echo "<script>pfname = '".$name['school_name']."'</script>";
                                                                            }

                                                                        ?>
                                                                </select>
                                                                <small id="cek_schoolname" style="color: red;"></small>
                                                            </div>
                                                        </div>
                                                    </div>  
                                                    <br>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('school_address'); ?></label>
                                                        <div class="col-sm-9">
                                                            <div class="fg-line">
                                                                <textarea name="school_address" id="school_address" rows="3" required class="input-sm form-control fg-input" placeholder="<?php echo $this->lang->line('typeschooladdress'); ?>" value=""></textarea>
                                                            </div>
                                                        </div>
                                                    </div>  
                                                    <input hidden type="text" name="school_id" id="school_id" value="<?php echo $get_school_id; ?>">    
                                                </div>
                                                <div class="col-xs-12">
                                                    <label style="cursor: pointer; color: #2196F3" class=""><input type="checkbox" value="15" id="check_alamat"> Alamat Sekolah masih sama</label>        
                                                </div>                                                  
                                            </div>                                               
                                            <br>
                                            <br><br>
                                            <div class="card-padding card-body">                                                                           
                                                <button type="submit" id="btnUpdateJenjang" name="btnUpdateJenjang" class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); ?></button>
                                            </div>
                                    </div>
                                <?php
                             }
                             else if ($cek_school_id!='0' && $getdata['flag'] !='0')
                             {
                                ?>
                                <div id="displaySchool">
                                    <div class="pmb-block">
                                        <div class="pmbb-body">
                                            <div class="pmbb-view">
                                                <dl class="dl-horizontal">
                                                    <dt><?php echo $this->lang->line('school_name'); ?></dt>
                                                    <dd>
                                                        <?php 
                                                            if ($get_school_name == "") {
                                                                echo $this->lang->line('nousername');
                                                            }
                                                            echo $get_school_name; 
                                                        ?>                                            
                                                    </dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt><?php echo $this->lang->line('school_address'); ?></dt>
                                                    <dd>
                                                        <?php 
                                                            if ($get_school_address == "") {
                                                                echo $this->lang->line('nousername');
                                                            }
                                                            echo $get_school_address; 
                                                        ?>                                            
                                                    </dd>
                                                </dl>
                                                
                                                <dl class="dl-horizontal">
                                                    <dt><?php echo $this->lang->line('selectkecamatann'); ?></dt>
                                                    <dd>
                                                        <?php 
                                                            if ($get_school_name == "") {
                                                                echo $this->lang->line('nousername');
                                                            }
                                                            echo $get_kecamatan; 
                                                        ?>                                            
                                                    </dd>
                                                </dl> 
                                                <?php
                                                    if ($get_provinsi != "Luar Negeri") {
                                                        ?>
                                                        <dl class="dl-horizontal">
                                                            <dt><?php echo $this->lang->line('selectprovinsii'); ?></dt>
                                                            <dd>
                                                                <?php 
                                                                    if ($get_school_name == "") {
                                                                        echo $this->lang->line('nousername');
                                                                    }
                                                                    echo $get_provinsi; 
                                                                ?>                                            
                                                            </dd>
                                                        </dl>     
                                                                <?php
                                                            }
                                                ?>                                                       
                                            </div>                            
                                        </div>
                                    </div>
                                </div>
                                <?php
                             }
                            ?>

                                

         
                           
                            <div align="Right" class="m-r-30">
                                
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div><!-- akhir container -->
    </section>            
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
        jQuery(document).ready(function(){
        
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        var code = null;
        var cekk = setInterval(function(){
            code = "<?php echo $this->session->userdata('code');?>";
            cek();
        },500);

        function cek(){
            if (code == "4044") {
                $("#alertsizebesar").css('display','none');
                $("#alertsuccessupdate").css('display','block');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: 'https://classmiles.com/Rest/clearsession/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
            else if (code == "4045") {
                $("#alertsuccessupdate").css('display','none');
                $("#alertsizebesar").css('display','block');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: 'https://classmiles.com/Rest/clearsession/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
        }

        jQuery('#change-pic').on('click', function(e){
            jQuery('#changePic').show();
            jQuery('#change-pic').hide();
            
        });
        
        jQuery('#photoimg').on('change', function()   
        { 
            jQuery("#preview-avatar-profile").html('');
            jQuery("#preview-avatar-profile").html('Uploading....');
            jQuery("#cropimage").ajaxForm(
            {
            target: '#preview-avatar-profile',
            success:    function() { 
                    jQuery('img#photo').imgAreaSelect({
                    aspectRatio: '1:1',
                    onSelectEnd: getSizes,
                });
                jQuery('#image_name').val(jQuery('#photo').attr('file-name'));
                }
            }).submit();

        });

        });
    </script>

<script type="text/javascript">
    $(function () {
        $('#datetimepicker2').datetimepicker({
            locale: 'id'
        });

        // $('#school_provinsi').change(function(){
        //     var prov = $(this).val();
        //     var placeprov = "<?php echo $this->lang->line('selectprovinsii'); ?>";
        //     $("#boxkabupaten").empty();
        //     var kotakboxprov = "<select id='school_kabupaten' class='select2 form-control' name='school_kabupaten' data-placeholder='"+placeprov+"'>"+isi+"</select>";
        //     $("#boxkabupaten").append(kotakboxprov);
        // });
        $('select.select2').each(function(){            
            var jid = document.getElementById('jenjang_level'),
                jid = jid.value;
            if($(this).attr('id') == 'school_provinsi' || $(this).attr('id') == 'school_kabupaten' || $(this).attr('id') == 'school_kecamatan' || $(this).attr('id') == 'school_name'){
                var a = null;
                if ($(this).attr('id') == 'school_provinsi') { a = "<?php echo $this->lang->line('chooseyourpropinsi'); ?>";} if ($(this).attr('id') == 'school_kecamatan') { a = "<?php echo $this->lang->line('chooseyourkecamatan'); ?>";} if ($(this).attr('id') == 'school_kabupaten') { a = "<?php echo $this->lang->line('chooseyourkabupaten'); ?>";}
                $(this).select2({
                    minimumInputLength: 1,
                    placeholder: a,
                    delay: 2000,
                    ajax: {
                        dataType: 'json',
                        type: 'GET',
                        url: '<?php echo base_url(); ?>ajaxer/ajax_indonesia_school/' + $(this).attr('id'),
                        data: function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1,
                                pfp: pfp,
                                pfka: pfka,
                                pfkec: pfkec,
                                pfname: pfname,
                                jid : pid
                            };
                        },
                        processResults: function(data){
                            return {
                                results: data.results,
                                pagination: {
                                    more: data.more
                                }
                            };
                        }
                    }
                });
            }
            else{
              $(this).select2();
            }
        });
        $('#jenjang_level').change(function(){
            
            jid = document.getElementById('jenjang_level'),
            jid = jid.value;
            $('#school_name.select2').select2("val","");
            $('#school_kecamatan.select2').select2("val","");
            $('#school_kabupaten.select2').select2("val","");
            $("#school_name").attr('disabled','true');
            $("#school_kabupaten").attr('disabled','true');
            $("#school_kecamatan").attr('disabled','true');
            // alert(jid);
        });

        $('#school_provinsi').change(function(){
            var prov = $(this).val();
            pfp = prov;            
            var jid = document.getElementById('jenjang_level'),
                jid = jid.value;
            pid = jid;
            $('#school_kabupaten.select2').select2("val","");
            $("#school_kabupaten").removeAttr('disabled');
            $("#school_kecamatan").attr('disabled','');
            $("#school_name").attr('disabled','true');
        });
        $('#school_kabupaten').change(function(){
            var prov = $(this).val();
            pfka = prov;            
            $('#school_kecamatan.select2').select2("val","");
            $("#school_kecamatan").removeAttr('disabled');            
            $("#school_name").attr('disabled','true');
        });
        $('#school_kecamatan').change(function(){
            var prov = $(this).val();
            pfkec = prov;            
            $('#school_name.select2').select2("val","");
            $("#school_name").removeAttr('disabled');
        });
       
        $('#school_name').change(function(){
            var prov = $(this).val();            
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>ajaxer/getSchoolAddress/'+prov,
                success:function(datis){
                    var address = $.trim(datis);
                    $('#school_address').val(address);
                }
            });
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>ajaxer/getSchoolID/'+prov,
                success:function(datis){
                    var schoolid = $.trim(datis);
                    $('#school_id').val(schoolid);
                }
            });
        });
    });
</script>
<script type="text/javascript">
        var jid = "AAAA";
        var pid = "AAAA";
        $(document).ready(function(){
            var code_cek = null;
            var cekk = setInterval(function(){
                code_cek = "<?php echo $this->session->userdata('code_cek');?>";
                cek();
            },500);
            // alert('aasdfsd');
            function cek(){
                if (code_cek == "888") {
                    $("#complete_modal").modal('show');
                    $("#alertcomplete_school").css('display','block');
                    // $('#alert_complete_data').css('display','block');
                    code_cek == null;
                    clearInterval(cekk);
                    $.ajax({
                        url: '<?php echo base_url(); ?>Rest/clearsession',
                        type: 'POST',
                        data: {
                            cek_code: code
                        },
                        success: function(response)
                        { 
                            console.warn(response);
                        }
                    });
                }
                else
                {

                }
                console.warn(code_cek);
            }
        });

        $('#check_umum').bind('change', function (v) {

            if($(this).is(':checked')) {
                $("#check_umum").val('15');
                $("#jenjang_level").val('15');
                $("#kotak_jenjang").css('display','none');
                $("#kotak_sekolah").css('display','none');
                $('#jenjang_level').attr('disabled','');

            } else {
                $('#jenjang_level').removeAttr('disabled','');
                $("#kotak_jenjang").css('display','block');
                $("#kotak_sekolah").css('display','block');
                $("#jenjang_level").empty();
            }
        });
        $('#check_alamat').bind('change', function (v) {

            if($(this).is(':checked')) {
                $(".new_alamat").css('display','none');
                $(".old_alamat").css('display','block');
                // $('.new_alamat').attr('disabled','');

            } else {
                // $(".new_alamat").empty();
                $(".new_alamat").css('display','block');
                $(".old_alamat").css('display','none');
            }
        });
        
        $('#btnUpdateJenjang').click(function(){
            var school_id = null;
            var id_user = <?php echo $this->session->userdata('id_user');?>;
            var jenjang_old = <?php echo $this->session->userdata('jenjang_id');?>;
            var jenjang_new = document.getElementById('jenjang_level'),
                jenjang_new = jenjang_level.value;
            if (jenjang_new=="") {
                jenjang_new =15;
                school_id=null;
            }
            else{
                school_id = $("#school_id").val();
            }

            $.ajax({
                url: '<?php echo BASE_URL();?>V1.0.0/changeDataFromStudent',
                type: 'POST',
                data: {
                    id_user : id_user,
                    jenjang_old : jenjang_old,
                    jenjang_new : jenjang_new,
                    school_id : school_id
                },
                success: function(response)
                {
                   var a = JSON.stringify(response);
                   console.log(a);
                    window.location.href = "<?php echo base_url(); ?> ";
                }
            });
        });
</script>