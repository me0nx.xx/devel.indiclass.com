<header id="header" class="clearfix" data-current-skin="blue">
<ul class="header-inner">
    <li id="menu-trigger" data-trigger="#sidebar">
        <div class="line-wrap">
            <div class="line top"></div>
            <div class="line center"></div>
            <div class="line bottom"></div>
        </div>
    </li>

    <li class="hidden-xs">
        <a href="<?php echo base_url(); ?>" class="m-l-10"><img src="<?php echo base_url(); ?>aset/img/logo/logoclass6.png" alt=""></a>
    </li>

    <li class="pull-right">
        <ul class="top-menu">

            <li class="dropdown">
                <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-more-vert"></i></a>
                <ul class="dropdown-menu dm-icon pull-right">
                    <li class="skin-switch hidden-xs">
                        <span class="ss-skin bgm-lightblue" data-skin="lightblue"></span>
                        <span class="ss-skin bgm-bluegray" data-skin="bluegray"></span>
                        <span class="ss-skin bgm-cyan" data-skin="cyan"></span>
                        <span class="ss-skin bgm-teal" data-skin="teal"></span>
                        <span class="ss-skin bgm-orange" data-skin="orange"></span>
                        <span class="ss-skin bgm-blue" data-skin="blue"></span>
                    </li>
                    <li class="divider hidden-xs"></li>
                    <li class="hidden-xs">
                        <a data-action="fullscreen" href=""><i class="zmdi zmdi-fullscreen"></i><?php echo $this->lang->line('togglefullscreen'); ?></a>
                    </li>
                    <li>
                        <a onclick="signOut()" data-action="" href="<?php echo base_url('/logout'); ?>"><i class="zmdi zmdi-time-restore"></i><?php echo $this->lang->line('logout'); ?></a>
                    </li>
                </ul>
            </li>
           <!--  <li class="hidden-xs" id="chat-trigger" data-trigger="#chat">
                <a href=""><i class="tm-icon zmdi zmdi-comment-alt-text"></i></a>
            </li> -->
        </ul>
    </li>
</ul>
</header>

<section id="main" data-layout="layout-1">
    <section id="content">
        <div class="container">
            <div class="alert alert-danger" style="display: block;">
              	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            	<center><h5 style="color:white;">Akun anda sudah terverifikasi, silahkan login.</h5></center>
            </div>
        </div>
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>


<!-- Top Search Content -->
<!-- <div id="top-search-wrap">
    <div class="tsw-inner">
        <i id="top-search-close" class="zmdi zmdi-arrow-left"></i>
        <input type="text">
    </div>
</div> -->

<script type="text/javascript">
    $(document).ready(function(){

    	setTimeout(function(){
            // window.location.reload();
         	location.href = '<?php echo base_url(); ?>';   
        }, 5000);
    });
</script>