<?php
define(JANUS_C1,"c1.classmiles.com");
define(JANUS_C2,"c2.classmiles.com");
define(JANUS_C3,"c3.classmiles.com");
define(JANUS_C101,"c101.classmiles.com");

if($_GET['c'] == '1'){
	$jlink = JANUS_C1;
}else if($_GET['c'] == '2'){
	$jlink = JANUS_C2;
}else if($_GET['c'] == '3'){
	$jlink = JANUS_C3;
}else if($_GET['c'] == '101'){
	$jlink = JANUS_C101;
}

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://".$jlink."/restart_janus.php");
// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
// curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_TIMEOUT_MS,20000); 

$curl_rets = curl_exec($ch);

if(curl_errno($ch))
{
  echo 'Curl error: ' . curl_error($ch);
}else{
	echo $curl_rets."<br>";
}


?>
