<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>


<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">

        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="container">        
 	        
            <div class="block-header">
                <h2><?php echo $this->lang->line('history');?></h2>
            </div>

 			<?php 
                $iduserskrng    =   $this->session->userdata('id_user');
                $query2 = $this->db->query("SELECT tbl_user.*, log_user.* FROM tbl_user ,log_user WHERE tbl_user.id_user='$iduserskrng' and log_user.id_user='$iduserskrng' ORDER BY log_user.uid DESC LIMIT 20 ")->result_array();

                if (!empty($query2)) {
                	?>
                	<div class="card m-b-10 z-depth-4">
		                <div class="page-header card-header">
		                
		                    <div class="pull-left">                         
		                        <h2><i class="glyphicon glyphicon-time m-r-10"></i></h2>
		                    </div>
		                    <div class="pull-right" style="margin-top:-10px;" id="btnrefresh" data-toggle="refresh" data-placement="right" title="Refresh">
		                    	<button class="btn btn-icon"  onclick="test()"><i class="zmdi zmdi-refresh"></i></button>
		                    </div>
		                <br>
		            </div>

		            <ul class="timeline" style="margin:20px;">
		           	<?php
		                
	                    $no = 0;
	                    foreach ($query2 as $row => $h) {                                        
	                        $no++;
	                        if ($no %2 == 0) {
		            		?>
		            			<li class="timeline-inverted">
                                    <div class="timeline-badge bgm-indigo"><i class="glyphicon glyphicon-credit-card"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <img style="margin-right:10px; margin-top:3px; float:left;" src="<?php echo ('https://classmiles.com/aset/img/user/'.$h['user_image']); ?>" alt="" width="48" height="48">
                                            <h4 class="timeline-title"><?php echo $h['action'] ?> - <?php echo $h['user_name']; ?></h4>

                                            <p><small class="text-muted"><i class="glyphicon glyphicon-time m-r-5"></i><?php echo $h['datetime']; ?></small>
                                            </p>
                                        </div>
                                    </div>
                                </li>
                            <?php
                            }
                            else
                            {
                                ?>                        
                                <li>
                                    <div class="timeline-badge bgm-teal"><i class="glyphicon glyphicon-credit-card"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <img style="margin-right:10px; margin-top:3px; float:left;" src="<?php echo ('https://classmiles.com/aset/img/user/'.$h['user_image']); ?>" alt="" width="48" height="48">
                                            <h4 class="timeline-title"><?php echo $h['action'] ?> - <?php echo $h['user_name']; ?></h4>

                                            <p><small class="text-muted"><i class="glyphicon glyphicon-time m-r-5"></i><?php echo $h['datetime']; ?></small>
                                            </p>
                                        </div>
                                    </div>
                                </li>               
                                <?php
                            }
                        }
                    ?>
                    </ul>
                    <?php                     
                }
                else
                {
                	?>
                	<div class="card m-b-10 z-depth-4">
		                <div class="page-header card-header">
		                
		                    <div class="pull-left">                         
		                        <h2><i class="glyphicon glyphicon-time m-r-10"></i><?php echo $this->lang->line('history');?></h2>
		                    </div>
		                    <div class="pull-right" style="margin-top:-10px;" id="btnrefresh" data-toggle="refresh" data-placement="right" title="Refresh">
		                    	<button class="btn btn-icon"  onclick="test()"><i class="zmdi zmdi-refresh"></i></button>
		                    </div>
		                <br>
		            </div>
                    
                    <div style="z-index:1000px; margin: 30px; bottom: 30px;" class="alert alert-success alert-dismissible text-center" role="alert">            
                    	<label><?php echo $this->lang->line('nohistory'); ?></label>
                    </div><br>
                    <?php
                }
                ?>
        </div>       

        </div>
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script>
$(document).ready(function () {
    setTimeout(function(){
            // window.location.reload();
         	location.href = '<?php echo base_url(); ?>first/history';   
    }, 15000);
    $('#btnrefresh').click(function()
    {
    	window.location.reload();
    });
});

</script>
