<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">

        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="container">

            <div class="block-header">
                <h2><?php  echo $this->lang->line('allsub'); ?></h2>

                <ul class="actions">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="#"><?php  echo $this->lang->line('home'); ?></a></li>
                            <li class="active"><?php  echo $this->lang->line('allsub'); ?></li>
                        </ol>
                    </li>
                </ul>
            </div><!-- akhir block header -->           

            <br>

            <div class="card" style="border-radius: 7px;">
                <div class="row">
                <!-- <h4 style="margin-left: 3%; margin-top: 1.5%; margin-bottom: 1%;">Search Subject</h4>                        
                <hr> --><br>
                <div class="col-sm-3">
                    <div class="form-input" style="margin-left: 10%;">                                                                              
                        <select class="selectpicker">
                            <option><?php echo $this->lang->line('select_class'); ?></option>
                            <option value="">3</option>
                            <option value="">4-5</option>
                            <option value="">6</option>
                            <option value="">7-8</option>
                            <option value="">9</option>
                            <option value="">10-11</option>
                            <option value="">12</option>
                        </select>

                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-input" style="margin-left: 5%;">                                                                              
                        <select class="selectpicker">
                            <option><?php echo $this->lang->line('select_level'); ?></option>
                            <option value="">SD</option>
                            <option value="">SMP</option>
                            <option value="">SMA IPA</option>
                            <option value="">SMA IPS</option>
                            <option value="">SMP</option>
                        </select>

                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-input" style="margin-left: 5%;">                                                                              
                        <select class="selectpicker">
                            <option><?php echo $this->lang->line('status'); ?></option>
                            <option value="">Free</option>
                            <option value="">Premium</option>                                        
                            <option value="">Comin soon</option> 
                        </select>

                    </div>
                </div>

                <div class="col-sm-2">
                    <button class="btn btn-primary btn-block" style="margin-left: 10%;"><?php echo $this->lang->line('button_search'); ?></button> 
                </div>

            </div>
            <br>
        </div>
        <!-- awal card -->
        <div class="card">
            <div class="card-header">
                <div class="row">

                    <div class="col-sm-4">
                        <div class="card-body card-padding bgm-teal c-white" style="border-radius: 7px;">
                            Matematika
                        </div>
                        <br>
                    </div>

                    <div class="col-sm-8">
                        <div class="col-sm-12">
                            <div class="contacts c-profile clearfix row">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="c-item">
                                        <a href="" class="ci-avatar">
                                            <img src="<?php echo base_url(); ?>aset/img/contacts/1.jpg" style="border-radius: 70px;" alt="">
                                        </a>

                                        <div class="c-info rating-list">
                                            <strong>Cathy Shelton</strong>
                                            <div class="rl-star">
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star"></i>
                                            </div>
                                        </div>

                                        <div class="c-footer">
                                            <button disabled="disable" class="waves-effect bgm-orange c-white" ><i class="zmdi zmdi-face-add"></i> <?php echo $this->lang->line('followed'); ?></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="c-item">
                                        <a href="" class="ci-avatar">
                                            <img src="<?php echo base_url(); ?>aset/img/contacts/2.jpg" style="border-radius: 70px;" alt="">
                                        </a>

                                        <div class="c-info rating-list">
                                            <strong>Cathy Shelton</strong>
                                            <div class="rl-star">
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                            </div>
                                        </div>

                                        <div class="c-footer">
                                            <button class="waves-effect bgm-teal c-white" ><i class="zmdi zmdi-face-add"></i> <?php echo $this->lang->line('follow'); ?></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="c-item">
                                        <a href="" class="ci-avatar">
                                            <img src="<?php echo base_url(); ?>aset/img/contacts/3.jpg" style="border-radius: 70px;" alt="">
                                        </a>

                                        <div class="c-info rating-list">
                                            <strong>Cathy Shelton</strong>
                                            <div class="rl-star">
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                            </div>
                                        </div>

                                        <div class="c-footer">
                                            <button class="waves-effect bgm-teal c-white" ><i class="zmdi zmdi-face-add"></i> <?php echo $this->lang->line('follow'); ?></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="c-item">
                                        <a href="" class="ci-avatar">
                                            <img src="<?php echo base_url(); ?>aset/img/contacts/4.jpg" style="border-radius: 70px;" alt="">
                                        </a>

                                        <div class="c-info rating-list">
                                            <strong>Cathy Shelton</strong>
                                            <div class="rl-star">
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                            </div>
                                        </div>

                                        <div class="c-footer">
                                            <button disabled="disable" class="waves-effect bgm-orange c-white" ><i class="zmdi zmdi-face-add"></i> <?php echo $this->lang->line('followed'); ?></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="c-item">
                                        <a href="" class="ci-avatar">
                                            <img src="<?php echo base_url(); ?>aset/img/contacts/5.jpg" style="border-radius: 70px;" alt="">
                                        </a>

                                        <div class="c-info rating-list">
                                            <strong>Cathy Shelton</strong>
                                            <div class="rl-star">
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                            </div>
                                        </div>

                                        <div class="c-footer">
                                            <button disabled="disable" class="waves-effect bgm-orange c-white" ><i class="zmdi zmdi-face-add"></i> <?php echo $this->lang->line('followed'); ?></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="c-item">
                                        <a href="" class="ci-avatar">
                                            <img src="<?php echo base_url(); ?>aset/img/contacts/6.jpg" style="border-radius: 70px;" alt="">
                                        </a>

                                        <div class="c-info rating-list">
                                            <strong>Cathy Shelton</strong>
                                            <div class="rl-star">
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                            </div>
                                        </div>

                                        <div class="c-footer">
                                            <button class="waves-effect bgm-teal c-white" ><i class="zmdi zmdi-face-add"></i> <?php echo $this->lang->line('follow'); ?></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="c-item">
                                        <a href="" class="ci-avatar">
                                            <img src="<?php echo base_url(); ?>aset/img/contacts/7.jpg" style="border-radius: 70px;" alt="">
                                        </a>

                                        <div class="c-info rating-list">
                                            <strong>Cathy Shelton</strong>
                                            <div class="rl-star">
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                            </div>
                                        </div>

                                        <div class="c-footer">
                                            <button class="waves-effect bgm-teal c-white" ><i class="zmdi zmdi-face-add"></i> <?php echo $this->lang->line('follow'); ?></button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div> 
                        <hr>                               
                    </div>

                </div>
            </div>
        </div>
        <!-- akhir matematika -->

        <!-- awal card -->
        <div class="card">
            <div class="card-header">
                <div class="row">

                    <div class="col-sm-4">
                        <div class="card-body card-padding bgm-teal c-white" style="border-radius: 7px;">
                            Bahasa Indonesia
                        </div>
                        <br>
                    </div>

                    <div class="col-sm-8">

                        <div class="col-sm-12">

                            <div class="contacts c-profile clearfix row">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="c-item">
                                        <a href="" class="ci-avatar">
                                            <img src="<?php echo base_url(); ?>aset/img/contacts/1.jpg" style="border-radius: 70px;" alt="">
                                        </a>

                                        <div class="c-info rating-list">
                                            <strong>Cathy Shelton</strong>
                                            <div class="rl-star">
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                            </div>
                                        </div>

                                        <div class="c-footer">
                                            <button class="waves-effect bgm-teal c-white" ><i class="zmdi zmdi-face-add"></i> <?php echo $this->lang->line('follow'); ?></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="c-item">
                                        <a href="" class="ci-avatar">
                                            <img src="<?php echo base_url(); ?>aset/img/contacts/2.jpg" style="border-radius: 70px;" alt="">
                                        </a>

                                        <div class="c-info rating-list">
                                            <strong>Cathy Shelton</strong>
                                            <div class="rl-star">
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star"></i>
                                            </div>
                                        </div>

                                        <div class="c-footer">
                                            <button disabled="disable" class="waves-effect bgm-orange c-white" ><i class="zmdi zmdi-face-add"></i> <?php echo $this->lang->line('followed'); ?></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="c-item">
                                        <a href="" class="ci-avatar">
                                            <img src="<?php echo base_url(); ?>aset/img/contacts/3.jpg" style="border-radius: 70px;" alt="">
                                        </a>

                                        <div class="c-info rating-list">
                                            <strong>Cathy Shelton</strong>
                                            <div class="rl-star">
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star"></i>
                                            </div>
                                        </div>

                                        <div class="c-footer">
                                            <button class="waves-effect bgm-teal c-white" ><i class="zmdi zmdi-face-add"></i> <?php echo $this->lang->line('follow'); ?></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="c-item">
                                        <a href="" class="ci-avatar">
                                            <img src="<?php echo base_url(); ?>aset/img/contacts/4.jpg" style="border-radius: 70px;" alt="">
                                        </a>

                                        <div class="c-info rating-list">
                                            <strong>Cathy Shelton</strong>
                                            <div class="rl-star">
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                            </div>
                                        </div>

                                        <div class="c-footer">
                                            <button class="waves-effect bgm-teal c-white" ><i class="zmdi zmdi-face-add"></i> <?php echo $this->lang->line('follow'); ?></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="c-item">
                                        <a href="" class="ci-avatar">
                                            <img src="<?php echo base_url(); ?>aset/img/contacts/5.jpg" style="border-radius: 70px;" alt="">
                                        </a>

                                        <div class="c-info rating-list">
                                            <strong>Cathy Shelton</strong>
                                            <div class="rl-star">
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                            </div>
                                        </div>

                                        <div class="c-footer">
                                            <button disabled="disable" class="waves-effect bgm-orange c-white" ><i class="zmdi zmdi-face-add"></i> <?php echo $this->lang->line('followed'); ?></button>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <hr>
                    </div>                            

                </div>

            </div>
        </div>
        <!-- akhir b.indonesia -->
        <!-- akhir card -->

        <!-- awal card -->
        <div class="card">
            <div class="card-header">
                <div class="row">

                    <div class="col-sm-4">
                        <div class="card-body card-padding bgm-teal c-white" style="border-radius: 7px;">
                            Bahasa Inggris
                        </div>
                        <br>
                    </div>

                    <div class="col-sm-8">
                        <div class="col-sm-12">
                            <div class="contacts c-profile clearfix row">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="c-item">
                                        <a href="" class="ci-avatar">
                                            <img src="<?php echo base_url(); ?>aset/img/contacts/1.jpg" style="border-radius: 70px;" alt="">
                                        </a>

                                        <div class="c-info rating-list">
                                            <strong>Cathy Shelton</strong>
                                            <div class="rl-star">
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                            </div>
                                        </div>

                                        <div class="c-footer">
                                            <button disabled="disable" class="waves-effect bgm-orange c-white" ><i class="zmdi zmdi-face-add"></i> <?php echo $this->lang->line('followed'); ?></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="c-item">
                                        <a href="" class="ci-avatar">
                                            <img src="<?php echo base_url(); ?>aset/img/contacts/2.jpg" style="border-radius: 70px;" alt="">
                                        </a>

                                        <div class="c-info rating-list">
                                            <strong>Cathy Shelton</strong>
                                            <div class="rl-star">
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star active"></i>
                                            </div>
                                        </div>

                                        <div class="c-footer">
                                            <button disabled="disable" class="waves-effect bgm-orange c-white" ><i class="zmdi zmdi-face-add"></i> <?php echo $this->lang->line('followed'); ?></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <div class="c-item">
                                        <a href="" class="ci-avatar">
                                            <img src="<?php echo base_url(); ?>aset/img/contacts/3.jpg" style="border-radius: 70px;" alt="">
                                        </a>

                                        <div class="c-info rating-list">
                                            <strong>Cathy Shelton</strong>
                                            <div class="rl-star">
                                                <i class="zmdi zmdi-star active"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                                <i class="zmdi zmdi-star"></i>
                                            </div>
                                        </div>

                                        <div class="c-footer">
                                            <button class="waves-effect bgm-teal c-white" ><i class="zmdi zmdi-face-add"></i> <?php echo $this->lang->line('follow'); ?></button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <hr>
                    </div>

                </div>
            </div>
        </div>
            <!-- akhir b.inggris
            akhir card -->

        </div><!-- akhir container -->
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>