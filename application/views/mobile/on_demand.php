<style type="text/css">
body{
    background-color: white;
    margin: 0; height: 100%; overflow: hidden;
}
    .button-right {
    float:right;
   /* width:45px;*/
    position: fixed;
    /*top: 100px;*/
    right: 20px; 
    bottom: 30px;
}
</style>
<script type="text/javascript">
    $(document).ready(function(){
    	// $('#button_search_private').attr('disabled','');
        $("#button_search_private").click(function(){
            $("#menucari_privateclass").hide();
            $("#hasilcari_privateclass").show();
        });
        $("#private_click").click(function(){
            $("#box_privateclass").show();
            $("#menucari_privateclass").show();
            $("#hasilcari_privateclass").hide();
        });
        $("#button_search_group").click(function(){
            $("#menucari_groupclass").hide();
            $("#hasilcari_groupclass").show();
        });
        $("#group_click").click(function(){
            $("#box_privateclass").hide();
            $("#menucari_privateclass").hide();
            $("#hasilcari_privateclass").hide();
            $("#box_privateclass").show();
            $("#menucari_groupclass").show();
            $("#hasilcari_groupclass").hide();
        });
    });

function show(elementId) { 
 document.getElementById("box_privateclass").style.display="none";
 document.getElementById("box_groupclass").style.display="none";
 document.getElementById("menu_ondemand").style.display="none";
 document.getElementById(elementId).style.display="block";
}
</script>
<header id="header" class="clearfix" style="background-color: #008080;" >
    <?php $this->load->view('mobile/inc/navbar'); ?>
    <div class="header-inner" style="color: white; padding-top: 0; margin-top: 0; ">
        <center>
        <!-- <div class="col-xs-12" style="background-color: #2196f3;"><a href="#" class=" m-l-10"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>logo/logoclass6.png" alt=""></a></div>     -->
        <div class="tabmenu col-xs-3" style=""><a style="color: white;" href="<?php echo base_url();?>">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/myclassputih-02.png" style="width: 16px;">
            <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('home'); ?></label></a>
        </div>
        <div class="tabmenu col-xs-3" style=""><a style="color: white;" href="<?php echo base_url(); ?>Subjects">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Tutor White.svg" style="width: 16px;  height: auto;">
            <br><label style="font-size: 9px; text-transform: uppercase;">TUTOR</label></a>
        </div>
        <div class=" col-xs-3" style=""><a style="color: white;" href="<?php echo base_url(); ?>Ondemand">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Extra Class Orange.svg" style="width: 16px;  height: auto;">
            <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('private');?></label></a>
        </div>
        <div class=" tabmenu col-xs-3" style=""><a style="color: white;" href="<?php echo base_url(); ?>About">
            <i class="zmdi zmdi-menu zmdi-hc-fw"></i>
            <!-- <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Account White.svg" style="width: 16px; margin-top: 3%;  height: auto;"> -->
            <br><label style="font-size: 9px; text-transform: uppercase;">Others<?php echo $this->lang->line('tab_other');?></label></a>
        </div>
        </center>                          
    </div>

</header>

<div class="card-body card-padding" style="margin-top: 35%;">
    <div class="modal fade fade" style="" id="modal_loading" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document" style="background:rgba(0,00,00,0.0);">   
            <div style="margin-top: 50%;">
                <center>
                   <div class="preloader pl-xxl pls-teal">
                        <svg class="pl-circular" viewBox="25 25 50 50">
                            <circle class="plc-path" cx="50" cy="50" r="20" />
                        </svg>
                    </div><br>
                   <p style="color: white;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
                </center>
            </div>
        </div>
    </div>
    <div role="tabpanel" class="">
        <ul class="tab-nav text-center" role="tablist" style="color: white">
            <li class="col-xs-4 active"><a href="#tab_private" aria-controls="tab_private" role="tab" data-toggle="tab"><?php echo $this->lang->line('private');?></a></li>
            <li class="col-xs-4"><a href="#tab_group" aria-controls="tab_group" role="tab" data-toggle="tab"><?php echo $this->lang->line('group');?></a></li>
            <li class="col-xs-4"><a href="#tab_invitation" aria-controls="tab_invitation" role="tab" data-toggle="tab"><?php echo $this->lang->line('invitation'); ?></a></li>
        </ul>  
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="tab_private">
                <!-- <button id="btn_add_private" style="display: inline; z-index: 7" class=" button-right btn bgm-amber btn-icon"><i class="zmdi zmdi-plus" ></i></button> -->
                <div id="list_private"> 
                </div>
                <div id="menu_private" style="display: none;" style="z-index: 9;">
                    <!-- <div class="col-md-12"> -->
                    <div id="menucari_privateclass" class="col-md-4" style="z-index: 9;">
                        <div class="card">
                            <div class="card-header ch-alt">
                                <i id="btn_close_private" class="zmdi zmdi-close" style="float:right; right: 30px;"></i>
                                <h2>Cari Private Class</h2>
                            </div>
                            <div class="card-body card-padding">
                                <form>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-storage"></i></span>
                                                <select class="select2 form-control" required id="subject_id">
                                                    <?php
                                                        $id = $this->session->userdata("id_user");
                                                        $allsub = $this->db->query("SELECT ms.*, mj.* FROM master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id")->result_array();
                                                        echo '<option disabled="disabled" selected="" value="0">Pilih Pelajaran</option>'; 
                                                        foreach ($allsub as $row => $v) {                                            
                                                            echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.$v['jenjang_name'].' '.$v['jenjang_level'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                                <input type="text" name="subjecta" id="subjecton" hidden="true">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="input-group form-group p-r-15">
                                                <!-- <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span> -->
                                                <!-- <div class="dtp-container fg-line m-l-5">                                                
                                                    <input id="dateon" type='text' required class="form-control" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
                                                </div> -->
                                                <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                                <div class="dtp-container fg-line m-l-5">
                                                    
                                                    <input type="text" required class=" form-control" id="dateon" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>"/>
                                                </div>
                                            </div>
                                        </div>  

                                        <div class="col-md-12">
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                                <div class="dtp-container fg-line m-l-5" id="tempatwaktuu">
                                                    <input type='text' class='form-control' id='timeeeeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />
                                                </div>                                            
                                            </div>
                                        </div>
                                      
                                        <div class="col-md-12">
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-time-restore-setting"></i></span>
                                                <div class="dtp-container fg-line">                                                
                                                    <select required class="select2 form-control" required id="duration" >
                                                        <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
                                                        <option value="900">15 Minutes</option>
                                                        <option value="1800">30 Minutes</option>
                                                        <option value="2700">45 Minutes</option>
                                                        <option value="3600">1 Hours</option>
                                                        <option value="5400">1 Hours 30 Minutes</option>
                                                        <option value="7200">2 Hours</option>
                                                        <option value="9000">2 Hours 30 Minutes</option>
                                                        <option value="10800">3 Hours</option>
                                                        <option value="12600">3 Hours 30 Minutes</option>
                                                        <option value="14400">4 Hours</option>
                                                        <option value="16200">4 Hours 30 Minutes</option>
                                                        <option value="18000">5 Hours</option>
                                                        <option value="19800">5 Hours 30 Minutes</option>
                                                        <option value="21600">6 Hours</option>
                                                        <option value="23400">6 Hours 30 Minutes</option>
                                                        <option value="25200">7 Hours</option>
                                                        <option value="27000">7 Hours 30 Minutes</option>
                                                        <option value="28800">8 Hours</option>
                                                        <option value="30600">8 Hours 30 Minutes</option>
                                                        <option value="32400">9 Hours</option>
                                                        <option value="34200">9 Hours 30 Minutes</option>
                                                        <option value="36000">10 Hours</option>
                                                        <option value="37800">10 Hours 30 Minutes</option>
                                                        <option value="39600">11 Hours</option>
                                                        <option value="41400">11 Hours 30 Minutes</option>
                                                        <option value="43200">12 Hours</option>
                                                        <option value="45000">12 Hours 30 Minutes</option>
                                                        <option value="46800">13 Hours</option>
                                                        <option value="48600">13 Hours 30 Minutes</option>
                                                        <option value="50400">14 Hours</option>
                                                        <option value="52200">14 Hours 30 Minutes</option>
                                                        <option value="54000">15 Hours</option>
                                                        <option value="55800">15 Hours 30 Minutes</option>
                                                        <option value="57600">16 Hours</option>
                                                        <option value="59400">16 Hours 30 Minutes</option>
                                                        <option value="61200">17 Hours</option>
                                                        <option value="63000">17 Hours 30 Minutes</option>
                                                        <option value="64800">18 Hours</option>
                                                        <option value="66600">18 Hours 30 Minutes</option>
                                                        <option value="68400">19 Hours</option>
                                                        <option value="70200">19 Hours 30 Minutes</option>
                                                        <option value="72000">20 Hours</option>
                                                        <option value="73800">20 Hours 30 Minutes</option>
                                                        <option value="75600">21 Hours</option>
                                                        <option value="77400">21 Hours 30 Minutes</option>
                                                        <option value="79200">22 Hours</option>
                                                        <option value="81000">22 Hours 30 Minutes</option>
                                                        <option value="82800">23 Hours</option>
                                                        <option value="84600">23 Hours 30 Minutes</option>
                                                        <option value="86400">24 Hours</option>
                                                    </select>
                                                    <input type="text" name="durationa" id="durationon" hidden="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button  ng-click="getondemandprivate()" id="button_search_private" class="btn btn-primary" style="margin-left: 10%; width: 80%;"><?php echo $this->lang->line('button_search'); ?></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div id="hasilcari_privateclass" class="">
                        <div class="card">
                            <div class="row">
                                <div  class="card-header ch-alt" ng-show="resulte">
                                    <i id="btn_close_cari_private" class="zmdi zmdi-close" style="float:right; right: 30px;"></i>
                                    <h2>Hasil Pencarian : {{hasil}} </h2>
                                    <label>{{subjects}}</label>
                                </div>
                                <div class="card-body card-padding">                                                                
                                    <div class="alert alert-danger alert-dismissible text-center" role="alert" ng-if="firste">              
                                        <label>Tidak ada tutor yang tersedia</label>
                                    </div>
                                    <div class="col-xs-12" ng-repeat=" x in datas" ng-show="resulte">
                                        <div class="media-demo col-xs-12 m-b-20">
                                            <div class="media">
                                                <div style="" class="col-xs-4">                       
                                                    <img class="media-object m-t-5" ng-src="{{x.user_image}}" style="height: 80px; width: 80px;" alt="">
                                                </div>
                                                <div class="media-body">
                                                    <div class="col-xs-12"  style="margin-left: -4%;">
                                                       <label class="f-13">{{x.user_name}}</label>
                                                        <label ng-if="x.exact == '1'">{{times}}</label>
                                                        <p class="f-13">Rp. {{x.harga}}</p> 
                                                    </div>                                               
                                                </div>
                                                <div class="col-xs-12 m-t-5">                         
                                                    <button gh="{{x.gh}}" harga="{{x.harga}}" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" dates="{{dates}}" start_time="{{times}}" subject_id="{{x.subject_id}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>                                       
                                                    <button ng-if="x.exact == '0'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{x.start_time}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
                <div id="nodata_private" style="text-align: center; ">
                    <center>
                        <img id="img_nodata_private" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>icons/NoPrivate.png" style="margin-top: 20%; width: 200px; height: 200px;"><br>
                        <label id="lbl_nodata_private" style="font-size: 14px;"><?php echo $this->lang->line('nodata_ondemand'); ?><br><?php echo $this->lang->line('nodata_private'); ?></label>
                    </center>   
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="tab_group">
                <!-- <button id="btn_add_group" style="display: inline; z-index: 7"  class=" button-right btn bgm-amber btn-icon"><i class="zmdi zmdi-plus" ></i></button> -->
                <div id="list_group">
                </div> 
                <div id="menu_group" style="display: none;" style="z-index: 9;">
                    <!-- <div class="col-md-12"> -->
                    <div id="menucari_groupclass" class="col-md-4" style="z-index: 9;">
                        <div class="card">
                            <div class="card-header ch-alt">
                                <i id="btn_close_group" class="zmdi zmdi-close" style="float:right; right: 30px;"></i>
                                <h2>Cari Group Class</h2>
                            </div>

                            <div class="card-body card-padding">
                                <form>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-storage"></i></span>
                                                <select class="select2 form-control" required id="subject_idg">
                                                    <?php
                                                        $id = $this->session->userdata("id_user");
                                                        $allsub = $this->db->query("SELECT ms.*, mj.* FROM master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id")->result_array();
                                                        echo '<option disabled="disabled" selected="" value="0">Pilih Pelajaran</option>'; 
                                                        foreach ($allsub as $row => $v) {                                            
                                                            echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.$v['jenjang_name'].' '.$v['jenjang_level'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                                <input type="text" name="subjecta" id="subjectong" hidden="true">
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-12">
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                                <div class="dtp-container fg-line m-l-5">                                                
                                                    <input id="dateong" type='text' required class="form-control date-picker" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="col-md-12">
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                                <div class="dtp-container fg-line m-l-5">                                                
                                                    <input type="text" required class="form-control" id="dateong" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>"/>
                                                </div>
                                            </div>
                                        </div> 
                                        <!-- <div class="col-md-12">
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                                <div class="dtp-container fg-line">                                                
                                                    <select class='select2 form-control' required id="timeg">
                                                        <option disabled selected><?php echo $this->lang->line('searchtime'); ?></option>
                                                        <?php                 
                                                            for ($i=0; $i < 24; $i++) { 
                                                              for($y = 0; $y<=45; $y+=15){  
                                                                echo "<option value=".sprintf('%02d',$i).":".sprintf('%02d',$y).">".sprintf('%02d',$i).":".sprintf('%02d',$y)."</option>";                                            
                                                            }
                                                        }
                                                        ?>
                                                    </select>  
                                                    <input type="text" name="timea" id="timeong" hidden="true">                              
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="col-md-12">                                    
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                                <!-- <div class="dtp-container fg-line m-l-5">
                                                    <input type="text" class="form-control" id="timeeee" placeholder="<?php echo $this->lang->line('searchtime'); ?>" />
                                                </div> -->
                                                <div class="dtp-container fg-line m-l-5" id="tempatwaktuuu">
                                                    <input type='text' class='form-control' id='timeeeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="durasigroup">
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-time-restore-setting"></i></span>
                                                <div class="dtp-container fg-line">                                                
                                                    <select required class="select2 form-control" id="durationg" >
                                                        <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
                                                        <option value="900">15 Minutes</option>
                                                        <option value="1800">30 Minutes</option>
                                                        <option value="2700">45 Minutes</option>
                                                        <option value="3600">1 Hours</option>
                                                        <option value="5400">1 Hours 30 Minutes</option>
                                                        <option value="7200">2 Hours</option>
                                                        <option value="9000">2 Hours 30 Minutes</option>
                                                        <option value="10800">3 Hours</option>
                                                        <option value="12600">3 Hours 30 Minutes</option>
                                                        <option value="14400">4 Hours</option>
                                                        <option value="16200">4 Hours 30 Minutes</option>
                                                        <option value="18000">5 Hours</option>
                                                        <option value="19800">5 Hours 30 Minutes</option>
                                                        <option value="21600">6 Hours</option>
                                                        <option value="23400">6 Hours 30 Minutes</option>
                                                        <option value="25200">7 Hours</option>
                                                        <option value="27000">7 Hours 30 Minutes</option>
                                                        <option value="28800">8 Hours</option>
                                                        <option value="30600">8 Hours 30 Minutes</option>
                                                        <option value="32400">9 Hours</option>
                                                        <option value="34200">9 Hours 30 Minutes</option>
                                                        <option value="36000">10 Hours</option>
                                                        <option value="37800">10 Hours 30 Minutes</option>
                                                        <option value="39600">11 Hours</option>
                                                        <option value="41400">11 Hours 30 Minutes</option>
                                                        <option value="43200">12 Hours</option>
                                                        <option value="45000">12 Hours 30 Minutes</option>
                                                        <option value="46800">13 Hours</option>
                                                        <option value="48600">13 Hours 30 Minutes</option>
                                                        <option value="50400">14 Hours</option>
                                                        <option value="52200">14 Hours 30 Minutes</option>
                                                        <option value="54000">15 Hours</option>
                                                        <option value="55800">15 Hours 30 Minutes</option>
                                                        <option value="57600">16 Hours</option>
                                                        <option value="59400">16 Hours 30 Minutes</option>
                                                        <option value="61200">17 Hours</option>
                                                        <option value="63000">17 Hours 30 Minutes</option>
                                                        <option value="64800">18 Hours</option>
                                                        <option value="66600">18 Hours 30 Minutes</option>
                                                        <option value="68400">19 Hours</option>
                                                        <option value="70200">19 Hours 30 Minutes</option>
                                                        <option value="72000">20 Hours</option>
                                                        <option value="73800">20 Hours 30 Minutes</option>
                                                        <option value="75600">21 Hours</option>
                                                        <option value="77400">21 Hours 30 Minutes</option>
                                                        <option value="79200">22 Hours</option>
                                                        <option value="81000">22 Hours 30 Minutes</option>
                                                        <option value="82800">23 Hours</option>
                                                        <option value="84600">23 Hours 30 Minutes</option>
                                                        <option value="86400">24 Hours</option>
                                                    </select>
                                                    <input type="text" name="durationa" id="durationong" hidden="true" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button ng-click="getondemandgroup()" id="button_search_group" class="btn btn-primary" style="margin-left: 10%; width: 80%;"><?php echo $this->lang->line('button_search'); ?></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="hasilcari_groupclass" class="col-md-8">
                        <div class="card">

                            <div class="card-header ch-alt" ng-show="resulte">
                                <i id="btn_close_cari_group" class="zmdi zmdi-close" style="float:right; right: 30px;"></i>
                                <h2>Hasil Pencarian : {{ghasil}} </h2>
                                <label>{{subjects}}</label>
                            </div>

                            <div class="card-body card-padding">
                                <div class="row">
                                    <div class="alert alert-danger alert-dismissible text-center" role="alert" ng-if="firste">                                
                                        <label>Tidak ada tutor yang tersedia</label>
                                    </div>
                                    <div class="col-xs-12" ng-repeat=" x in datasg" ng-show="resulte">
                                        <div class="media-demo col-xs-12 m-b-20">
                                            <div class="media">
                                                <div class="col-xs-4">
                                                    <img class="media-object  m-t-5" ng-src="{{x.user_image}}" style="height: 80px; width: 80px;" alt="">
                                                </div>
                                                <div class="media-body">
                                                    <div class="col-xs-12" style="">                                                        
                                                        <p class="f-13">{{x.user_name}}</p>
                                                        <label ng-if="x.exact == '0'">{{x.start_time}}</label>
                                                        <label >{{times}}</label>                                                        
                                                        </p>
                                                        <p class="f-13" style="margin-top: -5%;">Rp. {{x.harga}}</p>                                                        
                                                    </div>
                                                    <div class="col-xs-12" style="">
                                                        <p>Metode Pembayaran</p>
                                                        <div class="radio m-b-15">
                                                            <label>
                                                                <input type="radio" id="radiobutton1" idtutor="{{x.tutor_id}}" class='{{x.tutor_id}} a' name="bebanmetod" value="bayar_sendiri">
                                                                <i class="input-helper"></i>
                                                                Bayar Sendiri
                                                            </label>
                                                        </div>
                                                        
                                                        <div class="radio m-b-15">
                                                            <label>
                                                                <input type="radio" id="radiobutton2" idtutor="{{x.tutor_id}}" class='{{x.tutor_id}} b' name="bebanmetod" value="bagi_rata">
                                                                <i class="input-helper"></i>
                                                                Bagi Rata
                                                            </label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-xs-12">                         
                                                    <a class="openmodaladd {{x.tutor_id}} btn bgm-green btn-icon-text btn-block" gh="{{x.gh}}" harga="{{x.harga}}" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" dates="{{dates}}" start_time="{{times}}" subject_id="{{x.subject_id}}" tutor_id="{{x.tutor_id}}">
                                                        <i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?>                                                                                        
                                                    </a>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div> 
                <div id="nodata_group" style="text-align: center;  ">
                    <center>
                        <img id="img_nodata_group" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>icons/NoGroup.png" style="margin-top: 20%; width: 200px; height: 200px;"><br>
                        <label id="lbl_nodata_group" style="font-size: 14px;"><?php echo $this->lang->line('nodata_ondemand'); ?><br><?php echo $this->lang->line('nodata_group'); ?></label>
                    </center>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="tab_invitation">  
                <div id="list_invitation">
                </div> 
                <div id="menu_invitation" style="display: none;">
                    
                </div>
                <div id="nodata_invitation" style="text-align: center; ">
                    <center>
                        <img id="img_nodata_invitation" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>icons/NoInvitation.png" style="margin-top: 20%; width: 200px; height: 200px;"><br>
                        <label id="lbl_nodata_invitation" style="font-size: 14px;"><?php echo $this->lang->line('nodata_invitation'); ?></label>
                    </center>
                </div> 
            </div>
        </div>
    </div>
</div>
    <script type="text/javascript">
        $("#btn_add_private").click(function() {
            $('#menu_private').css('display','block');
            $('#btn_add_private').css('display','none');
            $('#list_private').css('display','none');
            $('#nodata_private').css('display','none');
        });
        $("#btn_close_private").click(function() {
            $('#menu_private').css('display','none');
            $('#btn_add_private').css('display','block');
            $('#list_private').css('display','block');
            $('#nodata_private').css('display','block');
        });

        $("#btn_close_cari_private").click(function() {
            

        });

        $("#btn_add_group").click(function() {
            $('#menu_group').css('display','block');
            $('#btn_add_group').css('display','none');
            $('#list_group').css('display','none');
            $('#nodata_group').css('display','none');
        });
        $("#btn_close_group").click(function() {
            $('#menu_group').css('display','none');
            $('#btn_add_group').css('display','block');
            $('#list_group').css('display','block');
            $('#nodata_group').css('display','block');
        });
    </script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>aset/js/magicsuggest/magicsuggest.css" /> 
    <script type="text/javascript" src="<?php echo base_url();?>aset/js/magicsuggest/magicsuggest.js"></script>    
    <script type="text/javascript">
        var id_user = "<?php echo $this->session->userdata('id_user');?>";
        var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc;
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        $.ajax({
            url: '<?php echo base_url(); ?>Rest/get_dataprivate/access_token/'+tokenjwt,
            type: 'POST',
            data: {
                id_user: id_user,
                user_utc: user_utc,
                user_device: 'web'
            },
            success: function(response)
            {   
                console.warn(response);
                // var a = JSON.parse(response);
                var a = JSON.stringify(response['response']);
                // var jsonPretty = JSON.stringify(JSON.parse(a),null,2);                      
                // $("#aaa").text(jsonPretty); 
                console.warn('total '+response.response.length);
                if (response.response == 0) 
                {                            
                   $('#img_nodata_private').css('display','block');
                   $('#lbl_nodata_private').css('display','block');
                }else{
                    $('#img_nodata_private').css('display','none');
                    $('#lbl_nodata_private').css('display','none');
                    for (var i = 0; i< response.response.length;i++) {

                        var request_id = response.response[i]['request_id'];
                        var avtime_id = response.response[i]['avtime_id'];
                        var id_user_requester = response.response[i]['id_user_requester'];
                        var tutor_id = response.response[i]['tutor_id'];
                        var subject_id = response.response[i]['subject_id'];
                        var topic = response.response[i]['topic'];
                        var date_requested = response.response[i]['date_requested'];
                        var duration_requested = response.response[i]['duration_requested'];
                        var approve = response.response[i]['approve'];
                        var template = response.response[i]['template'];
                        var user_utc = response.response[i]['user_utc'];
                        var datetime = response.response[i]['datetime'];
                        var date_requested_utc = response.response[i]['date_requested_utc'];
                        day = moment(date_requested_utc).format('dddd');
                        date = moment(date_requested_utc).format('DD MMMM YYYY');
                        time = moment(date_requested_utc).format('HH:mm');
                        var country = response.response[i]['country'];
                        var user_name = response.response[i]['user_name'];
                        var user_image = response.response[i]['user_image'];
                        var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                        var subject_name = response.response[i]['subject_name'];
                        if (approve == 0) {
                            var status_tutor ="Waiting for Tutor approval";
                        }else if (approve == 1) {
                            var status_tutor ="Confirmed by Tutor";
                        }else{ 
                            var status_tutor ="Tutor is not Available"
                        }

                        var data_private ="<div class='col-xs-12' style='background-color: #d6d6d6'><div style='width:103%; margin-left:-1.5%; padding:1px; background-color:white;' class='m-t-5 m-b-5 col-xs-12'><div  class='col-xs12 m-l-25'>"+day+", "+date+" - "+time+"</div><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:#ff9800; border-bottom-style: solid; margin-bottom:10px;'></div><div class='col-xs-3 col-sm-1 ' style=' width:80px;'><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+" width='60px' height='60px' style=' '><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5 ' style='width:44% height:70px;' ><h6 class=' c-red' style='font-size:11px; '>"+user_name+"</h6><h6 class='c-blue' style=' font-size:10px; height:12px; ; cursor:pointer;  margin-right:-28px;'>"+subject_name+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'>"+status_tutor+"</h5></div></div></div>";
                        // console.warn('jhvjh');
                        $("#list_private").append(data_private);
                    }
                }

                    
               
            }
        });

        $.ajax({
            url: '<?php echo base_url(); ?>Rest/get_datagroup/access_token/'+tokenjwt,
            type: 'POST',
            data: {
                id_user: id_user,
                user_utc: user_utc,
                user_device: 'web'
            },
            success: function(response)
            {   
                console.warn(response);
                // var a = JSON.parse(response);
                var a = JSON.stringify(response['response']);
                // var jsonPretty = JSON.stringify(JSON.parse(a),null,2);                      
                // $("#aaa").text(jsonPretty); 
                // console.warn('total '+response.response.length);
                if (response.response == 0) 
                {                            
                    $('#img_nodata_group').css('display','block');
                   $('#lbl_nodata_group').css('display','block');
                }
                else{
                     $('#img_nodata_group').css('display','none');
                   $('#lbl_nodata_group').css('display','none');
                    for (var i = 0; i< response.response.length;i++) {

                        var request_id = response.response[i]['request_id'];
                        var avtime_id = response.response[i]['avtime_id'];
                        var id_user_requester = response.response[i]['id_user_requester'];
                        var tutor_id = response.response[i]['tutor_id'];
                        var subject_id = response.response[i]['subject_id'];
                        var topic = response.response[i]['topic'];
                        var date_requested = response.response[i]['date_requested'];
                        var duration_requested = response.response[i]['duration_requested'];
                        var approve = response.response[i]['approve'];
                        var template = response.response[i]['template'];
                        var user_utc = response.response[i]['user_utc'];
                        var datetime = response.response[i]['datetime'];
                        var date_requested_utc = response.response[i]['date_requested_utc'];
                        day = moment(date_requested_utc).format('dddd');
                        date = moment(date_requested_utc).format('DD MMMM YYYY');
                        time = moment(date_requested_utc).format('HH:mm');
                        var country = response.response[i]['country'];
                        var user_name = response.response[i]['user_name'];
                        var user_image = response.response[i]['user_image'];
                        var subject_name = response.response[i]['subject_name'];
                        if (approve == 0) {
                            var status_tutor ="Waiting for Tutor approval";
                        }else if (approve == 1) {
                            var status_tutor ="Confirmed by Tutor";
                        }else{ 
                            var status_tutor ="Tutor is not Available"
                        }

                        var data_group ="<div class='col-xs-12' style='background-color: #d6d6d6'><div style='width:103%; margin-left:-1.5%; padding:1px; background-color:white;' class='m-t-5 m-b-5 col-xs-12'><div  class='col-xs12 m-l-25'>"+day+", "+date+" - "+time+"</div><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:#ff9800; border-bottom-style: solid; margin-bottom:10px;'></div><div class='col-xs-3 col-sm-1 ' style=' width:80px;'><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+" width='60px' height='60px' style=' '><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5 ' style='width:44% height:70px;' ><h6 class=' c-red' style='font-size:11px; '>"+user_name+"</h6><h6 class='c-blue' style=' font-size:10px; height:12px; ; cursor:pointer;  margin-right:-28px;'>"+subject_name+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'>"+status_tutor+"</h5></div></div></div>";
                        // console.warn('jhvjh');
                        $("#list_group").append(data_group);
                    }
                }

            }
        });

        $.ajax({
            url: '<?php echo base_url(); ?>Rest/get_friendinvitation/access_token/'+tokenjwt,
            type: 'POST',
            data: {
                id_user: id_user,
                user_utc: user_utc,
                user_device: 'web'
            },
            success: function(response)
            {   
                console.warn(response);
                // var a = JSON.parse(response);
                var a = JSON.stringify(response['response']);
                // var jsonPretty = JSON.stringify(JSON.parse(a),null,2);                      
                // $("#aaa").text(jsonPretty); 
                console.warn('total '+response.response.length);

                if (response.response == 0) 
                {                            
                    $('#img_nodata_invitation').css('display','block');
                   $('#lbl_nodata_invitation').css('display','block');                 
                }
                else{
                     $('#img_nodata_invitation').css('display','none');
                   $('#lbl_nodata_invitation').css('display','none');
                    for (var i = 0; i< response.response.length;i++) {

                        var request_id = response.response[i]['request_id'];
                        var avtime_id = response.response[i]['avtime_id'];
                        var id_user_requester = response.response[i]['id_user_requester'];
                        var tutor_id = response.response[i]['tutor_id'];
                        var subject_id = response.response[i]['subject_id'];
                        var topic = response.response[i]['topic'];
                        var date_requested = response.response[i]['date_requested'];
                        var duration_requested = response.response[i]['duration_requested'];
                        var approve = response.response[i]['approve'];
                        var template = response.response[i]['template'];
                        var user_utc = response.response[i]['user_utc'];
                        var datetime = response.response[i]['datetime'];
                        var date_requested_utc = response.response[i]['date_requested_utc'];
                        day = moment(date_requested_utc).format('dddd');
                        date = moment(date_requested_utc).format('DD MMMM YYYY');
                        time = moment(date_requested_utc).format('HH:mm');
                        var country = response.response[i]['country'];
                        var user_name = response.response[i]['user_name'];
                        var user_image = response.response[i]['user_image'];
                        var subject_name = response.response[i]['subject_name'];
                        if (approve == 0) {
                            var status_tutor ="Waiting for Tutor approval";
                        }else if (approve == 1) {
                            var status_tutor ="Confirmed by Tutor";
                        }else{ 
                            var status_tutor ="Tutor is not Available"
                        }

                        var data_invitation ="<div class='col-xs-12' style='background-color: #d6d6d6'><div style='width:103%; margin-left:-1.5%; padding:1px; background-color:white;' class='m-t-5 m-b-5 col-xs-12'><div  class='col-xs12 m-l-25'>"+day+", "+date+" - "+time+"</div><div class='col-xs-12' style='border-width: 1px; border-bottom-width:1px;border-bottom-color:#ff9800; border-bottom-style: solid; margin-bottom:10px;'></div><div class='col-xs-3 col-sm-1 ' style=' width:80px;'><img class='img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+" width='60px' height='60px' style=' '><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1;  filter: grayscale(00%); position: absolute; height: 15px; width: 30px; bottom: 0; right: 0;'></div><div class='col-xs-5 ' style='width:44% height:70px;' ><h6 class=' c-red' style='font-size:11px; '>"+user_name+"</h6><h6 class='c-blue' style=' font-size:10px; height:12px; ; cursor:pointer;  margin-right:-28px;'>"+subject_name+"</h6><h5 class='c-gray' style=' margin-top:-5%; margin-right:-22px;'>"+status_tutor+"</h5></div></div></div>";
                        // console.warn('jhvjh');
                        $("#list_invitation").append(data_invitation);
                    }
                }

                    
               
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {

            var date = null;
            var tgll = new Date();  
            var formattedDatee = moment(tgll).format('YYYY-MM-DD'); 
            var a = [];
            var b = [];            
            var c = [];
            var d = [];
            var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";

            $('#dateon').on('dp.change', function(e) {
                date = moment(e.date).format('YYYY-MM-DD');                
                var hoursnoww = tgll.getHours(); 
                // alert(tgl);
                if (hoursnoww == 23) {                    
                    notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',"Minimum an hour required between request time and classroom schedule.");
                    $("#button_search").attr('disabled');
                }
                else
                {
                    if (date == formattedDatee) {
                        a = [];
                        $("#tempatwaktuu").html("");
                        $("#tempatwaktuu").append("<input type='text' class='form-control' id='timeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeee").removeAttr('disabled');
                        for (var i = hoursnoww+1; i < 24; i++) {                                                
                            a.push(i);
                        }
                        console.warn(a);
                        $('#timeee').datetimepicker({                    
                            sideBySide: true, 
                            showClose: true,                   
                            format: 'HH:mm',
                            stepping: 15,
                            enabledHours: a,
                        });
                    }
                    else
                    {
                        $("#tempatwaktuu").html("");
                        $("#tempatwaktuu").append("<input type='text' class='form-control' id='timeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeee").removeAttr('disabled');
                        b = [];
                        for (var i = 0; i < 24; i++) {                                                
                            b.push(i);
                        }
                        console.warn(b);
                        $('#timeee').datetimepicker({                    
                            sideBySide: true,                    
                            format: 'HH:mm',
                            showClose: true,
                            stepping: 15,
                            enabledHours: b,
                        });
                    }
                }
            });

            $('#dateong').on('dp.change', function(e) {
                date = moment(e.date).format('YYYY-MM-DD');                
                var hoursnoww = tgll.getHours(); 
                // alert(tgl);
                
                if (hoursnoww == 23) {
                    notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',"Minimum an hour required between request time and classroom schedule.");
                }
                else
                {
                    if (date == formattedDatee) {
                        c = [];
                        $("#tempatwaktuuu").html("");
                        $("#tempatwaktuuu").append("<input type='text' class='form-control' id='timeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeeee").removeAttr('disabled');
                        for (var i = hoursnoww+1; i < 24; i++) {                                                
                            c.push(i);
                        }
                        console.warn(c);
                        $('#timeeee').datetimepicker({                    
                            sideBySide: true, 
                            showClose: true,                   
                            format: 'HH:mm',
                            stepping: 15,
                            enabledHours: c,
                        });
                    }
                    else
                    {
                        $("#tempatwaktuuu").html("");
                        $("#tempatwaktuuu").append("<input type='text' class='form-control' id='timeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeeee").removeAttr('disabled');
                        d = [];
                        for (var i = 0; i < 24; i++) {                                                
                            d.push(i);
                        }
                        console.warn(d);
                        $('#timeeee').datetimepicker({                    
                            sideBySide: true,                    
                            format: 'HH:mm',
                            showClose: true,
                            stepping: 15,
                            enabledHours: d,
                        });
                    }
                }
            });

            $(function () {
                var tgl = new Date();  
                var formattedDate = moment(tgl).format('YYYY-MM-DD');         
                
                $('#dateon').datetimepicker({  
                    minDate: moment(formattedDate, 'YYYY-MM-DD')
                });

                $('#dateong').datetimepicker({  
                    minDate: moment(formattedDate, 'YYYY-MM-DD')
                });
                
            });
            $(".select2").width(210);
            $('select.select2').each(function(){
                
                if($(this).attr('id') == 'tagorang'){
                	$(this).select2({
						delay: 2000,
						tokenSeparators: [','],
						maximumSelectionLength: 3,					
						ajax: {
							dataType: 'json',
							type: 'GET',
							url: '<?php echo base_url(); ?>ajaxer/getNameuser',
							data: function (params) {
								return {
								  term: params.term,
								  page: params.page || 1
								};
							},
							processResults: function(data){
								return {
									results: data.results,
									pagination: {
										more: data.more
									}						
								};
							}					
						}
					});  
                }
				                  
                
            });

            var namaa = [];
            var ininama;
            $.ajax({
				url: '<?php echo base_url(); ?>Rest/getnameuser',
				type: 'POST',				
				success: function(data)
				{	
					var a = JSON.stringify(data['response']);
                    var b = JSON.parse(a);
                    ininama = b;
                    // console.warn(b);
                    // console.warn(b.length);
                    for (i = 0; i < b.length; i++) {
                        var maskid = "";
                        var myemailId =  b[i].email;
                        var prefix= myemailId.substring(0, myemailId .lastIndexOf("@"));
                        var postfix= myemailId.substring(myemailId .lastIndexOf("@"));

                        for(var j=0; j<prefix.length; j++){
                            if(j >= prefix.length - 4 ) {   
                                maskid = maskid + "*";////////
                            }
                            else {
                                maskid = maskid + prefix[j].toString();
                            }
                        }
                        maskid =maskid +postfix;
                        namaa.push(b[i].user_name+" ("+maskid+")");
                    }
				}
			});	

            $(document).on("click", ".openmodaladd", function () {
                var that = $(this);
                var a = $(this).attr('tutor_id');
                var b = $("."+a).attr('idtutor');
                var c = $(".openmodaladd."+a).attr('tutor_id');
                var iduser = "<?php echo $this->session->userdata('id_user');?>";                  
                var tokenjwttt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
                // var hargamapel = $(this).attr('hargamapel'); 
                var gh = $(this).attr('gh');
                var date = $(this).attr('dates');

                if ($('.'+b).is(":checked") || $('.'+b).is(":checked")) {                    
                    $.ajax({
                        url: '<?php echo base_url(); ?>Rest/isEnoughBalance/access_token/'+tokenjwttt,
                        type: 'POST',
                        data: {
                            id_user: iduser,
                            gh : gh                        
                        },               
                        success: function(data)
                        {             
                            balanceuser = data['balance'];
                            // alert(hargamapel);
                            // alert(balanceuser);

                            if (data['status'])
                            {
                                var metod = $("input[name='bebanmetod']:checked").val();
                                var user_utc = new Date().getTimezoneOffset();
                                user_utc = -1 * user_utc;                                
                                
                                if (a == c) {
                                    var subject_idg = that.attr('subject_id');
    							    var start_timeg = that.attr('start_time');
    							    var avtime_idg = that.attr('avtime_id');
    							    var durationg = that.attr('duration');
                                    var user_name = that.attr('username');
                                    var image_user = that.attr('image');
                                    var tutor_id = c;
    							    // var io = $(".openmodaladd").attr('durationgg');
    							    // alert(durationgg);
                                    
                                    $(".modal-body #data_subject_id").val( subject_idg );
                                    $(".modal-body #data_start_time").val( start_timeg );
                                    $(".modal-body #data_avtime_id").val( avtime_idg );
                                    $(".modal-body #data_duration").val( durationg );
                                    $(".modal-body #data_metod").val( metod );                                
                                    
                                    $('.btn-req-group-demand').attr('demand-link-group','<?php echo base_url(); ?>process/demand_me_grup?start_time='+start_timeg+"&tutor_id="+tutor_id+"&subject_id="+subject_idg+"&duration="+durationg+"&metod="+metod+"&date="+date+"&user_utc="+user_utc);
                                    $("#modaladdfriends").modal('show');
                                }
                            }
                            
                            else
                            {
                                notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Saldo anda tidak mencukupi! Mohon isi dahulu saldo anda");
                            }
                        }
                    }); 
                }
                else
                {
                    notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',"Mohon pilih metode pembayaran terlebih dahulu!");
                }
                
            });
            $(document.body).on('click', '.btn-req-group-demand' ,function(e){ 
                 // var a = $("#tags3").val();
                 // var array;
                 var idtage = $("#tagorang").val()+ '';

                 // alert(idtag);
                 
                 // alert(link);
                //  array = idtage.split(",");

                // for (var i = 0; i < array.length; i++) {
                    
                //     console.log(i+" id "+array[i]);
                // }       
                var link = $(".btn-req-group-demand").attr('demand-link-group')+"&id_friends="+idtage;
                $.get(link,function(data){
                    location.reload();
                });
            });   

            });
    </script>

    <script type="text/javascript">

    // $('input.timepicker').timepicker({
    //     timeFormat: 'HH:mm:ss',
    //     minTime: '11:45:00',
    //     maxHour: 20,
    //     maxMinutes: 30,
    //     startTime: new Date(0,0,0,15,0,0), // 3:00:00 PM - noon
    //     interval: 15 // 15 minutes
    // });

    $('#subject_id').change(function(e){
        $('#subjecton').val($(this).val());
    });
    // $('#time').change(function(e){        
    //     $('#timeon').val($(this).val());
    // });

    $('#duration').change(function(e){
        $('#durationon').val($(this).val());
    });

    $('#subject_idg').change(function(e){
        $('#subjectong').val($(this).val());
    });
    // $('#timeg').change(function(e){
    //     $('#timeong').val($(this).val());
    // });
    $('#durationg').change(function(e){
        $('#durationong').val($(this).val());
    });

    var cekkotak = 0;
    $("#btntambah").click(function(){
        if (cekkotak == 0) 
        {            
            $("#kotakpilihanteman").append("<div class='col-md-8 m-b-25' style='padding: 0;' id='kotakpilihanteman2'><div class='col-md-12'><label for='nama'>Cari Temanmu</label><input type='text' class='form-control typeahead' id='nama' data-provide='typeahead' autocomplete='off'></div></div><div class='col-md-4'><button class='btn btn-info waves-effect btn-sm m-t-20' id='btntambah'><i class='zmdi zmdi-plus'></i></button><button class='btn btn-danger waves-effect btn-sm m-t-20' id='btnkurang'><i class='zmdi zmdi-minus'></i></button></div>");
            cekkotak+1;
        }        
    });

    /*$('.btn-confirm').click(function(e){
        alert('yes');
       
    });*/
    $('#button_search').click(function(){
        
    })
    $(document.body).on('click', '.btn-ask' ,function(e){
        var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc;

        var subject_id = $(this).attr('subject_id');
        var start_time = $(this).attr('start_time');
        var date = $(this).attr('dates');
        var duration = $(this).attr('duration');    
        var tutor_id = $(this).attr('tutor_id');

        var subject_idg = $(this).attr('subject_idg');
        var start_timeg = $(this).attr('start_timeg');
        var avtime_idg = $(this).attr('avtime_idg');
        var durationg = $(this).attr('durationg'); 

        var user_name = $(this).attr('username');
        var image_user = $(this).attr('image');
        var harga = $(this).attr('harga');
        var gh = $(this).attr('gh');
        var iduser = "<?php echo $this->session->userdata('id_user');?>";
        var tokenjwtt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";

        $('#namemodal').text(user_name);
        $('#timestart').text("Rp. "+harga);
        $('#image').attr("src", image_user);        

        if (subject_id == '' && start_time == '' && avtime_id == '' && duration == '') {
            alert('tidak boleh kosong');
        }
        else
        {
            $.ajax({
                url: '<?php echo base_url(); ?>/Rest/isEnoughBalance/access_token/'+tokenjwtt,
                type: 'POST',
                data: {
                    id_user: iduser,
                    gh : gh
                },               
                success: function(data)
                {
                    balanceuser = data['balance'];
                            // alert(harga);
                            // alert(balanceuser);
                    // console.log(data);
                    if (data['status'])
                    {
                        $('.btn-req-demand').attr('demand-link','<?php echo base_url(); ?>/process/demand_me?start_time='+start_time+"&tutor_id="+tutor_id+"&subject_id="+subject_id+"&duration="+duration+"&date="+date+"&user_utc="+user_utc);
                        $('#modalconfrim').modal('show');
                    } else {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
                    }
                }
            });
        }
    });
    $(document.body).on('click', '.btn-req-demand' ,function(e){
        $.get($(this).attr('demand-link'),function(data){
            console.log(data);
            location.reload();
        });
    });    

    $("#box_privateclass").css('opacity','100');
    // $("#box_groupclass").css('opacity','0');    
    $("#kotaknyagroup").removeClass('bgm-orange');
    $("#kotaknyagroup").addClass('bgm-gray');    
    $("#privateclassclick").click(function(){ 
        $("#kotaknyagroup").removeClass('bgm-orange');
        $("#kotaknyagroup").addClass('bgm-gray');
        $("#kotaknyaprivate").removeClass('bgm-gray');
        $("#kotaknyaprivate").addClass('bgm-orange');

        $("#box_groupclass").css('display','none');
        $("#privateclick").css('filter','grayscale();');
        $("#groupklik").css('filter','none');
        $("#box_privateclass").css('display','block');
        $('#modal_menu').modal('toggle');
    });
    $("#groupclassclick").click(function(){
        $("#kotaknyaprivate").removeClass('bgm-orange');
        $("#kotaknyaprivate").addClass('bgm-gray'); 
        $("#kotaknyagroup").removeClass('bgm-gray');
        $("#kotaknyagroup").addClass('bgm-orange');
        $("#box_privateclass").css('display','none');
        $("#box_groupclass").css('opacity','100');        
        $("#durasigroup").addClass('col-md-12');
        $("#box_groupclass").css('display','block');
        $('#modal_menu').modal('toggle');
    });


$(".tabmenu").click(function() {
    // body...
    $("#modal_loading").modal('show');
});
</script>
