
<div id="loader">
    <div class="page-loader" style="background-color:#1274b3;">
        <div class="preloader pl-xl pls-white">
            <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20" />
            </svg>
            <p>Classmiles . . .</p>
        </div>
    </div>
</div>
<style type="text/css">
	html, body, main-content {
	    height:100%;
	    overflow:hidden;
	    max-height: 100%;
	    background-image: url(../aset/img/headers/math.jpg);
	    background-repeat: no-repeat; 
	    background-position: 0 0;
	    background-size: cover; 
	    background-attachment: fixed;        
	}
    .outer-container {
        width: 100%;
        height: 100%;
        z-index: 20;
        text-align: center;
    }
    .inner-container {
        display: inline-block;
        position: relative;
        width: 100%;
        height: 100vh;
        background-color: #ececec;      
    }
    @media all and (orientation:portrait) {
        #content {
            width: 100%;
            height: 60vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
    }
    @media all and (orientation:landscape) {
        #content {
            width: 100%;
            height: 100vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
    }
</style>

<section id="content" class="tampilanandro">
    
    <!-- NAVBAR MENU  -->

    <div class="outer-container bgm-blue" style="z-index: 10;">                
        <!-- <img class="pull-right p-5 p-t-5" src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" height="28px" width="130px" style='position: absolute;'>    -->     

        <div class="inner-container bgm-lightblue" id="tempatwhiteboard">      
            <img src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" id="logoclassmiles" height="28px" width="130px" style="display:none; z-index: 1; position: absolute; margin-right: 7%; margin-top: 1%; right: 0; cursor: pointer;">                                                         
            <iframe id="whiteboardonly" width="100%" height="100%" style="padding: 0px; position: relative;"></iframe>         
        </div>
    </div>

</section>
<script type="text/javascript">
        try{
            //we replace default localStorage with our Android Database one
            window.localStorage=LocalStorage;    
        }catch(e){
            //LocalStorage class was not found. be sure to add it to the webview
                    // alert("LocalStorage ERROR : can't find android class LocalStorage. switching to raw localStorage")              
        }
    //then use your localStorage as usual
    localStorage.setItem('foo','it works')
    // alert(localStorage.getItem('foo'))
    
</script>

