<style type="text/css">
    /* scroller browser */
::-webkit-scrollbar {
    width: 9px;
}

/* Track */
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
    -webkit-border-radius: 7px;
    border-radius: 7px;
}

/* Handle */
::-webkit-scrollbar-thumb {
    -webkit-border-radius: 7px;
    border-radius: 7px;
    background: #a6a5a5;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
}
::-webkit-scrollbar-thumb:window-inactive {
    background: rgba(0,0,0,0.4); 
}
</style>
<ul class="header-inner clearfix">
    <li id="menu-trigger" data-trigger=".ha-menu" class="visible-xs">
        <div class="line-wrap">
            <div class="line top"></div>
            <div class="line center"></div>
            <div class="line bottom"></div>
        </div>
    </li>
    <li class="logo hidden-xs">
        <a href="#" class="m-l-20"><img src="<?php echo base_url(); ?>aset/img/logo/logoclass6.png" style='margin-top: -7px;' alt=""></a>
    </li>
    <li class="pull-right">
        <div id="usertypestudent" style="display: none;">
            <ul class="top-menu">

            <li data-toggle="tooltip" data-placement="bottom" title="Raise Hands">
                <a data-toggle="dropdown" href="">
                    <div style="background: url('../aset/images/raisehandd.png');
  background-size: 30px 30px;
  height: 30px;
  background-repeat: no-repeat;
  z-index: 90;"></div>
                </a>               
            </li>
            <li class="dropdown" id="notif" data-toggle="tooltip" data-placement="bottom" title="Raise Hands" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;" id_user="<?php echo $this->session->userdata('id_user');?>">
                <a data-toggle="dropdown" href="" ng-init="getnotif()">
                    <i class="tm-icon zmdi zmdi-local-phone"></i>
                    <i class='tmn-counts bara' ng-if="nes > '0'">{{nes}}</i>
                </a>
                <div class="dropdown-menu dropdown-menu-lg pull-right" >
                    <div class="listview" id="notifications" >
                        <div class="lv-header">
                            Raise Hand

                            <!-- <ul class="actions" data-toggle="tooltip" title="Read all" data-placement="right">
                                <li class="dropdown">   
                                    <a class="readsa" style="cursor: pointer;" id_user='<?php echo $this->session->userdata('id_user');?>' >
                                        <i class="zmdi zmdi-check-all" ></i>
                                    </a>                                                                  
                                </li>
                            </ul> -->
                        </div>                        
                        <div class="lv-body" id="scol" style="overflow-y:auto; height: 10px;">
                            <div class="col-md-12">

                                <div class="card-body card-padding">
                                    <div class="col-md-4 m-t-15">
                                        <div class="pull-left">
                                        Ramdhani
                                        </div>
                                    </div>
                                    <div class="col-md-8 m-t-15">
                                        <div class="pull-right">
                                        <a href="#"><img src="<?php echo base_url(); ?>aset/img/iconvideo.png"></a>
                                        </div>
                                    </div>
                                    <div class="col-md-4 m-t-15">
                                        <div class="pull-left">
                                        Fikri
                                        </div>
                                    </div>
                                    <div class="col-md-8 m-t-15">
                                        <div class="pull-right">
                                        <a href="#"><img src="<?php echo base_url(); ?>aset/img/iconvideo.png"></a>
                                        </div>
                                    </div>                                   
                                </div>
                                

                            </div>
                        </div>                     

                    </div>

                </div>
            </li>
            <li data-toggle="tooltip" data-placement="bottom" title="Chat" >
                <a href="" id="chat-trigger" data-trigger="#chat"><i class="tm-icon zmdi zmdi-comment-alt-text"></i></a>               
            </li>            
            <li class="dropdown">
                <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-more-vert"></i></a>
                <ul class="dropdown-menu dm-icon pull-right" style="margin: 5px;">                    
                    <li style="margin:4%;">
                        <div id="checkwhiteboard" style="border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
                            <input type="checkbox" name="klikwhiteboard" id="klikwhiteboard" value="klikwhiteboard" checked onchange="toggleCheckbox(this)"  style="display: none;">
                            <label for="klikwhiteboard" id="nsfwbtn" class="btn btn-info m-l-15 m-t-5 f-12" rel="tooltip" style="width: 90%; height: 30px;">Whiteboard.</label>
                        </div>
                    </li>
                    <li style="margin:4%;">
                        <div id="checkvideo" style="margin-top: -6%; border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
                            <input type="checkbox" name="klikvideo" id="klikvideo" value="klikvideo" checked onchange="toggleCheckbox(this)" style="display: none; margin-top: -3%;">
                            <label for="klikvideo" id="nsfwbtn" class="btn btn-info m-l-15 m-t-5" rel="tooltip" style="width: 90%; height: 30px;">Video.</label>
                        </div>
                    </li>
                    <li style="margin:4%;">
                        <div id="checkscreen" style="margin-top: -6%; border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
                            <input type="checkbox" name="klikscreen" id="klikscreen" value="klikscreen" checked onchange="toggleCheckbox(this)" style="display: none;margin-top: -3%; ">                                        
                            <label for="klikscreen" id="nsfwbtn" class="btn btn-info m-l-15 m-t-5" rel="tooltip" style="width: 90%; height: 30px;">Screen Share.</label></p>
                        </div>
                    </li>
                </ul>
            </li>
            <li data-toggle="tooltip" data-placement="bottom" title="Settings">
                <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-settings"></i></a>               
            </li>
            <li data-toggle="tooltip" data-placement="bottom" title="Logout">
                <a  data-toggle="modal" href="#modalDefault"><i class="tm-icon zmdi zmdi-power-setting"></i></a>               
            </li>
        </ul>
        </div>
        <div id="usertypetutor" style="display: none;">
            dd
        </div>
    </li>
</ul>
<div style="width: 125%; margin-left: -10%;">
    <div class="col-md-12" style="text-align: left; background-color: #03a2ea;">
        <nav class="ha-menu">
            <ul>
                <li class="pull-left" style="margin-left: 8%;"><a href="#" class="c-white">Tutor : <label id="nametutor"></label></a></li>
                <li style="margin-left: 23%;"><a href="#" class="c-white"><label id="classname"></label></a></li>
                <li class="pull-right" style="margin-right: 12%;"><a href="#" class="c-white"><label id="datenow"></label></a></li>
            </ul>
        </nav>
    </div>
    <center>
    <!-- <div class="col-md-4">
        <nav class="ha-menu">
            <ul>            
                <li><a href="#" class="c-white waves-effect">Bahasa Indonesia - Sinopsis</a></li>            
            </ul>
        </nav>
    </div>
    </center>
    <div class="col-md-4" style="text-align: right;">
        <nav class="ha-menu">
            <ul>            
                <li><a href="#" class="c-white waves-effect">Kamis, 12 Januari 2017</a></li>            
            </ul>
        </nav>
    </div> -->
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#btn_setindonesia').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
        });
        $('#btn_setenglish').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/english'); ?>',function(){ location.reload(); });
        });
    });
</script>