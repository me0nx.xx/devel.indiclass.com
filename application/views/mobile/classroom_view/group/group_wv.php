<div id="loader">
    <div class="page-loader" style="background-color:#1274b3;">
        <div class="preloader pl-xl pls-white">
            <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20" />
            </svg>
            <p>Classmiles. . .</p>
        </div>
    </div>
</div>

<style type="text/css">
    html, body, main-content {
        height:100%;
        overflow:hidden;
        max-height: 100%;
        background-image: url(../aset/img/headers/BGclassroom-01.png);
        background-repeat: no-repeat; 
        background-position: 0 0;
        background-size: cover; 
        background-attachment: fixed;
    }
    .video2 {
        object-fit: fill;
        padding: 0px;
        width: 100%;
        height: 100%;        
    }
    #menu1{
        bottom: 0px;
        position: relative;
    }
    .outer-container {
        width: 100%;
        height: 100%;
        text-align: center;
    }
    .inner-container {
        display: inline-block;
        position: relative;
        width: 100%;
        background-color: #ececec;      
    }
    .papanwhiteboard {
        display: inline-block;
        position: relative;
        width: 100%;
        background-color: #ececec;
        height: auto;
    }
    .papanscreen {
        display: none;
        position: relative;
        width: 100%;
        background-color: #ececec;
        height: auto;
    }
    .video-overlay {
        position: absolute;
        left: 0px;
        bottom: 0px;        
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 100%;
        height: 46vh;
        float: left;
        /*background-color: rgba(50, 50, 50, 0.3);*/
    }
    .chating {
        left: 0px;
        bottom: 0px;        
        padding: 5px 5px;
        margin-bottom: 3%;
        font-size: 16px;
        font-family: Helvetica;
        color: #FFF;
        width: 100%;
        float: left;
        text-align: left;
        background-color: rgba(50, 50, 50, 0.4);
    }
    .video-overlay1 {
        left: 0px;
        bottom: 5px;
        margin: 0px;
        margin-left: 2%;
        margin-bottom: 2%;      
        padding: 5px 5px;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 95%;
        float: left;
        /*background-color: rgba(50, 50, 50, 0.4);*/
    }
    .video-overlay2 {
        right: 0px;
        bottom: 0px;
        margin: 0px;
        padding: 5px 5px;
        margin-left: 3%;
        font-size: 20px;
        font-family: Helvetica;
        color: #FFF;
        width: 30%;
        float: left;
        background-color: rgba(50, 50, 50, 0.4);
    }
    #player {
        width: 100%;
        height: 60vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #player.toggle {
        width: 100%;
        height: 60vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
        -moz-transform:scale(1) rotate(90deg);
        -webkit-transform:scale(1) rotate(90deg);
        -o-transform:scale(1) rotate(90deg);
        -ms-transform:scale(1) rotate(90deg);
        transform:scale(1) rotate(90deg);
    }
    #content {
        width: 100%;
        height: 100vh;
        object-fit: fill;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #content.toggle {
        width: 100%;
        height: 50vh;
        padding: 0; margin: 0;      
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
        -moz-transform:scale(1) rotate(90deg);
        -webkit-transform:scale(1) rotate(90deg);
        -o-transform:scale(1) rotate(90deg);
        -ms-transform:scale(1) rotate(90deg);
        transform:scale(1) rotate(90deg);
    }
    #papanchat{
        display: block;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    #papanchat.toggle{
        display: none;
        transition: all .5s ease;
          -webkit-transition: all .5s ease;
          -moz-transition: all .5s ease;
          -ms-transition: all .5s ease;
          -o-transition: all .5s ease;
    }
    @media all and (orientation:portrait) {
        #content {
            width: 100%;
            height: 60vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        #player {
            width: 100%;
            height: 60vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        .papanwhiteboard {
            display: inline-block;
            position: relative;
            width: 100%;
            background-color: #ececec;
            height: 40vh;
        }
        .papanscreen {
            display: none;
            position: relative;
            width: 100%;
            background-color: #ececec;
            height: 40vh;
        }
    }

    @media all and (orientation:landscape) {
        #content {
            width: 100%;
            height: 100vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
        #player {
            width: 100%;
            height: 100vh;
            object-fit: fill;
            transition: all .5s ease;
              -webkit-transition: all .5s ease;
              -moz-transition: all .5s ease;
              -ms-transition: all .5s ease;
              -o-transition: all .5s ease;
        }
    }
</style>

<header id="header" class="clearfix" style="background-color: rgba(33, 150, 243, 0.5);">    
    <div style="margin-left: 7%; margin-right: 9%;">
        <ul class="header-inner clearfix">
            <li id="menu-trigger" data-trigger=".ha-menu" class="visible-xs">
                <div class="line-wrap">
                    <div class="line top"></div>
                    <div class="line center"></div>
                    <div class="line bottom"></div>
                </div>
            </li>
            <li class="logo hidden-xs">
                <a href="#" class="m-l-20"><img src="<?php echo base_url(); ?>aset/img/logo/logoclass6.png" style='margin-top: -7px;' alt=""></a>
            </li>
            <li class="pull-right">            

                <div id="usertypestudent" style="display: none;">
                    <ul class="top-menu">                        
                        <!-- <li class="dropdown" id="tour2">
                            <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-tv-list"></i></a>
                            <ul class="dropdown-menu dm-icon pull-right" style="margin: 5px;">                    
                                <li style="margin:4%;">
                                    <div id="checkwhiteboard" style="border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
                                        <input type="checkbox" name="klikwhiteboard" id="klikwhiteboard" value="klikwhiteboard" checked onchange="toggleCheckbox(this)"  style="display: none;">
                                        <label for="klikwhiteboard" id="nsfwbtn" class="btn btn-info m-l-15 m-t-5 f-12" rel="tooltip" style="width: 90%; height: 30px;">Whiteboard.</label>
                                    </div>
                                </li>
                                <li style="margin:4%;">
                                    <div id="checkvideo" style="margin-top: -6%; border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
                                        <input type="checkbox" name="klikvideo" id="klikvideo" value="klikvideo" checked onchange="toggleCheckbox(this)" style="display: none; margin-top: -3%;">
                                        <label for="klikvideo" id="nsfwbtn" class="btn btn-info m-l-15 m-t-5" rel="tooltip" style="width: 90%; height: 30px;">Video.</label>
                                    </div>
                                </li>
                                <li style="margin:4%;">
                                    <div id="checkscreen" style="margin-top: -6%; border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
                                        <input type="checkbox" name="klikscreen" id="klikscreen" value="klikscreen" checked onchange="toggleCheckbox(this)" style="display: none; margin-top: -3%; ">                                        
                                        <label for="klikscreen" id="nsfwbtn" class="btn btn-info m-l-15 m-t-5" rel="tooltip" style="width: 90%; height: 30px;">Screen Share.</label></p>
                                    </div>
                                </li>
                            </ul>
                        </li> -->
                        <!-- <li data-toggle="tooltip" data-placement="bottom" title="Raise Hands" style="cursor: pointer;" id="tour1">                            
                            <a id="raisehandclick">
                                <div id="icontangan">
                                    <div id="tanganbiasa" style="background: url('../aset/images/raisehandd.png'); background-size: 30px 30px; height: 30px; background-repeat: no-repeat; z-index: 90; margin-left: 9px;">
                                    </div>
                                    <div id="tanganmerah" style="background: url('../aset/images/raisehanddd.png'); background-size: 30px 30px; height: 30px; background-repeat: no-repeat; z-index: 90; margin-left: 9px; display: none;">
                                    </div>
                                </div>
                            </a>               
                        </li>  -->
                        <li style="cursor: pointer;" id="tour3">
                            <a id="chat-trigger" data-trigger="#chat"><i class="tm-icon zmdi zmdi-comment-alt-text" id="klikchat"></i></a>               
                        </li>                                
                        <!-- <li data-toggle="tooltip" data-placement="bottom" title="Settings">
                            <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-settings"></i></a>
                        </li> -->
                        <li style="cursor: pointer;" id="tour4">
                            <a id="tourlagi"><i class="tm-icon zmdi zmdi-navigation"></i></a>               
                        </li>
                        <!-- <li data-toggle="tooltip" data-placement="bottom" style="cursor: pointer;">
                          <a id="mute_murid"><i class="tm-icon zmdi zmdi-volume-up" ></i></a>
                        </li> -->
                        <li style="cursor: pointer;" id="tour5">
                            <a id="keluarcobaaa"><i class="tm-icon zmdi zmdi-power-setting"></i></a>               
                        </li>
                        <!-- <li  style="cursor: pointer;">
                            <a id="clearinterval"><i class="tm-icon zmdi zmdi-comment-alt-text"></i></a>               
                        </li>   -->
                    </ul>
                </div>

                <div id="usertypetutor" style="display: none;">
                    <ul class="top-menu">                                        
                        <!-- START PLAY AND PAUSE -->
                        <!-- <li data-toggle="tooltip" id="tourtutor1" data-placement="bottom" title="Raise Hands" style="cursor: pointer;">                            
                            <a id="bukakotakraise" data-toggle="dropdown">
                                <div style="background: url('../aset/images/raisehandd.png'); background-size: 30px 30px; height: 30px; background-repeat: no-repeat; z-index: 90; margin-left: 9px;">
                                </div>
                                <i class='tmn-counts bara' id="totalraisehand"></i>
                            </a>                          
                        </li> -->

                        <li id="tourtutor2" id="tempatx" style="cursor: pointer;">
                            <a href="#" id="play" style="display: none;"><i class="tm-icon zmdi zmdi-play"></i></a>
                            <a href="#" id="playboongan" style="display: none;"><i class="tm-icon zmdi zmdi-play"></i></a>
                            <!-- <button id="play" style="display: none;" title="Play"><i class="tm-icon zmdi zmdi-play"></i></button> -->
                            <a href="#" id="pause"><i class="tm-icon zmdi zmdi-pause"></i></a>
                        </li>
                        <!-- END PLAY AND PAUSE -->
                        <!-- START SHOW AND OFF -->


                        <li id="tourtutor3" style="cursor: pointer;">
                          <a href="#" id="unpublish"><i class="tm-icon zmdi zmdi-eye"></i></a>
                        </li>
                        <!-- <li data-toggle="tooltip" id="tourtutor3" data-placement="bottom" style="cursor: pointer;">
                          <a href="#" id="nyalalist"><i class="tm-icon zmdi zmdi-surround-sound"></i></a>
                        </li> -->
                        <!-- END SHOW AND OFF -->
                        <!-- START AUDIO -->
                        <li id="tourtutor4" style="cursor: pointer;">
                          <a href="#" id="mute"><i class="tm-icon zmdi zmdi-volume-up" ></i></a>
                        </li>
                        <!-- END AUDIO -->
                        <!-- START SCREENSHARE -->
                        <li id="tourtutor5" style="cursor: pointer;">
                          <a id="openscreenshare" target="_blank"><i class="tm-icon zmdi zmdi-window-maximize"></i></a>
                        </li>                    
                        <!-- END SCREEN SET -->
                        <li id="tourtutor6" style="cursor: pointer;">
                          <a id="chat-trigger" data-trigger="#chat"><i class="tm-icon zmdi zmdi-comment-alt-text" id="klikchat"></i></a>
                        </li>
                        <li id="tourtutor7" style="cursor: pointer;">
                          <a id="devicedetect"><i class="tm-icon zmdi zmdi-settings"></i></a>
                        </li>
                        <li id="tourtutor8" style="cursor: pointer;" >
                            <a id="tourlagii"><i class="tm-icon zmdi zmdi-navigation"></i></a>               
                        </li>
                        <li id="tourtutor9" style="cursor: pointer;">
                            <a id="keluarcobaa"><i class="tm-icon zmdi zmdi-power-setting"></i></a>               
                        </li>

                    </ul>
                </div>

            </li>
        </ul>
        <div style="width: 125%; margin-left: -10%;">
            <div class="col-md-12" style="text-align: left; background-color: rgba(33, 150, 243, 0.3);">
                <nav class="ha-menu">
                    <ul>
                        <li class="pull-left c-white" style="margin-left: 8%;"><label class="c-white">Tutor : <label id="nametutor"></label></label></li>
                        <li style="margin-left: 23%;"><label class="c-white"><label id="classname"></label></label></li>
                        <li class="pull-right" style="margin-right: 12%;"><label class="c-white"><label id="datenow"></label></label></li>
                    </ul>
                </nav>
            </div>                    
        </div>
    </div>   
     
</header>

<section id="main" class="tampilanweb" style="overflow: hidden; height: 100%;">

     <aside id="chat" class="sidebar c-overflow" style="margin-top: 3.7%; overflow: hidden;"> 
        
        <div class="col-md-12" id="panel1">
            <div class="chat-search" id="panel2" style="height: 40px; margin-top: -1%;">
                <label id="totalstudent" ></label>            
            </div>
            <hr>

            <div id="chatBoxScroll" class="ba" style="overflow-y: auto; margin-top: -6%; margin-bottom: -6%;">
                
            </div>
            
            <hr>
            <div class="chat-search" id="panel3" style="height: 80px; width: 100%; padding: 0;">            
                <div class="fg-line" id="panel4">

                    <!-- <textarea id="chatInput" rows="4" style="border: 1px #ececec solid; padding: 5px; resize: none;" class="form-control" placeholder="Type your message" maxlength="300"></textarea>                     -->
                    <textarea maxlength="300" id="chatInput" rows="4" style="width:100%; border: 1px #ececec solid; resize: none; padding: 0;" class="form-control btn-block" placeholder="Type your message"></textarea>
                    <!-- <script type="text/javascript">
                        $("#textarea-1").maxlength();
                        $("textarea").bind("update.maxlength", function(event, element, lastLength, length, maxLength, left){
                            console.log(event, element, lastLength, length, maxLength, left);
                        });
                    </script> -->
                    <!-- <input type="text" style="border: 1px #ececec solid; padding: 5px;" class="form-control" placeholder="Type your message" autocomplete="off" id="chatInput" class="ba"> -->                
                </div>
            </div><br>
        </div>
        
    </aside>

    <section id="content">
          
    <style type="text/css">
        #container {
            width: 100%;       
            position: relative;
            text-align:center;
        }
        #container div {
            position: relative;
            margin:3px;     
            background-color: #bfbfbf;
            border-radius:3px;
            text-align:center;
            display:inline-block;
        }
        #video{
            width: 100%;
        }

        .video2 {
            object-fit: fill;
            width: 100%;
            height: 100%;
        }
        #tempatstudent{
            width: 100%;     
        }
        #tempatstudent.toggle{
            width: 80%;     
        }
        #tempattutor{
            width: 100%;     
        }
        #tempattutor.toggle{
            width: 80%;     
        }
        .raise-hand-video-box{
          height: 360px; 
        }

    </style>
    
    <div id="tempatstudent" style="margin-top:4%; display: none;">
        <section class="row" id="container2" style="margin-left: 5%; margin-right: 5%; width: 90%; height: 70vh;">
        <!-- <div class="container"> -->
            <div class="col-md-12" id="fulled" style="height: 100%; display: none;">
                
            </div>
            <div class="col-md-8" id="setengah1" style="height: 100%;">
                <div class="move col-md-12" id="whboard" style="height: 100%; background-color: rgba(0, 0, 0, 0.3); padding: 0; margin: 0;">
                    <img id="pindah1" class="klikbego" src="<?php echo base_url();?>aset/images/arrow.png" width="30px" height="30px" style="display:none; z-index: 1; position: absolute; margin: 2%; cursor: pointer;" data-toggle="tooltip"
                    data-placement="bottom" title="Switch Screen">
                    <!-- <div class="col-md-12" style="z-index: 1px; position: absolute; padding-right: 3%;"><label class="bgm-blue f-20 pull-right " >Ini Whiteboard</label></div> -->
                    <!-- <div id="wbtutup" style="height: 100%; width: 100%; background-color: white; z-index: 10; position: absolute; opacity: 0.01;"></div> -->
                    <!-- <video id="videodia" disabled autoplay src="<?php echo base_url(); ?>aset/video/bigbunny.mp4" style="position: absolute; height: 100px; width: 100px;" /> -->                                            
                    <!-- <iframe id="wbcard" width="100%" height="100%" style="padding: 0px; z-index: 1;" ></iframe> -->
                    <video id="myvideo3" class="video2" autoplay muted ></video>
                    <!-- <iframe src="<?php echo base_url(); ?>First/aslkfsdklf" width="100%" height="100%" style="object-fit: fill;"></iframe> -->
                </div>

            </div>
            <div class="col-md-6" id="setengah2" style="height: 100%; display: none; margin-left: -1%;">
            </div>
            <div class="col-md-4" id="seperempat" style="height: 100%;" >
                <div class="move col-md-12" id="vdio" style="height: 49%; padding: 0px; margin-bottom: 2.5%;background: rgba(0, 0, 0, 0.3);" >
                    <img id="pindah2" class="klikbego" src="<?php echo base_url();?>aset/images/arrow.png" width="30px" height="30px" style="display:none; z-index: 1; position: absolute; margin: 2%; cursor: pointer;" data-toggle="tooltip" data-placement="bottom" title="Switch Screen">
                    
                    <!-- <video class="video2" id="privatetutorlocal" disabled width="50%" height="50%" autoplay src="<?php echo base_url(); ?>aset/video/bigbunny.mp4" style="display: none;"/>  -->

                    <video class="video2" id="myvideo" style="z-index: 1; position: absolute; margin: 2%; height: 100px; width: 100px; bottom: 0; right: 0;" autoplay loop muted></video>
                     
                </div>

                <div class="move col-md-12" id="sshare" style="height: 49%; padding: 0px; background-color: rgba(0, 0, 0, 0.3); width: 100%;">
                    <img id="pindah3" class="klikbego" src="<?php echo base_url();?>aset/images/arrow.png" width="30px" height="30px" style="display:none; z-index: 1; position: absolute; margin: 2%; cursor: pointer;" data-toggle="tooltip"
                    data-placement="bottom" title="Switch Screen">
                    <iframe id="screensharestudent" class="screen-share-iframe" scrolling="no" frameborder="0" allowfullscreen style="width: 100%; height: 100%; max-width: 100%;" ></iframe>
                </div>
            </div>
            <div class="move col-md-2" style="height: 100%; overflow-y: auto; padding: 0; display: none;" id="tempatstudent_private">
                <!-- <video class="video2" id="remotevideo2" style="z-index: 1; margin-top: 2%; height: 150px; width: 100%; " autoplay loop muted></video>
                <video class="video2" id="remotevideo3" style="z-index: 1;  height: 150px; width: 100%;" autoplay loop muted></video>
                <video class="video2" id="remotevideo4" style="z-index: 1;  height: 150px; width: 100%;" autoplay loop muted></video> -->
            </div>
            
        <!-- </div> -->
        </section> 
        <div id="berkas" class="col-md-12" style="display: none;"></div>        
    </div>

    <br>

    <div id="tempattutor" style="margin-top:4%; display: none;">
        <section class="row" id="container2" style="height: 70vh; width: 90%; margin-left: 5%; margin-right: 5%;">
        <div class="container">

            <div class="col-md-12" id="fulled" style="height: 100%; display: none;">
                
            </div>
            <div class="move col-md-6" style="height: 450px;" id="kotak1tutor">
                <div class=" col-md-12" id="whboardtutor" style="height: 450px; background-color: rgba(0, 0, 0, 0.3); padding: 0; margin: 0;">
                    <!-- <div class="col-md-12" style="z-index: 1px; position: absolute; padding-right: 3%;"><label class="bgm-blue f-20 pull-right " >Ini Whiteboard</label></div> -->
                    <img id="settingvideowb" class="klikwb" src="<?php echo base_url();?>aset/img/icons/Arrow 4-01.png" width="30px" height="30px" style="z-index: 1; position: absolute; margin: 2%; cursor: pointer; right:0;" data-toggle="tooltip" data-placement="bottom" title="Change Video"></img>
                    <video id="myvideo2" class="video2" autoplay muted ></video>
                </div>
            </div>
            <div class="move col-md-6" style="height: 450px; background-color: rgba(0, 0, 0, 0.3); object-fit: fill; padding: 0; margin: 0;" id="kotak2tutor">
                <!-- <div class="panel-body" id="videolocal"> -->
                <video class="video2" id="myvideot" style="z-index: 1; position: absolute; margin: 2%; height: 100px; width: 100px; bottom: 0; right: 0;" autoplay loop muted></video>
                <!-- <video class="video2" id="remotevideo11" disabled width="100%" height="100%" autoplay muted />   -->
            </div>
            <div class="move col-md-2" style="height: 450px; overflow-y: auto; margin-left: 1%; display: none;" id="tempatstudent_private_tutor">
                <!-- <video class="video2" id="remotevideo12" style="z-index: 1; margin-bottom: 2%; height: 150px; width: 100%; padding:0; " autoplay loop muted></video>
                <video class="video2" id="remotevideo13" style="z-index: 1;  height: 150px; width: 100%;" autoplay loop muted></video>                
                <video class="video2" id="remotevideo14" style="z-index: 1;  height: 150px; width: 100%;" autoplay loop muted></video>    -->             
            </div>
        </div>          
        </section> 
        <div id="berkas" class="col-md-12" style="display: none;"></div>       
    </div>
    <div id="tempatwaktu" class="text-center" style="position:absolute;
    width:300px;
    height:40px;
    background:rgba(3, 169, 244, 0.6);
    padding-left: 3px;
    padding-right: 3px;
    bottom:5px;
    border-radius: 5px;
    right:25%;
    left:50%;
    margin-left: -150px;
    display: none;">
        <h5 class="c-white">Classroom will be ended in <label id="countdown"></label></h5>
    </div>

    <script type="text/javascript">
        // var view_state = 0;
        // $('#container2').on('click','.move',function(e){
        //     // if(view_state == 0 && $(this).parent().attr('id') != 'seperempat' && $(this).parent().attr('id') != 'setengah2' && $(this).parent().attr('id') != 'fulled'){
        //         var that = $(this);
        //         var old_view = $('#tempatstudent_private1').find('div');
        //         var i=1;
        //         var new_view1;
        //         var new_view2;
        //         // $('#tempatstudent_private1 video').each(function(e){
        //         //     if(i == 1){
        //         //         new_view1 = $(this);    
        //         //     }else{
        //         //         new_view2 = $(this);    
        //         //     }
                    
        //         //     i++;
        //         // });
                
        //         $(that).find('video').css('margin-bottom','');
        //         $(that).appendTo('#tempatstudent_private1');
        //         $(that).css('height','300px');
        //         // $('#tempatstudent_private2').find('video').css('margin-bottom','2.5%');
        //         $(old_view).appendTo('#tempatstudent_private2');
        //         old_view.show('slow');
        //         old_view.css('height','150px');
        //         old_view.css('width','400px');
        //     // }else if(view_state > 0){
        //     //     return false;
        //     // }
        //     $('#myvideo').get(0).play();
        //     // $('#remotevideot').get(0).play();        
        //     $('#myvideot').get(0).play();
        //     $('#remotevideo1').get(0).play();
        //     $('#remotevideo2').get(0).play();
        //     $('#remotevideo3').get(0).play();
        //     $('#remotevideo4').get(0).play();
        //     $('#remotevideo11').get(0).play();
        //     $('#remotevideo12').get(0).play();
        //     $('#remotevideo13').get(0).play();
        //     $('#remotevideo14').get(0).play();
        // });
    </script>
<script type="text/javascript">
    var view_state = 0;
    // 0 = 3 3 nya ada
    // 1 = 2 setengah2
    // 2 = 1 full
    $('#container2').on('click','.move',function(e){
        if(view_state == 0 && $(this).parent().attr('id') != 'setengah1' && $(this).parent().attr('id') != 'setengah2' && $(this).parent().attr('id') != 'fulled'){
            var that = $(this);
            var old_view = $('#setengah1').find('div');
            var i=1;
            var new_view1;
            var new_view2;
            $('#seperempat div').each(function(e){
                if(i == 1){
                    new_view1 = $(this);    
                }else{
                    new_view2 = $(this);    
                }
                
                i++;
            });
            
            $(that).find('div').css('margin-bottom','');
            $(that).appendTo('#setengah1');
            $(that).css('height','100%');
            $('#seperempat').find('div').css('margin-bottom','2.5%');
            $(old_view).appendTo('#seperempat');
            old_view.show('slow');
            old_view.css('height','49%');
        }else if(view_state > 0){
            return false;
        }
        $('#myvideo').get(0).play();
        // $('#remotevideot').get(0).play();        
        $('#myvideot').get(0).play();
        $('#remotevideo1').get(0).play();
        $('#remotevideo2').get(0).play();
        $('#remotevideo3').get(0).play();
        $('#remotevideo4').get(0).play();
        $('#remotevideo11').get(0).play();
        $('#remotevideo12').get(0).play();
        $('#remotevideo13').get(0).play();
        $('#remotevideo14').get(0).play();
    });
</script>
<script type="text/javascript">
    
    // $("video").bind("contextmenu",function(){
    //     return false;
    // });

    var urutan = new Array(2);
    var i=0;
    $('#seperempat div').each(function(e){
        urutan[i++] = $(this).attr('id');
    });

    function toggleCheckboxtutor(element)
    {
        if(klikwhiteboardtutor.checked && klikvideotutor.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-6');
            $('#setengah2').css('display','block');
            $('#setengah2').attr('class','col-md-6');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#fulled').css('display','none');
        }  
        
        else if (klikwhiteboardtutor.checked) {
            $('#setengah2').css('display','none');
            $('#setengah2').css('display','col-md-1');
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-12');
            $('#fulled').css('display','block');
            $('#whboardtutor').appendTo('#fulled');
            $('#whboardtutor').css('height','100%');
            $('#whboardtutor').css('width','100%');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#vdiotutor').appendTo('#berkas');
                                                    
        } 
        else if (klikvideotutor.checked) {
            $('#setengah1').css('display','none');
            $('#setengah2').css('display','none');
            $('#fulled').css('display','block');
            $('#vdiotutor').appendTo('#fulled');
            $('#vdiotutor').css('height','100%');
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#whboardtutor').appendTo('#berkas');
        }
        else
        {
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', '#ececec'); 
        }
        $('#myvideo').get(0).play();
        // $('#remotevideot').get(0).play();
        // $('#remotevideo').get(0).play();
        $('#myvideot').get(0).play();

        $('#remotevideo1').get(0).play();
        $('#remotevideo2').get(0).play();
        $('#remotevideo3').get(0).play();
        $('#remotevideo4').get(0).play();
        $('#remotevideo11').get(0).play();
        $('#remotevideo12').get(0).play();
        $('#remotevideo13').get(0).play();
        $('#remotevideo14').get(0).play();
    }
    function toggleCheckbox(element)
    {
        if(klikwhiteboard.checked && klikvideo.checked && klikscreen.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-8');
            $('#setengah2').css('display','none');
            $('#setengah2').attr('class','col-md-4');
            $('#seperempat').css('display','block');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#fulled').css('display','none');
            $('#berkas').find('div').appendTo('#seperempat');
            $('#setengah2').find('div').appendTo('#seperempat');
            $('#seperempat').find('div').css('margin-bottom','2.5%');
            $('#seperempat div').each(function(e){
                $(this).css('height','49%');
            });
        }
        else if (klikwhiteboard.checked && klikvideo.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-6');
            $('#setengah2').css('display','block');
            $('#setengah2').attr('class','col-md-6');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', '#ececec');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','none');
            
            var i = 0;
            $('#seperempat div').each(function(e){
                urutan[i++] = $(this).attr('id');
            });            
            $('#sshare').appendTo('#berkas');
            if($('#setengah1').find('div').attr('id') == 'whboard'){
                $('#vdio').appendTo('#setengah2');
                $('#vdio').css('height','100%');

            }else if($('#setengah1').find('div').attr('id') == 'vdio'){
                $('#whboard').appendTo('#setengah2');
                $('#whboard').css('height','100%');

            }else{
                $('#vdio').appendTo('#setengah1');
                $('#whboard').appendTo('#setengah2');
                $('#vdio').css('height','100%');
                $('#whboard').css('height','100%');
            }

        }  
        else if (klikwhiteboard.checked && klikscreen.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-6');
            $('#setengah2').css('display','block');
            $('#setengah2').attr('class','col-md-6');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','none');
            $('#vdio').appendTo('#berkas');
            if($('#setengah1').find('div').attr('id') == 'whboard'){
                $('#sshare').appendTo('#setengah2');
                $('#sshare').css('height','100%');

            }else if($('#setengah1').find('div').attr('id') == 'sshare'){
                $('#whboard').appendTo('#setengah2');
                $('#whboard').css('height','100%');

            }else{
                $('#sshare').appendTo('#setengah1');
                $('#whboard').appendTo('#setengah2');
                $('#sshare').css('height','100%');
                $('#whboard').css('height','100%');
            }                          
        }
        else if (klikvideo.checked && klikscreen.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-6');
            $('#setengah2').css('display','block');
            $('#setengah2').attr('class','col-md-6');
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','none');
            $('#whboard').appendTo('#berkas');
            if($('#setengah1').find('div').attr('id') == 'sshare'){
                $('#vdio').appendTo('#setengah2');
                $('#vdio').css('height','100%');

            }else if($('#setengah1').find('div').attr('id') == 'vdio'){
                $('#sshare').appendTo('#setengah2');
                $('#sshare').css('height','100%');

            }else{
                $('#vdio').appendTo('#setengah1');
                $('#sshare').appendTo('#setengah2');
                $('#vdio').css('height','100%');
                $('#sshare').css('height','100%');
            }                 
        } 
        else if (klikwhiteboard.checked) {
            $('#setengah1').css('display','none');
            $('#setengah2').css('display','none');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','block');
            $('#whboard').appendTo('#fulled');
            $('#whboard').css('height','100%');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', '#ececec');
            $('#vdio').appendTo('#berkas');
            $('#sshare').appendTo('#berkas');
                                                    
        } 
        else if (klikvideo.checked) {
            $('#setengah1').css('display','none');
            $('#setengah2').css('display','none');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','block');
            $('#vdio').appendTo('#fulled');
            $('#vdio').css('height','100%');
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', '#ececec');
            $('#whboard').appendTo('#berkas');
            $('#sshare').appendTo('#berkas');
        }
        else if (klikscreen.checked) {
            $('#setengah1').css('display','none');
            $('#setengah2').css('display','none');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','block');
            $('#sshare').appendTo('#fulled');
            $('#sshare').css('height','100%');
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#whboard').appendTo('#berkas');
            $('#vdio').appendTo('#berkas');
        }
        else
        {
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', '#ececec'); 
        }
        $('#myvideo').get(0).play();
        // $('#remotevideot').get(0).play();
        // $('#remotevideo').get(0).play();
        $('#myvideot').get(0).play();

        $('#remotevideo1').get(0).play();
        $('#remotevideo2').get(0).play();
        $('#remotevideo3').get(0).play();
        $('#remotevideo4').get(0).play();
        $('#remotevideo11').get(0).play();
        $('#remotevideo12').get(0).play();
        $('#remotevideo13').get(0).play();
        $('#remotevideo14').get(0).play();
    }

    // $("#keluarcobaaa").click(function(){        
    //     $("#classkeluar").modal("show");
    // });

    // $("#keluarcobaa").click(function(){     
    //     $("#classkeluar").modal("show");
    // });
</script>
    </section>

    <div id="classkeluar" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p class="f-15 pull-left m-t-20">Anda Yakin Keluar ?</p><br><br><br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    <button type="button" class="btn btn-info" id="keluar">OK</button>
                </div>
            </div>
        </div>
    </div>

    <div id="deviceConfigurationModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Change Video Devices <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-offset-2 col-md-8" id="buttonlist">
                  <!-- <button class="btn btn-default btn-block" id="iddevice">
                    <label id="listdevice"></label>
                  </button> -->
                </div>
              </div>
            </div>
          </div>
        </div>
    </div> 

    <div id="deviceConfigurationModalWB" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"><?php echo $this->lang->line('ubahcamera'); ?> Whiteboard<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-offset-2 col-md-8" id="buttonlistWB">
                <!-- <button class="btn btn-default btn-block" id="iddevice">
                  <label id="listdevice"></label>
                </button> -->
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>  

    <div id="kotakmenulist" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">List User Raise Hands <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-offset-2 col-md-8" id="kotakraisehand">
                  <!-- <button class="btn btn-default btn-block" id="iddevice">
                    <label id="listdevice"></label>
                  </button> -->
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>  

</section>

<section id="content" class="tampilanandro">
    
    <!-- NAVBAR MENU  -->
    <div class="" style="position: absolute; width: 100%; z-index: 1;">
        <div class="pull-left p-5 p-t-10">          
            <img src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" height="37px" width="155px" >
        </div>
        <div class="pull-right p-15">
            <!-- <div class="m-l-5" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
                <a data-toggle="modal" href="#modalDefault"><img src="<?php echo base_url(); ?>aset/img/icons/close.png" height="30px" width="30px"></a>
            </div> -->
            <!-- <div class="m-l-5" id="showchat" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
                <a ><img src="<?php echo base_url(); ?>aset/img/icons/comment.png" height="35px" width="35px"></a>
            </div> -->
            <!-- <div class="m-l-5" id="showpapan" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
                <a href="#"><img src="<?php echo base_url(); ?>aset/img/icons/whiteboard.png" height="35px" width="35px"></a>
            </div> -->
            <div class="m-l-5" style="background-color: rgba(50, 50, 50, 0.3); border-radius: 4px; float: right;">
                <a id="raisehandclickAndroid" ><img src="<?php echo base_url(); ?>aset/img/icons/raisehandd.png" height="35px" width="35px"></a>
            </div>
        </div>
    </div>

    <div class="outer-container" style="z-index: 2;">
        <div class="inner-container" id="tempatplayer">                                                               
            <video id="player" autoplay loop></video>          
        </div>
        <div class="video-overlay" id="papanchat" style="z-index: 10;">
                
            <div class="video-overlay1" style="position: absolute;">
                <div style="height:38vh; bottom: 0; overflow-y: auto; margin-bottom: 2%;" id="chatAndroid">
                    <!-- <div class="chating">
                        <label>Robith Ritz<br><small style="word-wrap: break-word;">koasdflksdjfkladsjfkladsjlfdaslfjldsakfjkldasjfkldsajflkdasjflkdsajklfjdsalkfjlksdafjklsadjflkdsajflkdsjflksdjflkdsjf</small></label>
                    </div>
                    <div class="chating">
                        <label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
                    </div>
                    <div class="chating">
                        <label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
                    </div>
                    <div class="chating">
                        <label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
                    </div>
                    <div class="chating">
                        <label>Robith Ritz<br><small style="word-wrap: break-word;">testing aja ko</small></label>
                    </div> -->
                </div>
                <div style="width: 100%;">
                    <input maxlength="300" id="chatInputAndroid" type="text" name="sent" class="input form-control c-black" style="padding: 5px;" placeholder="Type your message">
                </div>
            </div>
        </div>
        <div class="papanwhiteboard c-black">
            <a href="#" id="papanwhiteboard"><img src="<?php echo base_url() ?>aset/img/icons/rotate-3d.png" width="35px" height="35px" style="z-index: 1; position: absolute; margin: 2%; left:0;"></a>
            <iframe id="wbcarddd" width="100%" height="100%" style="padding: 0px;"></iframe>
        </div>
        <div class="papanscreen c-black">
            <a href="#" id="papanscreen"><img src="<?php echo base_url() ?>aset/img/icons/rotate-3d.png" width="35px" height="35px" style="z-index: 1; position: absolute; margin: 2%; left:0;"></a>
            <iframe id="screensharestudenttt" class="screen-share-iframe" scrolling="no" frameborder="0" allowfullscreen style="width: 100%; height: 100%; max-width: 100%;" ></iframe>
        </div>
    </div>

    <!-- MODAL RAISE HAND -->   
    <!-- <div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">   
                <div class="modal-body">
                    <video src="<?php echo base_url(); ?>aset/video/line.mp4" autoplay loop muted style="height: 30vh; width: 100%; padding: 0; margin-top: 10%;"></video>  
                    <video src="<?php echo base_url(); ?>aset/video/juki.mp4" autoplay loop muted style="height: 30vh; width: 100%; padding: 0; margin-top: 10%;"></video>  
                    
                </div>             
                <div class="modal-footer">                    
                    <hr>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hangout</button>                    
                </div>
            </div>
        </div>
    </div>
 -->
</section>

<div id="videoCallModall" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg modal-raise-hand" style="width: 85%;">
      <div class="modal-content">
        <div class="modal-header bgm-blue">
            <h4 class="modal-title c-white">Raise Hand <label id="hangups" class="close c-white btn-small" aria-label="Close"><span aria-hidden="true" class="c-white">Hangup</span></label></h4>
        </div>
        <div class="modal-body">
        <br>
          <div class="row">
            <div class="col-md-12" id="kotakr">
              <div class="col-md-6 raise-hand-video-box" id="kotaka">
                <center id="videoBoxLocal" ></center>
              </div>
              <div class="col-md-6 raise-hand-video-box">
                <center id="videoBoxRemote"></center>
              </div>
            </div>
          </div>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" id="hangups" >Hangup</button>
        </div> -->
      </div>
    </div>
  </div>

  <div id="tidakadadata" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header bgm-orange">
            <h4 class="modal-title c-white">INFO <label id="tutupalert" class="close c-white btn-small" aria-label="Close"><span aria-hidden="true" class="c-white f-13">X</span></label></h4>
        </div>
        <div class="modal-body">
        <br>
          <div class="row">
            <center><h4>Tidak ada data</h4></center>
          </div>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" id="hangups" >Hangup</button>
        </div> -->
      </div>
    </div>
  </div>

  <div id="callingModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Calling ... <button type="button" id="hangupss" class="close" aria-label="Close"><span aria-hidden="true">X</span></button></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <b id="messageCalling"></b>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
    
    // $("#papanshow2").css('display','none');
    $("#showpapan").click(function(){
        $("#content").toggleClass('toggle');
    });

    $("#showchat").click(function(){
        $("#papanchat").toggleClass('toggle');
    });

    $("#player").click(function(){
        $("#papanchat").toggleClass('toggle');
    });

    $("#tutupalert").click(function(){
        $("#tidakadadata").modal("hide");
    });

    $("#papanwhiteboard").click(function(){
        $(".papanwhiteboard").css('display','none');
        $(".papanscreen").css('display','inline-block');
    });

    $("#papanscreen").click(function(){
        $(".papanscreen").css('display','none');
        $(".papanwhiteboard").css('display','inline-block');
    });
</script>



