<script type="text/javascript" src="<?php echo base_url(); ?>aset/janus/janus.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>aset/janus/test.js"></script>
<style type="text/css">
    #container {
        width: 100%;       
        position: relative;
        text-align:center;
    }
    #container div {
        position: relative;
        margin:3px;     
        background-color: #bfbfbf;
        border-radius:3px;
        text-align:center;
        display:inline-block;
    }
    #video{
        width: 100%;
    }

    .video2 {
        object-fit: fill;
        width: 100%;
        height: 100%;
    }
    #tempatstudent{
        width: 100%;     
    }
    #tempatstudent.toggle{
        width: 80%;     
    }
    #tempattutor{
        width: 100%;     
    }
    #tempattutor.toggle{
        width: 80%;     
    }

</style>
    
    <div id="tempatstudent" style="margin-top:4%; display: none;">
        <section class="row" id="container2" style="margin-left: 5%; margin-right: 5%; width: 90%; height: 70vh;">
        <!-- <div class="container"> -->
            <div class="col-md-12" id="fulled" style="height: 100%; display: none;">
                
            </div>
            <div class="col-md-8" id="setengah1" style="height: 100%;" id="tour4">
                <div class="move col-md-12" id="whboard" style="height: 100%; background-color: purple; padding: 0; margin: 0;">
                    <img id="pindah1" class="klikbego" src="<?php echo base_url();?>aset/images/Arrow.png" width="30px" height="30px" style="display:none; z-index: 1; position: absolute; margin: 2%; cursor: pointer;" data-toggle="tooltip"
                    data-placement="bottom" title="Switch Screen">
                    <!-- <div class="col-md-12" style="z-index: 1px; position: absolute; padding-right: 3%;"><label class="bgm-blue f-20 pull-right " >Ini Whiteboard</label></div> -->
                    <!-- <div id="wbtutup" style="height: 100%; width: 100%; background-color: white; z-index: 10; position: absolute; opacity: 0.01;"></div> -->
                    <iframe id="wbcard" width="100%" height="100%" style="padding: 0px; z-index: 1;" ></iframe>

                    <!-- <iframe src="<?php echo base_url(); ?>First/aslkfsdklf" width="100%" height="100%" style="object-fit: fill;"></iframe> -->
                </div>
            </div>
            <div class="col-md-6" id="setengah2" style="height: 100%; display: none;  margin-left: -1%;">
            </div>
            <div class="col-md-4" id="seperempat" style="height: 100%;">
                <div class="move col-md-12" id="vdio" style="height: 49%; padding: 0px; margin-bottom: 2.5%;background: #ececec;" >
                    <img id="pindah2" class="klikbego" src="<?php echo base_url();?>aset/images/Arrow.png" width="30px" height="30px" style="display:none; z-index: 1; position: absolute; margin: 2%; cursor: pointer;" data-toggle="tooltip" data-placement="bottom" title="Switch Screen">
                    <video class="video2" id="myvideostudent" disabled width="100%" height="100%" autoplay />
                </div>

                <div class="move col-md-12" id="sshare" style="height: 49%; padding: 0px; background-color: blue; width: 100%;">
                    <img id="pindah3" class="klikbego" src="<?php echo base_url();?>aset/images/Arrow.png" width="30px" height="30px" style="display:none; z-index: 1; position: absolute; margin: 2%; cursor: pointer;" data-toggle="tooltip"
                    data-placement="bottom" title="Switch Screen">
                    <iframe id="screensharestudent" class="screen-share-iframe" scrolling="no" frameborder="0" allowfullscreen style="width: 100%; height: 100%; max-width: 100%;" ></iframe>
                </div>
            </div>
        <!-- </div> -->
        </section> 
        <div id="berkas" class="col-md-12" style="display: none;"></div>        
    </div>

    <br>

    <div id="tempattutor" style="margin-top:4%; display: none;">
        <section class="row" id="container2" style="height: 70vh;">
        <div class="container">

            <div class="col-md-12" id="fulled" style="height: 100%; display: none;">
                
            </div>
            <div class="col-md-6" style="height: 450px;">
                <div class="move col-md-12" id="whboardtutor" style="height: 450px; background-color: #efefef; padding: 0; margin: 0;">
                    <!-- <div class="col-md-12" style="z-index: 1px; position: absolute; padding-right: 3%;"><label class="bgm-blue f-20 pull-right " >Ini Whiteboard</label></div> -->
                    <iframe id="wbcardd" width="100%" height="100%" style="padding: 0px;"></iframe>
                </div>
            </div>
            <div class="col-md-6" style="height: 450px; background-color: #efefef; object-fit: fill; padding: 0; margin: 0;" >
                <!-- <div class="panel-body" id="videolocal"> -->
                <video id="myvideo" class="video2" autoplay ></video> 
            </div>
        </div>          
        </section> 
        <div id="berkas" class="col-md-12" style="display: none;"></div>       
    </div>
    <div id="tempatwaktu" class="text-center" style="position:absolute;
    width:300px;
    height:40px;
    background:rgba(3, 169, 244, 0.6);
    padding-left: 3px;
    padding-right: 3px;
    bottom:5px;
    border-radius: 5px;
    right:25%;
    left:50%;
    margin-left: -150px;
    display: none;">
        <h5 class="c-white">Classroom will be ended in <label id="countdown"></label></h5>
    </div>

    
<script type="text/javascript">
    var view_state = 0;
    // 0 = 3 3 nya ada
    // 1 = 2 setengah2
    // 2 = 1 full
    $('#container2').on('click','.move',function(e){
        if(view_state == 0 && $(this).parent().attr('id') != 'setengah1' && $(this).parent().attr('id') != 'setengah2' && $(this).parent().attr('id') != 'fulled'){
            var that = $(this);
            var old_view = $('#setengah1').find('div');
            var i=1;
            var new_view1;
            var new_view2;
            $('#seperempat div').each(function(e){
                if(i == 1){
                    new_view1 = $(this);    
                }else{
                    new_view2 = $(this);    
                }
                
                i++;
            });
            
            $(that).find('div').css('margin-bottom','');
            $(that).appendTo('#setengah1');
            $(that).css('height','100%');
            $('#seperempat').find('div').css('margin-bottom','2.5%');
            $(old_view).appendTo('#seperempat');
            old_view.show('slow');
            old_view.css('height','49%');
        }else if(view_state > 0){
            return false;
        }
        $('#myvideo').get(0).play();
        $('#myvideostudent').get(0).play();

    });
</script>
<script type="text/javascript">
    
    // $("video").bind("contextmenu",function(){
    //     return false;
    // });

    var urutan = new Array(2);
    var i=0;
    $('#seperempat div').each(function(e){
        urutan[i++] = $(this).attr('id');
    });

    function toggleCheckboxtutor(element)
    {
        if(klikwhiteboardtutor.checked && klikvideotutor.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-6');
            $('#setengah2').css('display','block');
            $('#setengah2').attr('class','col-md-6');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#fulled').css('display','none');
        }  
        
        else if (klikwhiteboardtutor.checked) {
            $('#setengah2').css('display','none');
            $('#setengah2').css('display','col-md-1');
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-12');
            $('#fulled').css('display','block');
            $('#whboardtutor').appendTo('#fulled');
            $('#whboardtutor').css('height','100%');
            $('#whboardtutor').css('width','100%');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#vdiotutor').appendTo('#berkas');
                                                    
        } 
        else if (klikvideotutor.checked) {
            $('#setengah1').css('display','none');
            $('#setengah2').css('display','none');
            $('#fulled').css('display','block');
            $('#vdiotutor').appendTo('#fulled');
            $('#vdiotutor').css('height','100%');
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#whboardtutor').appendTo('#berkas');
        }
        else
        {
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', '#ececec'); 
        }
        $('#myvideo').get(0).play();
        $('#myvideostudent').get(0).play();
    }
    function toggleCheckbox(element)
    {
        if(klikwhiteboard.checked && klikvideo.checked && klikscreen.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-8');
            $('#setengah2').css('display','none');
            $('#setengah2').attr('class','col-md-4');
            $('#seperempat').css('display','block');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#fulled').css('display','none');
            $('#berkas').find('div').appendTo('#seperempat');
            $('#setengah2').find('div').appendTo('#seperempat');
            $('#seperempat').find('div').css('margin-bottom','2.5%');
            $('#seperempat div').each(function(e){
                $(this).css('height','49%');
            });
        }
        else if (klikwhiteboard.checked && klikvideo.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-6');
            $('#setengah2').css('display','block');
            $('#setengah2').attr('class','col-md-6');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', '#ececec');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','none');
            
            var i = 0;
            $('#seperempat div').each(function(e){
                urutan[i++] = $(this).attr('id');
            });            
            $('#sshare').appendTo('#berkas');
            if($('#setengah1').find('div').attr('id') == 'whboard'){
                $('#vdio').appendTo('#setengah2');
                $('#vdio').css('height','100%');

            }else if($('#setengah1').find('div').attr('id') == 'vdio'){
                $('#whboard').appendTo('#setengah2');
                $('#whboard').css('height','100%');

            }else{
                $('#vdio').appendTo('#setengah1');
                $('#whboard').appendTo('#setengah2');
                $('#vdio').css('height','100%');
                $('#whboard').css('height','100%');
            }

        }  
        else if (klikwhiteboard.checked && klikscreen.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-6');
            $('#setengah2').css('display','block');
            $('#setengah2').attr('class','col-md-6');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','none');
            $('#vdio').appendTo('#berkas');
            if($('#setengah1').find('div').attr('id') == 'whboard'){
                $('#sshare').appendTo('#setengah2');
                $('#sshare').css('height','100%');

            }else if($('#setengah1').find('div').attr('id') == 'sshare'){
                $('#whboard').appendTo('#setengah2');
                $('#whboard').css('height','100%');

            }else{
                $('#sshare').appendTo('#setengah1');
                $('#whboard').appendTo('#setengah2');
                $('#sshare').css('height','100%');
                $('#whboard').css('height','100%');
            }                          
        }
        else if (klikvideo.checked && klikscreen.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-6');
            $('#setengah2').css('display','block');
            $('#setengah2').attr('class','col-md-6');
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','none');
            $('#whboard').appendTo('#berkas');
            if($('#setengah1').find('div').attr('id') == 'sshare'){
                $('#vdio').appendTo('#setengah2');
                $('#vdio').css('height','100%');

            }else if($('#setengah1').find('div').attr('id') == 'vdio'){
                $('#sshare').appendTo('#setengah2');
                $('#sshare').css('height','100%');

            }else{
                $('#vdio').appendTo('#setengah1');
                $('#sshare').appendTo('#setengah2');
                $('#vdio').css('height','100%');
                $('#sshare').css('height','100%');
            }                 
        } 
        else if (klikwhiteboard.checked) {
            $('#setengah1').css('display','none');
            $('#setengah2').css('display','none');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','block');
            $('#whboard').appendTo('#fulled');
            $('#whboard').css('height','100%');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', '#ececec');
            $('#vdio').appendTo('#berkas');
            $('#sshare').appendTo('#berkas');
                                                    
        } 
        else if (klikvideo.checked) {
            $('#setengah1').css('display','none');
            $('#setengah2').css('display','none');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','block');
            $('#vdio').appendTo('#fulled');
            $('#vdio').css('height','100%');
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', '#ececec');
            $('#whboard').appendTo('#berkas');
            $('#sshare').appendTo('#berkas');
        }
        else if (klikscreen.checked) {
            $('#setengah1').css('display','none');
            $('#setengah2').css('display','none');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','block');
            $('#sshare').appendTo('#fulled');
            $('#sshare').css('height','100%');
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#whboard').appendTo('#berkas');
            $('#vdio').appendTo('#berkas');
        }
        else
        {
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', '#ececec'); 
        }
        $('#video2').get(0).play();
    }

</script>