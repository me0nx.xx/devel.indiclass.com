<style type="text/css">
	#container {
	    width: 100%;	   
	    position: relative;
	    text-align:center;
	}
	#container div {
	    position: relative;
	    margin:3px;	    
	    background-color: #bfbfbf;
	    border-radius:3px;
	    text-align:center;
	    display:inline-block;
	}

	#whiteboard{    
        width: 50%;
        background-color: blue;
        height: 70vh;
        position: static;
        float: left;
        color: white;
        font-size: 30px;

        -webkit-animation-duration: 10s;
        animation-duration: 10s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
    } 
    #video{
        width: 30%;
        background-color: teal;
        height: 35vh;
        float: left;
        color: white;
        font-size: 30px;

        -webkit-animation-duration: 10s;
        animation-duration: 10s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
    } 
    #screen{
        width: 30%;
        background-color: purple;
        height: 35vh;
        float: left;
        color: white;
        font-size: 30px;

        -webkit-animation-duration: 10s;
        animation-duration: 10s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
    }
    .video2.gede {
        object-fit: fill;
        width: 100%;
        height: 70.8vh;
        margin:0px;
        padding: 0px;
        cursor: default;
    }
    #one.gedee{
        cursor: pointer;
    }
    #one{
        cursor: default;
    }
    #three.gedee{
        cursor: default;
    }
    #three{
        cursor: pointer;
    }
    .video2 {
        object-fit: fill;
        width: 100%;
        height: 35vh;
        margin: 0px;
        padding: 0px;
        cursor: pointer;
    }
    .menu{
    	margin: 5%;
    }
</style>

    <div id="container">
        <div id="one" class="move" style="height:70.7vh; width:60%; float:left; background-color: purple;"><div id="whiteboard2">INI WHITEBOARD</div></div>
        <div id="two" class="move" style="height:35vh; width:36%; float:left;"><video id="video2" class="video2" src="<?php echo base_url(); ?>aset/video/bigbunny.mp4" autoplay muted loop></video></div>
        <div id="three" class="move" style="height:35vh; width:36%; float:left; background-color: teal;">INI SCREEN SHARE</div>
    </div>
    
<script type="text/javascript">
	
    $("video").bind("contextmenu",function(){
        return false;
        });
	var animating = false;

	$('#container').on('click', '.move', function () {
	    
	    var clickedDiv = $(this).closest('div'),
	        prevDiv = $("#container > :first-child"),
	        distance = clickedDiv.offset().right - prevDiv.offset().right;
	    
	    if (!clickedDiv.is(":first-child")) {
            prevDiv.css('left', '0px');
            prevDiv.css('width', '36%');
            prevDiv.css('height', '35vh');
            clickedDiv.css('left', '0px');
            clickedDiv.css('width', '60%');
            clickedDiv.css('height', '70.7vh');
            prevDiv.insertBefore(clickedDiv);
            clickedDiv.prependTo("#container");
            $('#video2').get(0).play();  	            
            var contentPanelId = clickedDiv.attr("id");        
			var contentPanelprev = prevDiv.attr("id");            
			if (contentPanelId == "two" || contentPanelprev == "two") {
				$("#video2").toggleClass("gede");            
			}
            if (contentPanelId == "three" || contentPanelprev == "three") {
                $("#three").toggleClass("gedee");                
            }
            if (contentPanelId == "one" || contentPanelprev == "one") {
                $("#one").toggleClass("gedee");                
            }
	    }
	});

</script>