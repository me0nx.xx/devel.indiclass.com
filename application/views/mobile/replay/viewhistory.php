<style type="text/css">
    .bp-header:hover
    {
        cursor: pointer;
    }
    .thumb {
        padding: 10px;
    }
/*  #video2 {
        object-fit: fill;
        width: 100%;
        height: 100%;
        padding: 0px;
    }*/

    .video2 {
        object-fit: fill;
        width: 100%;
        height: 100%;
    }

</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>aset/videojs/video-js.min.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/videojs/video.min.js"></script>
<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main" data-layout="layout-1" >
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content" >        
        <div class="container">            

            <div class="card">
                <div class="row">
                    <?php
                    if($class == null){
                        echo '
                        <div class="alert alert-success">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          video belum siap/video gak ada
                      </div>
                        ';
                    }else{
                    ?>
                 <div class="card-header">                            
                    <div class="pull-left"><h2 class="m-l-15"><?php echo $class['name']." - ".$class['description']; ?><small><?php echo $class['start_time']; ?></small></h2></div>
                    <div class="pull-right m-r-20"><a href="<?php echo base_url(); ?>ClassHistory"><button class="btn btn-warning">Kembali</button></a></div>
                </div>
               </div>
               <hr>

               <div class="card-body card-padding">
                  
                    <!-- PERTAMA -->
                    <div class="row">

                       <!--  <div id="container">
                            <div id="one" class="" style="height:70vh; width:57%; float:left; margin-left: 2%;">
                                <img src="<?php echo base_url(); ?>aset/images/pindah.png" width="40px" height="40px" style="z-index: 1; position: absolute; margin: 0.5%; cursor: pointer;" data-toggle="tooltip" data-placement="bottom" title="Switch Screen" class="move">
                                <video  id="video_whboard" class="video-js video2" style=" object-fit: fill;" controls autoplay data-setup="{}"  src="<?php echo base_url(); ?>aset/video/line.mp4">    
                                </video>
                            </div>
                            <div id="two" class="" style="height:35vh; width:36%; margin-left: 2%; float:left;">
                                <img src="<?php echo base_url(); ?>aset/images/pindah.png" width="40px" height="40px" style="z-index: 1; position: absolute; margin: 0.5%; cursor: pointer;" data-toggle="tooltip" data-placement="bottom" title="Switch Screen" class="move">

                                <video id="video_vdio" class="video-js video2" style=" object-fit: fill;" controls autoplay data-setup="{}" src="<?php echo base_url('aset/home_videos/'.$class['class_id'].'/'.$class['class_id'].'.mp4'); ?>" poster="<?php echo base_url() ?>aset/img/novideoavailable.jpg">    
                               </video>
                            </div>                            
                            <div id="three" class="" style="height:35vh; width:36%; margin-left: 2%; float:left;">
                                <img src="<?php echo base_url(); ?>aset/images/pindah.png" width="40px" height="40px" style="z-index: 1; position: absolute; margin: 0.5%; cursor: pointer;" data-toggle="tooltip" data-placement="bottom" title="Switch Screen" class="move">

                                <video id="video_sshare" class="video-js video2" style=" object-fit: fill;" controls autoplay data-setup="{}" src="<?php echo base_url(); ?>aset/video/bigbunny.mp4">    
                               </video>
                            </div>
                        </div> -->    

                        <div id="container">
                            <div id="one" class="" style="height:100vh; width:95%; float:left; margin-left: 2%;">
                                <!-- <img src="<?php echo base_url(); ?>aset/images/pindah.png" width="40px" height="40px" style="z-index: 1; position: absolute; margin: 0.5%; cursor: pointer;" data-toggle="tooltip" data-placement="bottom" title="Switch Screen" class="move"> -->
                                <video  id="video_whboard" poster="<?php echo base_url() ?>aset/img/novideoavailable.jpg" class="video-js video2" style=" object-fit: fill;" data-video-aspect-ratio="16:9" controls autoplay data-setup="{}"  src="http://sto1.classmiles.com/video/<?php echo $_GET['c'];?>/<?php echo $_GET['c'];?>.mp4">    
                                </video>
                            </div>                           
                        </div>                                                
                    </div>

                 </div>
                 
             </div>
             <?php
                }
            ?>
         </div>
</div>

</div><!-- akhir container -->
</section>
</section>


<script type="text/javascript">                            
    $("video").bind("contextmenu",function(){
        return false;
        });
    var animating = false;

    $('#container').on('click', '.move', function () {
        
        var clickedDiv = $(this).closest('div'),
            prevDiv = $("#container > :first-child"),
            distance = clickedDiv.offset().right - prevDiv.offset().right;
        
        if (!clickedDiv.is(":first-child")) {
            prevDiv.css('left', '0px');
            prevDiv.css('width', '36%');
            prevDiv.css('margin-left', '2%');            
            prevDiv.css('height', '35vh');
            clickedDiv.css('left', '0px');
            clickedDiv.css('width', '57%');
            clickedDiv.css('margin-left', '2%');
            clickedDiv.css('height', '70vh');
            prevDiv.insertBefore(clickedDiv);
            clickedDiv.prependTo("#container");
            // $('#video_vdio').get(0).play();
            // $('#video_whboard').get(0).play();
            // $('#video_sshare').get(0).play();
            $("#video_vdio video").get(0).play();
            $("#video_whboard video").get(0).play();
            $("#video_sshare video").get(0).play();
            var contentPanelId = clickedDiv.attr("id");        
            var contentPanelprev = prevDiv.attr("id");                
            // if (contentPanelId == "two" || contentPanelprev == "two") {
            //     $("#video2").toggleClass("gede");            
            // }
            // if (contentPanelId == "three" || contentPanelprev == "three") {
            //     $("#three").toggleClass("gedee");                
            // }
            // if (contentPanelId == "one" || contentPanelprev == "one") {
            //     $("#one").toggleClass("gedee");                
            // }
        }
        
    });

</script>
<script type="text/javascript">  

    var urutan = new Array(2);
    var i=0;
    $('#seperempat div').each(function(e){
        urutan[i++] = $(this).attr('id');
    });

    function toggleCheckbox(element)
    {
        if(klikwhiteboard.checked && klikvideo.checked && klikscreen.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-8');
            $('#setengah2').css('display','none');
            $('#setengah2').attr('class','col-md-4');
            $('#seperempat').css('display','block');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#fulled').css('display','none');
            $('#berkas').find('div').appendTo('#seperempat');
            $('#setengah2').find('div').appendTo('#seperempat');
            $('#seperempat').find('div').css('margin-bottom','2.5%');
            $('#seperempat div').each(function(e){
                $(this).css('height','49%');
            });
        }
        else if (klikwhiteboard.checked && klikvideo.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-6');
            $('#setengah2').css('display','block');
            $('#setengah2').attr('class','col-md-6');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', '#ececec');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','none');
            
            var i = 0;
            $('#seperempat div').each(function(e){
                urutan[i++] = $(this).attr('id');
            });            
            $('#sshare').appendTo('#berkas');
            if($('#setengah1').find('div').attr('id') == 'whboard'){
                $('#vdio').appendTo('#setengah2');
                $('#vdio').css('height','100%');

            }else if($('#setengah1').find('div').attr('id') == 'vdio'){
                $('#whboard').appendTo('#setengah2');
                $('#whboard').css('height','100%');

            }else{
                $('#vdio').appendTo('#setengah1');
                $('#whboard').appendTo('#setengah2');
                $('#vdio').css('height','100%');
                $('#whboard').css('height','100%');
            }

        }  
        else if (klikwhiteboard.checked && klikscreen.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-6');
            $('#setengah2').css('display','block');
            $('#setengah2').attr('class','col-md-6');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','none');
            $('#vdio').appendTo('#berkas');
            if($('#setengah1').find('div').attr('id') == 'whboard'){
                $('#sshare').appendTo('#setengah2');
                $('#sshare').css('height','100%');

            }else if($('#setengah1').find('div').attr('id') == 'sshare'){
                $('#whboard').appendTo('#setengah2');
                $('#whboard').css('height','100%');

            }else{
                $('#sshare').appendTo('#setengah1');
                $('#whboard').appendTo('#setengah2');
                $('#sshare').css('height','100%');
                $('#whboard').css('height','100%');
            }                          
        }
        else if (klikvideo.checked && klikscreen.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-6');
            $('#setengah2').css('display','block');
            $('#setengah2').attr('class','col-md-6');
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','none');
            $('#whboard').appendTo('#berkas');
            if($('#setengah1').find('div').attr('id') == 'sshare'){
                $('#vdio').appendTo('#setengah2');
                $('#vdio').css('height','100%');

            }else if($('#setengah1').find('div').attr('id') == 'vdio'){
                $('#sshare').appendTo('#setengah2');
                $('#sshare').css('height','100%');

            }else{
                $('#vdio').appendTo('#setengah1');
                $('#sshare').appendTo('#setengah2');
                $('#vdio').css('height','100%');
                $('#sshare').css('height','100%');
            }                 
        } 
        else if (klikwhiteboard.checked) {
            $('#setengah1').css('display','none');
            $('#setengah2').css('display','none');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','block');
            $('#whboard').appendTo('#fulled');
            $('#whboard').css('height','100%');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', '#ececec');
            $('#vdio').appendTo('#berkas');
            $('#sshare').appendTo('#berkas');

        } 
        else if (klikvideo.checked) {
            $('#setengah1').css('display','none');
            $('#setengah2').css('display','none');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','block');
            $('#vdio').appendTo('#fulled');
            $('#vdio').css('height','100%');
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', '#ececec');
            $('#whboard').appendTo('#berkas');
            $('#sshare').appendTo('#berkas');
        }
        else if (klikscreen.checked) {
            $('#setengah1').css('display','none');
            $('#setengah2').css('display','none');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','block');
            $('#sshare').appendTo('#fulled');
            $('#sshare').css('height','100%');
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#whboard').appendTo('#berkas');
            $('#vdio').appendTo('#berkas');
        }
        else
        {
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', '#ececec'); 
        }
        $('#video_vdio').get(0).play();
        $('#video_whboard').get(0).play();
        $('#video_sshare').get(0).play();
    }

</script>