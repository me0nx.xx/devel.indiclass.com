<style type="text/css">
	.bp-header:hover
	{
		cursor: pointer;
	}
	.thumb img {
		opacity: 1;		
		-webkit-transition: .3s ease-in-out;
		transition: .3s ease-in-out;
	}
	.thumb:hover img {
		opacity: 0.6;
	}
</style>
<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main" data-layout="layout-1" style="background-color: #f1f1f1;">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content" >        
        <div class="container">

            <div class="block-header">
                <h2 style="margin-left: -4px;"><?php echo $this->lang->line('replay');?></h2>

            </div><!-- akhir block header -->

            <div class="card">
            <div class="row">
            	<div class="card-header m-l-15">
            		<!-- <h2><?php echo $this->lang->line('replaytitle');?></h2> -->
            		<hr>
            	</div>
            	<div class="col-md-12" id="boxreplay">
		           
		        </div>
		    </div>
            </div>

		</div><!-- akhir container -->
	</section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function(){
    	
    	var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
    	var iduser 	 = "<?php echo $this->session->userdata('id_user');?>";

        var dateOld = new Date();
        dateOld.setDate(dateOld.getDate() - 7);            
        // var dateMsg = date.getDate()+'-'+ (date.getMonth()+1) +'-'+date.getFullYear();            
        var dateMsgOld = dateOld.getFullYear()+'-'+(dateOld.getMonth()+1)+'-'+dateOld.getDate();
        dateMsgOld = moment(dateMsgOld).format('YYYY-MM-DD'); 

    	$.ajax({
            url: '<?php echo base_url(); ?>Rest/my_ended_class/access_token/'+tokenjwt,
            type: 'POST',
            data: {
                id_user: iduser,
                startdate: dateMsgOld
            },
            success: function(response)
            {   
            	var a = JSON.stringify(response);
                var jsonPretty = JSON.stringify(JSON.parse(a),null,2); 
               	
               	if (response.data.length != null) {
                    for (var i = response.data.length-1; i >=0;i--) {
                        
                        var class_id    = response['data'][i]['class_id'];
                        var subject_id  = response['data'][i]['subject_id'];
                        var name        = response['data'][i]['name'];
                        var description = response['data'][i]['description'];
                        var tutor_id    = response['data'][i]['tutor_id'];
                        var first_name  = response['data'][i]['first_name'];
                        var user_image  = response['data'][i]['user_image'];
                        var start_time  = response['data'][i]['start_time'];
                        var finish_time = response['data'][i]['finish_time'];                        
                        var a           = response['data'][i]['participant'];

                        if (a != null) {
                        	var participant_listt = response['data'][i]['participant']['participant'];
                            // alert(participant_listt);
                            var im_exist = false;
                        	if(participant_listt != null){
                        		
                        		for (var iai = 0; iai < participant_listt.length ;iai++) {
                            		// var a = moment(tgl).format('YYYY-MM-DD');
                        			if (iduser == participant_listt[iai]['id_user']) {
                            			im_exist = true;
                        			}                        		
                            	}
                        	}

                            var kotak1 = "<div class='col-md-3' style='padding:20px; margin-top:-3%;'><div class='card blog-post bgm-white'><div class='row'><div class='bp-header'><a href='?c="+class_id+"'><div class='thumb'><div><img class='play' src='https://classmiles.com/aset/img/playvideowhite.png' style='height: 25%; width: 25%; position: absolute; z-index: 1; margin-left:auto;margin-right:auto; margin-top:auto; margin-bottom:auto; left:-5px; right:0; top:-80px; bottom:0;'></div><img src='https://classmiles.com/aset/img/user/"+tutor_id+".jpg' style='height: 20vh; width: 100%; background-repeat: no-repeat; background-position:center;'></div><div class='bp-title bgm-white c-black' style='height:15vh;'><h2 style='color:#167ac6;'>"+name+" - <small style='margin-bottom:5px; color:#167ac6; font-size:16px;''> "+description+" </small></h2><small style='color: #a3a6ac; word-wrap:break-word; font-size:13px;'> "+first_name+" </small><br><small style='color: #a3a6ac;'> "+start_time+"</small><br></div></a></div></div></div></div>";
                            
                        	if(im_exist){
                        		$("#boxreplay").append(kotak1);
                        	}                            
                        }                        
                    }
                }
                else
                {
                    var kotakg = "<div class='p-20'><div class='alert alert-info text-center'>Maaf saat ini belum ada History class</div></div>";
                    $("#boxreplay").append(kotakg);
                }

            }
        });

    });
</script>
