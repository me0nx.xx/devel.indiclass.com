<style type="text/css">
	.bp-header:hover
	{
		cursor: pointer;
	}
	.thumb {
		padding: 10px;
	}
/*	#video2 {
        object-fit: fill;
        width: 100%;
        height: 100%;
        padding: 0px;
    }*/

    .video2 {
        object-fit: fill;
        width: 100%;
        height: 100%;
    }

</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>aset/videojs/video-js.min.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>aset/videojs/video.min.js"></script>
<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main" data-layout="layout-1" >
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content" >        
        <div class="container">            

            <div class="card">
                <div class="row">
                    <?php
                    if($class == null){
                        echo '
                        <div class="alert alert-success">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          video belum siap/video gak ada
                      </div>
                        ';
                    }else{
                    ?>
                 <div class="card-header">
                    
                    <h2 class="m-l-15"><?php echo $class['name']." - ".$class['description']; ?><small><?php echo $class['start_time']; ?></small></h2>
                    <ul class="actions" style="margin-right: 2%;">
                        <li class="dropdown action-show">
                            <a href="" data-toggle="dropdown">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>

                            <div class="dropdown-menu pull-right" data-toggle="tooltip" data-placement="bottom" title="Setting">
                                <p class="p-5">
                                	<div id="checkwhiteboard" style="border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
                                       <input type="checkbox" name="klikwhiteboard" id="klikwhiteboard" value="klikwhiteboard" checked onchange="toggleCheckbox(this)"  style="display: none;">
                                       <label for="klikwhiteboard" id="nsfwbtn" class="btn btn-info m-l-15 f-12" rel="tooltip" style="width: 90%; height: 30px;">Whiteboard.</label>
                                   </div>
                                   <div id="checkvideo" style="margin-top: -6%; border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
                                       <input type="checkbox" name="klikvideo" id="klikvideo" value="klikvideo" checked onchange="toggleCheckbox(this)" style="display: none; margin-top: -3%;">
                                       <label for="klikvideo" id="nsfwbtn" class="btn btn-info m-l-15" rel="tooltip" style="width: 90%; height: 30px;">Video.</label>
                                   </div>
                                   <div id="checkscreen" style="margin-top: -6%; border-left-color: teal; border-right-color: #ffffff; border-bottom-color: #ffffff; border-top-color: #ffffff; border-style: solid; border-width: medium; height: 5vh;" class="m-b-25">
                                       <input type="checkbox" name="klikscreen" id="klikscreen" value="klikscreen" checked onchange="toggleCheckbox(this)" style="display: none;margin-top: -3%; ">                                        
                                       <label for="klikscreen" id="nsfwbtn" class="btn btn-info m-l-15" rel="tooltip" style="width: 90%; height: 30px;">Screen Share.</label>
                                   </div>                                    
                                   <hr>                                
                                   <a href="<?php echo base_url(); ?>ClassHistory"><button class="btn btn-warning btn-block">Kembali</button></a>
                               </p>
                           </div>
                       </li>
                   </ul>

                   <hr>
               </div>            

               <div class="card-body card-padding">
                    
                  <div class="row" id="container2" style="height: 70vh; padding: 2%;">

                   <div class="col-md-12" id="fulled" style="height: 100%; display: none;">

                   </div>
                   <div class="col-md-8" id="setengah1" style="height: 100%; margin-left: -1%;">
                    <div class="move col-md-12" id="whboard" style="height: 100%; background-color: purple; padding: 0px;">

                        <!-- <video id="video_whboard" controls class="video2" src="<?php echo base_url(); ?>aset/video/line.mp4" autoplay loop poster="<?php echo base_url() ?>aset/img/novideoavailable.jpg">
                        </video> -->
                        <video  id="video_whboard" class="video-js vjs-default-skin video2" controls autoplay data-setup="{}"  src="<?php echo base_url(); ?>aset/video/line.mp4">    
                        </video>
                        </div>
                    </div>
                    <div class="col-md-6" id="setengah2" style="height: 100%; display: none;  margin-left: -1%; padding: 0px;">
                    </div>
                    <div class="col-md-4" id="seperempat" style="height: 100%;  margin-left: -1%;">
                        <div class="move col-md-12" id="vdio" style="height: 49%; padding: 0px; margin-bottom: 2.5%; background-color: #ececec;">
                             <!-- <video id="video_vdio" controls class="video2" src="<?php echo base_url('aset/home_videos/'.$class['class_id'].'/'.$class['class_id'].'.mp4'); ?>" autoplay loop poster="<?php echo base_url() ?>aset/img/novideoavailable.jpg">
                             </video> -->
                             <!-- <video  id="video_vdio" class="video-js vjs-default-skin video2" controls autoplay data-setup="{}" src="https://beta.dev.meetaza.com/classmiles/aset/home_videos/787601/787601.mp4" preload="none">  -->
                             <video  id="video_vdio" class="video-js vjs-default-skin video2" controls autoplay data-setup="{}" src="<?php echo base_url('aset/home_videos/'.$class['class_id'].'/'.$class['class_id'].'.mp4'); ?>" poster="<?php echo base_url() ?>aset/img/novideoavailable.jpg">    
                            </video>
                         </div>

                     <div class="move col-md-12" id="sshare" style="height: 49%; background-color: blue; padding: 0px;">
                         <!-- <h2>SCREEN SHARE</h2> -->
                         <!-- <video id="video_sshare" controls class="video2" src="<?php echo base_url(); ?>aset/video/bigbunny.mp4" poster="<?php echo base_url() ?>aset/img/novideoavailable.jpg" autoplay loop>
                         </video> -->
                         <video id="video_sshare" class="video-js vjs-default-skin video2" controls autoplay data-setup="{}" src="<?php echo base_url(); ?>aset/video/bigbunny.mp4">    
                        </video>
                     </div>
                 </div>

                 </div>
                 <div id="berkas" class="col-md-12" style="display: none;">
                 </div>
                 
             </div>
             <?php
                }
            ?>
         </div>
</div>

</div><!-- akhir container -->
</section>
</section>


<script type="text/javascript">

	$("#klikvideo1").click(function(){
		document.getElementById("source").src = "<?php echo base_url(); ?>aset/video/line.mp4";
        document.getElementById("videoPlayer").load();
    });
	$("#klikvideo2").click(function(){
		document.getElementById("source").src = "<?php echo base_url(); ?>aset/video/juki.mp4";
        document.getElementById("videoPlayer").load();
    });

    var view_state = 0;
    // 0 = 3 3 nya ada
    // 1 = 2 setengah2
    // 2 = 1 full
    // $('#container2').on('click','.move',function(e){
    //     if(view_state == 0 && $(this).parent().attr('id') != 'setengah1' && $(this).parent().attr('id') != 'setengah2' && $(this).parent().attr('id') != 'fulled'){
    //         var that = $(this);
    //         var old_view = $('#setengah1').find('div');
    //         var i=1;
    //         var new_view1;
    //         var new_view2;
    //         $('#seperempat div').each(function(e){
    //             if(i == 1){
    //                 new_view1 = $(this);    
    //             }else{
    //                 new_view2 = $(this);    
    //             }
                
    //             i++;
    //         });
            
    //         $(that).find('div').css('margin-bottom','');
    //         $(that).appendTo('#setengah1');
    //         $(that).css('height','100%');
    //         $('#seperempat').find('div').css('margin-bottom','2.5%');
    //         $(old_view).appendTo('#seperempat');
    //         old_view.show('slow');
    //         old_view.css('height','49%');
    //     }else if(view_state > 0){
    //         return false;
    //     }
    //     $('#video_vdio').get(0).play();
    //     $('#video_whboard').get(0).play();
    //     $('#video_sshare').get(0).play();

    // });
</script>
<script type="text/javascript">  

    var urutan = new Array(2);
    var i=0;
    $('#seperempat div').each(function(e){
        urutan[i++] = $(this).attr('id');
    });

    function toggleCheckbox(element)
    {
        if(klikwhiteboard.checked && klikvideo.checked && klikscreen.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-8');
            $('#setengah2').css('display','none');
            $('#setengah2').attr('class','col-md-4');
            $('#seperempat').css('display','block');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#fulled').css('display','none');
            $('#berkas').find('div').appendTo('#seperempat');
            $('#setengah2').find('div').appendTo('#seperempat');
            $('#seperempat').find('div').css('margin-bottom','2.5%');
            $('#seperempat div').each(function(e){
                $(this).css('height','49%');
            });
        }
        else if (klikwhiteboard.checked && klikvideo.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-6');
            $('#setengah2').css('display','block');
            $('#setengah2').attr('class','col-md-6');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', '#ececec');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','none');
            
            var i = 0;
            $('#seperempat div').each(function(e){
                urutan[i++] = $(this).attr('id');
            });            
            $('#sshare').appendTo('#berkas');
            if($('#setengah1').find('div').attr('id') == 'whboard'){
                $('#vdio').appendTo('#setengah2');
                $('#vdio').css('height','100%');

            }else if($('#setengah1').find('div').attr('id') == 'vdio'){
                $('#whboard').appendTo('#setengah2');
                $('#whboard').css('height','100%');

            }else{
                $('#vdio').appendTo('#setengah1');
                $('#whboard').appendTo('#setengah2');
                $('#vdio').css('height','100%');
                $('#whboard').css('height','100%');
            }

        }  
        else if (klikwhiteboard.checked && klikscreen.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-6');
            $('#setengah2').css('display','block');
            $('#setengah2').attr('class','col-md-6');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','none');
            $('#vdio').appendTo('#berkas');
            if($('#setengah1').find('div').attr('id') == 'whboard'){
                $('#sshare').appendTo('#setengah2');
                $('#sshare').css('height','100%');

            }else if($('#setengah1').find('div').attr('id') == 'sshare'){
                $('#whboard').appendTo('#setengah2');
                $('#whboard').css('height','100%');

            }else{
                $('#sshare').appendTo('#setengah1');
                $('#whboard').appendTo('#setengah2');
                $('#sshare').css('height','100%');
                $('#whboard').css('height','100%');
            }                          
        }
        else if (klikvideo.checked && klikscreen.checked) {
            $('#setengah1').css('display','block');
            $('#setengah1').attr('class','col-md-6');
            $('#setengah2').css('display','block');
            $('#setengah2').attr('class','col-md-6');
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','none');
            $('#whboard').appendTo('#berkas');
            if($('#setengah1').find('div').attr('id') == 'sshare'){
                $('#vdio').appendTo('#setengah2');
                $('#vdio').css('height','100%');

            }else if($('#setengah1').find('div').attr('id') == 'vdio'){
                $('#sshare').appendTo('#setengah2');
                $('#sshare').css('height','100%');

            }else{
                $('#vdio').appendTo('#setengah1');
                $('#sshare').appendTo('#setengah2');
                $('#vdio').css('height','100%');
                $('#sshare').css('height','100%');
            }                 
        } 
        else if (klikwhiteboard.checked) {
            $('#setengah1').css('display','none');
            $('#setengah2').css('display','none');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','block');
            $('#whboard').appendTo('#fulled');
            $('#whboard').css('height','100%');
            $('#checkwhiteboard').css('border-left-color', 'teal');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', '#ececec');
            $('#vdio').appendTo('#berkas');
            $('#sshare').appendTo('#berkas');

        } 
        else if (klikvideo.checked) {
            $('#setengah1').css('display','none');
            $('#setengah2').css('display','none');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','block');
            $('#vdio').appendTo('#fulled');
            $('#vdio').css('height','100%');
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', 'teal');
            $('#checkscreen').css('border-left-color', '#ececec');
            $('#whboard').appendTo('#berkas');
            $('#sshare').appendTo('#berkas');
        }
        else if (klikscreen.checked) {
            $('#setengah1').css('display','none');
            $('#setengah2').css('display','none');
            $('#seperempat').css('display','none');
            $('#fulled').css('display','block');
            $('#sshare').appendTo('#fulled');
            $('#sshare').css('height','100%');
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', 'teal');
            $('#whboard').appendTo('#berkas');
            $('#vdio').appendTo('#berkas');
        }
        else
        {
            $('#checkwhiteboard').css('border-left-color', '#ececec');
            $('#checkvideo').css('border-left-color', '#ececec');
            $('#checkscreen').css('border-left-color', '#ececec'); 
        }
        $('#video_vdio').get(0).play();
        $('#video_whboard').get(0).play();
        $('#video_sshare').get(0).play();
    }

</script>