<header id="header" class="clearfix" style="background-color: #Cf000f;">
    <div class="col-xs-1"  style="margin-top: 7%" >
        <a href="<?php echo base_url(); ?>Tutor/About" id="back_home" type="button" class="loading c-white f-19" style="cursor: pointer;">
        <i class="zmdi zmdi-arrow-left"></i>
        </a>
    </div>
    <div class=" c-white f-17 col-xs-10" style="margin-top: 7%">
        <center><?php echo $this->lang->line('selectsubjecttutor'); ?></center>
    </div>

</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<style type="text/css">
	body{
		background-color: white;
	}
	 hr.sub_garis {
            /*margin-top: -2%;*/
            margin-left: -11%;
            color: black;
            background-color: grey;
            height: 1px;
            width: 122%; 
        }
</style>
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('./inc/sidetutor'); ?>
	</aside>

	<section id="content">
		<div class="">  
			<br>
			<section id="modal_harga">
			</section>  
			<?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>
			<!-- disini untuk dashboard murid -->
			<section class=""  id="list_subject">
			</section>               
			<!-- <div class="card" style="margin-top: 2%;">
				<div class="card-body card-padding">
					<div class="row">
						<div class="table-responsive">
							<table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
								<thead>
									<?php
									$select_all = $this->db->query("SELECT ms.subject_id, ms.jenjang_id, ms.subject_name, ms.indonesia, ms.english, mj. * FROM master_jenjang AS mj INNER JOIN master_subject AS ms ON ms.jenjang_id = ms.jenjang_id WHERE mj.jenjang_id = ms.jenjang_id order by ms.subject_id asc")->result_array();
									?>
									<tr>
										<th data-column-id="name"><?php echo $this->lang->line('subject'); ?></th>
										<th data-column-id="jenjang"><?php echo $this->lang->line('school_jenjang'); ?></th>
										<th data-column-id="kelas"><?php echo $this->lang->line('selectgrade'); ?></th>
										<th data-column-id="aksi"><?php echo $this->lang->line('aksi');?></th>

									</tr>
								</thead>
								<tbody>  
									<?php
									$no=1;
									foreach ($select_all as $row => $v) {
										$booked = $this->db->query("SELECT * FROM tbl_booking WHERE subject_id='".$v['subject_id']."' AND id_user='".$this->session->userdata('id_user')."' ")->row_array();
										?>       
										<tr>
											<td>
												<?php 
													// echo $this->lang->line('subject_'.$v['subject_id']);
													echo $v['subject_name'];
												?>
											</td>
											<td><?php echo($v['jenjang_name']); ?></td>
											<td><?php echo($v['jenjang_level']); ?></td>
											<td>
												<?php 
												if(!empty($booked)){
													echo '<a class="waves-effect bgm-red c-white btn btn-danger btn-block unchoose_ajax" id="'.$v['subject_id'].'" href="'.base_url('/master/unchoosesub?id_user='.$this->session->userdata('id_user').'&subject_id='.$v['subject_id']).'"><i class="zmdi zmdi-face-add"></i>'.$this->lang->line('tidakikutisubject').'</a>';
												}else{
													echo '<a class="waves-effect bgm-green c-white btn btn-success btn-block choose_ajax" id="'.$v['subject_id'].'" href="'.base_url('/master/choosesub?id_user='.$this->session->userdata('id_user').'&subject_id='.$v['subject_id']).'"><i class="zmdi zmdi-face-add"></i>'.$this->lang->line('ikutisubject').'</a>';
												}
												?>
											</td>
										</tr>
										<?php 
										$no++;
									}
									?>		
								</tbody>   
							</table>   
						</div>
					</div>
				</div>
			</div> -->
		</div>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">
	var access_token_jwt = "<?php echo $this->session->userdata('access_token_jwt')?>";
	var iduser 	= "<?php echo $this->session->userdata('id_user');?>";
	$.ajax({
		url: '<?php echo base_url();?>Rest/subjecttutor/access_token/'+access_token_jwt,
		type: 'POST',
		data: {
			id_user: iduser
			
		},
		success: function(response)
		{ 
			// var dataa = JSON.parse(response);
			console.warn("test class "+response['status']);
			for (var i = 0; i< response.data.length;i++) {
                var idsubject = response['data'][i]['subject_id'];
                var idjenjang = response['data'][i]['jenjang_id'];
                var icon = response['data'][i]['icon'];
                var booked = response['data'][i]['booked'];
                var indonesia = response['data'][i]['indonesia'];
                var inggris = response['data'][i]['english'];
                var namesubject = response['data'][i]['subject_name'];
                var booked = response['data'][i]['booked'];
                var namejenjang = response['data'][i]['jenjang_name'];
                var leveljenjang = response['data'][i]['jenjang_level'];
                var kodesubject = idsubject+idjenjang;

                
             	if(booked == 0){
                	var tombol_ikuti ="<a class='waves-effect  bgm-green c-white btn btn-success btn-block' style='margin-top: 10%;' href='#harga_modal_"+kodesubject+"' data-toggle='modal' id='btn_"+kodesubject+"'><?php echo $this->lang->line('tambahdata'); ?></a></div>";
               
                }
                else
                {	
                	var tombol_ikuti ="<a class='waves-effect bgm-warning c-white btn btn-warning btn-block' style='margin-top: 10%;' href=<?php echo base_url(); ?>master/unchoosesub_mobile?id_user="+iduser+"&subject_id="+idsubject+"><?php echo $this->lang->line('hapusdata'); ?></a></div>";
                	
                }
                var list_subject ="<section style='background-color: white;'><div style='' class='col-xs-2'><img style='width: 45px; height: 45px;' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>class_icon/"+icon+"'></div><div style='' class=' col-xs-6'><label style='color:red;'>"+namesubject+"<strong><br><label style='color:blue;'>Kelas "+leveljenjang+" "+namejenjang+"</strong></label></div><div style='' class=' col-xs-4'>"+tombol_ikuti+"<div class='col-xs-12'><hr class='sub_garis'></div></section>";

                var kotak_modal ="<div class='modal' data-modal-color='white' id='harga_modal_"+kodesubject+"' tabindex='-1' data-backdrop='static' role='dialog'><div class='modal-dialog modal-sm' role='document'><div class='modal-content' style='margin-top:30%;'><div  class='modal-header bgm-info' style='text-align: center; font-size: 3vh; background-color: #2196f3;'>Masukkan Harga Kelas</div><br><div id='modal_harga_kelas' class='modal-body' style='color: black' ><br><div style='margin-left:-5%;' class='col-xs-2'><img style=' width: 45px; height: 45px;'  src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>class_icon/"+icon+"'></div><div style='' class=' col-xs-6'><label id='namasubject' style='color:red;'>"+namesubject+"<strong><br><label id='jenjangsubject' style='color:blue;'>Kelas "+leveljenjang+" "+namejenjang+"</strong></label></div><input hidden id='kodesubject'><div class='form-group' ><div class='fg-line'><label style='' class='fg-label'>Harga Kelas Private <small class='c-red'>(Per 15 Menit)</small></label><input onkeypress='return hanyaAngka(event)'  type='text' id='hargaprivate' name='harga_private' required class='nominal input-sm form-control fg-input' value='0'></div></div><div class='form-group'><div class='fg-line'><label style='' class='fg-label'>Harga Kelas Group</label><small class='c-red'> (Per 1 Peserta)</small><input type='text' id='hargagroup' name='harga_group' required class='nominal input-sm form-control fg-input' value='0'  onkeypress='return hanyaAngka(event)'></div></div><label style='font-size: 10px; color: red; text-align: left;'>*Mohon masukkan harga yang ingin Anda dapatkan dari kelas ini. Harga yang muncul ke siswa adalah harga yang Anda masukkan ditambah biaya jasa Classmiles yang bervariasi</label><div class='modal-footer' style=' background-color: white;'><button id='btnBatal' type='button' class='btn btn-danger' data-dismiss='modal' data-toggle='modal' aria-label='Close' style=''>Cancel</button><a class='btn btn-success' style='' href=<?php echo base_url(); ?>master/choosesub_mobile?id_user="+iduser+"&subject_id="+idsubject+">Save</a></div></div></div></div></div></div>";


                $("#modal_harga").append(kotak_modal);
                $("#list_subject").append(list_subject);


                



                
                
            }
			// alert('hh');
			// if(data['code'] == 1){
			// 	notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut',data['message']);
			// 	that.attr('class','waves-effect bgm-green c-white btn btn-success btn-block follow_ajax f-12');
			// 	that.html('<?php echo $this->lang->line('follow'); ?>');				
			// }else{
			// 	notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
			// }
		}
  	});  

	/*$(document).on('click', "a.choose_ajax", function(e) {
		e.preventDefault();
		$(this).removeClass('choose_ajax');
    	alert('ke choose');
	});
	$(document).on('click', "a.unchoose_ajax", function() {
    	alert('ke unchoose');
	});*/
     function hanyaAngka(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
          return true;
    }     	
    $('.nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

    $(".nominal").keyup(function() {        
        var clone = $(this).val();
        var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
        $("#nominalsave").val(cloned);
    });
	$('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable();

	$(document).on('click', "a.choose_ajax", function(e){
		e.preventDefault();
		var urls = $(this).attr('href');
		var sid = $(this).attr('id');
		var that = $(this);
		
		
		$.ajax({
			url: urls,
			type: 'GET',
			success: function(data){
				data = JSON.parse(data);
				if(data['status'] == 1){
					notify('top','right','fa fa-check','inverse','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-red c-white btn btn-danger btn-block unchoose_ajax');
					that.attr('href',"<?php echo base_url('/master/unchoosesub_mobile?id_user='.$this->session->userdata('id_user').'&subject_id='); ?>"+sid);
					that.html('<i class="zmdi zmdi-face-add"></i><?php echo $this->lang->line('tidakikutisubject'); ?>');
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
		});
		redirect('/tutor/choose');
		
	});
	$(document).on('click','a.unchoose_ajax', function(e){
		e.preventDefault();
		var urls = $(this).attr('href');
		var sid = $(this).attr('id');
		var that = $(this);
		
		
		$.ajax({
			url: urls,
			type: 'GET',
			success: function(data){
				data = JSON.parse(data);
				if(data['status'] == 1){
					notify('top','right','fa fa-check','inverse','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-green c-white btn btn-success btn-block choose_ajax');
					that.attr('href',"<?php echo base_url('/master/choosesub_mobile?id_user='.$this->session->userdata('id_user').'&subject_id='); ?>"+sid);
					that.html('<i class="zmdi zmdi-face-add"></i><?php echo $this->lang->line('ikutisubject'); ?>');
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
		});
		redirect('/tutor/choose');
		
	});
	
</script>