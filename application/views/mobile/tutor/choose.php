<style type="text/css">
 html, body {
            max-width: 100%;
            overflow-x: hidden;
            background-color: white;
        }
</style>

<header id="header" class="clearfix" style="background-color: #Cf000f;">
    <div class="col-xs-1"  style="margin-top: 7%" >
        <a href="<?php echo base_url(); ?>Tutor/About" id="back_home" type="button" class="loading c-white f-19" style="cursor: pointer;">
        <i class="zmdi zmdi-arrow-left"></i>
        </a>
    </div>
    <div class=" c-white f-17 col-xs-10" style="margin-top: 7%">
        <center><?php echo $this->lang->line('selectsubjecttutor'); ?></center>
    </div>

</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<section id="content">
		<div class="">
			<?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>
			<!-- disini untuk dashboard murid -->  
			           
			<div class="" style="margin-top: 2%;">
				<div class="card-header"></div>
				<div class="card-body card-padding">
					<div class="row">
						<div class="table-responsive">
							<table id="table_subject" class="display table table-striped table-bordered data" cellspacing="0" >
								<thead>
									<?php
									$select_all = $this->db->query("SELECT ms.subject_id, ms.jenjang_id, ms.subject_name, ms.icon, ms.indonesia, ms.english, mj. * FROM master_jenjang AS mj INNER JOIN master_subject AS ms ON ms.jenjang_id = ms.jenjang_id WHERE mj.jenjang_id = ms.jenjang_id order by ms.subject_id asc")->result_array();
									?>
									<tr style="display: none;">
										<th data-column-id="logo">Logo</th>
										<th data-column-id="name"><?php echo $this->lang->line('subject'); ?></th>
										<th data-column-id="aksi"><?php echo $this->lang->line('aksi');?></th>

									</tr>
								</thead>
								<tbody>  
									<?php
									$no=1;
									foreach ($select_all as $row => $v) {
										$booked = $this->db->query("SELECT * FROM tbl_booking WHERE subject_id='".$v['subject_id']."' AND id_user='".$this->session->userdata('id_user')."' ")->row_array();
										?>       
										<tr>
											<td><img style="width: 40px; height: 40px;" src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL.'class_icon/'.$v['icon']; ?>'></td>
											<td>
												<?php 
													// echo $this->lang->line('subject_'.$v['subject_id']);
													echo $v['subject_name'];
												?><br><?php echo($v['jenjang_name']); ?> - <?php echo($v['jenjang_level']); ?>
											</td>
											<td>
												<?php 
												if(!empty($booked)){
													echo '<a class="waves-effect bgm-red c-white btn btn-danger btn-block unchoose_ajax" id="'.$v['subject_id'].'" href="'.base_url('/master/unchoosesub?id_user='.$this->session->userdata('id_user').'&subject_id='.$v['subject_id']).'"><i class="zmdi zmdi-face-add"></i>'.$this->lang->line('tidakikutisubject').'</a>';
												}else{
													echo '<a class="waves-effect bgm-green c-white btn btn-success btn-block choose_ajax" id="'.$v['subject_id'].'" href="'.base_url('/master/choosesub?id_user='.$this->session->userdata('id_user').'&subject_id='.$v['subject_id']).'"><i class="zmdi zmdi-face-add"></i>'.$this->lang->line('ikutisubject').'</a>';
												}
												?>
											</td>
										</tr>
										<?php 
										$no++;
									}
									?>		
								</tbody>   
							</table>   
						</div>
					</div>
				</div>
			</div>
		</div>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	/*$(document).on('click', "a.choose_ajax", function(e) {
		e.preventDefault();
		$(this).removeClass('choose_ajax');
    	alert('ke choose');
	});
	$(document).on('click', "a.unchoose_ajax", function() {
    	alert('ke unchoose');
	});*/
	$('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: false
        }
    } );
    $('.data').DataTable();

	$(document).on('click', "a.choose_ajax", function(e){
		e.preventDefault();
		var urls = $(this).attr('href');
		var sid = $(this).attr('id');
		var that = $(this);
		
		
		$.ajax({
			url: urls,
			type: 'GET',
			success: function(data){
				data = JSON.parse(data);
				if(data['status'] == 1){
					notify('top','right','fa fa-check','inverse','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-red c-white btn btn-danger btn-block unchoose_ajax');
					that.attr('href',"<?php echo base_url('/master/unchoosesub?id_user='.$this->session->userdata('id_user').'&subject_id='); ?>"+sid);
					that.html('<i class="zmdi zmdi-face-add"></i><?php echo $this->lang->line('tidakikutisubject'); ?>');
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
		});
		
	});
	$(document).on('click','a.unchoose_ajax', function(e){
		e.preventDefault();
		var urls = $(this).attr('href');
		var sid = $(this).attr('id');
		var that = $(this);
		
		
		$.ajax({
			url: urls,
			type: 'GET',
			success: function(data){
				data = JSON.parse(data);
				if(data['status'] == 1){
					notify('top','right','fa fa-check','inverse','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-green c-white btn btn-success btn-block choose_ajax');
					that.attr('href',"<?php echo base_url('/master/choosesub?id_user='.$this->session->userdata('id_user').'&subject_id='); ?>"+sid);
					that.html('<i class="zmdi zmdi-face-add"></i><?php echo $this->lang->line('ikutisubject'); ?>');
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
		});
		
	});
	
</script>
