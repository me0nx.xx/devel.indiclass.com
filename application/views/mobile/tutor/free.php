<header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #Cf000f;">
        <?php $this->load->view('mobile/inc/navbar'); ?>
        <div class="header-inner" style="color: white;  ">
            <center>
            <!-- <div class="col-xs-12" style="background-color: #2196f3;"><a href="#" class=" m-l-10"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>logo/logoclass6.png" alt=""></a></div>     -->
            <div class="col-xs-4" id="kelassaya" style=""><a style="color: white;" href="<?php echo base_url();?>">
                <img id="img_kelassaya" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_myclass_white.png" style="width: 20px;">
                <br><label style="font-size: 9px; text-transform: uppercase; "><?php echo $this->lang->line('home'); ?></label></a>
            </div>
            <!-- <div class="tabmenu col-xs-4" id="aturkelas" style=""><a style="color: white;" href="#">
                <img id="img_aturkelas" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_timer_greens.png" style="width: 20px;  height: auto;">
                <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('aturkelas'); ?></label></a>
            </div> -->
            <div class="tabmenu col-xs-4" id="private" style=""><a style="color: white;" href="<?php echo base_url(); ?>Ondemand">
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_extraclass_white.png" style="width: 20px;  height: auto;">
                <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('private');?></label></a>
            </div>
            <div class="tabmenu col-xs-4" id="akun" style=""><a style="color: white;" href="<?php echo base_url(); ?>tutor/about">
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_account_white.png" style="width: 20px;  height: auto;">
                <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('tab_account');?></label></a>
            </div>
            </center>                           
        </div>
</header>
<br><br>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1" style="background-color: white">
    <section id="content">
            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>
            <div class="alert alert-danger" id="kotakalerttutor" style="display: none;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                Maaf, Sebelum mendapat persetujuan dari pihak kami, anda hanya diperbolehkan membuat kelas sebanyak 5 kali.
            </div>
            <!-- disini untuk dashboard murid -->               
            <div style="" class=" col-md-12" style="">
                <form class="form-horizontal" role="form">
                    <div class="card-header">
                        <h4><?php echo $this->lang->line('atur'); ?> <?php echo $this->lang->line('gratis') ?> <br><small><?php echo $this->lang->line('mengatur'); ?> <?php echo $this->lang->line('gratis') ?></small><br><small class="c-red" style="font-style: italic;"><b>* Harap memilih mata pelajaran yang ingin anda buat kelas di menu "<a href="<?php echo base_url();?>tutor/choose">Pelajaran</a>". Untuk memunculkan di menu pilihan mata pelajaran dibawah ini.</b></small></h4><hr>                        
                    </div>
                    
                    <div  class=" card-body card-padding">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-xs-12 control-label">Tanggal</label>
                            <div class="col-xs-12">
                                <div class="dtp-container fg-line m-l-5">                                                
                                    <input id="tanggal" type='text' required class="form-control"  data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-xs-12 control-label">Waktu</label>
                            <div class="col-xs-12">
                                <div class="dtp-container fg-line m-l-5" id="tempatwaktuu">
                                    <input type='text' class='form-control' id='timeeeeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />                                
                                </div>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label for="inputEmail3" class="col-xs-12 control-label">Durasi</label>
                            <div class="col-xs-12">
                                <div class="dtp-container fg-line">   
                                    <?php 
                                        $status = $this->session->userdata('status');                                        
                                        if ($status == 2) {
                                            ?>
                                    <select class='select2 form-control' required id="durasi">
                                        <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
                                        <option value="900">15 Minutes</option>
                                    </select>
                                            <?php       
                                        }
                                        else
                                        {
                                    ?>
                                    <select class='select2 form-control' required id="durasi">
                                        <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
                                        <option value="900">15 Minutes</option>
                                        <option value="1800">30 Minutes</option>
                                        <option value="2700">45 Minutes</option>
                                        <option value="3600">1 Hours</option>
                                    </select>  
                                        <?php 
                                        }
                                    ?>                                                           
                                </div>
                            </div>
                        </div> 

                        <!-- <div class="form-group">
                            <label for="inputEmail3" class="col-xs-12 control-label">Template</label>
                            <div class="col-xs-12">
                                <div class="dtp-container fg-line">                                                
                                    <select class='select2 form-control' required id="template">
                                        <option disabled selected>Pilih Template</option>
                                        <option value="whiteboard_digital">Multicast Whiteboard Digital</option>
                                        <option value="whiteboard_videoboard">Multicast Whiteboard VideoBoard</option>
                                        <option value="whiteboard_no">Multicast No Whiteboard</option>
                                    </select>                                                             
                                </div>
                            </div>
                        </div> --> 

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Pelajaran</label>
                            <div class="col-xs-12">
                                <div class="dtp-container fg-line">                                                
                                    <select class='select2 form-control' required id="pelajaran">
                                        <?php
                                            $id = $this->session->userdata("id_user");
                                            $allsub = $this->db->query("SELECT tb.*, mj.*, ms.subject_name FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$id' AND tb.tutor_id='0' order by ms.subject_id ASC")->result_array();
                                            echo '<option disabled="disabled" selected="" value="0">Pilih Pelajaran</option>'; 
                                            foreach ($allsub as $row => $v) {

                                                $jenjangnamelevel = array();
                                                $jenjangid = json_decode($v['jenjang_id'], TRUE);
                                                if ($jenjangid != NULL) {
                                                    if(is_array($jenjangid)){
                                                        foreach ($jenjangid as $key => $value) {                                    
                                                            $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();
                                                            
                                                            if ($selectnew['jenjang_level']=='0') {
                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']; 
                                                            }
                                                            else{
                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
                                                            }

                                                            
                                                        }   
                                                    }else{
                                                        $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();   
                                                        if ($selectnew['jenjang_level']=='0') {
                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']; 
                                                            }
                                                            else{
                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
                                                            }                                                         
                                                    }                                       
                                                }

                                                // $allsub = $this->db->query("SELECT tb.*, mj.*, ms.subject_name FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$id' AND tb.tutor_id='0' AND (tb.status='verified' OR tb.status='unverified') order by ms.subject_id ASC")->result_array();                                                
                                                // foreach ($allsub as $row => $v) {                                            
                                                    echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.implode(", ",$jenjangnamelevel).'</option>';
                                                // }
                                            }
                                        ?>
                                    </select>                                                               
                                </div>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Topik Pelajaran</label>
                            <div class="col-xs-12">
                                <div class="dtp-container fg-line">                                                                                
                                    <!-- <input type="text" name="topikpelajaran" class="form-control" required placeholder="Masukan Topik pelajaran"> -->
                                    <textarea rows="5" name="topikpelajaran" id="topikpelajaran" class="form-control" required placeholder="Masukan Topik Pelajaran"></textarea>        
                                </div>
                            </div>
                        </div> 
                        <input type="text" name="user_utc" id="user_utc" value="<?php echo $this->session->userdata('user_utc'); ?>" hidden>                   

                        <br><hr>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-7">
                                <!-- <a class="waves-effect c-white btn btn-primary btn-block simpanprice"><i class="zmdi zmdi-face-add"></i>Buat Kelas</a> -->
                                <button type="submit" class="waves-effect c-white btn btn-primary btn-block " id="simpankelas">Buat Kelas</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>                     
    </section>
</section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

$(document).ready(function(){

    var tgll = new Date();  
    var formattedDatee = moment(tgll).format('YYYY-MM-DD');
    var a = [];
    var b = [];            
    var c = [];
    var d = [];

    $(function () {
        var tgl = new Date();  
        var formattedDate = moment(tgl).format('YYYY-MM-DD');         
        
        $('#tanggal').datetimepicker({  
            minDate: moment(formattedDate, 'YYYY-MM-DD')
        });
        
    });

    $('#tanggal').on('dp.change', function(e) {
        date = moment(e.date).format('YYYY-MM-DD');                
        var hoursnoww = tgll.getHours(); 
        // alert(tgl);
        if (date == formattedDatee) {
            a = [];
            $("#tempatwaktuu").html("");
            $("#tempatwaktuu").append("<input type='text' class='form-control' id='time' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
            $("#time").removeAttr('disabled');
            for (var i = hoursnoww+1; i < 24; i++) {                                                
                a.push(i);
            }
            console.warn(a);
            $('#time').datetimepicker({                    
                sideBySide: true, 
                showClose: true,                   
                format: 'HH:mm',
                stepping: 15,
                enabledHours: a,
            });
        }
        else
        {
            $("#tempatwaktuu").html("");
            $("#tempatwaktuu").append("<input type='text' class='form-control' id='time' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
            $("#time").removeAttr('disabled');
            b = [];
            for (var i = 0; i < 24; i++) {                                                
                b.push(i);
            }
            console.warn(b);
            $('#time').datetimepicker({                    
                sideBySide: true,                    
                format: 'HH:mm',
                showClose: true,
                stepping: 15,
                enabledHours: b,
            });
        }
        
    });

    $('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable();
    
    $("#simpankelas").click(function(e){
        $("#simpankelas").attr('disabled', true);
        var rate_value = null;
        // var isChecked = $('.aba').prop('checked');
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        var idtutorni = "<?php echo $this->session->userdata('id_user');?>";
        var tanggal = $("#tanggal").val();      
        var time = $("#time").val()+":00";
        var durasi = $("#durasi").val();
        var template = 'all_featured';
        var pelajaran = $("#pelajaran").val();
        var topikpelajaran = $("#topikpelajaran").val();
        var user_utc = $("#user_utc").val();
        var status = "<?php echo $this->session->userdata('status');?>";
        $.ajax({
            url: '<?php echo base_url(); ?>Rest/createclass_free/access_token/'+tokenjwt,
            type: 'POST',
            data: {
                id_tutor: idtutorni,
                tanggal: tanggal,
                time: time,
                durasi: durasi,
                template: template,
                pelajaran: pelajaran,
                topikpelajaran: topikpelajaran,             
                user_device: 'mobile',
                user_utc: user_utc,
                status: status
            },
            success: function(response)
            {                   
                // alert(response['code'])
                if (response['code'] == "200") {
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil Menambah Kelas");
                    setTimeout(function(){
                        window.location.replace("<?php echo base_url();?>");
                    },1000);
                }
                else if (response['code'] == "405") {
                    // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Gagal, Sebelum mendapat persetujuan dari pihak kami, anda hanya diperbolehkan membuat kelas sebanyak 5 kali.");
                    // setTimeout(function(){
                    //     location.reload();
                    // },10000);
                    $(window).scrollTop(0);
                    $("#kotakalerttutor").css('display','block');
                    setTimeout(function(){
                        location.reload();
                    },10000);
                }else if(response['code'] == '403') {
                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', response['message']);
                    setTimeout(function(){
                        location.reload();
                    },2000);
                }
                else
                {
                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Gagal Menambah Kelas");
                    setTimeout(function(){
                        // location.reload();
                    },2000);
                }
            }
        });
    
    });
    }); 
    
</script>