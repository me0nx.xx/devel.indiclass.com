<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar');

    ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('./inc/sidetutor'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <?php if(isset($mes_alert)){ ?>
            <div class="alert alert-<?php echo $mes_alert; ?>" style="display:<?php echo $mes_display; ?>">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <i class="fa fa-ban-circle"></i><?php echo $mes_message; ?>
            </div>
            <?php } ?>

            <div class="block-header">
                <h2><?php echo $this->lang->line('setavailabilitytime'); ?></h2>

                <ul class="actions">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>tutor"><?php echo $this->lang->line('hometutor'); ?></a></li>
                            <li class="active"><?php echo $this->lang->line('setavailabilitytime'); ?></li>
                        </ol>
                    </li>
                </ul>
            </div>

            <br>
            <div class="card z-depth-2">
                <div class="card-header">
                    <h2><?php echo $this->lang->line('subdisc'); ?></h2><br><br>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <th width="100">No</th>
                                    <th width="350"><?php echo $this->lang->line('title_pg_subject'); ?></th>
                                    <th width="250"><?php echo $this->lang->line('subdisc'); ?></th>                                        
                                </thead>
                                <tbody>
                                <?php 
 
                                    $now = date('N');
                                    $nextMon = date('Y-m-d', strtotime('+'.(8-$now).' day', strtotime(date('Y-m-d'))));
                                    $my_subjects = $this->db->query("SELECT * FROM tbl_booking WHERE id_user='".$this->session->userdata('id_user')."'")->result_array();
                                    foreach ($my_subjects as $key => $value) {
                                        $this->Rumus->get_subdesc($value['subject_id'],$value['id_user'],$nextMon);

                                        $matpel = $this->Rumus->get_subjectbyid($value['subject_id'])['subject_name'];
                                        echo '<tr>
                                        <td>'.($key+1).'</td>
                                        <td>                                               
                                            <label><h2>'.$matpel.'</h2></label>                              
                                        </td>
                                        <td>
                                            <textarea class="form-control" rows="5" placeholder="Contoh : Bangun ruang, Aljabar, dll."></textarea>
                                        </td>
                                        </tr>';
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>

                        <a href="<?php echo base_url(''); ?>tutor/availability"><button class="btn btn-primary btn-block"><?php echo $this->lang->line('settime');?></button></div>
                        </div>
                    </div>
                </div>




                <div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">                                                      
                                <div class="pull-left">
                                    <h3 class="modal-title"><?php echo $this->lang->line('description_tutor');?></h3>
                                </div>
                                <div class="pull-right">
                                    <button type="button" class="btn bgm-gray" data-dismiss="modal">X</button>
                                </div>                          
                                <hr style="margin-top:6%;">

                            </div>
                            <div class="modal-body">


                            </div>
                            <br><br>
                            <div class="modal-footer m-r-30" >

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>


