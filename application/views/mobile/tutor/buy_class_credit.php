<style type="text/css">
        body{
        background-color: white;
    }
</style>
<header id="header" class="clearfix" style="background-color: #Cf000f;">
    <div class="col-xs-1"  style="margin-top: 7%" >
        <a href="" id="btn_back" type="button" class="loading c-white f-19" style="cursor: pointer;">
        <i class="zmdi zmdi-arrow-left"></i>
        </a>
    </div>
    <div class=" c-white f-20 col-xs-10" style="margin-top: 7%" >
        <center><?php echo $this->lang->line('buy_class_credit'); ?></center>
    </div>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="" data-layout="layout-1" style="padding-top: 30px;">
    <section id="content">
        <div class="">
            <div class="block-header">
            </div> <!-- akhir block header    -->        
            <br>
            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>
            <!-- disini untuk dashboard murid -->               
            <div class="col-md-12 p-t-10 ">
                <form class="form-horizontal" role="form">
                    <!-- <div class="card-header">
                        <h2>Atur Kelas <small>Mengatur Jadwal Kelas Berbayar</small></h2><hr>                        
                    </div> -->                 
                    <div class="card-body card-padding" style="padding-left: 0; padding-right: 0;"> 
                    	<div class="col-md-12">
                        	<div class="row">
                        		<div class="col-xs-12" id="boxVolume" align="center">
                        			<p class="f-14">Silahkan Pilih Jumlah participants </p>
                        			<div class="form-group" id="boxVolumeParticipants">
    		                            
    			                    </div>
                        		</div>
                        		<div class="col-xs-12" id="boxSideDetail" style="display: none;">                    		                    		

                        			<div class="card pt-inner">
    	                                <div class="pti-header bgm-green" style="padding-top: 5px; padding-bottom: 40px;">
    	                                    <h2 style="font-size: 25px;" id="countVolume">
    	                                    </h2>
    	                                    <div class="ptih-title" style="text-transform: capitalize;" id="priceVolume"></div>
    	                                </div>

    	                                <div class="pti-body">
    	                                    <div class="ptib-item" id="detailVolume">
    	                                        
    	                                    </div>
    	                                </div>

    	                                <div class="pti-footer">
    	                                    <button type="submit" class="waves-effect c-white btn btn-lg btn-warning btn-block " id="askinvoice"><?php echo $this->lang->line('btnclasscredit'); ?></button>
    	                                </div>
    	                            </div>
                        		</div>
                        	</div>
                        </div>
                        <input type="text" name="user_utcc" id="user_utcc" value="<?php echo $this->session->userdata('user_utc'); ?>" hidden>                                        

                    </div>
                    
                </form>
            </div>
        </div>                                
    </section>
</section>
<script type="text/javascript">

$(document).ready(function(){

    var tgll = new Date();  
    var formattedDatee = moment(tgll).format('YYYY-MM-DD');
    var id_user = "<?php echo $this->session->userdata('id_user');?>"
    var a = [];
    var b = [];            
    var c = [];
    var d = [];    
    var lastid = "";
    var user_utc = new Date().getTimezoneOffset();
    var status = 0;
    user_utc = -1 * user_utc;

    $.ajax({
        url: '<?php echo base_url(); ?>Rest/list_paidmulticast_volume',
        type: 'POST',
        data: {
            type : 'paid_multicast'
        },
        success: function(response)
        { 
        	var code = response['code'];
        	if (code == 200) {

        		var kotak = '';
        		for (var i = 0; i < response['data'].length; i++) {
        			var id 		= response['data'][i]['id'];
	        		var volume 	= response['data'][i]['volume'];
	        		var price 	= response['data'][i]['price'];	
	        		var price_cv= response['data'][i]['price_cv'];	

					kotak += "<div class='col-md-4 col-xs-4 m-b-20 p-0'>"+
					  	"<button class='btn btn-default btn-lg btn-block waves-effect chooseVolume' id='choose"+id+"' style='color:#bdc3c7; word-wrap:break-word;' data-id='"+id+"' data-pr='"+price+"' data-prc='"+price_cv+"' data-vr='"+volume+"'>"+volume+" </button>"+
					"</div>";        		
        		}   
        		$("#boxVolumeParticipants").append(kotak);

        	}
        }
    });
    $(document.body).on('click','#btn_back', function(e){
        
        if (status == 0) {
            window.location = '<?php echo base_url('/'); ?>';
        }
        else{
            $("#boxSideDetail").css('display','none');
            $("#boxVolume").css('display','block');
            status = 0;
        }

    });

    $(document.body).on('click', '.chooseVolume' ,function(e){ 
        status = 1;
    	var chooseid 	= $(this).attr('id');
    	var id 			= $(this).data('id');
    	var pr 			= $(this).data('pr');
    	var vr 			= $(this).data('vr');
    	var prc 		= $(this).data('prc');
    	$("#"+chooseid).css('color','#34495e');

    	if (lastid == "") {    	
    		lastid = chooseid;
    	}
    	else
    	{    		
    		$("#"+lastid).css('color','#bdc3c7');
    		lastid = chooseid;    		
    	}

    	$("#priceVolume").html("<small>Rp.</small> "+prc);
    	$("#countVolume").html(vr+" Participants");
    	$("#detailVolume").html('<?php echo $this->lang->line('classcredit'); ?>');
    	$("#boxSideDetail").css('display','block');
        $("#boxVolume").css('display','none');
    	$("#askinvoice").attr("idp",id);
    	$("#askinvoice").attr('price',pr);
    	$("#askinvoice").attr('volume',vr);
    });

    $(document.body).on('click', '#askinvoice' ,function(e){ 
    	var user_utc    = $("#user_utcc").val();
    	var id 			= $(this).attr('idp');
    	var price 		= $(this).attr('price');
    	var volume 		= $(this).attr('volume');    	

    	$.ajax({
	        url: '<?php echo base_url(); ?>Rest/request_invoice_multicast',
	        type: 'POST',
	        data : {
	        	id_tutor : id_user,
	        	price : price,
	        	volume : volume,
	        	type : 'paid_multicast',
	        	user_utc : user_utc
	        },
	        success: function(response)
	        { 
	        	var code = response['code'];
	        	if (code == 200) {
                    swal("Success Request Invoice, please check your email", "", "success")
                    setTimeout(function(){
                        window.location.replace("<?php echo base_url();?>Tutor/BuyClassCredit");
                    },1000);
        		}
        		else
        		{
                    swal("Failed request", "", "failled")
                    setTimeout(function(){
                        location.reload();
                    },3000);
        		}
        	}
        });
    });

    $('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable();

    $('.nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

    $(".nominal").keyup(function() {        
        var clone = $(this).val();
        var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
        $("#nominalsave").val(cloned);
    });
    
    $("#simpankelas").click(function(e){
        $("#simpankelas").attr('disabled', true);
        var rate_value = null;
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        var idtutorni = "<?php echo $this->session->userdata('id_user');?>";
        var tanggal = $("#tanggal").val();      
        var time = $("#time").val()+":00";        
        var durasi = $("#durasi").val();
        var template = 'all_featured';        
        var nominal = $("#nominalsave").val();
        var pelajaran = $("#pelajaran").val();
        var topikpelajaran = $("#topikpelajaran").val();
        var user_utc = $("#user_utcc").val();
        if (tanggal=="") {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih tanggal kelas terlebih dahulu!!!");
        }
        else if (time=="undefined:00") {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih waktu kelas terlebih dahulu!!!");
        }
        else if (durasi == null) {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih durasi kelas terlebih dahulu!!!");
        }
        else if (template == null) {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih template kelas terlebih dahulu!!!");
        }
        else if (pelajaran == null) {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih pelajaran kelas terlebih dahulu!!!");
        }
        else if (topikpelajaran == "") {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon mengisi topik pelajarn kelas terlebih dahulu!!!");
        }
        else
        {
            $.ajax({
                url: '<?php echo base_url(); ?>Rest/createclass_paid/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    id_tutor: idtutorni,
                    tanggal: tanggal,
                    time: time,    
                    durasi:durasi,                
                    template: template,
                    pelajaran: pelajaran,
                    topikpelajaran: topikpelajaran,
                    harga: nominal,
                    user_device: 'web',
                    user_utc: user_utc
                },
                success: function(response)
                {                   
                    // alert(response['code'])
                    if (response['code'] == "200") {
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil Menambah Kelas");
                        setTimeout(function(){
                            window.location.replace("<?php echo base_url();?>");
                        },1000);
                    }else if(response['code'] == '403') {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', 'Maaf, anda tidak membuat kelas di waktu bersamaan.');
                        $("#simpankelas").removeAttr('disabled');
                        // setTimeout(function(){
                        //     location.reload();
                        // },2000);
                    }
                    else
                    {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Gagal Menambah Kelas");
                        setTimeout(function(){
                            location.reload();
                        },3000);
                    }
                }
            });
        }
        
    }); 
}); 
    
</script>