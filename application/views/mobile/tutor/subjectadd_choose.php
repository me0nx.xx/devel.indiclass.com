<style type="text/css">
 html, body {
            max-width: 100%;
            overflow-x: hidden;
            background-color: white;
        }
</style>

<header id="header" class="clearfix" style="background-color: #Cf000f;">
    <div class="col-xs-1"  style="margin-top: 7%" >
        <a href="<?php echo base_url(); ?>Tutor/About" id="back_home" type="button" class="loading c-white f-19" style="cursor: pointer;">
        <i class="zmdi zmdi-arrow-left"></i>
        </a>
    </div>
    <div class=" c-white f-17 col-xs-10" style="margin-top: 7%">
        <center>Menentukan Harga</center>
    </div>

</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<section id="content">
		<section class="">     
			<?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>
			<!-- disini untuk dashboard murid -->               
			<div class="">
				<div class="card-header"></div>
				<div class="card-body card-padding">
				<div class="row">
				<div class="table-responsive">
					<table id="" class="table " cellspacing="0" width="100%">
						<thead style="display: none;">
							<?php
							$idtutor = $this->session->userdata('id_user');
							$select_all = $this->db->query("SELECT tb.*, mj.*, ms.subject_name, ms.icon FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$idtutor' AND tb.tutor_id='0' order by ms.subject_id ASC")->result_array();
							?>
							<tr>
								<th data-column-id="name"><?php echo $this->lang->line('subject'); ?></th>
								<!-- <th data-column-id="price">Harga</th> -->

							</tr>
						</thead>
						<tbody>  
							<?php
							$no=1;
							foreach ($select_all as $row => $v) {
								$data = $this->db->query("SELECT ( SELECT harga FROM tbl_service_price WHERE subject_id='".$v['subject_id']."' AND tutor_id='".$this->session->userdata('id_user')."' AND class_type='private' ) as private_harga, ( SELECT harga FROM tbl_service_price WHERE subject_id='".$v['subject_id']."' AND tutor_id='".$this->session->userdata('id_user')."' AND class_type='group' ) as group_harga ")->row_array();
								?> 
								<tr>
									<td><img style="width: 40px; height: 40px;" src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL.'class_icon/'.$v['icon']; ?>'>
										<?php 
											echo $v['subject_name'].' - '.$v['jenjang_level'].' '.$v['jenjang_name'];
										?><br><br>
										<label>Pasang Harga Private :</label><input call() type="text" id="nominal" subject="<?php echo $v['subject_id']; ?>" required class="price<?php echo $v['subject_id']; ?> input-sm form-control fg-input nominal" placeholder=" Masukan harga private anda" minlength="9" step="500" maxlength="14" min="10000" max="500000" value="<?php echo $data['private_harga']; ?>"/>
                                        <input type="text" id="price<?php echo $v['subject_id']; ?>" class="hargakelas<?php echo $v['subject_id']; ?>" value="<?php echo $data['private_harga']; ?>" hidden  />
										<br>
										<label>Pasang Harga Group :</label><input type="text" id="nominalg" subject="<?php echo $v['subject_id']; ?>" required class="price<?php echo $v['subject_id']; ?> input-sm form-control fg-input nominalg" placeholder=" Masukan harga group anda " minlength="9" step="500" maxlength="14" min="10000" max="500000" value="<?php echo $data['group_harga']; ?>"/>
	                                        <input type="text" id="price<?php echo $v['subject_id']; ?>" class="hargakelasgroup<?php echo $v['subject_id']; ?>" value="<?php echo $data['group_harga']; ?>" hidden  />
	                                        <br>
	                                        <?php 
										// if(!empty($booked)){
										// 	echo '<a class="waves-effect bgm-red c-white btn btn-danger btn-block unchoose_ajax" id="'.$v['subject_id'].'" href="'.base_url('/master/unchoosesub?id_user='.$this->session->userdata('id_user').'&subject_id='.$v['subject_id']).'"><i class="zmdi zmdi-face-add"></i>'.$this->lang->line('tidakikutisubject').'</a>';
										// }else{
										// 	echo '<a class="waves-effect bgm-green c-white btn btn-success btn-block choose_ajax" id="'.$v['subject_id'].'" href="'.base_url('/master/choosesub?id_user='.$this->session->userdata('id_user').'&subject_id='.$v['subject_id']).'"><i class="zmdi zmdi-face-add"></i>'.$this->lang->line('ikutisubject').'</a>';
										// }
										if ($data['group_harga'] != NULL || $data['private_harga'] != NULL)
										{
											echo '<a class="waves-effect c-white btn bgm-deeporange btn-block simpanprice" id="'.$v['subject_id'].'"><i class="zmdi zmdi-face-add"></i>Ubah</a>';
										}
										else
										{
											echo '<a class="waves-effect bgm-green c-white btn btn-success btn-block simpanprice" id="'.$v['subject_id'].'"><i class="zmdi zmdi-face-add"></i>Tambah</a>';
										}
										
										?>
									</td>					
								</tr>
								<?php 
								$no++;
							}
							?>		
						</tbody>   
					</table><hr>
					<div class="container">
						<label class="c-red" style="text-align: center;">* Silahkan Masukan harga dan pilih tipe kembali untuk mengubah data yang sebelumnya</label>
					</div>
					   
				</div>
				</div>
				</div>
			</div>
		</section>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	$('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable();
	$(document).ready(function(){
		

        $('.nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

        $(".nominal").keyup(function() {
        	var sb = $(this).attr('subject');        	
			var clone = $(this).val();
			var cloned = clone.replace(/[A-Za-z$. ,-]/g, "");
			$(".hargakelas"+sb).val(cloned);
        });

        $('.nominalg').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

        $(".nominalg").keyup(function() {
        	var sb = $(this).attr('subject');        	
			var clone = $(this).val();
			var cloned = clone.replace(/[A-Za-z$. ,-]/g, "");
			$(".hargakelasgroup"+sb).val(cloned);
        });

   //      function displayharga(harga){ 
   //      	var bilangan = harga;
	
			// var	number_string = bilangan.toString(),
			// 	sisa 	= number_string.length % 3,
			// 	rupiah 	= number_string.substr(0, sisa),
			// 	ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
					
			// if (ribuan) {
			// 	separator = sisa ? '.' : '';
			// 	rupiah += separator + ribuan.join('.');
			// }
			// $('.nominal').val("Rp. "+rupiah);
   //      	var sb = $(this).attr('subject');  
			// console.warn(sb+" Rp. "+rupiah);
   //      }

    });
	/*$(document).on('click', "a.choose_ajax", function(e) {
		e.preventDefault();
		$(this).removeClass('choose_ajax');
    	alert('ke choose');
	});
	$(document).on('click', "a.unchoose_ajax", function() {
    	alert('ke unchoose');
	});*/

	$(document).on('click', "a.simpanprice", function(e){
		var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
		var idtutorni = "<?php echo $this->session->userdata('id_user');?>";
		var subject_idd = $(this).attr('id');		
		var harga = $(".hargakelas"+subject_idd).val();
		var hargagroup = $(".hargakelasgroup"+subject_idd).val();
		var type = $("#type"+subject_idd).val();
		
		$.ajax({
			url: '<?php echo base_url();?>Rest/subjectadd/access_token/'+tokenjwt,
			type: 'POST',
			data: {
				id_tutor: idtutorni,
				subject_id: subject_idd,
				harga: harga,
				hargagroup: hargagroup,
				class_type: type
			},
			success: function(data)
			{	
				code = data['code'];
				if (code == "200") {
					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil Menambah harga");
					setTimeout(function(){
						location.reload();
					},500);
				}
				else if(code == "201")
				{
					notify('top','right','fa fa-check','failed','animated fadeInDown','animated fadeOut', "Gagal Menambah harga");
					setTimeout(function(){
						location.reload();
					},500);
				}
				else if (code == "300") 
				{
					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil Merubah harga");
					setTimeout(function(){
						location.reload();
					},500);
				}
				else if(code == "201")
				{
					notify('top','right','fa fa-check','failed','animated fadeInDown','animated fadeOut', "Gagal Merubah harga");
					setTimeout(function(){
						location.reload();
					},500);
				}
			}
		});

	});		

	$(document).on('click', "a.choose_ajax", function(e){
		e.preventDefault();
		var urls = $(this).attr('href');
		var sid = $(this).attr('id');
		var that = $(this);
		
		
		$.ajax({
			url: urls,
			type: 'GET',
			success: function(data){
				data = JSON.parse(data);
				if(data['status'] == 1){
					notify('top','right','fa fa-check','inverse','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-red c-white btn btn-danger btn-block unchoose_ajax');
					that.attr('href',"<?php echo base_url('/master/unchoosesub?id_user='.$this->session->userdata('id_user').'&subject_id='); ?>"+sid);
					that.html('<i class="zmdi zmdi-face-add"></i><?php echo $this->lang->line('tidakikutisubject'); ?>');
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
		});
		
	});
	$(document).on('click','a.unchoose_ajax', function(e){
		e.preventDefault();
		var urls = $(this).attr('href');
		var sid = $(this).attr('id');
		var that = $(this);
		
		
		$.ajax({
			url: urls,
			type: 'GET',
			success: function(data){
				data = JSON.parse(data);
				if(data['status'] == 1){
					notify('top','right','fa fa-check','inverse','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-green c-white btn btn-success btn-block choose_ajax');
					that.attr('href',"<?php echo base_url('/master/choosesub?id_user='.$this->session->userdata('id_user').'&subject_id='); ?>"+sid);
					that.html('<i class="zmdi zmdi-face-add"></i><?php echo $this->lang->line('ikutisubject'); ?>');
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
		});
		
	});
	
</script>