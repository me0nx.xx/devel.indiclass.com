<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');?>
</header>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1" >
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('inc/sidetutor'); ?>
	</aside>

	<section id="content" ng-init="gettutorapproval_group()">
		<div class="container">

			<div class="block-header">
				<h2><?php echo $this->lang->line('approvalondemand'); ?> Grup</h2>

				<ul class="actions hidden-xs">
					<li>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>tutor"><?php echo $this->lang->line('hometutor'); ?></a></li>
							<li class="active"><?php echo $this->lang->line('approvalondemand'); ?> Group</li>
						</ol>
					</li>
				</ul>
			</div> <!-- akhir block header    --> 

			<div style="margin-top: 8%;">

				<div class="alert alert-success alert-dismissible text-center" role="alert" ng-if="firste">                                
		            <label><?php echo $this->lang->line('norequest');?></label>
		        </div> 

				<div class="col-sm-4 col-md-4" style="" ng-repeat="x in daturg">
					<div class="card c-gray">
						<div class="card-header bgm-blue" style=" height: 145px; border-radius: 5px 5px 0px 0px;">

							<img ng-click="" style="float:left; border-radius: 5px; " class="m-r-10" width="100" height="100" src="<?php echo "https://classmiles.com/";?>aset/img/user/{{x.id_user_requester}}.jpg">

							<h2 style="margin-left: 5px;">{{x.user_name}} <small>{{x.jenjang_level}} {{x.jenjang_name}} - {{x.subject_name}}</small>
							<small>{{x.date_requested | date:'EEEE, d MMMM y'}} {{x.time_requested}}</small>                            
							<small>Rp. {{x.harga_kelas}} | Duration {{x.duration_requested}} minutes</small></h2>

						</div>

						<div class="card-body card-padding" style="height:11vh; top:10px;">

							<div style="width:48%; float:left;">
								<button request_id="{{x.request_id}}" user_id="{{x.id_user_requester}}" class="btn-confirm-group btn btn-block btn-success btn-icon-text"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('approve');?></button><br><br>                                                                     
							</div>
							<div style="width:48%; float:left; margin-left:3%;">								
								<button request_id="{{x.request_id}}" class="btn-reject-group btn btn-block bgm-red btn-icon-text"><i class="zmdi zmdi-close"></i> <?php echo $this->lang->line('decline');?></button>
								
							</div>                                                
						</div>

					</div>
				</div>  

				<!-- Modal CONFRIM -->    
                <!-- <div class="modal fade" id="modalconfrim" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"><?php echo $this->lang->line('approve');?></h4>
                                <hr>
                            </div>
                            <div class="modal-body">
                                <center>
                                <img id="gambar" style="border-radius: 1%;" class="m-b-5" width="90%" height="90%" alt=""><br>
                                <label><h3><?php echo $this->session->userdata('nama_lengkap');?></h3></label><br>
                                <label>18:00</label>
                                <hr>
                                <div class="dtp-container fg-line">                                                
	                                <select class='select2 form-control' required id="templategroup">
	                                    <option disabled selected>Pilih Template</option>
	                                    <option value="whiteboard_digital">Group Whiteboard Digital</option>
	                                    <option value="whiteboard_videoboard">Group Whiteboard VideoBoard</option>
	                                    <option value="whiteboard_no">Group No Whiteboard</option>
	                                </select>                                                             
	                            </div>
                                </center>
                                <label class="f-14"><?php echo $this->lang->line('areyousure'); ?>?</label>
                            </div>
                            <div class="modal-footer">
                                <hr>
                                <button type="button" class="btn-choose-demand-group btn btn-success" demand-link-group=""><?php echo $this->lang->line('ikutidemand');?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('cancel');?></button>
                            </div>
                        </div>
                    </div>
                </div>  -->
                <div class="modal" style="margin-top: 12%; margin-left: 12%; margin-right: 12%;" id="modalconfrim" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
	                <div class="modal-dialog modal-sm">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <h4 class="modal-title c-black">Pilih Template Kelas</h4>
	                            <hr>	                            
                                <label class="f-14 m-b-15 c-black fg-line">Harap Memilih Tipe Tampilan kelas yang akan digunakan nanti.</label>
                                <!-- <div class="col-sm-12 m-b-5">
                                    <select class="selectpicker" required id="templategroup">
                                        <option disabled selected>Pilih Template Group</option>
	                                    <option value="whiteboard_digital">Group Whiteboard Digital</option>
	                                    <option value="whiteboard_videoboard">Group Whiteboard VideoBoard</option>
	                                    <option value="whiteboard_no">Group No Whiteboard</option>
                                    </select>
                                </div>	  -->                           	                          	                            
	                        </div>                                                
	                        <div class="modal-footer">
	                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal"><?php echo $this->lang->line('cancel');?></button>
	                            <button type="button" class="btn-choose-demand-group btn bgm-green" id="confrimbro" demand-link-group=""><?php echo $this->lang->line('ikutidemand');?></button>
	                        </div>                        
	                    </div>
	                </div>
	            </div>    

                <!-- Modal REJECT -->    
               <!--  <div class="modal fade" id="modalreject" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-body">                                
                                <label class="f-20 m-t-20"><?php echo $this->lang->line('areyousure');?>?</label>
                            </div>
                            <div class="modal-footer">  
                            	<hr>                              
                                <button type="button" class="btn-reject-demand-group btn btn-success" demand-link-group=""><?php echo $this->lang->line('yes');?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('no');?></button>                                
                            </div>
                        </div>
                    </div>
                </div> --> 

                <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="modalreject" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
	                <div class="modal-dialog modal-sm">
	                    <div class="modal-content">
	                        <div class="modal-header">
	                            <h4 class="modal-title c-black">Tolak Permintaan Kelas</h4>
	                            <hr>	                            
                                <label class="f-14 c-black"><?php echo $this->lang->line('areyousure');?></label>
	                        </div>                                                
	                        <div class="modal-footer">
	                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal"><?php echo $this->lang->line('no');?></button>
	                            <button type="button" class="btn-reject-demand-group btn bgm-green" demand-link-group=""><?php echo $this->lang->line('yes');?></button>
	                        </div>                        
	                    </div>
	                </div>
	            </div>                 

			</div>

		</div>
	</section>
</section>


