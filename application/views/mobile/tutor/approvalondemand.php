<header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #Cf000f;">
        <?php $this->load->view('mobile/inc/navbar'); ?>
        <div class="header-inner" style="color: white; padding-top: 0; margin-top: 0; ">
            <center>
            <!-- <div class="col-xs-12" style="background-color: #2196f3;"><a href="#" class=" m-l-10"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>logo/logoclass6.png" alt=""></a></div>     -->
            <div class="tabmenu col-xs-4" style=""><a style="color: white;" href="<?php echo base_url();?>">
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/My Class White.svg" style="width: 20px;">
                <br><label style="font-size: 9px; text-transform: uppercase; "><?php echo $this->lang->line('home'); ?></label></a>
            </div>
            <!-- <div class="tabmenu col-xs-4" style=""><a style="color: white;" href="<?php echo base_url(); ?>tutor/set_class">
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Jam White.svg" style="width: 20px;  height: auto;">
                <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('aturkelas'); ?></label></a>
            </div> -->
            <div class=" col-xs-4" style=""><a style="color: white;" href="<?php echo base_url(); ?>tutor/approval_ondemand">
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Extra Class Green.svg" style="width: 20px;  height: auto;">
                <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('private');?></label></a>
            </div>
            <div class="tabmenu col-xs-4" style=""><a style="color: white;" href="<?php echo base_url(); ?>tutor/about">
                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Account White.svg" style="width: 20px;  height: auto;">
                <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('tab_account');?></label></a>
            </div>
            </center>                           
        </div>  
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1" >
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('inc/sidetutor'); ?>
	</aside>
	<style type="text/css">
        body{
            background-color: white;
             max-width: 100%;
            overflow-x: hidden;
        }
    </style>
    <div role="tabpanel" class="">
	        <ul class="tab-nav text-center" role="tablist">
	            <li class="col-xs-6 active"><a href="#kotak_approval" aria-controls="kotak_approval" role="tab" data-toggle="tab">Private</a></li>
	            <li class="col-xs-6"><a href="#kotak_approval_group" aria-controls="kotak_approval_group" role="tab" data-toggle="tab">Group</a></li>
	        </ul>
	      	<div class="modal fade fade" style="" id="modal_loading" tabindex="-1" data-backdrop="static" role="dialog">
			    <div class="modal-dialog" role="document" style="background:rgba(0,00,00,0.0);">   
			        <div style="margin-top: 50%;">
			            <center>
			               <div class="preloader pl-xxl pls-teal">
			                    <svg class="pl-circular" viewBox="25 25 50 50">
			                        <circle class="plc-path" cx="50" cy="50" r="20" />
			                    </svg>
			                </div><br>
			               <p style="color: white;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
			            </center>
			        </div>
			    </div>
			</div>
	        <div class="tab-content">

	            <section role="tabpanel" class="tab-pane active" id="kotak_approval">
	            	<div class="container" id="content" ng-init="gettutorapproval()">
						<div style="">

							<div class="alert alert-info alert-dismissible text-center" role="alert" ng-if="firste">                                
					            <label><?php echo $this->lang->line('norequest');?></label>
					        </div> 

							<div class="col-sm-4" style="" ng-repeat="x in datur">
								<div class="card c-gray">
									<div class="card-header bgm-blue" style=" height: 145px; border-radius: 5px 5px 0px 0px;">

										<img ng-click="" style="float:left; border-radius: 5px; " class="m-r-10" width="100" height="100" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>/{{x.user_image}}">

										<h2 style="margin-left: 5px;">{{x.user_name}} <small>{{x.jenjang_level}} {{x.jenjang_name}} - {{x.subject_name}}</small>
										<small>{{x.valid_date_requested | date:'EEEE, d MMMM y'}}</small>
										<small>Duration {{x.duration_requested}}</small></h2>

									</div>

									<div class="card-body card-padding" style="height:11vh; top:10px;">

										<div style="width:48%; float:left;">
											<button request_id="{{x.request_id}}" user_id="{{x.id_user_requester}}" class="btn-confirm btn btn-block btn-success btn-icon-text"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('approve');?></button><br><br>                                                                     
										</div>
										<div style="width:48%; float:left; margin-left:3%;">								
											<button request_id="{{x.request_id}}" class="btn-reject btn btn-block bgm-red btn-icon-text"><i class="zmdi zmdi-close"></i> <?php echo $this->lang->line('decline');?></button>
											
										</div>                                                
									</div>

								</div>
							</div>  

							<!-- Modal CONFRIM -->    
				            <!-- <div class="modal fade" id="modalconfrimm" tabindex="-1" role="dialog" aria-hidden="true">
				                <div class="modal-dialog modal-sm">
				                    <div class="modal-content">
				                        <div class="modal-header">
				                            <h4 class="modal-title"><?php echo $this->lang->line('approve');?></h4>
				                            <hr>
				                        </div>
				                        <div class="modal-body">
				                            <center>
				                            <img id="gambar" style="border-radius: 1%;" class="m-b-5" width="90%" height="90%" alt=""><br>
				                            <label><h3><?php echo $this->session->userdata('nama_lengkap');?></h3></label><br>
				                            <label>18:00</label>
				                            <hr>
				                            <div class="dtp-container fg-line">                                                
				                                <select class='select2 form-control' required id="template">
				                                    <option disabled selected>Pilih Template</option>
				                                    <option value="whiteboard_digital">Private Whiteboard Digital</option>
				                                    <option value="whiteboard_videoboard">Private Whiteboard VideoBoard</option>
				                                    <option value="whiteboard_no">Private No Whiteboard</option>
				                                </select>                                                             
				                            </div>
				                            </center>
				                            <label class="f-14"><?php echo $this->lang->line('areyousure'); ?>?</label>
				                        </div>
				                        <div class="modal-footer">
				                            <hr>
				                            <button type="button" class="btn-choose-demand btn btn-success" demand-link=""><?php echo $this->lang->line('ikutidemand');?></button>
				                            <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('cancel');?></button>
				                        </div>
				                    </div>
				                </div>
				            </div> -->

				            <!-- Modal CONFRIM -->  
				            <div class="modal" style="margin-top: 12%; margin-left: 12%; margin-right: 12%;" id="modalconfrim" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
				                <div class="modal-dialog modal-sm">
				                    <div class="modal-content">
				                        <!-- <div class="modal-header">
				                            <h4 class="modal-title c-black">Pilih Template Kelas</h4>
				                            <hr>	                            
				                            <label class="f-14 m-b-15 c-black fg-line">Harap Memilih Tipe Tampilan kelas yang akan digunakan nanti.</label>
				                            <div class="col-sm-12 m-b-5">
				                                <select class="selectpicker" required id="template">
				                                    <option disabled selected>Pilih Template Private</option>
				                                    <option value="whiteboard_digital">Private Whiteboard Digital</option>
				                                    <option value="whiteboard_videoboard">Private Whiteboard VideoBoard</option>
				                                    <option value="whiteboard_no">Private No Whiteboard</option>
				                                </select>
				                            </div>	                            	                          	                            
				                        </div> -->                                                
				                        <div class="modal-footer">
				                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal"><?php echo $this->lang->line('cancel');?></button>
				                            <button type="button" class="btn-choose-demand btn bgm-green" id="confrimbro" demand-link=""><?php echo $this->lang->line('ikutidemand');?></button>
				                        </div>                        
				                    </div>
				                </div>
				            </div>

				            <!-- Modal REJECT -->    
				            <!-- <div class="modal fade" id="modalreject" tabindex="-1" role="dialog" aria-hidden="true">
				                <div class="modal-dialog modal-sm">
				                    <div class="modal-content">
				                        <div class="modal-body">    
				                        	                            
				                            <label class="f-20 m-t-20"><?php echo $this->lang->line('areyousure');?>?</label>
				                        </div>
				                        <div class="modal-footer">  
				                        	<hr>                              
				                            <button type="button" class="btn-reject-demand btn btn-success" demand-link=""><?php echo $this->lang->line('yes');?></button>
				                            <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('no');?></button>                                
				                        </div>
				                    </div>
				                </div>
				            </div> -->
				            <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="modalreject" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
				                <div class="modal-dialog modal-sm">
				                    <div class="modal-content">
				                        <div class="modal-header">
				                            <h4 class="modal-title c-black">Tolak Permintaan Kelas</h4>
				                            <hr>	                            
				                            <label class="f-14 c-black"><?php echo $this->lang->line('areyousure');?></label>
				                        </div>                                                
				                        <div class="modal-footer">
				                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal"><?php echo $this->lang->line('no');?></button>
				                            <button type="button" class="btn-reject-demand btn bgm-green" demand-link=""><?php echo $this->lang->line('yes');?></button>
				                        </div>                        
				                    </div>
				                </div>
				            </div>               
						</div>
					</div>
	            </section>
	           <section role="tabpanel" class="tab-pane" id="kotak_approval_group">
		           <div class="container"  id="content" ng-init="gettutorapproval_group()">
						<div style="">

							<div class="alert alert-success alert-dismissible text-center" role="alert" ng-if="firste">                                
					            <label><?php echo $this->lang->line('norequest');?></label>
					        </div> 

							<div class="col-sm-4 col-md-4" style="" ng-repeat="x in daturg">
								<div class="card c-gray">
									<div class="card-header bgm-blue" style=" height: 145px; border-radius: 5px 5px 0px 0px;">

										<img ng-click="" style="float:left; border-radius: 5px; " class="m-r-10" width="100" height="100" src="<?php echo "https://classmiles.com/";?>aset/img/user/{{x.id_user_requester}}.jpg">

										<h2 style="margin-left: 5px;">{{x.user_name}} <small>{{x.jenjang_level}} {{x.jenjang_name}} - {{x.subject_name}}</small>
										<small>{{x.date_requested | date:'EEEE, d MMMM y'}} {{x.time_requested}}</small>                            
										<small>Rp. {{x.harga_kelas}} | Duration {{x.duration_requested}} minutes</small></h2>

									</div>

									<div class="card-body card-padding" style="height:11vh; top:10px;">

										<div style="width:48%; float:left;">
											<button request_id="{{x.request_id}}" user_id="{{x.id_user_requester}}" class="btn-confirm-group btn btn-block btn-success btn-icon-text"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('approve');?></button><br><br>                                                                     
										</div>
										<div style="width:48%; float:left; margin-left:3%;">								
											<button request_id="{{x.request_id}}" class="btn-reject-group btn btn-block bgm-red btn-icon-text"><i class="zmdi zmdi-close"></i> <?php echo $this->lang->line('decline');?></button>
											
										</div>                                                
									</div>

								</div>
							</div>  

							<!-- Modal CONFRIM -->    
				            <!-- <div class="modal fade" id="modalconfrim" tabindex="-1" role="dialog" aria-hidden="true">
				                <div class="modal-dialog modal-sm">
				                    <div class="modal-content">
				                        <div class="modal-header">
				                            <h4 class="modal-title"><?php echo $this->lang->line('approve');?></h4>
				                            <hr>
				                        </div>
				                        <div class="modal-body">
				                            <center>
				                            <img id="gambar" style="border-radius: 1%;" class="m-b-5" width="90%" height="90%" alt=""><br>
				                            <label><h3><?php echo $this->session->userdata('nama_lengkap');?></h3></label><br>
				                            <label>18:00</label>
				                            <hr>
				                            <div class="dtp-container fg-line">                                                
				                                <select class='select2 form-control' required id="templategroup">
				                                    <option disabled selected>Pilih Template</option>
				                                    <option value="whiteboard_digital">Group Whiteboard Digital</option>
				                                    <option value="whiteboard_videoboard">Group Whiteboard VideoBoard</option>
				                                    <option value="whiteboard_no">Group No Whiteboard</option>
				                                </select>                                                             
				                            </div>
				                            </center>
				                            <label class="f-14"><?php echo $this->lang->line('areyousure'); ?>?</label>
				                        </div>
				                        <div class="modal-footer">
				                            <hr>
				                            <button type="button" class="btn-choose-demand-group btn btn-success" demand-link-group=""><?php echo $this->lang->line('ikutidemand');?></button>
				                            <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('cancel');?></button>
				                        </div>
				                    </div>
				                </div>
				            </div>  -->
				            <div class="modal" style="margin-top: 12%; margin-left: 12%; margin-right: 12%;" id="modalconfrim" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
				                <div class="modal-dialog modal-sm">
				                    <div class="modal-content">
				                        <div class="modal-header">
				                            <h4 class="modal-title c-black">Pilih Template Kelas</h4>
				                            <hr>	                            
				                            <label class="f-14 m-b-15 c-black fg-line">Harap Memilih Tipe Tampilan kelas yang akan digunakan nanti.</label>
				                            <div class="col-sm-12 m-b-5">
				                                <select class="selectpicker" required id="templategroup">
				                                    <option disabled selected>Pilih Template Group</option>
				                                    <option value="whiteboard_digital">Group Whiteboard Digital</option>
				                                    <option value="whiteboard_videoboard">Group Whiteboard VideoBoard</option>
				                                    <option value="whiteboard_no">Group No Whiteboard</option>
				                                </select>
				                            </div>	                            	                          	                            
				                        </div>                                                
				                        <div class="modal-footer">
				                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal"><?php echo $this->lang->line('cancel');?></button>
				                            <button type="button" class="btn-choose-demand-group btn bgm-green" id="confrimbro" demand-link-group=""><?php echo $this->lang->line('ikutidemand');?></button>
				                        </div>                        
				                    </div>
				                </div>
				            </div>    

				            <!-- Modal REJECT -->    
				           <!--  <div class="modal fade" id="modalreject" tabindex="-1" role="dialog" aria-hidden="true">
				                <div class="modal-dialog modal-sm">
				                    <div class="modal-content">
				                        <div class="modal-body">                                
				                            <label class="f-20 m-t-20"><?php echo $this->lang->line('areyousure');?>?</label>
				                        </div>
				                        <div class="modal-footer">  
				                        	<hr>                              
				                            <button type="button" class="btn-reject-demand-group btn btn-success" demand-link-group=""><?php echo $this->lang->line('yes');?></button>
				                            <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('no');?></button>                                
				                        </div>
				                    </div>
				                </div>
				            </div> --> 

				            <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="modalreject" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
				                <div class="modal-dialog modal-sm">
				                    <div class="modal-content">
				                        <div class="modal-header">
				                            <h4 class="modal-title c-black">Tolak Permintaan Kelas</h4>
				                            <hr>	                            
				                            <label class="f-14 c-black"><?php echo $this->lang->line('areyousure');?></label>
				                        </div>                                                
				                        <div class="modal-footer">
				                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal"><?php echo $this->lang->line('no');?></button>
				                            <button type="button" class="btn-reject-demand-group btn bgm-green" demand-link-group=""><?php echo $this->lang->line('yes');?></button>
				                        </div>                        
				                    </div>
				                </div>
				            </div>                 
						</div>
					</div>   
	            </section>
	        </div>
	    </div>	
</section>

<script type="text/javascript">
	
	$(document).ready(function(){

		$("#confrimbro").click(function(){			
			var template = $("#template").val();
			if (template=="") {				
			}
		});
	});
	$(".tabmenu").click(function() {
            // body...
            $("#modal_loading").modal('show');
        });
</script>