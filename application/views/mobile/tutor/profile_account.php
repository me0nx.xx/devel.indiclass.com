<style type="text/css">
    body{
        background-color: white;
    }
</style>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <section id="content">
        <div class="">
         <?php if(isset($rets['mes_alert'])){ ?>
                <div class="alert alert-<?php echo $rets['mes_alert']; ?>" style="display: <?php echo $rets['mes_display']; ?>">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <?php echo $rets['mes_message']; ?>
                </div>
        <?php } ?>         
        <div class="" style="">
            <div class="" id="profile-main">
                <header id="header" class="clearfix" style="background-color: #Cf000f;">
                    <div class="col-xs-1"  style="margin-top: 7%" >
                        <a href="<?php echo base_url('About'); ?>" id="back_home" type="button" class="loading c-white f-19" style="cursor: pointer;">
                        <i class="zmdi zmdi-arrow-left"></i>
                        </a>
                    </div>
                    <div class=" c-white f-17 col-xs-10" style="margin-top: 7%">
                        <center><?php echo $this->lang->line('settings'); ?></center>
                    </div>

                </header>
                <?php
                    $this->load->view('mobile/inc/sideprofile');
                ?><br><br>

                <div class="pm-body clearfix">
                    <?php 
                    $id_user = $this->session->userdata('id_user');
                    $v = $this->db->query("SELECT ts. * , tpt. * 
                        FROM tbl_profile_tutor AS tpt
                        INNER JOIN tbl_user AS ts ON tpt.tutor_id = ts.id_user
                        WHERE ts.id_user =  '$id_user'")->row_array();
                       $first_name = $v['first_name'];
                       $last_name = $v['last_name'];
                       $email_addr = $v['email'];
                       $user_birthplace = $v['user_birthplace'];
                       $user_birthdate = $v['user_birthdate'];
                       $user_ktp = $v['user_ktp'];
                       $user_callnum = $v['user_callnum'];
                       $user_gender = $v['user_gender'];
                       $user_religion = $v['user_religion'];
                       $id_provinsi = $v['id_provinsi'];
                       $id_kabupaten = $v['id_kabupaten'];
                       $id_kecamatan = $v['id_kecamatan'];
                       $id_desa = $v['id_desa'];
                       $kodepos = $v['kodepos'];
                       $user_address = $v['user_address'];
                    ?>
                    <form action="<?php echo base_url('/master/saveEditAccountTutor') ?>" method="post">
                    <div class="card-padding card-body">
                        <div class="col-sm-12" >  
                            <input type="hidden" name="id_user" required class="form-control" value="<?php echo $id_user; ?>">
                            <!-- <div class="alert alert-info" role="alert"><?php //echo $this->lang->line('tab_account'); ?></div> -->
                            
                            <!-- <div class="input-group fg-float">
                                <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                <div class="fg-line">
                                    <input type="text" name="first_name" required class="input-sm form-control fg-input" value="<?php echo $first_name; ?>">
                                    <label class="fg-label"><?php echo $this->lang->line('first_name'); ?></label>
                                </div>                                     
                            </div>

                            <div class="input-group fg-float">
                                <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                <div class="fg-line">
                                    <input type="text" name="last_name" required class="input-sm form-control fg-input" value="<?php echo $last_name; ?>">
                                    <label class="fg-label"><?php echo $this->lang->line('last_name'); ?></label>
                                </div>                                    
                            </div> -->

                            <div class="input-group fg-float">
                                <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                <div class="fg-line">
                                    <input type="text" name="first_name" required class="input-sm form-control fg-input" value="<?php echo $first_name; ?>">
                                    <label class="fg-label"><?php echo $this->lang->line('first_name'); ?></label>
                                </div> 
                            </div>

                            <br><br>

                            <div class="input-group fg-float">
                                <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                <div class="fg-line">
                                    <input type="text" name="last_name" required class="input-sm form-control fg-input" value="<?php echo $last_name; ?>">
                                    <label class="fg-label"><?php echo $this->lang->line('last_name'); ?></label>
                                </div>                                    
                            </div>      
                           

                            <br/><br/>

                            <div class="input-group fg-float">
                                <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                                <div class="fg-line" style="background: #DADFE1;">
                                    <input type="text" readonly="readonly"  required name="email" class="form-control" value="<?php echo $email_addr; ?>" style="cursor:not-allowed;">
                                    <label class="fg-label"><?php echo $this->lang->line('email_address'); ?></label>
                                </div>                                    
                            </div>

                            <br><br/>
                            <div class="input-group fg-float">
                                <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                <div class="fg-line">
                                    <input type="text"  maxlength="16" name="user_ktp" required class="input-sm form-control fg-input" value="<?php echo $user_ktp; ?>">
                                    <label class="fg-label"><?php echo $this->lang->line('numberid'); ?></label>
                                </div>                                    
                            </div> 
                            <br/><br/>
                            <div class="input-group fg-float">
                                <span class="input-group-addon"><i class="zmdi zmdi-pin"></i></span>
                                <div class="fg-line">
                                    <input type="text" name="user_birthplace" required class="input-sm form-control fg-input" value="<?php echo $user_birthplace; ?>">
                                    <label class="fg-label"><?php echo $this->lang->line('placeofbirth'); ?></label>
                                </div>                                    
                            </div>                                

                            <br/><br/>

                            <div class="input-group fg-float">
                                <span class="input-group-addon"><i class="zmdi zmdi-home"></i></span>
                                <div class="fg-line">
                                    <input type="text" name="user_address" required class="input-sm form-control fg-input" value="<?php echo $user_address; ?>">
                                    <label class="fg-label"><?php echo $this->lang->line('home_address'); ?></label>
                                </div>                                    
                            </div> 

                            <br/>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                <div class="col-sm-4 m-t-10">
                                    <div class="fg-line">
                                        <select name="tanggal_lahir" id="tanggal_lahir" class="select2 form-control">
                                                <option disabled selected><?php echo $this->lang->line('datebirth'); ?></option>
                                                <?php 
                                                if(isset($user_birthdate)){
                                                    $tgl = substr($user_birthdate, 8,2);
                                                    $bln = substr($user_birthdate, 5,2);
                                                    $thn = substr($user_birthdate, 0,4);
                                                }

                                                    for ($date=01; $date <= 31; $date++) {
                                                        echo '<option value="'.$date.'" ';
                                                        if(isset($user_birthdate) && $tgl == $date){ echo "selected='true'";}
                                                        echo '>'.$date.'</option>';
                                                    }
                                                ?>  
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 m-t-10">
                                    <div class="fg-line">
                                        <select name="bulan_lahir" class="select2 form-control">
                                                <option disabled selected><?php echo $this->lang->line('monthbirth'); ?></option>
                                                <option value="01" <?php if(isset($user_birthdate) && $bln == "01"){ echo "selected='true'";} ?>>January</option>
                                                <option value="02" <?php if(isset($user_birthdate) && $bln == "02"){ echo "selected='true'";} ?>>February</option>
                                                <option value="03" <?php if(isset($user_birthdate) && $bln == "03"){ echo "selected='true'";} ?>>March</option>
                                                <option value="04" <?php if(isset($user_birthdate) && $bln == "04"){ echo "selected='true'";} ?>>April</option>
                                                <option value="05" <?php if(isset($user_birthdate) && $bln == "05"){ echo "selected='true'";} ?>>May</option>
                                                <option value="06" <?php if(isset($user_birthdate) && $bln == "06"){ echo "selected='true'";} ?>>June</option>
                                                <option value="07" <?php if(isset($user_birthdate) && $bln == "07"){ echo "selected='true'";} ?>>July</option>
                                                <option value="08" <?php if(isset($user_birthdate) && $bln == "08"){ echo "selected='true'";} ?>>August</option>
                                                <option value="09" <?php if(isset($user_birthdate) && $bln == "09"){ echo "selected='true'";} ?>>September</option>
                                                <option value="10" <?php if(isset($user_birthdate) && $bln == "10"){ echo "selected='true'";} ?>>October</option>
                                                <option value="11" <?php if(isset($user_birthdate) && $bln == "11"){ echo "selected='true'";} ?>>November</option>
                                                <option value="12" <?php if(isset($user_birthdate) && $bln == "12"){ echo "selected='true'";} ?>>December</option>                                                            
                                            </select>
                                    </div>
                                    <!-- <span class="zmdi zmdi-triangle-down form-control-feedback"></span> -->
                                </div>
                                <div class="col-sm-4 m-t-10">                                   
                                    <div class="fg-line">
                                        <select name="tahun_lahir" id="tahun_lahir" class="select2 form-control">
                                            <option disabled selected><?php echo $this->lang->line('yearbirth'); ?></option>
                                            <?php 
                                                for ($i=1960; $i <= 2016; $i++) {                                                                            
                                                    echo '<option value="'.$i.'" ';
                                                    if(isset($user_birthdate) && $thn == $i){ echo "selected='true'";}
                                                    echo '>'.$i.'</option>';
                                                } 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                           

                            <div class="input-group fg-float">
                                <span class="input-group-addon"><i class="zmdi zmdi-phone"></i></span>

                                <div class="col-md-3 m-t-30">                  
                                    <div class="fg-line">

                                        <select required name="kode_area" class="select2 form-control">
                                        
                                        <?php
                                            $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                                            foreach ($allsub as $row => $d) {
                                                if($d['phonecode'] == $v['user_countrycode']){
                                                    echo "<option value='".$d['phonecode']."' selected='true'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                }
                                                if ($d['phonecode'] != $v['user_countrycode']) {
                                                    echo "<option value='".$d['phonecode']."'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                }                                                            
                                            }
                                        ?> 
                                        </select>
                                        <label class="fg-label"><?php echo $this->lang->line('countrycode'); ?></label>
                                    </div>
                                </div>
                                <div class="col-md-9 m-t-30">
                                    <div class="fg-line">
                                        <input type="text" onkeyup="validPhone(this)" onkeypress="return hanyaAngka(event)" name="user_callnum" id="user_callnum" minlength="9" maxlength="14" required class="input-sm form-control fg-input" value="<?php echo $user_callnum; ?>">
                                        <label class="fg-label"><?php echo $this->lang->line('mobile_phone'); ?></label>
                                    </div>
                                </div>                                         
                            </div> 

                            <br/>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-male-female"></i></span>
                                <div class="">
                                <?php 
                                        $flag = $user_gender;
                                    
                                ?>
                                    <select required name="user_gender" class="select2 form-control">
                                        <option disabled selected><?php echo $this->lang->line('select_gender'); ?></option>
                                        <option value="Male" <?php if($flag=="Male"){ echo "selected='true'";} ?> ><?php echo $this->lang->line('male');?></option>
                                        <option value="Female" <?php if($flag=="Female"){ echo "selected='true'";} ?> ><?php echo $this->lang->line('women');?></option>
                                    </select>
                                </div>
                            </div>

                            <br/>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-library"></i></span>
                                <div class="">
                                  <?php 
                                        $flag = $user_religion;
                                    
                                ?>
                                    <select name="user_religion" required class="select2 form-control">
                                        <option disabled selected><?php echo $this->lang->line('select_religion'); ?></option>
                                        <option value="Islam" <?php if($flag=="Islam"){ echo "selected='true'";} ?>><?php echo $this->lang->line('Islam'); ?></option>
                                        <option value="Kristen Protestan" <?php if($flag=="Kristen Protestan"){ echo "selected='true'";} ?>><?php echo $this->lang->line('Protestan'); ?></option>
                                        <option value="Kristen Katolik" <?php if($flag=="Kristen Katolik"){ echo "selected='true'";} ?>><?php echo $this->lang->line('Katolik'); ?></option>
                                        <option value="Hindu" <?php if($flag=="Hindu"){ echo "selected='true'";} ?>><?php echo $this->lang->line('Hindu'); ?></option>
                                        <option value="Buddha" <?php if($flag=="Buddha"){ echo "selected='true'";} ?>><?php echo $this->lang->line('Buddha'); ?></option>
                                        <option value="Konghucu" <?php if($flag=="Konghucu"){ echo "selected='true'";} ?>><?php echo $this->lang->line('Konghucu'); ?></option>
                                    </select>
                                </div>
                            </div>

                            <br>                              

                            <div class="input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-menu"></i></span>
                                <select id='provinsi_id' class="select2 form-control" name="id_provinsi" data-placeholder="<?php echo $this->lang->line('selectprovinsi'); ?>">
                                    <option value=""></option>
                                    <?php
                                        $prv = $this->Master_model->provinsi($id_provinsi);
                                        if($prv != ''){
                                            echo "<option value='".$prv['provinsi_id']."' selected='true'>".$prv['provinsi_name']."</option>";
                                            echo "<script>pfp = '".$prv['provinsi_id']."'</script>";
                                        }

                                    ?>
                                </select>
                            </div>

                            <br>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-menu"></i></span>
                              
                                <select id='kabupaten_id' required class="select2 form-control" name="id_kabupaten" data-placeholder="<?php echo $this->lang->line('selectkabupaten'); ?>">
                                    <option value=""></option>
                                    <?php
                                        $kbp = $this->Master_model->getAllKabupaten($id_kabupaten,$id_provinsi);
                                        if($kbp != ''){
                                            echo "<option value='".$kbp['kabupaten_id']."' selected='true'>".$kbp['kabupaten_name']."</option>";
                                            echo "<script>pfka = '".$kbp['kabupaten_id']."'</script>";
                                        }

                                    ?>
                                </select>
                            </div>

                            <br>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-menu"></i></span>
                                
                                <select id='kecamatan_id' required class="select2 form-control" name="id_kecamatan" data-placeholder="<?php echo $this->lang->line('selectkecamatan'); ?>">
                                    <option value=""></option>
                                    <?php
                                        $kcm = $this->Master_model->getAllKecamatan($id_kecamatan,$id_kabupaten);
                                        if($kcm != ''){
                                            echo "<option value='".$kcm['kecamatan_id']."' selected='true'>".$kcm['kecamatan_name']."</option>";
                                            echo "<script>pfkec = '".$kcm['kecamatan_id']."'</script>";

                                        }

                                    ?>
                                </select>
                            </div>

                            <br>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-menu"></i></span>
                                <select id='desa_id' class="select2 form-control" name="id_desa" data-placeholder="<?php echo $this->lang->line('selectkelurahan'); ?>">
                                    <option value=""></option>
                                    <?php
                                        $des = $this->Master_model->getAllKelurahan($id_desa,$id_kecamatan);
                                        if($des != ''){
                                            echo "<option value='".$des['desa_id']."' selected='true'>".$des['desa_name']."</option>";
                                        }

                                    ?>
                                </select>
                            </div>

                            <br/><br/>                                                          

                            <div class="input-group fg-float">
                                <span class="input-group-addon"><i class="zmdi zmdi-email-open"></i></span>
                                <div class="fg-line">
                                    <input type="text" style=" margin-left: 5px;" id="kodepos" readonly="readonly"  name="kodepos" class="input-sm form-control fg-input" value="<?php echo $kodepos; ?>">
                                    <label class="fg-label"><?php echo $this->lang->line('postalcode'); ?></label>
                                </div>                                    
                            </div>                                 
                                
                            <br><br> 
                            <blockquote class="m-b-25">
                                <p><?php echo $this->lang->line('tab_cp'); ?></p>
                            </blockquote>                                                            
                            <!-- <div class="alert alert-danger bgm-teal" role="alert"><?php //echo $this->lang->line('tab_cp'); ?></div> -->

                            <div class="input-group fg-float">
                                <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                                <div class="fg-line">
                                    <input type="password" name="oldpass" id="oldpass" class="input-sm form-control fg-input">
                                    <label class="fg-label"><?php echo $this->lang->line('old_pass'); ?></label>
                                </div>                                    
                            </div>

                            <br>

                            <div class="input-group fg-float">
                                <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                                <div class="fg-line">
                                    <input type="password" name="newpass" id="newpass" class="input-sm form-control fg-input">
                                    <label class="fg-label"><?php echo $this->lang->line('new_pass'); ?></label>                                        
                                </div>
                                <small class="help-block" id="divPassword" style="display: none;"></small>                                    
                            </div>
                            
                            <br>    

                            <div class="input-group fg-float">
                                <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                                <div class="fg-line">
                                    <input type="password" name="conpass" id="conpass" class="input-sm form-control fg-input" ">                                    
                                    <label class="fg-label"><?php echo $this->lang->line('confirm_pass'); ?></label>
                                </div>
                                <small class="help-block" id="divCheckPasswordMatch" style="display: none;"></small>
                            </div>                               

                            <br>                                                                                                        
                            <br><br>
                        </div>                                                    

                        <div class="card-padding card-body">
                            <div class="col-xs-12">                                                                          
                                <button type="submit" name="simpanedit" id="simpanedit" class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); 
                                ?></button>                                        
                            </div> 
                        </div>
                    </div>
                    </form>

                </div>
            </div>
        </div>

        </div><!-- akhir container -->
    </section>            
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">
     $(document).ready(function(){
     // $('#user_callnum').keyboard({type:'numpad'});
});

</script>
<script type="text/javascript">

    function checkPasswordMatch() {
        var oldpass = $('#oldpass').val();
        var password = $("#newpass").val();
        var confirmPassword = $("#conpass").val();
        if (oldpass != '' && password == '') {
            $("#divPassword").css('display','block');
            $("#divPassword").html("<p style='color:red'><?php echo $this->lang->line('noempty'); ?></p>");
            $('#simpanedit').attr('disabled','');  
        }
        else if(oldpass == ''){
            $("#divPassword").html("<p style='color:green'></p>");
            $('#simpanedit').removeAttr('disabled');
            $("#divPassword").css('display','none');
        }
        else if(oldpass != '' && password != ''){
            $("#divPassword").html("<p style='color:green'></p>");
            $('#simpanedit').removeAttr('disabled');
            $("#divPassword").css('display','none');
        }
        else if(confirmPassword == '')
        {
            $("#divCheckPasswordMatch").html("<p style='color:green'></p>");
            $('#simpanedit').removeAttr('disabled');
            $("#divCheckPasswordMatch").css('display','none');
        }
        else if (password != confirmPassword)
        {
            $("#divCheckPasswordMatch").css('display','block');
            $("#divCheckPasswordMatch").html("<p style='color:red'><?php echo $this->lang->line('passwordnotmatch'); ?></p>");   
            $('#simpanedit').attr('disabled','');
        }
        else
        {
            $("#divCheckPasswordMatch").html("<p style='color:green'></p>");
            $('#simpanedit').removeAttr('disabled');
            $("#divCheckPasswordMatch").css('display','none');
        }       
    }

    $(document).ready(function () {
       $("#conpass").keyup(checkPasswordMatch);
    });
    $(document).ready(function () {
       $("#oldpass").keyup(checkPasswordMatch);
    });
    $(document).ready(function () {
       $("#newpass").keyup(checkPasswordMatch);
    });
</script>
<script type="text/javascript">
   function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }
     function validPhone(a)
    {
        if(!/(^0$)|(^[1-9]\d{0,12}$)/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
</script>