<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:url"           content="https://www.classmiles.com" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="Classmiles" />
        <meta property="og:description"   content="Fitur belajar online interaktif yang lengkap serta banyak pilihan guru atau tutor handal dan menyenangkan" />
        <meta property="og:image"         content="<?php echo base_url();?>aset/img/playstore-icon.png" />
        <title>Classmiles</title>

        <?php
            $iduser = $this->session->userdata('id_user');
            $querycek = $this->db->query("SELECT count(*) as jumlah FROM tbl_notif WHERE id_user='$iduser' AND read_status=0")->row_array();
            $jumlahnotif = $querycek['jumlah'];
        ?>
        <link rel="icon" href="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL ?>/<?php if ($jumlahnotif > 0){ echo 'icon.ico'; } else { echo 'icon.ico'; } ?>">
        <!-- CSS -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500">        
        <link rel="stylesheet" href="../aset/channel/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../aset/channel/typicons/typicons.min.css">
        <link rel="stylesheet" href="../aset/channel/css/animate.css">
		<link rel="stylesheet" href="../aset/channel/css/form-elements.css">
        <link rel="stylesheet" href="../aset/channel/css/style.css">
        <link rel="stylesheet" href="../aset/channel/css/media-queries.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/typicons/2.0.9/typicons.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="../aset/channel/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../aset/channel/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../aset/channel/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../aset/channel/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../aset/channel/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body style="background-color: rgba(108, 122, 137, 0.7);">
    
        <!-- Loader -->
    	<div class="loader">
    		<div class="loader-img"></div>
    	</div>
				
        <!-- Top content -->
        <div class="top-content">
        	
        	<!-- Top menu -->
			<nav class="navbar navbar-inverse navbar-no-bg" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button> -->
						<a class="navbar-brand" href="#" style="width: 33%; margin-top: -2%;">
                            <img src="https://cdn.classmiles.com/sccontent/baru/classmiles_logo_channel.png">
                        </a> 
                        <a class="navbar-brand navbar-nav navbar-right" href="#" style="width: 33%; margin-top: -2%; margin-left: 30%;">
                            <img src="https://cdn.classmiles.com/usercontent/Y2hhbm5lbC8wNDRfbFZaN2xLVEJJT1p6YkFRQUcuanBn" style="height: 35px; width: 35px;">
                        </a>                        
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="top-navbar-1">
						<ul class="nav navbar-nav navbar-right">							
							<li><img src="https://cdn.classmiles.com/usercontent/Y2hhbm5lbC8wNDRfbFZaN2xLVEJJT1p6YkFRQUcuanBn" style="width: 80%; height: 80%; margin-top: 10%;"></li>
						</ul>
					</div>
				</div>
			</nav>
        	
            <div class="inner-bg" style="margin-top: -4%;">
                <div class="container">
                    <div class="row">                        
                        <div class="col-sm-5 col-xs-12 form-box wow fadeInUp">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Puri Kids</h3>
                            		<p>Please login to your account.</p>
                        		</div>
                        		<div class="form-top-right">
                        			<span aria-hidden="true" class="typcn typcn-arrow-forward"></span>
                        		</div>                                
                            </div>
                            <div class="form-bottom">
                                <div class="alert alert-danger" style="display: none; ?>" id="alertwrong">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                    <?php echo $this->lang->line('wrongpassword'); ?>
                                </div>
                                <div class="alert alert-danger" style="display: none; ?>" id="alertemailwrong">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                    Email Facebook anda tidak mengizinkan!!!
                                </div>
                                <div class="alert alert-danger" style="display: none; ?>" id="alertfailedregister">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                    <?php echo $this->lang->line('failed');?> Mendaftar 
                                </div>
                                <div class="alert alert-success" style="display: none; ?>" id="alertverifikasisuccess">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                    <?php echo $this->lang->line('activationsuccess');?> 
                                </div>
                                <div class="alert alert-success" style="display: none; ?>" id="alertverifikasisuccesstutor">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                    <?php echo $this->lang->line('activationsuccess').' '.$this->lang->line('activationtutor');?> 
                                </div>
                                <div class="alert alert-danger" style="display: none; ?>" id="alertverifikasifailed">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                    <?php echo $this->lang->line('alreadyactivation');?> 
                                </div>
                                <div class="alert alert-warning" style="display: none; ?>" id="alertverifikasiemail">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                    <?php 
                                        $load = $this->lang->line('youremail3');
                                        $load2 = $this->lang->line('here');
                                        $load3 = $this->lang->line('emailresend');
                                        echo $load."<a href=".base_url('/Master/resendEmaill')."> <label style='color:#008080; cursor:pointer;'>".$load2."</label></a> ".$load3;
                                    ?> 
                                </div>
                                <div class="alert alert-success" style="display: none; ?>" id="alertsuccessendemail">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                    <?php echo $this->lang->line('successfullysent');?>
                                </div>
                                <div class="alert alert-danger" style="display: none; ?>" id="alertfailedsendemail">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                    <?php echo $this->lang->line('failed');?>
                                </div>
                                <div class="alert alert-warning" style="display: none; ?>" id="alertverifikasitutor">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                    <?php echo $this->lang->line('youremail2');?>
                                </div>

                                <div class="alert alert-danger" style="display: none; ?>" id="alertfailedchangepassword">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                    <?php echo $this->lang->line('failedchange');?>
                                </div>
                                <div class="alert alert-warning" style="display: none; ?>" id="alertsuccesschangepassword">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" >&times;</a>
                                    <?php echo $this->lang->line('successfullypassword');?>
                                </div>
			                    <form role="form" action="<?php echo base_url('Master/aksi_login'); ?>" method="post">
                                    <label style="color: #95A5A6; font-weight: normal;">Enter your email</label>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-email">Email</label>
			                        	<input type="text" name="email" id="emaill" placeholder="Email..." class="form-email form-control" id="form-email">
			                        </div>
                                    <label style="color: #95A5A6; font-weight: normal;">Enter your password</label>
                                    <div class="form-group">
                                        <label class="sr-only" for="form-first-name">Password</label>
                                        <input type="password" name="kata_sandi" placeholder="Password..." class="form-first-name form-control" id="form-first-name" style="height: 50px;margin: 0;padding: 0 20px;vertical-align: middle;background: #f8f8f8;border: 1px solid #ddd;font-family: 'Roboto', sans-serif;font-size: 16px;font-weight: 300;line-height: 50px;color: #888;-moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;-moz-box-shadow: 0 1px 0 0 #fff; -webkit-box-shadow: 0 1px 0 0 #fff; box-shadow: 0 1px 0 0 #fff;-o-transition: all .3s; -moz-transition: all .3s; -webkit-transition: all .3s; -ms-transition: all .3s; transition: all .3s;">
                                    </div>
                                    <input type="text" name="user_utc" id="user_utc" hidden>
                                    <input type="text" name="bapuks" value="" id="bapuks" hidden>
                                    <input type="text" name="redirect" id="redirect" value="channel" hidden>
                                    <input type="text" name="channel_id" value="<?php echo $_GET['ch'];?>" id="channel_id" hidden>
                                       
			                        <button type="submit" class="btn btn-block">Login</button>			                        
			                    </form>
		                    </div>
                        </div>
                        <div class="col-sm-7 col-xs-12 text">
                            <h1 class="wow fadeInLeftBig">Classmiles - <strong>Classroom in Your Hand</strong></h1>
                            <div class="description wow fadeInLeftBig">
                                <p>
                                    Classmiles menyediakan layanan pembelajaran online dimana di dalamnya berisi banyak ruang kelas virtual.
                                    Classmiles memungkinkan komunikasi interaktif antara guru dan siswa serta antar siswa.
                                </p>
                            </div>
                            <div class="top-big-link wow fadeInUp">
                                <a href="https://play.google.com/store/search?q=classmiles.com" target="_blank" class="wow  fadeInUp animation-delay-7" style="visibility: visible; animation-name: fadeInUp;"> <img id="img_android" style="" src="https://cdn.classmiles.com/sccontent/banner/android-02.png"></a>                            
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="backstretch" id="idbackk" style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px; z-index: -999998; position: absolute; display: block; height: 1069px; width: 360px;">
            	<img src="https://cdn.classmiles.com/sccontent/baru/BG_Channel1.jpg" id="bgChannel" style="position: absolute; margin: 0px; padding: 0px; border: none; max-height: none; max-width: none; z-index: -999999; left: -63.137px; top: 0px; width: 1596px;">
            </div>
            
        </div>
        
        <!-- MODAL: Privacy policy -->
        <div class="modal fade" id="modal-privacy" tabindex="-1" role="dialog" aria-labelledby="modal-privacy-label" aria-hidden="true">
        	<div class="modal-dialog">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal">
        					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
        				</button>
        				<h2 class="modal-title" id="modal-privacy-label">Privacy policy</h2>
        			</div>
        			<div class="modal-body">
        				<h3>1. Dolor sit amet</h3>
        				<p>
	                    	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
	                    	labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
	                    </p>
	                    <ul>
	                    	<li>Easy To Use</li>
	                    	<li>Awesome Design</li>
	                    	<li>Cloud Based</li>
	                    </ul>
	                    <p>
	                    	Ut wisi enim ad minim veniam, <a href="#">quis nostrud exerci tation</a> ullamcorper suscipit lobortis nisl ut aliquip ex ea 
	                    	commodo consequat nostrud tation.
	                    </p>
	                    <h3>2. Sed do eiusmod</h3>
	                    <p>
	                    	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
	                    	labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
	                    </p>
	                    <h3>3. Nostrud exerci tation</h3>
	                    <p>
	                    	Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea 
	                    	commodo consequat nostrud tation.
	                    </p>
	                    <p>
	                    	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
	                    	labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
	                    </p>
        			</div>
        			<div class="modal-footer">
        				<button type="button" class="btn" data-dismiss="modal">Close</button>
        			</div>
        		</div>
        	</div>
        </div>

        <!-- Javascript -->
        <script src="../aset/channel/js/jquery-1.11.1.min.js"></script>
        <script src="../aset/channel/bootstrap/js/bootstrap.min.js"></script>
        <script src="../aset/channel/js/jquery.backstretch.min.js"></script>
        <script src="../aset/channel/js/wow.min.js"></script>
        <script src="../aset/channel/js/retina-1.1.0.min.js"></script>
        <script src="../aset/channel/js/scripts.js"></script>
        <script type="text/javascript">
            var height = $(window).height();
            var width  = $(window).width();            
            $("#idbackk").css('width',width);   
            $("#bgChannel").css('width',width+60);
        </script>
        <script type="text/javascript">
            var user_utc = new Date().getTimezoneOffset();
            user_utc = -1 * user_utc;
            $("#user_utc").val(user_utc);

            $(document).ready(function(){
                var code = null;
                var cekk = setInterval(function(){
                    code = "<?php echo $this->session->userdata('code');?>";
                    cek();
                },500);
                // alert('aasdfsd');
                function cek(){
                    if (code == "101") {
                        $("#login_modal").modal('show');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                         $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alertwrong").css('display','block');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else if (code == "102") {
                        $("#login_modal").modal('show');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                         $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alertwrong").css('display','none');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else if (code == "105") {
                        $("#login_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                         $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alertemailwrong").css('display','block');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else if (code == "106") {
                        $("#login_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertemailwrong").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                         $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alertfailedregister").css('display','block');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else if (code == "107") {
                        $("#login_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alertverifikasisuccess").css('display','block');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else if (code == "108") {
                        $("#login_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alertverifikasifailed").css('display','block');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else if (code == "109") {
                        $("#login_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else if (code == "110") {
                        $("#login_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                        $("#alertverifikasisuccesstutor").css('display','block');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else if (code == "121") {
                        $("#login_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alertverifikasisuccess").css('display','block');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }                
                    else if (code =="189")
                    {
                        $("#login_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alertverifikasiemail").css('display','block');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else if (code =="188")
                    {
                        $("#login_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                        $("#alertverifikasiemail").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alertfailedsendemail").css('display','block');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else if (code =="187")
                    {
                        $("#login_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                        $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alertsuccessendemail").css('display','block');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else if (code =="290")
                    {
                        $("#login_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                        $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasitutor").css('display','block');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else if (code =="301")
                    {
                        $("#login_modal").modal('hide');
                        $("#forgot_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                        $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasitutor").css('display','none');                    
                        $("#alert_successforgot").css('display','none');
                        $("#alert_failedforgot").css('display','block');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else if (code =="302")
                    {
                        $("#login_modal").modal('hide');
                        $("#forgot_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                        $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alert_failedforgot").css('display','none');
                        $("#alert_successforgot").css('display','block');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else if (code =="303")
                    {
                        $("#login_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                        $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertsuccesschangepassword").css('display','none');
                        $("#alertfailedchangepassword").css('display','block');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else if (code =="304")
                    {
                        $("#login_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                        $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasitutor").css('display','none');                    
                        $("#alertfailedchangepassword").css('display','none');
                        $("#alertsuccesschangepassword").css('display','block');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                     else if (code =="800")
                    {
                        $("#login_modal").modal('show');
                        $("#alertwrong").css('display','none');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertverifikasisuccess").css('display','none');
                        $("#alertverifikasiemail").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        code == null;
                        clearInterval(cekk);
                        $.ajax({
                            url: '<?php echo base_url(); ?>Rest/clearsession',
                            type: 'POST',
                            data: {
                                code: code
                            },
                            success: function(response)
                            { 
                                console.warn(response);
                            }
                        });
                    }
                    else
                    {
                        $("#login_modal").modal('hide');
                        $("#alertemailwrong").css('display','none');
                        $("#alertfailedregister").css('display','none');
                        $("#alertfailedsendemail").css('display','none');
                        $("#alertsuccessendemail").css('display','none');
                        $("#alertverifikasiemail").css('display','none');
                        $("#alertverifikasifailed").css('display','none');
                        $("#alertverifikasitutor").css('display','none');
                        $("#alertwrong").css('display','none');
                        clearInterval(cekk);
                    }
                    console.warn(code);
                }
            });
        </script>
        <!--[if lt IE 10]>
            <script src="../aset/channel/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>