<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style=" z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sideadmin'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>List Point Channel</h2>
                <!-- <ul class="actions hidden-xs">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><?php echo $this->lang->line('accountlist'); ?></li>
                    </ol>                    
                </ul> -->
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="card">  
                <div class="card-header">
                    <div class="pull-left"><h2>List</h2></div>
                    <div class="pull-right"><a data-toggle="modal" href="#topup"><button class="btn btn-success">+ Topup Point</button></a></div>
                    <br><br>
                </div>              
                <div class="card-body card-padding">
                    <div class="row"> 
                        <div class="table-responsive">
                            <br>
                            <table id="list" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="bgm-teal c-white" style="width: 10px">No</th>
                                        <th class="bgm-teal c-white" style="width: 15px">Channel Name</th>
                                        <th class="bgm-teal c-white" style="width: 20px">Point Active</th>
                                        <th class="bgm-teal c-white" style="width: 30px">Channel Email</th>
                                        <th class="bgm-teal c-white" style="width: 30px">Channel Phone</th>                                        
                                    </tr>
                                </thead>
                                <tbody id="box_listpoint">
                                           
                                </tbody>
                            </table> 
                        </div>                           
                    </div>                                               
                </div>                                             
            </div> 

            <!-- Modal TAMBAH -->  
            <div class="modal" id="topup" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Topup Point</h4>
                            <hr>
                        </div>                        
                        <div class="modal-body">
                            <div class="card">
                            <div class="row p-15">   

                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Channel Name</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <div class="select">
                                                    <select required name="channelname" id="channelname" class="selectpicker" data-live-search="true" style="z-index: 10;">
                                                    <option disabled selected>Pilih Channel</option>   
                                                    <?php
                                                       $allchannel = $this->db->query("SELECT uc.*, mc.* FROM uangpoint_channel as uc INNER JOIN master_channel as mc ON uc.channel_id=mc.channel_id")->result_array();
                                                       foreach ($allchannel as $row => $v) {
                                                            echo '<option value="'.$v['channel_id'].'">'.$v['channel_name'].'</option>';
                                                        }
                                                    ?>                                                    
                                                    </select>
                                                </div>

                                            </div>                                                
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Amount Point</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <input type="text" name="amountpoint" id="amountpoint" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" maxlength="5" /> 
                                                <label class="c-red" id="numberonly" style="display: none;">Please enter number only</label>   
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn bgm-green" id="savetopup">Topup</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>                              

            <!-- Modal DELETE -->  
            <div class="modal fade" id="modalDelete" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Hapus Akun Bank</h4>
                            <hr>
                            <p><label class="c-gray f-15">Apakah anda yakin ingin menghapus</label></p>
                            <input type="text" name="id_rekeningg" id="id_rekeningg" class="c-black" value=""/>
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" id="hapusdatarekening">Ya</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>

        </div>
    </section>

</section>
<script type="text/javascript">

    // $('#simpandatarekening').click(function(){
    //     alert('test');
    // });
    $(document).ready(function(){
        // var channelid = "<?php echo $this->session->userdata('channel_id');?>";
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        $.ajax({
            url: '<?php echo base_url(); ?>Rest/getListPointChannel/access_token/'+tokenjwt,
            type: 'POST',
            success: function(response)
            {   
                var a = JSON.stringify(response);
                var jsonPretty = JSON.stringify(JSON.parse(a),null,2);                      
                // $("#aaa").text(jsonPretty);  
                // alert(response['data'][1]['class_id']);

                if (response['data'] == null) 
                {                                                
                }                   
                else
                {
                    var no = 0;
                    if (response.data.length != null) {
                        for (var i = response.data.length-1; i >=0;i--) {

                            var channel_name          = response['data'][i]['channel_name'];
                            var active_point          = response['data'][i]['active_point'];
                            var channel_email         = response['data'][i]['channel_email'];
                            var channel_number        = response['data'][i]['channel_country_code']+" "+response['data'][i]['channel_callnum'];                              
                            no += 1;

                            var kotak = "<tr><td>"+no+"</td><td>"+channel_name+"</td><td>"+active_point+"</td><td>"+channel_email+"</td><td>"+channel_number+"</td></tr>";
                            
                            $("#list tbody").append(kotak);
                            $("#list" ).DataTable();
                        }
                    }
                }
            }
        });

        $("#savetopup").click(function(){
            var channelid = $("#channelname").val();
            var amountpoint = $("#amountpoint").val();
            
            $.ajax({
                url: '<?php echo base_url(); ?>Rest/topupPointChannel/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    channel_id: channelid,
                    amountpoint: amountpoint
                },
                success: function(response)
                {
                    var stat = response['code'];
                    if (stat == 200) {
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Topup point success");
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                    else
                    {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Failed topup point");
                        setTimeout(function(){
                            location.reload();
                        },2000);
                    }
                }
            });
        });

        $('#amountpoint').on('keypress',function(e){
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                $("#numberonly").css('display', 'block');
                return false;
            }
        });
    });
</script>
