<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');

	?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('inc/sideadmin'); ?>
		<?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
		?>
	</aside>

	<section id="content">
		<div class="container">            
			<div class="block-header">
				<h2><?php echo $this->lang->line('homeadminn');?></h2>

				<ul class="actions hidden-xs">
					<li>
						<ol class="breadcrumb">
							<li><a href="#"><?php echo $this->lang->line('homeadminn'); ?></a></li>
							<li class="active"><?php echo $this->lang->line('aktivasiuser'); ?> </li>
						</ol>
					</li>
				</ul>
			</div> <!-- akhir block header    -->        		
			<div class="card">
				
				<div class="card-header">
					<h2><?php echo $this->lang->line('aktivasiuser'); ?></h2>
				</div>
				
				<?php 
				foreach ($alldatatutor as $row => $v) {
					?>
					<div class="col-sm-4 m-t-20" >
						<div class="card profile-tutor" id="card_<?php echo $v['id_user']; ?>"  style="cursor: hand; cursor: pointer;">
							<!-- data-toggle="modal" href="#modalDefault" -->
							<div class="pv-header" data-toggle="modal" id="modalWider">
								<img src="<?php echo base_url('aset/img/user/'.$v['user_image']); ?>" class="pv-main" alt="">
							</div>

							<div class="pv-body">
								<h2><?php echo $v['user_name']; ?></h2>
							</div>
						</div>
					</div>
					<?php
				}
				?>

			</div>			

			<!-- Modal Default -->	
			<div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">														
							<div class="pull-left">
								<h3 class="modal-title">Profile Tutor</h3>
							</div>
							<div class="pull-right">
								<!-- <button disabled="disable" class="bgm-orange c-white" ><i class="zmdi zmdi-face-add"></i> <?php //echo $this->lang->line('followed'); ?></button> -->
								<!--<button class="btn bgm-deeporange"><i class="zmdi zmdi-face-add"></i>Unfollow</button>-->
								<button type="button" class="btn bgm-gray" data-dismiss="modal">X</button>
							</div>							
							<hr style="margin-top:6%;">
							
						</div>
						<div class="modal-body">
							<div class="col-sm-12">
								<div class="col-sm-4">
									<img id="modal_image" src="<?php echo base_url('aset/img/user/47.jpg') ?>" width="95%" height="180%" alt="">
								</div>
								<div class="col-sm-8" style="margin-top:-20px;">
									<!-- <h3><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('summary'); ?></h3>
									<label id="modal_self_desc"></label> -->
									<h3><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('basic_info'); ?></h3>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('full_name'); ?></dt>
										<dd id="modal_user_name"></dd>
									</dl>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('placeofbirth'); ?></dt>
										<dd id="modal_birthplace"></dd>
									</dl> 
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('birthday'); ?></dt>
										<dd id="modal_birthdate"></dd>
									</dl>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('age'); ?></dt>
										<dd id="modal_age"></dd>
									</dl>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('mobile_phone'); ?></dt>
										<dd id="modal_callnum"></dd>
									</dl>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('gender'); ?></dt>
										<dd id="modal_gender"></dd>
									</dl>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('religion'); ?></dt>
										<dd id="modal_religion"></dd>
									</dl>
								</div>
							</div>

							<div class="col-sm-12 table-responsive">
								<br><br>
								<h3><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('latbelpend'); ?></h3>
								<table class="table table-bordered table-hover">
									<thead>
										<th><?php echo $this->lang->line('educational_level'); ?></th>
										<th><?php echo $this->lang->line('educational_competence'); ?></th>
										<th><?php echo $this->lang->line('educational_institution'); ?></th>
										<th><?php echo $this->lang->line('institution_address'); ?></th>
										<th><?php echo $this->lang->line('graduation_year'); ?></th>
									</thead>
									<tbody id="modal_eduback">
										
									</tbody>
								</table>

								<br/>
								<h3><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('pengalaman_mengajar'); ?></h3>
								<table class="table table-bordered table-hover">
									<thead>
										<th><?php echo $this->lang->line('description_experience'); ?></th>
										<th colspan="2"><?php echo $this->lang->line('year_experience'); ?></th>
										<th><?php echo $this->lang->line('place_experience'); ?></th>
										<th><?php echo $this->lang->line('information_experience'); ?></th>
									</thead>
									<tbody id="modal_teachexp">
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tbody>
								</table>
							</div>

						</div>
						<br>
						<div class="modal-footer m-r-30" >
							<button type="button" id="" class="btn btn-danger modal_decline" style="margin-top:40px;"><?php echo $this->lang->line('decline'); ?></button>
							<button type="button" id="" class="btn btn-primary modal_approve" style="margin-top:40px;"><?php echo $this->lang->line('approve'); ?></button>
						</div>    				
					</div>
				</div>
			</div>
		</section>
	</section>

	<footer id="footer">
		<?php $this->load->view('inc/footer'); ?>
	</footer>
	<script type="text/javascript">
		$('.card.profile-tutor').click(function(){
			var ids = $(this).attr('id');
			ids = ids.substr(5,10);
			$.get('<?php echo base_url(); ?>first/getTutorProfile/'+ids,function(hasil){
				hasil = JSON.parse(hasil);
				$('.modal_approve').attr('id',ids);
				$('.modal_decline').attr('id',ids);
				$('#modal_image').attr('src','<?php echo base_url(); ?>aset/img/user/'+hasil['user_image']);
				$('#modal_self_desc').html(hasil['self_description']);
				$('#modal_user_name').html(hasil['user_name']);
				$('#modal_birthplace').html(hasil['user_birthplace']);
				$('#modal_birthdate').html(hasil['user_birthdate']);
				$('#modal_age').html(hasil['user_age']);
				$('#modal_callnum').html(hasil['user_callnum']);
				$('#modal_gender').html(hasil['user_gender']);
				$('#modal_religion').html(hasil['user_religion']);

				$('#modal_eduback').html('');
				$('#modal_teachexp').html('');
				if(hasil['education_background'] != ''){
					for (var i = 0; i<hasil['education_background'].length; i++) {
						$('#modal_eduback').append('<tr>'+
							'<td>'+hasil['education_background'][i]['educational_level']+'</td>'+
							'<td>'+hasil['education_background'][i]['educational_competence']+'</td>'+
							'<td>'+hasil['education_background'][i]['educational_institution']+'</td>'+
							'<td>'+hasil['education_background'][i]['institution_address']+'</td>'+
							'<td>'+hasil['education_background'][i]['graduation_year']+'</td>'+
							'</tr>');
					}
				}
				if(hasil['teaching_experience'] != ''){
					for (var i = 0; i<hasil['teaching_experience'].length; i++) {
						$('#modal_teachexp').append('<tr>'+
							'<td>'+hasil['teaching_experience'][i]['description_experience']+'</td>'+
							'<td>'+hasil['teaching_experience'][i]['year_experience1']+'</td>'+
							'<td>'+hasil['teaching_experience'][i]['year_experience2']+'</td>'+
							'<td>'+hasil['teaching_experience'][i]['place_experience']+'</td>'+
							'<td>'+hasil['teaching_experience'][i]['information_experience']+'</td>'+
											// '<td>'+hasil['teaching_experience'][i]['graduation_year']+'</td>'+
											'</tr>');
					}
				}
				$('#modalDefault').modal('show');
			});
			$('.modal_approve').click(function(){
				var newids = $(this).attr('id');
				$.get('<?php echo base_url(); ?>first/approveTheTutor/'+newids,function(s){
					location.reload();
				});
			});
			$('.modal_decline').click(function(){
				var newids = $(this).attr('id');
				$.get('<?php echo base_url(); ?>first/declineTheTutor/'+newids,function(s){
					location.reload();
				});
			});


		});
	</script>