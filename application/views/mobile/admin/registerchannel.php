<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sideadmin'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>Register</h2>
                <!-- <ul class="actions hidden-xs">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><?php echo $this->lang->line('accountlist'); ?></li>
                    </ol>                    
                </ul> -->
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>            

            <div class="card m-t-20 p-20" style="">
                <div class="card-header">
                    <div class="pull-left"><h2>List Channel</h2></div>
                    <div class="pull-right"><a data-toggle="modal" href="#modaladdstudent"><button class="btn btn-success">+ Tambah Channel</button></a></div>
                <br><br>
                <hr>

                <div class="row" style="margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <?php
                                    $select_all = $this->db->query("SELECT * FROM master_channel")->result_array();
                                    ?>
                                    <tr>
                                        <th data-column-id="no">No</th>
                                        <th data-column-id="name">Nama</th>
                                        <th data-column-id="email">Email</th>
                                        <th data-column-id="notelp">No Telp</th>                                        
                                        <th data-column-id="alamat">Alamat</th>
                                        <th data-column-id="aksi" style="text-align: right;" >Action</th>
                                        <th data-column-id="aksi" style="text-align: left;" ></th>

                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;
                                    foreach ($select_all as $row => $v) {
                                        
                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['channel_name']; ?></td>
                                            <td><?php echo($v['channel_email']); ?></td>
                                            <td><?php echo($v['channel_country_code'].' '.$v['channel_callnum']); ?></td>                                            
                                            <td><?php echo($v['channel_address']); ?></td>
                                            <td>
                                                <a href="<?php echo base_url(); ?>Admin/EditChannel?id=<?php echo $v['channel_id'] ?>"><button class="btn bgm-bluegray" title="Edit Channel"><i class="zmdi zmdi-edit"></i></button></a>
                                                
                                            </td>
                                            <td>
                                                <a data-toggle="modal" rtp="<?php echo $this->encryption->encrypt($v['channel_id']); ?>" class="hapuschannel" data-target-color="green" href="#modalDelete"><button class="btn bgm-bluegray" title="Hapus Channel"><i class="zmdi zmdi-delete"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div> 
                                            
            </div> 

            <!-- Modal TAMBAH -->  
            <div class="modal" id="modaladdstudent" tabindex="-1" role="dialog" style="top: 13%;" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bgm-cyan">
                            <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                            <h4 class="modal-title c-white">Tambah Akun</h4>
                        </div>
                        <div class="modal-body m-t-20">
                            <form method="post" action="<?php echo base_url('Process/add_channel'); ?>" enctype="multipart/form-data">
                            <div class="row p-15">    
                                
                                <div class="col-sm-12 col-md-12 col-xs-12">                                                    
                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <input type="text" name="channel_name" required class="input-sm form-control fg-input">
                                            <label class="fg-label">Nama Channel</label>
                                        </div>
                                    </div>
                                </div>                                

                                <div class="col-sm-12 m-t-5">                                                                                           
                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <input type="email" name="channel_email" required class="input-sm form-control fg-input">
                                            <label class="fg-label">Email</label>
                                        </div>
                                    </div>
                                </div>  

                                <div class="col-sm-12 m-t-5">                                                                                           
                                    <div class="form-group fg-float">
                                        <div class="fg-line">                                            
                                            <textarea rows="4" name="channel_address" required class="input-sm form-control fg-input"></textarea>
                                            <label class="fg-label">Alamat</label>
                                        </div>
                                    </div>
                                </div>                                
                                
                                <div class="col-sm-5 m-t-10">
                                    <div class="fg-line">                                                       
                                        <select required name="channel_country_code" class="select2 form-control">
                                            <?php
                                                $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                                                foreach ($allsub as $row => $d) {
                                                    if($d['phonecode'] == $alluser['user_countrycode']){
                                                        echo "<option value='".$d['phonecode']."' selected='true'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                    }
                                                    if ($d['phonecode'] != $alluser['user_countrycode']) {
                                                        echo "<option value='".$d['phonecode']."'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                    }                                                            
                                                }
                                            ?>                                        
                                        </select>
                                    </div>
                                    <!-- <span class="zmdi zmdi-flag form-control-feedback"></span> -->
                                </div>
                                <div class="col-sm-7 m-t-10">
                                    <div class="fg-line">
                                        <input type="text" id="no_hape" minlength="9" maxlength="13" name="channel_callnum" required class="form-control " placeholder="No Handphone">
                                    </div>
                                </div>
                                <div class="col-md-12 m-t-20">
                                    <button class="btn btn-success btn-block">Simpan</button>
                                </div>
                            </div>
                            </form>
                            <br>
                        </div>
                    </div>
                </div>
            </div>                             

            <!-- Modal DELETE -->  
            <div class="modal fade" id="modalDelete" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Hapus Akun Channel</h4>
                            <hr>
                            <p><label class="c-gray f-15">Apakah anda yakin ingin menghapus</label></p>                            
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" data-id="" id="hapusdatachannel" rtp="">Ya</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>



        </div>
    </section>

</section>

<script type="text/javascript">

    // $('#simpandatarekening').click(function(){
    //     alert('test');
    // });
    $(document).ready(function(){

        $('#savetutor').click(function(e){
            
            var idtage = $("#tagorang").val()+ '';
            // alert(idtage);
            var channelid = "1";
            // alert(channelid);
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/addstudentchannel",
                type:"post",
                data: {
                    channel_id: channelid,
                    id_user: idtage
                },
                success: function(html){             
                    alert("Berhasil Menambah Siswa");
                    $('#modalTambah').modal('hide');
                    window.location.reload();                  
                } 
            });
         });


        $('#hapusdatachannel').click(function(e){
            var rtp = $(this).attr('rtp');
            // alert(id_user);
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/deletechannel",
                type:"post",
                data: {
                    rtp: rtp
                },
                success: function(html){             
                    // alert("Berhasil menghapus data siswa");
                    $('#modalDelete').modal('hide');
                    location.reload();                     
                } 
            });
        });
    });
    $(document).on("click", ".hapuschannel", function () {
         var myBookId = $(this).attr('rtp');
         $('#hapusdatachannel').attr('rtp',myBookId);

    });   
    $('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable(); 
</script>

