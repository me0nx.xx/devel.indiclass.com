<style type="text/css">
    .hover11 img {
        opacity: 1;     
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .hover11:hover img {
        opacity: 0.5;
    }
</style>
<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sideadmin'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>Subject Add</h2>
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>            

            <div class="card m-t-20 p-20" style="">
                <div class="card-header">
                    <div class="pull-left"><h2>List Subject</h2></div>
                    <div class="pull-right"><a data-toggle="modal" href="#modaladdsubject"><button class="btn btn-success">+ Tambah Subject</button></a></div>
                <br><br>
                <hr>

                <div class="row" style="margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <?php
                                    $select_all = $this->db->query("SELECT ms.*, mj.* FROM master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id;")->result_array();
                                    ?>
                                    <tr>
                                        <th data-column-id="no">No</th>
                                        <th data-column-id="name">Subject Nama</th>                                        
                                        <th data-column-id="jenjang">Jenjang</th>  
                                        <th data-column-id="name">Indonesia</th>
                                        <th data-column-id="name">Inggris</th>                                      
                                        <!-- <th data-column-id="iconweb">Icon Web</th>                                         -->
                                        <th data-column-id="iconandroid">Icon</th>
                                        <th data-column-id="aksi" style="text-align: right;" >Action</th>
                                        <th data-column-id="aksi" style="text-align: left;" ></th>

                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;
                                    foreach ($select_all as $row => $v) {
                                        
                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['subject_name']; ?></td>
                                            <td><?php echo($v['jenjang_level'].' '.$v['jenjang_name']); ?></td>
                                            <td><?php echo $v['indonesia']; ?></td>
                                            <td><?php echo $v['english']; ?></td>
                                            <!-- <td class="hover11">
                                                <?php if (empty($v['iconweb'])) {
                                                ?>
                                                <a href="#" class="pop"><img src="<?php echo base_url('aset/img/no_image.png').'?'.time(); ?>" width="100px" height="100px"></a>
                                                <?php 
                                                }
                                                else
                                                {
                                                    ?>
                                                    <a href="#" class="pop"><img src="<?php echo base_url('aset/img/class_icon/'.$v['iconweb']).'?'.time(); ?>" width="100px" height="100px"></a>
                                                    <?php
                                                    }
                                                ?> 
                                            </td> -->                                           
                                            <td>
                                                <?php if (empty($v['icon'])) {
                                                ?>
                                                <a href="#" class=""><img src="<?php echo base_url('aset/img/no_image.png').'?'.time(); ?>" width="60px" height="60px"></a>
                                                <?php 
                                                }
                                                else
                                                {
                                                    ?>
                                                    <a href="#" class=""><img src="<?php echo base_url('aset/img/class_icon/'.$v['icon'].'?'.time()); ?>" width="50px" height="50px"></a>
                                                    <?php
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url(); ?>Admin/SubjectEdit?id=<?php echo $v['subject_id'] ?>"><button class="btn bgm-bluegray" title="Edit Subject"><i class="zmdi zmdi-edit"></i></button></a>
                                                
                                            </td>
                                            <td>
                                                <a data-toggle="modal" rtp="<?php echo $v['subject_id'] ?>" class="hapussubject" data-target-color="green" href="#modalDelete"><button class="btn bgm-bluegray" title="Hapus Subject"><i class="zmdi zmdi-delete"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div> 
                                            
            </div> 

            <!-- Modal TAMBAH -->  
            <div class="modal" id="modaladdsubject" tabindex="-1" role="dialog" style="top: 13%;" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bgm-cyan">
                            <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                            <h4 class="modal-title c-white">Tambah Subject</h4>
                        </div>
                        <div class="modal-body m-t-20">
                            <form method="post" action="<?php echo base_url('Admin/add_subject'); ?>" enctype="multipart/form-data">
                            <div class="row p-15">    
                                
                                <div class="col-sm-12 col-md-12 col-xs-12">                                                    
                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <input type="text" name="subject_name" required class="input-sm form-control fg-input">
                                            <label class="fg-label">Nama Subject</label>
                                        </div>
                                    </div>
                                </div>                                

                                <div class="col-md-12 m-t-5">        
                                <label class="fg-label c-gray">Jenjang</label>                                                                                   
                                    <div class="form-group fg-float">                                    
                                        <div class="fg-line">
                                            <select required name="jenjang_id" class="select2 col-md-6 input-sm fg-input form-control" style="width: 100%;">
                                            <?php
                                                $allsub = $this->db->query("SELECT * FROM master_jenjang ORDER BY jenjang_level ASC")->result_array();
                                                foreach ($allsub as $row => $d) {
                                                    
                                                        echo "<option value='".$d['jenjang_id']."'>".$d['jenjang_level']." ".$d['jenjang_name']."</option>";               
                                                }
                                            ?> 
                                            </select>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12 col-xs-12">                                                    
                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <input type="text" name="subject_name_indonesia" required class="input-sm form-control fg-input">
                                            <label class="fg-label">Nama Subject Indonesia</label>
                                        </div>
                                    </div>
                                </div> 

                                <div class="col-sm-12 col-md-12 col-xs-12">                                                    
                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <input type="text" name="subject_name_inggris" required class="input-sm form-control fg-input">
                                            <label class="fg-label">Nama Subject English</label>
                                        </div>
                                    </div>
                                </div>   
                                
                                <div class="col-md-12 m-t-5"> 
<!--                                     <div class="col-md-6 m-t-5">    
                                        <label class="fg-label">Icon Web</label>                                              
                                        <div class="form-group fg-float">
                                            <div class="fg-line">                                            
                                                <input type="file" required name="iconweb" accept="image/*">
                                                
                                            </div>
                                        </div>
                                    </div> -->
                                    <!-- <div class="col-md-6 m-t-5">  -->
                                        <label class="fg-label">Icon Android</label>                                                    
                                        <div class="form-group fg-float">
                                            <div class="fg-line">                                            
                                                <input type="file" required name="iconandroid" accept="image/*">
                                                
                                            </div>
                                        </div>
                                    <!-- </div> -->
                                </div>                                

                                <div class="col-md-12 m-t-20">
                                    <button class="btn btn-success btn-block">Simpan</button>
                                </div>
                            </div>
                            </form>                            

                            <br>
                        </div>
                    </div>
                </div>
            </div>                             

            <!-- Modal DELETE -->  
            <div class="modal fade" id="modalDelete" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Hapus Subject</h4>
                            <hr>
                            <p><label class="c-gray f-15">Apakah anda yakin ingin menghapus</label></p>                            
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" data-id="" id="hapusdatasubject" rtp="">Ya</button>                            
                        </div>                        
                    </div>
                </div>
            </div>

            <div class="modal" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 8%;">
                <div class="modal-dialog">
                    <div class="modal-content">              
                        <div class="modal-body">
                            <br><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><br><br><br>
                            <img src="" class="imagepreview" style="width: 150px; height: 150px;" ><br><br><br>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal" id="imagemodalandroid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 8%;">
                <div class="modal-dialog">
                    <div class="modal-content">              
                        <div class="modal-body">
                            <br><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><br><br><br>
                            <img src="" class="imagepreviewandroid" style="width: 150px; height: 150px;" ><br><br><br>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

</section>

<script type="text/javascript">

    // $('#simpandatarekening').click(function(){
    //     alert('test');
    // });
    $(document).ready(function(){

        $('.pop').on('click', function() {
            $('.imagepreview').attr('src', $(this).find('img').attr('src'));
            $('#imagemodal').modal('show');   
        });

        $('.pop').on('click', function() {
            $('.imagepreviewandroid').attr('src', $(this).find('img').attr('src'));
            $('#imagemodalandroid').modal('show');   
        });

        $('#hapusdatasubject').click(function(e){
            var rtp = $(this).attr('rtp');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/deletesubject",
                type:"POST",
                data: {
                    rtp: rtp
                },
                success: function(html){             
                    // alert("Berhasil menghapus data siswa");
                    $('#modalDelete').modal('hide');
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Berhasil Menghapus data Subject");
                    setTimeout(function(){
                        location.reload(); 
                    },2000);
                                        
                } 
            });
        });
    });
    $(document).on("click", ".hapussubject", function () {
         var myBookId = $(this).attr('rtp');
         $('#hapusdatasubject').attr('rtp',myBookId);

    });   
    $('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable(); 

   
</script>

