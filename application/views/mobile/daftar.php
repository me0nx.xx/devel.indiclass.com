<!--Body-->
<body class="space-top" ng-controller="ModelController as modelapi">

  	<!--Page Preloading-->
    <div id="preloader"><div id="spinner"></div></div>
    
    <!--Login Modal-->
    <div class="light-skin modal fade" id="loginModal" tabindex="-1" role="form" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Login here</h4>
                </div>
                <div class="modal-body">
                    <!-- <form class="login-form" role="form" method="POST" action="<?php echo base_url();?>First/login"> -->
                    <div class="alert alert-danger alert-dismissable" id="alertWrong" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Wrong email or password.
                    </div>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Enter Username" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required>
                    </div>
                  
                    <a id="button_login"><button type="submit"  class="btn btn-primary">Login</button></a>                        
                  
                    <!-- </form> -->
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <!--Header-->
    <header class="stiky-header">
        <div class="container group">
            <a class="logo" href="<?php echo base_url();?>"><img src="https://cdn.classmiles.com/usercontent/Y2hhbm5lbC8wNDRfbFZaN2xLVEJJT1p6YkFRQUcuanBn" alt="Channel" style="margin-bottom: 2%;" /></a>
            <a class="logo-mobile" href="<?php echo base_url();?>"><img src="https://cdn.classmiles.com/usercontent/Y2hhbm5lbC8wNDRfbFZaN2xLVEJJT1p6YkFRQUcuanBn" alt="Channel"/></a>
            <div class="navi-toggle group">
                <span class="dot"></span><span class="line"></span>
                <span class="dot"></span><span class="line"></span>
                <span class="dot"></span><span class="line"></span>
                <span class="dot"></span><span class="line"></span>
            </div>
            <a class="btn btn-primary btn-login" href="<?php echo base_url();?>" title="Kembali" ><i class="fa fa-home"></i></a>
            <nav class="main-navi">
            <ul style="display: none;">
              	<li class="active"><a class="scroll-up" href="#">Home</a></li>
              	<li><a class="scroll" href="#features">Class</a></li>
              	<li><a class="scroll" href="#gallery">Tutor</a></li>            
            </ul>
            </nav>
        </div>
    </header>
    <section class="" id="slider">
        <center>
            <div class="row page-header">
                <br><br>
                <span>Mendafar sebagai : </span>
            </div>
            <div class="row features nav-tabs">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <a href="#clean-code" data-toggle="tab">
                        <div class="feature-img"><img src="https://cdn.classmiles.com/sccontent/baru/logo_baru/SD%20Sederajat.png"></div>
                        <h3>Orang tua siswa sekolah dasar (SD)</h3>
                    </a>                    
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <a href="#font-icons" data-toggle="tab">
                        <div class="feature-img"><img src="https://cdn.classmiles.com/sccontent/baru/logo_baru/SMP-SMA.png"></div>
                        <h3>Siswa Smp/Sma</h3>
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <a href="#more" data-toggle="tab"> 
                        <div class="feature-img"><img src="https://cdn.classmiles.com/sccontent/baru/logo_baru/Mahasiswa-Umum.png"></div>
                        <h3>Umum</h3>
                    </a>                   
                </div>
            </div>
            
            <!-- Tab panes -->
            <div class="container" style="margin-top: -5%;">
                <div class="row tab-content central">
                    
                    <div class="tab-pane fade in active col-lg-12" id="clean-code">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="row space-bottom">
                                    <div class="col-lg-12 col-md-12">
                                        <h3>MENDAFTAR SEBAGAI ORANG TUA SISWA SEKOLAH DASAR (SD)</h3>
                                    </div>                                    
                                </div>
                                <div class="row">
                                	<input type="text" class="form-control" name="type_regis_sd" id="type_regis_sd" style="display: none;" placeholder="type" value="ortu">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-8">                                      
                                        <div class="col-lg-6 col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="sr-only" for="name1">Name</label>                                                
                                                <input type="text" class="form-control" name="name_depan_sd" id="name_depan_sdd" placeholder="Nama Depan">
                                                <label style="color: red; float: left; display: none;" id="alert_nama_depan_sd">Nama depan Mohon diisi</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="sr-only" for="name1">Name</label>
                                                <input type="text" class="form-control" name="name_belakang_sd" id="name_belakang_sd" placeholder="Nama Belakang">
                                                <label style="color: red; float: left; display: none;" id="alert_name_belakang_sd">Nama belakang Mohon diisi</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="sr-only" for="email1">Email</label>
                                                <input type="email" class="form-control" name="email_sd" id="email_sd" placeholder="Email">
                                                <label style="color: red; float: left; display: none;" id="alert_email_sd">Email Mohon diisi</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="sr-only" for="message">Kata Sandi</label>
                                                <input type="password" class="form-control" name="password_sd" id="password_sd" placeholder="Kata Sandi">
                                                <label style="color: red; float: left; display: none;" id="alert_password_sd">Kata sandi Mohon diisi</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="sr-only" for="message">Konfirmasi Kata Sandi</label>
                                                <input type="password" class="form-control" name="password_cek_sd" id="password_cek_sd" placeholder="Konfirmasi Kata Sandi">
                                                <label style="color: red; float: left; display: none;" id="alert_password_cek_sd">Kata sandi tidak sama</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <label style="float: left;">Tanggal Lahir :</label>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-4 col-md-4 col-xs-4" style="padding: 0;">
                                                <div class="form-group">
                                                    <label class="sr-only" for="message">Tanggal</label>
                                                    <select class="form-control" id="tanggal_sd" name="tangal_sd">
                                                        <option disabled selected  value=''>Pilih Tanggal</option>
                                                        <?php 
                                                            for ($date=01; $date <= 31; $date++) {
                                                                ?>                                                  
                                                                <option value="<?php echo $date ?>"><?php echo $date ?></option>                                                  
                                                                <?php 
                                                            }
                                                        ?>
                                                    </select>
                                                    <label style="color: red; float: left; display: none;" id="alert_tanggal_sd">Tanggal Mohon dipilih</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-xs-4">
                                                <div class="form-group">
                                                    <label class="sr-only" for="message">Bulan</label>
                                                    <select id="bulan_sd" required name="bulan_sd" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                        <option disabled selected value=''>Pilih Bulan</option>
                                                        <option value="01">Januari</option>
                                                        <option value="02">Februari</option>
                                                        <option value="03">Maret</option>
                                                        <option value="04">April</option>
                                                        <option value="05">Mei</option>
                                                        <option value="06">Juni</option>
                                                        <option value="07">Juli</option>
                                                        <option value="08">Agustus</option>
                                                        <option value="09">September</option>
                                                        <option value="10">Oktober</option>
                                                        <option value="11">November</option>
                                                        <option value="12">Desember</option>
                                                    </select>
                                                    <label style="color: red; float: left; display: none;" id="alert_bulan_sd">Bulan Mohon dipilih</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-xs-4" style="padding: 0;">
                                                <div class="form-group">
                                                    <label class="sr-only" for="message">Tahun</label>
                                                    <select id="tahun_sd" required name="tahun_sd" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                        <option disabled selected value=''>Pilih Tahun</option>
                                                        <?php 
                                                        for ($i=1960; $i <= 2018; $i++) {
                                                            ?>                                                 
                                                              <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                            <?php 
                                                        } 
                                                        ?>
                                                    </select>
                                                    <label style="color: red; float: left; display: none;" id="alert_tahun_sd">Tahun Mohon dipilih</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <label style="float: left;">Jenis Kelamin :</label>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <select id="jenis_kelamin_sd" required name="jenis_kelamin_sd" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                    <option disabled selected value=''>Pilih Jenis Kelamin</option>
                                                    <option value="Male">Pria</option>
                                                    <option value="Female">Wanita</option>
                                                </select>
                                                <label style="color: red; float: left; display: none;" id="alert_jenis_kelamin_sd">Jenis kelamin Mohon dipilih</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <label style="float: left;">Kewarganegaraan :</label>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <select id="nama_negara_sd" required name="nama_negara_sd" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                    <option disabled selected value=''>Pilih Negara</option>
                                                </select>
                                                <label style="color: red; float: left; display: none;" id="alert_nama_negara_sd">Negara Mohon dipilih</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <label style="float: left;">Nomor Handphone :</label>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-4 col-md-4 col-xs-4" style="padding: 0;">
                                                <div class="form-group">
                                                    <select id="kode_area_sd" required name="kode_area_sd" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                    </select>
                                                    <label style="color: red; float: left; display: none;"  id="alert_kode_area_sd">Kode area Mohon dipilih</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-xs-8" style="padding-right: 0;">
                                                <input type="text" class="form-control" name="no_hape_sd" id="no_hape_sd" placeholder="87xxxxxxxxx">
                                                <label style="color: red; float: left; display: none;" id="alert_no_hape_sd">No Handphone Mohon diisi</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <button type="button" style="margin: 0;" class="btn btn-primary btn-block" id="daftar_sd">Daftar</button>
                                        </div>
                                        <!-- Validation Response -->
                                        <div class="response-holder"></div>
                                    </div>
                                    <div class="col-lg-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade col-lg-12" id="font-icons">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="row space-bottom">
                                    <div class="col-lg-12 col-md-12">
                                        <h3>MENDAFTAR SEBAGAI SISWA SMP / SMA</h3>
                                    </div>                                    
                                </div>
                                <div class="row">
                                	<input type="text" class="form-control" name="type_regis_sma" id="type_regis_sma" style="display: none;" placeholder="type" value="sma">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-8">                                      
                                        <div class="col-lg-6 col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="sr-only" for="name1">Name</label>
                                                <input type="text" class="form-control" name="name_depan_sma" id="name_depan_sma" placeholder="Nama Depan">
                                                <label style="color: red; float: left; display: none;" id="alert_nama_depan_sma">Nama depan Mohon diisi</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="sr-only" for="name1">Name</label>
                                                <input type="text" class="form-control" name="name_belakang_sma" id="name_belakang_sma" placeholder="Nama Belakang">
                                                <label style="color: red; float: left; display: none;" id="alert_name_belakang_sma">Nama belakang Mohon diisi</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="sr-only" for="email1">Email</label>
                                                <input type="email" class="form-control" name="email_sma" id="email_sma" placeholder="Email">
                                                <label style="color: red; float: left; display: none;" id="alert_email_sma">Email Mohon diisi</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="sr-only" for="message">Kata Sandi</label>
                                                <input type="password" class="form-control" name="password_sma" id="password_sma" placeholder="Kata Sandi">
                                                <label style="color: red; float: left; display: none;" id="alert_password_sma">Kata sandi Mohon diisi</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="sr-only" for="message">Konfirmasi Kata Sandi</label>
                                                <input type="password" class="form-control" name="password_cek_sma" id="password_cek_sma" placeholder="Konfirmasi Kata Sandi">
                                                <label style="color: red; float: left; display: none;" id="alert_password_cek_sma">Kata sandi tidak sama</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <label style="float: left;">Tanggal Lahir :</label>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-4 col-md-4 col-xs-4" style="padding: 0;">
                                                <div class="form-group">
                                                    <label class="sr-only" for="message">Konfirmasi Kata Sandi</label>
                                                    <select class="form-control" id="tanggal_sma" name="tangal_sma">
                                                        <option disabled selected  value=''>Pilih Tanggal</option>
                                                        <?php 
                                                            for ($date=01; $date <= 31; $date++) {
                                                                ?>                                                  
                                                                <option value="<?php echo $date ?>"><?php echo $date ?></option>                                                  
                                                                <?php 
                                                            }
                                                        ?>
                                                    </select>
                                                    <label style="color: red; float: left; display: none;" id="alert_tanggal_sma">Tanggal Mohon dipilih</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-xs-4">
                                                <div class="form-group">
                                                    <label class="sr-only" for="message">Konfirmasi Kata Sandi</label>
                                                    <select id="bulan_sma" required name="bulan_sma" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                        <option disabled selected value=''>Pilih Bulan</option>
                                                        <option value="01">Januari</option>
                                                        <option value="02">Februari</option>
                                                        <option value="03">Maret</option>
                                                        <option value="04">April</option>
                                                        <option value="05">Mei</option>
                                                        <option value="06">Juni</option>
                                                        <option value="07">Juli</option>
                                                        <option value="08">Agustus</option>
                                                        <option value="09">September</option>
                                                        <option value="10">Oktober</option>
                                                        <option value="11">November</option>
                                                        <option value="12">Desember</option>
                                                    </select>
                                                    <label style="color: red; float: left; display: none;" id="alert_bulan_sma">Bulan Mohon dipilih</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-xs-4" style="padding: 0;">
                                                <div class="form-group">
                                                    <label class="sr-only" for="message">Konfirmasi Kata Sandi</label>
                                                    <select id="tahun_sma" required name="tahun_sma" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                        <option disabled selected value=''>Pilih Tahun</option>
                                                        <?php 
                                                        for ($i=1960; $i <= 2018; $i++) {
                                                            ?>                                                 
                                                              <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                            <?php 
                                                        } 
                                                        ?>
                                                    </select>
                                                    <label style="color: red; float: left; display: none;" id="alert_tahun_sma">Tahun Mohon dipilih</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <label style="float: left;">Jenis Kelamin :</label>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <select id="jenis_kelamin_sma" required name="jenis_kelamin_sma" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                    <option disabled selected value=''>Pilih Jenis Kelamin</option>
                                                    <option value="Male">Pria</option>
                                                    <option value="Female">Wanita</option>
                                                </select>
                                                <label style="color: red; float: left; display: none;" id="alert_jenis_kelamin_sma">Jenis kelamin Mohon dipilih</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <label style="float: left;">Kewarganegaraan :</label>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <select id="nama_negara_sma" required name="nama_negara_sma" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                    <option disabled selected value=''>Pilih Negara</option>
                                                </select>
                                                <label style="color: red; float: left; display: none;" id="alert_nama_negara_sma">Negara Mohon dipilih</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <label style="float: left;">Nomor Handphone :</label>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-4 col-md-4 col-xs-4" style="padding: 0;">
                                                <div class="form-group">
                                                    <select id="kode_area_sma" required name="kode_area_sma" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                    </select>
                                                    <label style="color: red; float: left; display: none;"  id="alert_kode_area_sma">Kode area Mohon dipilih</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-xs-8" style="padding-right: 0;">
                                                <input type="text" class="form-control" name="no_hape_sma" id="no_hape_sma" placeholder="87xxxxxxxxx">
                                                <label style="color: red; float: left; display: none;" id="alert_no_hape_sma">No Handphone Mohon diisi</label>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <label style="float: left;">Jenjang :</label>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <select id="jenjang_sma" required name="jenjang_sma" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                    <option disabled selected value=''>Pilih Jenjang</option>
                                                </select>
                                                <label style="color: red; float: left; display: none;" id="alert_jenjang_sma">Jenjang Mohon dipilih</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <button type="button" style="margin: 0;" class="btn btn-primary btn-block" id="daftar_sma">Daftar</button>
                                        </div>
                                        <!-- Validation Response -->
                                        <div class="response-holder"></div>
                                    </div>
                                    <div class="col-lg-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane fade col-lg-12" id="more">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="row space-bottom">
                                    <div class="col-lg-12 col-md-12">
                                        <h3>MENDAFTAR SEBAGAI UMUM</h3>
                                    </div>                                    
                                </div>
                                <div class="row">
                                	<input type="text" class="form-control" name="type_regis_umum" id="type_regis_umum" style="display: none;" placeholder="type" value="umum">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-8">                                      
                                        <div class="col-lg-6 col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="sr-only" for="name1">Name</label>
                                                <input type="text" class="form-control" name="name_depan_umum" id="name_depan_umum" placeholder="Nama Depan">
                                                <label style="color: red; float: left; display: none;" id="alert_nama_depan_umum">Nama depan Mohon diisi</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="sr-only" for="name1">Name</label>
                                                <input type="text" class="form-control" name="name_belakang_umum" id="name_belakang_umum" placeholder="Nama Belakang">
                                                <label style="color: red; float: left; display: none;" id="alert_name_belakang_umum">Nama belakang Mohon diisi</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="sr-only" for="email1">Email</label>
                                                <input type="email" class="form-control" name="email_umum" id="email_umum" placeholder="Email">
                                                <label style="color: red; float: left; display: none;" id="alert_email_umum">Email Mohon diisi</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="sr-only" for="message">Kata Sandi</label>
                                                <input type="password" class="form-control" name="password_umum" id="password_umum" placeholder="Kata Sandi">
                                                <label style="color: red; float: left; display: none;" id="alert_password_umum">Kata sandi Mohon diisi</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="sr-only" for="message">Konfirmasi Kata Sandi</label>
                                                <input type="password" class="form-control" name="password_cek_umum" id="password_cek_umum" placeholder="Konfirmasi Kata Sandi">
                                                <label style="color: red; float: left; display: none;" id="alert_password_cek_umum">Kata sandi tidak sama</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <label style="float: left;">Tanggal Lahir :</label>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-4 col-md-4 col-xs-4" style="padding: 0;">
                                                <div class="form-group">
                                                    <label class="sr-only" for="message">Konfirmasi Kata Sandi</label>
                                                    <select class="form-control" id="tanggal_umum" name="tangal_umum">
                                                        <option disabled selected  value=''>Pilih Tanggal</option>
                                                        <?php 
                                                            for ($date=01; $date <= 31; $date++) {
                                                                ?>                                                  
                                                                <option value="<?php echo $date ?>"><?php echo $date ?></option>                                                  
                                                                <?php 
                                                            }
                                                        ?>
                                                    </select>
                                                    <label style="color: red; float: left; display: none;" id="alert_tanggal_umum">Tanggal Mohon dipilih</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-xs-4">
                                                <div class="form-group">
                                                    <label class="sr-only" for="message">Konfirmasi Kata Sandi</label>
                                                    <select id="bulan_umum" required name="bulan_umum" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                        <option disabled selected value=''>Pilih Bulan</option>
                                                        <option value="01">Januari</option>
                                                        <option value="02">Februari</option>
                                                        <option value="03">Maret</option>
                                                        <option value="04">April</option>
                                                        <option value="05">Mei</option>
                                                        <option value="06">Juni</option>
                                                        <option value="07">Juli</option>
                                                        <option value="08">Agustus</option>
                                                        <option value="09">September</option>
                                                        <option value="10">Oktober</option>
                                                        <option value="11">November</option>
                                                        <option value="12">Desember</option>
                                                    </select>
                                                    <label style="color: red; float: left; display: none;" id="alert_bulan_umum">Bulan Mohon dipilih</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-xs-4" style="padding: 0;">
                                                <div class="form-group">
                                                    <label class="sr-only" for="message">Konfirmasi Kata Sandi</label>
                                                    <select id="tahun_umum" required name="tahun_umum" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                        <option disabled selected value=''>Pilih Tahun</option>
                                                        <?php 
                                                        for ($i=1960; $i <= 2018; $i++) {
                                                            ?>                                                 
                                                              <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                            <?php 
                                                        } 
                                                        ?>
                                                    </select>
                                                    <label style="color: red; float: left; display: none;" id="alert_tahun_umum">Tahun Mohon dipilih</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <label style="float: left;">Jenis Kelamin :</label>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <select id="jenis_kelamin_umum" required name="jenis_kelamin_umum" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                    <option disabled selected value=''>Pilih Jenis Kelamin</option>
                                                    <option value="Male">Pria</option>
                                                    <option value="Female">Wanita</option>
                                                </select>
                                                <label style="color: red; float: left; display: none;" id="alert_jenis_kelamin_umum">Jenis kelamin Mohon dipilih</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <label style="float: left;">Kewarganegaraan :</label>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="form-group">
                                                <select id="nama_negara_umum" required name="nama_negara_umum" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                    <option disabled selected value=''>Pilih Negara</option>
                                                </select>
                                                <label style="color: red; float: left; display: none;" id="alert_nama_negara_umum">Negara Mohon dipilih</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <label style="float: left;">Nomor Handphone :</label>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="col-lg-4 col-md-4 col-xs-4" style="padding: 0;">
                                                <div class="form-group">
                                                    <select id="kode_area_umum" required name="kode_area_umum" class="select2 form-control" data-live-search="true" style="width: 100%;">
                                                    </select>
                                                    <label style="color: red; float: left; display: none;"  id="alert_kode_area_umum">Kode area Mohon dipilih</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-xs-8" style="padding-right: 0;">
                                                <input type="text" class="form-control" name="no_hape_umum" id="no_hape_umum" placeholder="87xxxxxxxxx">
                                                <label style="color: red; float: left; display: none;" id="alert_no_hape_umum">No Handphone Mohon diisi</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <button type="button" style="margin: 0;" class="btn btn-primary btn-block" id="daftar_umum">Daftar</button>
                                        </div>
                                        <!-- Validation Response -->
                                        <div class="response-holder"></div>
                                    </div>
                                    <div class="col-lg-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <br><br>
        </center>
    </section>
    
    <div class="modal" tabindex="-1" id="alert_modal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="alert_title_modal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="alert_message_modal"></p>
                </div>          
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" id="modal_cekumur" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="alert_title_modal">Peringatan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="alertumur"></p>
                </div>          
            </div>
        </div>
    </div>

    <!--Footer-->
    <footer class="footer" style="background-color: #6C7A89; bottom: 0;">
        <div class="row">
        	<div class="col-lg-12">
          	<p class="copyright">&copy; 2018 Puri Kids. All Rights Reserved.</p>
          </div>
        </div>
      </div>
    </footer>
    
    <!--Scroll To Top Button-->
    <div id="scroll-top" class="scroll-up"><i class="fa fa-arrow-up"></i></div>
    
    <!--Javascript (jQuery) Libraries and Plugins-->
    <!-- <script src="assets/js/libs/jquery-1.11.2.min.js"></script> -->
    <script src="https://classmiles.com/aset/vendors/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="assets/js/libs/jquery.easing.1.3.js"></script>
    <script src="assets/js/plugins/bootstrap.min.js"></script>
    <script src="assets/js/plugins/jquery.touchSwipe.min.js"></script>
    <script src="assets/js/plugins/jquery.placeholder.js"></script>
    <script src="assets/js/plugins/icheck.min.js"></script>
    <script src="assets/js/plugins/jquery.validate.min.js"></script>
    <script src="assets/js/plugins/gallery.js"></script>
    <script src="assets/js/plugins/jquery.fitvids.js"></script>
    <script src="assets/js/plugins/jquery.bxslider.min.js"></script>
    <!-- <script src="assets/js/plugins/chart.js"></script> -->
    <script src="assets/js/plugins/waypoints.min.js"></script>
    <script src="assets/js/plugins/smoothscroll.js"></script>
    <script src="assets/js/plugins/color-switcher.js"></script>
    <!-- <script src="assets/mailer/mailer.js"></script> -->
    <script src="assets/js/landing2.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            var channel_id  = "44";
                        
            $.ajax({
                url: 'https://air.classmiles.com/v1/ListCountry',
                type: 'POST',
                success: function(response)
                {
                    var a = JSON.stringify(response);
                    var geoNow = window.localStorage.getItem('geo_location');                    
                    for (var i = 0; i < response.list_country.length; i++) {
                        var nicename = response.list_country[i]['nicename'];
                        var phonecode = response.list_country[i]['phonecode'];
                        var iso = response.list_country[i]['iso'];
                        if (geoNow == iso) {
                        	//SD
	                        $('#nama_negara_sd').append("<option value="+nicename+" selected='true'>"+nicename+"</option>");
	                        $('#kode_area_sd').append("<option value="+nicename+" selected='true'>"+nicename+" +"+phonecode+"</option>");
	                        //SMA / SMK
	                        $('#nama_negara_sma').append("<option value="+nicename+" selected='true'>"+nicename+"</option>");
	                        $('#kode_area_sma ').append("<option value="+nicename+" selected='true'>"+nicename+" +"+phonecode+"</option>");
	                        //UMUM
	                        $('#nama_negara_umum').append("<option value="+nicename+" selected='true'>"+nicename+"</option>");
	                        $('#kode_area_umum ').append("<option value="+nicename+" selected='true'>"+nicename+" +"+phonecode+"</option>");
                        }
                        else
                        {
	                        //SD
	                        $('#nama_negara_sd').append("<option value="+nicename+">"+nicename+"</option>");
	                        $('#kode_area_sd').append("<option value="+nicename+">"+nicename+" +"+phonecode+"</option>");
	                        //SMA / SMK
	                        $('#nama_negara_sma').append("<option value="+nicename+">"+nicename+"</option>");
	                        $('#kode_area_sma ').append("<option value="+nicename+">"+nicename+" +"+phonecode+"</option>");
	                        //UMUM
	                        $('#nama_negara_umum').append("<option value="+nicename+">"+nicename+"</option>");
	                        $('#kode_area_umum ').append("<option value="+nicename+">"+nicename+" +"+phonecode+"</option>");
	                    }
                    }
                }
            }); 

            $.ajax({
                url: 'https://air.classmiles.com/v1/ListJenjang',
                type: 'POST',
                success: function(response)
                {                    
                    for (var i = 0; i < response.list_jenjang.length; i++) {
                        var id = response.list_jenjang[i]['id'];
                        var text = response.list_jenjang[i]['text'];
                        $('#jenjang_sma').append("<option value="+id+">"+text+"</option>")
                    }
                }
            });           

            //CEK PASSWORD SD
            $('#password_sd').on('keyup',function(){
	            if($('#password_cek_sd').val() != $(this).val()){
	                $('#daftar_sd').attr('disabled','');			        			        
			        $('#alert_password_cek_sd').html('Kata sandi tidak cocok').css('display', 'block');
			    }else{			            
			        $('#alert_password_cek_sd').html('').css('display', 'none');
			        $("#daftar_sd").removeAttr('disabled');
		        }
		    });
            $('#password_cek_sd').on('keyup',function(){
	            if($('#password_sd').val() != $(this).val() ){
	                $('#daftar_sd').attr('disabled','');
	        		$('#alert_password_cek_sd').html('Kata sandi tidak cocok').css('display', 'block');
	            }else{
	        		$('#alert_password_cek_sd').html('').css('display', 'none');
	        		$("#daftar_sd").removeAttr('disabled');
	            }
	        });

            //CEK PASSWORD SMA/SMK
	        $('#password_sma').on('keyup',function(){
	            if($('#password_cek_sma').val() != $(this).val()){
	                $('#daftar_sma').attr('disabled','');			        			        
			        $('#alert_password_cek_sma').html('Kata sandi tidak cocok').css('display', 'block');
			    }else{			            
			        $('#alert_password_cek_sma').html('').css('display', 'none');
			        $("#daftar_sma").removeAttr('disabled');
		        }
		    });
            $('#password_cek_sma').on('keyup',function(){
	            if($('#password_sma').val() != $(this).val() ){
	                $('#daftar_sma').attr('disabled','');
	        		$('#alert_password_cek_sma').html('Kata sandi tidak cocok').css('display', 'block');
	            }else{
	        		$('#alert_password_cek_sma').html('').css('display', 'none');
	        		$("#daftar_sma").removeAttr('disabled');
	            }
	        });

	        //CEK PASSWORD UMUM
	        $('#password_umum').on('keyup',function(){
	            if($('#password_cek_umum').val() != $(this).val()){
	                $('#daftar_umum').attr('disabled','');			        			        
			        $('#alert_password_cek_umum').html('Kata sandi tidak cocok').css('display', 'block');
			    }else{			            
			        $('#alert_password_cek_umum').html('').css('display', 'none');
			        $("#daftar_umum").removeAttr('disabled');
		        }
		    });
            $('#password_cek_umum').on('keyup',function(){
	            if($('#password_umum').val() != $(this).val() ){
	                $('#daftar_umum').attr('disabled','');
	        		$('#alert_password_cek_umum').html('Kata sandi tidak cocok').css('display', 'block');
	            }else{
	        		$('#alert_password_cek_umum').html('').css('display', 'none');
	        		$("#daftar_umum").removeAttr('disabled');
	            }
	        });

            //CEK UMUR SD
            $('#tanggal_sd').on('change', function() {
            	var type_regis = $("#type_regis_sd").val();            	
		        var lahirtgl = $("#tanggal_sd").val();
		        var bulantgl = $("#bulan_sd").val();
		        var tahuntgl = $("#tahun_sd").val();		        
		        if (lahirtgl!=null&&bulantgl!=null&&tahuntgl!=null) {		        	
		            var age = checkBOD('sd');
		            var choose = chooseOccupation(age,type_regis,'sd');		            
		            if (choose == -2) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur pelajar.');
		                $('#daftar_sd').attr('disabled','true');
		            }
		            else if (choose == -1) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur mahasiswa, professional, dsbnya.');
		                $('#daftar_sd').attr('disabled','true');
		            }
		            else
		            {
		                $('#daftar_sd').removeAttr('disabled');		                
		            }
		        }
		    });
		    $('#bulan_sd').on('change', function() {
		        var type_regis = $("#type_regis_sd").val();
		        var lahirtgl = $("#tanggal_sd").val();
		        var bulantgl = $("#bulan_sd").val();
		        var tahuntgl = $("#tahun_sd").val();
		        if (lahirtgl!=null&&bulantgl!=null&&tahuntgl!=null) {
		            var age = checkBOD('sd');
		            var choose = chooseOccupation(age,type_regis,'sd');
		            if (choose == -2) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur pelajar.');
		                $('#daftar_sd').attr('disabled','true');
		            }
		            else if (choose == -1) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur mahasiswa, professional, dsbnya.');
		                $('#daftar_sd').attr('disabled','true');
		            }
		            else
		            {
		                $('#daftar_sd').removeAttr('disabled');		                
		            }
		        }
		    });
		    $('#tahun_sd').on('change', function() {
		        var type_regis = $("#type_regis_sd").val();
		        var lahirtgl = $("#tanggal_sd").val();
		        var bulantgl = $("#bulan_sd").val();
		        var tahuntgl = $("#tahun_sd").val();
		        if (lahirtgl!=null&&bulantgl!=null&&tahuntgl!=null) {
		            var age = checkBOD('sd');            
		            var choose = chooseOccupation(age,type_regis,'sd');
		            if (choose == -2) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur pelajar.');
		                $('#daftar_sd').attr('disabled','true');		               
		            }
		            else if (choose == -1) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur mahasiswa, professional, dsbnya.');
		                $('#daftar_sd').attr('disabled','true');		                
		            }
		            else
		            {		                
		                $('#daftar_sd').removeAttr('disabled');
		            }
		        }
		    });

		    //CEK UMUR SMA/SMK
		    $('#tanggal_sma').on('change', function() {
            	var type_regis = $("#type_regis_sma").val();            	
		        var lahirtgl = $("#tanggal_sma").val();
		        var bulantgl = $("#bulan_sma").val();
		        var tahuntgl = $("#tahun_sma").val();		        
		        if (lahirtgl!=null&&bulantgl!=null&&tahuntgl!=null) {		        	
		            var age = checkBOD('sma');
		            var choose = chooseOccupation(age,type_regis,'sma');		            
		            if (choose == -2) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur pelajar.');
		                $('#daftar_sma').attr('disabled','true');
		            }
		            else if (choose == -1) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur mahasiswa, professional, dsbnya.');
		                $('#daftar_sma').attr('disabled','true');
		            }
		            else
		            {
		                $('#daftar_sma').removeAttr('disabled');		                
		            }
		        }
		    });
		    $('#bulan_sma').on('change', function() {
		        var type_regis = $("#type_regis_sma").val();
		        var lahirtgl = $("#tanggal_sma").val();
		        var bulantgl = $("#bulan_sma").val();
		        var tahuntgl = $("#tahun_sma").val();
		        if (lahirtgl!=null&&bulantgl!=null&&tahuntgl!=null) {
		            var age = checkBOD('sma');
		            var choose = chooseOccupation(age,type_regis,'sma');
		            if (choose == -2) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur pelajar.');
		                $('#daftar_sma').attr('disabled','true');
		            }
		            else if (choose == -1) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur mahasiswa, professional, dsbnya.');
		                $('#daftar_sma').attr('disabled','true');
		            }
		            else
		            {
		                $('#daftar_sma').removeAttr('disabled');		                
		            }
		        }
		    });
		    $('#tahun_sma').on('change', function() {
		        var type_regis = $("#type_regis_sma").val();
		        var lahirtgl = $("#tanggal_sma").val();
		        var bulantgl = $("#bulan_sma").val();
		        var tahuntgl = $("#tahun_sma").val();
		        if (lahirtgl!=null&&bulantgl!=null&&tahuntgl!=null) {
		            var age = checkBOD('sma');            
		            var choose = chooseOccupation(age,type_regis,'sma');
		            if (choose == -2) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur pelajar.');
		                $('#daftar_sma').attr('disabled','true');		               
		            }
		            else if (choose == -1) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur mahasiswa, professional, dsbnya.');
		                $('#daftar_sma').attr('disabled','true');		                
		            }
		            else
		            {		                
		                $('#daftar_sma').removeAttr('disabled');
		            }
		        }
		    });

		    //CEK UMUR UMUM
		    $('#tanggal_umum').on('change', function() {
            	var type_regis = $("#type_regis_umum").val();            	
		        var lahirtgl = $("#tanggal_umum").val();
		        var bulantgl = $("#bulan_umum").val();
		        var tahuntgl = $("#tahun_umum").val();		        
		        if (lahirtgl!=null&&bulantgl!=null&&tahuntgl!=null) {		        	
		            var age = checkBOD('umum');
		            var choose = chooseOccupation(age,type_regis,'umum');		            
		            if (choose == -2) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur pelajar.');
		                $('#daftar_umum').attr('disabled','true');
		            }
		            else if (choose == -1) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur mahasiswa, professional, dsbnya.');
		                $('#daftar_umum').attr('disabled','true');
		            }
		            else
		            {
		                $('#daftar_umum').removeAttr('disabled');		                
		            }
		        }
		    });
		    $('#bulan_umum').on('change', function() {
		        var type_regis = $("#type_regis_umum").val();
		        var lahirtgl = $("#tanggal_umum").val();
		        var bulantgl = $("#bulan_umum").val();
		        var tahuntgl = $("#tahun_umum").val();
		        if (lahirtgl!=null&&bulantgl!=null&&tahuntgl!=null) {
		            var age = checkBOD('umum');
		            var choose = chooseOccupation(age,type_regis,'umum');
		            if (choose == -2) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur pelajar.');
		                $('#daftar_umum').attr('disabled','true');
		            }
		            else if (choose == -1) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur mahasiswa, professional, dsbnya.');
		                $('#daftar_umum').attr('disabled','true');
		            }
		            else
		            {
		                $('#daftar_umum').removeAttr('disabled');		                
		            }
		        }
		    });
		    $('#tahun_umum').on('change', function() {
		        var type_regis = $("#type_regis_umum").val();
		        var lahirtgl = $("#tanggal_umum").val();
		        var bulantgl = $("#bulan_umum").val();
		        var tahuntgl = $("#tahun_umum").val();
		        if (lahirtgl!=null&&bulantgl!=null&&tahuntgl!=null) {
		            var age = checkBOD('umum');            
		            var choose = chooseOccupation(age,type_regis,'umum');
		            if (choose == -2) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur pelajar.');
		                $('#daftar_umum').attr('disabled','true');		               
		            }
		            else if (choose == -1) {
		                $("#modal_cekumur").modal('show');
		                $("#alertumur").html('Apakah Anda sudah memilih Tanggal Lahir atau Pekerjaan dengan benar? Kami mendeteksi umur Anda tidak termasuk dalam kelompok umur mahasiswa, professional, dsbnya.');
		                $('#daftar_umum').attr('disabled','true');		                
		            }
		            else
		            {		                
		                $('#daftar_umum').removeAttr('disabled');
		            }
		        }
		    });

		    function checkBOD(pekerjaan) {		    	
		        var today = new Date();
		        var thn_ini = today.getFullYear();
		        var bln_ini = today.getMonth();
		        var tgl_ini = today.getDate();

		        var tgl_lahir = document.getElementById("tanggal_"+pekerjaan).value;
		        var bln_lahir = document.getElementById("bulan_"+pekerjaan).value;
		        var thn_lahir = document.getElementById("tahun_"+pekerjaan).value;

		        var age = thn_ini - thn_lahir;
		        var ageMonth = bln_ini - bln_ini;
		        var ageDay = tgl_ini - tgl_lahir;
		        // alert(age);

		        if (ageMonth < 0 || (ageMonth == 0 && ageDay < 0)) {
		        age = parseInt(age) - 1;
		        // alert(age);
		        }
		        return age;
		    }

    		function chooseOccupation(age,type,pekerjaan) {    
		        var today = new Date();
		        var thn_ini = today.getFullYear();
		        var bln_ini = today.getMonth();
		        var tgl_ini = today.getDate();

		        var tgl_lahir = document.getElementById("tanggal_"+pekerjaan).value;
		        var bln_lahir = document.getElementById("bulan_"+pekerjaan).value;
		        var thn_lahir = document.getElementById("tahun_"+pekerjaan).value;

		        var age = thn_ini - thn_lahir;
		        var ageMonth = bln_ini - bln_ini;
		        var ageDay = tgl_ini - tgl_lahir;		        		        		           
		        if (pekerjaan == "ortu" || pekerjaan == "umum") {
		            if (age <= 19) {                                                    
		                return -1;
		            }
		            else
		            {   
		                return 1;
		            }
		        }
		        else
		        {
		            if (age > 20) {
		                return -2
		            }
		            else
		            {
		                return 1;
		            }
		        }
		    }

            $("#daftar_sd").click(function(){            	
                var name_depan_sd     = $("#name_depan_sdd").val();
                var name_belakang_sd  = $("#name_belakang_sd").val();
                var email_sd          = $("#email_sd").val();
                var password_sd       = $("#password_sd").val();
                var password_cek_sd   = $("#password_cek_sd").val();
                var tanggal_sd        = $("#tanggal_sd").val();
                var bulan_sd          = $("#bulan_sd").val();
                var tahun_sd          = $("#tahun_sd").val();
                var jenis_kelamin_sd  = $("#jenis_kelamin_sd").val();
                var nama_negara_sd    = $("#nama_negara_sd").val();
                var kode_area_sd      = $("#kode_area_sd").val();
                var no_hape_sd        = $("#no_hape_sd").val();                               
                if (name_depan_sd == "") {
                    $("#alert_nama_depan_sd").css('display','block');
                    $("#alert_name_belakang_sd").css('display','none');
                    $("#alert_email_sd").css('display','none');
                    $("#alert_password_sd").css('display','none');
                    $("#alert_password_cek_sd").css('display','none');
                    $("#alert_tanggal_sd").css('display','none');
                    $("#alert_bulan_sd").css('display','none');
                    $("#alert_tahun_sd").css('display','none');
                    $("#alert_jenis_kelamin_sd").css('display','none');
                    $("#alert_nama_negara_sd").css('display','none');
                    $("#alert_kode_area_sd").css('display','none');
                    $("#alert_no_hape_sd").css('display','none');
                }
                else if (name_belakang_sd == "") {
                    $("#alert_nama_depan_sd").css('display','none');
                    $("#alert_name_belakang_sd").css('display','block');
                    $("#alert_email_sd").css('display','none');
                    $("#alert_password_sd").css('display','none');
                    $("#alert_password_cek_sd").css('display','none');
                    $("#alert_tanggal_sd").css('display','none');
                    $("#alert_bulan_sd").css('display','none');
                    $("#alert_tahun_sd").css('display','none');
                    $("#alert_jenis_kelamin_sd").css('display','none');
                    $("#alert_nama_negara_sd").css('display','none');
                    $("#alert_kode_area_sd").css('display','none');
                    $("#alert_no_hape_sd").css('display','none');
                }
                else if (email_sd == "") {
                    $("#alert_nama_depan_sd").css('display','none');
                    $("#alert_name_belakang_sd").css('display','none');
                    $("#alert_email_sd").css('display','block');
                    $("#alert_password_sd").css('display','none');
                    $("#alert_password_cek_sd").css('display','none');
                    $("#alert_tanggal_sd").css('display','none');
                    $("#alert_bulan_sd").css('display','none');
                    $("#alert_tahun_sd").css('display','none');
                    $("#alert_jenis_kelamin_sd").css('display','none');
                    $("#alert_nama_negara_sd").css('display','none');
                    $("#alert_kode_area_sd").css('display','none');
                    $("#alert_no_hape_sd").css('display','none');
                }
                else if (password_sd == "") {
                    $("#alert_nama_depan_sd").css('display','none');
                    $("#alert_name_belakang_sd").css('display','none');
                    $("#alert_email_sd").css('display','none');
                    $("#alert_password_sd").css('display','block');
                    $("#alert_password_cek_sd").css('display','none');
                    $("#alert_tanggal_sd").css('display','none');
                    $("#alert_bulan_sd").css('display','none');
                    $("#alert_tahun_sd").css('display','none');
                    $("#alert_jenis_kelamin_sd").css('display','none');
                    $("#alert_nama_negara_sd").css('display','none');
                    $("#alert_kode_area_sd").css('display','none');
                    $("#alert_no_hape_sd").css('display','none');
                }
                else if (password_cek_sd == "") {
                    $("#alert_nama_depan_sd").css('display','none');
                    $("#alert_name_belakang_sd").css('display','none');
                    $("#alert_email_sd").css('display','none');
                    $("#alert_password_sd").css('display','none');
                    $("#alert_password_cek_sd").css('display','block');
                    $("#alert_tanggal_sd").css('display','none');
                    $("#alert_bulan_sd").css('display','none');
                    $("#alert_tahun_sd").css('display','none');
                    $("#alert_jenis_kelamin_sd").css('display','none');
                    $("#alert_nama_negara_sd").css('display','none');
                    $("#alert_kode_area_sd").css('display','none');
                    $("#alert_no_hape_sd").css('display','none');
                }
                else if (tanggal_sd == "") {
                    $("#alert_nama_depan_sd").css('display','none');
                    $("#alert_name_belakang_sd").css('display','none');
                    $("#alert_email_sd").css('display','none');
                    $("#alert_password_sd").css('display','none');
                    $("#alert_password_cek_sd").css('display','none');
                    $("#alert_tanggal_sd").css('display','block');
                    $("#alert_bulan_sd").css('display','none');
                    $("#alert_tahun_sd").css('display','none');
                    $("#alert_jenis_kelamin_sd").css('display','none');
                    $("#alert_nama_negara_sd").css('display','none');
                    $("#alert_kode_area_sd").css('display','none');
                    $("#alert_no_hape_sd").css('display','none');
                }
                else if (bulan_sd == "") {
                    $("#alert_nama_depan_sd").css('display','none');
                    $("#alert_name_belakang_sd").css('display','none');
                    $("#alert_email_sd").css('display','none');
                    $("#alert_password_sd").css('display','none');
                    $("#alert_password_cek_sd").css('display','none');
                    $("#alert_tanggal_sd").css('display','none');
                    $("#alert_bulan_sd").css('display','block');
                    $("#alert_tahun_sd").css('display','none');
                    $("#alert_jenis_kelamin_sd").css('display','none');
                    $("#alert_nama_negara_sd").css('display','none');
                    $("#alert_kode_area_sd").css('display','none');
                    $("#alert_no_hape_sd").css('display','none');
                }
                else if (tahun_sd == "") {
                    $("#alert_nama_depan_sd").css('display','none');
                    $("#alert_name_belakang_sd").css('display','none');
                    $("#alert_email_sd").css('display','none');
                    $("#alert_password_sd").css('display','none');
                    $("#alert_password_cek_sd").css('display','none');
                    $("#alert_tanggal_sd").css('display','none');
                    $("#alert_bulan_sd").css('display','none');
                    $("#alert_tahun_sd").css('display','block');
                    $("#alert_jenis_kelamin_sd").css('display','none');
                    $("#alert_nama_negara_sd").css('display','none');
                    $("#alert_kode_area_sd").css('display','none');
                    $("#alert_no_hape_sd").css('display','none');
                }
                else if (jenis_kelamin_sd == "") {
                    $("#alert_nama_depan_sd").css('display','none');
                    $("#alert_name_belakang_sd").css('display','none');
                    $("#alert_email_sd").css('display','none');
                    $("#alert_password_sd").css('display','none');
                    $("#alert_password_cek_sd").css('display','none');
                    $("#alert_tanggal_sd").css('display','none');
                    $("#alert_bulan_sd").css('display','none');
                    $("#alert_tahun_sd").css('display','none');
                    $("#alert_jenis_kelamin_sd").css('display','block');
                    $("#alert_nama_negara_sd").css('display','none');
                    $("#alert_kode_area_sd").css('display','none');
                    $("#alert_no_hape_sd").css('display','none');
                }
                else if (nama_negara_sd == "") {
                    $("#alert_nama_depan_sd").css('display','none');
                    $("#alert_name_belakang_sd").css('display','none');
                    $("#alert_email_sd").css('display','none');
                    $("#alert_password_sd").css('display','none');
                    $("#alert_password_cek_sd").css('display','none');
                    $("#alert_tanggal_sd").css('display','none');
                    $("#alert_bulan_sd").css('display','none');
                    $("#alert_tahun_sd").css('display','none');
                    $("#alert_jenis_kelamin_sd").css('display','none');
                    $("#alert_nama_negara_sd").css('display','block');
                    $("#alert_kode_area_sd").css('display','none');
                    $("#alert_no_hape_sd").css('display','none');
                }
                else if (kode_area_sd == "") {
                    $("#alert_nama_depan_sd").css('display','none');
                    $("#alert_name_belakang_sd").css('display','none');
                    $("#alert_email_sd").css('display','none');
                    $("#alert_password_sd").css('display','none');
                    $("#alert_password_cek_sd").css('display','none');
                    $("#alert_tanggal_sd").css('display','none');
                    $("#alert_bulan_sd").css('display','none');
                    $("#alert_tahun_sd").css('display','none');
                    $("#alert_jenis_kelamin_sd").css('display','none');
                    $("#alert_nama_negara_sd").css('display','none');
                    $("#alert_kode_area_sd").css('display','block');
                    $("#alert_no_hape_sd").css('display','none');
                }
                else if (no_hape_sd == "") {
                    $("#alert_nama_depan_sd").css('display','none');
                    $("#alert_name_belakang_sd").css('display','none');
                    $("#alert_email_sd").css('display','none');
                    $("#alert_password_sd").css('display','none');
                    $("#alert_password_cek_sd").css('display','none');
                    $("#alert_tanggal_sd").css('display','none');
                    $("#alert_bulan_sd").css('display','none');
                    $("#alert_tahun_sd").css('display','none');
                    $("#alert_jenis_kelamin_sd").css('display','none');
                    $("#alert_nama_negara_sd").css('display','none');
                    $("#alert_kode_area_sd").css('display','none');
                    $("#alert_no_hape_sd").css('display','block');
                }
                else
                {
                    $("#alert_nama_depan_sd").css('display','none');
                    $("#alert_name_belakang_sd").css('display','none');
                    $("#alert_email_sd").css('display','none');
                    $("#alert_password_sd").css('display','none');
                    $("#alert_password_cek_sd").css('display','none');
                    $("#alert_tanggal_sd").css('display','none');
                    $("#alert_bulan_sd").css('display','none');
                    $("#alert_tahun_sd").css('display','none');
                    $("#alert_jenis_kelamin_sd").css('display','none');
                    $("#alert_nama_negara_sd").css('display','none');
                    $("#alert_kode_area_sd").css('display','none');
                    $("#alert_no_hape_sd").css('display','none');
                    $.ajax({
                        url: 'https://air.classmiles.com/v1/Channel_daftarMurid',
                        type: 'POST',
                        data: {
                            channel_id: channel_id,
                            first_name: name_depan_sd,
                            last_name: name_belakang_sd,
                            email: email_sd,
                            kata_sandi: password_sd,
                            tanggal_lahir: tanggal_sd,
                            bulan_lahir: bulan_sd,
                            tahun_lahir: tahun_sd,
                            jenis_kelamin: jenis_kelamin_sd,
                            kode_area: kode_area_sd,
                            nama_negara: nama_negara_sd,
                            no_hape: no_hape_sd,
                            type: 'ortu'
                        },
                        success: function(response)
                        {                             
                            var code = response['code'];
                            $("#alert_modal").modal("show");
                            if (code == 200) {
                                $("#alert_title_modal").text("Pendaftaran Sukses");
                                $("#alert_message_modal").text("Silahkan cek email anda untuk melakukan aktivasi");
                            }
                            else if(code == -102){
                                $("#alert_title_modal").text("Pemberitahuan"); 
                                $("#alert_message_modal").text("Anda telah terdaftar, Silahkan login dengan akun Anda");
                            }
                            else if(code == -100){
                                $("#alert_title_modal").text("Pendaftaran gagal"); 
                                $("#alert_message_modal").text("Silahkan coba kembali.");
                            }
                            else
                            {
                                $("#alert_title_modal").text("Terjadi Kesalahan");
                                $("#alert_message_modal").text("Silahkan coba kembali");
                            }
                            setTimeout(function(){
                                location.reload();
                            },4000);
                        }
                    });
                }
            });
            
            $("#daftar_sma").click(function(){
            	$("#type_regis_sma").val("sma");
                var name_depan_sma     = $("#name_depan_sma").val();
                var name_belakang_sma  = $("#name_belakang_sma").val();
                var email_sma          = $("#email_sma").val();
                var password_sma       = $("#password_sma").val();
                var password_cek_sma   = $("#password_cek_sma").val();
                var tanggal_sma        = $("#tanggal_sma").val();
                var bulan_sma          = $("#bulan_sma").val();
                var tahun_sma          = $("#tahun_sma").val();
                var jenis_kelamin_sma  = $("#jenis_kelamin_sma").val();
                var nama_negara_sma    = $("#nama_negara_sma").val();
                var kode_area_sma      = $("#kode_area_sma").val();
                var no_hape_sma        = $("#no_hape_sma").val();
                var jenjang_sma        = $("#jenjang_sma").val();
                if (name_depan_sma == "") {
                    $("#alert_nama_depan_sma").css('display','block');
                    $("#alert_name_belakang_sma").css('display','none');
                    $("#alert_email_sma").css('display','none');
                    $("#alert_password_sma").css('display','none');
                    $("#alert_password_cek_sma").css('display','none');
                    $("#alert_tanggal_sma").css('display','none');
                    $("#alert_bulan_sma").css('display','none');
                    $("#alert_tahun_sma").css('display','none');
                    $("#alert_jenis_kelamin_sma").css('display','none');
                    $("#alert_nama_negara_sma").css('display','none');
                    $("#alert_kode_area_sma").css('display','none');
                    $("#alert_no_hape_sma").css('display','none');
                    $("#alert_jenjang_sma").css('display','none');
                }
                else if (name_belakang_sma == "") {
                    $("#alert_nama_depan_sma").css('display','none');
                    $("#alert_name_belakang_sma").css('display','block');
                    $("#alert_email_sma").css('display','none');
                    $("#alert_password_sma").css('display','none');
                    $("#alert_password_cek_sma").css('display','none');
                    $("#alert_tanggal_sma").css('display','none');
                    $("#alert_bulan_sma").css('display','none');
                    $("#alert_tahun_sma").css('display','none');
                    $("#alert_jenis_kelamin_sma").css('display','none');
                    $("#alert_nama_negara_sma").css('display','none');
                    $("#alert_kode_area_sma").css('display','none');
                    $("#alert_no_hape_sma").css('display','none');
                    $("#alert_jenjang_sma").css('display','none');
                }
                else if (email_sma == "") {
                    $("#alert_nama_depan_sma").css('display','none');
                    $("#alert_name_belakang_sma").css('display','none');
                    $("#alert_email_sma").css('display','block');
                    $("#alert_password_sma").css('display','none');
                    $("#alert_password_cek_sma").css('display','none');
                    $("#alert_tanggal_sma").css('display','none');
                    $("#alert_bulan_sma").css('display','none');
                    $("#alert_tahun_sma").css('display','none');
                    $("#alert_jenis_kelamin_sma").css('display','none');
                    $("#alert_nama_negara_sma").css('display','none');
                    $("#alert_kode_area_sma").css('display','none');
                    $("#alert_no_hape_sma").css('display','none');
                    $("#alert_jenjang_sma").css('display','none');
                }
                else if (password_sma == "") {
                    $("#alert_nama_depan_sma").css('display','none');
                    $("#alert_name_belakang_sma").css('display','none');
                    $("#alert_email_sma").css('display','none');
                    $("#alert_password_sma").css('display','block');
                    $("#alert_password_cek_sma").css('display','none');
                    $("#alert_tanggal_sma").css('display','none');
                    $("#alert_bulan_sma").css('display','none');
                    $("#alert_tahun_sma").css('display','none');
                    $("#alert_jenis_kelamin_sma").css('display','none');
                    $("#alert_nama_negara_sma").css('display','none');
                    $("#alert_kode_area_sma").css('display','none');
                    $("#alert_no_hape_sma").css('display','none');
                    $("#alert_jenjang_sma").css('display','none');
                }
                else if (password_cek_sma == "") {
                    $("#alert_nama_depan_sma").css('display','none');
                    $("#alert_name_belakang_sma").css('display','none');
                    $("#alert_email_sma").css('display','none');
                    $("#alert_password_sma").css('display','none');
                    $("#alert_password_cek_sma").css('display','block');
                    $("#alert_tanggal_sma").css('display','none');
                    $("#alert_bulan_sma").css('display','none');
                    $("#alert_tahun_sma").css('display','none');
                    $("#alert_jenis_kelamin_sma").css('display','none');
                    $("#alert_nama_negara_sma").css('display','none');
                    $("#alert_kode_area_sma").css('display','none');
                    $("#alert_no_hape_sma").css('display','none');
                    $("#alert_jenjang_sma").css('display','none');
                }
                else if (tanggal_sma == "") {
                    $("#alert_nama_depan_sma").css('display','none');
                    $("#alert_name_belakang_sma").css('display','none');
                    $("#alert_email_sma").css('display','none');
                    $("#alert_password_sma").css('display','none');
                    $("#alert_password_cek_sma").css('display','none');
                    $("#alert_tanggal_sma").css('display','block');
                    $("#alert_bulan_sma").css('display','none');
                    $("#alert_tahun_sma").css('display','none');
                    $("#alert_jenis_kelamin_sma").css('display','none');
                    $("#alert_nama_negara_sma").css('display','none');
                    $("#alert_kode_area_sma").css('display','none');
                    $("#alert_no_hape_sma").css('display','none');
                    $("#alert_jenjang_sma").css('display','none');
                }
                else if (bulan_sma == "") {
                    $("#alert_nama_depan_sma").css('display','none');
                    $("#alert_name_belakang_sma").css('display','none');
                    $("#alert_email_sma").css('display','none');
                    $("#alert_password_sma").css('display','none');
                    $("#alert_password_cek_sma").css('display','none');
                    $("#alert_tanggal_sma").css('display','none');
                    $("#alert_bulan_sma").css('display','block');
                    $("#alert_tahun_sma").css('display','none');
                    $("#alert_jenis_kelamin_sma").css('display','none');
                    $("#alert_nama_negara_sma").css('display','none');
                    $("#alert_kode_area_sma").css('display','none');
                    $("#alert_no_hape_sma").css('display','none');
                    $("#alert_jenjang_sma").css('display','none');
                }
                else if (tahun_sma == "") {
                    $("#alert_nama_depan_sma").css('display','none');
                    $("#alert_name_belakang_sma").css('display','none');
                    $("#alert_email_sma").css('display','none');
                    $("#alert_password_sma").css('display','none');
                    $("#alert_password_cek_sma").css('display','none');
                    $("#alert_tanggal_sma").css('display','none');
                    $("#alert_bulan_sma").css('display','none');
                    $("#alert_tahun_sma").css('display','block');
                    $("#alert_jenis_kelamin_sma").css('display','none');
                    $("#alert_nama_negara_sma").css('display','none');
                    $("#alert_kode_area_sma").css('display','none');
                    $("#alert_no_hape_sma").css('display','none');
                    $("#alert_jenjang_sma").css('display','none');
                }
                else if (jenis_kelamin_sma == "") {
                    $("#alert_nama_depan_sma").css('display','none');
                    $("#alert_name_belakang_sma").css('display','none');
                    $("#alert_email_sma").css('display','none');
                    $("#alert_password_sma").css('display','none');
                    $("#alert_password_cek_sma").css('display','none');
                    $("#alert_tanggal_sma").css('display','none');
                    $("#alert_bulan_sma").css('display','none');
                    $("#alert_tahun_sma").css('display','none');
                    $("#alert_jenis_kelamin_sma").css('display','block');
                    $("#alert_nama_negara_sma").css('display','none');
                    $("#alert_kode_area_sma").css('display','none');
                    $("#alert_no_hape_sma").css('display','none');
                    $("#alert_jenjang_sma").css('display','none');
                }
                else if (nama_negara_sma == "") {
                    $("#alert_nama_depan_sma").css('display','none');
                    $("#alert_name_belakang_sma").css('display','none');
                    $("#alert_email_sma").css('display','none');
                    $("#alert_password_sma").css('display','none');
                    $("#alert_password_cek_sma").css('display','none');
                    $("#alert_tanggal_sma").css('display','none');
                    $("#alert_bulan_sma").css('display','none');
                    $("#alert_tahun_sma").css('display','none');
                    $("#alert_jenis_kelamin_sma").css('display','none');
                    $("#alert_nama_negara_sma").css('display','block');
                    $("#alert_kode_area_sma").css('display','none');
                    $("#alert_no_hape_sma").css('display','none');
                    $("#alert_jenjang_sma").css('display','none');
                }
                else if (kode_area_sma == "") {
                    $("#alert_nama_depan_sma").css('display','none');
                    $("#alert_name_belakang_sma").css('display','none');
                    $("#alert_email_sma").css('display','none');
                    $("#alert_password_sma").css('display','none');
                    $("#alert_password_cek_sma").css('display','none');
                    $("#alert_tanggal_sma").css('display','none');
                    $("#alert_bulan_sma").css('display','none');
                    $("#alert_tahun_sma").css('display','none');
                    $("#alert_jenis_kelamin_sma").css('display','none');
                    $("#alert_nama_negara_sma").css('display','none');
                    $("#alert_kode_area_sma").css('display','block');
                    $("#alert_no_hape_sma").css('display','none');
                    $("#alert_jenjang_sma").css('display','none');
                }
                else if (no_hape_sma == "") {
                    $("#alert_nama_depan_sma").css('display','none');
                    $("#alert_name_belakang_sma").css('display','none');
                    $("#alert_email_sma").css('display','none');
                    $("#alert_password_sma").css('display','none');
                    $("#alert_password_cek_sma").css('display','none');
                    $("#alert_tanggal_sma").css('display','none');
                    $("#alert_bulan_sma").css('display','none');
                    $("#alert_tahun_sma").css('display','none');
                    $("#alert_jenis_kelamin_sma").css('display','none');
                    $("#alert_nama_negara_sma").css('display','none');
                    $("#alert_kode_area_sma").css('display','none');
                    $("#alert_no_hape_sma").css('display','block');
                    $("#alert_jenjang_sma").css('display','none');
                }
                else if (jenjang_sma == "") {
                    $("#alert_nama_depan_sma").css('display','none');
                    $("#alert_name_belakang_sma").css('display','none');
                    $("#alert_email_sma").css('display','none');
                    $("#alert_password_sma").css('display','none');
                    $("#alert_password_cek_sma").css('display','none');
                    $("#alert_tanggal_sma").css('display','none');
                    $("#alert_bulan_sma").css('display','none');
                    $("#alert_tahun_sma").css('display','none');
                    $("#alert_jenis_kelamin_sma").css('display','none');
                    $("#alert_nama_negara_sma").css('display','none');
                    $("#alert_kode_area_sma").css('display','none');
                    $("#alert_no_hape_sma").css('display','none');
                    $("#alert_jenjang_sma").css('display','block');
                }
                else
                {
                    $("#alert_nama_depan_sma").css('display','none');
                    $("#alert_name_belakang_sma").css('display','none');
                    $("#alert_email_sma").css('display','none');
                    $("#alert_password_sma").css('display','none');
                    $("#alert_password_cek_sma").css('display','none');
                    $("#alert_tanggal_sma").css('display','none');
                    $("#alert_bulan_sma").css('display','none');
                    $("#alert_tahun_sma").css('display','none');
                    $("#alert_jenis_kelamin_sma").css('display','none');
                    $("#alert_nama_negara_sma").css('display','none');
                    $("#alert_kode_area_sma").css('display','none');
                    $("#alert_no_hape_sma").css('display','none');
                    $("#alert_jenjang_sma").css('display','none');
                    $.ajax({
                        url: 'https://air.classmiles.com/v1/Channel_daftarMurid',
                        type: 'POST',
                        data: {
                            channel_id: channel_id,
                            first_name: name_depan_sma,
                            last_name: name_belakang_sma,
                            email: email_sma,
                            kata_sandi: password_sma,
                            tanggal_lahir: tanggal_sma,
                            bulan_lahir: bulan_sma,
                            tahun_lahir: tahun_sma,
                            jenis_kelamin: jenis_kelamin_sma,
                            kode_area: kode_area_sma,
                            nama_negara: nama_negara_sma,
                            no_hape: no_hape_sma,
                            jenjang_id: jenjang_sma,
                            type: 'siswa'
                        },
                        success: function(response)
                        {                             
                            var code = response['code'];
                            $("#alert_modal").modal("show");
                            if (code == 200) {
                                $("#alert_title_modal").text("Pendaftaran Smp/Sma Sukses");
                                $("#alert_message_modal").text("Silahkan cek email anda untuk melakukan aktivasi");
                            }
                            else if(code == -102){
                                $("#alert_title_modal").text("Pemberitahuan"); 
                                $("#alert_message_modal").text("Anda telah terdaftar, Silahkan login dengan akun Anda");
                            }
                            else if(code == -100){
                                $("#alert_title_modal").text("Pendaftaran gagal"); 
                                $("#alert_message_modal").text("Silahkan coba kembali.");
                            }
                            else
                            {
                                $("#alert_title_modal").text("Terjadi Kesalahan");
                                $("#alert_message_modal").text("Silahkan coba kembali");
                            }
                            setTimeout(function(){
                                location.reload();
                            },4000);
                        }
                    });
                }
            });
            
            $("#daftar_umum").click(function(){
            	$("#type_regis_umum").val("umum");
                var name_depan_umum     = $("#name_depan_umum").val();
                var name_belakang_umum  = $("#name_belakang_umum").val();
                var email_umum          = $("#email_umum").val();
                var password_umum       = $("#password_umum").val();
                var password_cek_umum   = $("#password_cek_umum").val();
                var tanggal_umum        = $("#tanggal_umum").val();
                var bulan_umum          = $("#bulan_umum").val();
                var tahun_umum          = $("#tahun_umum").val();
                var jenis_kelamin_umum  = $("#jenis_kelamin_umum").val();
                var nama_negara_umum    = $("#nama_negara_umum").val();
                var kode_area_umum      = $("#kode_area_umum").val();
                var no_hape_umum        = $("#no_hape_umum").val();                               
                if (name_depan_umum == "") {
                    $("#alert_nama_depan_umum").css('display','block');
                    $("#alert_name_belakang_umum").css('display','none');
                    $("#alert_email_umum").css('display','none');
                    $("#alert_password_umum").css('display','none');
                    $("#alert_password_cek_umum").css('display','none');
                    $("#alert_tanggal_umum").css('display','none');
                    $("#alert_bulan_umum").css('display','none');
                    $("#alert_tahun_umum").css('display','none');
                    $("#alert_jenis_kelamin_umum").css('display','none');
                    $("#alert_nama_negara_umum").css('display','none');
                    $("#alert_kode_area_umum").css('display','none');
                    $("#alert_no_hape_umum").css('display','none');
                }
                else if (name_belakang_umum == "") {
                    $("#alert_nama_depan_umum").css('display','none');
                    $("#alert_name_belakang_umum").css('display','block');
                    $("#alert_email_umum").css('display','none');
                    $("#alert_password_umum").css('display','none');
                    $("#alert_password_cek_umum").css('display','none');
                    $("#alert_tanggal_umum").css('display','none');
                    $("#alert_bulan_umum").css('display','none');
                    $("#alert_tahun_umum").css('display','none');
                    $("#alert_jenis_kelamin_umum").css('display','none');
                    $("#alert_nama_negara_umum").css('display','none');
                    $("#alert_kode_area_umum").css('display','none');
                    $("#alert_no_hape_umum").css('display','none');
                }
                else if (email_umum == "") {
                    $("#alert_nama_depan_umum").css('display','none');
                    $("#alert_name_belakang_umum").css('display','none');
                    $("#alert_email_umum").css('display','block');
                    $("#alert_password_umum").css('display','none');
                    $("#alert_password_cek_umum").css('display','none');
                    $("#alert_tanggal_umum").css('display','none');
                    $("#alert_bulan_umum").css('display','none');
                    $("#alert_tahun_umum").css('display','none');
                    $("#alert_jenis_kelamin_umum").css('display','none');
                    $("#alert_nama_negara_umum").css('display','none');
                    $("#alert_kode_area_umum").css('display','none');
                    $("#alert_no_hape_umum").css('display','none');
                }
                else if (password_umum == "") {
                    $("#alert_nama_depan_umum").css('display','none');
                    $("#alert_name_belakang_umum").css('display','none');
                    $("#alert_email_umum").css('display','none');
                    $("#alert_password_umum").css('display','block');
                    $("#alert_password_cek_umum").css('display','none');
                    $("#alert_tanggal_umum").css('display','none');
                    $("#alert_bulan_umum").css('display','none');
                    $("#alert_tahun_umum").css('display','none');
                    $("#alert_jenis_kelamin_umum").css('display','none');
                    $("#alert_nama_negara_umum").css('display','none');
                    $("#alert_kode_area_umum").css('display','none');
                    $("#alert_no_hape_umum").css('display','none');
                }
                else if (password_cek_umum == "") {
                    $("#alert_nama_depan_umum").css('display','none');
                    $("#alert_name_belakang_umum").css('display','none');
                    $("#alert_email_umum").css('display','none');
                    $("#alert_password_umum").css('display','none');
                    $("#alert_password_cek_umum").css('display','block');
                    $("#alert_tanggal_umum").css('display','none');
                    $("#alert_bulan_umum").css('display','none');
                    $("#alert_tahun_umum").css('display','none');
                    $("#alert_jenis_kelamin_umum").css('display','none');
                    $("#alert_nama_negara_umum").css('display','none');
                    $("#alert_kode_area_umum").css('display','none');
                    $("#alert_no_hape_umum").css('display','none');
                }
                else if (tanggal_umum == "") {
                    $("#alert_nama_depan_umum").css('display','none');
                    $("#alert_name_belakang_umum").css('display','none');
                    $("#alert_email_umum").css('display','none');
                    $("#alert_password_umum").css('display','none');
                    $("#alert_password_cek_umum").css('display','none');
                    $("#alert_tanggal_umum").css('display','block');
                    $("#alert_bulan_umum").css('display','none');
                    $("#alert_tahun_umum").css('display','none');
                    $("#alert_jenis_kelamin_umum").css('display','none');
                    $("#alert_nama_negara_umum").css('display','none');
                    $("#alert_kode_area_umum").css('display','none');
                    $("#alert_no_hape_umum").css('display','none');
                }
                else if (bulan_umum == "") {
                    $("#alert_nama_depan_umum").css('display','none');
                    $("#alert_name_belakang_umum").css('display','none');
                    $("#alert_email_umum").css('display','none');
                    $("#alert_password_umum").css('display','none');
                    $("#alert_password_cek_umum").css('display','none');
                    $("#alert_tanggal_umum").css('display','none');
                    $("#alert_bulan_umum").css('display','block');
                    $("#alert_tahun_umum").css('display','none');
                    $("#alert_jenis_kelamin_umum").css('display','none');
                    $("#alert_nama_negara_umum").css('display','none');
                    $("#alert_kode_area_umum").css('display','none');
                    $("#alert_no_hape_umum").css('display','none');
                }
                else if (tahun_umum == "") {
                    $("#alert_nama_depan_umum").css('display','none');
                    $("#alert_name_belakang_umum").css('display','none');
                    $("#alert_email_umum").css('display','none');
                    $("#alert_password_umum").css('display','none');
                    $("#alert_password_cek_umum").css('display','none');
                    $("#alert_tanggal_umum").css('display','none');
                    $("#alert_bulan_umum").css('display','none');
                    $("#alert_tahun_umum").css('display','block');
                    $("#alert_jenis_kelamin_umum").css('display','none');
                    $("#alert_nama_negara_umum").css('display','none');
                    $("#alert_kode_area_umum").css('display','none');
                    $("#alert_no_hape_umum").css('display','none');
                }
                else if (jenis_kelamin_umum == "") {
                    $("#alert_nama_depan_umum").css('display','none');
                    $("#alert_name_belakang_umum").css('display','none');
                    $("#alert_email_umum").css('display','none');
                    $("#alert_password_umum").css('display','none');
                    $("#alert_password_cek_umum").css('display','none');
                    $("#alert_tanggal_umum").css('display','none');
                    $("#alert_bulan_umum").css('display','none');
                    $("#alert_tahun_umum").css('display','none');
                    $("#alert_jenis_kelamin_umum").css('display','block');
                    $("#alert_nama_negara_umum").css('display','none');
                    $("#alert_kode_area_umum").css('display','none');
                    $("#alert_no_hape_umum").css('display','none');
                }
                else if (nama_negara_umum == "") {
                    $("#alert_nama_depan_umum").css('display','none');
                    $("#alert_name_belakang_umum").css('display','none');
                    $("#alert_email_umum").css('display','none');
                    $("#alert_password_umum").css('display','none');
                    $("#alert_password_cek_umum").css('display','none');
                    $("#alert_tanggal_umum").css('display','none');
                    $("#alert_bulan_umum").css('display','none');
                    $("#alert_tahun_umum").css('display','none');
                    $("#alert_jenis_kelamin_umum").css('display','none');
                    $("#alert_nama_negara_umum").css('display','block');
                    $("#alert_kode_area_umum").css('display','none');
                    $("#alert_no_hape_umum").css('display','none');
                }
                else if (kode_area_umum == "") {
                    $("#alert_nama_depan_umum").css('display','none');
                    $("#alert_name_belakang_umum").css('display','none');
                    $("#alert_email_umum").css('display','none');
                    $("#alert_password_umum").css('display','none');
                    $("#alert_password_cek_umum").css('display','none');
                    $("#alert_tanggal_umum").css('display','none');
                    $("#alert_bulan_umum").css('display','none');
                    $("#alert_tahun_umum").css('display','none');
                    $("#alert_jenis_kelamin_umum").css('display','none');
                    $("#alert_nama_negara_umum").css('display','none');
                    $("#alert_kode_area_umum").css('display','block');
                    $("#alert_no_hape_umum").css('display','none');
                }
                else if (no_hape_umum == "") {
                    $("#alert_nama_depan_umum").css('display','none');
                    $("#alert_name_belakang_umum").css('display','none');
                    $("#alert_email_umum").css('display','none');
                    $("#alert_password_umum").css('display','none');
                    $("#alert_password_cek_umum").css('display','none');
                    $("#alert_tanggal_umum").css('display','none');
                    $("#alert_bulan_umum").css('display','none');
                    $("#alert_tahun_umum").css('display','none');
                    $("#alert_jenis_kelamin_umum").css('display','none');
                    $("#alert_nama_negara_umum").css('display','none');
                    $("#alert_kode_area_umum").css('display','none');
                    $("#alert_no_hape_umum").css('display','block');
                }
                else
                {
                    $("#alert_nama_depan_umum").css('display','none');
                    $("#alert_name_belakang_umum").css('display','none');
                    $("#alert_email_umum").css('display','none');
                    $("#alert_password_umum").css('display','none');
                    $("#alert_password_cek_umum").css('display','none');
                    $("#alert_tanggal_umum").css('display','none');
                    $("#alert_bulan_umum").css('display','none');
                    $("#alert_tahun_umum").css('display','none');
                    $("#alert_jenis_kelamin_umum").css('display','none');
                    $("#alert_nama_negara_umum").css('display','none');
                    $("#alert_kode_area_umum").css('display','none');
                    $("#alert_no_hape_umum").css('display','none');
                    $.ajax({
                        url: 'https://air.classmiles.com/v1/Channel_daftarMurid',
                        type: 'POST',
                        data: {
                            channel_id: channel_id,
                            first_name: name_depan_umum,
                            last_name: name_belakang_umum,
                            email: email_umum,
                            kata_sandi: password_umum,
                            tanggal_lahir: tanggal_umum,
                            bulan_lahir: bulan_umum,
                            tahun_lahir: tahun_umum,
                            jenis_kelamin: jenis_kelamin_umum,
                            kode_area: kode_area_umum,
                            nama_negara: nama_negara_umum,
                            no_hape: no_hape_umum,
                            type: 'ortu'
                        },
                        success: function(response)
                        {                             
                            var code = response['code'];
                            $("#alert_modal").modal("show");
                            if (code == 200) {
                                $("#alert_title_modal").text("Pendaftaran Umum Sukses");
                                $("#alert_message_modal").text("Silahkan cek email anda untuk melakukan aktivasi");
                            }
                            else if(code == -102){
                                $("#alert_title_modal").text("Pemberitahuan");
                                $("#alert_message_modal").text("Anda telah terdaftar, Silahkan login dengan akun Anda");
                            }
                            else if(code == -100){
                                $("#alert_title_modal").text("Pendaftaran gagal"); 
                                $("#alert_message_modal").text("Silahkan coba kembali.");
                            }
                            else
                            {
                                $("#alert_title_modal").text("Terjadi Kesalahan");
                                $("#alert_message_modal").text("Silahkan coba kembali");
                            }
                            setTimeout(function(){
                                location.reload();
                            },4000);
                        }
                    });
                }
            });

        });
    </script>

</body><!--Close Body-->
</html>
