
<style type="text/css">
.jenis_signup{
    font-size: 12px;
    text-align: justify;
}
    body
    {
        background-color: white;
    }
    @media screen and (orientation:landscape) {
        #navbar_atas
        {
        }
        body
        {
            background-color: white;
        }

        #menu_register{
            margin-top: 10%;
        }
    }
    @media screen and (orientation:portrait) {

        #menu_register{
            margin-top: 20%;
        }
    }
    p.wrap {
        width: 200px; 
        border: 1px solid #000000;
        word-wrap: break-word;
    }
    .judul_register{
        font-size: 15px;
    }
    .brand {
        position: absolute;
        left: 50%;
        margin-left: -70px !important;  /* 50% of your logo width */
        display: block;
    }
    .toggle-switch .ts-label {
        min-width: 130px;
    }
    html, body {
        /*margin: 0px;
        padding: 0px;*/
        /*height: 100%;*/
        overflow: auto;
    }
    @media screen and (min-width: 450px){
        #navbar_atas_1{
            background-color: #db1f00; 
            /*background:rgba(0,80,80,0.7);*/
        }
    }
    @media screen and (min-width: 850px){
        body{
            background-size: cover; 
            background-repeat: no-repeat; 
            background-attachment: fixed; 
            height: 100% width:100%;
            background-color: white;
            /*background-image:url(../aset/img/blur/img3.jpg);*/
        }
        .gender{
            margin-top: 5%;
        }
    }
    @media screen and (max-width: 450px){
        body {
            background-color: white;
        }
    }
</style>
<style type="text/css"> 
    input::-webkit-input-placeholder {
        color: grey !important;
    }

    #navbar_atas_m{
        background-color: #db1f00; 
        /*background:rgba(0,80,80,0.7);"*/
    }
    #button_murid
    {
        width: 105%;
    }
    #button_tutor
    {
        width: 105%;
    }
    #button_tutor2
    {
        margin-left: -10px;
    }
    #level_siswa
    {
        font-size: 10px;
    }
    #level_umum
    {
        font-size: 10px;
    }  
    @media screen and (min-width: 770px){
        #menu_register
        {
            margin-top: 5%;
        }
    }
    @media screen and (max-width: 1125px){
        #levelregister
        {
            font-size: 15px;
        }
    }
    @media screen and (max-width: 1074px){
        #levelregister
        {
            font-size: 13px;
        }
    }
    @media screen and (max-width: 1112px){
        #level_siswa
        {
            font-size: 9px;
        }
        #level_umum
        {
            font-size: 9px;
        }
    }
    @media screen and (max-width: 1037px){
        #level_siswa
        {
            font-size: 8px;
        }
        #level_umum
        {
            font-size: 8px;
        }
    }
    @media screen and (max-width: 767px){
        #nav_mobile
        {
            display: inline;
        }
        .gender{
            margin-top: 10%;
        }
        #level_siswa
        {
            font-size: 10px;
        }
        #level_umum
        {
            font-size: 10px;
        }
        #button_murid
        {
            width: 100%;
        }
        #button_tutor
        {
            width: 100%;
        }
        #button_tutor2
        {
            margin-left: 0%;
        }
    }
    @media screen and (max-width: 360px){
        #level_siswa
        {
            font-size: 12px;
        }
        #level_umum
        {
            font-size: 12px;
        }
        .gender{
            margin-top: 20%;
        }
    }
    @media screen and (max-width: 320px){
        .captcha
        {
        	text-align: center;
            transform:scale(0.85);-webkit-transform:scale(0.85);transform-origin:0 0;-webkit-transform-origin:0 0
        }
    }
</style>
<script type="text/javascript">
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    function show(elementId) {       
        document.getElementById("register_student").style.display="none";
        // document.getElementById("register_nonstudent").style.display="none";
        document.getElementById("register_menu").style.display="none";
        document.getElementById("register_murid").style.display="none";
        document.getElementById("home_atas").style.display="none";
        document.getElementById("back_menu_atas").style.display="none";
        document.getElementById("back_student_atas").style.display="none";
        document.getElementById(elementId).style.display="block";
    }
    var CaptchaCallback = function() {
        grecaptcha.render('RecaptchaField3', {'sitekey' : '6Le_mnwUAAAAAKloUTgurltTfRPvkj2UqVY-_9tE'});
        // grecaptcha.render('RecaptchaField4', {'sitekey' : '6LcEsSUTAAAAAJPFvRwClTlzAcQFQ6IsAiERxOLV'});
    };
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://apis.google.com/js/api:client.js"></script>
<script type="text/javascript">
    $(document).ready(function(){ 
            gapi.load('auth2', function(){
                auth2 = gapi.auth2.init({
                      client_id: '38717113134-d2hpda3q6ttec690s17sk50huqf68jtm.apps.googleusercontent.com',
                      cookiepolicy: 'single_host_origin',
                });
                attachSignin(document.getElementById('customBtn'));
                // attachSignin(document.getElementById('customBtnn'));
            });
            function attachSignin(element) {
                console.log(element.id);
                auth2.attachClickHandler(element, {},
                function(googleUser) {              
                    var profile = googleUser.getBasicProfile();                    
                    $('#google_data').html(JSON.stringify(profile));
                    $('#google_form').submit();
                }, 
                function(error) {
                });
            }
             function signOut() {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
                console.log('User signed out.');
                });
                FB.logout(function(response) {
                alert(JSON.stringify(response));
                });
                }
                function onLoad() {
                gapi.load('auth2', function() {
                auth2 = gapi.auth2.init({
                client_id: '38717113134-d2hpda3q6ttec690s17sk50huqf68jtm.apps.googleusercontent.com',
                  // scope: 'profile email'
                });
            });
        }        
        $('#f_signInbtn_login').click(function(){
            console.warn("klik");
            FB.login(function(response){
                if(response.status == 'connected'){
                    var new_data = {};
                    FB.api('/me', { locale: 'en_US', fields: 'name, email' }, function(response) {
                        console.log(JSON.stringify(response));
                        new_data['name'] = response.name;
                        new_data['email'] = response.email;
                        FB.api('/me/picture',{type: 'large'} ,function(response) {
                            console.log(JSON.stringify(response));
                            new_data['image'] = response.data.url;
                            new_data = JSON.stringify(new_data);
                            $('#fb_data').html(new_data);
                            $('#facebook_form').submit();
                        });
                    });
                }
            }, {scope: 'public_profile,email'});
        });
    }); 
    $(document).on('keyup', '[data-min_max]', function(e){
        var min = parseInt($(this).data('min'));
        var max = parseInt($(this).data('max'));
        var val = parseInt($(this).val());
        if(val > max)
        {
            $(this).val(max);
            return false;
        }
        else if(val < min)
        {
            $(this).val(min);
            return false;
        }
    });
    $(document).on('keydown', '[data-toggle=just_number]', function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode == 65 && e.ctrlKey === true) ||
            (e.keyCode == 67 && e.ctrlKey === true) ||
            (e.keyCode == 88 && e.ctrlKey === true) ||
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var code = null;        
        var cekk = setInterval(function(){
            code = "<?php echo $this->session->userdata('code');?>";
            cek();
        },500);

        function cek(){
            if (code == "121") {
                $("#register_modal").modal('show');
                $("#alertverifikasiemail").css('display','block');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: '<?php echo base_url();?>Rest/clearsession',
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
            else if (code =="870")
            {
                
                $("#facebook_modal").modal('show');
                $("#google_modal").modal('hide');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: '<?php echo base_url();?>Rest/clearsession',
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
            else if (code =="871")
            {
                
                $("#facebook_modal").modal('hide');
                $("#google_modal").modal('show');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: '<?php echo base_url();?>Rest/clearsession',
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
            else if (code == "204") {
                    $("#register_modal").modal('show');
                    $("#alertverifikasiemail").css('display','block');
                    code == null;
                    clearInterval(cekk);
                    $.ajax({
                        url: '<?php echo base_url(); ?>Rest/clearsession',
                        type: 'POST',
                        data: {
                            code: code
                        },
                        success: function(response)
                        { 
                            console.warn(response);
                        }
                    });
            }
        }
    });
</script>

<div class="modal    modal-dark" id="facebook_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
        <div class="modal-dialog modal-sm animated zoomIn animated-3x" role="document" style="margin-top: 5%;">
            <div class="modal-content">

                <div class="modal-header" style="text-align: center; background-color: #db1f00;">
                    <h3 class="modal-title" style="color: white;" id="myModalLabel3"><?php echo $this->lang->line('akunsudahada');?></h3>
                </div>

                <div class="modal-body">
                        <div class="lv-body" style="margin-top: 3%;">
                            <div class="lv-item"><center>                           
                                <?php echo $this->lang->line('akunfacebook');?></center>
                            </div>
                            <div class="modal-footer">                   
                                <button class="btn btn-raised btn-success pull-right " data-dismiss="modal" data-toggle="modal" aria-label="Close" name="loginFB" id="loginFB" data-dismiss="modal" data-toggle="modal" style="background-color: #db1f00;">OK</button>
                            </div>
                        </div>    

                </div>
            </div>
        </div>
</div>
<div class="modal   modal-dark" id="google_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
        <div class="modal-dialog modal-sm animated zoomIn animated-3x" role="document" style="margin-top: 5%;">
            <div class="modal-content">

                <div class="modal-header" style="text-align: center; background-color: #db1f00;">
                    <h3 class="modal-title" style="color: white;" id="myModalLabel3">Akun Sudah Terdaftar</h3>
                </div>

                <div class="modal-body">
                        <div class="lv-body" style="margin-top: 3%;">
                            <div class="lv-item"><center>                           
                                <?php echo $this->lang->line('akungoogle');?></center>
                            </div>
                            <div class="modal-footer">                   
                                <button class="btn btn-raised btn-success pull-right " data-dismiss="modal" data-toggle="modal" aria-label="Close" name="loginFB" id="loginFB" data-dismiss="modal" data-toggle="modal" style="background-color: #db1f00;">OK</button>
                            </div>
                        </div>    

                </div>
            </div>
        </div>
</div>
<div class=" modal fade" data-modal-color="teal" id="register_modal" tabindex="-1" data-backdrop="static" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
                <center>
                    <div class="modal-body" style="margin-top: 65%;" ><br>
                        
                       <div class="alert " style="display: block; ?>" id="alertverifikasiemail">
                            <?php echo $this->lang->line('successfullyregistered');?>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="btn_sukses_register" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                    </div>
                </center>
        </div>
    </div>
</div>
<div class="pull-right hidden-xs  header-inner" >
    <nav id="navbar_atas_1" class=" navbar navbar-default navbar-fixed-top" >
        <div class="col-sm-1">
        </div>
        <a id="logo_atas" class="navbar-brand " data-scroll href="<?php echo base_url(); ?>" style="text-align: center;">       
            <img style="margin-top: -9px;margin-left: -17px; height: 25px;" src="https://indiclass.id/aset/img/indiLogo2.png" alt="">
        </a>
        <div class=" nav navbar-nav navbar-right" style="margin-top: 16px; margin-right: 180px;">
            <a href="<?php echo base_url(); ?>" id="home_atas" type="button" class="c-white f-20" style=" cursor: pointer;">
                <i class="zmdi zmdi-home"></i>
            </a>
            <a hidden id="back_menu_atas" type="button" class="c-white f-20" onclick="show('register_menu')" style="cursor: pointer;">
                <i class="zmdi zmdi-arrow-left"></i>
            </a>
            <a hidden id="back_student_atas" type="button" class="c-white f-20" onclick="show('register_murid')" style="cursor: pointer;">
                <i class="zmdi zmdi-arrow-left"></i>
            </a>
        </div>
        <div class="col-sm-1"></div>    
        </div>
    </nav>
</div>
<div hidden id="nav_mobile" class="pull-right header-inner" >
    <nav id="navbar_atas_m" class=" navbar navbar-default navbar-fixed-top" >
        <div style=" margin-top: 12px; margin-left: 5%;">
            <a href="<?php echo base_url(); ?>" id="back_home" type="button" class="c-white f-20" style=" cursor: pointer;">
                <i class="zmdi zmdi-home"></i>
            </a>
            <a hidden id="back_menu" type="button" class="c-white f-20" onclick="show('register_menu')" style="cursor: pointer;">
                <i class="zmdi zmdi-arrow-left"></i>
            </a>
            <a hidden id="back_student" type="button" class="c-white f-20" onclick="show('register_murid')" style="cursor: pointer;">
                <i class="zmdi zmdi-arrow-left"></i>
            </a>
        </div>

    </nav>
</div>

<div id="menu_register" >
  <?php
      function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
          $output = NULL;
          if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
              $ip = $_SERVER["REMOTE_ADDR"];
              if ($deep_detect) {
                  if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                  if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                      $ip = $_SERVER['HTTP_CLIENT_IP'];
              }
          }
          $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
          $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
          $continents = array(
              "AF" => "Africa",
              "AN" => "Antarctica",
              "AS" => "Asia",
              "EU" => "Europe",
              "OC" => "Australia (Oceania)",
              "NA" => "North America",
              "SA" => "South America"
          );
          if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
              $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
              if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                  switch ($purpose) {
                      case "location":
                          $output = array(
                              "city"           => @$ipdat->geoplugin_city,
                              "state"          => @$ipdat->geoplugin_regionName,
                              "country"        => @$ipdat->geoplugin_countryName,
                              "country_code"   => @$ipdat->geoplugin_countryCode,
                              "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                              "continent_code" => @$ipdat->geoplugin_continentCode
                          );
                          break;
                      case "address":
                          $address = array($ipdat->geoplugin_countryName);
                          if (@strlen($ipdat->geoplugin_regionName) >= 1)
                              $address[] = $ipdat->geoplugin_regionName;
                          if (@strlen($ipdat->geoplugin_city) >= 1)
                              $address[] = $ipdat->geoplugin_city;
                          $output = implode(", ", array_reverse($address));
                          break;
                      case "city":
                          $output = @$ipdat->geoplugin_city;
                          break;
                      case "state":
                          $output = @$ipdat->geoplugin_regionName;
                          break;
                      case "region":
                          $output = @$ipdat->geoplugin_regionName;
                          break;
                      case "country":
                          $output = @$ipdat->geoplugin_countryName;
                          break;
                      case "countrycode":
                          $output = @$ipdat->geoplugin_countryCode;
                          break;
                  }
              }
          }
          return $output;
      }
  ?>

    <div class="col-sm-3"></div>
    <div class="col-sm-6" style=" background-color: white; border-color: white; border-style: solid;">
            
      <!--Menu pilihan register -->
    <div class="tn-justified f-16 " style=" background-color: white;" id="register_menu">
            <div hidden id="nav_mobile" class="pull-right header-inner" >
              <nav id="navbar_atas_m" class=" navbar navbar-default navbar-fixed-top" >
                  <div style=" margin-left: 5%;">
                      <a hidden id="back_menu" type="button" class="c-white f-20" onclick="show('register_menu')" style="cursor: pointer;">
                          <i class="zmdi zmdi-arrow-left"></i>
                      </a>
                      <a hidden id="back_student" type="button" class="c-white f-20 back_student" onclick="show('register_murid')" style="cursor: pointer;">
                          <i class="zmdi zmdi-arrow-left"></i>
                      </a>
                  </div>
                  <a id="logo_mobile" style="margin-top: 2%" class="brand " data-scroll href="<?php echo base_url(); ?>">     
                      <img  src="https://indiclass.id/aset/img/indiLogo2.png" style="height: 25px;" alt="">
                  </a>
              </nav>
            </div>
            <div class="modal fade" style="" id="modal_loading" tabindex="-1" data-backdrop="static" role="dialog">
                <div class="modal-dialog" role="document" style="background:rgba(0,00,00,0.0);">   
                	<div style="margin-top: 50%;">
	                    <center>
	                       <img style="width: 30%" src="https://www.silver-peak.com/sites/all/themes/silverpeak/images/loader.gif"><br>
	                       <label style="color: white;">Loading...</label>
	                    </center>
	                </div>
                </div>
            </div>
            <div class="c-teal col-sm-12 btn-lg m-b-10 m-t-5 text-center"><b><?php echo $this->lang->line('sayaadalah'); ?>...</b></div>  
            <div class="radio jenis_signup m-b-15">
                <label>
                    <input id="rd_ortu" type="radio" name="jenis_signup" value="ortu">
                    <i class="input-helper"></i><b style="font-weight: bold; text-align: left;"><?php echo $this->lang->line('ortu'); ?></b><br>
                    <?php echo $this->lang->line('detail_ortu'); ?>
                </label>
            </div>
            <div class="radio jenis_signup m-b-15">
                <label>
                    <input id="rd_siswa"  type="radio" name="jenis_signup" value="siswa">
                    <i class="input-helper"></i>
                    <b><?php echo $this->lang->line('detail_siswa'); ?></b>
                </label>
            </div>
            <div class="radio jenis_signup m-b-15">
                <label>
                    <input id="rd_umum"  type="radio" name="jenis_signup" value="umum">
                    <i class="input-helper"></i>
                    <b><?php echo $this->lang->line('detail_umum'); ?></b>
                </label>
            </div>
            <div class="radio jenis_signup m-b-15">
                <label>
                    <input id="rd_tutor"  type="radio" name="jenis_signup" value="tutor">
                    <i class="input-helper"></i><b style="font-weight: bold; text-align: left;"><?php echo $this->lang->line('tutor'); ?></b><br>
                    <?php echo $this->lang->line('detail_tutor'); ?>
                </label>
            </div>
            

          <!-- <div class="col-sm-5 text-center " style="">
              <div id="button_murid" style="cursor: pointer;" class="" onclick="show('register_student')">
                   <a href="" style=" height: 56px;" class="c-white btn-lg bgm-red waves-effect btn-block"><?php echo $this->lang->line('pelajar'); ?><p  id="level_siswa" style="margin-top: 0px; margin-bottom: 2px;"><?php echo $this->lang->line('levelpelajar'); ?></p></a>
              </div>
          </div>
          <div class="col-sm-2 text-center btn-lg " style=""><?php echo $this->lang->line('or'); ?></div>

          <div id="button_tutor2" class="col-sm-5 text-center " style="">
              <div id="button_tutor" style="cursor: pointer;" class="">
                 <a href="<?php echo base_url(); ?>RegisterTutor" style=" height: 56px;" class="c-white btn-lg bgm-indigo waves-effect btn-block"><?php echo $this->lang->line('tutor'); ?><p  id="level_umum" style="margin-top: 0px; margin-bottom: 2px;"><?php echo $this->lang->line('leveltutor'); ?></p></a>
              </div>
          </div> -->

          <div id="btn_signup" class="col-sm-5 text-center " style="display: none;" onclick="show('register_student')" >
              <div id="" style="cursor: pointer;" class="">
                 <a href="#" style="" class="c-white btn-lg bgm-orange waves-effect btn-block"><?php echo $this->lang->line('next'); ?></a>
              </div>
          </div>
           <div id="btn_signup_tutor"  class="col-sm-5 text-center " style="display: none;" >
              <div id="" style="cursor: pointer;" class="">
                 <a href="<?php echo base_url(); ?>RegisterTutor" style="" class="c-white btn-lg bgm-orange waves-effect btn-block"><?php echo $this->lang->line('next'); ?></a>
              </div>
          </div>
          <div><hr></div>
          <div class="col-sm-12 m-b-5 text-center">
              <label class="fg-label f-12 m-r-20"><?php echo $this->lang->line('alreadyaccount'); ?> 
                  <a id="nav_login"  class="c-blue" style="cursor: pointer;"><?php echo $this->lang->line('here'); ?></a>
              </label>
          </div>
          <div class="col-sm-12 text-center" >
              <?php             
                  if($this->session->flashdata('mes_alert')){                         
              ?>
              <div class="m-t-10 alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" id='flashdata' style=" display: <?php echo $this->session->flashdata('mes_display'); ?>">
                <a   href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->session->flashdata('mes_message'); ?>
              </div>
              <?php } ?> 
          </div>
    </div>

      <div hidden class="tn-justified f-16 " style="background-color: white;" id="register_murid">
          <div class="lv-header hidden-xs" style="background-color: white; height: 65px;">
              <div class="c-teal judul_register m-t-10 m-b-10" style="font-weight: bold; font-family: 'Trebuchet MS'; font-style:normal;"><?php echo $this->lang->line('pendaftaransiswa'); ?></div>
                  <div class=""  style="text-align-last: left;  margin-top: -26px;" >
                      <a id="back_home" type="button" class="c-teal f-19 m-l-10" onclick="show('register_menu')" style="cursor: pointer;">
                          <i class="zmdi zmdi-arrow-left"></i>
                      </a>
                  </div> 
          </div><br>
          <div hidden id="nav_mobile" class="pull-right header-inner" >
              <nav id="navbar_atas_m" class=" navbar navbar-default navbar-fixed-top" >
                  <div style=" margin-top: 12px; margin-left: 5%;">
                      <a  id="back_menu" type="button" class="c-white f-20" onclick="show('register_menu')" style="cursor: pointer;">
                          <i class="zmdi zmdi-arrow-left"></i>
                      </a>
                      <a hidden id="back_student" type="button" class="c-white f-20" onclick="show('register_murid')" style="cursor: pointer;">
                          <i class="zmdi zmdi-arrow-left"></i>
                      </a>

                  </div>
              </nav>
          </div>  
          <div class="col-sm-12 text-center " style="">
              <p  id="levelregister"><?php echo $this->lang->line('pilihlevel'); ?></p>
          </div>
          <div class="col-sm-5 text-center " style="">
              <div id="" style="cursor: pointer;background-color: white;" class="c-white btn-lg bgm-orange waves-effect btn-block" onclick="show('register_student')">
                  <p><?php echo $this->lang->line('sstudent'); ?></p><p id="level_siswa" style="margin-top: -20px; margin-bottom: 2px;"><?php echo $this->lang->line('levelsiswa'); ?></p>
              </div>
          </div>
          <div class="col-sm-2 text-center btn-lg " style=""><?php echo $this->lang->line('or'); ?></div>
          <div class="col-sm-5 text-center " style="">
              <div id="" style="cursor: pointer;" class="c-white btn-lg bgm-blue waves-effect btn-block">
                  <p><?php echo $this->lang->line('nonstudent'); ?></p><p id="level_umum" style="margin-top: -20px; margin-bottom: 2px;"><?php echo $this->lang->line('levelumum'); ?></p>
              </div>
          </div>
          <div class="col-sm-12 m-b-10 text-center"><br>
              <br> 
          </div>
      </div>

    <div hidden="true" id="register_student" style="background-color: white; margin-top: 5%;">
            <div class="lv-header hidden-xs" style="background-color: white; height: 65px;">
                <div class="c-teal judul_register m-t-10 m-b-10" style="font-weight: bold; font-family: 'Trebuchet MS'; font-style:normal;">Blabla<p id="judul_pendaftaran">bla</p></div>
                <div class=""  style="text-align-last: left;  margin-top: -26px;" >
                    <a id="back_home" type="button" class="c-teal f-19 m-l-10" onclick="show('register_menu')" style="cursor: pointer;">
                  <i class="zmdi zmdi-arrow-left"></i>
                  </a>
                </div><br>
            </div>
            <div class="modal fade" data-modal-color="teal" id="modal_cekumur" tabindex="-1" data-backdrop="static" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                            <center>
                                <div class="modal-body" style="margin-top: 15%;" ><br>
                                    
                                   <div class="alert " style="display: block; ?>" id="alertumur">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                                </div>
                            </center>
                    </div>
                </div>
            </div>
            <div hidden id="nav_mobile" class="pull-right header-inner" >
              <nav id="navbar_atas_m" class=" navbar navbar-default navbar-fixed-top" >
                <div style=" margin-top: 12px; margin-left: 5%;">
                  <a  id="back_menu" type="button" class="c-white f-20" onclick="show('register_menu')" style="cursor: pointer;">
                    <i class="zmdi zmdi-arrow-left"></i>
                  </a>
                  <a hidden id="back_student" type="button" class="c-white f-20" onclick="show('register_murid')" style="cursor: pointer;">
                    <i class="zmdi zmdi-arrow-left"></i>
                  </a>
                </div>
                <a id="logo_mobile" style="margin-top: -9%;" class="brand " data-scroll href="<?php echo base_url(); ?>">     
                <img style="height: 25px;"  src="https://indiclass.id/aset/img/indiLogo2.png" alt="">
                </a>
              </nav>
            </div>
            <div id="tabber" class="form-wizard-basic fw-container" style="margin-left: 5%; margin-right: 5%;" >
                <div class="">
                    <div class="tab-pane " id="tab1" >
                        <form role="form" action="<?php  echo base_url(); ?>master/daftarbaru" method="post" novalidate>
                            <div class="row" >
                                <div class="col-sm-2 text-center"></div>
                                <div class="col-sm-8 text-center">
                                    <div id="f_signInbtn" style="cursor: pointer;" class="btn bgm-facebook waves-effect btn-block">
                                        <i class="zmdi zmdi-facebook"></i>&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <?php echo $this->lang->line('signupfacebook'); ?>
                                    </div>
                                    <div class="col-sm-8 text-center"><br></div>
                                    <div id="customBtn" style="cursor: pointer;" class="btn bgm-google waves-effect btn-block">
                                        <i class="zmdi zmdi-google"></i>&nbsp;&nbsp;
                                            <?php echo $this->lang->line('signupgoogle'); ?>
                                    </div><br><br>
                                    <label class="fg-label f-12 m-r-20 m-b-20"><?php echo $this->lang->line('orsignup'); ?></label>
                                                <!-- <script>startApp();</script> -->
                                </div>
                                <div class="col-sm-12 text-center" style="margin-top: -3%;"><hr></div>
                                    <!-- <input type="text" name="tab" style="display: none;" value="1"> -->
                                <div class="col-xs-6">
                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <input type="text" name="first_name" required class="input-sm form-control fg-input">
                                            <label class="fg-label"><?php echo $this->lang->line('first_name'); ?></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <input type="text" name="last_name" required class="input-sm form-control fg-input">
                                            <label class="fg-label"><?php echo $this->lang->line('last_name'); ?></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 m-t-5">
                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <input type="email" name="email" id="email" required class="input-sm form-control fg-input">
                                            <label class="fg-label">Email</label>
                                        </div>
                                    </div>
                                    <small id="capemail" style="color:red; display: none;"><?php echo $this->lang->line('invalidemail');?></small>
                                </div>
                                <div class="col-xs-12 m-t-5">
                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <input type="password" name="kata_sandi" id="kata_sandi_student" required class="input-sm form-control fg-input">
                                            <label class="fg-label"><?php echo $this->lang->line('password'); ?></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 m-t-5">
                                    <div class="form-group fg-float ">
                                        <div class="fg-line">
                                            <input type="password" name="konf_kata_sandi" id="konf_kata_sandi_student" required class="input-sm form-control fg-input">
                                            <label class="fg-label"><?php echo $this->lang->line('confirm_pass'); ?></label>
                                        </div>
                                        <small id="cek_sandi" style="color: red;"></small>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <label><?php echo $this->lang->line('birthday'); ?> :</label>
                                </div>
                                <div style="cursor: pointer;" class="col-xs-4 col-sm-4">
                                    <select placeh required name="tanggal_lahir" id="tanggal_lahir" class="select form-control" data-live-search="true">
                                        <option disabled selected  value=''><?php echo $this->lang->line('datebirth'); ?></option>
                                            <?php 
                                            for ($date=01; $date <= 31; $date++) {
                                                ?>                                                  
                                                <option value="<?php echo $date ?>" <?php if(isset($form_cache) && $form_cache['tanggal_lahir'] == $date){ echo 'selected="true"';} ?>><?php echo $date ?></option>                                                  
                                                <?php 
                                            }
                                            ?>
                                    </select>
                                </div>
                                <div style="cursor: pointer;" class="col-xs-4 col-md-4">
                                        <select id="bulan_lahir" required name="bulan_lahir" class="select form-control" data-live-search="true">
                                            <option disabled selected value=''><?php echo $this->lang->line('monthbirth'); ?></option>
                                            <option value="01" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "01"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('jan'); ?></option>
                                            <option value="02" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "02"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('feb'); ?></option>
                                            <option value="03" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "03"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('mar'); ?></option>
                                            <option value="04" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "04"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('apr'); ?></option>
                                            <option value="05" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "05"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('mei'); ?></option>
                                            <option value="06" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "06"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('jun'); ?></option>
                                            <option value="07" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "07"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('jul'); ?></option>
                                            <option value="08" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "08"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('ags'); ?></option>
                                            <option value="09" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "09"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('sep'); ?></option>
                                            <option value="10" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "10"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('okt'); ?></option>
                                            <option value="11" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "11"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('nov'); ?></option>
                                            <option value="12" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "12"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('des'); ?></option>
                                        </select>
                                </div>
                                <div style="cursor: pointer;" class="col-xs-4 col-md-4">
                                    <select onchange="checkBOD()" required name="tahun_lahir" id="tahun_lahir" class="select form-control" data-live-search="true">
                                        <option disabled selected value=''><?php echo $this->lang->line('yearbirth'); ?></option>
                                        <?php 
                                        for ($i=1960; $i <= 2016; $i++) {
                                            ?>                                                 
                                            <option value="<?php echo $i; ?>" <?php if(isset($form_cache) && $form_cache['tahun_lahir'] == $i){ echo 'selected="true"';} ?>><?php echo $i; ?></option>
                                            <?php 
                                        } 
                                        ?>
                                    </select>
                                </div>
                                <div style="" class="m-t-20 col-xs-12 gender">
                                    <div class="fg-line">
                                        <label class="radio radio-inline ">
                                            <input type="radio" name="jenis_kelamin" value="Male">
                                            <i class="input-helper"></i><?php echo $this->lang->line('male'); ?>  
                                        </label>
                                        
                                        <label class="radio radio-inline " >
                                            <input type="radio" name="jenis_kelamin" value="Female">
                                            <i class="input-helper"></i><?php echo $this->lang->line('women'); ?>  
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-12 m-t-20">
                                    <label><?php echo $this->lang->line('kewarganegaraan'); ?> :</label>
                                </div>
                                <div class="col-xs-12 col-md-12">
                                    <select required name="nama_negara" class="selectpicker form-control" data-live-search="true">
                                      <?php                       
                                        $nama_negara = $this->session->userdata('get_location');
                                        $db = $this->db->query("SELECT * FROM `master_country` WHERE iso='$nama_negara'")->row_array();
                                        $negara = $db['phonecode'];
                                        $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                                        foreach ($allsub as $row => $d) {
                                            if($negara == $d['phonecode']){
                                                echo "<option value='".$d['nicename']."' selected='true'>".$d['nicename']."</option>";
                                            }
                                            if ($negara != $d['phonecode']) {
                                                echo "<option value='".$d['nicename']."'>".$d['nicename']."</option>";
                                            }                                                            
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xs-12 m-t-20">
                                    <label><?php echo $this->lang->line('mobile_phone'); ?> :</label>
                                </div>
                                <div class="col-xs-6 col-md-5">
                                    <select required name="kode_area" class="selectpicker form-control" data-live-search="true">
                                      <?php                       
                                        $nama_negara = $this->session->userdata('get_location');
                                        $db = $this->db->query("SELECT * FROM `master_country` WHERE iso='$nama_negara'")->row_array();
                                        $phone = $db['phonecode'];
                                        $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                                        foreach ($allsub as $row => $d) {
                                            if($phone == $d['phonecode']){
                                                echo "<option value='".$d['phonecode']."' selected='true'>".$d['nicename']." +".$d['phonecode']."</option>";
                                            }
                                            if ($phone != $d['phonecode']) {
                                                echo "<option value='".$d['phonecode']."'>".$d['nicename']." +".$d['phonecode']."</option>";
                                            }                                                            
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-xs-6 col-md-7" >
                                    <div class="fg-line">
                                        <input type="text" id="no_hape" minlength="9" maxlength="13" name="no_hape" onkeyup="validPhone(this)"  onkeyup="validAngka(this)" onkeypress="return hanyaAngka(event)" required class="form-control " placeholder="87XXXXXX" value="<?php if(isset($form_cache)){ echo $form_cache['no_hape'];} ?>">
                                    </div>
                                </div>  
                                <div hidden class="col-xs-12 m-t-20">
                                  <label><?php echo $this->lang->line('sayaadalah'); ?></label>
                                </div>
                                <div hidden style="cursor: pointer;" class="col-md-12" id="combopekerjaan">
                                    <div class="fg-line">
                                        <div class="select" >
                                            <input type="text" id="level_pekerjaan" name="level_pekerjaan" style="display: none;">
                                            <select class="form-control" id='select_pekerjaan' onchange="chooseOccupation()" data-placeholder="Select Occupation">
                                                <option  value="" style="cursor: default;">
                                                 <?php echo $this->lang->line('pilihpekerjaan'); ?> 
                                                </option>
                                                <option value="Student">
                                                 <?php echo $this->lang->line('daftar_siswa'); echo "  "; echo $this->lang->line('levelsiswa'); ?> 
                                                </option>
                                                <option value="General">
                                                <?php echo $this->lang->line('daftar_umum'); echo "  "; echo $this->lang->line('levelumum'); ?>
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>               
                                <div style="cursor: pointer;"  class="col-xs-12 m-t-20" id="combojenjang">
                                    <div class="fg-line">
                                        <input type="text" name="tulisan_jenjang_id" id="tulisan_jenjang_id" style="display: none" value="<?php if(isset($form_cache)){ echo $form_cache['tulisan_jenjang_id'];} ?>">
                                        <select required class="select2 form-control" id='jenjang_level' data-placeholder="<?php echo $this->lang->line('select_class'); ?>">
                                            <option disabled selected value=''><?php echo $this->lang->line('select_class'); ?></option>
                                            <?php 
                                            if(isset($form_cache)){
                                                echo '<option value="'.$form_cache['jenjang_id'].'" >'.$form_cache['tulisan_jenjang_id'].'</option>';
                                            } 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" name="jenjang_id" id="jenjang_id" value="">
                                <center>
                                    <div class=" col-xs-12 captcha" ><br><br>
                                        <div class="fg-line" id="RecaptchaField3">
                                        </div>
                                    </div>
                                </center>
                                <div class="col-xs-12 m-t-5" style="">
                                    <label id='cek_syarat' >
                                        <input type="checkbox" value="oke" id="term_agreement">
                                        <?php echo $this->lang->line('syarat');?> <a target="_blank" class="c-blue" href="<?php echo base_url(); ?>syarat-ketentuan"><?php echo $this->lang->line('syarat1'); ?></a> <?php echo $this->lang->line('syarat2'); ?>
                                    </label>
                                </div>
                                <div class="col-xs-12 m-t-15">
                                    <button type="submit" class="btn bgm-teal btn-block" id="submit_signup_student"><?php echo $this->lang->line('btnsignup'); ?></button><br><br><br>
                                </div>       
                            </div>
                        </form>
                    </div>
                </div>
            </div>    
    </div>
    <div hidden="true" id="register_nonstudent">
        <div class="lv-header hidden-xs" style="background-color: white; height: 65px;">
          <div class="c-teal judul_register m-t-10 m-b-10" style="font-weight: bold; font-family: 'Trebuchet MS'; font-style:normal;"></div>
          <div class=""  style="text-align-last: left;  margin-top: -26px;" >
            <a id="back_home" type="button" class="c-teal f-19 m-l-10" onclick="show('register_murid')" style="cursor: pointer;">
            <i class="zmdi zmdi-arrow-left"></i>
            </a>
          </div><br> 
        </div>
        <div hidden id="nav_mobile" class="pull-right header-inner" >
          <nav id="navbar_atas_m" class=" navbar navbar-default navbar-fixed-top" >
            <div style=" margin-top: 12px; margin-left: 5%;">
              <a  id="back_menu" type="button" class="c-white f-20" onclick="show('register_menu')" style="cursor: pointer;">
                <i class="zmdi zmdi-arrow-left"></i>
              </a>
              <a hidden id="back_student" type="button" class="c-white f-20" onclick="show('register_murid')" style="cursor: pointer;">
                <i class="zmdi zmdi-arrow-left"></i>
              </a>
            </div>
            <a id="logo_mobile" style="margin-top: -9%;" class="brand " data-scroll href="<?php echo base_url(); ?>">     
            <img style="height: 25px;"  src="https://indiclass.id/aset/img/indiLogo2.png" alt="">
            </a>
          </nav>
        </div>
                
        <div id="tabber" class="form-wizard-basic fw-container" style="margin-left: 5%; margin-right: 5%;" >
          <div class="">
                        <div class="tab-pane" id="tab2">
                            <form role="form" action="<?php  echo base_url(); ?>master/daftarbaru" method="post" novalidate>
                                <div class="row">
                                        <div class="col-sm-2 text-center"></div>
                                        <div class="col-sm-8 text-center">
                                            
                                            <!-- <button class="btn bgm-facebook waves-effect btn-icon-text btn-block"><i class="zmdi zmdi-facebook"></i> Mendaftar dengan Facebook</button> -->
                                            <div  id="f_signInbtn2" style="cursor: pointer;" class="btn bgm-facebook waves-effect btn-block"><i class="zmdi zmdi-facebook"></i>&nbsp;&nbsp;  <?php echo $this->lang->line('signupfacebook'); ?></div>
                                            <div class="col-sm-12"><br></div>
                                            <div id="customBtnn" style="cursor: pointer;" class=" btn bgm-google waves-effect btn-block"><i class="zmdi zmdi-google"></i>&nbsp;&nbsp; &nbsp;&nbsp;  <?php echo $this->lang->line('signupgoogle'); ?></div><br><br>
                                            <label class="fg-label f-12 m-r-20 m-b-20"><?php echo $this->lang->line('orsignup'); ?>
                                            </label>
                                        </div>
                                        <div class="col-sm-12 text-center" style="margin-top: -3%;"><hr></div>

                                    <input type="text" name="tab" style="display: none;" value="2">

                                    <div class="col-sm-6">
                                        <div class="form-group fg-float">
                                            <div class="fg-line">
                                                <input type="text" name="first_name" required class="input-sm form-control fg-input" value="<?php if(isset($form_cache)){ echo $form_cache['first_name'];} ?>">
                                                <label class="fg-label"><?php echo $this->lang->line('first_name'); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group fg-float">
                                            <div class="fg-line">
                                                <input type="text" name="last_name" required class="input-sm form-control fg-input" value="<?php if(isset($form_cache)){ echo $form_cache['last_name'];} ?>">
                                                <label class="fg-label"><?php echo $this->lang->line('last_name'); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 m-t-5">
                                        <div class="form-group fg-float">
                                            <div class="fg-line">
                                                <input type="email" name="email" id="emaill_nonstudent" class="input-sm form-control fg-input" value="<?php if(isset($form_cache)){ echo $form_cache['email'];} ?>">
                                                <label class="fg-label">Email</label>
                                            </div>
                                            <small id="capemaill" style="color:red; display: none;"><?php echo $this->lang->line('invalidemail');?></small>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 m-t-5">
                                        <div class="form-group fg-float">
                                            <div class="fg-line">
                                                <input type="password" name="kata_sandi" id="kata_sandi_nonstudent" required class="input-sm form-control fg-input">
                                                <label class="fg-label"><?php echo $this->lang->line('password'); ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 m-t-5">
                                        <div class="form-group fg-float">
                                            <div class="fg-line">
                                                <input type="password" name="konf_kata_sandi" id="konf_kata_sandi_nonstudent" required class="input-sm form-control fg-input">
                                                <label class="fg-label"><?php echo $this->lang->line('confirm_pass'); ?></label><br>
                                            </div>
                            <small id="cek_sandi_umum" style="color: red;"></small>
                                        </div>
                                    </div>
                                        <div class="col-md-12">
                                        <label><?php echo $this->lang->line('birthday'); ?> :</label>
                                    </div>
                  <div style="cursor: pointer;" class="col-xs-3 col-sm-3">
                      <select placeh required name="tanggal_lahir" id="tanggal_lahir_nonstudent" class="select form-control" data-live-search="true">
                          <option disabled selected  value=''><?php echo $this->lang->line('datebirth'); ?></option>
                          <?php 
                          for ($date=01; $date <= 31; $date++) {
                              ?>                                                  
                              <option value="<?php echo $date ?>" <?php if(isset($form_cache) && $form_cache['tanggal_lahir'] == $date){ echo 'selected="true"';} ?>><?php echo $date ?></option>                                                  
                              <?php 
                          }
                          ?>
                      </select>
                  </div>
                  <div style="cursor: pointer;" class="col-xs-5 col-md-5">
                      <select required name="bulan_lahir" class="select form-control" data-live-search="true">
                        <option disabled selected value=''><?php echo $this->lang->line('monthbirth'); ?></option>
                        <option value="01" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "01"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('jan'); ?></option>
                        <option value="02" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "02"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('feb'); ?></option>
                        <option value="03" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "03"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('mar'); ?></option>
                        <option value="04" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "04"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('apr'); ?></option>
                        <option value="05" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "05"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('mei'); ?></option>
                        <option value="06" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "06"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('jun'); ?></option>
                        <option value="07" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "07"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('jul'); ?></option>
                        <option value="08" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "08"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('ags'); ?></option>
                        <option value="09" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "09"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('sep'); ?></option>
                        <option value="10" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "10"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('okt'); ?></option>
                        <option value="11" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "11"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('nov'); ?></option>
                        <option value="12" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "12"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('des'); ?></option>
                      </select>
                  </div>
                  <div  style="cursor: pointer;" class="col-xs-4 col-md-4">
                      <select required name="tahun_lahir" id="tahun_lahir_nonstudent" class="select form-control" data-live-search="true">
                        <option disabled selected value=''><?php echo $this->lang->line('yearbirth'); ?></option>
                        <?php 
                        for ($i=1960; $i <= 2016; $i++) {
                          ?>                                                 
                          <option value="<?php echo $i; ?>" <?php if(isset($form_cache) && $form_cache['tahun_lahir'] == $i){ echo 'selected="true"';} ?>><?php echo $i; ?></option>
                          <?php 
                        } 
                        ?>
                      </select>
                  </div>
                                     <div class="col-md-12 gender" >
                                        <div class="fg-line">
                                            <label class="radio radio-inline ">
                      <input type="radio" name="jenis_kelamin" value="Male" <?php if(isset($form_cache) && $form_cache['jenis_kelamin'] == "Male"){ echo 'selected="true"';} ?>>
                      <i class="input-helper"></i><?php echo $this->lang->line('male'); ?>  
                      </label>
                      
                      <label class="radio radio-inline " >
                      <input type="radio" name="jenis_kelamin" value="Female" <?php if(isset($form_cache) && $form_cache['jenis_kelamin'] == "Female"){ echo 'selected="true"';} ?>>
                      <i class="input-helper"></i><?php echo $this->lang->line('women'); ?>  
                      </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 m-t-20">
                                        <label><?php echo $this->lang->line('mobile_phone'); ?> :</label>
                                    </div>
                                    <div class="col-xs-6 col-md-5">
                                            <select required name="kode_area" class="selectpicker form-control" data-live-search="true">
                                                <?php                       
                          $nama = ip_info("Visitor", "Country");
                          $db = $this->db->query("SELECT * FROM `master_country` WHERE nicename='$nama'")->row_array();
                          $phone = $db['phonecode'];
                          $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                          foreach ($allsub as $row => $d) {
                              if($phone == $d['phonecode']){
                                  echo "<option value='".$d['phonecode']."' selected='true'>".$d['nicename']." +".$d['phonecode']."</option>";
                              }
                              if ($phone != $d['phonecode']) {
                                  echo "<option value='".$d['phonecode']."'>".$d['nicename']." +".$d['phonecode']."</option>";
                              }                                                            
                          }
                          ?>
                                            </select>
                                    </div>

                                    <div class="col-xs-6 col-md-7">
                                        <div class="fg-line">
                                            <input type="text" id="no_hape_nonstudent" minlength="9" maxlength="13" name="no_hape" onkeyup="validPhone(this)"  onkeyup="validAngka(this)" onkeypress="return hanyaAngka(event)"  required class="form-control " placeholder="87XXXXXX" value="<?php if(isset($form_cache)){ echo $form_cache['no_hape'];} ?>">
                                        </div>
                                    </div> 
                                    <center>
                                        <div class="col-xs-12 captcha" ><br><br>
                                            <div class="fg-line" id="RecaptchaField4">
                                            </div>

                                        </div>
                                    </center>

                                    <div class="col-md-12 m-t-5">
                                        <label>
                                            <input type="checkbox" value="oke" id="term_agreementt_nonstudent">
                                            <?php echo $this->lang->line('syarat');?> <a target="_blank" class="c-blue" href="<?php echo base_url(); ?>syarat-ketentuan"><?php echo $this->lang->line('syarat1'); ?></a> <?php echo $this->lang->line('syarat2'); ?>
                                        </label>
                                    </div>


                                    <div class="col-sm-12 m-t-20">
                                        <button class="btn bgm-teal btn-block" type="submit" id="submit_signup"><?php echo $this->lang->line('btnsignup'); ?></button><br><br><br>
                                    </div>


                                    <!-- <div class="col-sm-12 m-t-20 text-center">
                                        <label class="fg-label f-14 m-r-20"><?php echo $this->lang->line('alreadyaccount'); ?>
                                        <a id="nav_loginn" style="cursor: pointer;"><?php echo $this->lang->line('here'); ?></a>
                                        </label>
                                        <hr>
                                    </div> -->

                                    
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

      <div hidden="true" id="register_thirdparty">
          <div class="lv-header hidden-xs" style="background-color: white; height: 65px;">
              <div class="c-teal judul_register m-t-10 m-b-10" style="font-weight: bold; font-family: 'Trebuchet MS'; font-style:normal;"><?php echo $this->lang->line('sebagai'); ?> <?php echo $this->lang->line('nonstudent'); ?>                
              </div>
              <div class=""  style="text-align-last: left;  margin-top: -26px;" >
                  <a id="back_home" type="button" class="c-teal f-19 m-l-10" onclick="show('register_murid')" style="cursor: pointer;">
                      <i class="zmdi zmdi-arrow-left"></i>
                  </a>
              </div>
              <br> 
          </div>
          <div hidden id="nav_mobile" class="pull-right header-inner" >
              <nav id="navbar_atas_m" class=" navbar navbar-default navbar-fixed-top" >
                  <div style=" margin-top: 12px; margin-left: 5%;">
                      <a  id="back_menu" type="button" class="c-white f-20" onclick="show('register_menu')" style="cursor: pointer;">
                          <i class="zmdi zmdi-arrow-left"></i>
                      </a>
                      <a hidden id="back_student" type="button" class="c-white f-20" onclick="show('register_murid')" style="cursor: pointer;">
                          <i class="zmdi zmdi-arrow-left"></i>
                      </a>
                  </div>
                  <a id="logo_mobile" style="margin-top: -9%;" class="brand " data-scroll href="<?php echo base_url(); ?>">     
                      <img style="height: 25px;"  src="https://indiclass.id/aset/img/indiLogo2.png" alt="">
                  </a>
              </nav>
          </div>

          <div id="tabber" class="form-wizard-basic fw-container" style="margin-left: 5%; margin-right: 5%;" >
              <div class="">
                  <div class="tab-pane" id="tab2">
                      <form role="form" action="<?php  echo base_url(); ?>master/daftarbaru" method="post">
                          <div class="row">
                              <div class="col-sm-2 text-center"></div>
                              <div class="col-sm-8 text-center">                              
                                  <div  id="f_signInbtn2" style="cursor: pointer;" class="btn bgm-facebook waves-effect btn-block"><i class="zmdi zmdi-facebook"></i>&nbsp;&nbsp;  <?php echo $this->lang->line('signupfacebook'); ?>
                                  </div>
                                  <div class="col-sm-12"><br></div>
                                  <div id="customBtnn" style="cursor: pointer;" class=" btn bgm-google waves-effect btn-block"><i class="zmdi zmdi-google"></i>&nbsp;&nbsp; &nbsp;&nbsp;  <?php echo $this->lang->line('signupgoogle'); ?>
                                  </div>
                                  <br><br>
                                  <label class="fg-label f-12 m-r-20 m-b-20"><?php echo $this->lang->line('orsignup'); ?>
                                  </label>
                              </div>
                              <div class="col-sm-12 text-center" style="margin-top: -3%;"><hr></div>
                                  <input type="text" name="tab" style="display: none;" value="2">
                                  <div class="col-sm-6">
                                      <div class="form-group fg-float">
                                          <div class="fg-line">
                                              <input type="text" name="first_name" required class="input-sm form-control fg-input" value="<?php if(isset($form_cache)){ echo $form_cache['first_name'];} ?>">
                                              <label class="fg-label"><?php echo $this->lang->line('first_name'); ?></label>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-6">
                                      <div class="form-group fg-float">
                                          <div class="fg-line">
                                              <input type="text" name="last_name" required class="input-sm form-control fg-input" value="<?php if(isset($form_cache)){ echo $form_cache['last_name'];} ?>">
                                              <label class="fg-label"><?php echo $this->lang->line('last_name'); ?></label>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-12 m-t-5">
                                      <div class="form-group fg-float">
                                          <div class="fg-line">
                                              <input type="email" name="email" id="emaill" class="input-sm form-control fg-input" value="<?php if(isset($form_cache)){ echo $form_cache['email'];} ?>">
                                              <label class="fg-label">Email</label>
                                          </div>
                                          <small id="capemaill" style="color:red; display: none;"><?php echo $this->lang->line('invalidemail');?></small>
                                      </div>
                                  </div>
                                  <div class="col-sm-12 m-t-5">
                                      <div class="form-group fg-float">
                                          <div class="fg-line">
                                              <input type="password" name="kata_sandi" id="kata_sandi" required class="input-sm form-control fg-input">
                                              <label class="fg-label"><?php echo $this->lang->line('password'); ?></label>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-12 m-t-5">
                                      <div class="form-group fg-float">
                                          <div class="fg-line">
                                              <input type="password" name="konf_kata_sandi" id="konf_kata_sandi" required class="input-sm form-control fg-input">
                                              <label class="fg-label"><?php echo $this->lang->line('confirm_pass'); ?></label>
                                          </div>
                                          <small id="cek_sandi_umum" style="color: red;"></small>
                                      </div>
                                  </div>
                                  <div class="col-md-12">
                                  <label><?php echo $this->lang->line('birthday'); ?> :</label>
                                  </div>
                                  <div class="col-xs-3 col-sm-3">
                                      <select placeh required name="tanggal_lahir" id="tanggal_lahir_thirdparty" class="select form-control" data-live-search="true">
                                          <option disabled selected  value=''><?php echo $this->lang->line('datebirth'); ?></option>
                                          <?php 
                                              for ($date=01; $date <= 31; $date++) {
                                              ?>                                                  
                                                  <option value="<?php echo $date ?>" <?php if(isset($form_cache) && $form_cache['tanggal_lahir'] == $date){ echo 'selected="true"';} ?>><?php echo $date ?></option>                                                  
                                              <?php 
                                              }
                                          ?>
                                      </select>
                                  </div>
                                  <div class="col-xs-5 col-md-5">
                                      <select required name="bulan_lahir" class="select form-control" data-live-search="true">
                                          <option disabled selected value=''><?php echo $this->lang->line('monthbirth'); ?></option>
                                            <option value="01" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "01"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('jan'); ?></option>
                                            <option value="02" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "02"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('feb'); ?></option>
                                            <option value="03" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "03"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('mar'); ?></option>
                                            <option value="04" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "04"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('apr'); ?></option>
                                            <option value="05" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "05"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('mei'); ?></option>
                                            <option value="06" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "06"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('jun'); ?></option>
                                            <option value="07" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "07"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('jul'); ?></option>
                                            <option value="08" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "08"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('ags'); ?></option>
                                            <option value="09" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "09"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('sep'); ?></option>
                                            <option value="10" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "10"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('okt'); ?></option>
                                            <option value="11" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "11"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('nov'); ?></option>
                                            <option value="12" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "12"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('des'); ?></option>
                                      </select>
                                  </div>
                                  <div class="col-xs-4 col-md-4">
                                      <select required name="tahun_lahir" id="tahun_lahir_thirdparty" class="select form-control" data-live-search="true">
                                          <option disabled selected value=''><?php echo $this->lang->line('yearbirth'); ?></option>
                                              <?php 
                                              for ($i=1960; $i <= 2016; $i++) {
                                              ?>                                                 
                                                  <option value="<?php echo $i; ?>" <?php if(isset($form_cache) && $form_cache['tahun_lahir'] == $i){ echo 'selected="true"';} ?>><?php echo $i; ?></option>
                                              <?php 
                                              } 
                                          ?>
                                      </select>
                                  </div>
                                  <div class="col-md-12 gender" >
                                      <div class="fg-line">
                                          <label class="radio radio-inline ">
                                              <input type="radio" name="jenis_kelamin" value="Male" <?php if(isset($form_cache) && $form_cache['jenis_kelamin'] == "Male"){ echo 'selected="true"';} ?>>
                                              <i class="input-helper"></i><?php echo $this->lang->line('male'); ?>  
                                          </label>

                                          <label class="radio radio-inline " >
                                              <input type="radio" name="jenis_kelamin" value="Female" <?php if(isset($form_cache) && $form_cache['jenis_kelamin'] == "Female"){ echo 'selected="true"';} ?>>
                                              <i class="input-helper"></i><?php echo $this->lang->line('women'); ?>  
                                          </label>
                                      </div>
                                  </div>
                                  <div class="col-md-12 m-t-20">
                                      <label><?php echo $this->lang->line('mobile_phone'); ?> :</label>
                                  </div>
                                  <div class="col-xs-6 col-md-5">
                                      <select required name="kode_area" class="selectpicker form-control" data-live-search="true">
                                          <?php                       
                                              $nama = ip_info("Visitor", "Country");
                                              $db = $this->db->query("SELECT * FROM `master_country` WHERE nicename='$nama'")->row_array();
                                              $phone = $db['phonecode'];
                                              $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                                              foreach ($allsub as $row => $d) {
                                                  if($phone == $d['phonecode']){
                                                      echo "<option value='".$d['phonecode']."' selected='true'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                  }
                                                  if ($phone != $d['phonecode']) {
                                                      echo "<option value='".$d['phonecode']."'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                  }                                                            
                                              }
                                          ?>
                                      </select>
                                  </div>
                                  <div class="col-xs-6 col-md-7">
                                      <div class="fg-line">
                                          <input type="text" id="no_hape_thirdparty" minlength="9" maxlength="13" name="no_hape" onkeyup="validPhone(this)"  onkeyup="validAngka(this)" onkeypress="return hanyaAngka(event)"  required class="form-control " placeholder="87XXXXXX" value="<?php if(isset($form_cache)){ echo $form_cache['no_hape'];} ?>">
                                      </div>
                                  </div> 
                                  <center>
                                      <div class="col-xs-12 captcha" ><br><br>
                                          <div class="fg-line" id="RecaptchaField4">
                                          </div>
                                      </div>
                                  </center>
                                  <div class="col-md-12 m-t-5">
                                      <label>
                                          <input type="checkbox" value="oke" id="term_agreementt">
                                          <?php echo $this->lang->line('syarat');?> <a target="_blank" class="c-blue" href="<?php echo base_url(); ?>syarat-ketentuan"><?php echo $this->lang->line('syarat1'); ?></a> <?php echo $this->lang->line('syarat2'); ?>
                                      </label>
                                  </div>
                                  <div class="col-sm-12 m-t-20">
                                      <button class="btn bgm-teal btn-block" type="submit" id="submit_signup_thirdparty"><?php echo $this->lang->line('btnsignup'); ?></button><br><br><br>
                                  </div>
                            </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>

      <div hidden="true" class="card-body" style="margin-left:10%; margin-right:10%; ">
          <div id="tabber" class="form-wizard-basic fw-container" style="margin-right: -15px; margin-left: -15px;" >
              <ul class="tab-nav tn-justified f-16">
                  <li id="li_tab1">
                      <a href="#tab1" data-toggle="tab"><?php echo $this->lang->line('student'); ?><br><p id="level_siswa" style=""><?php echo $this->lang->line('levelsiswa'); ?></p>
                      </a>
                  </li>
                  <li id="li_tab2">
                      <a href="#tab2" data-toggle="tab"><?php echo $this->lang->line('nonstudent'); ?><br><p id="level_umum" style=""><?php echo $this->lang->line('levelumum'); ?></p>
                      </a>
                  </li>
              </ul>
              <div  class="tab-content" >               
                  <form hidden method="POST" id='google_form' action='<?php echo base_url(); ?>master/google_loginn'>                
                      <textarea name="google_data" id="google_data"></textarea>
                      <input type="text" name="usertype_id" value="student">
                  </form>
                  <form hidden method="POST" action='<?php echo base_url(); ?>master/facebook_login' id='facebook_form'>                  
                      <textarea name="fb_data" id="fb_data"></textarea>
                      <input type="text" name="usertype_id" value="student">
                  </form>
              </div>
          </div>
      </div>
    </div>


   
</div>


<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ REGISTER MOBILE ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<script type="text/javascript">

$(document).ready(function() {
      $(document).ready(function(){

       

            $("#nav_login").click(function(){
                $.ajax({
                url: '<?php echo base_url(); ?>Rest/createsession',
                type: 'POST',
                success: function(response)
                { 
                    console.warn(response);
                    console.warn("<?php echo $this->session->userdata('code');?>");
                    window.location.replace('/');
                }
            });
            });
            $("#nav_loginn").click(function(){
                $.ajax({
                url: '<?php echo base_url(); ?>Rest/createsession',
                type: 'POST',
                success: function(response)
                { 
                    console.warn(response);
                    console.warn("<?php echo $this->session->userdata('code');?>");
                    window.location.replace('/');
                }
            });
            });
            $("#btn_sukses_register").click(function(){
                $("#modal_loading").show('modal');
                $.ajax({
                url: '<?php echo base_url(); ?>Rest/createsession',
                type: 'POST',
                success: function(response)
                { 
                    console.warn(response);
                    console.warn("<?php echo $this->session->userdata('code');?>");
                    window.location.replace('/');
                }
            });
            });
          // Untuk Mobile
            $("#nav_m_login").click(function(){
                $.ajax({
                    url: '<?php echo base_url(); ?>Rest/createsession',
                    type: 'POST',
                    success: function(response)
                    { 
                        console.warn(response);
                        console.warn("<?php echo $this->session->userdata('code');?>");
                        window.location.replace('/');
                    }
                });
            });
            $("#nav_m_loginn").click(function(){
                $.ajax({
                url: '<?php echo base_url(); ?>Rest/createsession',
                type: 'POST',
                success: function(response)
                { 
                    console.warn(response);
                    console.warn("<?php echo $this->session->userdata('code');?>");
                    window.location.replace('/');
                }
            });
            });
    });
    $('input[type=radio][name=jenis_signup]').change(function() {
        if (this.value == 'ortu') {
            $('#btn_signup_tutor').css('display','none');
            $('#btn_signup').css('display','inline');
            $("#combojenjang").css('display', 'none');
            $('#jenjang_id').val(15);
            $('#judul_pendaftaran').html('<?php echo $this->lang->line('sebagai'); ?> <?php echo $this->lang->line('ortu'); ?>');

        }else if (this.value == 'siswa') {
            $('#btn_signup_tutor').css('display','none');
            $('#btn_signup').css('display','inline');
            $("#combojenjang").css('display', 'inline');
            $('#judul_pendaftaran').html('<?php echo $this->lang->line('sebagai'); ?> <?php echo $this->lang->line('smp-sma'); ?>');
            
        }else if (this.value == 'umum') {
            $('#btn_signup_tutor').css('display','none');
            $('#btn_signup').css('display','inline');
            $("#combojenjang").css('display', 'none');
            $('#jenjang_id').val(15);
            $('#judul_pendaftaran').html('<?php echo $this->lang->line('sebagai'); ?> <?php echo $this->lang->line('daftar_umum'); ?>');
        }
        else if (this.value == 'tutor') {
            $('#btn_signup').css('display','none');
            $('#btn_signup_tutor').css('display','inline');    
        }
    });
});
$('#btn_signup_tutor').click (function(){
 $("#modal_loading").modal('show');
});
$('#jenjang_level').on('change', function(e){
        $('#jenjang_id').val($('#jenjang_level').val());
    });

</script>
<script>
    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }
    function validAngka(a)
    {
        if(!/^[0-9.]+$/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
      function validPhone(a)
    {
        if(!/(^0$)|(^[1-9]\d{0,12}$)/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#submit_signup_student').click(function(e){
            var sEmail = $('#email').val();
            if (validateEmail(sEmail)) {
                $("#capemail").css('display','none');
            }
            else {
                $("#capemail").css('display','block');
                e.preventDefault();
            }
        });

        $('#submit_signup').click(function(e){
            var sEmail = $('#emaill').val();
            if (validateEmail(sEmail)) {
                $("#capemaill").css('display','none');
            }
            else {
                $("#capemaill").css('display','block');
                e.preventDefault();
            }
        });

        function validateEmail(sEmail) {
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if (filter.test(sEmail)) {
                return true;
            }
            else {
                return false;
            }
        }
        $('#f_signInbtnn').click(function(){
            FB.login(function(response){                
                  if(response.status == 'connected'){
                      var new_data = {};
                      FB.api('/me', { locale: 'en_US', fields: 'name, email' }, function(response) {
                          console.log(JSON.stringify(response));
                          new_data['name'] = response.name;
                          new_data['email'] = response.email;
                          var id = response.id;
                          FB.api('/me/picture',{type: 'large'} ,function(response) {
                              // console.log(JSON.stringify(response));
                              new_data['image'] = "https://graph.facebook.com/"+id+"/picture";
                              new_data = JSON.stringify(new_data);
                              $('#fb_data').html(new_data);
                              $('#facebook_form').submit();
                          });
                      });
                  }
              }, {scope: 'public_profile,email'});
        });

        $('#f_signInbtn').click(function(){             
              FB.login(function(response){                
                  if(response.status == 'connected'){
                      var new_data = {};
                      FB.api('/me', { locale: 'en_US', fields: 'name, email' }, function(response) {
                          console.log(JSON.stringify(response));
                          new_data['name'] = response.name;
                          new_data['email'] = response.email;
                          var id = response.id;
                          FB.api('/me/picture',{type: 'large'} ,function(response) {
                              // console.log(JSON.stringify(response));
                              new_data['image'] = "https://graph.facebook.com/"+id+"/picture";
                              new_data = JSON.stringify(new_data);
                              $('#fb_data').html(new_data);
                              $('#facebook_form').submit();
                          });
                      });
                  }
              }, {scope: 'public_profile,email'});
          });
        $('#f_signInbtn2').click(function(){
            FB.login(function(response){                
                  if(response.status == 'connected'){
                      var new_data = {};
                      FB.api('/me', { locale: 'en_US', fields: 'name, email' }, function(response) {
                          console.log(JSON.stringify(response));
                          new_data['name'] = response.name;
                          new_data['email'] = response.email;
                          var id = response.id;
                          FB.api('/me/picture',{type: 'large'} ,function(response) {
                              // console.log(JSON.stringify(response));
                              new_data['image'] = "https://graph.facebook.com/"+id+"/picture";
                              new_data = JSON.stringify(new_data);
                              $('#fb_data').html(new_data);
                              $('#facebook_form').submit();
                          });
                      });
                  }
              }, {scope: 'public_profile,email'});
        });
        $('#kata_sandi').on('keyup',function(){
            if($('#konf_kata_sandi').val() != $(this).val()){
                $('#submit_signup').attr('disabled','');
        $('#term_agreementt').removeAttr('checked');
        
        $('#cek_sandi_umum').html('<?php echo $this->lang->line('passwordnotmatch'); ?>').css('color', 'red');
            }else{
                // $('#submit_signup').removeAttr('disabled');
        $('#cek_sandi_umum').html('').css('color', 'red');
            }
        });
        $('#konf_kata_sandi').on('keyup',function(){
            if($('#kata_sandi').val() != $(this).val() ){
                $('#submit_signup').attr('disabled','');
        $('#term_agreementt').removeAttr('checked');
        $('#cek_sandi_umum').html('<?php echo $this->lang->line('passwordnotmatch'); ?>').css('color', 'red');
            }else{
                // $('#submit_signup').removeAttr('disabled');
        
        $('#cek_sandi_umum').html('').css('color', 'red');
            }
        });
        $('#kata_sandi_student').on('keyup',function(){
      if($('#konf_kata_sandi_student').val() != $(this).val()){
        $('#submit_signup_student').attr('disabled','');
        $('#term_agreement').removeAttr('checked');
        $('#cek_sandi').html('<?php echo $this->lang->line('passwordnotmatch'); ?>').css('color', 'red');
      }
            else if($('#konf_kata_sandi_student').val() != $(this).val()){
                $('#submit_signup_student').attr('disabled','');
        $('#term_agreement').removeAttr('checked');
        $('#cek_sandi').html('<?php echo $this->lang->line('passwordnotmatch'); ?>').css('color', 'red');
            }else{
        $('#cek_sandi').html('').css('color', 'red');
            }
        });
        $('#konf_kata_sandi_student').on('keyup',function(){
            if($('#kata_sandi_student').val() != $(this).val() ){
                $('#submit_signup_student').attr('disabled','');
        // $('#cek_syarat').attr('hidden','');
        $('#term_agreement').removeAttr('checked');
        $('#cek_sandi').html('<?php echo $this->lang->line('passwordnotmatch'); ?>').css('color', 'red');
        
            }else{
                // $('#submit_signup_student').removeAttr('disabled');
        $('#cek_sandi').html('').css('color', 'red');
        $('#cek_syarat').removeAttr('hidden');
        
            }
        });

        $('#term_agreementt').change(function(){
            if($('#kata_sandi_student').val() == $('#konf_kata_sandi_student').val() && $('#term_agreementt').is(':checked') == false){
                $('#submit_signup').attr('disabled','');
            }else{
                $('#submit_signup').removeAttr('disabled');
            }
        });

        $('#term_agreement').change(function(){
            if($('#kata_sandi_student').val() == $('#konf_kata_sandi_student').val() &&  $('#term_agreement').is(':checked') == false){
                $('#submit_signup_student').attr('disabled','');
            }else{
                $('#submit_signup_student').removeAttr('disabled');
            }
        });
        var g_startup = 0;
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
              var target = $(e.target).attr("href"); // activated tab
              if(target == "#tab1"){
                // alert(g_startup);
                if(g_startup == 2){
                    g_startup = 1;
                  // alert($('#recap_2').html());
                  $('#renderer').appendTo('#recap_1');  
              }

        }
        else if(target == "#tab2")
        {
            // alert(g_startup);
            if(g_startup == 1 || g_startup == 0){
                g_startup = 2;
                $('#renderer').appendTo('#recap_2');  
            }
        }
        var g_startup = 0;
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
              var target = $(e.target).attr("href"); // activated tab
              if(target == "#tabm1"){
                // alert(g_startup);
                if(g_startup == 2){
                    g_startup = 1;
                  // alert($('#recap_2').html());
                  $('#renderer').appendTo('#recap_m1');  
              }

        }
        else if(target == "#tabm2")
        {
            // alert(g_startup);
            if(g_startup == 1 || g_startup == 0){
                g_startup = 2;
                $('#renderer').appendTo('#recap_m2');  
            }
        }
        });
        $('#jenjang_level').change(function(e){
            $('#tulisan_jenjang_id').val($(this).find("option:selected").text());
        });
        var vuvu = $('#tabber').tabs();

        <?php 
        if(isset($form_cache) && $form_cache['tab'] == "2"){
            echo "
            $( '#tabber').tabs({ active: 1 });
            if(g_startup == 1 || g_startup == 0){
                g_startup = 2;
                $('#renderer').appendTo('#recap_2');  
            }
            $('#li_tab2').attr('class','ui-state-default ui-corner-top ui-tabs-active ui-state-active active');
            $('#li_tab1').attr('class','ui-state-default ui-corner-top');
            ";
        }
        ?>
    });

    });
</script>
<script type="text/javascript">
    $('.btn_setindonesialp').click(function(e){
      e.preventDefault();    
      var lang  = "<?php echo $this->session->userdata('lang'); ?>";
      if (lang == "indonesia") {         
          $.get('<?php echo base_url('set_lang/english'); ?>',function(hasil){  location.reload(); });
      } 
      else 
      {
          $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
      }              
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn_setindonesia').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
        });
        $('#btn_setenglish').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/english'); ?>',function(){ location.reload(); });
        });

        $('#lightblue').click(function(e){
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','lightblue'); ?>',function(){  });
        });
        $('#bluegray').click(function(e){
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','bluegray'); ?>',function(){ location.reload(); });                       
        });
        $('#cyan').click(function(e){   
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','cyan'); ?>',function(){ location.reload(); });           
        });
        $('#teal').click(function(e){
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','teal'); ?>',function(){ location.reload(); });           
        });
        $('#orange').click(function(e){
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','orange'); ?>',function(){ location.reload(); });         
        });
        $('#blue').click(function(e){
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','blue'); ?>',function(){ location.reload(); });           
        });
        $('#btn_callsupport').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url(); ?>process/callsupport',function(ret){
                ret = JSON.parse(ret);
                if(ret['status'] == true){
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',ret['message']);
                }else{
                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',ret['message']);
                }
                

            });
        });
    });
</script>


