<div class="modal fade" data-modal-color="teal" id="modal_cekumur" tabindex="-1" data-backdrop="static" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <center>
                    <div class="modal-body" style="margin-top: 15%;" ><br>
                        
                       <div class="alert " style="display: block; ?>" id="alertumur">
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                    </div>
                </center>
        </div>
    </div>
</div>
<div id="Nav_registrasi" class="pull-right hidden-xs header-inner" >
    <nav class=" navbar navbar-default navbar-fixed-top" style="; background:rgba(0,0,0,0.1);">
        <div class="col-sm-1">
        </div>
        <a class="navbar-brand" data-scroll href="https://classmiles.com">          
            <img style="margin-top: -9px;margin-left: -17px;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>aset/img/logo/LogoIjo.png" alt="">
        </a>
        
        <div class=" nav navbar-nav navbar-right" style="margin-top: 16px; margin-right: 180px;">
            <img class="btn_setindonesialp animated zoomInDown animation-delay-10"  height="20px" style="cursor: pointer; margin-right: 60px; " src="<?php if ($this->session->userdata('lang') == "indonesia") { echo CDN_URL.STATIC_IMAGE_CDN_URL.'language/flaginggris.png'; } else { echo CDN_URL.STATIC_IMAGE_CDN_URL.'language/flagindo.png'; } ?>" />
            <a href="<?php echo base_url(); ?>" class="" style=" font-size: 14px; color: #ffffff;">
                <?php echo $this->lang->line('kembali')?>
            </a>

        <div class="col-sm-1"></div>    
        </div>
    </nav>
</div>
<script type="text/javascript">
    $('.btn_setindonesialp').click(function(e){
        e.preventDefault();    
        var lang  = "<?php echo $this->session->userdata('lang'); ?>";
        if (lang == "indonesia") {         
            $.get('<?php echo base_url('set_lang/english'); ?>',function(hasil){  location.reload(); });
        } 
        else 
        {
            $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
        }              
    });
</script>
<div class="container" style="margin-top: 5%;"> 
    <div class="col-sm-2"></div>
    <div id="student">
        <div class="col-sm-8">
            <div class="card bgm-white z-depth-2">
                <center>
                    <div class="card-header ">                                
                        <div class="blockquotee m-b-25">
                            <p><?php echo $this->lang->line('createpassword');?></p>
                        </div>
                        <label style="font-size:12px;"><?php echo $this->lang->line('laststep');?></label>
                        <hr>
                    </div>
                </center>
                <form class="form-horizontal" role="form" action="<?php echo base_url(); ?>master/reg_email" method="POST">
                    <input type="text" style="display: none;" name="user_name" value="<?php 
                    if(isset($datas)){ 
                        echo $datas['user_name'];
                    }else if(isset($google_data)){
                        echo $google_data['user_name'];
                    } ?>">
                    <input type="text" style="display: none;" id="type_regis" name="type_regis" value="<?php 
                    if(isset($datas)){ 
                        echo $datas['type_regis'];
                    }else if(isset($google_data)){
                        echo $google_data['type_regis'];
                    } ?>">
                    <input type="text" style="display: none;" name="first_name" value="<?php 
                    if(isset($datas)){ 
                        echo $datas['first_name'];
                    }else if(isset($google_data)){
                        echo $google_data['first_name'];
                    } ?>">
                    <input type="text" style="display: none;" name="last_name" value="<?php 
                    if(isset($datas)){ 
                        echo $datas['last_name'];
                    }else if(isset($google_data)){
                        echo $google_data['last_name'];
                    } ?>">

                    <input type="text" style="display: none;" name="user_image" value="<?php 
                    if(isset($datas)){ 
                        echo $datas['user_image'];
                    }else if(isset($google_data)){
                        echo $google_data['user_image'];
                    } ?>">
                    <input type="text" style="display: none;" name="usertype_id" value="<?php
                    if(isset($datas)){
                        echo $datas['usertype_id'];
                    }else if(isset($google_data)){
                        echo $google_data['usertype_id'];
                    } ?>">
                    <div class="card-padding card-body">
                        <div class="form-group row-fluid">
                            <!-- <label><?php echo $datas['user_name']; ?></label> -->
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <div class="fg-line">
                                    <input type="email" readonly required class="form-control input-lg" style="background-color:#F5F5F5;" name="email" id="emailll" value="<?php 
                                    if(isset($datas)){ 
                                        echo $datas['email'];
                                    }else if(isset($google_data)){
                                        echo $google_data['email'];
                                    } ?>">
                                </div>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('password');?></label>
                            <div id="kotakpassword"  class="col-sm-9">
                                <div class="fg-line">
                                    <input type="password" required class="form-control input-sm " name="password" id="kata_sandi">
                                </div>
                                <small id="txterrorpassword" class="help-block" style="display: none;"><?php echo $this->lang->line('passwordnotmatch');?></small>
                            </div>
                        </div>
                        <div  class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('confirm_pass');?></label>
                            <div id="kotakkonfpassword" class="col-sm-9">
                                <div class="fg-line">
                                    <input type="password" required class="form-control input-sm" name="confirm_password" id="konf_kata_sandi">
                                </div>
                            </div>
                        </div>
                         <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('gender');?></label>
                                <div class="col-sm-9">
                                    <div class="fg-line">
                                        <select required name="user_gender" class="select2 form-control">
                                         <option disabled selected value=''><?php echo $this->lang->line('gender');?></option>
                                         <option value="Male"><?php echo $this->lang->line('male');?></option>
                                         <option value="Female"><?php echo $this->lang->line('women');?></option>
                                     </select>
                                 </div>
                             </div>
                         </div>
                         <div class="form-group">
                                <label class="col-sm-3 control-label">Daftar Sebagai :</label>
                                <div class="col-sm-9">
                                    <div class="fg-line">
                                        <select id="tipe_register" required name="tipe_register" class="select2 form-control">
                                         <option disabled selected value=''>Pilih salah satu</option>
                                         <option value="ortu"><?php echo $this->lang->line('ortu'); ?></option>
                                         <option value="siswa"><?php echo $this->lang->line('smp-sma'); ?></option>
                                         <option value="umum"><?php echo $this->lang->line('daftar_umum'); ?></option>
                                     </select>
                                 </div>
                             </div>
                         </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('datebirth');?></label>
                            <div class="col-sm-3">
                                <div class="fg-line">
                                    <select required name="tanggal_lahir" id="tanggal_lahir" class="select2 form-control">
                                        <option disabled selected  value=''><?php echo $this->lang->line('datebirth');?></option>
                                        <?php 
                                        for ($date=01; $date <= 31; $date++) {
                                            $date = sprintf('%02d', $date);
                                            ?>                                                  
                                            <option value="<?php echo $date ?>" <?php if(isset($datas) && substr($datas['user_birthdate'],8,2) == $date){ echo "selected='true'";} ?>><?php echo $date ?></option>                                                  
                                            <?php 
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                               <div class="fg-line">
                                    <select required name="bulan_lahir" id="bulan_lahir" class="select2 form-control">
                                        <option value="01" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "01"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('jan'); ?></option>
                                        <option value="02" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "02"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('feb'); ?></option>
                                        <option value="03" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "03"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('mar'); ?></option>
                                        <option value="04" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "04"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('apr'); ?></option>
                                        <option value="05" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "05"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('mei'); ?></option>
                                        <option value="06" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "06"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('jun'); ?></option>
                                        <option value="07" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "07"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('jul'); ?></option>
                                        <option value="08" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "08"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('ags'); ?></option>
                                        <option value="09" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "09"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('sep'); ?></option>
                                        <option value="10" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "10"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('okt'); ?></option>
                                        <option value="11" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "11"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('nov'); ?></option>
                                        <option value="12" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "12"){ echo 'selected="true"';} ?>><?php echo $this->lang->line('des'); ?></option>                                                            
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="fg-line">
                                    <select required name="tahun_lahir" id="tahun_lahir" class="select2 form-control">
                                        <option disabled selected value=''><?php echo $this->lang->line('yearbirth');?></option>
                                        <?php 
                                        for ($i=1960; $i <= 2016; $i++) {
                                            ?>                                                 
                                            <option value="<?php echo $i; ?>" <?php if(isset($datas) && substr($datas['user_birthdate'],0,4) == $i){ echo "selected='true'";} ?>><?php echo $i; ?></option>
                                            <?php 
                                        } 
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('mobile_phone');?></label>
                            <div class="col-sm-4">
                                    <div class="fg-line">
                                       <select required name="kode_area" class="select2 form-control">
                                            <?php                       
                                                // $nama = ip_info("Visitor", "Country Code");
                                                $namanegara = $this->session->userdata('get_location');
                                                $db = $this->db->query("SELECT * FROM `master_country` WHERE iso='$namanegara'")->row_array();
                                                $phone = $db['phonecode'];
                                                $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                                                foreach ($allsub as $row => $d) {
                                                    if($phone == $d['phonecode']){
                                                        echo "<option value='".$d['phonecode']."' selected='true'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                    }
                                                    if ($phone != $d['phonecode']) {
                                                        echo "<option value='".$d['phonecode']."'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                    }                                                            
                                                }
                                                ?>
                                        </select>
                                   </div>
                                </div>
                                <div id="kotakphone" class="col-sm-8">
                                    <div class="fg-line">
                                        <input type="text" class="form-control input-sm nohp" id="no_hape" minlength="9" maxlength="13" name="no_hape" onmousemove="validPhone(this)" onkeyup="validPhone(this)"  onkeyup="validAngka(this)" onkeypress="return hanyaAngka(event)" required class="form-control " placeholder="87XXXXXX" value="<?php if(isset($form_cache)){ echo $form_cache['no_hape'];} ?>">
                                    </div>
                                    <small id="txterrorphone" class="help-block" style="display: none;"><?php echo $this->lang->line('noempty');?></small>
                                </div>
                        </div>
                        <div class="form-group" style="display: none;">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-sm-3">
                                <div class="radio m-b-15">
                                    <label>
                                        <input type="radio" name="sample" value="0" checked id="showkotakstudent">
                                        <i class="input-helper"></i>
                                        <?php echo $this->lang->line('student');?>
                                    </label>
                                </div>
                            </div>                            
                            <div class="col-sm-3">
                                <div class="radio m-b-15">
                                    <label>
                                        <input type="radio" name="sample" value="1" id="hidekotakstudent">
                                        <i class="input-helper"></i>
                                        <?php echo $this->lang->line('nonstudent');?>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div id="kotakstudent" style="display: none;">
                            <!-- <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('select_level');?></label>
                                <div class="col-sm-9">
                                    <div class="fg-line">
                                        <select required id="jenjang_name"  class="select2 form-control" data-placeholder="<?php echo $this->lang->line('select_level');?>">
                                           <option value=""></option>
                                        </select>
                                   </div>
                               </div>
                            </div> -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $this->lang->line('select_class');?></label>
                                <div id="kotakjenjang" class="col-md-9">
                                   <div class="fg-line">
                                        <input type="text" name="id_jenjang" id="id_jenjang" style="display: none;" value="">
                                        <select required class="select2 form-control" name="jenjang_name" id='jenjang_level' data-placeholder="<?php echo $this->lang->line('select_class'); ?>" style='width: 100%;'>
                                            <option disabled selected value=''><?php echo $this->lang->line('select_class'); ?></option>
                                            <?php 
                                            if(isset($form_cache)){
                                                echo '<option value="'.$form_cache['jenjang_id'].'" >'.$form_cache['tulisan_jenjang_id'].'</option>';
                                            } 
                                            ?>
                                        </select>    
                                    </div>
                                    <small id="txterrorjenjang" class="help-block" style="display: none;"><?php echo $this->lang->line('noempty');?></small>
                                </div>
                            </div>
                        </div>
                        <br>
                        <hr>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>
                                    <input type="checkbox" value="oke" id="term_agreement">
                                    <?php echo $this->lang->line('syarat');?> <a target="_blank" class="c-blue" href="<?php echo base_url(); ?>syarat-ketentuan"><?php echo $this->lang->line('syarat1'); ?></a> <?php echo $this->lang->line('syarat2'); ?>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <center>
                                <div class="col-sm-3">
                                </div>

                                <div class="col-sm-6">
                                    <div class="fg-line">
                                        <button type="submit" style="height:40px;" disabled class="btn btn-primary btn-block" id="submit_signup"><?php echo $this->lang->line('btnsignup');?></button>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                </div>
                            </center>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    <!-- AKHIR STUDENT -->
    </div>    
</div>
<!-- <div class="col-lg-4" style="margin-top:3%; width:20%;">
    <button disabled class="btn btn-primary" id="indonesia">Indonesia</button>
    <button class="btn btn-warning" id="inggris">Inggris</button>
</div> -->
<!-- </section>
</section>
-->


<script type="text/javascript">
    var type_regis = null;
    var lahirtgl = null;
    var bulantgl = null;
    var tahuntgl = null;
    var tipe = null;
    $("#submit_signup").click(function(){
        
    });
    $(document).ready(function(){        
        $('#tipe_register').on('change', function() {
            tipe = $("#tipe_register").val();
            // alert(tipe);
            if (tipe == "ortu" || tipe == "umum") {
                $("#kotakstudent").css('display','none');
                $("#jenjang_level").val(15);
                $("#id_jenjang").val(15);
                $('#jenjang_level').removeAttr('required');
                $('#jenjang_name').removeAttr('required');
            }
            else if (tipe == "siswa") {
                $("#kotakstudent").css('display','block');
                $('#jenjang_level').attr('required','');
                $('#jenjang_name').attr('required','');
            }
            type_regis = $("#tipe_register").val();
            lahirtgl = $("#tanggal_lahir").val();
            bulantgl = $("#bulan_lahir").val();
            tahuntgl = $("#tahun_lahir").val();
            // console.log(type_regis);
            // console.log(lahirtgl);
            // console.log(bulantgl);
            // console.log(tahuntgl);
            if (lahirtgl!=null&&bulantgl!=null&&tahuntgl!=null) {
                var age = checkBOD();            
                var choose = chooseOccupation(age);
                // console.log(choose);
                if (choose == -2) {
                    $("#modal_cekumur").modal('show');
                    $("#alertumur").html('<?php echo $this->lang->line('pesanumursiswa'); ?>');
                    $('#submit_signup').attr('disabled','true');
                }
                else if (choose == -1) {
                    $("#modal_cekumur").modal('show');
                    $("#alertumur").html('<?php echo $this->lang->line('pesanumurumum'); ?>');
                    $('#submit_signup').attr('disabled','true');
                }
                else
                {
                    $('#submit_signup').removeAttr('disabled');
                }
            }
        });
        $('#jenjang_level').on('change', function() {
            var a = $(this).val();
            $("#id_jenjang").val(a);
        });
        var type_regis = $("#type_regis").val();
        if (type_regis == 'siswa') {
            $("#kotakstudent").css('display','block');
        }else{
            $("#kotakstudent").css('display','none');
            $("#jenjang_id").val(15);
            $("#jenjang_level").val(15);
        }


        /*if($('#hidekotakstudent').is(':checked')){
            $("#kotakstudent").css('display','none');
        }
        $("#showkotakstudent").click(function(){
            $("#kotakstudent").css('display','block');
            $('#jenjang_level').attr('required','');
            $('#jenjang_name').attr('required','');
        });
        $("#hidekotakstudent").click(function(){
            $("#kotakstudent").css('display','none');
            $('#jenjang_level').removeAttr('required');
            $('#jenjang_name').removeAttr('required');
        });*/

        $('#kata_sandi').on('keyup',function(){
            if($('#konf_kata_sandi').val() != $(this).val() ){
                $('#submit_signup').attr('disabled','');
                $('#kotakpassword').addClass('has-error');
                $('#kotakkonfpassword').addClass('has-error');
                $("#txterrorpassword").css('display','block');
            }else if($('#term_agreement').is(':checked') == false){
                $('#submit_signup').attr('disabled','');
                $('#kotakpassword').removeClass('has-error');
                $('#kotakkonfpassword').removeClass('has-error');
                $("#txterrorpassword").css('display','none');
            }else{
                $('#submit_signup').removeAttr('disabled');
                $('#kotakpassword').removeClass('has-error');
                $('#kotakkonfpassword').removeClass('has-error');
                $("#txterrorpassword").css('display','none');
            }
        });
        $('#konf_kata_sandi').on('keyup',function(){
            if($('#kata_sandi').val() != $(this).val() ){
                $('#submit_signup').attr('disabled','');
                $('#kotakpassword').addClass('has-error');
                $('#kotakkonfpassword').addClass('has-error');
                $("#txterrorpassword").css('display','block');
            }else if($('#term_agreement').is(':checked') == false){
                $('#submit_signup').attr('disabled','');
                $('#kotakpassword').removeClass('has-error');
                $('#kotakkonfpassword').removeClass('has-error');
                $("#txterrorpassword").css('display','none');
            }else{
                $('#submit_signup').removeAttr('disabled');
                $('#kotakpassword').removeClass('has-error');
                $('#kotakkonfpassword').removeClass('has-error');
                $("#txterrorpassword").css('display','none');
            }
        });

        $('#term_agreement').change(function(){
            if($('#term_agreement').is(':checked') == true){
                // console.log('D');
                $('#submit_signup').removeAttr('disabled');
                if ($('#kata_sandi').val()!="" && $('#konf_kata_sandi').val()!="") {
                    // console.log('A');
                    $('#submit_signup').removeAttr('disabled');
                    $('#kotakpassword').removeClass('has-error');
                    $('#kotakkonfpassword').removeClass('has-error');
                    if ($('#tipe_register').val()!=null) {
                        // console.log('B');
                        if ($("#no_hape").val()!="") {
                            // console.log('C');
                            $('#kotakphone').removeClass('has-error');
                            $("#txterrorphone").css('display','none');
                            if ($("#tanggal_lahir").val()!=null) {
                                // console.log('F');
                                $('#submit_signup').removeAttr('disabled');
                                if ($("#bulan_lahir").val()!=null) {
                                    // console.log('G');
                                    $('#submit_signup').removeAttr('disabled');
                                    if ($("#tahun_lahir").val()!=null) {
                                        // console.log('H');
                                        $('#submit_signup').removeAttr('disabled');
                                    }
                                    else{
                                        // console.log('H1');
                                        $('#submit_signup').attr('disabled','');
                                    }
                                }
                                else{
                                    // console.log('G1');
                                    $('#submit_signup').attr('disabled','');
                                }
                            }
                            else{
                                // console.log('F1');
                                $('#submit_signup').attr('disabled','');
                            }
                        }
                        else{
                            // console.log('C1');
                            $('#submit_signup').attr('disabled','');
                            $('#kotakphone').addClass('has-error');
                            $("#txterrorphone").css('display','block');
                            
                        }
                    }
                    else{
                        // console.log('B1');
                        $('#submit_signup').attr('disabled','');
                        
                    }
                }
                else{
                    // console.log('A1');
                    $('#submit_signup').attr('disabled','');
                    $('#kotakpassword').addClass('has-error');
                    $('#kotakkonfpassword').addClass('has-error');
                }
            }
            else{
                // console.log('D1');
                $('#submit_signup').attr('disabled','');
            }
            type_regis = $("#tipe_register").val();
            lahirtgl = $("#tanggal_lahir").val();
            bulantgl = $("#bulan_lahir").val();
            tahuntgl = $("#tahun_lahir").val();
                // console.log(type_regis);
                // console.log(lahirtgl);
                // console.log(bulantgl);
                // console.log(tahuntgl);
            if (lahirtgl!=null&&bulantgl!=null&&tahuntgl!=null) {
                var age = checkBOD();
                var choose = chooseOccupation(age);
                if (choose == -2) {
                    $("#modal_cekumur").modal('show');
                    $("#alertumur").html('<?php echo $this->lang->line('pesanumursiswa'); ?>');
                    $('#submit_signup').attr('disabled','true');
                }
                else if (choose == -1) {
                    $("#modal_cekumur").modal('show');
                    $("#alertumur").html('<?php echo $this->lang->line('pesanumurumum'); ?>');
                    $('#submit_signup').attr('disabled','true');
                }
                else
                {
                    $('#submit_signup').removeAttr('disabled');
                }
            }
        });
        $('#tanggal_lahir').on('change', function() {
            type_regis = $("#tipe_register").val();
            lahirtgl = $("#tanggal_lahir").val();
            bulantgl = $("#bulan_lahir").val();
            tahuntgl = $("#tahun_lahir").val();
                // console.log(type_regis);
                // console.log(lahirtgl);
                // console.log(bulantgl);
                // console.log(tahuntgl);
            if (lahirtgl!=null&&bulantgl!=null&&tahuntgl!=null) {
                var age = checkBOD();
                var choose = chooseOccupation(age);
                if (choose == -2) {
                    $("#modal_cekumur").modal('show');
                    $("#alertumur").html('<?php echo $this->lang->line('pesanumursiswa'); ?>');
                    $('#submit_signup').attr('disabled','true');
                }
                else if (choose == -1) {
                    $("#modal_cekumur").modal('show');
                    $("#alertumur").html('<?php echo $this->lang->line('pesanumurumum'); ?>');
                    $('#submit_signup').attr('disabled','true');
                }
                else
                {
                    $('#submit_signup').removeAttr('disabled');
                }
            }
        });

        $('#bulan_lahir').on('change', function() {
            type_regis = $("#tipe_register").val();
            lahirtgl = $("#tanggal_lahir").val();
            bulantgl = $("#bulan_lahir").val();
            tahuntgl = $("#tahun_lahir").val();
            // console.log(type_regis);
                // console.log(lahirtgl);
                // console.log(bulantgl);
                // console.log(tahuntgl);
            if (lahirtgl!=null&&bulantgl!=null&&tahuntgl!=null) {
                var age = checkBOD();
                var choose = chooseOccupation(age);            
                if (choose == -2) {
                    $("#modal_cekumur").modal('show');
                    $("#alertumur").html('<?php echo $this->lang->line('pesanumursiswa'); ?>');
                    $('#submit_signup').attr('disabled','true');
                }
                else if (choose == -1) {
                    $("#modal_cekumur").modal('show');
                    $("#alertumur").html('<?php echo $this->lang->line('pesanumurumum'); ?>');
                    $('#submit_signup').attr('disabled','true');
                }
                else
                {
                    $('#submit_signup').removeAttr('disabled');
                }
            }
        });

        $('#tahun_lahir').on('change', function() {
            type_regis = $("#tipe_register").val();
            lahirtgl = $("#tanggal_lahir").val();
            bulantgl = $("#bulan_lahir").val();
            tahuntgl = $("#tahun_lahir").val();
            // console.log(type_regis);
                // console.log(lahirtgl);
                // console.log(bulantgl);
                // console.log(tahuntgl);
            if (lahirtgl!=null&&bulantgl!=null&&tahuntgl!=null) {
                var age = checkBOD();            
                var choose = chooseOccupation(age);
                // console.log(choose);
                if (choose == -2) {
                    $("#modal_cekumur").modal('show');
                    $("#alertumur").html('<?php echo $this->lang->line('pesanumursiswa'); ?>');
                    $('#submit_signup').attr('disabled','true');
                }
                else if (choose == -1) {
                    $("#modal_cekumur").modal('show');
                    $("#alertumur").html('<?php echo $this->lang->line('pesanumurumum'); ?>');
                    $('#submit_signup').attr('disabled','true');
                }
                else
                {
                    $('#submit_signup').removeAttr('disabled');
                }
            }
        });
        function checkBOD() {
            var today = new Date();
            var thn_ini = today.getFullYear();
            var bln_ini = today.getMonth();
            var tgl_ini = today.getDate();

            var tgl_lahir = document.getElementById("tanggal_lahir").value;
            var bln_lahir = document.getElementById("bulan_lahir").value;
            var thn_lahir = document.getElementById("tahun_lahir").value;

            var age = thn_ini - thn_lahir;
            var ageMonth = bln_ini - bln_ini;
            var ageDay = tgl_ini - tgl_lahir;
            // console.log(age);

            if (ageMonth < 0 || (ageMonth == 0 && ageDay < 0)) {
            age = parseInt(age) - 1;
            // alert(age);
            }
            return age;
        }
        function chooseOccupation(age) {    
            var today = new Date();
            var thn_ini = today.getFullYear();
            var bln_ini = today.getMonth();
            var tgl_ini = today.getDate();

            var tgl_lahir = document.getElementById("tanggal_lahir").value;
            var bln_lahir = document.getElementById("bulan_lahir").value;
            var thn_lahir = document.getElementById("tahun_lahir").value;

            var age = thn_ini - thn_lahir;
            var ageMonth = bln_ini - bln_ini;
            var ageDay = tgl_ini - tgl_lahir;
            // alert(age);
            // var pekerjaan = document.getElementById("select_pekerjaan").value;
            var pekerjaan = $("#tipe_register").val();
            // var level_id = document.getElementById("level_pekerjaan").value;        
            if (pekerjaan == "ortu" || pekerjaan == "umum") {
                if (age <= 18) {                                                    
                    return -1;
                }
                else
                {   
                    return 1;
                }
            }
            else
            {
                if (age > 20) {
                    return -2
                }
                else
                {
                    return 1;
                }
            }


            // if(pekerjaan == "Student"){
            //     // $("#level_pekerjaan").value(0);
            //     // alert(level_id);
            //   if(age > 20){
                
            //     // alert("Umur anda tua");

            //    $("#combojenjang").css('display','inline');
            //    $("#modal_cekumur").modal('show');
            //    $("#alertumur").html('<?php echo $this->lang->line('pesanumursiswa'); ?>');
            //   }
            //   else{
            //     $("modal_cekumur").modal('hide');
            //     $("#combojenjang").css('display','inline');
            //   }
            // }
            // else if (pekerjaan == "General"){
            //     $('#jenjang_id').val(15);
            //     // $("#level_pekerjaan").val(1);
            //     // level_id ='1'
            //     // alert(level_id);
            //   if(age < 18){
            //     // alert('Umur anda muda');
            //     $("#modal_cekumur").modal('show');
            //     $("#alertumur").html('<?php echo $this->lang->line('pesanumurumum'); ?>');
            //     $("#combojenjang").css('display','none');
            //     $('#jenjang_id').val(15);
            //   }
            //   else{
            //     $("#modal_cekumur").modal('hide');
            //     $("#combojenjang").css('display','none');
            //     $('#jenjang_id').val(15);
            //   }
            // }
            // else{
            //   $("#modal_cekumur").modal('hide');
            //   $("#combojenjang").css('display','none');  
            //   $('#jenjang_id').val(15);
            // }
        }
    });
</script>
<script>
    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }
    function validAngka(a)
    {
        if(!/^[0-9.]+$/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
      function validPhone(a)
    {
        if(!/(^0$)|(^[1-9]\d{0,12}$)/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
</script>