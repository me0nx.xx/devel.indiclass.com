<div class="container">

    <div class="col-sm-12">

        <div class="col-sm-8" style="margin-top:2%;">

            <div class="left">
                <div class="col-sm-8" >
                <center>
                    <img id="logologin" src="<?php echo base_url(); ?>aset/img/blur/logo_classmiles.png" style="margin-left:-30px;" height="5%" width="120%" alt="">
                    </center>
                </div>
                <div class="col-lg-8">

                    <p class="hidden-xs" style="font-size: 26px; color: #5f9ea0">Classroom at your hands</p> 
                    <br>
                    <!-- <div class="col-sm-8"> -->
                    <div class="col-sm-5 bgm-indigo hidden-xs">
                        <a href="#"><img src="<?php echo base_url(); ?>aset/img/widgets/btn-appstore.png" style="margin-left:-15px;" width="120%" height="110%" alt="App Store"></a>
                    </div>
                    <div class="col-sm-5 bgm-orange hidden-xs" style="margin-left:10px;">
                        <a href="#"><img src="<?php echo base_url(); ?>aset/img/widgets/btn-googleplay.png" style="margin-left:-15px;" width="120%" height="110%" alt="Google Play"></a>
                    </div>
                    <!-- </div> -->
                </div>
                <div class="col-sm-12">
                    <center>
                        <p class="hidden-xs" style="color:#ffffff; font-size:10.44px; top: 5%; text-align:justify; font-family:tahoma;">
                            <?php  echo $this->lang->line('description_loginfooter'); ?>
                        </p>
                    </center>
                </div>
            </div>
        </div>    
        <div class="right">
            <div class="col-sm-4" style="margin-top:0%; ">
                <div class="card">
                    <div class="card-header">
                        <p class="hidden-xs" style="font-size: 23px; color: #009688; text-align:center;">Selamat datang di Classmiles</p>
                        <?php if($this->session->flashdata('mes_alert')){ ?>
                        <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <?php echo $this->session->flashdata('mes_message'); ?>
                      </div>
                      <?php } ?>
                      <!-- <p style="font-size: 23px; color: #009688; text-align:center;"><?php //echo $this->sessions->flashdata('msgLogout'); ?></p> -->
                      <hr class="hidden-xs">
                  </div>

                  <div class="card-body card-padding">
                    <form role="form" action="<?php echo base_url('Master/aksi_login'); ?>" method="post">
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                <input type="text" name="email" autofocus required class="input-sm form-control fg-input" style="color:#009688;">
                                <label class="fg-label" style="color:#009688;">Email</label>
                            </div>
                        </div>
                        <br>
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                <input type="password" required name="kata_sandi" class="input-sm form-control fg-input" style="color:#009688;">
                                <label class="fg-label" style="color:#009688;">Kata sandi</label>
                            </div>
                        </div>
                        <br>
                        <div class="checkbox">
                            <!-- <label style="color:#009688;">
                                <input type="checkbox" value="">
                                <i class="input-helper"></i>
                                Ingat saya
                            </label> -->
                            
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary btn-block">Masuk</button>
                        <br>
                        <br>
                        <center><label class="fg-label">Belum punya akun? Silakan daftar</label><a href="<?php echo base_url(); ?>first/Register"> disini</a></center>
                        <center><label style="color:#ff9000;"><a href="<?php echo base_url(); ?>first/forgot">Lupa password ?</a></label></center><hr>
                        <button type="button" id="f_signInbtn" class="btn bgm-facebook waves-effect btn-icon-text btn-block"><i class="zmdi zmdi-facebook"></i> Masuk dengan Facebook</button>
                        <button id="g_signInbtn" type="button" class="btn bgm-google waves-effect btn-icon-text btn-block"><i class="zmdi zmdi-google"></i> Masuk dengan Google</button>
                        <!-- <div class="g-signin2" data-onsuccess="onSignIn"></div> -->
                    </form>
                </div>                            
            </div>
            <div class="col-md-4">
                <div class="col-xs-6 col-md-12" style="margin-top:3%; width:50%; bottom:10px; ">
                    <button disabled class="btn btn-primary btn-block" id="indonesia">Indonesia</button>
                </div>
                <div class="col-xs-6 col-md-12" style="margin-top:3%; width:50%; bottom:10px; ">
                    <button class="btn btn-warning btn-block" id="inggris">Inggris</button>
                </div>
            </div>                             
        </div>
    </div>    
    
</div>

</div><!-- akhir container -->
<form hidden method="POST" action='<?php echo base_url(); ?>master/google_login' id='google_form'>
    <!-- <input type="text" name="email" id="google_email"> -->
    <textarea name="google_data" id="google_data"></textarea>
    <input type="text" name="usertype_id" value="student">
</form>
<form hidden method="POST" action='<?php echo base_url(); ?>master/facebook_login' id='facebook_form'>
    <!-- <input type="text" name="email" id="google_email"> -->
    <textarea name="fb_data" id="fb_data"></textarea>
    <input type="text" name="usertype_id" value="student">
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $('#g_signInbtn').click(function(){
            auth2.signIn().then(function() {
                var data = auth2.currentUser.get().getBasicProfile();
                // $('#google_email').val(data.getEmail());
                $('#google_data').html(JSON.stringify(data));
                $('#google_form').submit();
                console.log(auth2.currentUser.get().getId());
            });
        });
        $('#f_signInbtn').click(function(){
            FB.login(function(response){
                if(response.status == 'connected'){
                    var new_data = {};
                    FB.api('/me', { locale: 'en_US', fields: 'name, email' }, function(response) {
                        console.log(JSON.stringify(response));
                        new_data['name'] = response.name;
                        new_data['email'] = response.email;
                        FB.api('/me/picture',{type: 'large'} ,function(response) {
                            console.log(JSON.stringify(response));
                            new_data['image'] = response.data.url;
                            new_data = JSON.stringify(new_data);
                            $('#fb_data').html(new_data);
                            $('#facebook_form').submit();
                        });
                    });
                }
            }, {scope: 'public_profile,email'});
        });

     /*   function checking(){
            if(auth2.isSignedIn.get()){
                var data = auth2.currentUser.get().getBasicProfile();
                console.log(data.getName());

            }
        }*/

        function inDulu() {
            auth2.signIn().then(function() {
                console.log(auth2.currentUser.get().getId());
            });
        }

    });

</script>

