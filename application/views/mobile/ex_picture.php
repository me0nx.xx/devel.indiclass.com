<div id="userpic" class="userpic">
			<div class="js-preview userpic__preview"></div>
			<center><div class="btn btn-success js-fileapi-wrapper">
				<div class="js-browse">
					<span class="btn-txt">Choose</span>
					<input type="file" name="filedata"/>
				</div>
				<div class="js-upload" style="display: none;">
					<div class="progress progress-success"><div class="js-progress bar"></div></div>
					<span class="btn-txt">Uploading</span>
				</div>
			</div></center>
		</div>

		<script>
			examples.push(function (){
				$('#userpic').fileapi({
					url: 'http://rubaxa.org/FileAPI/server/ctrl.php',
					accept: 'image/*',
					imageSize: { minWidth: 200, minHeight: 200 },
					elements: {
						active: { show: '.js-upload', hide: '.js-browse' },
						preview: {
							el: '.js-preview',
							width: 200,
							height: 200
						},
						progress: '.js-progress'
					},
					onSelect: function (evt, ui){
						var file = ui.files[0];

						if( !FileAPI.support.transform ) {
							alert('Your browser does not support Flash :(');
						}
						else if( file ){
							$('#popup').modal({
								closeOnEsc: true,
								closeOnOverlayClick: false,
								onOpen: function (overlay){
									$(overlay).on('click', '.js-upload', function (){
										$.modal().close();
										$('#userpic').fileapi('upload');
									});

									$('.js-img', overlay).cropper({
										file: file,
										bgColor: '#000',
										maxSize: [$(window).width()-100, $(window).height()-100],
										minSize: [200, 200],
										selection: '90%',
										onSelect: function (coords){
											$('#userpic').fileapi('crop', file, coords);
										}
									});
								}
							}).open();
						}
					}
				});
			});
		</script>
		<div id="popup" class="popup">
			<div class="popup__body"><div class="js-img" style="z-index:-10;"></div></div>
			<div style="margin: 0 0 5px; text-align: center;">
				<div class="js-upload btn btn_browse btn_browse_small">Upload</div>
			</div>
		</div>