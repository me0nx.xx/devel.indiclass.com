<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar');

     ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
        <?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
        ?>
    </aside>
    <aside id="chat" class="sidebar c-overflow">

        <div class="chat-search">
            <div class="fg-line">
                <input type="text" class="form-control" placeholder="Search People">
            </div>
        </div>

        <div class="listview">
            <a class="lv-item" href="">
                <div class="media">
                    <div class="pull-left p-relative">
                        <img class="lv-img-sm" src="img/profile-pics/2.jpg" alt="">
                        <i class="chat-status-busy"></i>
                    </div>
                    <div class="media-body">
                        <div class="lv-title"><?php echo $this->session->userdata('lang'); ?>Jonathan Morris</div>
                        <small class="lv-small">Available</small>
                    </div>
                </div>
            </a>

            <a class="lv-item" href="">
                <div class="media">
                    <div class="pull-left">
                        <img class="lv-img-sm" src="img/profile-pics/1.jpg" alt="">
                    </div>
                    <div class="media-body">
                        <div class="lv-title">David Belle</div>
                        <small class="lv-small">Last seen 3 hours ago</small>
                    </div>
                </div>
            </a>

            <a class="lv-item" href="">
                <div class="media">
                    <div class="pull-left p-relative">
                        <img class="lv-img-sm" src="img/profile-pics/3.jpg" alt="">
                        <i class="chat-status-online"></i>
                    </div>
                    <div class="media-body">
                        <div class="lv-title">Fredric Mitchell Jr.</div>
                        <small class="lv-small">Availble</small>
                    </div>
                </div>
            </a>

            <a class="lv-item" href="">
                <div class="media">
                    <div class="pull-left p-relative">
                        <img class="lv-img-sm" src="img/profile-pics/4.jpg" alt="">
                        <i class="chat-status-online"></i>
                    </div>
                    <div class="media-body">
                        <div class="lv-title">Glenn Jecobs</div>
                        <small class="lv-small">Availble</small>
                    </div>
                </div>
            </a>

            <a class="lv-item" href="">
                <div class="media">
                    <div class="pull-left">
                        <img class="lv-img-sm" src="img/profile-pics/5.jpg" alt="">
                    </div>
                    <div class="media-body">
                        <div class="lv-title">Bill Phillips</div>
                        <small class="lv-small">Last seen 3 days ago</small>
                    </div>
                </div>
            </a>

            <a class="lv-item" href="">
                <div class="media">
                    <div class="pull-left">
                        <img class="lv-img-sm" src="img/profile-pics/6.jpg" alt="">
                    </div>
                    <div class="media-body">
                        <div class="lv-title">Wendy Mitchell</div>
                        <small class="lv-small">Last seen 2 minutes ago</small>
                    </div>
                </div>
            </a>
            <a class="lv-item" href="">
                <div class="media">
                    <div class="pull-left p-relative">
                        <img class="lv-img-sm" src="img/profile-pics/7.jpg" alt="">
                        <i class="chat-status-busy"></i>
                    </div>
                    <div class="media-body">
                        <div class="lv-title">Teena Bell Ann</div>
                        <small class="lv-small">Busy</small>
                    </div>
                </div>
            </a>
        </div>
    </aside>


    <section id="content">
        <div class="container">
            <?php if($this->session->flashdata('mes_alert')){ ?>
                        <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <?php echo $this->session->flashdata('mes_message'); ?>
                        </div>
                        <?php } ?>
            <div class="block-header">
                <h2><?php echo $this->lang->line('home'); 
                        //echo $this->session->flashdata('msgSuccess');
                ?></h2>

                <ul class="actions">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="#"><?php echo $this->lang->line('home'); ?></a></li>
                            <!-- <li class="active"></li> -->
                        </ol>
                    </li>
                </ul>
            </div> <!-- akhir block header    -->        
            <?php
            $now = 7-(date('N')-1);
            $nextMon = date('Y-m-d', strtotime('+'.$now.' day', strtotime(date('Y-m-d'))));
            $oneWeek = date('Y-m-d', strtotime('+6 day', strtotime($nextMon)));
            $nextMon = DateTime::createFromFormat('Y-m-d',$nextMon);
            $oneWeek = DateTime::createFromFormat('Y-m-d',$oneWeek);

            $timeArray = array();

            $arrayFul;
            $stime = 0;
            for($x=0;$x<7;$x++){
                $timeArray[$x]["day"] =  date('D, d M', strtotime('+'.$x.' day' , strtotime($nextMon->format('Y-m-d'))));
                $timeArray[$x]["date"] = date('Y-m-d', strtotime('+'.$x.' day' , strtotime($nextMon->format('Y-m-d'))));
                $query_checker[$x] = $this->db->query("SELECT * FROM tbl_avtime WHERE tutor_id=".$this->session->userdata('id_user')." AND date='".$timeArray[$x]["date"]."'")->result_array();
                
                for ($i=0; $i < 24; $i++) {
                    $timeArray[$x][$i] = $stime;
                    $stime+=3600;
                }
                $stime = 0;
            }

            ?> 
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-3 pull-left">
                            <!-- <button>Prev</button> -->
                        </div>
                        <div class="col-sm-6">
                            <h2 class="text-center"><?php echo $nextMon->format('F d')." - ".$oneWeek->format('F d'); ?></h2>
                            <h2 class="text-center m-t-10"><?php echo $nextMon->format('Y'); ?></h2>
                        </div>
                        <div class="col-sm-3 pull-right">
                            <!-- <button>Next</button> -->
                        </div>
                    </div>
                </div>

                <!-- <div style="overflow-x:scroll"> -->
                <div class="card-body card-padding" style="height:auto; width:100%;">
                    <table class="table table-striped" style="text-align:center;">                        
                        <thead>
                            <tr>
                                <th width="5"></th>
                                <?php foreach ($timeArray as $key => $value) {
                                    echo "<th style='text-align:center;'>".$value['day']."</th>";
                                }
                                ?>
                            </tr>                                
                        </thead>                        
                        <tbody>
                            <?php 
                            for($x=0;$x<24;$x++){

                                echo '
                                <tr>
                                    <td>'.sprintf('%02d',$x).':00'.'</td>';
                                foreach ($timeArray as $key => $value) {
                                    $start_time = $value[$x];
                                    $stop_time = $value[$x]+3600;

                                    $AsTime = strtotime($value["date"]." 00:00:00");
                                    $b1 = date('Y-m-d H:i:s',$AsTime+$start_time);
                                    $b2 = date('Y-m-d H:i:s',$AsTime+$stop_time);


                                    $query_sche = $this->db->query("SELECT ts.session_id, ts.subject_id,ts.name, ts.description, ts.tutor_id, tu.user_name, tu.user_image, ts.start_time, ts.finish_time, ts.public, tps.id, tps.id_user FROM `tbl_session` as ts INNER JOIN tbl_participant_session as tps ON ts.session_id=tps.session_id INNER JOIN tbl_user as tu ON tu.id_user=ts.tutor_id WHERE tps.id_user=".$this->session->userdata('id_user')." AND start_time>='$b1' AND finish_time<='$b2'")->result_array();

                                    // print_r('pre');
                                    // print_r($query_sche);
                                    // print_r('</pre>');

                                    if(!empty($query_sche)){
                                        foreach ($query_sche as $key => $value) {
                                            echo '<td>
                                                <h5>'.$value["name"].'</h5><br>
                                                <img src="'.base_url().'aset/img/user/'.$value["user_image"].'" style="border-radius:50%; " width="50px" height="50px"  alt=""><br>
                                                <h6>'.$value["user_name"].'</h6>
                                                <div class="c-info rating-list">                                                
                                                    <div class="rl-star">
                                                        <i class="zmdi zmdi-star active"></i>
                                                        <i class="zmdi zmdi-star active"></i>
                                                        <i class="zmdi zmdi-star active"></i>
                                                        <i class="zmdi zmdi-star active"></i>
                                                        <i class="zmdi zmdi-star"></i>
                                                    </div>
                                                </div>
                                                <a id="buttonSession" class="btn btn-primary" href="https://beta.dev.meetaza.com/angular/#access_session='.$value["id"].'"><img src="'.base_url().'aset/img/login.png"></img>&nbsp;&nbsp;Enter Class</a>
                                            </td>';
                                        }
                                    }else{
                                        echo "<td></td>";
                                    }
                                    
                                    
                                   /* echo '
                                    <td><label class="checkbox checkbox-inline"><input type="checkbox" ';
                                    foreach($query_checker[$key] as $kas => $valas) {
                                        // echo $value[$x];
                                        if($valas['start_time'] == $value[$x] && $valas['available'] == 1){
                                            echo "checked";
                                        }
                                    }
                                    echo ' name="'.$key.'_'.$value[$x].'"><i class="input-helper"></i> </label></td>';*/
                                }
                                echo '
                                </tr>';    
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- </div> -->

        

        </div>
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
