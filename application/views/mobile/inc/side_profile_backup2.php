
<div class="pm-overview c-overflow">

	<div class="pmo-pic"  style="cursor:pointer;">
		<div class="p-relative" data-target="#myModal" data-toggle="modal">
			<a>				
				<img class="img-responsive" src="<?php echo base_url('aset/img/user/'.$dataimage['user_image']); ?>" > 
			</a>

			<div class="pmop-edit" style="cursor:pointer;">
				<i class="zmdi zmdi-camera"></i> <span class="hidden-xs" >Update Profile Picture</span>
			</div>
		</div>

		<!-- <div id="userpic" class="userpic">
			<div class="js-preview userpic__preview" id="kode"></div>
			<center><div class="btn btn-success js-fileapi-wrapper">
				<div class="js-browse">
					<span class="btn-txt">Choose</span>
					<input type="file" name="filedata"/>
				</div>
				<div class="js-upload" style="display: none;">
					<div class="progress progress-success"><div class="js-progress bar"></div></div>
					<span class="btn-txt">Uploading</span>
				</div>
			</div></center>
		</div>

		<script>

			examples.push(function (){


				$('#userpic').fileapi({
					url: 'http://rubaxa.org/FileAPI/server/ctrl.php',
					accept: 'image/*',
					imageSize: { minWidth: 512, minHeight: 512 },
					elements: {
						active: { show: '.js-upload', hide: '.js-browse' },
						preview: {
							el: '.js-preview',
							width: 200,
							height: 200
						},
						progress: '.js-progress'
					},
					onSelect: function (evt, ui){
						var file = ui.files[0];

						if( !FileAPI.support.transform ) {
							alert('Your browser does not support Flash :');
						}
						else if( file ){
							$("#popup").css('display','none');
							$('#popup').modal({
								closeOnEsc: true,
								closeOnOverlayClick: false,
								onOpen: function (overlay){
									$(overlay).on('click', '.js-upload', function (){
										$.modal().close();
										$('#userpic').fileapi('upload');																				
									});

									$('.js-img', overlay).cropper({
										file: file,
										bgColor: '#000',
										maxSize: [$(window).width()-100, $(window).height()-100],
										minSize: [512, 512],
										selection: '90%',
										onSelect: function (coords){
											$('#userpic').fileapi('crop', file, coords);							
										}
									});
								}
							}).open();

						}
					}
				});
			});
		</script>
		<div id="popup" class="popup" style="position:absolute; z-index:10; top:10%; display: none; ">
			<div class="popup__body" style="width:600px; height:600px;"><div class="js-img" id="js_img"></div></div>
			<div style="margin: 0 0 5px; text-align: center;">
				<div class="js-upload btn btn_browse btn_browse_small" id="save_img">Save</div>
			</div>
		</div> -->
		
	</div>

	<div class="pmo-block pmo-contact hidden-xs">
		<h2><?php echo $this->lang->line('contact'); ?></h2>

		<ul>
			<li><i class="zmdi zmdi-phone"></i> <?php echo $this->session->userdata('no_hp'); ?></li>
			<li><i class="zmdi zmdi-email"></i> <?php echo $this->session->userdata('email'); ?></li>                                    
			<li>
				<i class="zmdi zmdi-pin"></i>
				<address class="m-b-0 ng-binding">
					<?php 
					$address = $this->session->userdata('address');
					if ($address == "") {
						echo "No address";
					}
					echo $this->session->userdata('address');
					?>
				</address>
			</li>            
		</ul>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel"></h4>
	      </div>	      
	        <div class="modal-body" style="width:300px;">
	      	<hr>	      		
	      		<form id="form1" method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>Master/update_photo">

				<input type="file" name="gbjalann" id="gbjalann" required />
				<br>
				
				<img src="<?php echo base_url('aset/img/user/'.$dataimage['user_image']);?>" width="100%" height="250px" id="viewgambar" class="m-t-20 m-b-20">
				<input type="submit" value="Upload" class="btn btn-primary btn-block bgm-blue" name="save">
				
				</form>

				<script type="text/javascript">

	function readURL(input) {

	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#blah').attr('src', e.target.result);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	$("#gbjalann").change(function(){
        readURL(this);
    });
</script>

				<!-- <form id="upload_form" enctype="multipart/form-data">
				   <input type="file" name="gbjalann" id="gbjalann"><br>
				   <input type="button" value="Upload File" onclick="uploadFile()">
				   <progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
				   <h3 id="status"></h3>
				   <p id="total"></p>
				</form> -->

				<?php				
				

				// if(isset($_POST['save'])){
				// 	$ekstensi_diperbolehkan	= array('png','jpg');
				// 	$nama = $this->session->userdata('user_image');
				// 	$x = explode('.', $nama);
				// 	$ekstensi = strtolower(end($x));
				// 	$ukuran	= $_FILES['gbjalan']['size'];
				// 	$file_tmp = $_FILES['gbjalan']['tmp_name'];	
		 
				// 	if(in_array($ekstensi, $ekstensi_diperbolehkan) === true){
				// 		if($ukuran > 1000000){										
							
				// 			if(move_uploaded_file($file_tmp, './aset/img/user/'.$nama)){
				// 				echo "<script>alert('Upload Sukses');</script>";
				// 				redirect('/first/about');
				// 			}else{
				// 				echo "<script>alert('Gagal!!!');</script>";
				// 				redirect('/first/about');
				// 			}
				// 		}else{
				// 			echo "<script>alert('File terlalu besar');</script>";
				// 			redirect('/first/about');
				// 		}
				// 	}else{						
				// 		echo "<script>alert('Format gambar tidak diperbolehkan');</script>";
				// 		redirect('/first/about');
				// 	}
				// }			
				 
				?>

				<!-- <div id="gambar" style="display:block;">
					<img width="100%" height="20%" src="<?php echo base_url('aset/img/user/'.$this->session->userdata('user_image')); ?>">
				</div> -->

	      </div>
	      <div class="modal-footer">
	        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    <button type="button" id="btn-crop" class="btn btn-primary">Crop & Save</button> -->
	      </div>
	    </div>
	  </div>
	</div>

</div>

<script type="text/javascript">
	
	jQuery(document).ready(function(){

		jQuery('#change-pic').on('click', function(e){
	        jQuery('#changePic').show();
			jQuery('#change-pic').hide();
	        
	    });
		
		jQuery('#photoimg').on('change', function()   
		{ 
			jQuery("#preview-avatar-profile").html('');
			jQuery("#preview-avatar-profile").html('Uploading....');
			jQuery("#cropimage").ajaxForm(
			{
			target: '#preview-avatar-profile',
			success:    function() { 
					jQuery('img#photo').imgAreaSelect({
					aspectRatio: '1:1',
					onSelectEnd: getSizes,
				});
				jQuery('#image_name').val(jQuery('#photo').attr('file-name'));
				}
			}).submit();

		});
		
		jQuery('#btn-crop').on('click', function(e){
	    e.preventDefault();
	    params = {
	            targetUrl: '<?php echo base_url(); ?>/First/post_profile/save',
	            action: 'save',
	            x_axis: jQuery('#hdn-x1-axis').val(),
	            y_axis : jQuery('#hdn-y1-axis').val(),
	            x2_axis: jQuery('#hdn-x2-axis').val(),
	            y2_axis : jQuery('#hdn-y2-axis').val(),
	            thumb_width : jQuery('#hdn-thumb-width').val(),
	            thumb_height:jQuery('#hdn-thumb-height').val()
	        };

	        saveCropImage(params);
	    });
	    
	 
	    
	    function getSizes(img, obj)
	    {
	        var x_axis = obj.x1;
	        var x2_axis = obj.x2;
	        var y_axis = obj.y1;
	        var y2_axis = obj.y2;
	        var thumb_width = obj.width;
	        var thumb_height = obj.height;
	        if(thumb_width > 0)
	            {

	                jQuery('#hdn-x1-axis').val(x_axis);
	                jQuery('#hdn-y1-axis').val(y_axis);
	                jQuery('#hdn-x2-axis').val(x2_axis);
	                jQuery('#hdn-y2-axis').val(y2_axis);
	                jQuery('#hdn-thumb-width').val(thumb_width);
	                jQuery('#hdn-thumb-height').val(thumb_height);
	                
	            }
	        else
	            alert("Please select portion..!");
	    }
	    
	    function saveCropImage(params) {
	    jQuery.ajax({
	        url: params['targetUrl'],
	        cache: false,
	        dataType: "html",
	        data: {
	            action: params['action'],
	            id: jQuery('#hdn-profile-id').val(),
	             t: 'ajax',
	                                w1:params['thumb_width'],
	                                x1:params['x_axis'],
	                                h1:params['thumb_height'],
	                                y1:params['y_axis'],
	                                x2:params['x2_axis'],
	                                y2:params['y2_axis'],
									image_name :jQuery('#image_name').val()
	        },
	        type: 'Post',
	       // async:false,
	        success: function (response) {
	                jQuery('#changePic').hide();
					jQuery('#change-pic').show();
	                jQuery(".imgareaselect-border1,.imgareaselect-border2,.imgareaselect-border3,.imgareaselect-border4,.imgareaselect-border2,.imgareaselect-outer").css('display', 'none');
	                
	                jQuery("#avatar-edit-img").attr('src', response);
	                jQuery("#preview-avatar-profile").html('');
	                jQuery("#photoimg").val('');
	        },
	        error: function (xhr, ajaxOptions, thrownError) {
	            alert('status Code:' + xhr.status + 'Error Message :' + thrownError);
	        }
	    });
	    }
		});

</script>
