<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Classmiles</title>

	<!-- Vendor CSS -->
	<link href="<?php echo base_url(); ?>aset/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>aset/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>aset/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">        
	<link href="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>aset/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>aset/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>aset/vendors/farbtastic/farbtastic.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>aset/vendors/bower_components/chosen/chosen.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>aset/vendors/summernote/dist/summernote.css" rel="stylesheet">

	<link href="<?php echo base_url(); ?>aset/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">

	<!-- CSS -->
	<link href="<?php echo base_url(); ?>aset/css/app.min.1.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>aset/css/app.min.2.css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>aset/imgareaselect/css/imgareaselect-default.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>aset/sel2/css/select2.css" />

	

	<!-- Following CSS are used only for the Demp purposes thus you can remove this anytime. -->
	<style type="text/css">
		.toggle-switch .ts-label {
			min-width: 130px;
		}
	</style>
</head>

<body <?php if(isset($sat)){ echo $sat; } ?>>   

	<!-- <script src="<?php echo base_url(); ?>aset/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js "></script> -->
	<script src="<?php echo base_url(); ?>aset/vendors/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<script src="<?php echo base_url(); ?>aset/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="<?php echo base_url(); ?>aset/vendors/bower_components/Waves/dist/waves.min.js"></script>
	<script src="<?php echo base_url(); ?>aset/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
	<script src="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
	<script src="<?php echo base_url(); ?>aset/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
	<script src="<?php echo base_url(); ?>aset/vendors/bower_components/moment/min/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>aset/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
	<script src="<?php echo base_url(); ?>aset/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>aset/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>aset/vendors/summernote/dist/summernote-updated.min.js"></script>
	<script src="<?php echo base_url(); ?>aset/sel2/js/select2.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>aset/imgareaselect/scripts/jquery.imgareaselect.pack.js"></script>

	<script src="<?php echo base_url(); ?>aset/vendors/bower_components/chosen/chosen.jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>aset/vendors/fileinput/fileinput.min.js"></script>
	<script src="<?php echo base_url(); ?>aset/vendors/input-mask/input-mask.min.js"></script>
	<script src="<?php echo base_url(); ?>aset/vendors/farbtastic/farbtastic.min.js"></script>
	
	
	<script src="<?php echo base_url(); ?>aset/js/functions.js"></script>
	<!-- // ANJE -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
	<script src="<?php echo base_url(); ?>aset/js/anjeondemand.js"></script>

  <script type="text/javascript">
    
      $(document).ready(function(){
          $('input.timepicker').timepicker({
              timeFormat: 'HH:mm:ss',
              minTime: '11:45:00',
              maxHour: 20,
              maxMinutes: 30,
              startTime: new Date(0,0,0,15,0,0), // 3:00:00 PM - noon
              interval: 15 // 15 minutes
          });
      });

  </script>