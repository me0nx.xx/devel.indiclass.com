<div class="profile-menu">
    <a href="">
        <div class="profile-pic">
            <img src="https://classmiles.com/aset/img/user/'.$this->session->userdata('user_image').'?'.time(); ?>" alt="">
        </div>

        <div class="profile-info">

            <?php 
                $idadmin = $this->session->userdata('id_user');
                $cek     = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$idadmin'")->result_array();
                foreach ($cek as $row => $a) {
                    echo $a['user_name'];
                }
            ?>        
        
            <i class="zmdi zmdi-caret-down"></i>
        </div>
    </a>

    <ul class="main-menu">
        <li>
            <a href="<?php echo base_url('/logout'); ?>"><i class="zmdi zmdi-time-restore"></i> <?php echo $this->lang->line('logout'); ?></a>
        </li>
    </ul>
</div>
<ul class="main-menu">
    <li class="    
        <?php if($sideactive=="homeadmin"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>admin"><i class="zmdi zmdi-home"></i>  <?php echo $this->lang->line('homeadminn'); ?></a>
    </li>
    <li class="    
        <?php if($sideactive=="approval_tutor"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>admin/tutor_approval"><i class="zmdi zmdi-accounts-list"></i>  <?php echo $this->lang->line('aktivasiuser'); ?></a>
    </li>
    <li class="    
        <?php if($sideactive=="enggineadmin"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>admin/Engine"><i class="zmdi zmdi-assignment-o"></i>  <?php echo $this->lang->line('schedule_engine'); ?></a>
    </li>
    <li class="    
        <?php if($sideactive=="support"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>admin/Support"><i class="zmdi zmdi-folder-person"></i>  Support</a>
    </li>

     <li class="    
        <?php if($sideactive=="addsubject"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>admin/Subjectadd"><i class="zmdi zmdi-plus-square"></i>  Add Subject</a>
    </li>

    <li class="<?php if($sideactive=="registerchannel"){ echo "sub-menu active";} else if ($sideactive=="pointchannel"){ echo "sub-menu active";} else if($sideactive=="usepointchannel"){ echo "sub-menu active";} else if($sideactive=="activitychannel"){ echo "sub-menu active";} else{ echo "sub-menu";           
        } ?>">
        <a href=""><i class="zmdi zmdi-view-list-alt"></i> Channel</a>
        <ul>
            <li class="<?php if($sideactive=="registerchannel"){ echo "active";} else{} ?>"> <a href="<?php echo base_url(); ?>admin/Register"> Register Channel</a></li>
            <li><a class="<?php if($sideactive=="pointchannel"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('admin/PointChannel'); ?>">Point Channel</a></li>
            <li><a class="<?php if($sideactive=="usepointchannel"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('admin/UsePointChannel'); ?>"> Use Point Channel</a></li>            
            <li><a class="<?php if($sideactive=="activitychannel"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('admin/ActivityChannel'); ?>"> Activity Channel</a></li>
        </ul>
    </li>

    <li class="<?php if($sideactive=="datauangsaku"){ echo "sub-menu active";} else if ($sideactive=="confrimuangsaku"){ echo "sub-menu active";} else{ echo "sub-menu";           
        } ?>">
        <a href=""><i class="zmdi zmdi-view-list-alt"></i> Uang Saku</a>
        <ul>
            <li><a class="<?php if($sideactive=="datauangsaku"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('admin/Data_transaction'); ?>">Data Transaction</a></li>
            <li><a class="<?php if($sideactive=="confrimuangsaku"){ echo "active"; }else { echo "";} ?>" href="<?php echo base_url('admin/Confrim_transaction'); ?>">Confrim Transaction</a></li>            
        </ul>
    </li>

    <li hidden class="<?php if($sideactive=="profile"){ echo "active"; } else{
        } ?>"> <a href="<?php echo base_url(); ?>admin/about"><i class="zmdi zmdi-account-circle"></i> <?php echo $this->lang->line('profil'); ?></a>
    </li>

    <li class="    
        <?php if($sideactive=="announcement"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>admin/Announcement"><i class="zmdi zmdi-notifications-active"></i>  Announcement</a>
    </li>
</ul>