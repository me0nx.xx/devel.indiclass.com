<div class="profile-menu">
    <a href="">
        <div class="profile-pic">
            <img src="<?php echo "https://classmiles.com/"; ?>aset/img/profile-pics/1.jpg" alt="">
        </div>

        <div class="profile-info">
            <?php echo $this->session->userdata('user_name'); ?>
        
            <i class="zmdi zmdi-caret-down"></i>
        </div>
    </a>
</div>
<ul class="main-menu">

    <li class="<?php if($sideactive=="home"){echo "active";}else{

    } ?>"><a href="<?php echo base_url(); ?>"><i class="zmdi zmdi-home"></i>Dashboard</a></li>   
    <li class="    
        <?php if($sideactive=="datauangsaku"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>operator/DataTransaction"><i class="zmdi zmdi-accounts-list"></i>  Data Transaction</a>
    </li>
    <li class="    
        <?php if($sideactive=="confrimuangsaku"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>operator/ConfrimTransaction"><i class="zmdi zmdi-assignment-o"></i>  Confrim Transaction</a>
    </li>
    <li>
        <a href=""><i class="zmdi zmdi-time-restore"></i> <?php echo $this->lang->line('logout'); ?></a>
    </li>  

</ul>