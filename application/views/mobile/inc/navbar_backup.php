<ul class="header-inner">
    <li id="menu-trigger" data-trigger="#sidebar">
        <div class="line-wrap">
            <div class="line top"></div>
            <div class="line center"></div>
            <div class="line bottom"></div>
        </div>
    </li>

    <li class="hidden-xs">
        <?php if ($this->session->userdata('status')==1) { ?>
            <a href="<?php echo base_url(); ?>" class="m-l-10"><img src="<?php echo base_url(); ?>aset/img/logo/logoclass6.png" alt=""></a>            
        <?php }?>
        <?php if ($this->session->userdata('status')== 0) { ?>
            <a href="#" class="m-l-10"><img src="<?php echo base_url(); ?>aset/img/logo/logoclass6.png" alt=""></a>            
        <?php }?>
        <?php if ($this->session->userdata('status')== 2) { ?>
            <a href="#" class="m-l-10"><img src="<?php echo base_url(); ?>aset/img/logo/logoclass6.png" alt=""></a>            
        <?php }?>
        <?php if ($this->session->userdata('status')== 3) { ?>
            <a href="#" class="m-l-10"><img src="<?php echo base_url(); ?>aset/img/logo/logoclass6.png" alt=""></a>            
        <?php }?>        
        
    </li>

    <li class="pull-right">
        <ul class="top-menu">
            <!-- <li id="toggle-width">
                <div class="toggle-switch">
                    <input id="tw-switch" type="checkbox" hidden="hidden">
                    <label for="tw-switch" class="ts-helper"></label>
                </div>
            </li> -->

            <!-- <li>
                <center>
                <h4 style="color:white;">Contact</h4>
                </center>
            </li> -->            
            
            <!-- <li id="top-search">
                <a href=""><i class="tm-icon zmdi zmdi-search"></i></a>
            </li> -->

            <li class="dropdown">
                <a data-toggle="dropdown" href="">
                    <i class="tm-icon zmdi zmdi-notifications"></i>
                    <?php 
                        if ($this->session->userdata('status')==1) {
                            $id = $this->session->userdata('id_user');
                            $querycek = $this->db->query('SELECT count(*) as jumlah FROM tbl_notif WHERE id_user="$id" AND status="0"')->row_array();
                            if ($querycek['jumlah'] == 0) {
                                
                            }
                        }
                    ?>
                    <?php 
                        if ($this->session->userdata('status')== 0) {
                            $id = $this->session->userdata('id_user');                            
                            $querycek = $this->db->query("SELECT count(*) as jumlah FROM tbl_notif WHERE id_user='$id' AND status='0'")->row_array();
                            if ($querycek['jumlah'] == 0) {
                                echo '<i class="tmn-counts"></i>';
                            }
                            echo "<i class='tmn-counts'>".$querycek['jumlah']."</i>";

                        }
                    ?>

                    <?php 
                        if ($this->session->userdata('status')== 3) {
                            $id = $this->session->userdata('id_user');                            
                            $querycek = $this->db->query("SELECT count(*) as jumlah FROM tbl_notif WHERE id_user='$id' AND status='0'")->row_array();
                            if ($querycek['jumlah'] == 0) {
                                echo '<i class="tmn-counts"></i>';
                            }
                            echo "<i class='tmn-counts'>".$querycek['jumlah']."</i>";
                        }
                    ?>

                    <?php 
                        if ($this->session->userdata('status')== 2) { 
                            $id = $this->session->userdata('id_user');                            
                            $querycek = $this->db->query("SELECT count(*) as jumlah FROM tbl_notif WHERE id_user='$id' AND status='0'")->row_array();
                            if ($querycek['jumlah'] == 0) {
                                echo '<i class="tmn-counts"></i>';
                            }
                            echo "<i class='tmn-counts'>".$querycek['jumlah']."</i>";
                        }
                    ?>
                </a>
                <div class="dropdown-menu dropdown-menu-lg pull-right">
                    <div class="listview" id="notifications">
                        <div class="lv-header">
                            Notification

                            <ul class="actions">
                                <li class="dropdown">
                                    <a href="" data-clear="notification">
                                        <i class="zmdi zmdi-check-all"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <?php 
                        if ($this->session->userdata('status')==0 && $this->session->userdata('usertype_id')=="student") { ?>
                            <div class="lv-body">
                                <a class="lv-item">
                                    <div class="media" data-toggle="tooltip" data-placement="bottom" title="Please, check your email to verify this account">
                                        <div class="pull-left">
                                            <img class="lv-img-sm" src="<?php echo base_url(); ?>aset/img/icons/notif.png">                                        
                                        </div>
                                        <div class="media-body">
                                            <div class="lv-title">Administrator</div>
                                            <small class="lv-small">Please, check your email to verify this account</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php 
                        }
                        else if($this->session->userdata('status')==3 && $this->session->userdata('usertype_id')=="tutor")
                        {
                        ?>
                            <div class="lv-body">
                                <a class="lv-item">
                                    <div class="media" data-toggle="tooltip" data-placement="bottom" title="Please, complete your profile for tutor registration approval">
                                        <div class="pull-left">
                                            <img class="lv-img-sm" src="<?php echo base_url(); ?>aset/img/icons/notif.png">                                        
                                        </div>
                                        <div class="media-body">
                                            <div class="lv-title">Administrator</div>
                                            <small class="lv-small">Please, complete your profile for tutor registration approval</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php 
                        }
                        else if($this->session->userdata('status')==2 && $this->session->userdata('usertype_id')=="tutor")
                        {
                        ?>
                            <div class="lv-body">
                                <a class="lv-item">
                                    <div class="media" data-toggle="tooltip" data-placement="bottom" title="Your request has been sent, please wait for approval from Classmiles Team. Most probably, we will try to contact you through email or your phone number. Thank you.">
                                        <div class="pull-left">
                                            <img class="lv-img-sm" src="<?php echo base_url(); ?>aset/img/icons/notif.png">                                        
                                        </div>
                                        <div class="media-body">
                                            <div class="lv-title">Administrator</div>
                                            <small class="lv-small">Your request has been sent, please wait for approval from Classmiles Team. Most probably, we will try to contact you through email or your phone number. Thank you.</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php 
                        }
                        else if($this->session->userdata('status')==0 && $this->session->userdata('usertype_id')=="tutor")
                        {
                        ?>
                            <div class="lv-body">
                                <a class="lv-item">
                                    <div class="media" data-toggle="tooltip" data-placement="bottom" title="Please, check your email to verify this account">
                                        <div class="pull-left">
                                            <img class="lv-img-sm" src="<?php echo base_url(); ?>aset/img/icons/notif.png">                                        
                                        </div>
                                        <div class="media-body">
                                            <div class="lv-title">Administrator</div>
                                            <small class="lv-small">Please, check your email to verify this account</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php 
                        }
                        else if($this->session->userdata('status')==1)
                        {
                        ?>
                            
                        <?php
                        }
                        ?>

                        <!-- <a class="lv-footer" href="">View Previous</a> -->
                    </div>

                </div>
            </li>
            <!-- <li class="dropdown">
                <a data-toggle="dropdown" href="">
                    <i class="tm-icon zmdi zmdi-view-list-alt"></i>
                    <i class="tmn-counts">2</i>
                </a>
                <div class="dropdown-menu pull-right dropdown-menu-lg">
                    <div class="listview">
                        <div class="lv-header">
                            Tasks
                        </div>
                        <div class="lv-body">
                            <div class="lv-item">
                                <div class="lv-title m-b-5">HTML5 Validation Report</div>

                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%">
                                        <span class="sr-only">95% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="lv-item">
                                <div class="lv-title m-b-5">Google Chrome Extension</div>

                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                        <span class="sr-only">80% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="lv-item">
                                <div class="lv-title m-b-5">Social Intranet Projects</div>

                                <div class="progress">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                        <span class="sr-only">20% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="lv-item">
                                <div class="lv-title m-b-5">Bootstrap Admin Template</div>

                                <div class="progress">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                        <span class="sr-only">60% Complete (warning)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="lv-item">
                                <div class="lv-title m-b-5">Youtube Client App</div>

                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                        <span class="sr-only">80% Complete (danger)</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <a class="lv-footer" href="">View All</a>
                    </div>
                </div>
            </li>  -->        

            <li class="dropdown" style="height: 35px;">
                <a data-toggle="dropdown" href="">
                     
                    <?php
                        $patokan =  $this->lang->line('profil');

                        if ($patokan == "Profile") {
                            ?>
                            <img class="m-t-5" width="35px" height="20px" style="border-radius:15%;" src="<?php echo base_url('aset/img/language/flaginggris.png'); ?>"                
                            <?php
                        }
                        else if ($patokan == "Data Diri"){
                            ?>
                            <img class="m-t-5" width="35px" height="20px" style="border-radius:15%;" src="<?php echo base_url('aset/img/language/flagindo.png'); ?>"                
                            <?php
                        }
                    ?>                           
                </a>
                <div class="dropdown-menu dropdown-menu-lg pull-right">
                    <div class="listview" id="notifications">

                        <div class="lv-header">
                            Choose language                                                            
                        </div>

                        <div class="lv-body" id="checklang">                            
                            <a class="lv-item" href="<?php echo base_url('set_lang/english'); ?>" id="btn_setenglish">
                                <div class="media">
                                    <div class="pull-left">
                                        <img width="35px" height="20px" style="border-radius:15%;" src="<?php echo base_url(); ?>aset/img/language/flaginggris.png" alt="">
                                    </div>
                                    <div class="media-body">
                                        <div class="lv-title">English</div>                                                    
                                    </div>
                                </div>
                            </a>  
                            <a class="lv-item" href="<?php echo base_url('set_lang/indonesia'); ?>" id="btn_setindonesia">
                                <div class="media">
                                    <div class="pull-left">
                                        <img width="35px" height="20px" style="border-radius:15%;" src="<?php echo base_url(); ?>aset/img/language/flagindo.png" alt="">
                                    </div>
                                    <div class="media-body">
                                        <div class="lv-title">Bahasa Indonesia</div>                                                    
                                    </div>
                                </div>
                            </a>         
                            <!-- <a class="lv-item" href="<?php echo base_url('set_lang/arab'); ?>">
                                <div class="media">
                                    <div disabled="true" class="pull-left">
                                        <img class="lv-img-sm" src="<?php echo base_url(); ?>aset/img/language/saudi_arabia.png" alt="">
                                    </div>
                                    <div class="media-body">
                                        <div class="lv-title">Arabic</div>                                                    
                                    </div>
                                </div>
                            </a> -->                                      
                        </div>                                                
                    </div>

                </div>
            </li>

            <li class="dropdown">
                <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-more-vert"></i></a>
                <ul class="dropdown-menu dm-icon pull-right">
                    <li class="skin-switch hidden-xs">
                        <span class="ss-skin bgm-lightblue" data-skin="lightblue"></span>
                        <span class="ss-skin bgm-bluegray" data-skin="bluegray"></span>
                        <span class="ss-skin bgm-cyan" data-skin="cyan"></span>
                        <span class="ss-skin bgm-teal" data-skin="teal"></span>
                        <span class="ss-skin bgm-orange" data-skin="orange"></span>
                        <span class="ss-skin bgm-blue" data-skin="blue"></span>
                    </li>
                    <li class="divider hidden-xs"></li>
                    <li class="hidden-xs">
                        <a data-action="fullscreen" href=""><i class="zmdi zmdi-fullscreen"></i><?php echo $this->lang->line('togglefullscreen'); ?></a>
                    </li>
                    <li>
                        <a onclick="signOut()" data-action="" href="<?php echo base_url('/logout'); ?>"><i class="zmdi zmdi-time-restore"></i><?php echo $this->lang->line('logout'); ?></a>
                    </li>
                </ul>
            </li>
           <!--  <li class="hidden-xs" id="chat-trigger" data-trigger="#chat">
                <a href=""><i class="tm-icon zmdi zmdi-comment-alt-text"></i></a>
            </li> -->
        </ul>
    </li>
</ul>


<!-- Top Search Content -->
<!-- <div id="top-search-wrap">
    <div class="tsw-inner">
        <i id="top-search-close" class="zmdi zmdi-arrow-left"></i>
        <input type="text">
    </div>
</div> -->

<script type="text/javascript">
    $(document).ready(function(){
        $('#btn_setindonesia').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
        });
        $('#btn_setenglish').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/english'); ?>',function(){ location.reload(); });
        });
    });
</script>