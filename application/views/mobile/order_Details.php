<style type="text/css">
    .modal-backdrop.in {
        opacity: 0.9;
    }
    body{
        background-color: white;
        margin: 0; height: 100%; overflow: hidden;
    }

    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;  
        opacity: 0.7;      
        background: url('https://2aih25gkk2pi65s8wfa8kzvi-wpengine.netdna-ssl.com/gre/wp-content/plugins/magoosh-lazyload-comments-better-ui/assets/img/loading.gif') center no-repeat #000;
        background-size: 150px 150px;
    }
    .modal-content  {
        -webkit-border-radius: 0px !important;
        -moz-border-radius: 0px !important;
        border-radius: 10px !important; 
    }
    .bp-header:hover
    {
        cursor: pointer;
    }
    .ci-avatar {
        background-color: rgba(255, 255, 255, 1);
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .ci-avatar:hover {
        background-color: rgba(52, 73, 94, 0.2);
    }
    .modal-body{
        max-height: 80vh;
    }
</style>

<header id="header" class="clearfix" data-current-skin="blue" style="background-color: #008080;">
    <div class="" style="">
        <h2 style="color: white"><a style="color: white" href="<?php echo base_url();?>"><i class="zmdi zmdi-arrow-back"> </i></a> <b style="padding-left: 5%">Detail Invoice</b></h2>
    </div>
</header>

<section id="main" style="padding-bottom: 0; padding-top: 20%;">
    
    <?php       
        $order_id = null;
        $UnixCode = null;         
        if (!isset($_GET['order_id']) != null) {
            ?>
            <div class="alert alert-info" role="alert">Tidak ada data</div>
            <?php 
        }
        else
        {                
            $order_id           = $_GET['order_id'];
            $order              = $this->db->query("SELECT * FROM tbl_order as tor INNER JOIN tbl_invoice as ti ON tor.order_id=ti.order_id WHERE tor.order_id='$order_id'")->row_array();                    
            if (empty($order)) {
                $this->session->set_flashdata('mes_alert','info');
                $this->session->set_flashdata('mes_display','block');
                $this->session->set_flashdata('mes_message','Maaf, Order tersebut tidak dapat kami temukan diserver kami.');
                redirect('/');
            }
            else
            {
                $invoice_id         = $order['invoice_id'];
                $datetrans          = $order['last_update']; 
                $UnixCode           = $order['unix_code'];
                $order_status       = $order['order_status'];
                $hargakelas         = $order['total_price']-$UnixCode;
                $hargakelas         = number_format($hargakelas, 0, ".", ".");
                $hasiljumlah        = number_format($order['total_price'], 0, ".", ".");

                $user_utc           = $this->session->userdata('user_utc');
                $server_utcc        = $this->Rumus->getGMTOffset();
                $intervall          = $user_utc - $server_utcc;
                $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$datetrans);
                $tanggaltransaksi->modify("+".$intervall ." minutes");
                $tanggall           = $tanggaltransaksi->format('d-m-Y');    
                $tanggaltransaksi   = $tanggaltransaksi->format('Y-m-d H:i:s');                    
                $dateee             = $tanggaltransaksi;
                $datetime           = new DateTime($dateee);
                $datetime->modify('+1 hours');
                $limitdateee        = $datetime->format('Y-m-d H:i:s');
                $showlimitdate      = $datetime->format('D, d M Y H:i');

                $datelimit          = date_create($limitdateee);
                $dateelimit         = date_format($datelimit, 'd/m/y');
                $harilimit          = date_format($datelimit, 'd');
                $hari               = date_format($datelimit, 'l');
                $tahunlimit         = date_format($datelimit, 'Y');
                $waktulimit         = date_format($datelimit, 'H:s');
                                                        
                $datelimit          = $dateelimit;
                $sepparatorlimit    = '/';
                $partslimit         = explode($sepparatorlimit, $datelimit);
                $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));                        
            ?>

            <div class="card bgm-white" style="margin-bottom: 0;">
                <div class="card " style="padding: 2%; margin-bottom: 0;  background-color: rgba(0, 128, 128, 0.8)">
                    <div class="c-white m-b-5">INVOICE</div>
                    <h4 class="m-0 c-white f-200"><?php echo $invoice_id;?></h4>
                </div>
                <div class="card-header text-center">
                    <label>Make the payment immediately before :</label>
                    <h4><?php echo $showlimitdate ;?></h4>
                </div>
                <div class="card-body " style="padding: 7%">
                    <div class="col-md-12" style="padding: 2%; margin-bottom: 0;  background-color: rgba(237, 236, 236)">
                        <div class="row">
                            <div class="col-xs-6">
                                <label>Total Price Product</label><br>    
                                <label>Unique Code</label>    
                            </div>
                            <div class="col-xs-6" align="right">
                                <label><?php echo "Rp. ". $hargakelas ; ?></label><br>
                                <label><?php echo "Rp. ". $UnixCode ; ?></label><br>
                            </div>
                        </div>
                    </div>
                    <div align="center" style="padding: 2%; color: black">
                        <label style="margin-top: 2%;">Total Payment</label><br>
                        <label style="font-size: 7vh" ><?php echo "Rp. ".$hasiljumlah ;?></label >
                    </div>
                </div>
            </div>
            <div class="card bgm-white" style=" border-top: 1px solid">
                <div class="card-header">
                    <h4>Detail</h4>
                </div>
                <?php
                    
                    $getdataorder = $this->db->query("SELECT tod.*, trm.class_id as class_id, tc.name as subject_name, tr.subject_id as tr_subject_id, tr.tutor_id as tr_tutor_id, tr.approve as tr_approve, tr.date_requested as tr_date_requested, tr.topic as tr_topic, trg.subject_id as trg_subject_id, trg.tutor_id as trg_tutor_id, trg.approve as trg_approve, trg.date_requested as trg_date_requested, trg.topic as trg_topic, tc.subject_id as trm_subject_id, tc.tutor_id as trm_tutor_id, trm.status as trm_approve, tc.start_time as trm_date_requested, tc.description as trm_topic,trp.status as trp_approve, tpp.description as trp_topic, trp.created_at as trp_date_requested FROM tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tc ON tc.class_id=trm.class_id ) ON trm.request_id=tod.product_id LEFT JOIN (tbl_request_program as trp INNER JOIN tbl_price_program as tpp ON trp.program_id=tpp.list_id) ON tod.product_id=trp.request_id WHERE tod.order_id='$order_id'")->result_array();

                        $no = 1;
                        foreach ($getdataorder as $row => $v) {
                            $startimeclass  = $v['tr_date_requested'] != null ? $v['tr_date_requested'] : ( $v['trg_date_requested'] != null ? $v['trg_date_requested'] : ( $v['trm_date_requested'] != null ? $v['trm_date_requested'] : $v['trp_date_requested']));                                                
                            $subject_id     = $v['tr_subject_id'] != null ? $v['tr_subject_id'] : ( $v['trg_subject_id'] != null ? $v['trg_subject_id'] : ( $v['trm_subject_id'] != null ? $v['trm_subject_id'] : ''));
                            $topic          = $v['tr_topic'] != null ? $v['tr_topic'] : ( $v['trg_topic'] != null ? $v['trg_topic'] : ( $v['trm_topic'] != null ? $v['trm_topic'] : $v['trp_topic']));
                            $tutor_id       = $v['tr_tutor_id'] != null ? $v['tr_tutor_id'] : ( $v['trg_tutor_id'] != null ? $v['trg_tutor_id'] : $v['trm_tutor_id']);

                            if ($subject_id == '') {
                                $subject_name   = "";
                                $tutor_name     = "";
                                $tutor_image    = "";
                            }
                            else
                            {
                                $subject_name       = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
                                $tutor_name         = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_name'];
                                $tutor_image        = $this->db->query("SELECT user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_image'];
                            }                                                
                            $waktumulai         = DateTime::createFromFormat ('Y-m-d H:i:s',$startimeclass);
                            $waktumulai->modify("+".$intervall ." minutes");
                            $waktumulai         = $waktumulai->format('d-m-Y H:i:s'); 
                            $priceperclass      = $v['init_price'];  
                            $priceperclass      = number_format($priceperclass, 0, ".", ".");
                        ?>
                            <div class="col-md-12" style="padding: 5%; margin-bottom: 2%; background-color: rgba(237, 236, 236);  border-bottom: 1px solid; border-top: 1px solid">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <img class="img-circle" style="width: 40px; height: 40px" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$tutor_image ;?>">
                                    </div>
                                    <div class="col-xs-10">
                                        <label><?php echo $tutor_name;?></label><br>
                                        <label><?php echo $subject_name; ?></label><div class="pull-right" style="border-left: 1px solid; padding-left: 2%;"><?php echo ' Rp '.$priceperclass;?></div>
                                    </div>
                                    
                                </div>
                            </div>                        
                        <?php
                    $no++;
                }
                ?>
            </div>
            <div style="padding: 5%">
                 <button class="btn btn-danger btn-lg btn-block waves-effect f-14" id="btnchoosemethod" data-price="<?php echo $hasiljumlah; ?>" data-order="<?php echo $order_id;?>">                                
                Pilih Metode Pembayaran
            </button>     
            </div>
           

            <?php 
            }
        }
    ?>
    <div class="se-pre-con" style="display: none;"></div>
    <div class="modal fade in modalchoose" id="mdlchoosemethod" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" style="border-radius: 5%;">
                <div class="modal-content" style="background-color: rgba(248, 248, 247, 1);">
                    <div class="modal-header" style="background-color: #3498db;">                        
                        <h4 class="modal-title m-l-5 c-white pull-left">Pembayaran</h4>
                        <button type="button" class="close c-white" id="closechoosemethod">&times;</button>
                    </div>
                    <div>
                        <div class="modal-body m-t-15">
                            <div class="card-header bgm-white">
                                <p class="lead" style="color:#22313F; line-height: 1.42857143; font-family: open sans,sans-serif; font-size: 22px; padding-left: 17px; padding-top: 10px; padding-bottom: 10px;"><sup style="font-size: 12px;">Jumlah</sup><br>Rp. <label id="totaljumlah"></label></p>
                            </div>                
                        </div>
                        <input type="text" name="" id="orderidd" hidden>
                        <input type="text" name="" id="pilihan" hidden>
                    </div>
                    <div id="label">
                        <div class="modal-body">
                            <div class="card-header" style="background-color: rgba(248, 248, 247, 1);">
                                <p style="font-size: 15px;">Pilih metode pembayaran</p>
                            </div>
                        </div>
                    </div>
                    <div id="mandiri" data-bankid="008">
                        <div class="modal-body" style="padding: 0; margin: 0;">                            
                            <table class="table i-table waves-effect ci-avatar" style="cursor: pointer;">
                                <tbody>
                                    <td height="13">
                                        <img class="m-t-5" src="https://upload.wikimedia.org/wikipedia/en/thumb/f/fa/Bank_Mandiri_logo.svg/1280px-Bank_Mandiri_logo.svg.png" style="height: 27px; width: 80px;">
                                    </td>
                                    <td height="13" class="f-16" style="text-align: right;">
                                        <label class="m-t-15 pull-right" style="cursor: pointer;">Transfer Bank Mandiri</label>
                                    </td>
                                </tbody>
                            </table>
                        </div>
                    </div>  
                    <div id="paypal" data-bankid="000" style="margin-bottom: 15%;">
                        <div class="modal-body" style="padding: 0; margin: 0;">
                            <table class="table i-table waves-effect ci-avatar" style="cursor: pointer;">
                                <tbody>
                                    <td height="13">
                                        <img class="m-t-5" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL.'baru/PayPalLogo.png';?>" style="height: 40px; width: 80px;">
                                    </td>
                                    <td height="13" class="f-16" style="text-align: right;">
                                        <label class="m-t-15 pull-right" style="cursor: pointer;">Transfer Paypal</label>
                                    </td>
                                </tbody>
                            </table>
                        </div>
                    </div> 
                    <div id="rincianmandiri" style="display: none;">
                        <div class="modal-header" style="background-color: #ffffff; border-bottom: 1px solid #EEEEEE;">
                            <h4 class="modal-title m-l-5 c-black pull-left lead">Transfer Mandiri</h4> 
                            <label class="c-black pull-right" id="backmandiri"><img src="http://www.myiconfinder.com/uploads/iconsets/256-256-dc0fa3f38f18c9d1b4e2c99967401405-Arrow.png" style="width: 20px; height: 20px; cursor: pointer;" title="Kembali"></label>                           
                        </div>                        
                        <div class="modal-body bgm-white" style="margin: 0;"> 
                            <div class="p-t-10">
                                <div class="form-group fg-line">
                                    <label class="c-gray f-11" style="font-family: Helvetica;">No. Rekening Anda</label>
                                    <input type="text" name="norekening" id="norekening" maxlength="30" class="form-control f-14">
                                    <small class="m-t-5" style="color: #BFBFBF;">Masukan nomor rekening sesuai buku tabungan</small>
                                </div>                                
                                <div class="form-group fg-line">
                                    <label for="exampleInputEmail1" class="c-gray f-11" style="font-family: Helvetica;">Nama Pemilik Rekening</label>
                                    <input type="text" name="namarekening" id="namarekening" class="form-control f-14">
                                    <small class="m-t-5" style="color: #BFBFBF;">Masukan nama pemilik rekening sesuai buku tabungan</small>
                                </div>                                
                            </div>
                            <div class="p-l-5 p-r-5 p-t-15">
                                <ul>
                                    <li>Periksa kembali data pembayaran Anda sebelum melanjutkan transaksi.</li>
                                    <li>Harap bayar sesuai dengan jumlah yang tertera</li>
                                    <li>Melakukan transfer bank hanya tertuju atas nama PT Meetaza Prawira Media</li>
                                </ul>
                            </div>                            
                        </div>                        
                    </div>
                    <div id="rincianpaypal" style="display: none;">
                        <div class="modal-header" style="background-color: #ffffff; border-bottom: 1px solid #EEEEEE;">
                            <h4 class="modal-title m-l-5 c-black pull-left lead">Transfer Paypal</h4> 
                            <label class="c-black pull-right" id="backpaypal"><img src="http://www.myiconfinder.com/uploads/iconsets/256-256-dc0fa3f38f18c9d1b4e2c99967401405-Arrow.png" style="width: 20px; height: 20px; cursor: pointer;" title="Kembali"></label>                           
                        </div>                        
                        <div class="modal-body bgm-white" style="margin: 0;"> 
                            <div class="p-l-5 p-r-5 p-t-15">
                                <ul>
                                    <li>Periksa kembali data pembayaran Anda sebelum melanjutkan transaksi.</li>
                                    <li>Harap bayar sesuai dengan jumlah yang tertera</li>
                                    <li>Melakukan transfer bank hanya tertuju atas nama PT Meetaza Prawira Media</li>
                                </ul>
                            </div>                            
                        </div>                        
                    </div>    
                    <div class="modal-footer" style="margin: 0; display: none;" id="footerbutton">
                        <input type="text" id="txt_choosemethod" hidden>
                        <button type="button" class="btn btn-danger btn-lg waves-effect btn-block f-13" id="btnbayarr" data-orderid="" style="display: block;">Bayar</button>
                        <a id="lnk_click"><button type="button" class="btn btn-danger btn-lg waves-effect btn-block f-13" id="btnbayarpaypal" data-orderid="" style="display: none;">Bayar</button></a>
                    </div>                 
                
                </div>
            </div>
        </div>
    
</section>

<script type="text/javascript">
    $(document).ready(function(){
        
        var bankidpilihan = null;
        var userid = "<?php echo $this->session->userdata('id_user');?>";
        $("#norekening").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $(document).on("click", "#btnchoosemethod", function () {
        	var orderid 	= $(this).data('order');
            var price     = $(this).data('price');
        	$("#pilihan").val("");
        	$("#orderidd").val("");
            $("#totaljumlah").text("");
        	$('#orderidd').val(orderid);
            $('#totaljumlah').text(price);
            angular.element(document.getElementById('btnchoosemethod')).scope().loadings();
            setTimeout(function(){                
                angular.element(document.getElementById('btnchoosemethod')).scope().loadingsclose();
            },1000); 
            setTimeout(function(){
            	               
                $("#mdlchoosemethod").modal('show');                
            },1200);                  
        });

        $(document).on("click", "#mandiri", function () { 
            $("#txt_choosemethod").val('mandiri');
            $("#paypal").css('display','none');
            $("#mandiri").css('display','block');
            $("#btnbayarpaypal").css('display','none');
            $("#btnbayar").css('display','block');
            $("#footerbutton").css('display','block');
            bankidpilihan = $(this).data('bankid');
            $.ajax({
                url: '<?php echo base_url(); ?>master/get_rekuser',
                type: 'GET',
                data: {
                    bank_id : bankidpilihan
                },               
                success: function(data)
                { 
                    var sdata = JSON.parse(data);                    
                    if (sdata['status'] == 1) {
                        var nama_akun = sdata['nama_akun'];
                        var nomer_rekening = sdata['norek'];                        

                        $("#pilihan").val('bank_transfer');
                        $("#label").css('display','none');
                        $("#mandiri").css('display','none');
                        $("#rincianmandiri").css('display','block');
                        $("#footerbutton").css('display','block');
                        $("#btnbayarr").css('display','block');
                        $("#namarekening").val(nama_akun);
                        $("#norekening").val(nomer_rekening);
                    }
                    else
                    {
                        $("#pilihan").val('bank_transfer');
                        $("#label").css('display','none');
                        $("#mandiri").css('display','none');
                        $("#rincianmandiri").css('display','block');
                        $("#footerbutton").css('display','block');
                    }
                }
            });                    	
        });

        $(document).on("click", "#paypal", function () {
            $("#txt_choosemethod").val('paypal');
            $("#label").css('display','none');
            $("#mandiri").css('display','none');
            $("#rincianmandiri").css('display','none');
            $("#rincianpaypal").css('display','block');            
            $("#btnbayarr").css('display','none');
            $("#btnbayarpaypal").css('display','block');
            $("#footerbutton").css('display','block');
            var order_id        = $("#orderidd").val();           
            $('#lnk_click').attr('href','<?php echo BASE_URL();?>process/submitMethodPaymentPaypal/'+order_id);
        });

        $(document).on("click", "#backmandiri", function () {
            $("#footerbutton").css('display','none');
            $("#rincianmandiri").css('display','none');
            $("#label").css('display','block');
            $("#mandiri").css('display','block');  
            $("#paypal").css('display','block');              
            $("#btnbayarpaypal").css('display','none');
            $("#btnbayar").css('display','block');
        });

        $(document).on("click", "#backpaypal", function () {
            $("#footerbutton").css('display','none');
            $("#rincianpaypal").css('display','none');
            $("#btnbayarpaypal").css('display','none');
            $("#btnbayar").css('display','block');
            $("#label").css('display','block');
            $("#mandiri").css('display','block');
            $("#paypal").css('display','block');            
        });

        $(document).on("click", "#closechoosemethod", function () {
        	$("#footerbutton").css('display','none');
            $("#rincianmandiri").css('display','none');
            $("#btnbayarpaypal").css('display','none');
            $("#label").css('display','block');
            $("#mandiri").css('display','block');            
            $("#mdlchoosemethod").modal('hide');
        });

        $(document).on("click", "#btnbayarr", function () {
            var choosemethod    = $("#txt_choosemethod").val();
            var order_id        = $("#orderidd").val();
            var method          = $("#pilihan").val();         	
            var nomer_rekening  = $("#norekening").val();
            var nama_akun       = $("#namarekening").val();

            $('#mdlchoosemethod').modal('hide');   
            angular.element(document.getElementById('backmandiri')).scope().loadings();   
        	$.ajax({
                url: '<?php echo base_url(); ?>process/submitMethodPayment',
                type: 'POST',
                data: {
                	order_id : order_id,
                    payment_type: method,
                    bank_id : bankidpilihan,
                    nama_akun : nama_akun,
                    nomer_rekening : nomer_rekening                    
                },               
                success: function(data)
                {                     
                	var sdata = JSON.parse(data);
                	if (sdata['status'] == 1) {
                        angular.element(document.getElementById('backmandiri')).scope().loadingsclose();
                		window.location.href="<?php echo base_url();?>DetailPayment?order_id="+order_id;
                	}
                	else
                	{
                        angular.element(document.getElementById('backmandiri')).scope().loadingsclose();
                		notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Terjadi kesalahan, silahkan coba kembali");
                		setTimeout(function(){
                			location.reload();
                		},2000);                		
                	}
                }
            });
            
        });

    });
</script>