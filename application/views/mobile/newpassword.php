<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<style type="text/css">
	html,body{
		background: url('https://classmiles.com/aset/img/headers/backnewpass.png'); background-repeat: no-repeat; 
    background-position: 0 0;
    background-size: cover; 
    background-attachment: fixed;
	}
</style>
	<section id="content" >
		<div class="container">

			<div class="col-sm-2">
			</div>    
			<div class="col-sm-8" style="margin-top:13%;">

				<div class="card brd-2 z-depth-4">

					<div class="listview">

						<div style="height:7px;" class="bgm-green">							
						</div>

						<div class="lv-body">
							<div style="margin-left:7%; margin-right:7%; margin-top:5%; width:100%;">
								<label style="font-size:19px; color:#6C7A89; font-family:Arial;">Reset your password</label><br>
								<label style="font-size:12px; color:#6C7A89; font-family:Arial;">Enter a new password for your account</label>
							</div>
							
							<div style="margin-left:7%; margin-right:7%; margin-top:4%;">
							<form action="<?php echo base_url('/master/aksi_forgot'); ?>" method="post">
								<div class="form-group fg-float">
								<input type="hidden" name="r_link" value="<?php echo $this->input->get('randomLink'); ?>" required class="form-control fg-input" style="color:#009688;">
	                                	<input type="hidden" name="email_user" value="<?php echo $this->input->get('email'); ?>" required class="form-control fg-input" style="color:#009688;">
	                                <div class="fg-line">
	                                    <input type="password" id="kata_sandi" name="pass" required class="form-control fg-input" style="color:#009688;">
	                                    <label class="fg-label" style="color:gray;"><?php echo $this->lang->line('new_pass'); ?></label>
	                                </div>
	                            </div>
	                            <br>
								<div class="form-group fg-float">
	                                <div class="fg-line">
	                                    <input type="password" id="konf_kata_sandi" name="confirm_pass" required class="form-control fg-input" style="color:#009688;">
	                                    <label class="fg-label" style="color:gray;"><?php echo $this->lang->line('confirm_pass'); ?></label>
	                                </div>
	                            </div>
	                            <br>
	                            <div class="form-group">
	                            	<button type="submit" id="submit_signup" class="btn btn-primary btn-block">Reset Password</button>
	                            </div>
	                            <br>
	                        </form>
							</div>
						</div>

					</div>
					<div class="col-sm-2">                                                 
					</div>  

				</div>
			</section>
		</section>

		<!-- <footer id="footer"> -->
			<script type="text/javascript">
				$(document).ready(function(){
					$('#kata_sandi').on('keyup',function(){
						if($('#konf_kata_sandi').val() != $(this).val()){
							$('#submit_signup').attr('disabled','');
						}else{
							$('#submit_signup').removeAttr('disabled');
						}
					});
					$('#konf_kata_sandi').on('keyup',function(){
						if($('#kata_sandi').val() != $(this).val()){
							$('#submit_signup').attr('disabled','');
						}else{
							$('#submit_signup').removeAttr('disabled');
						}
					});
				})

			</script>			
		<!-- </footer> -->
