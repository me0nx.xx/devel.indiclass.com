<div class="profile-menu">
    <a href="">
        <div class="profile-pic">
            <img src="<?php echo base_url('aset/img/user/'.$this->session->userdata('user_image')); ?>" alt="">
        </div>

        <div class="profile-info">
            <?php echo $this->session->userdata('nama_lengkap');
            ?>        
        
            <i class="zmdi zmdi-caret-down"></i>
        </div>
    </a>

    <ul class="main-menu">
        <li>
        <a href="profile-about.php"><i class="zmdi zmdi-account"></i> <?php echo $this->lang->line('viewprofile'); ?></a>
        </li>
        <li>
            <a href=""><i class="zmdi zmdi-settings"></i> <?php echo $this->lang->line('settings'); ?></a>
        </li>
        <li>
            <a href=""><i class="zmdi zmdi-time-restore"></i> <?php echo $this->lang->line('logout'); ?></a>
        </li>
    </ul>
</div>
<ul class="main-menu">
    <li class="    
        <?php if($sideactive=="hometutor"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>tutor"><i class="zmdi zmdi-home"></i>  <?php echo $this->lang->line('hometutor'); ?></a>
    </li>
    <li class="    
        <?php if($sideactive=="setavailabilitytime"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>tutor/availability"><i class="zmdi zmdi-view-list-alt"></i>  <?php echo $this->lang->line('setavailabilitytime'); ?></a>
    </li>
    <li class="    
        <?php if($sideactive=="choosingsubjecttutor"){ echo "active";} else{            
        } ?>"> <a href="<?php echo base_url(); ?>tutor/choose"><i class="zmdi zmdi-dns"></i>  <?php echo $this->lang->line('choosesubjecttutor'); ?></a>
    </li>
    <li class="
        <?php if($sideactive=="profiletutor"){echo "active";}else{
        } ?>"><a href="<?php echo base_url(); ?>tutor/about"><i class="zmdi zmdi-account-circle"></i> <?php echo $this->lang->line('profiltutor'); ?></a>
    </li>                   
    
</ul>