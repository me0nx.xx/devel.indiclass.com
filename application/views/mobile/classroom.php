<header id="header-2" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('classroom/navbar'); ?>
</header>

<section id="main">

	<aside id="chat" class="sidebar c-overflow" style="margin-top: 4.2%;">
        
		<div class="chat-search">
            <div class="fg-line">
                <input type="text" class="form-control" placeholder="Type your message">
                <button class="btn btn-info btn-block m-t-10">Kirim</button>
            </div>
        </div>
        <hr>

        <div class="listview">
            <a class="lv-item" href="">
                <div class="media">
                    <div class="pull-left p-relative">
                        <img class="lv-img-sm" src="<?php echo base_url(); ?>aset/img/user/32.jpg" alt="">
                        <i class="chat-status-busy"></i>
                    </div>
                    <div class="media-body">
                        <div class="lv-title">Testing</div>
                        <small class="lv-small">blablabla</small>
                    </div>
                </div>
            </a>
        </div>
        
    </aside>

    <section id="content"  id="chat-trigger" data-trigger="#chat">
        <div class="container">  

       	</div>
    </section>
</section>