<script type="text/javascript">
	$(document).ready(function(){
  $("#menu").accordion({collapsible: false, active: false, animate: 500,});
});
</script>
<style type="text/css">
body{
	background-color: white;
}
#buttonunfollow{
	font-size: 10px;
}

#name_tutor{
	margin-left: 10%;
}
@media screen and (max-width: 320px){
#buttonunfollow{
	width: 100px;
}
#buttonunfollow{
	font-size: 10px;
}
  #name_tutor{
  	margin-left: 30%;
  }
}
@media screen and (max-width: 360px){
    #buttonunfollow{
    font-size: 9.5px;
  }

}
    .bp-header:hover
    {
        cursor: pointer;
    }
    .ci-avatar img {
        opacity: 1;     
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .ci-avatar:hover img {
        opacity: 0.6;
    }
</style>
<div class="modal fade fade" style="" id="modal_loading" tabindex="-1" data-backdrop="static" role="dialog">
    <div class="modal-dialog" role="document" style="background:rgba(0,00,00,0.0);">   
        <div style="margin-top: 50%;">
            <center>
               <div class="preloader pl-xxl pls-teal">
                    <svg class="pl-circular" viewBox="25 25 50 50">
                        <circle class="plc-path" cx="50" cy="50" r="20" />
                    </svg>
                </div><br>
               <p style="color: white;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
            </center>
        </div>
    </div>
</div>
<header id="header" class="clearfix" data-current-skin="blue" style="background-color: #ea2127;;">
    <div class="" style="padding-left: 5%; padding-bottom: 1%;">
        <?php $this->load->view('mobile/inc/navbar'); ?>    
    </div>

    <div class="header-inner" style="color: white; padding-top: 0; margin-top: 0; padding-bottom: 0 " align="center">
        <button type="button" class="btn-link" style="width: 65px;"><a style="color: white;" href="<?php echo base_url(); ?>">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_home_white.png" style="width: 20px; margin-top:-5%;">
            <!-- <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Account White.svg" style="width: 20px;  height: auto;"> -->
            <br><label style="font-size: 9px; text-transform: uppercase; margin: 0"><?php echo $this->lang->line('home'); ?></label></a>
        </button>
        <button type="button" class="btn-link" style="width: 65px;"><a style="color: white;" href="<?php echo base_url();?>/MyClass">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/myclassputih-02.png" style="width: 20px; margin-top: -5%;">
            <br><label style="font-size: 9px; text-transform: uppercase; " ><?php echo $this->lang->line('myclass'); ?></label></a>
        </button>
        <button type="button" class="btn-link" style="width: 65px;"><a style="color: white;" href="#">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Tutor Orange.svg" style="width: 20px;  margin-top: -5%;  height: auto;">
            <br><label style="font-size: 9px; text-transform: uppercase;">TUTOR</label></a>
        </button>
        <button type="button" class="btn-link" style="width: 65px;"><a style="color: white;" href="#">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_quiz_white.png" style="width: 20px;  margin-top: -5%; height: auto;">
            <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('quiz');?>Quiz</label></a>
        </button>
        <button type="button" class="btn-link" style="width: 65px;"><a style="color: white;" href="<?php echo base_url(); ?>About">
            <i style="font-size: 20px;" class="zmdi zmdi-menu zmdi-hc-fw"></i>
            <!-- <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Account White.svg" style="width: 20px;  height: auto;"> -->
            <br><label style="font-size: 9px; text-transform: uppercase;">Others<?php echo $this->lang->line('tab_other');?></label></a>
        </button>                          
    </div>
</header>
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); 
            $id_user = $this->session->userdata('id_user');
        ?>
    </aside>
    <section id="content">

	<div class="card-body card-padding"><br><br>
	    <div role="tabpanel" class="">
	        <ul class="tab-nav text-center" role="tablist">
	            <li class="col-xs-6 active"><a href="#list_tutor" aria-controls="list_tutor" role="tab" data-toggle="tab">Tutor</a></li>
	            <li class="col-xs-6"><a href="#list_subject" aria-controls="list_subject" role="tab" data-toggle="tab"><?php echo $this->lang->line('title_pg_subject'); ?></a></li>
	        </ul>
	      
	        <div class="tab-content">

	            <section role="tabpanel" class="bgm-blue tab-pane active" id="list_tutor">

	            </section>
	           <section role="tabpanel" class="tab-pane" id="list_subject">
	            	<div class="">
			            <?php if($this->session->flashdata('mes_alert')){ ?>
			            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
			                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			                <?php echo $this->session->flashdata('mes_message'); ?>
			            </div>
			            <?php } ?>

			            <div class="block-header">
			                <h2><?php echo $this->lang->line('title_pg_subject'); ?></h2>
			            </div>
			            
			            <?php            
			            if(empty($allsub))
			            {
			                echo '<div class="alert alert-info">'.$this->lang->line('no_subjects').'</div>';
			            }
			            else
			            {
			                foreach ($allsub as $row => $v) {
			                	
			                    $alltutor = $this->db->query("SELECT ts.usertype_id, ts.user_name, ts.first_name, ts.user_image, tb.* FROM tbl_booking as tb INNER JOIN tbl_user as ts ON tb.id_user=ts.id_user WHERE ts.usertype_id='tutor' AND tb.subject_id='$v[subject_id]'")->result_array();
			                    if (empty($alltutor)) {
			                    	?>
			                    	<div class="card">
				                        <div class="lv-header-alt clearfix m-b-5">
				                            <h2 class="lvh-label f-16"><?php echo $v['subject_name'];?></h2>	                           
				                            <div class="lvh-search">
				                                <input type="text" placeholder="Search typing..." class="lvhs-input">
				                                
				                                <i class="lvh-search-close">&times;</i>
				                            </div>
				                        </div>                        
				                        
				                        <div class="card-body card-padding">
				                        	<center>
			                    				<div class=""><?php echo $this->lang->line('no_tutor_here').$v['subject_name']; ?></div>
			                    			</center>
			                    		</div>
			                    	</div>
			                    	<?php
			                    }
			                    else
			                    {
			                    	?>
			                    	<?php
			                    		$subject_id = $v['subject_id'];
			                    		// echo $subject_id;
			                    	?>
			                    	<div class="panel-group" role="tablist" aria-multiselectable="true">

									    <div class="panel ">
									    	<a data-toggle="collapse" data-parent="" href="#collapse_<?php echo $subject_id; ?>" aria-expanded="" aria-controls="collapse_<?php echo $subject_id; ?>">
										        <div class="panel-heading m-b-5" role="tab" id="heading_<?php echo $subject_id; ?>" >
								                     <div id="" class=" lv-header-alt clearfix m-b-5">
								                     	
								                            <h2 class=" lvh-label f-16">
								                            <img class="m-r-5" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL. 'class_icon/'. $v['icon'];?>" style="height: 25px; width: 25px;">
								                            <?php 
								                            $bahasa =  $this->session->userdata('lang');
								                            if ($bahasa=="english") {echo $v['english'];}else{echo $v['indonesia'];}?></h2>
								                        
								                    </div>
										        </div>
									        </a>
									        <div id="collapse_<?php echo $subject_id; ?>" class="collapse" role="tabpanel" aria-labelledby="heading_<?php echo $subject_id; ?>">
									            <div class="panel-body">
									                <div class=" card-body card-padding m-b-20">
				                            
							                            <div class="contacts clearfix row">
							                            <?php
							                                foreach ($alltutor as $row_tutor => $va) 
							                                {
							                                    $tutor_id = $va['id_user'];
							                                ?>
							                                <div class="col-sm-6 col-xs-12" style="padding: 2px 2px 2px 4px; border-width: 1px; border-bottom-width:1px;border-bottom-color:gray; border-bottom-style: solid; ">
							                                    <?php
							                                        if ($this->session->userdata('status')==0) 
							                                        {
							                                            ?>
							                                            <div class="c-item">
							                                                <a href="#" class="ci-avatar showModal" id="card_<?php echo $tutor_id; ?>">
							                                                    <img class="" onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';"> src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$va['user_image']; ?>" style="width: 10;" alt="">
							                                                </a>
							                        
							                                                <div class="c-info">
							                                                    <strong><?php echo($va['first_name']); ?></strong>
							                                                   
							                                                </div>
							                        
							                                               
							                                            </div>
							                                        <?php
							                                        }
							                                        else
							                                        {
							                                        ?>
							                                            <div style=" " class="col-xs-2">
							                                                <a href="#" class="ci-avatar showModal" id="card_<?php echo $tutor_id; ?>">
							                                                    <img onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" class="img-circle" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$va['user_image']; ?>" style="margin-left: -20%; width: 60px; height: 60px; size: 10%; " alt=""><br>
							                                                </a>
							                                            </div>
							                                            <div class="col-xs-5">
							                                            	<label id="name_tutor" style=" vertical-align: middle; line-height: 30px; height: 30px;" class=" m-t-10"><?php echo($va['first_name']); ?></label>
							                                            </div>
							                                            <div style="" class="col-xs-5">
							                                                <?php
							                                                    $mybooking = $this->db->query("SELECT *FROM tbl_booking WHERE id_user='$id_user'")->result_array();
							                                                    $flag = 0;
							                                                    
							                                                    foreach ($mybooking as $rowbook => $valbook) {
							                                                        if ($tutor_id==$valbook['tutor_id'] && $va['subject_id']==$valbook['subject_id']) {
							                                                            $flag = 1;
							                                                        }
							                                                    }

							                                                    if ($flag==1) {
							                                                    ?>
							                                                            
							                                                            <button style="text-align: center; vertical-align: middle; margin-top: 15px;" id="buttonunfollow" subject_id='<?php echo $va['subject_id'];?>' tutorid='<?php echo $va['id_user'];?>' subject_name='<?php echo $v['subject_name'];?>' class=" waves-effect bgm-red c-white btn btn-danger btn-block unfollow_ajax"><?php echo $this->lang->line('unfollow'); ?></button>
							                                                    <?php
							                                                    }
							                                                    else
							                                                    {
							                                                    ?>
							                                                           
							                                                            <button style="text-align: center; vertical-align: middle; margin-top: 15px;" id="buttonfollow" subject_id='<?php echo $va['subject_id'];?>' tutorid='<?php echo $va['id_user'];?>' subject_namee='<?php echo $v['subject_name'];?>' class="waves-effect bgm-green c-white btn btn-success btn-block follow_ajax f-12"><?php echo $this->lang->line('follow'); ?></button>
							                                                    <?php
							                                                    }
							                                                ?>
							                                            </div>
							                                        <?php
							                                        }
							                                        ?>
							                                </div>
							                                <?php 
							                                }
							                            ?>                                
							                            </div>
				                
				                           
				                        </div>
									            </div>
									        </div>
									    </div>
									</div>
				                    
				                    <?php
			                    }
			                }
			            }
			            ?>
			        </div>   
	            </section>
	        </div>
	    </div>
	</div>
			<div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="profil_modal" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            
                        </div>                                                
                        <div class="modal-footer">
                            
                        </div>                        
                    </div>
                </div>
            </div>
  
        <!-- Modal Default -->  
        <div class="modal" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style=" background: #f7f7f7;">
                    <div class="modal-header bgm-teal">                                                     
                        <div class="pull-left">
                            <h3 class="modal-title c-white"><?php echo $this->lang->line('profil'); ?> Tutor</h3>
                            
                        </div>
                        <div class="pull-right">                                        
                            <button type="button" class="btn bgm-white" data-dismiss="modal">X</button>
                        </div>
                    </div>

                    <div class="modal-body" style="margin-top:30px;">
                            <div  class="col-xs-5">
                                <img class="img-circle z-depth-3-bottom" id="modal_image" width="100px" height="100px" alt="">
                            </div>
                            <div class="col-xs-7">
                                    <p style="font-size:14px; margin-top: 5px; overflow:hidden; word-wrap: break-word;" id="modal_user_name"></p>
                                    <label style="font-size:14px; margin-top: -5px;"  id="modal_gender"></label>
                            </div>

                            <div class="col-xs-12" style="margin-top:0px;">                                              
                                    <div class="card" style="padding: 10px; margin-top: 10px;">
                                        <label style="font-size:16px;"><?php echo $this->lang->line('description_tutor');?></label>
                                        <hr style="margin-top: -1px;">
                                        <label id="modal_self_desc" style="width:100%; text-align:justify;">
                                        </label><br><br>
                                    </div>
                                <div class="col-xs-12">
                                    <div class="col-xs-1" style="margin-left:-20px;">
                                        <img src="<?php echo ('https://classmiles.com/aset/img/icons/topi.png').'?'.time() ?>" width="30px" height="30px" alt="">
                                    </div>
                                    <div id="modal_eduback" class="col-sm-5 m-l-15 m-t-5">                                                 
                                    </div>
                                    <!-- <div class="col-sm-1" style="margin-left:-30px;">
                                        <img src="<?php echo base_url('aset/img/icons/waktu.png').'?'.time() ?>" width="27px" height="27px" alt="">
                                    </div>
                                    <div id="modal_teachexp" class="col-sm-6 m-t-5">
                                    </div> -->
                                </div>                                                                                  
								</div>
                                <div class="col-xs-12 m-t-10 table-responsive">     
                                    <center>
                                        <table style="margin-left: -12px; height:10px;" class="table">
                                            <thead>
                                                <center>    
                                                    <th class="bgm-teal c-white"><?php echo $this->lang->line('competency');?></th>                                                           
                                                </center>
                                            </thead>
                                            <tbody id="modal_competency" style="height: 10px;">                                                 
                                            </tbody>
                                        </table>
                                    </center>
                                </div>

                            
                    </div>

                    <div class="modal-footer m-r-30" >
                    </div>                  
                    <br><br><br>

                </div><!-- AKHIR modal-content -->
            </div><!-- AKHIR modal-dialog -->
        </div>
        <!-- AKHIR modal fade -->

    </section>
</section>
                      

<script type="text/javascript">
	var access_token_jwt = "<?php echo $this->session->userdata('access_token_jwt')?>";
	var iduser 	= "<?php echo $this->session->userdata('id_user');?>";
	$.ajax({
		url: '<?php echo base_url();?>Rest/allSubject_bytutor/access_token/'+access_token_jwt,
		type: 'GET',
		data: {
			id_user: iduser
			
		},
		success: function(response)
		{ 
			// var dataa = JSON.parse(response);
			console.warn("test class "+response['status']);
			for (var i = 0; i< response.data.length;i++) {
                var idtutor = response['data'][i]['tutor_id'];
                var nametutor = response['data'][i]['tutor_name'];
                var imgtutor = response['data'][i]['tutor_image'];
                var countrytutor = response['data'][i]['tutor_country'];
                var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";

                var list_pelajaran ="<a href='#' class='ci-avatar showModal' id='card_'"+idtutor+"'><section class='col-xs-3 bgm-white' style=' height:104px; '><center><a href='#' class='ci-avatar showModal' id='card_'"+idtutor+"'><img id='gambar' class=' img-circle' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+imgtutor+"' onerror="+alt_img+" style='margin-top:10px; width: 60px; height: 60px;'><br><label style='color: gray; font-size:12px; '>"+nametutor+"</label><br></center></section></a>";

                $("#list_tutor").append(list_pelajaran);
                
            }
			// alert('hh');
			// if(data['code'] == 1){
			// 	notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut',data['message']);
			// 	that.attr('class','waves-effect bgm-green c-white btn btn-success btn-block follow_ajax f-12');
			// 	that.html('<?php echo $this->lang->line('follow'); ?>');				
			// }else{
			// 	notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
			// }
		}
  	});  
    
    $(document).on('click', ".unfollow_ajax", function(e){
		e.preventDefault();
    // $(".unfollow_ajax").click(function(){    	
    	var id_user 	= "<?php echo $this->session->userdata('id_user');?>";
    	var subject_id 	= $(this).attr('subject_id');
    	var tutor_id 	= $(this).attr('tutorid');
    	var subject_name= $(this).attr('subject_name');
    	var that 		= $(this);
    	$.ajax({
			url: '<?php echo base_url();?>Master/unsavebooking',
			type: 'GET',
			data: {
				id_user: id_user,
				tutor_id: tutor_id,
				subject_id: subject_id,
				subject_name: subject_name
			},
			success: function(data)
			{ 
				data = JSON.parse(data);
				if(data['code'] == 1){
					notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-green c-white btn btn-success btn-block follow_ajax f-12');
					that.html('<?php echo $this->lang->line('follow'); ?>');				
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
      	});  


    });

    $(document).on('click', ".follow_ajax", function(e){
		e.preventDefault();
    // $(".follow_ajax").click(function(){
    	var id_user 	= "<?php echo $this->session->userdata('id_user');?>";
    	var subject_id 	= $(this).attr('subject_id');
    	var tutor_id 	= $(this).attr('tutorid');
    	var subject_namee= $(this).attr('subject_namee');
    	var that 		= $(this);
    	$.ajax({
			url: '<?php echo base_url();?>Master/savebooking',
			type: 'GET',
			data: {
				id_user: id_user,
				tutor_id: tutor_id,
				subject_id: subject_id,
				subject_namee: subject_namee
			},
			success: function(data)
			{ 		
				data = JSON.parse(data);		
				if(data['code'] == 1){
					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-red c-white btn btn-danger btn-block unfollow_ajax f-12');
					that.html('<?php echo $this->lang->line('unfollow'); ?>');								
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
      	}); 
    });


    // $('.judul_subject').click(function()){
    // 	alert('a');
    // });
    $('.showModal.ci-avatar').click(function(){
    	// alert('a');
        var ids = $(this).attr('id');
        ids = ids.substr(5,10);
        $.get('<?php echo base_url(); ?>master/onah?id_user='+ids,function(hasil){
            hasil = JSON.parse(hasil);
            $('.modal_approve').attr('id',ids);
            $('.modal_decline').attr('id',ids);
            $('#modal_image').attr('src','<?php echo "https://classmiles.com/"; ?>aset/img/user/'+hasil['user_image']+'?'+new Date().getTime());            
            if (hasil['self_description'] == '') {
                $('#modal_self_desc').html("<?php echo $this->lang->line('nodescription'); ?>");
            }
            else
            {
                $('#modal_self_desc').html(hasil['self_description']);
            }
            $('#modal_user_name').html(hasil['user_name']);
            $('#modal_birthplace').html(hasil['user_birthplace']);
            $('#modal_birthdate').html(hasil['user_birthdate']);
            $('#modal_age').html(hasil['user_age']);
            $('#modal_callnum').html(hasil['user_callnum']);
            // $('#modal_gender').html(hasil['user_gender']);
            // alert(hasil['user_gender']);
            if (hasil['user_gender'] == 'Male') {
                $('#modal_gender').html("<?php echo $this->lang->line('male');?>");
            }
            else if(hasil['user_gender'] == 'Female')
            {
                $('#modal_gender').html("<?php echo $this->lang->line('women');?>");
            }
            $('#modal_religion').html(hasil['user_religion']);
            $('#modal_competency').html(hasil['competency']);
            $('#modal_eduback').html("Pendidikan Terakhir "+hasil['last_education']);
            // $('#modal_teachexp').html("Pengalaman Mengajar "+hasil['year_experience'] +" Tahun" );
            if(hasil['competency'] != ''){
                for (var i = 0; i<hasil['competency'].length; i++) {
                    $('#modal_competency').append('<tr>'+
                        '<td>'+hasil['competency'][i]['subject_name']+' ('+hasil['competency'][i]['jenjang_name']+'  '+hasil['competency'][i]['jenjang_level']+')</td>'+                  
                        // '<td>'+hasil['education_background'][i]['institution_address']+'</td>'+
                        // '<td>'+hasil['education_background'][i]['graduation_year']+'</td>'+
                        '</tr>');
                }
            }
            $('#modalDefault').modal('show');
        });
        $('.modal_approve').click(function(){
            var newids = $(this).attr('id');
            $.get('<?php echo base_url(); ?>first/approveTheTutor/'+newids,function(s){
                location.reload();
            });
        });
        $('.modal_decline').click(function(){
            var newids = $(this).attr('id');
            $.get('<?php echo base_url(); ?>first/declineTheTutor/'+newids,function(s){
                location.reload();
            });
        });

        $("#show1").click(function(){            
            $("#contoh2").css('display','none');
            $("#contoh1").css('display','block');
        });
        $("#show2").click(function(){            
            $("#contoh1").css('display','none');
            $("#contoh2").css('display','block');    
        });

    });
    $(".tabmenu").click(function() {
    // body...
    $("#modal_loading").modal('show');
});

    
</script>
