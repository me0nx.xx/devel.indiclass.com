<style type="text/css">
    body{
        background-color: white;
    }
</style>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <section id="content">          
        <div class="" id="profile-main">
            <div class="pm-body clearfix">                                                                                 
                <div class="card-padding card-body">
                    <div class="col-xs-12 ">  
                        <header id="header" class="clearfix" style="background-color: #008080;">
                            <div class="col-xs-1"  style="margin-top: 7%" >
                                <a href="<?php echo base_url('About'); ?>" id="back_home" type="button" class="loading c-white f-19" style="cursor: pointer;">
                                <i class="zmdi zmdi-arrow-left"></i>
                                </a>
                            </div>
                            <div class=" c-white f-17 col-xs-10" style="margin-top: 7%">
                                <center><?php echo $this->lang->line('settings'); ?></center>
                            </div>

                        </header>
                         <?php
                            $this->load->view('mobile/inc/sideprofile');
                        ?><br><br>
                        <form action="<?php echo base_url('/master/saveEditAccountStudent') ?>" method="post">
                            <input name="id_user" type="hidden" value="<?php if(isset($alluser)){ echo $alluser['id_user']; } ?>">
                            <div class="form-group fg-float col-xs-6 m-t-10">
                                <div class="form-group fg-float" >
                                    <div class="fg-line">
                                        <input type="text" name="first_name" required class="input-sm form-control fg-input" value="<?php if(isset($alluser)){ echo $alluser['first_name']; } ?>">
                                        <label class="fg-label"><?php echo $this->lang->line('first_name'); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group fg-float col-xs-6 m-t-10">
                               <div class="fg-line">
                                    <input type="text" name="last_name" required class="input-sm form-control fg-input" value="<?php if(isset($alluser)){ echo $alluser['last_name']; } ?>">
                                    <label class="fg-label"><?php echo $this->lang->line('last_name'); ?></label>
                                </div>
                            </div>
                            <div class="form-group fg-float col-xs-12">
                                <div class="form-group fg-float">
                                    <div class="fg-line">
                                        <input type="text" name="user_address" required class="form-control" value="<?php if(isset($alluser)){ echo $alluser['user_address']; } ?>" style="cursor:not-allowed;" placeholder="<?php echo $this->lang->line('user_address'); ?>">
                                        <label class="fg-label"><?php echo $this->lang->line('home_address'); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group fg-float col-xs-12">
                                <div class="form-group fg-float">
                                    <div class="fg-line">
                                        <input type="text" name="email" readonly required class="form-control" value="<?php if(isset($alluser)){ echo $alluser['email']; } ?>" style="cursor:not-allowed;" placeholder="<?php echo $this->lang->line('email_address'); ?>">
                                        <label class="fg-label">Email</label>
                                    </div>
                                    <small id="capemail" style="color:red; display: none;"><?php echo $this->lang->line('invalidemail');?></small>
                                </div>
                                <small id="capemail" style="color:red; display: none;"><?php echo $this->lang->line('invalidemail');?></small>
                            </div>
                            <div style="cursor: pointer;" class="form-group col-xs-4 col-sm-4">
                                <select name="tanggal_lahir" id="tanggal_lahir" class="select2 form-control">
                                    <option disabled selected><?php echo $this->lang->line('datebirth'); ?></option>
                                    <?php 
                                    if(isset($alluser)){
                                        $tgl = substr($alluser['user_birthdate'], 8,2);
                                        $bln = substr($alluser['user_birthdate'], 5,2);
                                        $thn = substr($alluser['user_birthdate'], 0,4);
                                    }

                                        for ($date=01; $date <= 31; $date++) {
                                            echo '<option value="'.$date.'" ';
                                            if(isset($alluser) && $tgl == $date){ echo "selected='true'";}
                                            echo '>'.$date.'</option>';
                                        }
                                    ?>  
                                </select>
                            </div>
                            <div style="cursor: pointer;" class="form-group col-xs-4 col-md-4">
                                <select name="bulan_lahir" class="select2 form-control">
                                        <option disabled selected><?php echo $this->lang->line('monthbirth'); ?></option>
                                        <option value="01" <?php if(isset($alluser) && $bln == "01"){ echo "selected='true'";} ?>><?php echo $this->lang->line('jan'); ?></option>
                                        <option value="02" <?php if(isset($alluser) && $bln == "02"){ echo "selected='true'";} ?>><?php echo $this->lang->line('feb'); ?></option>
                                        <option value="03" <?php if(isset($alluser) && $bln == "03"){ echo "selected='true'";} ?>><?php echo $this->lang->line('mar'); ?></option>
                                        <option value="04" <?php if(isset($alluser) && $bln == "04"){ echo "selected='true'";} ?>><?php echo $this->lang->line('apr'); ?></option>
                                        <option value="05" <?php if(isset($alluser) && $bln == "05"){ echo "selected='true'";} ?>><?php echo $this->lang->line('mei'); ?></option>
                                        <option value="06" <?php if(isset($alluser) && $bln == "06"){ echo "selected='true'";} ?>><?php echo $this->lang->line('jun'); ?></option>
                                        <option value="07" <?php if(isset($alluser) && $bln == "07"){ echo "selected='true'";} ?>><?php echo $this->lang->line('jul'); ?></option>
                                        <option value="08" <?php if(isset($alluser) && $bln == "08"){ echo "selected='true'";} ?>><?php echo $this->lang->line('ags'); ?></option>
                                        <option value="09" <?php if(isset($alluser) && $bln == "09"){ echo "selected='true'";} ?>><?php echo $this->lang->line('sep'); ?></option>
                                        <option value="10" <?php if(isset($alluser) && $bln == "10"){ echo "selected='true'";} ?>><?php echo $this->lang->line('okt'); ?></option>
                                        <option value="11" <?php if(isset($alluser) && $bln == "11"){ echo "selected='true'";} ?>><?php echo $this->lang->line('nov'); ?></option>
                                        <option value="12" <?php if(isset($alluser) && $bln == "12"){ echo "selected='true'";} ?>><?php echo $this->lang->line('des'); ?></option>  
                                </select> 
                            </div>
                            <div style="cursor: pointer;" class="form-group col-xs-4 col-md-4">
                                <select name="tahun_lahir" id="tahun_lahir" class="select2 form-control">
                                    <option disabled selected><?php echo $this->lang->line('yearbirth'); ?></option>
                                    <?php 
                                        for ($i=1960; $i <= 2016; $i++) {                                                                            
                                            echo '<option value="'.$i.'" ';
                                            if(isset($alluser) && $thn == $i){ echo "selected='true'";}
                                            echo '>'.$i.'</option>';
                                        } 
                                    ?>
                                </select>
                            </div>
                            <div class="input-group fg-float m-t-10">
                                <div class="col-md-12 m-t-20">
                                    <label><?php echo $this->lang->line('mobile_phone'); ?> :</label>
                                </div>
                                <div class="form-group col-xs-12 col-md-12">
                                    <select required name="kode_area" class="select2 form-control">
                                            
                                            <?php
                                                $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                                                foreach ($allsub as $row => $d) {
                                                    if($d['phonecode'] == $alluser['user_countrycode']){
                                                        echo "<option value='".$d['phonecode']."' selected='true'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                    }
                                                    if ($d['phonecode'] != $alluser['user_countrycode']) {
                                                        echo "<option value='".$d['phonecode']."'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                    }                                                            
                                                }
                                            ?> 
                                            </select>
                                </div>
                                <div class="col-xs-12 col-md-7" >
                                    <div class="fg-line">
                                        <input type="text" id="user_callnum" name="user_callnum" minlength="9" maxlength="13" required class="input-sm form-control fg-input" value="<?php echo $alluser['user_callnum'];?>">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div id="tampilan_sekolah" class="">
                                <blockquote class="m-b-5">
                                    <p><?php echo $this->lang->line('tab_ps'); ?></p>
                                </blockquote> 
                                <div align="Right" class="m-r-30" style="margin-top: -10%">
                                    <a href="<?php echo base_url('Profile-School'); ?>"><img id="edit_sekolah" class="img-circle" style=" width: 20px; height: 20px;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_account_edit_black.png"></a>
                                </div>
                                <?php
                                    $id_user   = $this->session->userdata('id_user'); 
                                    // echo $id_user;
                                    $cek_school_id = $this->db->query("SELECT school_id FROM `tbl_profile_student` where student_id='$id_user'")->row_array()['school_id'];
                                    // echo $cek_school_id;

                                    $get_school_name= $this->db->query("SELECT school_name FROM `master_school` where school_id='$cek_school_id'")->row_array()['school_name'];
                                    $get_provinsi= $this->db->query("SELECT provinsi FROM `master_school` where school_id='$cek_school_id'")->row_array()['provinsi'];
                                    $get_kabupaten= $this->db->query("SELECT kabupaten FROM `master_school` where school_id='$cek_school_id'")->row_array()['kabupaten'];
                                    $get_kecamatan= $this->db->query("SELECT kecamatan FROM `master_school` where school_id='$cek_school_id'")->row_array()['kecamatan'];
                                    $get_kelurahan= $this->db->query("SELECT kelurahan FROM `master_school` where school_id='$cek_school_id'")->row_array()['kelurahan'];
                                     $get_school_address= $this->db->query("SELECT school_address FROM `master_school` where school_id='$cek_school_id'")->row_array()['school_address'];
                                 ?>
                                <div id="editSchool" style="display: none;">
                                        <?php 
                                        
                                        // echo $get_school_address;


                                            $provinsi = "";
                                            $d_kabupaten = "";
                                            $d_kecamatan = "";
                                            $d_kelurahan = "";
                                            $d_schoolname = "";
                                            
                                        ?>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label f-12">Provinsi Sekolah</label>
                                            <div class="col-sm-9">
                                                <div class="fg-line">                                        
                                                    <select id='school_provinsi' class="select2 form-control" name="school_provinsi" data-placeholder="<?php echo $this->lang->line('selectprovinsii'); ?>">                                            
                                                    <?php
                                                        
                                                        $prv = $this->Master_model->school_provinsi($provinsi);                                                
                                                        echo '<option disabled="disabled" selected="" value="0">Provinsi</option>'; 
                                                        foreach ($prv as $row => $v) {                                            
                                                            echo '<option value="'.$v['provinsi'].'">'.$v['provinsi'].'</option>';
                                                        }
                                                    ?>
                                                    </select>
                                                </div>                                           
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label f-12">Kabupaten Sekolah</label>
                                            <div class="col-sm-9">
                                                <div class="fg-line" id="boxkabupaten">                                        
                                                    <select disabled id='school_kabupaten' class="select2 form-control" name="school_kabupaten" data-placeholder="<?php echo $this->lang->line('selectkabupaten'); ?>">
                                                        <option value=""></option>
                                                        <?php
                                                            
                                                            $kbp = $this->Master_model->school_getAllKabupaten($d_kabupaten,$provinsi);
                                                            if($kbp != ''){
                                                                foreach ($kbp as $key => $value) {
                                                                    echo "<option value='".$value['kabupaten']."'>".$value['kabupaten']."</option>";
                                                                }
                                                                // echo "<script>pfka = '".$kbp['kabupaten']."'</script>";
                                                            }

                                                        ?>
                                                    </select>                                            
                                                </div>
                                            </div>                                    
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label f-12">Kecamatan Sekolah</label>
                                            <div class="col-sm-9">
                                                <div class="fg-line">                                       
                                                    <select  disabled id='school_kecamatan' class="select2 form-control" data-placeholder="<?php echo $this->lang->line('selectkecamatann'); ?>" name="id_kecamatan">
                                                        <option value=''></option>
                                                        <?php
                                                            $kcm = $this->Master_model->school_getAllKecamatan($d_kecamatan,$d_kabupaten);
                                                            if($kcm != ''){
                                                                echo "<option value='".$kcm['kecamatan']."' selected='true'>".$kcm['kecamatan']."</option>";
                                                                echo "<script>pfkec = '".$kcm['kecamatan']."'</script>";

                                                            }

                                                        ?>
                                                    </select>
                                                    <small id="cek_kecamatan" style="color: red;"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label f-12">Kelurahan Sekolah</label>
                                            <div class="col-sm-9">
                                                <div class="fg-line">
                                                    <select disabled id='school_kelurahan' required class="select2 form-control" data-placeholder="<?php echo $this->lang->line('selectkelurahann'); ?>" name="id_desa">
                                                        <option value=''></option>
                                                         <?php
                                                                $des = $this->Master_model->school_getAllKelurahan($d_kelurahan,$d_kecamatan);
                                                                if($des != ''){
                                                                    echo "<option value='".$des['kelurahan']."' selected='true'>".$des['kelurahan']."</option>";
                                                                    echo "<script>pfkel = '".$kcm['kelurahan']."'</script>";
                                                                }

                                                            ?>
                                                    </select>
                                                    <small id="cek_keluarahan" style="color: red;"></small>
                                                </div>
                                            </div>
                                        </div>  
                                        <br>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label f-12">Nama Sekolah</label>
                                            <div class="col-sm-9">
                                                <div class="fg-line">
                                                    <select disabled id='school_name' name="school_name" required class="select2 form-control" data-placeholder="<?php echo $this->lang->line('selectkelurahann'); ?>" name="id_desa">
                                                        <option value='<?php echo $get_school_name; ?>'></option>
                                                         <?php
                                                                $name = $this->Master_model->school_getName($d_schoolname,$d_kelurahan);
                                                                if($name != ''){
                                                                    echo "<option value='".$name['school_name']."' selected='true'>".$name['school_name']."</option>";
                                                                    echo "<script>pfname = '".$name['school_name']."'</script>";
                                                                }

                                                            ?>
                                                    </select>
                                                    <small id="cek_schoolname" style="color: red;"></small>
                                                </div>
                                            </div>
                                        </div>  
                                        <br>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label f-12 m-t-10"><?php echo $this->lang->line('school_address'); ?></label>
                                            <div class="col-sm-9">
                                                <div class="fg-line">
                                                    <textarea name="school_address" id="school_address" rows="3" required class="input-sm form-control fg-input" placeholder="<?php echo $this->lang->line('typeschooladdress'); ?>" value="<?php echo $get_school_address; ?>"><?php echo $get_school_address; ?></textarea>
                                                </div>
                                            </div>
                                        </div>      
                                        <br>
                                </div>
                                <div id="displaySchool">
                                    <div class="pmb-block">
                                        <div class="pmbb-body">
                                            <div class="pmbb-view">
                                                <dl class="dl-horizontal">
                                                    <dt><?php echo $this->lang->line('school_name'); ?></dt>
                                                    <dd>
                                                        <?php 
                                                            if ($get_school_name == "") {
                                                                echo $this->lang->line('nousername');
                                                            }
                                                            echo $get_school_name; 
                                                        ?>                                            
                                                    </dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt><?php echo $this->lang->line('school_address'); ?></dt>
                                                    <dd>
                                                        <?php 
                                                            if ($get_school_address == "") {
                                                                echo $this->lang->line('nousername');
                                                            }
                                                            echo $get_school_address; 
                                                        ?>                                            
                                                    </dd>
                                                </dl>
                                                <dl class="dl-horizontal">
                                                    <dt><?php echo $this->lang->line('selectkelurahann'); ?></dt>
                                                    <dd>
                                                        <?php 
                                                            if ($get_school_name == "") {
                                                                echo $this->lang->line('nousername');
                                                            }
                                                            echo $get_kelurahan; 
                                                        ?>                                            
                                                    </dd>
                                                </dl> 
                                                <dl class="dl-horizontal">
                                                    <dt><?php echo $this->lang->line('selectkecamatann'); ?></dt>
                                                    <dd>
                                                        <?php 
                                                            if ($get_school_name == "") {
                                                                echo $this->lang->line('nousername');
                                                            }
                                                            echo $get_kecamatan; 
                                                        ?>                                            
                                                    </dd>
                                                </dl> 
                                                <?php
                                                    if ($get_provinsi != "Luar Negeri") {
                                                        ?>
                                                        <dl class="dl-horizontal">
                                                            <dt><?php echo $this->lang->line('selectprovinsii'); ?></dt>
                                                            <dd>
                                                                <?php 
                                                                    if ($get_school_name == "") {
                                                                        echo $this->lang->line('nousername');
                                                                    }
                                                                    echo $get_provinsi; 
                                                                ?>                                            
                                                            </dd>
                                                        </dl>     
                                                                <?php
                                                            }
                                                ?>                                                       
                                            </div>                            
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <br>
                            <blockquote class="m-b-25">
                                <p><?php echo $this->lang->line('tab_cp'); ?></p>
                            </blockquote>
                            
                            <div class="input-group fg-float col-xs-12">
                                <div class="fg-line">
                                    <input type="password" name="oldpass" class="input-sm form-control fg-input">
                                    <label class="fg-label"><?php echo $this->lang->line('old_pass'); ?></label>
                                </div>                                    
                            </div>
                            <br>
                            <div class="input-group fg-float col-xs-12">
                                
                                <div class="fg-line">
                                    <input type="password" name="newpass" id="newpass" class="input-sm form-control fg-input">
                                    <label class="fg-label"><?php echo $this->lang->line('new_pass'); ?></label>
                                </div>                                    
                            </div>
                            <br>    
                            <div class="input-group fg-float col-xs-12">
                                
                                <div class="fg-line">
                                    <input type="password" name="conpass" id="conpass" class="input-sm form-control fg-input" onChange="checkPasswordMatch();">
                                    <label class="fg-label"><?php echo $this->lang->line('confirm_pass'); ?></label>
                                </div>
                                <small class="help-block" id="divCheckPasswordMatch"></small>                                    
                            </div>
                            <br><br>                                                   
                            <div class="card-padding card-body">    
                                <div class="col-xs-12">
                                    <button type="submit" name="simpanedit" id="simpanedit" class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); ?></button>                                
                                </div>                                                                        
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>            
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<!-- Page Loader -->

<script type="text/javascript">
    function checkPasswordMatch() {
        var password = $("#newpass").val();
        var confirmPassword = $("#conpass").val();

        if (password != confirmPassword)
        {
            $("#divCheckPasswordMatch").html("<p style='color:red'><?php echo $this->lang->line('passwordnotmatch'); ?></p>");   
            $('#simpanedit').attr('disabled','');
        }
        else
        {
            $("#divCheckPasswordMatch").html("<p style='color:green'></p>");
            $('#simpanedit').removeAttr('disabled');
        }       
    }

    $(document).ready(function () {
       $("#conpass").keyup(checkPasswordMatch);
    });
</script>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker2').datetimepicker({
            locale: 'id'
        });

        // $('#school_provinsi').change(function(){
        //     var prov = $(this).val();
        //     var placeprov = "<?php echo $this->lang->line('selectprovinsii'); ?>";
        //     $("#boxkabupaten").empty();
        //     var kotakboxprov = "<select id='school_kabupaten' class='select2 form-control' name='school_kabupaten' data-placeholder='"+placeprov+"'>"+isi+"</select>";
        //     $("#boxkabupaten").append(kotakboxprov);
        // });
        $('select.select2').each(function(){            
            if($(this).attr('id') == 'school_provinsi' || $(this).attr('id') == 'school_kabupaten' || $(this).attr('id') == 'school_kecamatan' || $(this).attr('id') == 'school_kelurahan' || $(this).attr('id') == 'school_name'){
                var a = null;
                if ($(this).attr('id') == 'school_provinsi') { a = "<?php echo $this->lang->line('chooseyourpropinsi'); ?>";} if ($(this).attr('id') == 'school_kecamatan') { a = "<?php echo $this->lang->line('chooseyourkecamatan'); ?>";} if ($(this).attr('id') == 'school_kabupaten') { a = "<?php echo $this->lang->line('chooseyourkabupaten'); ?>";} if ($(this).attr('id') == 'school_kelurahan') { a = "<?php echo $this->lang->line('chooseyourkelurahan'); ?>";}
                $(this).select2({
                    minimumInputLength: 1,
                    placeholder: a,
                    delay: 2000,
                    ajax: {
                        dataType: 'json',
                        type: 'GET',
                        url: '<?php echo base_url(); ?>ajaxer/ajax_indonesia_school/' + $(this).attr('id'),
                        data: function (params) {
                            return {
                                term: params.term,
                                page: params.page || 1,
                                pfp: pfp,
                                pfka: pfka,
                                pfkec: pfkec,
                                pfkel: pfkel,
                                pfname: pfname
                            };
                        },
                        processResults: function(data){
                            return {
                                results: data.results,
                                pagination: {
                                    more: data.more
                                }
                            };
                        }
                    }
                });
            }
            else{
              $(this).select2();
            }
        });

        $('#school_provinsi').change(function(){
            var prov = $(this).val();
            pfp = prov;            
            $('#school_kabupaten.select2').select2("val","");
            $("#school_kabupaten").removeAttr('disabled');
            $("#school_kecamatan").attr('disabled','');
            $("#school_kelurahan").attr('disabled','true');
            $("#school_name").attr('disabled','true');
        });
        $('#school_kabupaten').change(function(){
            var prov = $(this).val();
            pfka = prov;            
            $('#school_kecamatan.select2').select2("val","");
            $("#school_kecamatan").removeAttr('disabled');            
            $("#school_kelurahan").attr('disabled','true');
            $("#school_name").attr('disabled','true');
        });
        $('#school_kecamatan').change(function(){
            var prov = $(this).val();
            pfkec = prov;            
            $('#school_kelurahan.select2').select2("val","");
            $("#school_kelurahan").removeAttr('disabled');            
            $("#school_name").attr('disabled','true');
        });
        $('#school_kelurahan').change(function(){
            var prov = $(this).val();
            pfkel = prov;            
            $('#school_name.select2').select2("val","");
            $("#school_name").removeAttr('disabled');
        });
        $('#school_name').change(function(){
            var prov = $(this).val();            
            $.ajax({
                type: "POST",
                url: '<?php echo base_url(); ?>ajaxer/getSchoolAddress/'+prov,
                success:function(datis){
                    var address = $.trim(datis);
                    $('#school_address').val(address);
                }
            });
        });
    });
</script>