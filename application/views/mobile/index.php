<div id="">
    <div class="page-loader" style="background-color:#008080;">
        <div class="preloader pl-xs pls-white">
            <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20" />
            </svg>
            <p>Class Miles . . .</p>
        </div>
    </div>
</div>

<style type="text/css">
    .hidden {
      display: none;
      opacity: 0;
    }
    ::-webkit-scrollbar {
    display: none;
    }

        html, body {
            max-width: 100%;
            overflow-x: hidden;
            background-color: white;


        }


        hr.style-two {
            margin-top: -2%;
            margin-left: -1%;
            color: black;
            background-color: black;
            height: 1px;
            width: 96%; 
        }

        hr.style-one {
            margin-top: -2%;
            margin-left: 3%;
            color: #ededed;
            background-color: #ededed;
            /*height: 1px;*/
            width: 95%; 
        }

        #kalender{
            text-align: center; 
            /*margin-bottom: 10%;*/
            /*margin-top: -15%;*/
        }
        @media screen and (max-width: 768px){
            #kalender{
            text-align: center; 
            /*margin-bottom: 10%;*/

            }
            
        }
        @media screen and (max-width: 475px){
            #calender_xl{
               display: none;

           }
            #calender_m{
               display: inline;
           }

         }
          @media screen and (min-width: 475px){
            #calender_xl{
               display: inline;
           }
            #calender_m{
               display: none;
           }

         }
        body{
            /*background-color: white;*/
            margin: 0; height: 100%; overflow: hidden;
        }

            /* scroller browser */
        ::-webkit-scrollbar {
            width: 9px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
            -webkit-border-radius: 7px;
            border-radius: 7px;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            -webkit-border-radius: 7px;
            border-radius: 7px;
            background: #a6a5a5;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
        }
        ::-webkit-scrollbar-thumb:window-inactive {
            background: rgba(0,0,0,0.4); 
        }
        </style>
<header id="header" class="clearfix" data-current-skin="blue" style="-webkit-transition: background-color 1s ease-out; -moz-transition: background-color 1s ease-out; -o-transition: -color 1s ease-out; transition: background-color 1s ease-out; background-color: #008080;">
    <div class="" id="header_cm" style="padding-left: 5%; padding-bottom: 1%;">
        <?php $this->load->view('mobile/inc/navbar'); ?>    
    </div>
    
    <div class="header-inner" id="header_menu" style=" transition: all 2s linear; color: white; padding-top: 0; margin-top: 0; padding: 0;" align="center">
        <!-- <div class="col-xs-12" style="background-color: #2196f3;"><a href="#" class=" m-l-10"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>logo/logoclass6.png" alt=""></a></div>     -->
        <button type="button" class="btn-link" style="width: 65px;"><a style="color: orange;" href="#">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_home_orange.png" style="width: 20px;">
            <!-- <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Account White.svg" style="width: 20px;  height: auto;"> -->
            <br><label style="font-size: 9px; text-transform: uppercase; margin: 0"><?php echo $this->lang->line('home'); ?></label></a>
        </button>
        <button type="button" class="btn-link" style="width: 65px;"><a style="color: white;" href="<?php echo base_url();?>MyClass">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/myclassputih-02.png" style="width: 20px;">
            <br><label style="font-size: 9px; text-transform: uppercase; " ><?php echo $this->lang->line('myclass'); ?></label></a>
        </button>
        <button type="button" class="btn-link" style="width: 65px;"><a style="color: white;" href="<?php echo base_url(); ?>Subjects">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Tutor White.svg" style="width: 20px;   height: auto;">
            <br><label style="font-size: 9px; text-transform: uppercase;">TUTOR</label></a>
        </button>
        <button type="button" class="btn-link" style="width: 65px;"><a style="color: white;" href="#">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/ic_quiz_white.png" style="width: 20px;  height: auto;">
            <br><label style="font-size: 9px; text-transform: uppercase;"><?php echo $this->lang->line('quiz');?>Quiz</label></a>
        </button>
        <button type="button" class="btn-link" style="width: 65px;"><a style="color: white;" href="<?php echo base_url(); ?>About">
            <i style="font-size: 20px;" class="zmdi zmdi-menu zmdi-hc-fw"></i>
            <!-- <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Account White.svg" style="width: 20px;  height: auto;"> -->
            <br><label style="font-size: 9px; text-transform: uppercase;">Others<?php echo $this->lang->line('tab_other');?></label></a>
        </button>
    </div>
    <div class="header-inner" id="header_channel" style="display: none; color: white; padding-top: 0; margin-top: 0; padding: 0;" align="left">
        <button type="button" class="btn-link" style="width: 65px;"><a style="opacity: 0.5; color: white;" href="<?php echo base_url();?>MyClass">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/myclassputih-02.png" style="width: 20px;">
            <br><label style="font-size: 9px; text-transform: uppercase; " ><?php echo $this->lang->line('myclass'); ?></label></a>
        </button>
        <button type="button" class="leaveChannel btn-link" style="width: 65px;"><a style="color: white;" href="">
            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>exit-2.png" style="width: 20px;">
            <br><label style="margin-top: 2px; font-size: 9px; text-transform: uppercase; " >Leave</label></a>
        </button>
    </div>
</header>
<div class="card" id="box_descprogram" style="display: none;">
    <button class="btn btn-link btn_back" style="margin-top: 2%; margin-left: 2%; position: absolute;"><i class="zmdi zmdi-arrow-back" style="color: white; font-size: 3vh"></i></button>
    
    <div class=" w-item col-xs-12" style="padding: 0; padding-bottom: 5%">              
    </div>
</div>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section style="margin-left: -1%;" id="main" data-layout="layout-1">
    <button id="btn_extraclass" class="btn btn-sm bgm-orange" style="border-radius: 50px; cursor: pointer; position: fixed; bottom:5%; right:10%; z-index: 5;"><i class="zmdi zmdi-plus m-r-5"></i>  Create New Class</button>
    <img  class="notif_friend" id="btn_notifgroup" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/Class Invitation.png" data-toggle="tooltip" data-placement="right" data-original-title="Class invitation" title="Class invitation" style="display: none; cursor: pointer; position: fixed; bottom:13%; right:10%; width: 40px; height: 40px; z-index: 6;">
    <span class="pulse notif_friend"></span>
    <div class="modal fade fade" style="" id="modal_loading" tabindex="-1" data-backdrop="static" role="dialog">
            <div class="modal-dialog" role="document" style="background:rgba(0,00,00,0.0);">   
                <div style="margin-top: 50%;">
                    <center>
                       <div class="preloader pl-xxl pls-teal">
                            <svg class="pl-circular" viewBox="25 25 50 50">
                                <circle class="plc-path" cx="50" cy="50" r="20" />
                            </svg>
                        </div><br>
                       <p style="color: white;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
                    </center>
                </div>
            </div>
    </div>
    <div id="loading" style="display: none; margin-top: 22%;">
        <center>
          <img src="https://apps.applozic.com/resources/2636/sidebox/css/app/images/mck-loading.gif" style="height: 40%; width: 40%;">
          <p style="margin-left: -18px;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
        </center>
    </div>   
    <div class="" style="" id="viewDashboard">
            <?php
                    $id_user = $this->session->userdata("id_user"); 
                    $cek_channel = $this->db->query("SELECT mcs.*, mc.* FROM master_channel as mc INNER JOIN master_channel_student as mcs ON mcs.channel_id=mc.channel_id where id_user='$id_user' and mc.status='1' ")->result_array();
                    if (empty($cek_channel)) {
                        
                    }
                    else
                    {
                    ?>
                    <div class="go-social" id="listChannel" style="">
                        <div class="card-header">
                            <div class="m-t-10 m-l-20 cwh-year f-18" id="">My Channel</div>
                            <br>
                        </div>
                        <div class="card-body clearfix" style="">
                            <?php
                            $i = 0;
                            foreach ($cek_channel as $key => $value) {
                                ?>
                                <a id="channel_<?php echo $value['channel_id'];?>" logo_channel='<?php echo $value['channel_logo'];?>' id_channel='<?php echo $value['channel_id'];?>' name_channel='<?php echo $value['channel_name'];?>' color_channel='<?php echo $value['channel_color'];?>' class="clickChannel col-xs-3" href="#Channel" align='center' style=" ">
                                    <div style="border-radius: 50px; background-color: <?php echo $value['channel_color']; ;?>">
                                        <center style='padding: 2px;'>
                                            <img src="https://cdn.classmiles.com/usercontent/<?php echo $value['channel_logo'] ;?>" style="background-color: white; width: 50px; margin: 0; height: 50px; padding: 1px;" class="img-responsive img-circle" alt="">
                                        </center>    
                                    </div>
                                    <label class="c-gray" style="text-transform: uppercase;"><?php echo $value['channel_name'];?></label>
                                    
                                </a>
                                <?php
                               /* if ($i == 3) {
                                    ?>
                                    <label style="cursor: pointer;" class="pull-right">Load More ...</label>
                                    <?php
                                }
                                if(++$i > 3) 
                                    break;*/
                            }
                            ?>
                        </div>
                    </div>
                    <hr>
                    <?php    
                    }
                ?>
            

            <div class="modal fade" id="modalShowDescProgram" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="card-body" >
                            <div class="media">
                                <div class="media-body">
                                    <div class="" id="img_poster">
                                        <img class="avatar-img" src="" id="desc_poster" style="width: 100%;" alt="">
                                    </div>
                                    <div class="col-xs-12">
                                        <h4><span id="desc_name"></span></h4>
                                    </div>
                                    <div class="col-xs-12">
                                        <h5 style="color: #009688;">Deskripsi :</h5>
                                        <p class="text-justify" style=" margin: 0;" id="desc_description"></p>
                                    </div>
                                    <div class="col-xs-12" style="display: none;"><h5 style="color: #009688;">Bidang : </h5>
                                        <ul id="list_bidang" class="clist clist-star">
                                        </ul>
                                    </div>
                                    <div class="col-xs-12" style="display: none;" >
                                        <h5 style="color: #009688;">Fasilitas :</h5>
                                        <ul id="list_fasilitas" class="clist clist-star">
                                        </ul>
                                    </div>
                                    <div class="col-xs-12">
                                        <h4 id="desc_price" style="color: #22A7F0;"></h4>
                                    </div>
                                    <div class="col-xs-12">
                                        <h5 style="color: #009688;">Pilih Tipe :</h5>
                                        <div id="pilihtype"></div>
                                    </div>
                                    <div class="col-xs-12" id="pilih_jumlah" style="display: none;">
                                        <h5 style="color: #009688;">Jumlah :</h5>
                                        <div id="pilihjumlah">
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="col-xs-12 col-sm-12" style="margin-top: 10px; margin-bottom: 10px;">
                                        <button href="#" class=" buy_package btn btn-success" ><i class="zmdi zmdi-shopping-cart"></i>  Tambah Ke Keranjang</button>    
                                    </div>
                                </div>                                
                            </div>                          
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12" style="margin-top: 10px;">
                <div class="row">
                    <?php if($this->session->flashdata('mes_alert')){ ?>
                    <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('mes_message'); ?>
                    </div>
                    <?php } ?>
                    

                    <div class="" id="box_package" style="">
                        <div class="card-header">
                            <div class="m-l-20 cwh-year f-18" id="text_judul_program">Program</div>
                            <br>
                        </div>
                        <div class="card-body card-padding">
                                <!-- <div class="col-md-8" style="padding-bottom: 5px;">
                                    <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?>baru/SMAUtuh.png">
                                </div>
                                <div class="col-md-4" style="">
                                    <h3>Paket Cermat UN (SMA)</h3>
                                    <label>Untuk Siswa yang ini sukses Ujian Nasional</label><br>
                                    <label>Rp. 50.000</label><br>
                                    <a href="#" class="btn btn-success">Beli Paket</a>
                                </div> -->
                                
                                <div class="carousel slide" id="myCarousel">

                                    <div class="carousel-inner" id="slide_program" style="display: none;">
                                        <div id="box_slide1">     
                                        </div>                                          
                                    </div><!-- /#myCarousel -->
                                    <div class="carousel-inner" id="info_program" style="display: none;" align="center">
                                        <h4 style="color: #22A7F0;  padding-left: 10px; padding-right: 10px;"><?php echo $this->lang->line('no_program'); ?></h4>
                                    </div>
                                    <hr>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="card-header">
                    <div class="m-l-20 cwh-year f-18">Channels</div>
                    <br>
                </div>
                <div class="col-md-12">
                    <div>
                        <div class="row" id="kotak_listchannel">
                        <?php
                        $get_listchannel_active = $this->db->query("SELECT * FROM master_channel WHERE status = 1")->result_array();
                        foreach ($get_listchannel_active as $key => $value) {
                            $channel_id = $value['channel_id'];
                            $total_program = $this->db->query("SELECT count(*) as total_program FROM master_channel_program WHERE channel_id='$channel_id'")->row_array()['total_program'];
                            $total_group = $this->db->query("SELECT count(*) as total_group FROM master_channel_group WHERE channel_id='$channel_id'")->row_array()['total_group'];
                            $total_student = $this->db->query("SELECT count(*) as total_student FROM master_channel_student WHERE channel_id='$channel_id'")->row_array()['total_student'];
                            $total_tutor = $this->db->query("SELECT count(*) as total_tutor FROM master_channel_tutor WHERE channel_id='$channel_id'")->row_array()['total_tutor'];
                            $total_class = $this->db->query("SELECT count(*) as total_class FROM tbl_class WHERE channel_id='$channel_id'")->row_array()['total_class'];
                            ?>
                            <div class='col-xs-12 '>
                                <div class='showDetail card profile-view z-depth-2' channel_id='<?php echo $channel_id ;?>' style="background-color: white; border-radius: 5px 5px 5px 5px; border-width: 1px;  border-style: solid; border-color:<?php echo $value['channel_color']; ?> ">
                                    <div class="col-xs-3 col-xs-offset-9"  align="center">
                                        <img  border="5" style="border-width: 1px;  border-style: solid; border-color:<?php echo $value['channel_color']; ?>; margin-top: 10px; width: 30px; height: auto;" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL.'flag/flag_'.$value['channel_country'];?>.png">
                                    </div>
                                    <div style="padding: 20px;">
                                        <img style=" width: auto; height: 100px; " class=""  src="<?php echo CDN_URL.USER_IMAGE_CDN_URL;?><?php echo $value['channel_logo']; ?>" >
                                    </div>
                                    <div align="left" style="border-radius: 0px 0px 5px 5px; padding-top: 5px;  padding-bottom: 5px;background-color:<?php echo $value['channel_color']; ?> ">
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <img style="margin:5px; margin-top: 0; margin-bottom: 0; width: 40px; height: 40px; background-color: white;" class="img-circle"  src="<?php echo CDN_URL.USER_IMAGE_CDN_URL;?><?php echo $value['channel_logo']; ?>" >
                                            </div>
                                            <div class="col-xs-7" align="left" style="padding-left: 5px;">
                                                <label style="margin: 0;" class="c-white"><?php echo $value['channel_name']; ?></label><br>
                                                <small class="c-white"><?php echo $value['channel_description']; ?></small>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>                          
                            </div>
                            <?php
                        }
                        ?>    
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div class="m-t-30" id="viewMyChannel" align="center" style="display: none;">
        <div class="container">
            <div class="card-header">
                No Class
            </div>
        </div>
    </div> 
</section>
    <div class="modal fade" id="modal_choose_kids" tabindex="-1" role="dialog" style="" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                
                <div class="modal-header bgm-green">
                    <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                    <h4 class="modal-title c-white"><label><?php echo $this->lang->line('pilihakun'); ?> :</label></h4>
                </div>
                <div class="modal-body m-t-20">
                    <form name="form" action="" method="get">
                      <input type="text" name="get_jenjang_id" id="get_jenjang_id" value="" hidden>
                      <input type="text" name="get_list_id" id="get_list_id" value="" hidden>
                    </form>
                    <label><?php echo $this->lang->line('buyforkid'); ?> :</label>
                    <div style="cursor: pointer;">
                        <?php
                            $iduser = $this->session->userdata('id_user');
                            $id_user_kids = $this->session->userdata('id_user_kids');
                            $jenjang_id_paket =  "2";
                            $list_anak    = $this->db->query("SELECT tpk.kid_id, tpk.jenjang_id, tu.user_name, tu.first_name, tu.last_name, tu.user_image from tbl_profile_kid as tpk INNER JOIN tbl_user as tu where tu.id_user = tpk.kid_id and tpk.parent_id='".$iduser."' and tpk.jenjang_id='".$jenjang_id_paket."'")->result_array();
                                foreach ($list_anak as $row => $v) { 
                                ?>
                                <div class="radio m-b-15">
                                    <label>
                                        <input type="radio" name="list_anak" class="cek_anak" value="<?php echo $v['kid_id'] ;?>">
                                        <i class="input-helper"></i>
                                        <?php echo $v['user_name'] ;?>
                                    </label>
                                </div>
                                <?php 

                                }

                        ?>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button"  data-dismiss="modal" data-toggle="modal" aria-label="Close"  class="btn_kid_buy  btn-req-group-demand btn btn-success btn-block" demand-link-group="">OK</button>                            
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Extra Class -->
    <div class=" modal fade" data-modal-color="white" id="modal_extraclass" tabindex="-1" role="dialog" >
        <div class="modal-sm modal-dialog">
            <div class="modal-content" style="margin-top: 20%;">
                <div class="modal-header" align="center">
                    <button style="position: absolute; left: 20px;" class="btn btn-danger btn-raise" data-dismiss="modal"><i class="zmdi zmdi-close"></i></button>
                    <h3 class="">
                        <label style="text-align-last: center; margin-left: 5%;"><?php echo $this->lang->line('createextraclass');?></label>
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="row bgm-orange" style="border-radius: 5px;" align="center">
                        <div class="col-sm-6 col-md-6 col-xs-6 p-t-10 bgm-white" style="padding: 0; border: 2px solid orange; border-right :1px solid orange;" id="privateclick">
                            <a id="privateclassclick" style="cursor: pointer; text-align: center;" class="bgm-green">
                                <h4 style="" class=""><?php echo $this->lang->line('privateclass'); ?></h4>
                                <div class="bgm-orange" id="kotaknyaprivate" align="center" style="border-radius: 15px; margin: 5px">
                                    <div class="clearfix" >
                                        <div class=""><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>class_icon/Private_revisiw.png" class="img-responsive" style="height: 65px; width: 65px;" alt=""></div>
                                    </div>
                                </div>
                                <div class="col-xs-12" style="color: black">
                                    <p class=""><?php echo $this->lang->line('detailprivate'); ?></p>                                            
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-6 col-xs-6 p-t-10 bgm-white" style="padding: 0; border: 2px solid orange; border-left :1px solid orange;" id="groupclick">
                            <a id="groupclassclick" style="cursor: pointer; text-align: center;" class="bgm-green">
                                <h4 style="" class=""><?php echo $this->lang->line('groupclass'); ?></h4>
                                <div class="bgm-orange" id="kotaknyagroup" align="center" style="border-radius: 15px; margin: 5px">
                                    <div class="clearfix" >
                                        <div class=""><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>class_icon/Group_revisiw.png" class="img-responsive" style="height: 65px; width: 65px;" alt=""></div>
                                    </div>
                                </div>
                                <div class="col-xs-12" style="color: black">
                                    <p class=""><?php echo $this->lang->line('detailgroup'); ?></p>                                            
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-link">Save changes</button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_privateclass" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" align="center">
                    <h4 class="modal-title">
                        <button id="btn_back_private" class="btn bgm-orange" style="position: absolute; display: block;"><i class="zmdi zmdi-close"></i></button>    
                        <button id="btn_back_search_private" class="btn bgm-orange" style="position: absolute; display: block;"><i class="zmdi zmdi-arrow-left"></i></button>
                        Private Class
                </div>
                <div class="modal-body" id="body_privatea" style="overflow-y: auto;">
                        <div id="box_privateclass">
         
                            <div class="row">
                                <!-- <div class="col-md-12"> -->
                                <div class="col-md-12" style="z-index: 5;" id="kotakuntukpencarianprivat">
                                    <div class="card">
                                        <div class="card-header ch-alt">
                                            <h2><?php echo $this->lang->line('titlesearchpriv'); ?></h2>
                                        </div>

                                        <div class="card-body card-padding">
                                        <form>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group form-group p-r-15">
                                                        <span class="input-group-addon"><i class="zmdi zmdi-storage"></i></span>
                                                        <select class="select2 form-control" required id="subject_id" style="width: 100%;">
                                                            <?php
                                                                // $id = $this->session->userdata("id_user");
                                                                // $jenjangid = $this->session->userdata('jenjang_id');
                                                                // $allsub = $this->db->query("SELECT ms.*, mj.* FROM master_subject as ms LEFT JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE ms.jenjang_id='$jenjangid' OR ms.jenjang_id LIKE '".$jenjangid."'")->result_array();
                                                                // echo '<option disabled="disabled" selected="" value="0">'.$this->lang->line("selectsubjecttutor").'</option>'; 
                                                                // foreach ($allsub as $row => $v) {                                            
                                                                //     echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.$v['jenjang_name'].' '.$v['jenjang_level'].'</option>';
                                                                // }
                                                            echo '<option disabled="disabled" selected="" value="0">Pilih Pelajaran</option>'; 
                                                            $jenjangid = $this->session->userdata('jenjang_id_kids');
                                                            if ($jenjangid == "") {
                                                                $jenjangid = $this->session->userdata('jenjang_id');
                                                            }
                                                            $id = $this->session->userdata("id_user");                                              
                                                            $sub = $this->db->query("SELECT * FROM master_subject WHERE jenjang_id LIKE '%\"$jenjangid\"%' OR jenjang_id='$jenjangid'")->result_array();
                                                            foreach ($sub as $row => $v) {

                                                                $jenjangnamelevel = array();
                                                                $jenjangid = json_decode($v['jenjang_id'], TRUE);
                                                                if ($jenjangid != NULL) {
                                                                    if(is_array($jenjangid)){
                                                                        foreach ($jenjangid as $key => $value) {                                    
                                                                            $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();   
                                                                            // $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level']; 
                                                                            $jenjangnamelevel[] = $selectnew['jenjang_name'];
                                                                        }   
                                                                    }else{
                                                                        $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();   
                                                                        // $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];
                                                                        $jenjangnamelevel[] = $selectnew['jenjang_name'];
                                                                    }                                       
                                                                }

                                                                // $allsub = $this->db->query("SELECT tb.*, mj.*, ms.subject_name FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$id' AND tb.tutor_id='0' AND (tb.status='verified' OR tb.status='unverified') order by ms.subject_id ASC")->result_array();                                                
                                                                // foreach ($allsub as $row => $v) {                                            
                                                                    // echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.implode(", ",$jenjangnamelevel).'</option>';
                                                                    echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].'</option>';
                                                                // }
                                                            }
                                                            ?>
                                                        </select>
                                                        <input type="text" name="subjecta" id="subjecton" hidden="true">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="input-group form-group p-r-15">
                                                        <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                                        <div class="dtp-container fg-line m-l-5" >
                                                            <input type="text" class=" form-control" id="dateon" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>"/>
                                                        </div>
                                                    </div>
                                                </div>  

                                                <div class="col-md-12">                                    
                                                    <div class="input-group form-group p-r-15">
                                                        <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                                        <div class="dtp-container fg-line m-l-5" id="tempatwaktuu">
                                                            <input type='text' class='form-control' id='timeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />
                                                        </div>                                            
                                                    </div>
                                                </div>
                                              
                                                <div class="col-md-12">
                                                    <div class="input-group form-group p-r-15">
                                                        <span class="input-group-addon"><i class="zmdi zmdi-time-restore-setting"></i></span>
                                                        <div class="dtp-container fg-line">                                                
                                                            <select required class="select2 form-control" required id="duration"  style="width: 100%;">
                                                                <option disabled selected><?php echo $this->lang->line('searchduration'); ?></option>
                                                                <option value="900">15 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="1800">30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="2700">45 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="3600">1 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="5400">1 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="7200">2 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="9000">2 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="10800">3 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="12600">3 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="14400">4 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="16200">4 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="18000">5 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="19800">5 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="21600">6 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="23400">6 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="25200">7 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="27000">7 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="28800">8 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="30600">8 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="32400">9 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="34200">9 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="36000">10 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="37800">10 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="39600">11 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="41400">11 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="43200">12 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="45000">12 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="46800">13 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="48600">13 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="50400">14 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="52200">14 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="54000">15 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="55800">15 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="57600">16 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="59400">16 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="61200">17 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="63000">17 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="64800">18 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="66600">18 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="68400">19 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="70200">19 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="72000">20 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="73800">20 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="75600">21 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="77400">21 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="79200">22 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="81000">22 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="82800">23 <?php echo $this->lang->line('jam'); ?></option>
                                                                <option value="84600">23 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                                <option value="86400">24 <?php echo $this->lang->line('jam'); ?></option>
                                                            </select>
                                                            <input type="text" name="durationa" id="durationon" hidden="true">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <button id="button_search" class="btn btn-primary" style="margin-left: 10%; width: 80%;"><?php echo $this->lang->line('button_search'); ?></button>
                                                </div>
                                            </div>

                                        </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="col-xs-12" id="kotakhasilpencarianprivat" style="display: none;">                        
                                    <div class="card">
                                        
                                        <div class="card-header ch-alt" ng-show="resulte">
                                            <h2><?php echo $this->lang->line('titleresult'); ?> : {{hasil}} </h2>
                                        </div>
                                        <div class="card-body">
                                        	<div class="row">
                                                <div class="alert alert-info alert-dismissible text-center" role="alert" ng-show="awals">              
                                                    <label><?php echo $this->lang->line('alertfirst'); ?></label>
                                                </div>
                                                <div class="alert alert-danger alert-dismissible text-center" role="alert" ng-show="firste">              
                                                    <label><?php echo $this->lang->line('nodata'); ?></label>
                                                </div>                        
	                                            <div class="col-xs-6 " ng-repeat=" x in datas" ng-show="resulte">
	                                            	<div class="col-xs-12" align="center" style="margin-top: 10px; margin-bottom: 10px; background-color: #dcf1f4;">
	                                                    <!-- <div ng-if="x.status_tutor == 'verified'" class="ribbon"><span>Verified</span></div> -->
	                                                    <img class="img-circle media-object m-t-10 m-b-5" ng-src="{{x.user_image}}" style="height: 80%; width: 80%;" alt="">                                                     
	                                                    <p style="width: 15ch; overflow: hidden;   white-space: nowrap; text-overflow: ellipsis;" class="m-b-5 f-13">{{x.user_name}}</p>
	                                                    <!-- <p class="f-13" style="margin-top: -5%;"><label ng-if="x.exact == '0'">{{x.start_time}}</label>
	                                                    <label >{{times}}</label>                                                        
	                                                    </p> -->
	                                                    <p class="m-b-5 f-13">Rp. {{x.hargaakhir}}</p>
	                                                    <!-- <div class="" id="btn_cari">                          -->
                                                            <button gh="{{x.gh}}" harga="{{x.harga_cm}}" hargaakhir="{{x.hargaakhir}}" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" dates="{{dates}}" hargatutor="{{x.harga_tutor}}" start_time="{{times}}" subject_id="{{x.subject_id}}" tutor_id="{{x.tutor_id}}" class="btn-ask m-b-10 btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>                                       
                                                            <button ng-if="x.exact == '0'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{x.start_time}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block m-b-10"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>
                                                        <!-- </div>                                                                                                 -->
	                                                </div>        
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-link">Save changes</button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_groupclass" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header" align="center">
                    <h4 class="modal-title">
                        <button id="btn_back_group" class="btn bgm-orange" style="position: absolute; display: block;"><i class="zmdi zmdi-close"></i></button>    
                        <button id="btn_back_search_group" class="btn bgm-orange" style="position: absolute; display: none;"><i class="zmdi zmdi-arrow-left"></i></button>
                        Group Class
                </div>
                <div class="modal-body" id='body_group' style="overflow-y: auto;">
                    <div id="box_groupclass">
                        <div class="row">
                            <!-- <div class="col-md-12"> -->
                            <div class="col-xs-12" style="z-index: 5;" id="kotakuntukpencariangroup">
                                <div class="card">
                                    <div class="card-header ch-alt">
                                        <h2><?php echo $this->lang->line('titlesearchgrup');?></h2>
                                    </div>

                                    <div class="card-body card-padding">
                                    <form>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="input-group form-group p-r-15">
                                                    <span class="input-group-addon"><i class="zmdi zmdi-storage"></i></span>
                                                    <select class="select2 form-control" required id="subject_idg" style="width: 100%;">
                                                        <?php
                                                            // $id = $this->session->userdata("id_user");
                                                            // $jenjangid = $this->session->userdata('jenjang_id');
                                                            // $allsub = $this->db->query("SELECT ms.*, mj.* FROM master_subject as ms LEFT JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE ms.jenjang_id='$jenjangid' OR ms.jenjang_id LIKE '".$jenjangid."'")->result_array();
                                                            // echo '<option disabled="disabled" selected="" value="0">'.$this->lang->line("selectsubjecttutor").'</option>'; 
                                                            // foreach ($allsub as $row => $v) {                                            
                                                            //     echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.$v['jenjang_name'].' '.$v['jenjang_level'].'</option>';
                                                            // }
                                                        echo '<option disabled="disabled" selected="" value="0">Pilih Pelajaran</option>'; 
                                                        $jenjangid = $this->session->userdata('jenjang_id_kids');
                                                        if ($jenjangid == "") {
                                                            $jenjangid = $this->session->userdata('jenjang_id');
                                                        }
                                                        $id = $this->session->userdata("id_user");                                              
                                                        $sub = $this->db->query("SELECT * FROM master_subject WHERE jenjang_id LIKE '%\"$jenjangid\"%' OR jenjang_id='$jenjangid'")->result_array();
                                                        foreach ($sub as $row => $v) {

                                                            $jenjangnamelevel = array();
                                                            $jenjangid = json_decode($v['jenjang_id'], TRUE);
                                                            if ($jenjangid != NULL) {
                                                                if(is_array($jenjangid)){
                                                                    foreach ($jenjangid as $key => $value) {                                    
                                                                        $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();   
                                                                        // $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level']; 
                                                                        $jenjangnamelevel[] = $selectnew['jenjang_name'];
                                                                    }   
                                                                }else{
                                                                    $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();   
                                                                    // $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];
                                                                    $jenjangnamelevel[] = $selectnew['jenjang_name'];
                                                                }                                       
                                                            }

                                                            // $allsub = $this->db->query("SELECT tb.*, mj.*, ms.subject_name FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$id' AND tb.tutor_id='0' AND (tb.status='verified' OR tb.status='unverified') order by ms.subject_id ASC")->result_array();                                                
                                                            // foreach ($allsub as $row => $v) {                                            
                                                                // echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.implode(", ",$jenjangnamelevel).'</option>';
                                                            echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].'</option>';
                                                            // }
                                                        }
                                                        ?>
                                                    </select>
                                                    <input type="text" name="subjecta" id="subjectong" hidden="true">
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="input-group form-group p-r-15">
                                                    <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                                    <div class="dtp-container fg-line m-l-5">                                                
                                                        <input type="text" class="form-control" id="dateong" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>"/>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="col-xs-12">                                    
                                                <div class="input-group form-group p-r-15">
                                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                                    <!-- <div class="dtp-container fg-line m-l-5">
                                                        <input type="text" class="form-control" id="timeeee" placeholder="<?php echo $this->lang->line('searchtime'); ?>" />
                                                    </div> -->
                                                    <div class="dtp-container fg-line m-l-5" id="tempatwaktuuu">
                                                        <input type='text' class='form-control' id='timeeeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />
                                                    </div>                                            
                                                </div>
                                            </div>
                                            <div class="col-xs-12" id="durasigroup">
                                                <div class="input-group form-group p-r-15">
                                                    <span class="input-group-addon"><i class="zmdi zmdi-time-restore-setting"></i></span>
                                                    <div class="dtp-container fg-line">                                                
                                                        <select required class="select2 form-control" id="durationg" style="width: 100%;" >
                                                            <option disabled selected><?php echo $this->lang->line('searchduration'); ?></option>
                                                            <option value="900">15 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="1800">30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="2700">45 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="3600">1 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="5400">1 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="7200">2 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="9000">2 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="10800">3 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="12600">3 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="14400">4 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="16200">4 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="18000">5 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="19800">5 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="21600">6 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="23400">6 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="25200">7 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="27000">7 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="28800">8 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="30600">8 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="32400">9 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="34200">9 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="36000">10 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="37800">10 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="39600">11 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="41400">11 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="43200">12 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="45000">12 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="46800">13 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="48600">13 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="50400">14 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="52200">14 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="54000">15 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="55800">15 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="57600">16 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="59400">16 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="61200">17 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="63000">17 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="64800">18 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="66600">18 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="68400">19 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="70200">19 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="72000">20 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="73800">20 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="75600">21 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="77400">21 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="79200">22 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="81000">22 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="82800">23 <?php echo $this->lang->line('jam'); ?></option>
                                                            <option value="84600">23 <?php echo $this->lang->line('jam'); ?> 30 <?php echo $this->lang->line('menit'); ?></option>
                                                            <option value="86400">24 <?php echo $this->lang->line('jam'); ?></option>
                                                        </select>
                                                        <input type="text" name="durationa" id="durationong" hidden="true" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <button id="button_searchgroup" class="btn btn-primary" style="margin-left: 10%; width: 80%;"><?php echo $this->lang->line('button_search'); ?></button>
                                            </div>
                                        </div>

                                    </div>
                                    </form>
                                </div>
                            </div>

                            <div class="col-xs-12" id="kotakhasilpencariangroup" style="display: none;">
                                <div class="card">

                                    <div class="card-header ch-alt" ng-show="resulte">
                                        <h2><?php echo $this->lang->line('titleresult'); ?> : {{ghasil}} </h2>
                                        <div class="f-14" style=""><label>{{subjects}} <br> {{dates}}, {{times}} </label></div>
                                    </div>

                                    <div class="card-body" >
                                        <div class="row">
                                            <div class="alert alert-info alert-dismissible text-center" role="alert" ng-if="awals" >              
                                                <label><?php echo $this->lang->line('alertfirst'); ?></label>
                                            </div>
                                            <div class="alert alert-danger alert-dismissible text-center" role="alert" ng-if="firste">                                
                                                <label><?php echo $this->lang->line('nodata'); ?></label>
                                            </div>
                                            <div class="col-xs-6 " ng-repeat=" x in datasg" ng-show="resulte">
                                                <div class="col-xs-12" align="center" style="margin-top: 10px; margin-bottom: 10px; background-color: #dcf1f4;">
                                                    <!-- <div ng-if="x.status_tutor == 'verified'" class="ribbon"><span>Verified</span></div> -->
                                                    <img class="img-circle media-object m-t-10 m-b-5" ng-src="{{x.user_image}}" style="height: 80%; width: 80%;" alt="">                                                     
                                                    <p style="width: 15ch; overflow: hidden;   white-space: nowrap; text-overflow: ellipsis;" class="m-b-5 f-13">{{x.user_name}}</p>
                                                    <!-- <p class="f-13" style="margin-top: -5%;"><label ng-if="x.exact == '0'">{{x.start_time}}</label>
                                                    <label >{{times}}</label>                                                        
                                                    </p> -->
                                                    <p class="m-b-5 f-13">Rp. {{x.hargaakhir}}</p>
                                                    <a class="m-b-10 openmodaladd {{x.tutor_id}} btn bgm-green btn-icon-text btn-block" gh="{{x.gh}}" harga="{{x.harga_cm}}" hargatutor="{x.harga_tutor}}" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" dates="{{dates}}" start_time="{{times}}" subject_id="{{x.subject_id}}" tutor_id="{{x.tutor_id}}">
                                                        <i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?>     
                                                    </a>                                                                                                      
                                                </div>                                    
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-link">Save changes</button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div> -->
            </div>
        </div>
    </div>
    <div class="modal " id="modalinvitefriends" tabindex="-1" role="dialog" style="top: 13%;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bgm-cyan">
                    <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                    <h4 class="modal-title c-white"><?php echo $this->lang->line('titleinvite');?></h4>
                </div>
                <div class="modal-body m-t-20">
                    <div align="center" id="detailKelas" style="margin-bottom: 20px;">
                        <input type="text" id="id_kelas" name="id_kelas" hidden>
                        <img class="img-rounded" style="height: 80px; width: 80px;" id="imgTutor" src="">
                        <h3 id="namatutor"></h5>
                        <h4 id="subjectkelas"></h4>
                        <h6 id="deskripsiekelas"></h6>
                        <h6 id="waktukelas"></h6>
                    </div>
                    <hr>
                    <form id="invfriend">
                        <div class="col-md-12" style="padding: 0;">
                            <div class="col-md-12">
                                <label class="col-md-12"><?php echo $this->lang->line('findfriends');?></label><br>
                                <div class="col-md-12 m-t-10">                                          
                                    <select required name="tagteman[]" id="tagteman" multiple class="select2 form-control" style="width: 100%;">
                                        <!-- <option selected="true" value="volvo">Volvo</option> -->
                                    </select>           
                                    <label class="c-red" style="display: none;" id="alertemailempty"><?php echo $this->lang->line('emailkosong');?></label>                     
                                    <label class="c-red" style="display: none;" id="alertemailnotfound"><?php echo $this->lang->line('emailnotfound');?></label>
                                    <label class="c-red" style="display: none;" id="alertemailsendiri"><?php echo $this->lang->line('emailown');?></label>
                                </div>
                            </div>
                            <br>
                            <div class="col-md-1"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btnreqgroupdemand btnInvGroup btn btn-success btn-block m-t-15">Tambah Teman</button>                            
                </div>
            </div>
        </div>
    </div>
    <div class="  modal fade" id="modal_invitation" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="bgm-white modal-body" style=""> 
                <div class="card-header m-t-10">
                    <h2 class=""><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/channelgroup.png" style="height: 25px; width: 25px;"> <label class="m-t-20"><?php echo $this->lang->line('classinvitation'); ?></label></h2>
                </div>
                <table class="displayyy table datainvitation" style="size: 80%;">
                    <tbody id="tempatfriendinvitation">                         
                    </tbody>
                </table>
            </div>
        </div>       
    </div>
    <div class=" modal fade" data-modal-color="blue" id="modal_confirm" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                    <center>
                        <div id='' class="modal-body" style="margin-top: 30%;" ><br>
                           <div class="alert alert-info" style="display: block; ?>" id="">
                                <label id="labelclass"></label>
                            </div>
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="buttonok btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                    </div>
                </center>
            </div>
        </div>
    </div>
    <div class=" modal fade" data-modal-color="blue" id="modal_confirm_anak" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                    <center>
                        <div id='' class="modal-body" style="margin-top: 30%;" ><br>
                           <div class="alert alert-info" style="display: block; ?>" id="">
                                <label id="">Kelas tersebut tidak sesuai dengan jenjang Anda</label>
                            </div>
                        </div>
                    <div class="modal-footer">
                        <button id="btn_openmodal" type="button" class="btn btn-link"  data-dismiss="modal"  style="">Ganti User</button>
                        <button type="button" class="buttonok btn btn-link" data-dismiss="modal" data-="modal" aria-label="Close" style="">OK</button>
                    </div>
                </center>
            </div>
        </div>
    </div>
    <div class=" modal fade" data-modal-color="blue" id="modal_list_anak" tabindex="-1" data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                    <center>
                        <div id='' class="modal-body" style="margin-top: 30%; " ><br>
                                <div class="row">
                                    <div class="col-md-12"><label>Silakan pilih User yang akan bergabung dengan kelas ini:</label></div>
                                    <!-- <div class="col-md-3"><label>Username:</label></div> -->
                                    <div class="form-group col-xs-12 col-md-12">
                                        <select  id="nama_anak" required name="nama_anak" class="select2 selectpicker form-control" data-live-search="true"  style="width: 100%;">
                                          <?php                       

                                            $parent_id = $this->session->userdata('id_user');
                                            $parent_jenjang_id = $this->session->userdata('jenjang_id');
                                            $username = $this->session->userdata('user_name');

                                            $listanak = $this->db->query("SELECT tpk.kid_id, tpk.jenjang_id, tu.user_name, tu.first_name, tu.last_name, tu.user_image from tbl_profile_kid as tpk INNER JOIN tbl_user as tu where tu.id_user = tpk.kid_id and tpk.parent_id='$parent_id'")->result_array();
                                            // $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                                            // echo "<option id='".$parent_id."' value='".$parent_jenjang_id."' selected='true'>".$username." (Saya sendiri)</option>";
                                            foreach ($listanak as $row => $d) {
                                                    echo "<option id='".$d['kid_id']."'  value='".$d['jenjang_id']."'>".$d['user_name']."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>                              
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="buttonjoinanak btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                    </div>
                </center>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modaladdfriends" tabindex="-1" role="dialog" style="" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bgm-cyan">
                    <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                    <h4 class="modal-title c-white"><?php echo $this->lang->line('titledemandconfrim');?></h4>
                </div>
                <div class="modal-body " style="overflow-y: auto; height: 425px;"> 
                    <form id="frmdata">
                        <div class="col-xs-12 c-gray m-10">
                            <label><?php echo $this->lang->line('checkdemand'); ?></label>
                        </div>
                        <div class="col-xs-12" style="padding: 0;">
                            <div class="col-xs-12">
                                <input type="text" name="data_subject_id" id="data_subject_id" hidden />
                                <input type="text" name="data_start_time" id="data_start_time" hidden/>
                                <input type="text" name="data_avtime_id" id="data_avtime_id" hidden/>
                                <input type="text" name="data_duration" id="data_duration" hidden/>
                                <input type="text" name="data_metod" id="data_metod" hidden/><br>
                                <!-- <input name='ms' class="col-md-8 m-t-5" style="height: 35px;" id="ms"> -->
                                <div class="col-xs-12">
                                	<div class="m-t-10 m-b-10" align="center">                                                    
                                        <img class="img-rounded imagetutor_private" id="image_group" style="height: 60px; width: 60px;" alt="">
                                    </div>
                                    <div class="media-body c-gray">
                                            <p class="m-b-5 f-13"><?php echo $this->lang->line('demandname'); ?> : <label id="namemodal_group"></label></p>
                                            <p class="m-b-5 f-13" style=""><?php echo $this->lang->line('demandtime'); ?> : <label id="timestart_group"></label></p>
                                            <p class="m-b-5 f-13"><?php echo $this->lang->line('demanddate'); ?> : <label id="tanggalkelas_group"></label></p>
                                            <p class="m-b-5 f-13" style="">
                                                <?php echo $this->lang->line('demandprice'); ?> : <label id="hargakelas_group"></label></p> 
                                            <label class="m-t-10"><?php echo $this->lang->line('topikbelajar');?></label><br>                                               
                                            <textarea rows="4" class="form-control" name="topikgroup" id="topikgroup" style="width: 100%; margin-top: 10px; overflow: hidden;" placeholder="<?php echo $this->lang->line('isitopikanda');?>"></textarea>
                                            <label class="c-red" style="display: none;" id="alerttopikgroup" ><?php echo $this->lang->line('topikkosong');?></label>
                                    </div>                                                          
                                </div>                                                            
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                    </form>
                </div>
                <hr>
                <div class="modal-footer">
                    <button type="button" class="btn-req-group-demand btn btn-success btn-block m-t-15" demand-link-group=""><?php echo $this->lang->line('confrimdemand');?></button>                            
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" style="" id="modalconfrim" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">                          
                    <button type="button" class="close" data-dismiss="modal"><i class="zmdi zmdi-close"></i></button>
                    <!-- <center> -->
                        <h4 class="modal-title c-black f-20"><?php echo $this->lang->line('titledemandconfrim'); ?></h4>
                    <!-- </center> -->
                    <hr>
                    <div id="showalertpayment" style="display: none;">
                        <div class="alert alert-success f-14" role="alert"><?php echo $this->lang->line('successfullydemand');?></div>    
                    </div>
                </div>
                <div class="modal-body c-gray" style="overflow-y: auto; height: 425px;"> 
                    <div id="showdetailtutor">
                        <div class="col-xs-12 c-gray m-10">
                            <label><?php echo $this->lang->line('checkdemand'); ?></label>
                        </div>
                        <div class="col-xs-12">
                        	<div class="m-t-10 m-b-10" align="center">                                                    
                                <img class="img-rounded imagetutor_private" id="image" style="height: 100px; width: 100px;" alt="">
                            </div>
                            <p class="m-b-5 f-13"><?php echo $this->lang->line('demandname'); ?> : <label id="namemodal"></label></p>
                            <p class="m-b-5 f-13" style=""><?php echo $this->lang->line('demandtime'); ?> : <label id="timestart"></label></p>
                            <p class="m-b-5 f-13"><?php echo $this->lang->line('demanddate'); ?> : <label id="tanggalkelas"></label></p>
                            <p class="m-b-5 f-13" style=""><?php echo $this->lang->line('demandprice'); ?> : <label id="hargakelas"></label></p> 
                            <textarea rows="4" class="form-control" name="topikprivate" id="topikprivate" style="width: 100%;" placeholder="Isi topik yang ingin Anda pelajari ..."></textarea>
                            <label class="c-red" style="display: none;" id="alerttopik" >Harap isi topik pelajaran terlebih dahulu !</label>
                        </div>                                                          
                    </div>                                  
                </div> 
                <div class="modal-footer">
                    <!-- <div id="afterchoose" style="display: none;">
                        <button type="button" class="btn-next-demand btn btn-success pull-left"><i class="zmdi zmdi-chevron-left"></i> Lanjut pilih kelas lain</button>
                        <button type="button" class="btn-view-demand btn btn-danger" demand-link=""><?php echo $this->lang->line('confrimpayment');?> <i class="zmdi zmdi-chevron-right"></i></button>  
                    </div> -->
                    <div id="beforechoose">
                        <button type="button" id="getloading" class="btn-req-demand btn btn-success btn-block" demand-link=""><?php echo $this->lang->line('confrimdemand');?></button>
                    </div>
                </div>                                                             
            </div>
        </div>
    </div>
    <!-- End Modal Extra Class -->
<div class="modal fade" id="modaldescChannel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" style="height: 85%;">
        <div class="modal-content" style="height: 100%; min-height: 100%;">
            <div id="kotak_descchannel" class="p-15">
                <div class="row">
                    <div class="col-xs-2">
                    <img id="desc_logo_channel"  class='img-circle' style="background-color: white; width: 40px; height: 40px; ">
                </div>
                <div class="col-xs-8">
                    <label class="m-0 c-white" style="font-size: 20px;" id="desc_channel_name"></label><br>
                    <small class="" style="color: #fefefedb" id="desc_channel_description"></small>
                </div>
                <div class="col-xs-2 pull-right" style="padding-left: 10px;">
                    <img id="desc_channel_country"  border="5" style="margin-top: 10px; width: 30px; height: auto;">
                </div>
                </div>
            </div>
            <div class="profile-view" style="height: 75%; min-height: 75%; overflow-y: auto;">                
                <div class="pv-body" style="margin-top: 5px;">
                    <button id="btn_desc_join" style="color: white;" class="btn btn-lg btn-block m-t-10 m-b-5"></button>
                    <ul class='pv-follow'>
                        <li id="desc_total_student"></li>
                        <li id='desc_total_class'></li>
                        <li id='desc_total_tutor'></li>
                    </ul>
                </div>
                <div class="container" >
                    <div class="" id="listprogram" style="display: none;">
                        <h4 align="left">Program</h4>
                    </div>
                </div>
            </div>
            <div class="container" align="center" style="padding: 5px; margin-top: 5px;">
                <div class="card-padding">
                    <button data-dismiss="modal" data-toggle="modal" aria-label="Close" class="btn bgm-cyan"><i class="zmdi zmdi-long-arrow-up"></i></button>
                </div>
            </div>
        </div>
</div>


<script type="text/javascript">
    $(document.body).on('click', '.showDetail' ,function(e){
        var channel_id = $(this).attr('channel_id');
        var id_user = "<?php echo $this->session->userdata('id_user');?>";
        $.ajax({
                url: '<?php echo BASE_URL();?>Rest/channel_descChannel',
                type: 'POST',
                data: {
                    channel_id : channel_id,
                    id_user : id_user
                },
                success: function(response)
                {
                    var a = JSON.stringify(response);
                    // console.log(a);
                    $("#modaldescChannel").modal('show');
                    var channel_about = response['data']['channel_about'];
                    var channel_id = response['data']['channel_id'];
                    var channel_address = response['data']['channel_address'];
                    var channel_banner = response['data']['channel_banner'];
                    var channel_callnum = response['data']['channel_callnum'];
                    var channel_city = response['data']['channel_city'];
                    var channel_color = response['data']['channel_color'];
                    var channel_country = response['data']['channel_country'];
                    var channel_country_code = response['data']['channel_country_code'];
                    var channel_description = response['data']['channel_description'];
                    var channel_email = response['data']['channel_email'];
                    var channel_email = response['data']['channel_email'];
                    var channel_fitur = response['data']['channel_fitur'];
                    var channel_iduser = response['data']['channel_iduser'];
                    var channel_link = response['data']['channel_link'];
                    var channel_logo = response['data']['channel_logo'];
                    var channel_name = response['data']['channel_name'];
                    var membership_type = response['data']['membership_type'];
                    var total_class = response['total_class'];
                    var total_group = response['total_group'];
                    var total_program = response['total_program'];
                    var total_student = response['total_student'];
                    var total_tutor = response['total_tutor'];
                    var status_join = response['status_join'];
                    if (status_join == 1) {
                        $("#btn_desc_join").text('Membership');
                    }
                    else{
                        $("#btn_desc_join").text('Join');   
                        $("#btn_desc_join").click(function(){
                            $.ajax({
                                url: '<?php echo BASE_URL();?>Rest/channel_joinChannel',
                                type: 'POST',
                                data: {
                                    id_user : id_user,
                                    channel_id : channel_id
                                },
                                success: function(response)
                                {
                                    var code = response['code'];
                                    if (code == 200) {
                                        swal("Succes join this channel", "", "success")

                                        // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Success Join to Channel');
                                        setTimeout(function(){
                                            location.reload();
                                        },3000);
                                    }
                                    else
                                    {
                                        swal("Failed join this channel", "", "failled")
                                        // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','Failed, please try again!!!');
                                        setTimeout(function(){
                                            location.reload();
                                        },3000);
                                    }
                                }
                            });
                        });
                    }
                    var data_program = response['data_program'];
                    if (data_program == "") {
                        $("#listprogram").css('display','none');
                    }
                    else{
                        $("#listprogram").css('display','block');   
                    }
                    for (var i = 0; i < data_program.length; i++) {
                        var bidang = data_program[i]['bidang'];
                        var description = data_program[i]['description'];
                        var fasilitas = data_program[i]['fasilitas'];
                        var group_id = data_program[i]['group_id'];
                        var jenjang_id = data_program[i]['jenjang_id'];
                        var list_id = data_program[i]['list_id'];
                        var name = data_program[i]['name'];
                        var poster = data_program[i]['poster'];
                        var poster_mobile = data_program[i]['poster_mobile'];
                        var price = data_program[i]['price'];
                        var program_id = data_program[i]['program_id'];
                        var type = data_program[i]['type'];
                        var status = data_program[i]['status'];
                        var img_program = "<img class='z-depth-1' style='margin-bottom: 7px; margin-top: 7px; border-radius: 10px; width: 100%; height: auto;' src='"+poster_mobile+"' />";
                        $("#listprogram").append(img_program);
                    }
                    $("#desc_logo_channel").attr('src','<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>'+channel_logo);
                    $("#desc_channel_country").attr('src','<?php echo CDN_URL.STATIC_IMAGE_CDN_URL.'flag/flag_'?>'+channel_country+'.png');
                    $("#kotak_descchannel").css('background-color', channel_color);
                    $("#btn_desc_join").css('background-color', channel_color);
                    $("#desc_channel_name").text(channel_name);
                    $("#desc_total_tutor").text(total_tutor+" Tutors");
                    $("#desc_total_class").text(total_class+" Classes");
                    $("#desc_total_student").text(total_student+" Students");
                    $("#desc_channel_description").text(channel_description);
                }
            });


    });
    // Carousel Auto-Cycle
    $(document).ready(function() {
        var colorChannel = null;
        var logoChannel = null;
        var idChannel = null;
        var nameChannel = null;
        var linklogo = null;
        var pathname = window.location.pathname;
        var iduser = "<?php echo $this->session->userdata('id_user_kids'); ?>";
        if (iduser == null || iduser=="") {
            iduser  = "<?php echo $this->session->userdata('id_user'); ?>";
        }

        // Click Channel
        $(".clickChannel").click(function(){
            colorChannel = $(this).attr('color_channel');
            nameChannel = $(this).attr('name_channel');
            logoChannel = $(this).attr('logo_channel');
            idChannel = $(this).attr('id_channel');
            linklogo = "<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+logoChannel;
            $("#header").css('background-color',colorChannel);
            $("#header_menu").css('display','none');
            $("#header_channel").css('display','block');
            $("meta[name='theme-color']").attr("content", colorChannel);
            $("#viewDashboard").css('display','none');
            $("#viewMyChannel").css('display','block');
            $("#btn_extraclass").css('display','none');
            $("#purchase").css('display','none');
            $("#headerClassmiles").css('display','none');
            $("#headerChannel").css('display','block');
            $("#logochannel").attr('src',linklogo);
            $("#namechannel").text(nameChannel);
        
        });

        /*Handle tombol back*/
        window.onpopstate = function() {
        $("#header").css('background-color',"#008080");
        $("#header_menu").css('display','block');
        $("#header_channel").css('display','none');
        $("meta[name='theme-color']").attr("content", "#008080");
        $("#viewDashboard").css('display','block');
        $("#viewMyChannel").css('display','none');
        $("#btn_extraclass").css('display','block');
        $("#purchase").css('display','block');
        $("#headerClassmiles").css('display','block');
            $("#headerChannel").css('display','none');
        }; history.pushState({}, '');

        // Leave Channel
        $(".leaveChannel").click(function(){
            swal({   
                title: "Apakah kamu yakin?",   
                text: "Kamu tidak akan mengikuti kelas yang ada pada channel ini lagi",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Ya",   
                closeOnConfirm: false 
            }, function(){   
                $.ajax({
                    url: '<?php echo BASE_URL();?>Rest/channel_leavechannel',
                    type: 'POST',
                    data: {
                        id_user : iduser,
                        channel_id : idChannel
                    },
                    success: function(response)
                    {
                        var code = response['code'];
                        if (code == 200) {
                            swal("Succes leave this channel", "", "success")

                            // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Success Join to Channel');
                            setTimeout(function(){
                                location.reload();
                            },1000);
                        }
                        else
                        {
                            swal("Please try again", "", "failled")
                            // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','Failed, please try again!!!');
                            setTimeout(function(){
                                location.reload();
                            },1000);
                        }
                    }
                });
            });
        });

        /*===============================================*/

        $("#list_bidang").empty();
        $("#list_fasilitas").empty();
        // $('.carousel').carousel({
        //      interval: 6000
        // });
        var username_kids = "<?php echo $this->session->userdata('username_kids');?>";
        var user_name = "<?php echo $this->session->userdata('user_name');?>";
        var kode_cek = "<?php echo $this->session->userdata('kode_cek');?>";
        var kotakpaket = "";
        var kotakpaket2 = "";    
        var jenjang_user = "<?php echo $this->session->userdata('jenjang_type'); ?>"
        var id_user_kids = "<?php echo $this->session->userdata('id_user_kids'); ?>";
        var id_user = "<?php echo $this->session->userdata('id_user'); ?>";
        if (id_user != id_user_kids) {
            jenjang_user = "SD";
            // notify('top','right','fa fa-check','info','animated fadeInDown','animated flipOutX','<?php echo $this->lang->line('hello'); ?> '+username_kids);
        }
        else{
           jenjang_user = "<?php echo $this->session->userdata('jenjang_type'); ?>"; 
           // notify('top','right','fa fa-check','info','animated fadeInDown','animated flipOutX','<?php echo $this->lang->line('hello'); ?> '+user_name);
        }
        var jenjang_id = "<?php echo $this->session->userdata('jenjang_id_kids');?>";
        if (jenjang_id == "") {
            jenjang_id = "<?php echo $this->session->userdata('jenjang_id');?>";
        }

        function dtl_paket(kode_cek=''){
            $.ajax({
                url: '<?php echo BASE_URL();?>Rest/dtl_ProgramPackage',
                type: 'POST',
                data: {
                    list_id : kode_cek
                },
                success: function(response)
                {
                    var a = JSON.stringify(response);
                    console.log(a);
                    var name = response['data']['name'];
                    var description = response['data']['description'];
                    var bidang = response['data']['bidang'];
                    var fasilitas = response['data']['fasilitas'];
                    var listbidang = bidang.split(',');
                    var listfasilitas = fasilitas.split(',');
                    var price_plan = response['data']['price'];
                    var price =price_plan.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    var poster = response['data']['poster'];                    
                    var poster_mobile      = response['data']['poster_mobile'];
                    $("#desc_name").html(name);
                    $("#desc_description").html(description);
                    $("#desc_price").html('Price : Rp '+price);
                    $("#desc_poster").attr('src',poster_mobile);
                    $(".buy_package").attr('uid',kode_cek);
                    for (var i = 0; i < listbidang.length; i++) {
                        listbidang[i];
                        var list_bidang = "<li>"+listbidang[i]+"</li>";
                        $("#list_bidang").append(list_bidang);
                    }
                    for (var b = 0; b < listfasilitas.length; b++) {
                        listfasilitas[b];
                        var list_fasilitas = "<li>"+listfasilitas[b]+"</li>";
                        $("#list_fasilitas").append(list_fasilitas);
                    }
                }
            });
        };

        if (kode_cek!="") {
            // $("#header").css('display','none');
            // $("#header_cm").css('display','none');
            // $("#box_descprogram").css('display','block');
            // $("#box_package").css('display','none');
            dtl_paket(kode_cek);
        }
        else
        {
            // $("#header").css('display','block');
            // $("#header_cm").css('display','block');
            // $("#box_descprogram").css('display','none');
            // $("#box_package").css('display','block');
        }

        

        $.ajax({
           url: '<?php echo base_url(); ?>Rest/listdata_Program',
            type: 'POST',
            data:{
                id_user : id_user,
                jenjang_id :jenjang_id
            },
            success: function(response)
            {   
                if (response['data']==null) {
                    $("#slide_program").css('display','none');
                    $("#info_program").css('display','block');
                }
                else{
                    $("#slide_program").css('display','block');
                    $("#info_program").css('display','none');
                    for (var i = 0; i < response.data.length; i++) {
                        
                        var a = JSON.stringify(response);
                        list_id      = response['data'][i]['list_id'];
                        channel_id      = response['data'][i]['channel_id'];
                        channel_name      = response['data'][i]['channel_name'];
                        name      = response['data'][i]['name'];
                        poster      = response['data'][i]['poster'];
                        poster_mobile      = response['data'][i]['poster_mobile'];
                        description      = response['data'][i]['description'];
                        type      = response['data'][i]['type'];
                        price_plan      = response['data'][i]['price'];
                        jenjang_id      = response['data'][i]['jenjang_id'];

                        var billing =response['data'][i]['billing_type'];
                        var stringify = JSON.parse(billing);
                        for (var i = 0; i < stringify.length; i++) {
                            var options_billing = stringify[i]['options'];
                            var type_billing = stringify[i]['type'];
                            var text_metode = null;
                            if (type_billing=='byday') {
                                text_metode = "By Day";
                            }
                            else if (type_billing == 'bycounter') {
                                text_metode = "By Counter";
                            }
                            var kotak_type ="<label class='radio radio_metode radio-inline m-r-20'>"+
                                                "<input type='radio' options='"+options_billing+"' id='billing_type_"+type_billing+"' name='billing_type' value='"+type_billing+"'>"+
                                                "<i class='input-helper'</i>"+
                                                text_metode
                                            "</label>";
                                            $("#pilihtype").append(kotak_type);
                            // console.log(stringify[i]['options']);
                        }
       
                        var getJenjang_id = JSON.parse(jenjang_id);
                        var price =price_plan.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                        var alt_img         ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                        if (i > 4) {
                            $("#box_control").css('display','block');
                            kotakpaket2 += "<div class='col-xs-12'>"+
                                "<div class='fff'>"+
                                    "<div class='thumbnail'>"+
                                        "<a href='#'><img style='width:100%; height:auto;' src='"+poster_mobile+"' alt=''></a>"+
                                    "</div>"+
                                    "<div class='caption'>"+
                                        "<h4>"+name+"</h4>"+
                                        // "<p style='display:none;'>"+description+"</p>"+
                                        "<center style='display:none;'>><p>"+price+"</p></center>"+
                                        "<a class='btn btn-mini' href='#'>» Read More</a>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
                        }
                        else
                        {
                            kotakpaket += "<div class='col-xs-12 ' style='padding-left:1%; padding-right:1%;'>"+
                                "<div class='fff'>"+
                                    "<div class='selectpaket thumbnail bs-item z-depth-1' list_id='"+list_id+"'  style='margin-bottom:4%'>"+
                                        "<a href='#'><img class='img-rounded' style='width:100%; height:auto;' src='"+poster_mobile+"' alt=''></a>"+
                                        "<label style='padding-top:2%; padding-left:2%; padding-right:2%; color : black;'><b>"+name+"</b></label>"+
                                        // "<p style='padding-left:2%; padding-right:2%; font-size:11.5px; padding:1.5px; margin-bottom:0; height: 35px; width:100%; overflow: hidden;  white-space: wrap; text-overflow: ellipsis;'>"+description+"</p>"+
                                    "</div>"+
                                    "<div class='caption'>"+
                                        "<center style='display:none;'>><p style='font-size:18px; color:#16A085;'>Rp "+price+"</p></center>"+
                                        "<a class='btn btn-success btn-block selectpaket' list_id='"+list_id+"' style='display:none; margin-bottom:3px' href='#'>Beli Paket</a>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
                        }

                        
                        $("#box_slide1").append(kotakpaket);
                        $("#box_slide2").append(kotakpaket2);
                    }
                }
                
                

            }
        });
        $(document.body).on('click', '.selectpaket' ,function(e){
            var list_id = $(this).attr('list_id');
            $.ajax({
                url : '<?php echo base_url();?>Master/setkodecek',
                type: 'POST',
                data:{
                    kode_cek : list_id
                },
                success: function(response)
                {
                    var a = JSON.parse(response);
                    var kode = a['data']
                    // console.log(kode);
                    dtl_paket(kode);
                    // $("#box_package").css('display','none');
                    // $("#box_descprogram").css('display','block');
                    $("#modalShowDescProgram").modal('show');
                    // $("#header").css('display','none');
                    // $("#header_cm").css('display','none');
                }
            })
        });
        $(document).on('change', 'input:radio[id^="billing_type"]', function (event) {
            $("#pilihjumlah").empty();
            $("#pilih_jumlah").css('display','block');
            var type = $(this).val();
            var options = $(this).attr('options');
            var options_billing = options.split(',');

            for(var i = 0; i < options_billing.length; i++) {
               // Trim the excess whitespace.
               options_billing[i] = options_billing[i].replace(/^\s*/, "").replace(/\s*$/, "");
               // Add additional code here, such as:
               // alert(options_billing[i]);
               var kotak_options ="<label class='radio radio_metode radio-inline m-r-20'>"+
                                                "<input type='radio' id='billing_options_"+options_billing[i]+"' name='billing_options' value='"+options_billing[i]+"'>"+
                                                "<i class='input-helper'</i>"+
                                                options_billing[i]
                                            "</label>";
                                            $("#pilihjumlah").append(kotak_options);
            }
        });

        $(document.body).on('click', '.btn_back' ,function(e){
            $("#list_bidang").empty();
            $("#list_fasilitas").empty();
            // $("#box_package").css('display','block');
            // $("#header").css('display','block');
            // $("#header_cm").css('display','block');
            // $("#box_descprogram").css('display','none');

        });
        $(document.body).on('click', '.btn_kid_buy' ,function(e){
            var billing_type = $('input[name=billing_type]:checked').val();
            var billing_options = $('input[name=billing_options]:checked').val();
            var kid_id = $('.cek_anak:checked').val();
            var list_id = $("#get_list_id").val();
            // alert(list_id);
            // return false;
            $.ajax({
                url: '<?php echo BASE_URL();?>Rest/requestProgramPackage',
                type: 'POST',
                data: {
                    billing_type : billing_type,
                    billing_options :billing_options,
                    list_id : list_id,
                    id_requester : kid_id
                },
                success: function(response)
                {
                    var code = response['code'];
                    if (code == 200) {
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Package telah ke Keranjang, Silahkan lanjutkan pembayaran');
                        setTimeout(function(){
                            location.reload();
                        },3000);
                    }
                    else
                    {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','Gagal, silahkan coba lagi');
                        setTimeout(function(){
                            location.reload();
                        },3000);
                    }
                }
            });
        });
        $(document.body).on('click', '.buy_package' ,function(e){

            $("#modalShowDescProgram").modal('hide');
            var billing_type = $('input[name=billing_type]:checked').val();
            var billing_options = $('input[name=billing_options]:checked').val();
            // alert(billing_type);
            // alert(billing_options);
            // return false;

            $(".btn_kid_buy").attr('uid',kode_cek);
            var list_id = $(this).attr('uid');
            var jenjang_id_paket = $(this).attr('jenj');
            var id_requester = "<?php echo $this->session->userdata('id_user');?>";
            var jenjang_id = "<?php echo $this->session->userdata('jenjang_id');?>";
            if (billing_type == null || billing_options == null) {
                    swal({   
                        title: "Pembelian Gagal",   
                        text: "Silakan pilih tipe pembayaran terlebih dahulu",   
                        timer: 2000,   
                        showConfirmButton: false 
                    });
                    setTimeout(function(){
                        $("#modalShowDescProgram").modal('show');
                    },2000);



            }
            else if (jenjang_id != jenjang_id_paket) {
                $("#modal_choose_kids").modal('show');
                $("#modalShowDescProgram").modal('hide');
                $("#get_jenjang_id").val(jenjang_id_paket);
            }
            else{
                $.ajax({
                    url: '<?php echo BASE_URL();?>Rest/requestProgramPackage',
                    type: 'POST',
                    data: {

                        billing_type : billing_type,
                        billing_options :billing_options,
                        list_id : list_id,
                        id_requester : id_requester
                    },
                    success: function(response)
                    {
                        var code = response['code'];
                        if (code == 200) {
                            notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut','Package telah ke Keranjang, Silahkan lanjutkan pembayaran');
                            setTimeout(function(){
                                location.reload();
                            },3000);
                        }
                        else
                        {
                            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','Gagal, silahkan coba lagi');
                            setTimeout(function(){
                                location.reload();
                            },3000);
                        }
                    }
                });
            }
        });

    });

</script>
<!-- Script Extra Class -->
<script type="text/javascript">
    var cekkotak = 0;
    $("#btntambah").click(function(){
        if (cekkotak == 0) 
        {            
            $("#kotakpilihanteman").append("<div class='col-md-8 m-b-25' style='padding: 0;' id='kotakpilihanteman2'><div class='col-md-12'><label for='nama'>Tambahkan Temanmu melalui Email</label><input type='text' class='form-control typeahead' id='nama' data-provide='typeahead' autocomplete='off'></div></div><div class='col-md-4'><button class='btn btn-info waves-effect btn-sm m-t-20' id='btntambah'><i class='zmdi zmdi-plus'></i></button><button class='btn btn-danger waves-effect btn-sm m-t-20' id='btnkurang'><i class='zmdi zmdi-minus'></i></button></div>");
            cekkotak+1;
        }        
    });
    $("#btn_notifgroup").click(function(){
        $("#modal_invitation").modal('show');
        angular.element(document.getElementById('btn_notifgroup')).scope().getondemandprivate();
        getinvitation();
    });

    // var status_user = "<?php echo $this->session->userdata('id_user')?>";
    // alert(status_user);

    // var a = $('select[name=nama_anak]').val();
    $("#btn_openmodal").click(function(){
        $("#modal_list_anak").modal('show');
    });  
    var cek_anak = "<?php echo $this->session->userdata('punya_anak');?>";
    var jenjang_anak = "<?php echo $this->session->userdata('jenjang_anak');?>";
    var parent_id = <?php echo $this->session->userdata('id_user');?>;

    var stat_showdetail = "<?php echo $this->session->userdata('stat_showdetail');?>";     
    // alert(jenjang_anak);
    var tokenjwttt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
    var myjenjang = "<?php echo $this->session->userdata('jenjang_id'); ?>";
    var user_utcc = new Date().getTimezoneOffset();
    var tgll = new Date();
    var formattedDatee = moment(tgll).format('YYYY-MM-DD');
    var iduser = "<?php echo $this->session->userdata('id_user_kids'); ?>";
    if (iduser == null || iduser=="") {
        iduser  = "<?php echo $this->session->userdata('id_user'); ?>";
    }
    user_utcc = -1 * user_utcc;  

    if (stat_showdetail!="") {        

        $.ajax({
            url: '<?php echo BASE_URL();?>Rest/get_classdesc/access_token/'+tokenjwttt,
            type: 'POST',
            data: {
                user_utc: user_utcc,
                user_device: 'web',
                date: formattedDatee,
                class_id: stat_showdetail
            },
            success: function(data)
            {                   
                var a = JSON.stringify(data);
                console.warn('data '+a);
                subject_id      = data['data'][0]['subject_id'];
                jenjang_id      = data['data'][0]['jenjang_id'];
                status_all      = data['data'][0]['status_all'];
                subject_name    = data['data'][0]['name'];
                subject_description = data['data'][0]['description'];
                tutor_id        = data['data'][0]['tutor_id'];
                start_time      = data['data'][0]['start_time'];
                finish_time     = data['data'][0]['finish_time'];
                class_type      = data['data'][0]['class_type'];
                template_type   = data['data'][0]['template_type'];
                template        = data['data'][0]['template_type'];
                jenjangnamelevel        = data['data'][0]['jenjangnamelevel'];
                user_name       = data['data'][0]['user_name'];
                email           = data['data'][0]['email'];
                user_gender     = data['data'][0]['user_gender'];
                user_age        = data['data'][0]['user_age'];
                user_image      = data['data'][0]['user_image'];
                dateclass       = data['data'][0]['date'];
                timeshow        = data['data'][0]['time'];
                endtimeshow     = data['data'][0]['endtime'];
                jenjang_idd     = data['data'][0]['jenjang_idd'];
                timeclass       = data['data'][0]['date']+" "+data['data'][0]['time'];
                endtimeclass    = data['data'][0]['enddate']+" "+data['data'][0]['endtime'];
                var all_jenjang = null;

                if (jenjang_idd != null) {
                    
                }
                else{
                    jenjang_idd = jenjang_id;
                }
                
                var userutc     = "<?php echo $this->session->userdata('user_utc');?>";
                var participant_listt = data['data']['participant']['participant'];

                // alert(jenjangnamelevel);
                if (status_all != null) {
                    var desc_jenjang = "Semua Jenjang";
                }
                if (status_all == null) {
                    var desc_jenjang = jenjangnamelevel;   
                }

                if (myjenjang != jenjang_idd && cek_anak !="yes") {
                    // $("#modal_confirm").modal('show');
                    // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Kelas Tersebut tidak tersedia di jenjang Anda");

                    $("#modal_confirm").modal('show');
                    $("#modal_list_anak").modal('hide');
                    $("#labelclass").text("Kelas tersebut tidak sesuai dengan jenjang Anda");
                    $("#sidebar").css('margin-top','3.5%');
                    $("#kalender").css('display','block');
                    $("#content_classdescription").css('display','none');
                    $("#content_extraclass").css('display','none');
                    $("#content_myclass").css('display','block'); 
                    // $.ajax({
                    //     url: '<?php echo base_url(); ?>Ajaxer/unset_codedesc/',
                    //     type: 'POST',
                    //     success: function(data)
                    //     {              
                            
                    //     }
                    // });
                }
                else if (myjenjang != jenjang_idd && cek_anak == "yes") {

                    $.ajax({
                        url: '<?php echo base_url(); ?>Master/checkListKids',
                        type: 'POST',
                        data: {                
                            user_utc : user_utc,
                            device: 'web'
                        },            
                        success: function(data)
                        {         
                            result = JSON.parse(data);                
                            if (result['status'] == true) 
                            {                       
                                for (var i = 0; i < result['message'].length; i++) {
                                    
                                    var kid_id         = result['message'][i]['kid_id'];
                                    var kid_jenjang_id         = result['message'][i]['jenjang_id'];
                                    var kid_first_name         = result['message'][i]['first_name'];
                                    var kid_last_name         = result['message'][i]['last_name'];
                                    var kid_user_image         = result['message'][i]['user_image'];
                                    var kid_user_name         = result['message'][i]['user_name'];

                                    var cek_anak = 0;
                                    if (kid_jenjang_id != jenjang_idd) {
                                        
                                    }
                                    else{
                                        var ada_anak =null;
                                        ada_anak++;
                                    }

                                    
                                }
                                // alert(ada_anak);
                                if (ada_anak == null) {
                                    // alert("Tidak ada anak");

                                    $("#modal_confirm").modal('show');
                                    $("#modal_list_anak").modal('hide');
                                    $("#labelclass").text("Kelas tersebut tidak sesuai dengan jenjang Anda");
                                    $("#sidebar").css('margin-top','3.5%');
                                    $("#kalender").css('display','block');
                                    $("#content_classdescription").css('display','none');
                                    $("#content_extraclass").css('display','none');
                                    $("#content_myclass").css('display','block'); 
                                }
                                else if (ada_anak != null) {
                                    // alert("Ada anak");

                                    $("#modal_list_anak").modal('show');
                                    $("#modal_confirm").modal('hide');
                                    $("#content_myclass").css('display','block');
                                    $("#content_extraclass").css('display','none');
                                    $("#sidebar").css('margin-top','0%');
                                    $("#kalender").css('display','none');
                                    $("#modal_extraclass").modal('hide');
                                    $("#content_classdescription").css('display','none');

                                    // $.ajax({
                                        //     url: '<?php echo base_url(); ?>Ajaxer/unset_codedesc/',
                                        //     type: 'POST',
                                        //     success: function(data)
                                        //     {              
                                                
                                        //     }
                                        // });

                                        $('select').on('change', function() {

                                            var kid_jenjang_id =(this.value );

                                            var kid_id = $(this).children(":selected").attr("id");
                                            // alert(id);

                                            // alert(jenjang_idd);
                                            // alert("kid jenjang "+kid_jenjang_id);

                                            $('.buttonjoinanak').click(function(){
                                                
                                                // alert(h);
                                                if (kid_jenjang_id != jenjang_idd) {
                                                    $("#modal_confirm_anak").modal('show');
                                                    $("#modal_list_anak").modal('hide');
                                                    $("#content_myclass").css('display','block');
                                                    $("#content_extraclass").css('display','none');
                                                    $("#sidebar").css('margin-top','0%');
                                                    $("#kalender").css('display','none');
                                                    $("#modal_extraclass").modal('hide');
                                                    $("#content_classdescription").css('display','none');
                                                    // $("#labelclass").text("Kelas tersebut tidak sesuai dengan jenjang Anda");
                                                }
                                                else if (kid_jenjang_id == jenjang_idd) {

                                                    $("#modal_confirm_anak").modal('hide');
                                                    $("#modal_list_anak").modal('hide');
                                                    if (class_type=="multicast") {typeclass="Multicast";}else if(class_type=="private"){typeclass="Private";}else{typeclass="Group";}
                                                    if (template_type=="whiteboard_digital") {template_type="Digital Board";}else if(template_type=="whiteboard_videoboard"){template_type="Video Board";}else{template_type="All Features";}
                                                    $("#desc_tutorname").append("<i class='zmdi zmdi-assignment-account m-r-10'></i> "+user_name);
                                                    $("#desc_tutorgender").append("<i class='zmdi zmdi-male-female m-r-10'></i> "+user_gender);
                                                    $("#desc_tutorage").append("<i class='zmdi zmdi-time-interval m-r-10'></i> "+user_age+" tahun");
                                                    $("#desc_tutorimage").attr('src', '<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>'+user_image);

                                                    $("#titleone").text(subject_name);
                                                    $("#titletwo").text(subject_description);
                                                    $("#desc_jenjang").text(desc_jenjang);
                                                    $("#desc_date").text(dateclass);
                                                    $("#desc_starttime").text(timeshow+" - ");
                                                    $("#desc_finishtime").text(endtimeshow);
                                                    $("#desc_classtype").text(typeclass);
                                                    $("#desc_templatetype").text(template_type);
                                                    $("#nametutor").text(user_name);

                                                    $(".buttonjoinn").attr('link','Transaction/multicast_join/'+stat_showdetail+'?t=kids&child_id='+kid_id);

                                                    $("#content_myclass").css('display','none');
                                                    $("#content_extraclass").css('display','none');
                                                    $("#sidebar").css('margin-top','0%');
                                                    $("#kalender").css('display','none');
                                                    $("#modal_extraclass").modal('hide');
                                                    $("#content_classdescription").css('display','block');
                                                }

                                                else if (myjenjang != jenjang_idd)  {
                                                    $("#modal_confirm_anak").modal('show');
                                                    $("#modal_list_anak").modal('hide');
                                                    $("#content_myclass").css('display','block');
                                                    $("#content_extraclass").css('display','none');
                                                    $("#sidebar").css('margin-top','0%');
                                                    $("#kalender").css('display','none');
                                                    $("#modal_extraclass").modal('hide');
                                                    $("#content_classdescription").css('display','none');
                                                    // $("#labelclass").text("Kelas tersebut tidak sesuai dengan jenjang Anda");
                                                }
                                            });
                                        });

                                        $('.buttonjoinanak').click(function(){
                                                
                                                
                                            if (myjenjang != jenjang_idd)  {
                                                $("#modal_confirm_anak").modal('show');
                                                $("#modal_list_anak").modal('hide');
                                                $("#content_myclass").css('display','block');
                                                $("#content_extraclass").css('display','none');
                                                $("#sidebar").css('margin-top','0%');
                                                $("#kalender").css('display','none');
                                                $("#modal_extraclass").modal('hide');
                                                $("#content_classdescription").css('display','none');
                                                // $("#labelclass").text("Kelas tersebut tidak sesuai dengan jenjang Anda");
                                            }
                                        });


                                }
                            }
                        }
                    });
                    
                }
                else
                {
                    var im_exist = false;                   
                    if(participant_listt != null){
                        for (var iai = 0; iai < participant_listt.length ;iai++) {                                                              
                            if (id_user == participant_listt[iai]['id_user']) {
                                im_exist = true;
                            }                        
                        }
                    }

                    if (im_exist) {

                        
                        notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut',"Anda telah join di kelas "+subject_name+' - '+subject_description);
                        $("#sidebar").css('margin-top','3.5%');
                        $("#kalender").css('display','block');
                        $("#content_classdescription").css('display','none');
                        $("#content_extraclass").css('display','none');
                        $("#content_myclass").css('display','block'); 
                        // $.ajax({
                        //     url: '<?php echo base_url(); ?>Ajaxer/unset_codedesc/',
                        //     type: 'POST',
                        //     success: function(data)
                        //     {              
                                
                        //     }
                        // });
                    }
                    else
                    {
                        if (class_type=="multicast") {typeclass="Multicast";}else if(class_type=="private"){typeclass="Private";}else{typeclass="Group";}
                        if (template_type=="whiteboard_digital") {template_type="Digital Board";}else if(template_type=="whiteboard_videoboard"){template_type="Video Board";}else{template_type="All Features";}
                        $("#desc_tutorname").append("<i class='zmdi zmdi-assignment-account m-r-10'></i> "+user_name);
                        $("#desc_tutorgender").append("<i class='zmdi zmdi-male-female m-r-10'></i> "+user_gender);
                        $("#desc_tutorage").append("<i class='zmdi zmdi-time-interval m-r-10'></i> "+user_age+" tahun");
                        $("#desc_tutorimage").attr('src', '<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>'+user_image);

                        $("#titleone").text(subject_name);
                        $("#titletwo").text(subject_description);
                        $("#desc_jenjang").text(desc_jenjang);
                        $("#desc_date").text(dateclass);
                        $("#desc_starttime").text(timeshow+" - ");
                        $("#desc_finishtime").text(endtimeshow);
                        $("#desc_classtype").text(typeclass);
                        $("#desc_templatetype").text(template_type);
                        $("#nametutor").text(user_name);

                        $(".buttonjoinn").attr('link','Transaction/multicast_join/'+stat_showdetail+'?t=join');

                        $("#content_myclass").css('display','none');
                        $("#content_extraclass").css('display','none');
                        $("#sidebar").css('margin-top','0%');
                        $("#kalender").css('display','none');
                        $("#modal_extraclass").modal('hide');
                        $("#content_classdescription").css('display','block');
                    }
                }
            }
        });
        
        $(".buttonjoinn").click(function(){
            var link = "<?php echo base_url();?>"+$(this).attr('link');
            $.ajax({
                url: '<?php echo base_url(); ?>Ajaxer/unset_codedesc/',
                type: 'POST',
                success: function(data)
                {              
                    window.location.replace(link);
                }
            });            
        });

        $(".buttonok").click(function(){
            // var link = "<?php echo base_url();?>"+$(this).attr('link');
            $.ajax({
                url: '<?php echo base_url(); ?>Ajaxer/unset_codedesc/',
                type: 'POST',
                success: function(data)
                {              
                    // window.location.replace(link);
                }
            });            
        });
    }

    $('#selectkelas').change(function(e){
        var kelas = $("#selectkelas").val();

        if (kelas =="private") {
            $("#xboxprivateclass").css('display','block');
            $("#xboxgroupclass").css('display','none');
            $("#allclass").css('display','none');
        }
        else if (kelas =="group") {
            $("#xboxprivateclass").css('display','none');
            $("#xboxgroupclass").css('display','block');
            $("#allclass").css('display','none');
        }
        else if (kelas =="all") {
            $("#xboxprivateclass").css('display','block');
            $("#xboxgroupclass").css('display','block');
            $("#allclass").css('display','block');
        }
    });
    $("#btn_back_group").click(function(){
        $("#modal_groupclass").modal('hide');
        $("#modal_extraclass").modal('show');
    });
    $("#btn_back_search_group").click(function(){
        $("#kotakuntukpencariangroup").css('display','block');
        $("#kotakhasilpencariangroup").css('display','none');
        $("#btn_back_group").css('display','block');
        $("#btn_back_search_group").css('display','none');
    });
    $("#btn_back_private").click(function(){
        $("#modal_privateclass").modal('hide');
        $("#modal_extraclass").modal('show');
    });
    $("#btn_back_search_private").click(function(){
        $("#kotakuntukpencarianprivat").css('display','block');
        $("#kotakhasilpencarianprivat").css('display','none');
        $("#btn_back_private").css('display','block');
        $("#btn_back_search_private").css('display','none');
    });
    $("#privateclick").click(function(){
        $("#kotakuntukpencarianprivat").css('display','block');
        $("#kotakhasilpencarianprivat").css('display','none');
        // $("#sidebar").css('margin-top','0%');
        // $("#kalender").css('display','none');
        $("#modal_extraclass").modal('hide');
        // $("#content_myclass").css('display','none');
        $("#content_classdescription").css('display','none');
        // $("#content_extraclass").css('display','block');
        $("#modal_privateclass").modal('show');
        $("#btn_back_private").css('display','block');
        $("#btn_back_search_private").css('display','none');

        $("#kotaknyagroup").removeClass('bgm-orange');
        $("#kotaknyagroup").addClass('bgm-gray');
        $("#kotaknyaprivate").removeClass('bgm-gray');
        $("#kotaknyaprivate").addClass('bgm-orange');

        // $("#box_groupclass").css('display','none');
        $("#privateclick").css('filter','grayscale();');
        $("#groupklik").css('filter','none');
        // $("#box_privateclass").css('display','block');
    });
    $("#groupclassclick").click(function(){
        $("#kotakuntukpencariangroup").css('display','block');
        $("#kotakhasilpencariangroup").css('display','none');
        $("#sidebar").css('margin-top','0%');
        // $("#kalender").css('display','none');
        $("#modal_extraclass").modal('hide');
        $("#modal_groupclass").modal('show');
        // $("#content_myclass").css('display','none');
        // $("#content_classdescription").css('display','none');
        $("#content_extraclass").css('display','block');
        $("#kotaknyaprivate").removeClass('bgm-orange');
        $("#kotaknyaprivate").addClass('bgm-gray'); 
        $("#kotaknyagroup").removeClass('bgm-gray');
        $("#kotaknyagroup").addClass('bgm-orange');
        // $("#box_privateclass").css('display','none');
        $("#box_groupclass").css('opacity','100');        
        $("#durasigroup").addClass('col-md-12');
        // $("#box_groupclass").css('display','block');
    });
    $('#subject_id').change(function(e){
    $('#subjecton').val($(this).val());
    });
    $('#duration').change(function(e){
        $('#durationon').val($(this).val());
    });
    $('#subject_idg').change(function(e){
        $('#subjectong').val($(this).val());
    });
    $('#durationg').change(function(e){
        $('#durationong').val($(this).val());
    });

    // $("#btn_cari").click(function(){
    //     $("#modal_privateclass").modal('hide');
    // });

    $('#button_search').click(function(){
        // subject_id,dateon,timeeeeeee,duration
        var sbj_id      = $("#subject_id").val();
        var dton        = $("#dateon").val();        
        var drt         = $("#duration").val();        
        if (sbj_id == null) {            
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertsubjectnull'); ?>');    
        }
        else if(dton == ""){
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdatenull'); ?>');
        }
        else if(drt == null){
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdurationnull'); ?>');
        }   
        else
        {   
            $("#kotakhasilpencarianprivat").css('display','block');
            $("#body_private").css('max-height','500px');
            $("#kotakuntukpencarianprivat").css('display','none');
            $("#btn_back_private").css('display','none');
            $("#btn_back_search_private").css('display','block');
            angular.element(document.getElementById('button_search')).scope().getondemandprivate();
        }
    });

    $('#button_searchgroup').click(function(){
        // durationg,timeeeeee,dateong,subject_idg
        var sbj_idg      = $("#subject_idg").val();
        var dtong        = $("#dateong").val();        
        var drtg         = $("#durationg").val();
        if (sbj_idg == null) {            
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertsubjectnull'); ?>');    
        }
        else if(dtong == ""){
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdatenull'); ?>');
        }
        else if(drtg == null){
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdurationnull'); ?>');
        }   
        else
        {
            $("#kotakhasilpencariangroup").css('display','block');
            $("#body_group").css('max-height','600px');
            $("#kotakuntukpencariangroup").css('display','none');
            $("#btn_back_group").css('display','none');
            $("#btn_back_search_group").css('display','block');
            angular.element(document.getElementById('button_search')).scope().getondemandgroup();
        }
    });

    $(document.body).on('click', '.btn-ask' ,function(e){       
        $("#showalertpayment").css('display','none');
        $("#afterchoose").css('display','none');
        $("#beforechoose").css('display','block');
        $("#showdetailtutor").css('display','block');
        $("#topikprivate").val('');
        var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc;

        var subject_id = $(this).attr('subject_id');
        var start_time = $(this).attr('start_time');
        var date = $(this).attr('dates');
        var duration = $(this).attr('duration');    
        var tutor_id = $(this).attr('tutor_id');

        var subject_idg = $(this).attr('subject_idg');
        var start_timeg = $(this).attr('start_timeg');
        var avtime_idg = $(this).attr('avtime_idg');
        var durationg = $(this).attr('durationg'); 

        var user_name = $(this).attr('username');
        var image_user = $(this).attr('image');
        var harga = $(this).attr('harga');
        var hargaakhir = $(this).attr('hargaakhir');
        var gh = $(this).attr('gh');
        var iduser = "<?php echo $this->session->userdata('id_user_kids'); ?>";

        if (iduser == null || iduser=="") {
            iduser  = "<?php echo $this->session->userdata('id_user'); ?>";
        }

        var tokenjwtt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        $('#namemodal').text(user_name);
        $('#timestart').text(start_time);
        $('#tanggalkelas').text(date);
        $('#hargakelas').text("Rp. "+hargaakhir);
        $('.imagetutor_private').attr("src", image_user);        

        if (subject_id == '' && start_time == '' && avtime_id == '' && duration == '') {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Harap isi data dengan lengkap");
        }
        else
        {
            // $.ajax({
            //     url: '<?php echo base_url(); ?>/Rest/isEnoughBalance/access_token/'+tokenjwtt,
            //     type: 'POST',
            //     data: {
            //         id_user: iduser,
            //         gh : gh
            //     },               
            //     success: function(data)
            //     {
            //         balanceuser = data['balance'];

            //         if (data['status'])
            //         {
                        $('.btn-req-demand').attr('demand-link','<?php echo base_url(); ?>/process/demand_me?start_time='+start_time+"&tutor_id="+tutor_id+"&subject_id="+subject_id+"&duration="+duration+"&date="+date+"&user_utc="+user_utc+"&hr="+harga+"&hrt="+hargatutor+"&id_user="+iduser);
                        $('#modalconfrim').modal('show');
                        $('#modal_privateclass').modal('hide');
            //         } else {
            //             notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
            //         }
            //     }
            // });
        }
    });

    $(document.body).on('click', '.btn-req-demand' ,function(e){
        var topic= $("#topikprivate").val();
        var link = $(this).attr('demand-link');
        link     = link+"&topic="+topic;        
        if (topic == "") {
            $("#alerttopik").css('display','block');
        }
        else
        {             
            $('#modalconfrim').modal('hide');   
            angular.element(document.getElementById('getloading')).scope().loadings();
            $('.btn-req-demand').css('disabled','true');                        
            $.get(link,function(response){
                var a = JSON.parse(response);               
                if (a['status'] == 1) {
                    // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"<?php echo $this->lang->line('successfullydemand');?>");     
                    $('#modalconfrim').modal('show');           
                    $("#beforechoose").css('display','none');
                    $("#showdetailtutor").css('display','none');
                    $("#showalertpayment").css('display','block');
                    $("#afterchoose").css('display','block');
                    setTimeout(function(){
                        location.reload();
                    },3500);
                }
                else
                {
                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"<?php echo $this->lang->line('faileddemand');?>");
                    setTimeout(function(){              
                        location.reload();
                    },4000);
                }
                angular.element(document.getElementById('getloading')).scope().loadingsclose();
            });
        }
    }); 

    $(document.body).on('click','.btn-next-demand', function(e){
        $('#modalconfrim').modal("hide");
    }); 

    $(document.body).on('click','.btn-view-demand', function(e){
        location.reload();
    }); 

    $("#box_privateclass").css('opacity','100');
    $("#box_groupclass").css('opacity','0');    
    $("#kotaknyagroup").removeClass('bgm-orange');
    $("#kotaknyagroup").addClass('bgm-gray');   
    $("#btn_extraclass").click(function(){
        $("#body_private").css('max-height','700px');
        $("#body_group").css('max-height','500px');
        $("#modal_extraclass").modal('show');
        // $("#content_myclass").css('display','none');
        // $("#content_extraclass").css('display','block');
    });
    $(".btn_back_home").click(function(){
        $("#sidebar").css('margin-top','3.5%');
        $("#kalender").css('display','block');
        $("#content_classdescription").css('display','none');
        $("#content_myclass").css('display','block');
        $("#content_extraclass").css('display','none');
    }); 
    $(document).ready(function() {
            getinvitation();

            var date = null;
            var tgll = new Date();  
            var formattedDatee = moment(tgll).format('YYYY-MM-DD'); 
            var a = [];
            var b = [];            
            var c = [];
            var d = [];
            var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";            
            
            setTimeout(function(){
                // $("#kotakhasilpencarianprivat").css('display', 'block');                
                // $("#kotakhasilpencariangroup").css('display', 'block'); 
            },300);

            $( "#subject_id" ).change(function() {
                angular.element(document.getElementById('subject_id')).scope().clearSearch();
            });
            $( "#duration").change(function() {                
                angular.element(document.getElementById('duration')).scope().clearSearch();
            });

            $('#dateon').on('dp.change', function(e) {    
                var tglll = new Date();              
                angular.element(document.getElementById('dateon')).scope().clearSearch();
                date = moment(e.date).format('YYYY-MM-DD');                
                var hoursnoww = tglll.getHours(); 
                // alert(tgl);
                if (hoursnoww == 23) {                    
                    notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',"Minimum an hour required between request time and classroom schedule.");
                    $("#button_search").attr('disabled');
                    $("#button_searchgroup").attr('disabled');
                }
                else
                {
                    if (date == formattedDatee) {
                        a = [];
                        $("#tempatwaktuu").html("");
                        $("#tempatwaktuu").append("<input type='text' class='form-control' id='timeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeee").removeAttr('disabled');
                        for (var i = hoursnoww+2; i < 24; i++) {                                                
                            a.push(i);
                        }                        
                        $('#timeee').datetimepicker({                    
                            sideBySide: true, 
                            showClose: true,                   
                            format: 'HH:mm',
                            stepping: 15,
                            enabledHours: a,
                        });
                    }
                    else
                    {
                        $("#tempatwaktuu").html("");
                        $("#tempatwaktuu").append("<input type='text' class='form-control' id='timeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeee").removeAttr('disabled');
                        b = [];
                        for (var i = 0; i < 24; i++) {                                                
                            b.push(i);
                        }                        
                        $('#timeee').datetimepicker({                    
                            sideBySide: true,                    
                            format: 'HH:mm',
                            showClose: true,
                            stepping: 15,
                            enabledHours: b,
                        });
                    }
                }
            });

            $( "#subject_idg" ).change(function() {
                angular.element(document.getElementById('subject_id')).scope().clearSearch();
            });
            $( "#durationg").change(function() {                
                angular.element(document.getElementById('duration')).scope().clearSearch();
            });

            $('#dateong').on('dp.change', function(e) {
                angular.element(document.getElementById('dateong')).scope().clearSearch();
                date = moment(e.date).format('YYYY-MM-DD');                
                var hoursnoww = tgll.getHours(); 
                // alert(tgl);
                
                if (hoursnoww == 23) {
                    notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',"Minimum an hour required between request time and classroom schedule.");
                }
                else
                {
                    if (date == formattedDatee) {
                        c = [];
                        $("#tempatwaktuuu").html("");
                        $("#tempatwaktuuu").append("<input type='text' class='form-control' id='timeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeeee").removeAttr('disabled');
                        for (var i = hoursnoww+2; i < 24; i++) {                                                
                            c.push(i);
                        }                        
                        $('#timeeee').datetimepicker({                    
                            sideBySide: true, 
                            showClose: true,                   
                            format: 'HH:mm',
                            stepping: 15,
                            enabledHours: c,
                        });
                    }
                    else
                    {
                        $("#tempatwaktuuu").html("");
                        $("#tempatwaktuuu").append("<input type='text' class='form-control' id='timeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeeee").removeAttr('disabled');
                        d = [];
                        for (var i = 0; i < 24; i++) {                                                
                            d.push(i);
                        }                        
                        $('#timeeee').datetimepicker({                    
                            sideBySide: true,                    
                            format: 'HH:mm',
                            showClose: true,
                            stepping: 15,
                            enabledHours: d,
                        });
                    }
                }
            });
            var tgll = new Date();
            var formattedDatee = moment(tgll).format('YYYY-MM-DD'); 
            $('#dateon').on('dp.change', function(e) {                  
                angular.element(document.getElementById('dateon')).scope().clearSearch();
                date = moment(e.date).format('YYYY-MM-DD');                
                var hoursnoww = tgll.getHours(); 
                // alert(tgl);
                if (hoursnoww == 23) {                    
                    notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',"Minimum an hour required between request time and classroom schedule.");
                    $("#button_search").attr('disabled');
                    $("#button_searchgroup").attr('disabled');
                }
                else
                {
                    if (date == formattedDatee) {
                        a = [];
                        $("#tempatwaktuu").html("");
                        $("#tempatwaktuu").append("<input type='text' class='form-control' id='timeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeee").removeAttr('disabled');
                        for (var i = hoursnoww+2; i < 24; i++) {                                                
                            a.push(i);
                        }                       
                        $('#timeee').datetimepicker({                    
                            sideBySide: true, 
                            showClose: true,                   
                            format: 'HH:mm',
                            stepping: 15,
                            enabledHours: a,
                        });
                    }
                    else
                    {
                        $("#tempatwaktuu").html("");
                        $("#tempatwaktuu").append("<input type='text' class='form-control' id='timeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeee").removeAttr('disabled');
                        b = [];
                        for (var i = 0; i < 24; i++) {                                                
                            b.push(i);
                        }                       
                        $('#timeee').datetimepicker({                    
                            sideBySide: true,                    
                            format: 'HH:mm',
                            showClose: true,
                            stepping: 15,
                            enabledHours: b,
                        });
                    }
                }
            });

            $('#dateong').on('dp.change', function(e) {                
                angular.element(document.getElementById('dateong')).scope().clearSearch();
                date = moment(e.date).format('YYYY-MM-DD');                
                var hoursnoww = tgll.getHours(); 
                // alert(tgl);
                
                if (hoursnoww == 23) {
                    notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',"Minimum an hour required between request time and classroom schedule.");
                }
                else
                {
                    if (date == formattedDatee) {
                        c = [];
                        $("#tempatwaktuuu").html("");
                        $("#tempatwaktuuu").append("<input type='text' class='form-control' id='timeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeeee").removeAttr('disabled');
                        for (var i = hoursnoww+2; i < 24; i++) {                                                
                            c.push(i);
                        }                       
                        $('#timeeee').datetimepicker({                    
                            sideBySide: true, 
                            showClose: true,                   
                            format: 'HH:mm',
                            stepping: 15,
                            enabledHours: c,
                        });
                    }
                    else
                    {
                        $("#tempatwaktuuu").html("");
                        $("#tempatwaktuuu").append("<input type='text' class='form-control' id='timeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeeee").removeAttr('disabled');
                        d = [];
                        for (var i = 0; i < 24; i++) {                                                
                            d.push(i);
                        }                       
                        $('#timeeee').datetimepicker({                    
                            sideBySide: true,                    
                            format: 'HH:mm',
                            showClose: true,
                            stepping: 15,
                            enabledHours: d,
                        });
                    }
                }
            });

            $(function () {
                var tgl = new Date();  
                var formattedDate = moment(tgl).format('YYYY-MM-DD');
                $('#dateon').datetimepicker({  
                    minDate: moment(formattedDate, 'YYYY-MM-DD')
                });
                $('#dateong').datetimepicker({  
                    minDate: moment(formattedDate, 'YYYY-MM-DD')
                }); 
            });

            $('select.select2').each(function(){      
                if($(this).attr('id') == 'tagorang'){
                    $(this).select2({
                      tags:["",],
                      tokenSeparators: [","],
                      maximumSelectionLength: 3
                    });
                    // $(this).select2({
                    //  tokenSeparators: [','],
                    //  maximumSelectionLength: 3,              
                    //  ajax: {
                    //      dataType: 'json',
                    //      type: 'GET',
                    //      url: '<?php echo base_url(); ?>ajaxer/getNameuser',
                    //      data: function (params) {
                    //          return {
                    //            term: params.term,
                    //            page: params.page || 1
                    //          };
                    //      },
                    //      processResults: function(data){
                    //          return {
                    //              results: data.results,
                    //              pagination: {
                    //                  more: data.more
                    //              }                       
                    //          };
                    //      }                   
                    //  }
                    // });  
                }  
                else if($(this).attr('id') == 'tagteman'){

                    $(this).select2({
                      tags:["",],
                      tokenSeparators: [","],
                      maximumSelectionLength: 3
                    });
                    // $(this).select2({
                    //  tokenSeparators: [','],
                    //  maximumSelectionLength: 3,              
                    //  ajax: {
                    //      dataType: 'json',
                    //      type: 'GET',
                    //      url: '<?php echo base_url(); ?>ajaxer/getNameuser',
                    //      data: function (params) {
                    //          return {
                    //            term: params.term,
                    //            page: params.page || 1
                    //          };
                    //      },
                    //      processResults: function(data){
                    //          return {
                    //              results: data.results,
                    //              pagination: {
                    //                  more: data.more
                    //              }                       
                    //          };
                    //      }                   
                    //  }
                    // });  
                }                                             
            });

            var namaa = [];
            var ininama;
            $.ajax({
                url: '<?php echo base_url(); ?>Rest/getnameuser',
                type: 'POST',               
                success: function(data)
                {   
                    // var a = JSON.stringify(data['response']);
                    // var b = JSON.parse(a);
                    ininama = data['response'];                    
                    // console.warn(b);
                    // console.warn(b.length);
                    for (i = 0; i < b.length; i++) {
                        var maskid = "";
                        var myemailId =  b[i].email;
                        var prefix= myemailId.substring(0, myemailId .lastIndexOf("@"));
                        var postfix= myemailId.substring(myemailId .lastIndexOf("@"));

                        for(var j=0; j<prefix.length; j++){
                            if(j >= prefix.length - 4 ) {   
                                maskid = maskid + "*";////////
                            }
                            else {
                                maskid = maskid + prefix[j].toString();
                            }
                        }
                        maskid =maskid +postfix;
                        namaa.push(b[i].user_name+" ("+maskid+")");
                    }
                }
            }); 

            $(document).on("click", ".openmodaladd", function () {
                var that = $(this);
                var a = $(this).attr('tutor_id');
                var b = $("."+a).attr('idtutor');
                var c = $(".openmodaladd."+a).attr('tutor_id');
                var iduser = "<?php echo $this->session->userdata('id_user_kids'); ?>";

                if (iduser == null || iduser=="") {
                    iduser  = "<?php echo $this->session->userdata('id_user'); ?>";
                }
                var tokenjwttt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
                var harga = $(this).attr('harga'); 
                var gh = $(this).attr('gh');
                var date = $(this).attr('dates');

                // if ($('.'+b).is(":checked") || $('.'+b).is(":checked")) {                    
                    // $.ajax({
                    //     url: '<?php echo base_url(); ?>Rest/isEnoughBalance/access_token/'+tokenjwttt,
                    //     type: 'POST',
                    //     data: {
                    //         id_user: iduser,
                    //         gh : gh                        
                    //     },               
                    //     success: function(data)
                    //     {             
                    //         balanceuser = data['balance'];

                    //         if (data['status'])
                    //         {
                                // var metod = $("input[name='bebanmetod']:checked").val();
                                var user_utc = new Date().getTimezoneOffset();
                                user_utc = -1 * user_utc;                                
                                
                                if (a == c) {
                                    var subject_idg = that.attr('subject_id');
                                    var start_timeg = that.attr('start_time');
                                    var avtime_idg = that.attr('avtime_id');
                                    var durationg = that.attr('duration');
                                    var user_name = that.attr('username');
                                    var image_user = that.attr('image');
                                    var tutor_id = c;

                                    $('#namemodal_group').text(user_name);
                                    $('#timestart_group').text(start_timeg);
                                    $('#tanggalkelas_group').text(date);
                                    $('#hargakelas_group').text("Rp. "+harga);
                                    $('.imagetutor_private').attr("src", image_user);
                                    $(".modal-body #data_subject_id").val( subject_idg );
                                    $(".modal-body #data_start_time").val( start_timeg );
                                    $(".modal-body #data_avtime_id").val( avtime_idg );
                                    $(".modal-body #data_duration").val( durationg );
                                    $(".modal-body #data_metod").val( 'bayar_sendiri' );                                      
                                    
                                    $('.btn-req-group-demand').attr('demand-link-group','<?php echo base_url(); ?>process/demand_me_grup?start_time='+start_timeg+"&tutor_id="+tutor_id+"&subject_id="+subject_idg+"&duration="+durationg+"&metod=bayar_sendiri&date="+date+"&user_utc="+user_utc+"&harga="+harga+"&harga_tutor="+hargatutor);
                                    $("#modaladdfriends").modal('show');
                                    $("#modal_groupclass").modal('hide');
                                }
                    //         }
                            
                    //         else
                    //         {
                    //             notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Saldo anda tidak mencukupi! Mohon isi dahulu saldo anda");
                    //         }
                    //     }
                    // }); 
                // }
                // else
                // {
                //     notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',"Mohon pilih metode pembayaran terlebih dahulu!");
                // }    
            });

            $(document.body).on('click', '.btn-req-group-demand' ,function(e){
                var topic = $("#topikgroup").val();               
                // var idtage = $("#tagorang").val()+ '';                
                // if (idtage == null || idtage == "") {
                //     $(".btn-req-group-demand").removeAttr('disabled');
                //     $("#alerttopikgroup").css('display','none');
                //     $("#alertemailnotfound").css('display','block');
                //     $("#alertemailempty").css('display','block');                    
                // }
                if (topic == "") {
                    $("#alertemailnotfound").css('display','none');
                    $("#alertemailempty").css('display','none');
                    $("#alerttopikgroup").css('display','block');                    
                }
                else
                {
                    $('#modaladdfriends').modal('hide');   
                    angular.element(document.getElementById('getloading')).scope().loadings();                  
                    // $.ajax({
                    //     url: '<?php echo base_url(); ?>process/checkEmails',
                    //     type: 'POST',
                    //     data: {
                    //         id_friends: idtage                    
                    //     },               
                    //     success: function(data)
                    //     { 
                    //         var sdata = JSON.parse(data);
                    //         var id_friends = sdata['id_friends'];     
                    //         if (sdata['status'] == 1) {
                                $(".btn-req-group-demand").attr('disabled','true');
                                var link = $(".btn-req-group-demand").attr('demand-link-group')+"&id_friends=''&topic="+topic;
                                $.get(link,function(data){
                                    angular.element(document.getElementById('getloading')).scope().loadingsclose();
                              //       notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"<?php echo $this->lang->line('successfullydemand');?>");                
                                    
                                    $('#modalconfrim').modal('show');           
                                    $("#beforechoose").css('display','none');
                                    $("#showdetailtutor").css('display','none');
                                    $("#alertemailsendiri").css('display','none');
                                    $("#showalertpayment").css('display','block');
                                    $("#afterchoose").css('display','block');
                                    setTimeout(function(){
                                        location.reload();
                                    },3500);
                                });
                                
                    //         }
                    //         if(sdata['status'] == -2)
                    //         {
                    //             angular.element(document.getElementById('getloading')).scope().loadingsclose();
                    //             $('#modaladdfriends').modal('show');
                    //             $("#modal_groupclass").modal('hide');
                    //             $(".btn-req-group-demand").removeAttr('disabled');
                    //             $("#tagorang").select2("val", "");
                    //             $("#alerttopikgroup").css('display','none');
                    //             $("#alertemailempty").css('display','none');
                    //             $("#alertemailnotfound").css('display','none');
                    //             $("#alertemailsendiri").css('display','block');
                    //         }
                    //         else
                    //         {
                    //             $(".btn-req-group-demand").removeAttr('disabled');
                    //             $("#tagorang").select2("val", "");
                    //             $("#alerttopikgroup").css('display','none');
                    //             $("#alertemailempty").css('display','none');
                    //             $("#alertemailsendiri").css('display','none');
                    //             $("#alertemailnotfound").css('display','block');
                    //         }
                    //     }
                    // });                    
                }
            });   
    });
    $(document).ready(function() {

        var id = <?php echo $this->session->userdata('id_user');?>;
        // alert(id);

        setInterval(function(){  
            cekNotif();  
                 
        },3000);
    });
    function cekNotif(){
        var methodpembayaran = null;
        var hargakelas = null;
        var datetime = null;
        var iduser = "<?php echo $this->session->userdata('id_user_kids'); ?>";

        if (iduser == null || iduser=="") {
            iduser  = "<?php echo $this->session->userdata('id_user'); ?>";
        }
        var balanceuser = null;
        var requestid = null;
        var user = "<?php echo $this->session->userdata('id_user'); ?>";        
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        var user_utc = new Date().getTimezoneOffset();
        user_utc = 1 * user_utc;

        $.ajax({
            url: '<?php echo base_url(); ?>Rest/get_friendinvitation/access_token/'+tokenjwt,
            type: 'POST',   
            data: {
                id_user: user,
                user_utc:user_utc
            },             
            success: function(data)
            {                   
                var a = JSON.stringify(data['response']);
                var b = JSON.parse(a);
                // console.warn(a);
                // hargakelas = data['data']['harga_kelas'];
                if (data['code'] == 300) {
                }
                else
                {    
                 // alert(a); 
                    var no = 0;           
                    for (var i = data.response.length-1; i >=0;i--) {
                        var request_id = data['response'][i]['request_id'];
                        var requester_name = data['response'][i]['requester_name'];
                        var tutor_name = data['response'][i]['tutor_name'];
                        var subject_name = data['response'][i]['subject_name'];
                        var method_pembayaran = data['response'][i]['method_pembayaran'];
                        var harga_kelas = data['response'][i]['harga_kelas'];
                        var harga_kelass = data['response'][i]['harga_kelas'];
                        var duration_requested = data['response'][i]['duration_requested'];
                        var date_requested = data['response'][i]['date_requested'];
                        var approve = data['response'][i]['approve'];
                        var buttonnih = null;
                        var cekstatusdia = data['response'][i]['id_friends']['id_friends'];
                        var myStatus = null;
                        for (var z = cekstatusdia.length - 1; z >= 0; z--) {
                            if (cekstatusdia[z]['id_user'] == iduser) 
                            {
                                myStatus = cekstatusdia[z]['status'];
                            }
                        }
                        no = no+1;
                        if (myStatus == 0 && approve == 0) {

                           $(".notif_friend").css('display','block');
                        }   
                        else{                    
                            $(".notif_friend").css('display','none');
                        }
                        
                    }
                }
            }
        });
    }
    function getinvitation() {


        $("#tempatfriendinvitation").empty();
        var methodpembayaran = null;
        var hargakelas = null;
        var datetime = null;
        var tgl = new Date();
        var iduser = "<?php echo $this->session->userdata('id_user_kids'); ?>";

        if (iduser == null || iduser=="") {
            iduser  = "<?php echo $this->session->userdata('id_user'); ?>";
        }
        var formattedDate = moment(tgl).format('YYYY-MM-DD'); 
        var balanceuser = null;
        var requestid = null;
        var user = "<?php echo $this->session->userdata('id_user'); ?>";        
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc;
        // alert(user_utc);
        $.ajax({
            url: '<?php echo base_url(); ?>Rest/get_friendinvitation/access_token/'+tokenjwt,
            type: 'POST',   
            data: {
                id_user: user,
                user_utc: user_utc,
                user_device: 'web'                
            },             
            success: function(data)
            {                   
                var a = JSON.stringify(data['response']);
                var b = JSON.parse(a);
                // hargakelas = data['data']['harga_kelas'];
                if (data['code'] == 300) {
                    var kosong = "<tr><td colspan='11'><center>Tidak ada Data</center></td></tr>";
                    $("#tempatfriendinvitation").append(kosong);
                }
                else
                {    
                 // console.warn(a);
                    var no = 0;           
                    for (var i = 0; i < data.response.length; i++) {
                        var request_id = data['response'][i]['request_id'];
                        var requester_name = data['response'][i]['requester_name'];
                        var tutor_name = data['response'][i]['tutor_name'];
                        var user_image = data['response'][i]['user_image'];
                        var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                        var country = data['response'][i]['country'];
                        var topic = data['response'][i]['topic'];
                        var user_utc = data['response'][i]['user_utc'];
                        var subject_name = data['response'][i]['subject_name'];
                        var method_pembayaran = data['response'][i]['method_pembayaran'];
                        var harga_kelas = data['response'][i]['harga_kelas'];
                        var harga_kelass = data['response'][i]['harga_kelas'];
                        var duration_requested = data['response'][i]['duration_requested'];
                        var date_requested = data['response'][i]['date_requested'];
                        var date_requested_utc = data['response'][i]['date_requested_utc'];
                        // var endtime = date_requested + duration_requested;
                        var endtime = duration_requested / 60;

                        day = moment(date_requested_utc).format('dddd');
                        date = moment(date_requested_utc).format('DD MMMM YYYY');
                        time = moment(date_requested_utc).format('HH:mm');

                        var approve = data['response'][i]['approve'];
                        var buttonnih = null;
                        var cekstatusdia = data['response'][i]['id_friends']['id_friends'];
                        var myStatus = null;
                        var statusKelas = null;
                        methodpembayaran = method_pembayaran;
                        if (method_pembayaran=="bayar_sendiri") {
                            harga_kelas = "Rp. 0";
                            method_pembayaran = "Bayar Sendiri";
                        }
                        else
                        {
                            method_pembayaran = "Bagi Rata";
                        }
                        for (var z = 0; z < cekstatusdia.length; z++) {
                            if (cekstatusdia[z]['id_user'] == iduser) 
                            {
                                myStatus = cekstatusdia[z]['status'];

                            }


                        }
                        if (approve == 0 && myStatus == 0) {
                            statusKelas = "<i class='m-l-5 c-orange'>Status : <?php echo $this->lang->line('waitapprovedme');?></i> ";
                        }
                        else if (approve == 0 && myStatus != 0) {
                            statusKelas = "<i class='m-l-5 c-orange'>Status : <?php echo $this->lang->line('waitapprovedfriend');?></i> ";
                        }
                        else if (approve == -1) {
                            statusKelas = "<i class='m-l-5 c-red'>Status : <?php echo $this->lang->line('tutornotavailable');?></i>";
                        }
                        else if (approve == -2) {
                            statusKelas = "<i class='m-l-5 c-red'>Status : <?php echo $this->lang->line('tutornotavailable');?></i>";
                        }
                        else if (approve == -3) {
                            statusKelas = "<i class='m-l-5 c-red'>Status : <?php echo $this->lang->line('classexpired');?></i>";
                        }
                        // alert(JSON.stringify(cekstatusdia));
                        no = no+1;
                        if (myStatus == 0 && approve == 0) {
                            buttonnih = "<div class='col-md-6'><a data-toggle='modal' class='m-t-5' request_id='"+request_id+"' href='#Delete'><button class='aa-t btn btn-danger btn-block' request_id='"+request_id+"' title='Hapus'><?php echo $this->lang->line('tolak'); ?></button></a></div><div class='col-md-6'><button class='aa btn btn-success btn-block' id='kaka' request_id='"+request_id+"' gh='"+harga_kelass+"' title='Terima'><?php echo $this->lang->line('terima'); ?></button>";
                            var kotak ="<tr><td><div class='me_timer col-sm-6 col-md-12 col-xs-12'  ><div class='col-md-3'><div class='card picture-list'><div class='card blog-post' style=''><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+"  width='100%' height='150px'></div></div></div><div class='col-md-9'><span class='card-header '><h5 class='c-black' >"+day+", "+date+" - "+time+" <b class='m-l-5'></b>| "+statusKelas+"</h5><hr><h5 class='c-blue'>"+tutor_name+" - "+subject_name+"</h5><h5 class='c-blue' ><?php echo $this->lang->line('topikbelajar'); ?> "+topic+"</h5><h5 class='c-blue'><?php echo $this->lang->line('invitedby'); ?> : "+requester_name+"</h5><h5 class='c-blue'><?php echo $this->lang->line('demandprice'); ?> : "+harga_kelas+"</h5></span></div><div class='col-md-12'>"+buttonnih+"</div></div></td></tr>";
                            var kotaks = "<tr><td>"+requester_name+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+date_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td><?php echo $this->lang->line('waitingfriend'); ?></td><td>"+buttonnih+"</td></tr>";
                        }   
                        else{                    
                            if (approve == 1) {
                                buttonnih = "<button class='btn btn-gray f-16 btn-block' disabled>-</button>";
                                var kotak ="<tr><td><div class='me_timer col-sm-6 col-md-12 col-xs-12'  ><div class='col-md-3'><div class='card picture-list'><div class='card blog-post' style=''><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+"  width='100%' height='150px'></div></div></div><div class='col-md-9'><span class='card-header '><h5 class='c-black' >"+day+", "+date+" - "+time+" <b class='m-l-5'></b>| "+statusKelas+"</h5><hr><h5 class='c-blue'>"+tutor_name+" - "+subject_name+"</h5><h5 class='c-blue' ><?php echo $this->lang->line('topikbelajar'); ?> "+topic+"</h5><h5 class='c-blue'><?php echo $this->lang->line('invitedby'); ?> : "+requester_name+"</h5><h5 class='c-blue'><?php echo $this->lang->line('demandprice'); ?> : "+harga_kelas+"</h5></span></div><div class='col-md-12'>"+buttonnih+"</div></div></td></tr>";
                                var kotaks = "<tr><td>"+requester_name+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+date_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td><?php echo $this->lang->line('kelasapprove'); ?><a href='<?php echo base_url(); ?>'><?php echo $this->lang->line('myclass'); ?></a></td><td>"+buttonnih+"</td></tr>";
                            }
                            else if(approve == -1){
                                buttonnih = "<button class='btn btn-gray f-16 btn-block' disabled>-</button>";
                                var kotak ="<tr><td><div class='me_timer col-sm-6 col-md-12 col-xs-12'  ><div class='col-md-3'><div class='card picture-list'><div class='card blog-post' style=''><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+"  width='100%' height='150px'></div></div></div><div class='col-md-9'><span class='card-header '><h5 class='c-black' >"+day+", "+date+" - "+time+" <b class='m-l-5'></b>| "+statusKelas+"</h5><hr><h5 class='c-blue'>"+tutor_name+" - "+subject_name+"</h5><h5 class='c-blue' ><?php echo $this->lang->line('topikbelajar'); ?> "+topic+"</h5><h5 class='c-blue'><?php echo $this->lang->line('invitedby'); ?> : "+requester_name+"</h5><h5 class='c-blue'><?php echo $this->lang->line('demandprice'); ?> : "+harga_kelas+"</h5></span></div><div class='col-md-12'></div></div></td></tr>";
                                var kotaks = "<tr><td>"+requester_name+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+date_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td><?php echo $this->lang->line('waitingtutor'); ?></td><td>"+buttonnih+"</td></tr>";
                            }
                            else
                            {
                                // buttonnih = "<button class='aa btn btn-success btn-block' id='kaka' request_id='"+request_id+"' gh='"+harga_kelas+"' title='Terima'>Terima</button><br><a data-toggle='modal' class='m-t-5' request_id='"+request_id+"' href='#Delete'><button class='aa-t btn btn-danger btn-block' request_id='"+request_id+"' title='Hapus'>Tolak</button></a>";
                                buttonnih = "<button class='btn btn-gray f-16 btn-block' disabled>-</button>";
                                var kotak ="<tr><td><div class='me_timer col-sm-6 col-md-12 col-xs-12'  ><div class='col-md-3'><div class='card picture-list'><div class='card blog-post' style=''><img class='' src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>flag/flag_"+country+".png' style='z-index: 1; position: absolute; margin: 2%; height: 20px; width: 35px; top: 5; right: 0;'><img src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+user_image+"' onerror="+alt_img+"  width='100%' height='150px'></div></div></div><div class='col-md-9'><span class='card-header '><h5 class='c-black' >"+day+", "+date+" - "+time+" <b class='m-l-5'></b>| "+statusKelas+"</h5><hr><h5 class='c-blue'>"+tutor_name+" - "+subject_name+"</h5><h5 class='c-blue' ><?php echo $this->lang->line('topikbelajar'); ?> "+topic+"</h5><h5 class='c-blue'><?php echo $this->lang->line('invitedby'); ?> : "+requester_name+"</h5><h5 class='c-blue'><?php echo $this->lang->line('demandprice'); ?> : "+harga_kelas+"</h5></span></div><div class='col-md-12'></div></div></td></tr>";
                                var kotaks = "<tr><td>"+requester_name+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+date_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td><?php echo $this->lang->line('waitingtutor'); ?></td><td>"+buttonnih+"</td></tr>";
                            }
                        }
                        
                        $("#tempatfriendinvitation").append(kotak);
                        $('.datainvitation').DataTable();
                    }
                }
                // methodpembayaran = data['response']['method_pembayaran'];
                // if (methodpembayaran == "beban_sendiri") 
                // {
                //     successandsave();
                // }else{
                //     ceksaldo();
                // }
            }
        });


        $(document).on("click", ".aa", function () {       
            requestid = $(this).attr("request_id");
            var hargakelas = parseInt($(this).attr("gh"));  
            // alert(hargakelas);
            if (methodpembayaran == "bagi_rata") 
            {                    
                // $.ajax({
                //     url: '<?php echo base_url(); ?>Rest/ceksaldo/access_token/'+tokenjwt,
                //     type: 'POST',
                //     data: {
                //         id_user: iduser
                //     },               
                //     success: function(data)
                //     {             
                //         balanceuser = data['balance'];    
                //         // alert(balanceuser);
                //         // alert(hargakelas);
                //         if (balanceuser >= hargakelas)
                //         {
                            // alert(balanceuser);
                            $.ajax({
                                url: '<?php echo base_url(); ?>/Rest/approve_ondemandgroup/access_token/'+tokenjwt,
                                type: 'POST',
                                data: {
                                    request_id: requestid,
                                    id_user : iduser
                                },               
                                success: function(data)
                                {                    
                                    if (data['status'] == true) 
                                    {
                                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Anda telah mensetujui permintaan group class dari teman anda.");
                                        $(".aa").attr('disabled','true');
                                        $(".aa-t").attr('disabled','true');
                                        setTimeout(function(){
                                            window.location.reload();
                                        },3000);
                                        
                                    }
                                }
                            });
                //         }
                //         else
                //         {
                //             notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Saldo anda tidak mencukupi. Mohon untuk mengisi saldo terlebih dahulu.");
                //             $(".aa").attr('disabled','true');
                //             $(".aa-t").attr('disabled','true');
                //             setTimeout(function(){
                //                 window.location.replace('<?php echo base_url(); ?>/Topup');
                //             },3000);
                //         }
                //     }
                // });
            }
            else
            {
                $.ajax({
                    url: '<?php echo base_url(); ?>/Rest/approve_ondemandgroup/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        request_id: requestid,
                        id_user : iduser
                    },               
                    success: function(data)
                    {                    
                        if (data['status'] == true) 
                        {
                            notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Anda telah mensetujui permintaan group class dari teman anda.");
                            $(".aa").attr('disabled','true');
                            $(".aa-t").attr('disabled','true');
                            setTimeout(function(){
                                window.location.reload();
                            },3000);
                            
                        }
                    }
                });
            } 
        });

        $(document).on("click", ".aa-t", function () {       
            // alert($(this).attr("request_id"));
            requestid = $(this).attr("request_id");   
            $.ajax({
                url: '<?php echo base_url(); ?>/Rest/approve_ondemandgroup/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    request_id: requestid,
                    id_user : iduser,
                    answer: "t"

                },               
                success: function(data)
                {                  

                    if (data['status'] == true) 
                    {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Anda telah menolak permintaan group class ini");
                        setTimeout(function(){
                            window.location.reload();
                        },3000);
                    }
                }
            });

        });


        // function ceksaldo()
        // {
        //     $.ajax({
        //         url: '<?php echo base_url(); ?>/Rest/ceksaldo',
        //         type: 'POST',
        //         data: {
        //             id_user: iduser
        //         },               
        //         success: function(data)
        //         { 
        //             balanceuser = data['balance'];
        //             if (parseInt(balanceuser) > parseInt(hargakelas)) 
        //             {
        //                 successandsave();
        //             }
        //             else
        //             {
        //                 notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Saldo anda tidak mencukupi! Mohon isi dahulu saldo anda");                        
        //             }
        //         }
        //     });
        // }

        function successandsave()
        {
            $.ajax({
                url: '<?php echo base_url(); ?>/Rest/approve_ondemandgroup/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    request_id: requestid,
                    id_user : iduser

                },               
                success: function(data)
                {                    
                    if (data['status'] == true) 
                    {
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Anda telah mensetujui permintaan group class dari teman anda.");
                        window.location.reload();
                    }
                }
            });
        }

        $('table.display').DataTable( {
            fixedHeader: {
                header: true,
                footer: true
            }
        } );
        $('table.displayy').DataTable( {
            fixedHeader: {
                header: true,
                footer: true
            }
        } );
        
        $('.data').DataTable();    
    }

    $("#btn_extraclass").click(function(){
        $("#modal_extraclass").modal('show');
        // $("#content_myclass").css('display','none');
        // $("#content_extraclass").css('display','block');
    });
    $(".btn_back_home").click(function(){
        $("#sidebar").css('margin-top','3.5%');
        $("#kalender").css('display','block');
        $("#content_classdescription").css('display','none');
        $("#content_myclass").css('display','block');
        $("#content_extraclass").css('display','none');
    });
    $("#btn_back_group").click(function(){
        $("#modal_groupclass").modal('hide');
        $("#modal_extraclass").modal('show');
    });
    $("#btn_back_search_group").click(function(){
        $("#kotakuntukpencariangroup").css('display','block');
        $("#kotakhasilpencariangroup").css('display','none');
        $("#btn_back_group").css('display','block');
        $("#btn_back_search_group").css('display','none');
        $("#body_group").css('max-height','500px');
    });
    $("#btn_back_private").click(function(){
        $("#modal_privateclass").modal('hide');
        $("#modal_extraclass").modal('show');
    });
    $("#btn_back_search_private").click(function(){
        $("#kotakuntukpencarianprivat").css('display','block');
        $("#kotakhasilpencarianprivat").css('display','none');
        $("#btn_back_private").css('display','block');
        $("#btn_back_search_private").css('display','none');
        $("#body_private").css('max-height','700px');
    });
    $("#privateclick").click(function(){
        $("#body_private").css('max-height','500px');
        $("#body_group").css('max-height','500px');
        $("#kotakuntukpencarianprivat").css('display','block');
        $("#kotakhasilpencarianprivat").css('display','none');
        // $("#sidebar").css('margin-top','0%');
        // $("#kalender").css('display','none');
        $("#modal_extraclass").modal('hide');
        // $("#content_myclass").css('display','none');
        $("#content_classdescription").css('display','none');
        // $("#content_extraclass").css('display','block');
        $("#modal_privateclass").modal('show');
        $("#btn_back_private").css('display','block');
        $("#btn_back_search_private").css('display','none');

        $("#kotaknyagroup").removeClass('bgm-orange');
        $("#kotaknyagroup").addClass('bgm-gray');
        $("#kotaknyaprivate").removeClass('bgm-gray');
        $("#kotaknyaprivate").addClass('bgm-orange');

        // $("#box_groupclass").css('display','none');
        $("#privateclick").css('filter','grayscale();');
        $("#groupklik").css('filter','none');
        // $("#box_privateclass").css('display','block');
    });
    $("#groupclassclick").click(function(){
        $("#kotakuntukpencariangroup").css('display','block');
        $("#kotakhasilpencariangroup").css('display','none');
        $("#sidebar").css('margin-top','0%');
        // $("#kalender").css('display','none');
        $("#modal_extraclass").modal('hide');
        $("#modal_groupclass").modal('show');
        // $("#content_myclass").css('display','none');
        // $("#content_classdescription").css('display','none');
        $("#content_extraclass").css('display','block');
        $("#kotaknyaprivate").removeClass('bgm-orange');
        $("#kotaknyaprivate").addClass('bgm-gray'); 
        $("#kotaknyagroup").removeClass('bgm-gray');
        $("#kotaknyagroup").addClass('bgm-orange');
        // $("#box_privateclass").css('display','none');
        // $("#box_groupclass").css('opacity','100');        
        $("#groupclassclick").css('filter','grayscale();');
        $("#durasigroup").addClass('col-md-12');
        // $("#box_groupclass").css('display','block');
    });
    $('#subject_id').change(function(e){
    $('#subjecton').val($(this).val());
    });
    $('#duration').change(function(e){
        $('#durationon').val($(this).val());
    });
    $('#subject_idg').change(function(e){
        $('#subjectong').val($(this).val());
    });
    $('#durationg').change(function(e){
        $('#durationong').val($(this).val());
    });

    // $("#btn_cari").click(function(){
    //     $("#modal_privateclass").modal('hide');
    // });

    $('#button_search').click(function(){
        // subject_id,dateon,timeeeeeee,duration
        var sbj_id      = $("#subject_id").val();
        var dton        = $("#dateon").val();        
        var drt         = $("#duration").val();        
        if (sbj_id == null) {            
            // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertsubjectnull'); ?>');    
            $("#modal_privateclass").modal('hide');
        }
        else if(dton == ""){
            // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdatenull'); ?>');
            $("#modal_privateclass").modal('hide');
        }
        else if(drt == null){
            // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdurationnull'); ?>');
            $("#modal_privateclass").modal('hide');
        }   
        else
        {   
            $("#kotakhasilpencarianprivat").css('display','block');
            $("#kotakuntukpencarianprivat").css('display','none');
            $("#btn_back_private").css('display','none');
            $("#btn_back_search_private").css('display','block');
            angular.element(document.getElementById('button_search')).scope().getondemandprivate();
        }
    });

    $('#button_searchgroup').click(function(){
        // durationg,timeeeeee,dateong,subject_idg
        var sbj_idg      = $("#subject_idg").val();
        var dtong        = $("#dateong").val();        
        var drtg         = $("#durationg").val();
        if (sbj_idg == null) {            
            // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertsubjectnull'); ?>');    
            $("#modal_groupclass").modal('hide');
        }
        else if(dtong == ""){
            // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdatenull'); ?>');
            $("#modal_groupclass").modal('hide');
        }
        else if(drtg == null){
            // notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut','<?php echo $this->lang->line('alertdurationnull'); ?>');
            $("#modal_groupclass").modal('hide');
        }   
        else
        {
            $("#kotakhasilpencariangroup").css('display','block');
            $("#kotakuntukpencariangroup").css('display','none');
            $("#btn_back_group").css('display','none');
            $("#btn_back_search_group").css('display','block');
            angular.element(document.getElementById('button_search')).scope().getondemandgroup();
        }
    });
</script>
<!-- End Script Extra Class -->
<script type="text/javascript">

</script>
