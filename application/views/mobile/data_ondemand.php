<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="container">
            
            <div class="block-header">
                <h2>Data <?php echo $this->lang->line('ondemand'); ?></h2>
                
            </div> <!-- akhir block header    --> 

            <div class="col-md-12">
                <div class=" m-t-20">
                    <div class="">
                        
                        <h2><?php echo $this->lang->line('transactionlist'); ?> Request anda</h2>
                        <ul class="tab-nav pull-right" role="tablist">                                                                      
                            <li role="presentation" class="pull-right">
                                <a class="col-xs-4" href="#tab-3" id="friendinvitaion" aria-controls="tab-3" role="tab" data-toggle="tab">
                                    Friend invitation
                                </a>
                            </li>
                            <li role="presentation" class="pull-right">
                                <a class="col-xs-4" href="#tab-2" aria-controls="tab-2" role="tab" data-toggle="tab">
                                    Group Class
                                </a>
                            </li>
                            <li role="presentation" class="pull-right active" style="margin-right: 3%;">
                                <a class="col-xs-4" href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab">
                                    Private Class
                                </a>
                            </li> 
                        </ul>
                        <hr style="margin-top: 2%;">
                    </div>


                    <div class="" style="margin-top: -2%;">                   
                        
                        <div class="tab-content p-20">

                            <div role="tabpanel" class="tab-pane animated fadeIn in active" id="tab-1">
                                <div class="card-body card-padding table-responsive">
                                <label class="f-15" style="margin-top: -3%; margin-bottom: 1%;">Detail Private Class</label>
                                <table class="display table table-striped table-bordered data">
                                    <thead>
                                        <tr>
                                            <th class="bgm-teal c-white" style="width: 10px">No</th>
                                            <th class="bgm-teal c-white" style="width: 15px">Nama Tutor</th>
                                            <th class="bgm-teal c-white" style="width: 15px">Mata Pelajaran</th>
                                            <!-- <th class="bgm-teal c-white" style="width: 30px">Metode Pembayaran</th> -->
                                            <!-- <th class="bgm-teal c-white" style="width: 20px">Harga</th> -->
                                            <th class="bgm-teal c-white" style="width: 20px">Jam Permintaan</th>
                                            <th class="bgm-teal c-white" style="width: 20px">Durasi</th>                                        
                                            <th class="bgm-teal c-white" style="width: 20px">Tanggal Permintaan</th>
                                            <th class="bgm-teal c-white" style="width: 20px">Status</th>                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $no = 1;
                                            $user = $this->session->userdata('id_user');
                                            // $query2 = $this->db->query("SELECT lm.payment_type, lm.status_message, lo.* FROM log_midtrans_transaction as lm INNER JOIN log_order as lo ON lm.order_id=lo.order_id WHERE lo.id_user='$user' ORDER BY lo.timestamp DESC")->result_array();
                                            $query2 = $this->db->query("SELECT tr.*, tu.user_name, ms.subject_name FROM tbl_request as tr INNER JOIN tbl_user as tu ON tr.id_user_requester=tu.id_user INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tr.id_user_requester = '$user' ORDER BY datetime DESC")->result_array();
                                            if (empty($query2)) {
                                                ?>
                                            <tr>
                                                <td colspan='7'><center>Tidak ada tutor yang tersedia</center></td>
                                            </tr>
                                            <?php
                                            }
                                            else
                                            {     
                                                foreach ($query2 as $row => $v) {
                                                    // $hasilkredit = number_format($v['harga_kelas'], 0, ".", ".");
                                                  
                                                    $date = date_create($v['date_requested']);
                                                    $datee = date_format($date, 'd/m/y');
                                                    $hari = date_format($date, 'd');
                                                    $tahun = date_format($date, 'Y');
                                                                                            
                                                    $date = $datee;
                                                    $sepparator = '/';
                                                    $parts = explode($sepparator, $date);
                                                    $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));

                                                    // $j = sprintf('%02d',$v['time_requested']/3600);
                                                    // $m = sprintf('%02d',($v['time_requested']%3600)/60);
                                                    $timerequest = substr($v['date_requested'], 11);
                                                    $duration = sprintf('%02d',$v['duration_requested']/60);

                                                    // $pizza  = $v['id_friends'];
                                                    // $kosong = "";
                                                    // $pieces = explode(",", $v['id_friends']);
                                                    // foreach ($pieces as  $value) {
                                                    //     // echo $value." ";
                                                    //     $aa = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$value'")->row_array();
                                                    //     // echo $aa['user_name']." <br>";

                                                    //     $kosong .= "- ".$aa['user_name'].",<br>";
                                                    // }

                                            ?>
                                            <tr>
                                                <td><?php echo($no); ?></td>
                                                <td><?php echo $v['user_name'];?></td>
                                                <td><?php echo $v['subject_name'];?></td>
                                                <!-- <td>
                                                    <?php 
                                                        if ($v['method_pembayaran'] == 'bayar_sendiri')
                                                        {
                                                            echo "<label>Bayar Sendiri</label>"; 
                                                        }
                                                        else
                                                        {
                                                            echo "<label>Bagi Rata</label>";
                                                        }    
                                                    ?>
                                                </td> -->
                                                <!-- <td>Rp. <?php echo($hasilkredit); ?></td> -->
                                                <td><?php echo $timerequest;?></td>
                                                <td><?php echo $duration." Menit"?></td>                                            
                                                <td><?php echo $hari.' '. $bulan. ' '.$tahun ?></td>
                                                <td>
                                                    <?php 
                                                        if ($v['approve'] == 0)
                                                        {
                                                            echo "<label class='c-red'>Belum disetujui</label>"; 
                                                        }
                                                        else
                                                        {
                                                            echo "<label class='c-green'>Telah disetujui</label>";
                                                        }    
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                                $no++;
                                                }
                                                 
                                            } 
                                            ?>    
                                    </tbody>
                                </table>
                              </div>  
                            </div>
                            
                            <div role="tabpanel" class="tab-pane animated fadeIn in" id="tab-2">
                                <div class="card-body card-padding table-responsive">
                                <label class="f-15" style="margin-top: -3%; margin-bottom: 1%;">Detail Group Class</label>
                                <table class="displayy table table-striped table-bordered dataa">
                                    <thead>
                                        <tr>
                                            <th class="bgm-teal c-white" style="width: 10px">No</th>
                                            <th class="bgm-teal c-white" style="width: 15px">Nama Tutor</th>
                                            <th class="bgm-teal c-white" style="width: 15px">Mata Pelajaran</th>
                                            <th class="bgm-teal c-white" style="width: 30px">Metode Pembayaran</th>
                                            <th class="bgm-teal c-white" style="width: 20px">Harga</th>
                                            <th class="bgm-teal c-white" style="width: 20px">Jam Permintaan</th>
                                            <th class="bgm-teal c-white" style="width: 20px">Durasi</th>
                                            <th class="bgm-teal c-white" style="width: 20px">Teman Group</th>
                                            <th class="bgm-teal c-white" style="width: 20px">Tanggal Permintaan</th>
                                            <th class="bgm-teal c-white" style="width: 20px">Status</th>
                                            <!-- <th class="bgm-teal c-white" style="width: 20px">#</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $no = 1;
                                            $user = $this->session->userdata('id_user');
                                            // $query2 = $this->db->query("SELECT lm.payment_type, lm.status_message, lo.* FROM log_midtrans_transaction as lm INNER JOIN log_order as lo ON lm.order_id=lo.order_id WHERE lo.id_user='$user' ORDER BY lo.timestamp DESC")->result_array();
                                            $query2 = $this->db->query("SELECT trg.*, tu.user_name, ms.subject_name FROM tbl_request_grup as trg INNER JOIN tbl_user as tu ON trg.tutor_id=tu.id_user INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id WHERE trg.id_user_requester = '$user' ORDER BY datetime DESC")->result_array();
                                            if (empty($query2)) {
                                                ?>
                                            <tr>
                                                <td colspan='10'><center>Tidak ada Data</center></td>
                                            </tr>
                                            <?php
                                            }
                                            else
                                            {     
                                                foreach ($query2 as $row => $v) {
                                                    $hasilkredit = number_format($v['harga_kelas'], 0, ".", ".");
                                                  
                                                    $date = date_create($v['date_requested']);
                                                    $datee = date_format($date, 'd/m/y');
                                                    $hari = date_format($date, 'd');
                                                    $tahun = date_format($date, 'Y');
                                                                                            
                                                    $date = $datee;
                                                    $sepparator = '/';
                                                    $parts = explode($sepparator, $date);
                                                    $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));

                                                    $timerequest = substr($v['date_requested'], 11);
                                                    $duration = sprintf('%02d',$v['duration_requested']/60);

                                                    $pizza  = $v['id_friends'];
                                                    $json_a = json_decode($pizza,true);

                                                    $kosong = "";
                                                    $pieces = explode(",", $v['id_friends']);                                              
                                            ?>
                                            <tr>
                                                <td><?php echo($no); ?></td>
                                                <td><?php echo $v['user_name'];?></td>
                                                <td><?php echo $v['subject_name'];?></td>
                                                <td>
                                                    <?php 
                                                        if ($v['method_pembayaran'] == 'beban_sendiri')
                                                        {
                                                            echo "<label>Sendiri</label>"; 
                                                        }
                                                        else
                                                        {
                                                            echo "<label>Ramai Ramai</label>";
                                                        }    
                                                    ?>
                                                </td>
                                                <td>Rp. <?php echo($hasilkredit); ?></td>
                                                <td><?php echo $timerequest;?></td>
                                                <td><?php echo $duration." Menit"?></td>
                                                <td><?php foreach($json_a['id_friends'] as $p)
                                                    {
                                                        $s = $p['id_user'];
                                                        $aa = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$s'")->row_array();
                                                        // echo 'Name: '.$p['id_user'].' '.$p['status'].'';
                                                        if ($p['status'] == '0') {
                                                            echo "<label class='c-red'> - ".$aa['user_name']."</label>";
                                                        } else {
                                                            echo "<label class='c-green'> - ".$aa['user_name']."</label>";
                                                        }
                                                    } ?>
                                                </td>
                                                <td><?php echo $hari.' '. $bulan. ' '.$tahun ?></td>
                                                <td>
                                                    <?php 
                                                        // foreach($json_a['id_friends'] as $p)
                                                        // {
                                                        //     $s = $p['id_user'];
                                                        //     $aa = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$s'")->row_array();
                                                        //     // echo 'Name: '.$p['id_user'].' '.$p['status'].'';
                                                        //     if ($p['status'] == '1' && $v['approve'] == 0) {
                                                        //         echo "<label class='c-red'> Belum disetujui</label>";
                                                        //     } else {
                                                        //         echo "<label class='c-green'>Telah disetujui</label>";
                                                        //     }
                                                        // }
                                                        if ($v['approve'] == 0)
                                                        {
                                                            echo "<label class='c-red'>Menunggu disetujui oleh tutor</label>"; 
                                                        }
                                                        else
                                                        {
                                                            echo "<label class='c-green'>Telah disetujui</label>";
                                                        }    
                                                    ?>
                                                </td>
                                                <!-- <td></td> -->
                                            </tr> 
                                            <?php
                                            $no++;
                                            }
                                             
                                        } 
                                        ?>                                  
                                    </tbody>
                                </table>
                              </div>  
                            </div>

                            <div role="tabpanel" class="tab-pane animated fadeIn in" id="tab-3">
                                <div class="card-body card-padding table-responsive">
                                <label class="f-15" style="margin-top: -3%; margin-bottom: 1%;">Detail Friend Invite</label>
                                <table class="displayyy table table-striped table-bordered dataaa">
                                    <thead>
                                        <tr>
                                            <th class="bgm-teal c-white" style="width: 10px">No</th>
                                            <th class="bgm-teal c-white" style="width: 20px">Teman Pengajak</th>
                                            <th class="bgm-teal c-white" style="width: 15px">Nama Tutor</th>
                                            <th class="bgm-teal c-white" style="width: 15px">Mata Pelajaran</th>
                                            <th class="bgm-teal c-white" style="width: 30px">Metode Pembayaran</th>
                                            <th class="bgm-teal c-white" style="width: 20px">Harga</th>
                                            <th class="bgm-teal c-white" style="width: 20px">Jam Permintaan</th>
                                            <th class="bgm-teal c-white" style="width: 20px">Durasi</th>                                            
                                            <th class="bgm-teal c-white" style="width: 20px">Tanggal Permintaan</th>
                                            <th class="bgm-teal c-white" style="width: 300px">Status</th>
                                            <th class="bgm-teal c-white text-center" style="width: 20px">#</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tempatfriendinvitation">
                                                                
                                    </tbody>
                                </table>
                              </div>  
                            </div>                        

                        </div>
                    </div>   
                </div>            
            </div>            

    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<!-- <div class="page-loader bgm-blue">
    <div class="preloader pls-white pl-xxl">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p class="c-white f-14">Please wait...</p>
    </div>
</div> -->



<script type="text/javascript">    
    $(document).ready(function() {

        var methodpembayaran = null;
        var hargakelas = null;
        var datetime = null;
        var iduser = "<?php echo $this->session->userdata('id_user'); ?>";
        var balanceuser = null;
        var requestid = null;
        var user = "<?php echo $this->session->userdata('id_user'); ?>";        
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";

        $.ajax({
            url: 'https://classmiles.com/Rest/get_friendinvitation/access_token/'+tokenjwt,
            type: 'POST',   
            data: {
                id_user: user
            },             
            success: function(data)
            {                   
                var a = JSON.stringify(data['response']);
                var b = JSON.parse(a);
                // hargakelas = data['data']['harga_kelas'];
                if (data['code'] == 300) {
                    var kosong = "<tr><td colspan='11'><center>Tidak ada Data</center></td></tr>";
                    $("#tempatfriendinvitation").append(kosong);
                }
                else
                {
                    for (var i = data.response.length-1; i >=0;i--) {
                        var request_id = data['response'][i]['request_id'];
                        var requester_name = data['response'][i]['requester_name'];
                        var tutor_name = data['response'][i]['tutor_name'];
                        var subject_name = data['response'][i]['subject_name'];
                        var method_pembayaran = data['response'][i]['method_pembayaran'];
                        var harga_kelas = data['response'][i]['harga_kelas'];
                        var duration_requested = data['response'][i]['duration_requested'];
                        var date_requested = data['response'][i]['date_requested'];
                        var approve = data['response'][i]['approve'];
                        var buttonnih = null;
                        var cekstatusdia = data['response'][i]['id_friends']['id_friends'];
                        var myStatus = null;
                        methodpembayaran = method_pembayaran;
                        if (method_pembayaran=="bayar_sendiri") {
                            harga_kelas = "Rp. 0";
                            method_pembayaran = "Bayar Sendiri";
                        }
                        else
                        {
                            method_pembayaran = "Bagi Rata";
                        }
                        for (var z = cekstatusdia.length - 1; z >= 0; z--) {
                            if (cekstatusdia[z]['id_user'] == iduser) 
                            {
                                myStatus = cekstatusdia[z]['status'];
                            }
                        }
                        // alert(JSON.stringify(cekstatusdia));
                        var no = (data.response.length-i)+1; 
                        if (myStatus == 0) {
                            buttonnih = "<button class='aa btn btn-success btn-block' id='kaka' request_id='"+request_id+"' gh='"+harga_kelas+"' title='Terima'>Terima</button><br><a data-toggle='modal' class='m-t-5' request_id='"+request_id+"' href='#Delete'><button class='aa-t btn btn-danger btn-block' request_id='"+request_id+"' title='Hapus'>Tolak</button></a>";
                            var kotak = "<tr><td>"+no+"</td><td>"+requester_name+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+harga_kelas+"</td><td>"+date_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td>Menunggu Persetujuan Teman</td><td>"+buttonnih+"</td></tr>";
                        }   
                        else{                    
                            if (approve == 1) {
                                buttonnih = "<button class='btn btn-gray f-16 btn-block' disabled>-</button>";
                                var kotak = "<tr><td>"+no+"</td><td>"+requester_name+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+harga_kelas+"</td><td>"+date_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td>Permintaan Kelas sudah di setujui oleh tutor. Jadwal kelas sudah di <a href='https://classmiles.com'>My Class</a></td><td>"+buttonnih+"</td></tr>";
                            }
                            else
                            {
                                // buttonnih = "<button class='aa btn btn-success btn-block' id='kaka' request_id='"+request_id+"' gh='"+harga_kelas+"' title='Terima'>Terima</button><br><a data-toggle='modal' class='m-t-5' request_id='"+request_id+"' href='#Delete'><button class='aa-t btn btn-danger btn-block' request_id='"+request_id+"' title='Hapus'>Tolak</button></a>";
                                buttonnih = "<button class='btn btn-gray f-16 btn-block' disabled>-</button>";
                                var kotak = "<tr><td>"+no+"</td><td>"+requester_name+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+harga_kelas+"</td><td>"+date_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td>Permintaan Sudah diterima oleh tutor, tunggu hingga tutor kami Menerima.</td><td>"+buttonnih+"</td></tr>";
                            }
                        }
                        
                        $("#tempatfriendinvitation").append(kotak);
                    }
                }
                // methodpembayaran = data['response']['method_pembayaran'];
                // if (methodpembayaran == "beban_sendiri") 
                // {
                //     successandsave();
                // }else{
                //     ceksaldo();
                // }
            }
        });


        $(document).on("click", ".aa", function () {       
            requestid = $(this).attr("request_id");
            var hargakelas = parseInt($(this).attr("gh"));  
            // alert(hargakelas);
            if (methodpembayaran == "bagi_rata") 
            {                    
                $.ajax({
                    url: '<?php echo base_url(); ?>Rest/ceksaldo/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        id_user: iduser
                    },               
                    success: function(data)
                    {             
                        balanceuser = data['balance'];
                        
                        if (balanceuser >= hargakelas)
                        {
                            // alert(balanceuser);
                            $.ajax({
                                url: 'https://classmiles.com/Rest/approve_ondemandgroup',
                                type: 'POST',
                                data: {
                                    request_id: requestid,
                                    id_user : iduser
                                },               
                                success: function(data)
                                {                    
                                    if (data['status'] == true) 
                                    {
                                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Anda telah mensetujui permintaan group class dari teman anda.");
                                        setTimeout(function(){
                                            window.location.reload();
                                        },3000);
                                        
                                    }
                                }
                            });
                        }
                        else
                        {
                            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Saldo anda tidak mencukupi. Mohon untuk mengisi saldo terlebih dahulu.");
                            setTimeout(function(){
                                window.location.replace('<?php echo base_url(); ?>/Topup');
                            },3000);
                        }
                    }
                });
            }
            else
            {
                $.ajax({
                    url: 'https://classmiles.com/Rest/approve_ondemandgroup/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        request_id: requestid,
                        id_user : iduser
                    },               
                    success: function(data)
                    {                    
                        if (data['status'] == true) 
                        {
                            notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Anda telah mensetujui permintaan group class dari teman anda.");
                            setTimeout(function(){
                                window.location.reload();
                            },3000);
                            
                        }
                    }
                });
            } 
        });

        $(document).on("click", ".aa-t", function () {       
            // alert($(this).attr("request_id"));
            requestid = $(this).attr("request_id");            
            $.ajax({
                url: 'https://classmiles.com/Rest/approve_ondemandgroup/t/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    request_id: requestid,
                    id_user : iduser

                },               
                success: function(data)
                {                  

                    if (data['status'] == true) 
                    {
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Anda telah mensetujui permintaan group class dari teman anda.");
                        setTimeout(function(){
                            window.location.reload();
                        },3000);
                    }
                }
            });

        });


        // function ceksaldo()
        // {
        //     $.ajax({
        //         url: 'https://classmiles.com/Rest/ceksaldo',
        //         type: 'POST',
        //         data: {
        //             id_user: iduser
        //         },               
        //         success: function(data)
        //         { 
        //             balanceuser = data['balance'];
        //             if (parseInt(balanceuser) > parseInt(hargakelas)) 
        //             {
        //                 successandsave();
        //             }
        //             else
        //             {
        //                 notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Saldo anda tidak mencukupi! Mohon isi dahulu saldo anda");                        
        //             }
        //         }
        //     });
        // }

        function successandsave()
        {
            $.ajax({
                url: 'https://classmiles.com/Rest/approve_ondemandgroup/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    request_id: requestid,
                    id_user : iduser

                },               
                success: function(data)
                {                    
                    if (data['status'] == true) 
                    {
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Anda telah mensetujui permintaan group class dari teman anda.");
                        window.location.reload();
                    }
                }
            });
        }

        $('table.display').DataTable( {
            fixedHeader: {
                header: true,
                footer: true
            }
        } );
        $('table.displayy').DataTable( {
            fixedHeader: {
                header: true,
                footer: true
            }
        } );
        
        $('.data').DataTable();
        $('.dataaa').DataTable();
              
    });
</script>
    