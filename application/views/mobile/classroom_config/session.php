<div id="loader">
    <div class="page-loader" style="background-color:#1274b3;">
        <div class="preloader pl-xl pls-white">
            <svg class="pl-circular" viewBox="25 25 50 50">
                <circle class="plc-path" cx="50" cy="50" r="20" />
            </svg>
            <p>Classmiles. . .</p>
        </div>
    </div>
</div>
<script type="text/javascript">
 	$(document).ready(function(){
 		var token = "<?php echo $access_session; ?>";
                
        if (token != "") 
        {
            cekdatabro();
        }
        else
        {
            console.warn("kosong bro");
        }
        function cekdatabro()
        {
            function checkdata(){          
                $.ajax({   
                    url: "https://classmiles.com/Rest/get_token",
                    type: "POST",
                    data : {
                        session : token                    
                    },                  
                    success: function(data){                               
                        if (data['error']['code'] == '200') {
                            
                            var usertype = data['response']['usertype'];
                            var st_tour = data['response']['st_tour'];
                            var iduser = data['response']['id_user'];
                            window.localStorage.setItem('usertype', usertype);
                            window.localStorage.setItem('st_tour', st_tour);
                            window.localStorage.setItem('iduser', iduser);
                            setTimeout(function(){
                                window.localStorage.setItem('session', token);
                                window.location.replace("https://classmiles.com/classroom/room_tester");
                            },500);
                        }
                        else
                        {
                            checkdata();                       
                        }                         
                    }
                });
            }
            checkdata();
		}
	});    
</script>