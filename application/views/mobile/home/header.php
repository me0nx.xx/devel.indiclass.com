<!DOCTYPE html>
<?php
header('Access-Control-Allow-Origin: *'); 
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="Layanan dan konten belajar dengan teknologi online interaktif (tatap muka jarak jauh). Menghubungkan guru/tutor dan siswa dimanapun dan kapanpun."/>
    <meta name="keywords" content="
Les Online, Les Privat, Les Jarak Jauh, Les Mobile, Bimbel Online, Bimbel Privat, Bimbel Jarak Jauh, Bimbel Mobile, Aplikasi Belajar Online, Kelas Online, Belajar Online, Belajar Online Interaktif, Belajar Dimana Saja, Belajar Kapan Saja, Belajar Itu Menyenangkan, Belajar Bersama, Kelas Online, Belajar Kelompok, Kelas Privat, Kelas Jarak Jauh, Kelas Interaktif, Kelas Dalam Genggaman, Kelas Live, Guru Privat, Guru Les, Grup Kelas"/>
    <meta name="author" content="Indiclass.id"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="google-signin-client_id" content="850067769597-u7eaknsmcjfb2bc80lvfgt5nh71p7kuc.apps.googleusercontent.com">
    <meta name="theme-color" content="#ea2127">    
    <title>Indiclass</title>
    <!-- <meta name="description" content="Material Style Theme"> -->
    <link rel="icon" href="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL;?> aset/img/cm.ico">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/preload.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/plugins.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/style.light-blue-500.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>aset/landing_page/css/width-boxed.min.css" id="ms-boxed" disabled="">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="https://apis.google.com/js/api:client.js"></script>
    <script type="text/javascript">
      var startApp = function() {
          gapi.load('auth2', function(){
            // Retrieve the singleton for the GoogleAuth library and set up the client.
            auth2 = gapi.auth2.init({
              client_id: '38717113134-d2hpda3q6ttec690s17sk50huqf68jtm.apps.googleusercontent.com',
              cookiepolicy: 'single_host_origin',
              // Request scopes in addition to 'profile' and 'email'
              //scope: 'additional_scope'
            });
            attachSignin(document.getElementById('customBtn'));
          });
      };
      function onSignIn(googleUser) {
          // var profile = googleUser.getBasicProfile();
          // console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
          // console.log('Name: ' + profile.getName());
          // console.log('Image URL: ' + profile.getImageUrl());
          // console.log('Email: ' + profile.getEmail());
          // $('#google_data').html(JSON.stringify(profile));
          // $('#google_form').submit();
          // attachSignin(document.getElementById('customBtn'));
           // This is null if the 'email' scope is not present.
      }
      function attachSignin(element) {
          console.log(element.id);
          auth2.attachClickHandler(element, {},
              function(googleUser) {              
                    // googleUser.getBasicProfile().getName();
                    var profile = googleUser.getBasicProfile();                    
                    $('#google_data').html(JSON.stringify(profile));
                    // alert(JSON.stringify(profile));
                    $('#google_form').submit();
              }, function(error) {
                // alert(JSON.stringify(error, undefined, 2));
              });
        }
      function signOut() {
          var auth2 = gapi.auth2.getAuthInstance();
          auth2.signOut().then(function () {
          console.log('User signed out.');
          });
          FB.logout(function(response) {
          alert(JSON.stringify(response));
          });
      }
      function onLoad() {
        gapi.load('auth2', function() {
          auth2 = gapi.auth2.init({
            client_id: '38717113134-d2hpda3q6ttec690s17sk50huqf68jtm.apps.googleusercontent.com',
              // scope: 'profile email'
            });
        });
      }
    </script>
    <!--Start of Tawk.to Script-->
      <script type="text/javascript">
      var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
      (function(){
      var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
      s1.async=true;
      s1.src='https://embed.tawk.to/5a697039d7591465c70712a2/default';
      s1.charset='UTF-8';
      s1.setAttribute('crossorigin','*');
      s0.parentNode.insertBefore(s1,s0);
      })();
      </script>
    <!--End of Tawk.to Script-->
 
  </head>

  
  <style type="text/css">
      /* scroller browser */
      ::-webkit-scrollbar {
          width: 5px;
      }

      /* Track */
      ::-webkit-scrollbar-track {
          -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
          -webkit-border-radius: 7px;
          border-radius: 7px;
      }

      /* Handle */
      ::-webkit-scrollbar-thumb {
          -webkit-border-radius: 7px;
          border-radius: 7px;
          background: #a6a5a5;
          -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
      }
      ::-webkit-scrollbar-thumb:window-inactive {
          background: rgba(0,0,0,0.4); 
      }
  </style>
  <body>
    <script src="<?php echo base_url(); ?>aset/vendors/bower_components/jquery/dist/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="<?php echo base_url(); ?>aset/landing_page/js/plugins.min.js"></script>
    <script src="<?php echo base_url(); ?>aset/landing_page/js/app.min.js"></script>
     <!-- <script src="<?php echo base_url(); ?>aset/sel2/js/select2.js"></script> -->
     <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <div id="fb-root"></div>

  <script type="text/javascript">

    window.fbAsyncInit = function() {
      FB.init({
        appId      : '1647513202217309',
        xfbml      : true,
        version    : 'v2.8'
      });
      FB.AppEvents.logPageView();
      FB.getLoginStatus(function (response) {

      })
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));

    // function onSignIn(googleUser) {
    //   var profile = googleUser.getBasicProfile();
    //   alert(profile.getName());
    //       console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    //       console.log('Name: ' + profile.getName());
    //       console.log('Image URL: ' + profile.getImageUrl());
    //       console.log('Email: ' + profile.getEmail());
    //     }
    //     function signOut() {
    //       var auth2 = gapi.auth2.getAuthInstance();
    //       auth2.signOut().then(function () {
    //         console.log('User signed out.');
    //       });
    //       FB.logout(function(response) {
    //         alert(JSON.stringify(response));
    //       });
    //     }
    //     function onLoad() {
    //       gapi.load('auth2', function() {
    //         auth2 = gapi.auth2.init({
    //           client_id: '850067769597-u7eaknsmcjfb2bc80lvfgt5nh71p7kuc.apps.googleusercontent.com',
    //             // scope: 'profile email'
    //           });
    //       });
    //     }

  </script>