<div id="ms-preload" class="ms-preload">
    <div id="status">
      <div class="spinner">
          <div class="dot1"></div>
          <div class="dot2"></div>
      </div>
    </div>
</div>
<div class="sb-site-container">
    <!-- Modal -->
    <!-- <header id="nav1" class="ms-header ms-header-white" style="background: url('../aset/img/headers/awanback.jpg');"> -->
    <!-- <header id="nav1" class="ms-header ms-header-white" style="background: url('../aset/img/headers/patern1.jpg');"> -->
    <header id="nav1" class="ms-header ms-header-white" style="background: url('../aset/img/headers/backgroundanimation.jpg'); ">
        <?php 
        $this->load->view('home/topnavbar.php');
        ?>
    </header>

    <nav id="nav2" class="navbar navbar-static-top yamm ms-navbar ms-navbar-primary" style="margin: 0; padding: 0;">
        <?php 
            $this->load->view('home/nav_m.php');
        ?>    
    </nav>

    <?php $this->load->view('home/menu/one.php'); ?>

    <section id="fitur">
        <div class="mt-4 color-dark text-center">
        <h4 class="text-center color-primary mb-2 wow fadeInDown animation-delay-4 ml-5 mr-5">Fitur Belajar yang lengkap dan Berbagai tutor yang menyenangkan</h4>
        <p class="lead text-center aco wow fadeInDown animation-delay-5 mw-800 center-block mb-4" style="font-size: 12px;"> Classmiles mempunyai berbagai fitur untuk menunjang belajar dengan metode les. terdiri dari Classroom , Whiteboard, Screen Share dan Raise Hand</p>

        <div class="row">
          <div class="col-md-12">
            <div class="col-lg-3 col-md-3 col-sm-3" style="width: 50%; float: left;" data-toggle="modal" data-target="#myModal1">
              <div class="card card-info card-block text-center wow zoomInUp animation-delay-2">
                  <span class="ms-icon ms-icon-circle ms-icon-xlg color-info">
                      <i class="fa fa-laptop"></i>
                  </span>                
                  <p class="mt-2 no-mb lead small-caps" style="font-size: 15px;">Classroom</p>
              </div>
            </div>
            
            <div class="modal" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" style="padding-top: 50%;">
                <div class="modal-dialog modal-sm animated zoomIn animated-3x" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
                            <h3 class="modal-title" id="myModalLabel3">Classroom</h3>
                        </div>
                        <div class="modal-body">
                            <p>Memudahkan kamu untuk mengikuti suatu kelas/pembelajaran tanpa harus repot-repot datang ke tempat tersebut.</p>
                        </div>                      
                    </div>
                </div>
            </div>


            <div class="col-lg-3 col-md-3 col-sm-3" style="width: 50%; float: left;">
              <div class="card card-danger card-block text-center wow zoomInUp animation-delay-3" data-toggle="modal" data-target="#myModal2">
                <span class="ms-icon ms-icon-circle ms-icon-xlg color-danger">
                      <i class="fa fa-window-maximize"></i>
                  </span>
                <p class="mt-2 no-mb lead small-caps" style="font-size: 15px;">Whiteboard</p>
              </div>
            </div>
            <div class="modal" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" style="padding-top: 50%;">
                <div class="modal-dialog modal-sm animated zoomIn animated-3x" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
                            <h3 class="modal-title" id="myModalLabel3">Whiteboard</h3>
                        </div>
                        <div class="modal-body">
                            <p>Kegunaannya sama seperti papan tulis pada umumnya, namun inilah salah satu kelebihan Classmiles. Dengan ini belajar jadi lebih interaktif dan lebih mudah.</p>
                        </div>                      
                    </div>
                </div>
            </div>
          </div>

          <div class="col-md-12">
              <div class="col-lg-3 col-md-3 col-sm-3" style="width: 50%; float: left;">
                <div class="card card-warning card-block text-center wow zoomInUp animation-delay-5" data-toggle="modal" data-target="#myModal3">
                  <span class="ms-icon ms-icon-circle ms-icon-xlg color-warning">
                    <i class="fa fa-file-video-o"></i>
                  </span>
                  <p class="mt-2 no-mb lead small-caps" style="font-size: 15px;">Screen Share</p>
                </div>
              </div>
              <div class="modal" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" style="padding-top: 50%;">
                <div class="modal-dialog modal-sm animated zoomIn animated-3x" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
                            <h3 class="modal-title" id="myModalLabel3">Screen Share</h3>
                        </div>
                        <div class="modal-body">
                            <p>Fitur ini berfungsi untuk membagi tampilan layar dari sisi tutor untuk lebih memudahkan dalam penyampaian pelajaran.</p>
                        </div>                      
                    </div>
                  </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3" style="width: 50%; float: left;">
                <div class="card card-success card-block text-center wow zoomInUp animation-delay-6" data-toggle="modal" data-target="#myModal4">
                  <span class="ms-icon ms-icon-circle ms-icon-xlg color-success">
                    <i class="fa fa-hand-pointer-o"></i>
                  </span>
                  <p class="mt-2 no-mb lead small-caps" style="font-size: 15px;">Raise Hand</p>
                </div>
              </div>
              <div class="modal" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel4" style="padding-top: 50%;">
                <div class="modal-dialog modal-sm animated zoomIn animated-3x" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
                            <h3 class="modal-title" id="myModalLabel3">Raise Hand</h3>
                        </div>
                        <div class="modal-body">
                            <p>Belajar dengan bertatap muka jadi lebih praktis. cukup gunakan komputer atau gadgetmu lalu buka Classmiles.com, dimanapun-kapanpun.</p>
                        </div>                      
                    </div>
                  </div>
              </div>
          </div>
        </div>
    </section>

    <section class="wrap ms-hero-page ms-hero-bg-info ms-bg-fixed color-white mt-2" style="background: url('../aset/img/headers/1.png'); background-repeat: no-repeat; background-size: cover; background-attachment:fixed;">
        <div class="container">

          <h2 class="text-center fw-500 wow fadeInDown animation-delay-2 mb-2" style="font-size: 15px;">Unduh aplikasi Classmiles sekarang juga. Gratis!</h2>          
          <!-- <div class="row"> -->                      
            <div class="col-md-12">
                <div class="col-md-6" style="float: left; margin-left: 1%; background-color: #f59e2b;">
                    <div class="text-center wow animated flipInX animation-delay-4">
                      <i class="fa fa-android"></i>
                      <span>Download for </span>
                      <br>
                      <strong>Android</strong>
                    </div>
                </div>
                <div class="col-md-6" style="float: right; margin-right: 1%; background-color: #3f53a0;">
                    <div class="text-center wow animated flipInX animation-delay-6">
                      <i class="fa fa-apple"></i>
                      <span>Coming soon </span>
                      <br>
                      <strong>App Store</strong>
                    </div>  
                </div>
            </div>            
            <!-- <div class="col-md-12" style="background-color: black; width: 100%;">            
            <center>
              <div class="col-lg-6 col-md-6 col-sm-6" style="margin-right:1%; background-color: #f59e2b; float: left;">
                
                <div class="text-center wow animated flipInX animation-delay-4">
                  <i class="fa fa-android"></i>
                  <span>Download for </span>
                  <br>
                  <strong>Android</strong>
                </div>
                
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6" style="margin-left:1%; background-color: #3f53a0; float: left;">                
                <div class="text-center wow animated flipInX animation-delay-6">
                  <i class="fa fa-apple"></i>
                  <span>Coming soon </span>
                  <br>
                  <strong>App Store</strong>
                </div>                
              </div>
              </center>
            </div> -->
                    
          <!-- </div> -->
        </div>
    </section>

    <section id="fiturr">
      <div class="container mt-4">
        <div class="text-center">
          <h2 class="color-primary" style="font-size: 18px;">Dapat diakses
            <span class="text-normal"  style="font-size: 16px;">dimana saja</span> dan <span class="text-normal">kapan saja</span>.
          </h2>
        </div>
        <div class="mw-800 center-block">
          <ul class="nav nav-tabs nav-tabs-transparent indicator-primary nav-tabs-full nav-tabs-3" role="tablist">
            <li class="wow fadeInDown animation-delay-6 active" role="presentation">
              <a href="#windows" aria-controls="windows" role="tab" data-toggle="tab" class="withoutripple">
                <i class="zmdi zmdi-windows"></i>
                <span class="hidden-xs">Windows</span>
              </a>
            </li>
            <li class="wow fadeInDown animation-delay-4" role="presentation">
              <a href="#macos" aria-controls="macos" role="tab" data-toggle="tab" class="withoutripple">
                <i class="zmdi zmdi-apple"></i>
                <span class="hidden-xs">IOS</span>
              </a>
            </li>
            <li class="wow fadeInDown animation-delay-2" role="presentation">
              <a href="#android" aria-controls="android" role="tab" data-toggle="tab" class="withoutripple">
                <i class="fa fa-android"></i>
                <span class="hidden-xs">Android</span>
              </a>
            </li>
          </ul>
        </div>
        <div class="panel-body">
          <!-- Tab panes -->
          <div class="tab-content mt-4 ">
            <div role="tabpanel" class="tab-pane active in fade" id="windows">
              <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8 col-lg-7">
                    <img class="img-responsive animated fadeInRight animation-delay-3" src="<?php echo base_url(); ?>aset/images/screen/windows.png"> 
                </div>
                <div class="col-md-2"></div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="macos">
              <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-9 col-lg-9">
                    <img class="img-responsive animated fadeInLeft animation-delay-3" src="<?php echo base_url(); ?>aset/img/apple.png"> 
                </div>
                <div class="col-md-1"></div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="android"> 
              <div class="col-md-2"></div>     
              <div class="col-md-8 col-lg-8" style="margin-left: 2%;">
              <center>
                  <img class="img-responsive animated fadeInRight animation-delay-3" src="<?php echo base_url(); ?>aset/images/screen/android-media.png">
                  </center>
              </div>
              <div class="col-md-1"></div>
            </div>
          </div>
        </div>        
      </div>
    </section>

    <!-- <section id="faq" class="wrap ms-bg-fixed color-white" style="background-color:#5dc1ef;" >
      <div class="container">
        <div class="row">
          <div class="text-center mb-4" style="margin-top: -7%;">
            <h3>FAQ<br>
              <span class="text-normal" style="font-size: 18px;">Pertanyaan yang sering di ajukan</span></h3>
          </div>          
          <div class="col-md-2"></div>
          <div class="col-md-8">
            <div class="panel-group ms-collapse" id="accordion2" role="tablist" aria-multiselectable="true">
              <div class="panel panel-primary wow fadeInUp animation-delay-2">
                <div class="panel-heading" role="tab" id="headingOne2">
                  <h4 class="panel-title">
                    <a class="withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne2">
                      <i class="fa fa-lightbulb-o"></i> Apa itu Classmiles ? </a>
                  </h4>
                </div>
                <div id="collapseOne2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne2">
                  <div class="panel-body color-dark in">
                    <p>Classmiles merupakan situs penyedia jasa layanan pembelajaran online dimana di dalamnya berisi banyak ruang kelas virtual untuk siswa yaitu: SD (kelas 5-6), SMP (kelas 7-9), dan SMA (kelas 10-12). Classmiles juga menyediakan kursus umum seperti bahasa Inggris, Jepang, Arab, Memasak, yoga, tari dan lain sebagainya. Classmiles memungkinkan komunikasi interaktif antara guru dan siswa serta antar siswa.</p>
                  </div>
                </div>
              </div>
              <div class="panel panel-primary wow fadeInUp animation-delay-5">
                <div class="panel-heading" role="tab" id="headingTwo2">
                  <h4 class="panel-title">
                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                      <i class="fa fa-desktop"></i> Bagaimana saya bisa memulai kelas di Classmiles </a>
                  </h4>
                </div>
                <div id="collapseTwo2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo2">
                  <div class="panel-body color-dark">
                    <p>
                      Sangat mudah untuk dapat memulai kelas dalam classmiles. Anda hanya perlu mendaftar/membuat akun di classmiles atau bisa juga menggunakan akun facebook dan google yang telah anda miliki sebelumnya lalu lengkapilah data diri anda.
                    </p>
                  </div>
                </div>
              </div>
              <div class="panel panel-primary wow fadeInUp animation-delay-7">
                <div class="panel-heading" role="tab" id="headingThree3">
                  <h4 class="panel-title">
                    <a class="collapsed withripple" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree2" aria-expanded="false" aria-controls="collapseThree2">
                      <i class="fa fa-user"></i> Saya sudah mendaftar, tetapi kenapa tidak ada kelas yang saya bisa coba ? </a>
                  </h4>
                </div>
                <div id="collapseThree2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree2">
                  <div class="panel-body color-dark">
                    <p>Perlu diingat anda harus melengkapi data diri sesuai dengan kolom yang dibutuhkan, utamanya kolom jenjang pendidikan dan tingkatan kelas. Bila keduanya telah terisi, maka dapat dipastikan anda sudah bisa mengikuti kelas.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-2"></div>
        </div>
      </div>
    </section> -->

    <aside class="" id="contact" style="background-color: #E0E0E0">
        <div class="container">
          <div class="row">

            <div class="col-md-1 ms-footer-col" style="margin-top: 2%;margin-bottom: 2.5%;">
             
            </div>

            <div class="col-md-1"></div>
            <div class="col-md-8 ms-footer-col" style="margin-top: 2%; margin-bottom: 2.5%; margin-left: 0%; margin-right: 3%;">
              <div class="ms-footbar-block">              
                <h3 class="ms-footbar-title">Tentang Situs</h3>                      
                <address class="no-mb" style="margin-top: 0%;">
                  <p>Classmiles adalah sebuah layanan dari PT. Meetaza Prawira Media. Classmiles merupakan situs penyedia jasa layanan pembelajaran online dimana di dalamnya berisi banyak ruang kelas virtual untuk siswa yaitu: SD (kelas 5-6), SMP (kelas 7-9), dan SMA (kelas 10-12). Classmiles juga menyediakan kursus umum seperti bahasa Inggris, Jepang, Arab, Memasak, yoga, tari dan lain sebagainya. Classmiles memungkinkan komunikasi interaktif antara guru dan siswa serta antar siswa.</p>
                  <p>
                    <i class="color-danger-light zmdi zmdi-pin mr-1"></i> Jl. Godean Km.15, Klepu Sendangmulyo Minggir Sleman D.I.Yogyakarta 55562 Indonesia </p>
                  <p>
                    <i class="color-info-light zmdi zmdi-email mr-1"></i>
                    <a href="mailto:info@classmiles.com">info@classmiles.com</a>
                  </p>
                  <p>
                    <i class="color-royal-light zmdi zmdi-phone mr-1"></i>
                    +62-274-2820775, +62-21-22538277
                  </p>          
                </address>
                <hr>
                <p>
                    <a href="<?php echo base_url();?>syarat-ketentuan">
                      Syarat dan Ketentuan
                    </a>
                </p>
                
              </div>      
              </div>
            </div>
            <div class="col-md-2" style="margin-bottom: 2.5%;"></div>


          </div>
        </div>
    </aside>

    <footer class="ms-footer" style="background-color: #6C7A89; color: white;">
        <div class="container">
            <p style="font-size: 13px;">Copyright &copy; Classmiles 2017 <br>PT. Meetaza Prawira Media</p>
        </div>
    </footer>

    <div class="btn-back-top">
        <a href="#" data-scroll id="back-top" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised ">
            <i class="zmdi zmdi-long-arrow-up"></i>
        </a>
    </div>
</div>

<!-- <div class="ms-slidebar sb-slidebar sb-left sb-momentum-scrolling sb-style-overlay">
    <?php
        
    ?>
</div> -->
