<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="10000">   
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
      <li data-target="#carousel-example-generic" data-slide-to="2"></li>      
    </ol>

    <!-- WEB -->
    <div class="carousel-inner" role="listbox" id="sliderheightWeb">
      <div class="item active">
        <img src="<?php echo base_url(); ?>aset/images/screen/banner1.png" id="slider1" alt="...">
      </div>
      <div class="item">
        <img src="<?php echo base_url(); ?>aset/images/screen/banner2.png" id="slider2" alt="...">    
      </div>
      <div class="item">
        <img src="<?php echo base_url(); ?>aset/images/screen/banner3.png" id="slider3" alt="...">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

<script type="text/javascript">
  var a = $("#nav1").height();
  var b = $("#nav2").height();
  var c = $( window ).height();  
  var d = $( window ).width();  
  var heightsliderWeb = c - a - '50';    
  var heightsliderMobile = c - a - '125';
  var e = d * 0.5625;  

  $("#sliderheightMobile").css('display', 'none');
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      $("#sliderheightMobile").css('height', e);
      $("#slider1").css('width', d);
      $("#slider1").css('height', e);
      $("#slider2").css('width', d);
      $("#slider2").css('height', e);
      $("#slider3").css('width', d);
      $("#slider3").css('height', e); 
  }
  else
  {      
      $("#sliderheightWeb").css('height', heightsliderWeb);
      $("#slider1").css('width', '100%');
      $("#slider1").css('height', heightsliderWeb);
      $("#slider2").css('width', '100%');
      $("#slider2").css('height', heightsliderWeb);
      $("#slider3").css('width', '100%');
      $("#slider3").css('height', heightsliderWeb);
  }
  
</script>