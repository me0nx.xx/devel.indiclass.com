<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar');

     ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/sidechannel'); ?>
        <?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
        ?>
    </aside>

    <section id="content">
        <div class="container">
            
            <div class="block-header">
                <h2><?php echo $this->lang->line('homeadmin'); 
                        //echo $this->session->flashdata('msgSuccess');
                ?></h2>

                <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <!-- <li><a href="#"><?php echo $this->lang->line('homeadmin'); ?></a></li> -->
                            <!-- <li class="active"></li> -->
                        </ol>
                    </li>
                </ul>
            </div> <!-- akhir block header    -->
			<center>
                <div class="alert alert-success" style="display: block;">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  Selamat Datang Channel <?php echo $this->session->userdata('user_name'); ?>
                  <!-- <br>
                  <label><?php echo $this->session->userdata('channel_id');?></label> -->
                </div>       
            </center>
        </div>
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
    