<div class="container">

    <div class="col-sm-3">    
    </div>    
    <div class="col-sm-6" style="margin-top:15%;">
        <div class="card" style="border-radius:3%;">
            <div class="lv-header bgm-bluee">
                <div class="c-white f-17 m-t-10 m-b-10" style="font-family: 'Trebuchet MS'; font-style:normal;"><?php echo $this->lang->line('signup'); ?></div>
            </div>
            <div class="card-body" style="margin-left:7%; margin-right:7%;">
               
                    <div class="tab-content">
                        <?php if($this->session->flashdata('mes_alert')){ ?>
                        <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <?php echo $this->session->flashdata('mes_message'); ?>
                        </div>
                        <?php } ?>
      
                            <div class="row">
                            <form role="form" action="<?php echo base_url('Process/add_channel'); ?>" method="post">
                                <!-- <div class="col-sm-12 col-md-12 col-xs-12">                                                    
                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <input type="text" name="id_userr" required class="input-sm form-control fg-input" value="<?php echo $this->session->userdata('id_user');?>">
                                            <label class="fg-label">ID</label>
                                        </div>
                                    </div>
                                </div>  -->

                                <div class="col-sm-12 col-md-12 col-xs-12">                                                    
                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <input type="text" name="channel_name" required class="input-sm form-control fg-input">
                                            <label class="fg-label">Nama Channel</label>
                                        </div>
                                    </div>
                                </div>                                

                                <div class="col-sm-12 m-t-5">                                                                                           
                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <input type="email" name="channel_email" required class="input-sm form-control fg-input">
                                            <label class="fg-label">Email</label>
                                        </div>
                                    </div>
                                </div>  

                                <div class="col-sm-12 m-t-5">                                                                                           
                                    <div class="form-group fg-float">
                                        <div class="fg-line">                                            
                                            <textarea rows="4" name="channel_address" required class="input-sm form-control fg-input"></textarea>
                                            <label class="fg-label">Alamat</label>
                                        </div>
                                    </div>
                                </div>                                
                                
                                <div class="col-sm-4 m-t-10">
                                    <div class="fg-line">                                                       
                                        <select required name="channel_country_code" class="select2 form-control">
                                            <option disabled selected value=''><?php echo $this->lang->line('countrycode'); ?></option>
                                            <option value="+62">Indonesia +62</option>
                                            <option value="+60">Malaysia +60</option>
                                            <option value="+65">Singapore +65</option>                                        
                                        </select>
                                    </div>
                                    <!-- <span class="zmdi zmdi-flag form-control-feedback"></span> -->
                                </div>
                                <div class="col-sm-8 m-t-10">
                                    <div class="fg-line">
                                        <input type="text" id="no_hape" minlength="9" maxlength="13" name="channel_callnum" required class="form-control " placeholder="No Handphone">
                                    </div>
                                </div>                               
                                
                                <div class="col-sm-12 m-t-20">
                                    <button type="submit" id="submit_signup" class="btn btn-primary btn-block"><?php echo $this->lang->line('btnsignup'); ?></button>
                                </div>


                                <div class="col-sm-12 m-t-20 text-center">
                                    <label class="fg-label f-14 m-r-20"><?php echo $this->lang->line('alreadyaccount'); ?> <a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('here'); ?>
                                    </label></a>
                                    <hr>
                                </div>
                            </form>
                            </div>
                               
                        <!-- akhir student -->                            
                    </div>
                </div>
            
        </div>
    </div>
    <div class="col-sm-3">                                                 
    </div>  

</div><!-- akhir container -->
<!-- </section> -->

<!-- </section> -->
