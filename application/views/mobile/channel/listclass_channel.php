<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sidechannel'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>Daftar Siswa</h2>
                <!-- <ul class="actions hidden-xs">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><?php echo $this->lang->line('accountlist'); ?></li>
                    </ol>                    
                </ul> -->
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="card m-t-20 p-20" style="">
                <div class="card-header">
                    <div class="pull-left"><h2>List Class Channel</h2></div>
                    <!-- <div class="pull-right"><a data-toggle="modal" href="#modaladdstudent"><button class="btn btn-success">+ Tambah Siswa</button></a> -->
                </div>
                <br><br>
                <hr>

                <div class="row" style="margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="list" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <?php
                                    $channel_id = $this->session->userdata('channel_id');
                                    $tutorid = $this->session->userdata('id_user');                                    
                                    $select_all = $this->db->query("SELECT tu.email, tu.user_name, tc.* FROM tbl_user as tu INNER JOIN tbl_class as tc ON tu.id_user=tc.tutor_id WHERE tc.channel_id='$channel_id' AND ( tc.start_time>=now() OR ( tc.start_time<now() AND tc.finish_time>now() ) )")->result_array();
                                    ?>
                                    <tr>
                                        <th data-column-id="no">No</th>
                                        <th data-column-id="name">Tutor Name</th>
                                        <th data-column-id="name">Email Name</th>
                                        <th data-column-id="namee">Subject Class</th>
                                        <th data-column-id="email">Description Class</th>                                        
                                        <th data-column-id="jeniskelamin">Class Type</th>
                                        <th data-column-id="akasi">Template Type</th>
                                        <th data-column-id="alamat">Time Start</th>
                                        <th data-column-id="alamatt">End Class</th>
                                        <th data-column-id="aksi" style="text-align: center;" >Action</th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;
                                    if(empty($select_all)){
                                        ?>
                                        <tr>
                                        <td colspan="8" style="text-align: center;">TIdak ada data</td>
                                        </tr>
                                        <?php
                                    }else
                                    {
                                    foreach ($select_all as $row => $v) {
                                        
                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['user_name']; ?></td>
                                            <td><?php echo $v['email']; ?></td>
                                            <td><?php echo $v['name']; ?></td>
                                            <td><?php echo($v['description']); ?></td>
                                            <td><?php echo($v['class_type']); ?></td>
                                            <td><?php echo($v['template_type']); ?></td>
                                            <td><?php echo($v['start_time']); ?></td>                                            
                                            <td><?php echo($v['finish_time']); ?></td>
                                            <td>
                                            	<a data-toggle="modal" channel_id="<?php echo $v['channel_id']; ?>" rtp="<?php echo $v['class_id']; ?>" class="editclass" data-target-color="green" href="#modalEdit"><button class="btn btn-info" title="Show Student"><i class="zmdi zmdi-accounts-list zmdi-hc-fw"></i></button></a>
                                                <!-- <a rtp="<?php echo $v['class_id']; ?>" class="hapusclass" data-target-color="green" ><button class="btn bgm-red" title="Delete Class"><i class="zmdi zmdi-delete  zmdi-hc-fw"></i></button></a> -->
                                            </td>
                                        </tr>
                                        <?php 
                                        $no++;
                                    }}
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div> 
                                            
            </div> 

            <!-- Modal TAMBAH -->  
            <div class="modal" id="modalEdit" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bgm-cyan">
                            <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                            <h4 class="modal-title c-white">Edit Class</h4>
                        </div>
                        <div class="modal-body m-t-20">
                            
                                <div class="col-md-12" style="padding: 0;"> 
                                    <input type="text" name="channel_id" id="channel_id" hidden>
                                    <input type="text" name="modal_class_id" id="modal_class_id" hidden>
                                    <!-- <input type="text" name="modal_kotak_participant" id="modal_kotak_participant"> -->
                                    <textarea id="modal_kotak_participant" name="modal_kotak_participant" hidden></textarea>
                                	<div class="col-md-12" style="height: 350px; overflow-y: auto;">
                                		<table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
			                                <thead>
			                                    <tr>
			                                        <th data-column-id="no">No</th>
			                                        <th data-column-id="name">Name Student</th>
			                                        <th data-column-id="aksi" rowspan="2" style="text-align: center;" >Action</th>
			                                    </tr>
			                                </thead>
			                                <tbody id="boxlistname">
			                                	
			                                </tbody>
			                            </table>
                                	</div>      
                                	<form id="frmdata">                              
                                    <div class="col-md-12 m-t-20">
                                        <label>Add Student</label><br>
                                        <!-- <input name='ms' class="col-md-8 m-t-5" style="height: 35px;" id="ms"> -->
                                        <div class="col-md-12 m-t-10">                                          
                                            <select name="tagorang[]" channel_id="" id="tagorang" multiple class="select2 form-control" style="width: 100%;">                                                
                                            </select>                                           
                                        </div>
                                    </div>
                                    </form>
                                    <br><br>
                                    <div class="col-md-1"></div>                                                                
                                </div>
                            
                            <br>
                        </div>
                        <div class="modal-footer">                            
                            <button type="button" class="btn btn-success btn-block m-t-15" id="addstudent">Tambah</button>                            
                        </div>
                    </div>
                </div>
            </div>                             

            <!-- Modal DELETE -->  
            <div class="modal fade" id="modalDelete" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Delete Class</h4>
                            <hr>
                            <p><label class="c-gray f-15">Are you sure you want to delete ?</label></p>
                            <!-- <input  type="text" name="classiddd" id="classiddd" class="c-black" value=""/> -->
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" data-id="" id="hapusdataclass" rtp="">Ya</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </section>

</section>
<script type="text/javascript">

    // $('#simpandatarekening').click(function(){
    //     alert('test');
    // });
    $(document).ready(function(){
        
        function a()
        {
            id="123";

            var listarr = [45500, 23333, 123];

            for(var iai = 0 ; iai < listarr.length; iai++){
                if(listarr[iai] == id){
                    listarr.splice(iai, 1);
                    // alert(listarr);
                }
            }
        }
        var m_class_id = $('#modal_class_id').val();
        var myChanneliddd = null;
    	$('select.select2').each(function(){
            if($(this).attr('id') == 'tagorang'){
                $(this).select2({
                    delay: 2000,
                    maximumSelectionLength: 1,
                    ajax: {
                        dataType: 'json',
                        type: 'GET',
                        url: '<?php echo base_url(); ?>ajaxer/getStudentListByChannelForbidAdded?channel_id=<?php echo $this->session->userdata('channel_id');?>',
                        data: function (params) {
                            return {
                              term: params.term,
                              page: params.page || 1
                            };
                        },
                        processResults: function(data){
                            return {
                                results: data.results,
                                pagination: {
                                    more: data.more
                                }                       
                            };
                        }                   
                    }
                });  
            }                                          
        });


		$('#addstudent').click(function(e){
            
            var idtage = $("#tagorang").val()+ '';
            var class_id = $("#modal_class_id").val();
            var channelid = $("#channel_id").val();
            
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/addStudentClassChannel",
                type:"post",
                data: {
                    idtage: idtage,
                    class_id: class_id,
                    channel_id: channelid
                },
                success: function(data){             
                    var a = JSON.parse(data);                    
                    if (a['code'] == 200) {
                    	notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil Menambah Siswa");
						setTimeout(function(){
							location.reload();
						},1500);
                    }
                    else if (a['code'] == 405) {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Maaf, kelas private maksimal 1 siswa dalam 1 kelas.");
                    }
                    else if (a['code'] == 406) {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Maaf, kelas private maksimal 4 siswa dalam 1 kelas.");
                    }
                    else if (a['code'] == -201) {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Maaf, Point anda tidak mencukupi.");
                    }
                    else if (a['code'] == -202) {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Maaf, Anda tidak memiliki point data.");
                    }
                    else
                    {
                    	notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Gagal Menambah Siswa");
                    }
                } 
            });
         });


        $('#hapusdataclass').click(function(e){
            var rtp = $(this).attr('rtp');
            // alert(id_user);
            //menggunakan fungsi ajax untuk pengambilan data
            // $.ajax({
            //     url :"<?php echo base_url() ?>Process/deletestudentchannel",
            //     type:"post",
            //     data: {
            //     	rtp: rtp
            //     },
            //     success: function(html){             
            //         // alert("Berhasil menghapus data siswa");
            //         $('#modalDelete').modal('hide');
            //         location.reload();                     
            //     } 
            // });
        });
    });

    $(document).on("click", ".editclass", function() {
    	$("#boxlistname").empty();
        var myClassid = $(this).attr('rtp');
        // var myChannelid = $(this).attr('channel_id');
        var myChannelid = "<?php echo $this->session->userdata('channel_id');?>";
        $('#tagorang').attr('channel_id',myChannelid);
        $("#channel_id").val(myChannelid);
        $('#modal_class_id').val(myClassid);                    

        $("#modalEdit").modal("show"); 
        $.ajax({
            url :"<?php echo base_url() ?>Process/Showlist",
            type:"post",
            data: {
                class_id: myClassid,
                channel_id: myChannelid
            },
            success: function(data){ 
                var a = JSON.parse(data);
                // console.warn("a "+a['data']['part']);
                // alert("<?php echo $this->session->userdata('idclass');?>");
                $("#modal_kotak_participant").val(JSON.stringify(a['data']['participant']));
                console.log(JSON.stringify(a['data']['participant']));
                var id_user = null;
                var name = null;
                var no = 0;                
                // console.warn(" ID USER BRO "+data);
                // console.log(a['data']['participant'].length);
                for (var iai = 0; iai < a['data']['participant'].length ;iai++) {
                    id_user = a['data']['participant'][iai]['id_user'];
                    // $("#modal_kotak_participant").val(id_user+",");
                    // console.warn(" ID USER BRO "+id_user);                    
                    $.ajax({
                        url :"<?php echo base_url() ?>Process/Getname",
                        type:"post",
                        data: {
                            id_user: id_user
                        },
                        success: function(datus){
                            var b = JSON.parse(datus);
                            
                            // console.warn(b);
                            // for (var iaii = 0; iaii < b['data'].length ;iaii++) {
                            name = b['data'];
                            iduser = b['id_user'];
                            no += 1;

                            var buttonnih = "<button class='btn btn-danger f-16 ambilid' class_id='"+myClassid+"' id_user='"+iduser+"' id='cobaa"+no+"'><i class='zmdi zmdi-delete'></i></button>";
                            // console.warn(b['data']);
                            // console.warn(name);
                            var kotak = "<tr><td>"+no+"</td><td>"+name+"</td><td>"+buttonnih+"</td></tr>";

                            $("#boxlistname").append(kotak);                            
                            // }
                        }
                    });
                    // console.warn(a['data']['participant'][iai]['id_user']);

                };
                // alert("Berhasil menghapus data siswa");
                // $('#modalDelete').modal('hide');
                // location.reload();                     
            } 
        });       
    });

    $(document).on('click', '.ambilid', function(e){
        var myBookId = $(this).attr('id');
        var class_id = $(this).attr('class_id');
        var myIdUser = $(this).attr('id_user');
        // alert(myBookId);        
        // var id="149000";

        // var data = '{"status":1,"data":{"visible":"private","participant":[{"id_user":"149000"},{"id_user":"94000"},{"id_user":"227000"},{"id_user":"88000"},{"id_user":"236000"},{"id_user":"361000"},{"id_user":"33"},{"id_user":"87000"}]}}';
        // var a = JSON.parse(data);
        // for (var iai = 0; iai < a['data']['participant'].length ;iai++) {
        //     id_user = a['data']['participant'][iai]['id_user'];
        //     alert(id_user)
        //     if(id_user == id){
        //         listarrr.splice(iai, 1);
        //         alert(listarrr);
        //     }
        // }
        var listarrr = $("#modal_kotak_participant").val();
        listarrr = JSON.parse(listarrr);
        // var listarr = [45500, 23333, 123];
        // var listarrr = [{"id_user":"149000"},{"id_user":"94000"},{"id_user":"227000"},{"id_user":"88000"},{"id_user":"236000"},{"id_user":"361000"},{"id_user":"33"},{"id_user":"87000"}]
        // var listarrr = [{"id_user":"149000"},{"id_user":"94000"},{"id_user":"227000"}];
        // console.warn("ini list_participant "+list_participant);
        // console.warn("ini listarrr "+listarrr);        
        for(var iai = 0 ; iai < listarrr.length; iai++){
            // alert(listarrr[iai]);
            // console.warn(myIdUser);
            // console.warn(listarrr[iai]['id_user']);  
            var user = listarrr[iai]['id_user']; 
            // alert(user);
            if(user == myIdUser){
                // console.warn("YEAAAHHHH");
                listarrr.splice(iai, 1);
                // alert(user);
                // alert(myIdUser);
                // alert(listarrr);
                // console.warn(listarrr);
                // alert("ini listar "+JSON.stringify(listarrr));
                $.ajax({
                    url :"<?php echo base_url() ?>Process/UpdateParticipant",
                    type:"post",
                    data: {
                        list: listarrr,
                        id_user: myIdUser,
                        class_id: class_id
                    },
                    success: function(datus){
                        var a = JSON.parse(datus);
                        // alert(JSON.stringify(a));
                        if (a['code'] == 200) {
                            notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil Menghapus Siswa");
                            setTimeout(function(){
                                location.reload();
                            },1000);
                        }
                        else
                        {
                            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Gagal Menghapus Siswa");
                        }
                    }
                });
            }
        }
    });

    $(document).on("click", ".hapusclass", function () {        
        var myBookId = $(this).attr('rtp');        
        $('#hapusdataclass').attr('rtp',myBookId);
    }); 

    // $('table.display').DataTable( {
    //     fixedHeader: {
    //         header: true,
    //         footer: true
    //     }
    // } );
    $('#list').DataTable(); 
    $('#editclasss').DataTable();  
</script>
