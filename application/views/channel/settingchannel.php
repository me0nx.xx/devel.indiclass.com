<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

</style>
<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sidechannel'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2><?php echo $this->lang->line('settingchannel');?></h2>
                <!-- <ul class="actions hidden-xs">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><?php echo $this->lang->line('accountlist'); ?></li>
                    </ol>                    
                </ul> -->
            </div>
            <div class="modal fade" data-modal-color="red"  data-backdrop="static" id="modal_alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-body" style="margin-top: 75%;" > 
                            <br>
                            <div class="alert alert-danger" style="display: block; ?>" id=""><center><?php echo $this->lang->line('maxsizelogo');?>,<br><?php echo $this->lang->line('sizelogo');?></center>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal" data-target="#modal_alert" aria-label="Close" style="">OK</button>
                        </div>
                    </div>
                </div>
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>
 						<?php 
                        $channel_id = $this->session->userdata('channel_id');
                        $v = $this->db->query("SELECT tpt. * 
                            FROM master_channel AS tpt
                            WHERE tpt.channel_id =  '$channel_id'")->row_array();
							$channel_name = $v['channel_name'];
							$channel_description = $v['channel_description'];
                            $channel_about = $v['channel_about'];
							$channel_country_code = $v['channel_country_code'];
							$channel_callnum = $v['channel_callnum'];
							$channel_email = $v['channel_email'];
							$channel_address = $v['channel_address'];
							$channel_logo = $v['channel_logo'];
							$channel_banner = $v['channel_banner'];
							$channel_link = $v['channel_link'];

					 		$channel_id = $this->session->userdata('channel_id');
                            $get_image = $this->db->query("SELECT * FROM master_channel WHERE channel_id='".$channel_id."'")->row_array();
                        ?>  

                        <div class="modal " data-modal-color="" id="modalBanner" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg" style="">
                                <div class="modal-content">
                                <form id="form1" method="post" action="<?php echo base_url('Master/update_banner'); ?>" enctype="multipart/form-data">
                                    <div class="modal-header">
                                        <center>
                                            <h4 class="modal-title" style="color:gray; margin-bottom:-20px;"><?php echo $this->lang->line('changebanner');?></h4>                
                                        </center>
                                    </div>
                                    <hr>
                                    <div class="modal-body">
                                            <center>                        
                                                <input type="file" required name="inputBanner" id="inputBanner" class="filestyle" accept="image/x-png,image/gif,image/jpeg" data-placeholder="No Selected" style="margin-left:10px;">  
                                                <img src="<?php
                                                if ($channel_banner==null){
                                                    echo base_url('/aset/img/channel/banner/empty.jpg'); 
                                                }
                                                else
                                                {
                                                echo CDN_URL.USER_IMAGE_CDN_URL.$get_image['channel_banner'];     
                                                }
                                                

                                                ?>" style='width: 800px; height: 267px;' id="viewbanner" class="m-t-20">
                                            </center>
                                            <hr>
                                            <h4 class="c-gray">Note: </h4>
                                            <ul class="c-red">
                                                <li><?php echo $this->lang->line('maxsizebanner');?></li>
                                                <li><?php echo $this->lang->line('sizebanner');?></li>                   
                                            </ul>
                                            
                                            <script>
                                                function readURL1(input) {
                                                    if (input.files && input.files[0]) {
                                                        var reader = new FileReader();

                                                        reader.onload = function (e) {
                                                            $('#viewbanner').attr('src', e.target.result);
                                                        }

                                                        reader.readAsDataURL(input.files[0]);
                                                    }
                                                }

                                                $("#inputBanner").change(function () {
                                                    readURL1(this);
                                                });

                                                $(":file").filestyle({placeholder: "No file"});                        
                                            </script>   
                                    </div>
                                    <div class="progress progress-striped active m-t-10" style="height:18px;">
                                        <div class="progress-bar" style="width:0%; height:150px;"></div>
                                    </div>
                                    <div class="modal-footer bgm-teal">
                                        <button type="submit" class="btn btn-info upload" id="save" name="save"><?php echo $this->lang->line('upload');?></button>                
                                        <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo $this->lang->line('cancel');?></button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal show " data-modal-color="" id="modalLogo" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                <form id="form3" method="post" action="<?php echo base_url('Master/update_logo'); ?>" enctype="multipart/form-data">
                                    <div class="modal-header">
                                        <center>
                                            <h4 class="modal-title" style="color:gray; margin-bottom:-20px;"><?php echo $this->lang->line('changelogo');?></h4>                
                                        </center>
                                    </div>
                                    <hr>
                                    <div class="modal-body">
                                            <center>                        
                                                <input type="file" required name="inputFile1" id="inputFile1" class="filestyle" accept="image/x-png,image/gif,image/jpeg" data-placeholder="No Selected" style="margin-left:10px;">  
                                                <img src="<?php
                                                if ($channel_logo==null){
                                                    echo base_url('/aset/img/channel/logo/empty.jpg'); 
                                                }
                                                else
                                                {
                                                echo CDN_URL.USER_IMAGE_CDN_URL.$get_image['channel_logo'];     
                                                }
                                                

                                                ?>" style='width: 256px; height: 256px;' id="viewgambar" class="m-t-20">
                                            </center>
                                            <hr>
                                            <h4 class="c-gray">Note: </h4>
                                            <ul class="c-red">
                                                <li><?php echo $this->lang->line('maxsizelogo');?></li>
                                                <li><?php echo $this->lang->line('sizelogo');?></li>                   
                                            </ul>
                                            
                                            <script>
                                                function readURL(input) {
                                                    if (input.files && input.files[0]) {
                                                        var reader = new FileReader();

                                                        reader.onload = function (e) {
                                                            $('#viewgambar').attr('src', e.target.result);
                                                        }

                                                        reader.readAsDataURL(input.files[0]);
                                                    }
                                                }

                                                $("#inputFile1").change(function () {
                                                    readURL(this);
                                                });

                                                $(":file").filestyle({placeholder: "No file"});                        
                                            </script>   
                                    </div>
                                    <div class="progress progress-striped active m-t-10" style="height:18px;">
                                        <div class="progress-bar" style="width:0%; height:150px;"></div>
                                    </div>
                                    <div class="modal-footer bgm-teal">
                                        <button type="submit" class="btn btn-info upload" id="save_logo" name="save_logo"><?php echo $this->lang->line('upload');?></button>                
                                        <button type="button" class="btn btn-link" data-dismiss="modal"><?php echo $this->lang->line('cancel');?></button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="card">
			                <form action="<?php echo base_url('/master/saveEditChannel') ?>" method="post">
								<section id="content" style="margin-top: 2%; overflow-x: hidden;">
								    <div class="container">        
								        <div class="row"> 
								        	<div id="ganti_banner" style="cursor:pointer;" class=" col-md-12 col-sm-12 col-xs-12">

								        		<div>
                                                     <label data-target="#modalBanner" data-toggle="modal" id="button_banner"  style=" background:rgba(0,0,0,0.3); position: absolute;  margin-top:1%; width: 1110px;" class=" btn btn-primary btn-block" data-target="#modalLogo" data-toggle="modal"  ><i class="zmdi zmdi-camera"></i> <span class="hidden-xs" ><?php echo $this->lang->line('change_banner');?>Edit Banner</span></label>
													<img data-target="#modalBanner" data-toggle="modal" src="<?php
                                                if ($channel_banner==null){
                                                    echo base_url('/aset/img/channel/banner/empty.jpg'); 
                                                }
                                                else
                                                {
                                                echo CDN_URL.USER_IMAGE_CDN_URL.$get_image['channel_banner'];     
                                                }
                                                

                                                ?>" class="m-t-10" id=" " src="#" alt="" style="width: 1110px; height: 370px;">
												</div>
								        	</div>
								            <div class="col-md-4 col-sm-12 col-xs-12" style="margin-top:0; margin-left: 0%;">

								            	<div class="">
								                    <div class="pmo-pic center"  style="margin-top: 1%; cursor:pointer;">
								                       <div style="" class="p-relative" data-target="#modalLogo" data-toggle="modal">
															<img src="<?php
                                                if ($channel_logo==null){
                                                    echo base_url('/aset/img/channel/logo/empty.jpg'); 
                                                }
                                                else
                                                {
                                                echo CDN_URL.USER_IMAGE_CDN_URL.$get_image['channel_logo'];     
                                                }
                                                

                                                ?>"  class="m-t-10" id="imglogo" src="#" alt="Your Logo" style="width:360px; height:360px;"><br><br><br>
                                                            <label class="btn btn-primary btn-block" data-target="#modalColor" data-toggle="modal"  ><i class="zmdi zmdi-camera"></i> <span class="hidden-xs" ><?php echo $this->lang->line('changelogo');?></span></label>
														</div>
								                    </div>
													<br>				
								                </div>                                           
								            </div>
								            <div class="col-md-8 col-sm-12 col-xs-12 m-t-10" style="width: 60%; ">
								            	 <input type="hidden" name="channel_id" required class="form-control" value="<?php echo $channel_id; ?>">
													<div class="fg-line">
														<label class="fg-label m-t-5"><?php echo $this->lang->line('channelname'); ?> :</label>
														<input placeholder="<?php echo $this->lang->line('channelname'); ?>" type="text" name="channel_name" id="channel_name" rows="5" required class="form-control" value="<?php echo $channel_name; ?>">

													</div>
													<div class="fg-line">
														<label class="fg-label m-t-5"><?php echo $this->lang->line('channeldescription'); ?> :</label>
														<input placeholder="<?php echo $this->lang->line('channeldescription'); ?>" type="text" name="channel_description" id="channel_description" rows="5" required class=" form-control" value="<?php echo $channel_description; ?>">  
													</div>
													<div class="fg-line">
														<label class="fg-label m-t-5"><?php echo $this->lang->line('channelemail'); ?> :</label>
														<input readonly="readonly" placeholder="<?php echo $this->lang->line('channel_email'); ?>" type="text" name="channel_email" id="channel_email" rows="5" required class=" form-control" value="<?php echo $channel_email; ?>">  
													</div>
													<div class="col-md-12 m-t-10"> 
														<label class="fg-label m-t-5"><?php echo $this->lang->line('channelphone'); ?> :</label>
													</div>
													<div class="col-md-3 m-t-10"> 

				                                        <div class="fg-line">

				                                            <select required id="channel_country_code" name="channel_country_code" class="select2 form-control">
				                                            
				                                            <?php
				                                                $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
				                                                foreach ($allsub as $row => $d) {
				                                                    if($d['phonecode'] == $v['channel_country_code']){
				                                                        echo "<option value='".$d['phonecode']."' selected='true'>".$d['nicename']." +".$d['phonecode']."</option>";
				                                                    }
				                                                    if ($d['phonecode'] != $v['channel_country_code']) {
				                                                        echo "<option value='".$d['phonecode']."'>".$d['nicename']." +".$d['phonecode']."</option>";
				                                                    }                                                            
				                                                }
				                                            ?> 
				                                            </select>
				                                        </div>
				                                    </div>
				                                    <div class="col-md-9 m-t-15">
				                                        <div class="fg-line">
				                                            <input type="text" onkeyup="validPhone(this)"  onkeyup="validAngka(this)" onkeypress="return hanyaAngka(event)" name="channel_callnum" id="channel_callnum" minlength="9" maxlength="14" required class="input-sm form-control fg-input" value="<?php echo $channel_callnum; ?>">
				                                        </div>
				                                    </div> 
													<div class="fg-line">
														<label class="fg-label m-t-5"><?php echo $this->lang->line('channeladdress'); ?> :</label>
														<input placeholder="<?php echo $this->lang->line('channeladdress'); ?>" type="text" name="channel_address" id="channel_address" rows="5" required class=" form-control" value="<?php echo $channel_address; ?>">  
													</div>
                                                    <label class="fg-label m-t-5"><?php echo $this->lang->line('channellink'); ?> : https://classmiles.com/channel/</label><br>
                                                    <div style="" class="col-md-4 fg-line">
                                                        <input placeholder="<?php echo $this->lang->line('channellink'); ?>" type="text" name="channel_link" id="channel_link" rows="5" required class=" form-control" value="<?php echo $channel_link; ?>">
                                                    </div>
                                                    <div class="m-t-10">
                                                        <div class="form-group">
                                                            <label class="fg-label m-t-5"><?php echo $this->lang->line('channelabout'); ?> :</label>
                                                            <div class="fg-line">
                                                                <textarea class="form-control" rows="5" placeholder="" name="channel_about"><?php echo $channel_about; ?></textarea>
                                                            </div>
                                                        </div>

                                                    </div>
                                                           <br><br>                                                                 
                                                    <button  type="submit" name="simpanedit" id="simpanedit" class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); ?></button><br><br><br> 	                           	
								        	</div>
								    	</div>

								    </div>

								</section>
			                </form>  
			            </div>
        </div>
    </section>

</section>
<script type="text/javascript">
$(document).ready(function(){
     function proccess()
        {
            $(document).on('submit','form',function(e){
                var inp = $("#inputFile");                
                if (inp.val().length > 0) {
                    e.preventDefault();
                    $form = $(this);
                    uploadImage($form);            
                }          
            });
        }

        // PROGRESS BAR
        document.forms[0].addEventListener('submit', function( evt ) {
            var file = document.getElementById('inputFile').files[0];
            var inp = $("#inputFile");

            if(file && file.size < 2000000) { // 10 MB (this size is in bytes)
                //Submit form
                // alert('sip bro');
                proccess(); 
            
                // if (inp.val().length > 0) {

                //     e.preventDefault();

                //     $form = $(this);

                //     uploadImage($form);
                    
                // }   
                
            } else {
                //Prevent default and display error
                // alert('terlalu besar bro');
                $.ajax({
                    url: 'https://classmiles.com/Master/sizeistobigchannel',
                    type: 'POST',
                    success: function(response)
                    { 
                        location.reload();
                    }
                });
                evt.preventDefault();
                
            }
        }, false);

     function uploadImage($form){
            $form.find('.progress-bar').removeClass('progress-bar-success')
                                        .removeClass('progress-bar-danger');

            var formdata = new FormData($form[0]); //formelement
            var request = new XMLHttpRequest();

            //progress event...
            request.upload.addEventListener('progress',function(e){
                var percent = Math.round(e.loaded/e.total * 100);
                $form.find('.progress-bar').width(percent+'%').html(percent+'%');
            });

            //progress completed load event
            request.addEventListener('load',function(e){
                
                $form.find('.progress-bar').addClass('progress-bar-success').html("<?php if ($this->session->userdata('lang') == 'indonesia') { echo "Unggah Selesai...";} else { echo "Uploading Completed...";} ?>");
                // alert('Profile Completed Change');
                setTimeout(function(){
                    window.location.reload();
                }, 1000);
            });

            request.open('post', '<?php echo base_url(); ?>Master/update_logo');            
            request.send(formdata);

            $form.on('click','.cancel',function(){
                request.abort();

                $form.find('.progress-bar')
                    .addClass('progress-bar-danger')
                    .removeClass('progress-bar-success')
                    .html('upload aborted...');
            });

        }

    $("#ganti_banner").mouseover(function(){
        $("#button_banner").css("display", "inline");
    });
    $("#ganti_banner").mouseout(function(){
        $("#button_banner").css("display", "none");
    });

});

</script>
<script type="text/javascript">
    
    $(document).ready(function(){
        $('#inputFile1').change(function(){
                  readURL(this);
                });
                function readURL(input){
                  var file = document.getElementById('inputFile1').files[0];
                  var inp = $("#inputFile1");

                  if(file && file.size < 2000000) { // 10 MB (this size is in bytes)
                      console.warn('nothing');   
                      // var reader = new FileReader();

                      // reader.onload = function(e){
                      //   $('#imgprev').attr('src', e.target.result);
                      //   $('#imgdata').html(e.target.result);
                      //   $('#imgprev').css('visibility','visible');
                      //   $('#imgprev').css('display','block');
                      // }  
                      // reader.readAsDataURL(input.files[0]);                               
                  } else {
                      //Prevent default and display error                      
                      var $el = $('#inputFile1');
                      $el.wrap('<form>').closest('form').get(0).reset();
                      $el.unwrap();
                      $("#modal_alert").modal("show");     
                       $("#modalLogo").modal("hide");                       
                  }
                  // if(input.files && input.files[0]){
                  //   var reader = new FileReader();

                  //   reader.onload = function(e){
                  //     $('#imgprev').attr('src', e.target.result);
                  //     $('#imgdata').html(e.target.result);
                  //     $('#imgprev').css('visibility','visible');
                  //     $('#imgprev').css('display','block');
                  //   }

                  //   reader.readAsDataURL(input.files[0]);
                  // }
                }

        var tgl = new Date();
        var formattedDate = moment(tgl).format('YYYY-MM-DD');
        var channel_link    = "<?php echo $this->session->userdata('channel_n');?>";  
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        $.ajax({
            url: 'https://classmiles.com/Rest/ChannelDetails/access_token'+tokenjwt,
            type: 'POST',
            data: {
                channel_link: channel_link
            },
            success: function(response)
            { 
                var channel_id          = response['data']['channel_id'];
                var channel_name        = response['data']['channel_name'];
                var channel_description = response['data']['channel_description'];
                var channel_phone       = response['data']['channel_country_code']+' '+response['data']['channel_callnum'];
                var channel_email       = response['data']['channel_email'];
                var channel_address     = response['data']['channel_address'];
                var channel_logo        = response['data']['channel_logo']+'?'+new Date().getTime();
                var channel_banner      = response['data']['channel_banner']+'?'+new Date().getTime();

                $("#channel_logo").attr('src','../aset/img/channel/logo/'+channel_logo);
                $("#channel_name").text(channel_name);
                // $("#boxblue").css('background','url("../aset/img/channel/banner/'+channel_banner+'")');
                $("#boxblue").css('background','url("../aset/img/headers/4.png")')+'?'+new Date().getTime();
                $("#boxblue").css('background-size','100%');
                $("#desc_channellogo").attr('src', '../aset/img/channel/logo/'+channel_logo)+'?'+new Date().getTime();
                
                $("#desc_channeladdress").text(channel_address);
                $("#titleone").text(channel_name);
                $("#titletwo").text(channel_description);
                $("#desc_channelphone").text(channel_phone);
                $("#desc_channelemail").text(channel_email);
                $("#namachannel").text(channel_name);
                Showclass(channel_id);
            }
        });

        function Showclass(channel_id)
        {
            var show_linkclass = null;
            var user_utc = new Date().getTimezoneOffset();            
            user_utc = -1 * user_utc;
            var date = new Date();
            date.setDate(date.getDate() + 7);            
            // var dateMsg = date.getDate()+'-'+ (date.getMonth()+1) +'-'+date.getFullYear();            
            var dateMsg = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
            dateMsg = moment(dateMsg).format('YYYY-MM-DD');  
            // alert(formattedDate);
            // alert(dateMsg);
            $.ajax({
                url: 'https://classmiles.com/Rest/getChannelClasses?channel_id='+channel_id+'&start_date='+formattedDate+'&end_date='+dateMsg+'&user_utc='+user_utc+'&user_device=web/access_token/'+tokenjwt,
                type: 'GET',
                success: function(data)
                {            
                    console.log("ini data"+ JSON.stringify(data));
                    if (data.response.length != null) {
                        for (var i = data.response.length-1; i >=0;i--) {
                            var key = i;                                                    
                            var show_classid            = data['response'][i]['class_id'];
                            // get_encrypt(show_classid); 
                            var show_subject_id     = data['response'][i]['subject_id'];
                            var show_tutor_id       = data['response'][i]['tutor_id'];
                            var show_user_name      = data['response'][i]['user_name'];
                            var show_subject_name   = data['response'][i]['subject_name'];
                            var show_template_type  = data['response'][i]['template_type'];
                            var show_class_type     = data['response'][i]['class_type'];
                            var show_description    = data['response'][i]['description'];
                            var show_harga          = data['response'][i]['harga'];
                            var show_user_image     = data['response'][i]['user_image'];
                            var show_time           = data['response'][i]['time'];
                            var show_endtime        = data['response'][i]['endtime'];
                            var show_date           = moment(data['response'][i]['date']).format('DD-MM-YYYY');
                            var show_enddate        = data['response'][i]['enddate'];
                            if (show_template_type == "whiteboard_digital") {
                                show_template_type = "Digital Board";
                            }
                            else if (show_template_type == "whiteboard_videoboard") {
                                show_template_type = "Video Board";
                            }
                            else
                            {
                                show_template_type = "No Whiteboard";
                            }

                            if (show_harga == null) {
                                show_harga = "Free class";
                            }
                            
                            var boxclass = "<a class='"+show_classid+"'><div class='col-md-3 col-sm-6 p-t-5'><div id='best-selling' class='dash-widget-item waves-effect z-depth-1' style='cursor: pointer;'><div class='dash-widget-header'><img class='' src='../aset/img/user/"+show_user_image+"' style='z-index: 1; position: absolute; margin: 3%; height: 40px; width: 40px; top: 5; right: 0;'><img src='../aset/img/headers/BGclassroom-01.png'><div class='dash-widget-title'>"+show_template_type+"</div><div class='main-item'><h2>"+show_harga+"</h2></div></div><div class='listview p-15'></a><img class='pull-right' src='../aset/img/icons/share-variant.png' id='kocak' class='kocak' data-classid='"+show_classid+"' title='Share Class' style='cursor:pointer; z-index: 99; height: 25px; width: 25px;'/><a class='"+show_classid+"'><div class='f-14 c-gray'>"+show_user_name+"</div><br><small class='f-18 c-black'>"+show_subject_name+" - </small><br><small class='f-18 c-black'>"+show_description+"</small></div></div></div></a>";                                
                            // <div class='p-10 c-black' style='bottom: 0; position: absolute; left: 0;'>"+show_date+", "+show_time+"-"+show_endtime+"</div>
                            $("#boxclass_more").append(boxclass);

                            get_encrypt(show_classid);

                        }
                    }
                }
            });
        }

        function get_encrypt(show_classid)
        {            
            $.ajax({
                url: '<?php echo base_url(); ?>Tools/get_c/' + show_classid,
                type: 'GET',
                /*data: {
                    class_id: classid
                },*/
                success: function(data)
                {    
                    
                    data = data.trim();                                          
                    c = data;   
                    
                    link = "<?php echo base_url();?>class?c="+data;                                        
                    $("."+show_classid).attr('href',link);                                        
                }
            });
        }



        $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
            // Prevent default anchor event
            e.preventDefault();
            
            // Set values for window
            intWidth = intWidth || '500';
            intHeight = intHeight || '400';
            strResize = (blnResize ? 'yes' : 'no');

            // Set title and open popup with focus on it
            var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
                strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,            
                objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
        }
        
        $(document).ready(function ($) {
            $('.customer.share').on("click", function(e) {
                $(this).customerPopup(e);
            });
        });

    });
</script>
<script>
	function hanyaAngka(evt) {
	  var charCode = (evt.which) ? evt.which : event.keyCode
	   if (charCode > 31 && (charCode < 48 || charCode > 57))

	    return false;
	  return true;
	}
	function validAngka(a)
	{
		if(!/^[0-9.]+$/.test(a.value))
		{
		a.value = a.value.substring(0,a.value.length-31);
		}
	}
      function validPhone(a)
    {
        if(!/(^0$)|(^[1-9]\d{0,12}$)/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
</script>
<script type="text/javascript">
        $(document).ready(function(){
            // $("#modal").click(function()
            // {
            //     $("#tutor_modal").modal('show');    
            // });
            
            var code = null;
            var cekk = setInterval(function(){
                code = "<?php echo $this->session->userdata('code');?>";

                cek();
            },500);

            function cek(){
                if (code == "555") {
                    $("#modal_alert").modal('show');
                    code == null;
                    clearInterval(cekk);
                    $.ajax({
                        url: 'https://classmiles.com/Rest/clearsession/access_token/'+tokenjwt,
                        type: 'POST',
                        data: {
                            code: code
                        },
                        success: function(response)
                        { 
                            console.warn(response);
                        }
                    });
                }
            }
        });
</script>