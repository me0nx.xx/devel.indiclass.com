<div id="container" class="effect aside-float aside-bright mainnav-lg">
        
    <!--NAVBAR-->
    <!--===================================================-->
    <header id="navbar">
        <?php $this->load->view('inc/navbar_student'); ?>
    </header>
    <!--===================================================-->
    <!--END NAVBAR-->

    <div class="boxed">

        <!--CONTENT CONTAINER-->
        <!--===================================================-->
        <div id="content-container">
            <div id="page-head">                
                <div class="pad-all text-center">
                    <h3>Selamat Datang di Beranda Kelas Anda.</h3>
                </div>
            </div>

            
            <!--Page content-->
            <!--===================================================-->
            <div id="page-content">            
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-mint panel-colorful media middle pad-all">
                            <div class="media-left">
                                <div class="pad-hor">
                                    <i class="demo-pli-file-word icon-3x"></i>
                                </div>
                            </div>
                            <div class="media-body">
                                <p class="text-2x mar-no text-semibold">241</p>
                                <p class="mar-no">Class Live</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-warning panel-colorful media middle pad-all">
                            <div class="media-left">
                                <div class="pad-hor">
                                    <i class="demo-pli-file-zip icon-3x"></i>
                                </div>
                            </div>
                            <div class="media-body">
                                <p class="text-2x mar-no text-semibold">241</p>
                                <p class="mar-no">Class Up Coming</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-dark panel-colorful media middle pad-all">
                            <div class="media-left">
                                <div class="pad-hor">
                                    <i class="demo-pli-camera-2 icon-3x"></i>
                                </div>
                            </div>
                            <div class="media-body">
                                <p class="text-2x mar-no text-semibold">241</p>
                                <p class="mar-no">Class Ended</p>
                            </div>
                        </div>
                    </div>                        
            
                </div>

                <div class="alert alert-success" id="alertJoinSuccess" style="display: none;">
                    <strong>Join Berhasil!</strong> Silahkan Masuk Untuk mengikuti Kelas.
                </div>

                <div class="alert alert-danger" id="alertJoinFailed" style="display: none;">
                    <strong>Join Gagal!</strong> Silahkan Coba join kembali.
                </div>

                <div class="row" id="boxClasses">                                        

                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"><img src="https://cdn.classmiles.com/sccontent/baru/liveclass-04.png" style="height: 25px; width: 25px;"> Class Live</h3>
                            </div>
                        </div>

                        <div id="boxClassesLive"></div>
                    </div>

                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"><img src="https://cdn.classmiles.com/sccontent/baru/comingup-04.png" style="height: 25px; width: 25px;"> Class Coming</h3>
                            </div>
                        </div>

                        <div id="boxClassesComing"></div>
                    </div>

                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"><img src="https://cdn.classmiles.com/sccontent/baru/missedclass-04.png" style="height: 25px; width: 25px;"> Class Ended</h3>
                            </div>
                        </div>

                        <div id="boxClassesEnded"></div>
                    </div>

                </div>
                    
            </div>
            <!--===================================================-->
            <!--End page content-->

        </div>
        <!--===================================================-->
        <!--END CONTENT CONTAINER-->
        
        <!--MAIN NAVIGATION-->
        <!--===================================================-->
        <nav id="mainnav-container">
            <?php $this->load->view('inc/sidebar_student');?>
        </nav>
        <!--===================================================-->
        <!--END MAIN NAVIGATION-->

    </div>

    <!-- FOOTER -->
    <!--===================================================-->
    <footer id="footer">
        <?php $this->load->view('inc/bottom/bottom_student'); ?>
    </footer>
    <!--===================================================-->
    <!-- END FOOTER -->

    <!-- SCROLL PAGE BUTTON -->
    <!--===================================================-->
    <button class="scroll-top btn">
        <i class="pci-chevron chevron-up"></i>
    </button>
    <!--===================================================-->
    <div class="mainnav-backdrop"></div>

</div>

<script type="text/javascript">
    
    $(document).ready(function() {

        var access_token    = "<?php echo $this->session->userdata('access_token');?>";
        var id_user         = "<?php echo $this->session->userdata('id_user');?>";
        var d               = new Date();
        var month           = d.getMonth()+1;
        var day             = d.getDate();

        var date            = d.getFullYear() + '-' +
        ((''+month).length<2 ? '0' : '') + month + '-' +
        ((''+day).length<2 ? '0' : '') + day;
        
        var tgl = new Date();
        var formattedDate = moment(tgl).format('YYYY-MM-DD');
        var datenow = formattedDate+" "+tgl.getHours()+":"+tgl.getMinutes();
        var livecount = 0;
        var waitcount = 0;
        var passcount = 0;

        $.ajax({
            url: 'https://classmiles.com/api/v1/scheduleone/access_token/'+access_token,
            type: 'POST',
            data: {
                date: date,
                user_utc : '420',
                id_user : id_user,
                user_device : 'web'
            },
            success: function(response)
            {
                var status  = response['status'];

                if (status) {

                    var boxClassesLive = "";
                    var boxClassesComing = "";
                    var boxClassesEnded = "";

                    for (var i = 0; i < response['data'].length; i++) {
                        var class_id    = response['data'][i]['class_id'];
                        var class_type  = response['data'][i]['class_type'];
                        var date        = response['data'][i]['date'];
                        var description = response['data'][i]['description'];
                        var enddate     = response['data'][i]['enddate'];
                        var endtime     = response['data'][i]['endtime'];
                        var english     = response['data'][i]['english'];
                        var first_name  = response['data'][i]['first_name'];
                        var subject_name= response['data'][i]['subject_name'];
                        var time        = response['data'][i]['time'];
                        var user_image  = response['data'][i]['user_image'];
                        var user_name   = response['data'][i]['user_name'];
                        var participant = response['data'][i]['participant'];
                        var timeclass    = response['data'][i]['date']+" "+response['data'][i]['time'];
                        var endtimeclass = response['data'][i]['enddate']+" "+response['data'][i]['endtime'];

                        if (class_type == "multicast") 
                        {
                            var participant_listt = response['data'][i]['participant']['participant'];  
                            var im_exist = false;
                            if(participant_listt != null){
                                for (var iai = 0; iai < participant_listt.length ;iai++) {
                                    var a = moment(tgl).format('YYYY-MM-DD');
                                    if (id_user == participant_listt[iai]['id_user']) {
                                        im_exist = true;
                                    }
                                }
                            }

                            boxClassesLive = "<div class='col-lg-3'>"+
                                "<div class='panel'>"+
                                    "<div class='panel-body text-center'>"+
                                        "<img alt='Profile Picture' class='img-md img-circle mar-btm' src='https://cdn.classmiles.com/usercontent/"+user_image+"'>"+
                                        "<p class='text-lg text-semibold mar-no text-main'>"+subject_name+"</p>"+
                                        "<p class='text-muted'>"+user_name+"</p>"+                                    
                                        "<a href='#' class_id='"+class_id+"' class='btn btn-success mar-ver enterClass'>Enter Class</a>"+
                                        "<ul class='list-unstyled text-center bord-top pad-top mar-no row' style='word-wrap:break-word;'>"+
                                            "<li class='col-xs-4'>"+
                                                "<span class='text-lg text-semibold text-main'>Date</span>"+
                                                "<p class='text-muted mar-no'>"+date+"</p>"+
                                            "</li>"+
                                            "<li class='col-xs-4'>"+
                                                "<span class='text-lg text-semibold text-main'>Time</span>"+
                                                "<p class='text-muted mar-no'>"+time+" - "+endtime+"</p>"+
                                            "</li>"+                                        
                                            "<li class='col-xs-4'>"+
                                                "<span class='text-lg text-semibold text-main'>Participant</span>"+
                                                "<p class='text-muted mar-no'>0</p>"+
                                            "</li>"+
                                        "</ul>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";

                            boxClassesJoinLive = "<div class='col-lg-3'>"+
                                "<div class='panel'>"+
                                    "<div class='panel-body text-center'>"+
                                        "<img alt='Profile Picture' class='img-md img-circle mar-btm' src='https://cdn.classmiles.com/usercontent/"+user_image+"'>"+
                                        "<p class='text-lg text-semibold mar-no text-main'>"+subject_name+"</p>"+
                                        "<p class='text-muted'>"+user_name+"</p>"+                                    
                                        "<button class='btn btn-info mar-ver joinLiveClass' class_id='"+class_id+"'><i class='demo-pli-male icon-fw'></i>Join</button>"+
                                        "<ul class='list-unstyled text-center bord-top pad-top mar-no row' style='word-wrap:break-word;'>"+
                                            "<li class='col-xs-4'>"+
                                                "<span class='text-lg text-semibold text-main'>Date</span>"+
                                                "<p class='text-muted mar-no'>"+date+"</p>"+
                                            "</li>"+
                                            "<li class='col-xs-4'>"+
                                                "<span class='text-lg text-semibold text-main'>Time</span>"+
                                                "<p class='text-muted mar-no'>"+time+" - "+endtime+"</p>"+
                                            "</li>"+                                        
                                            "<li class='col-xs-4'>"+
                                                "<span class='text-lg text-semibold text-main'>Participant</span>"+
                                                "<p class='text-muted mar-no'>0</p>"+
                                            "</li>"+
                                        "</ul>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";

                            boxClassesComing = "<div class='col-lg-3'>"+
                                "<div class='panel'>"+
                                    "<div class='panel-body text-center'>"+
                                        "<img alt='Profile Picture' class='img-md img-circle mar-btm' src='https://cdn.classmiles.com/usercontent/"+user_image+"'>"+
                                        "<p class='text-lg text-semibold mar-no text-main'>"+subject_name+"</p>"+
                                        "<p class='text-muted'>"+user_name+"</p>"+
                                        "<ul class='list-unstyled text-center bord-top pad-top mar-no row' style='word-wrap:break-word;'>"+
                                            "<li class='col-xs-4'>"+
                                                "<span class='text-lg text-semibold text-main'>Date</span>"+
                                                "<p class='text-muted mar-no'>"+date+"</p>"+
                                            "</li>"+
                                            "<li class='col-xs-4'>"+
                                                "<span class='text-lg text-semibold text-main'>Time</span>"+
                                                "<p class='text-muted mar-no'>"+time+" - "+endtime+"</p>"+
                                            "</li>"+                                        
                                            "<li class='col-xs-4'>"+
                                                "<span class='text-lg text-semibold text-main'>Participant</span>"+
                                                "<p class='text-muted mar-no'>0</p>"+
                                            "</li>"+
                                        "</ul>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";

                            boxClassesJoinComing = "<div class='col-lg-3'>"+
                                "<div class='panel'>"+
                                    "<div class='panel-body text-center'>"+
                                        "<img alt='Profile Picture' class='img-md img-circle mar-btm' src='https://cdn.classmiles.com/usercontent/"+user_image+"'>"+
                                        "<p class='text-lg text-semibold mar-no text-main'>"+subject_name+"</p>"+
                                        "<p class='text-muted'>"+user_name+"</p>"+                                    
                                        "<button class='btn btn-info mar-ver joinComingClass' class_id='"+class_id+"'><i class='demo-pli-male icon-fw'></i>Join</button>"+
                                        "<ul class='list-unstyled text-center bord-top pad-top mar-no row' style='word-wrap:break-word;'>"+
                                            "<li class='col-xs-4'>"+
                                                "<span class='text-lg text-semibold text-main'>Date</span>"+
                                                "<p class='text-muted mar-no'>"+date+"</p>"+
                                            "</li>"+
                                            "<li class='col-xs-4'>"+
                                                "<span class='text-lg text-semibold text-main'>Time</span>"+
                                                "<p class='text-muted mar-no'>"+time+" - "+endtime+"</p>"+
                                            "</li>"+                                        
                                            "<li class='col-xs-4'>"+
                                                "<span class='text-lg text-semibold text-main'>Participant</span>"+
                                                "<p class='text-muted mar-no'>0</p>"+
                                            "</li>"+
                                        "</ul>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";

                            boxClassesEnded = "<div class='col-lg-3'>"+
                                "<div class='panel'>"+
                                    "<div class='panel-body text-center'>"+
                                        "<img alt='Profile Picture' class='img-md img-circle mar-btm' src='https://cdn.classmiles.com/usercontent/"+user_image+"'>"+
                                        "<p class='text-lg text-semibold mar-no text-main'>"+subject_name+"</p>"+
                                        "<p class='text-muted'>"+user_name+"</p>"+                                       
                                        "<ul class='list-unstyled text-center bord-top pad-top mar-no row' style='word-wrap:break-word;'>"+
                                            "<li class='col-xs-4'>"+
                                                "<span class='text-lg text-semibold text-main'>Date</span>"+
                                                "<p class='text-muted mar-no'>"+date+"</p>"+
                                            "</li>"+
                                            "<li class='col-xs-4'>"+
                                                "<span class='text-lg text-semibold text-main'>Time</span>"+
                                                "<p class='text-muted mar-no'>"+time+" - "+endtime+"</p>"+
                                            "</li>"+                                        
                                            "<li class='col-xs-4'>"+
                                                "<span class='text-lg text-semibold text-main'>Participant</span>"+
                                                "<p class='text-muted mar-no'>0</p>"+
                                            "</li>"+
                                        "</ul>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";

                            if(im_exist){
                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                {   
                                    waitcount++;
                                    $("#boxClassesComing").append(boxClassesComing);
                                }
                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                    livecount++;
                                    $("#boxClassesLive").append(boxClassesLive);
                                }
                                else
                                {       
                                    passcount++;
                                    $("#boxClassesEnded").append(boxClassesEnded);
                                }
                            }
                            else 
                            {
                                if (Date.parse(datenow) < Date.parse(timeclass)) 
                                {             
                                    waitcount++; 
                                    $("#boxClassesComing").append(boxClassesJoinComing);
                                }
                                else if (Date.parse(timeclass) <= Date.parse(datenow) && Date.parse(datenow) < Date.parse(endtimeclass)) {
                                    livecount++;
                                    $("#boxClassesLive").append(boxClassesJoinLive);
                                }
                                else
                                {
                                   
                                }
                            }
                        }            
                        
                    }

                    // $("#boxClasses").append(boxClasses);
                }
            }
        });
        
        $(document.body).on('click', '.enterClass' ,function(e){
            var class_id = $(this).attr('class_id');
            if (/Android|webOS|iPhone|iPad|BlackBerry|Windows Phone|Opera Mini|IEMobile|Mobile/i.test(navigator.userAgent))
            {
                var p = 'android';
            }
            else
            {
                var p = 'web';
            }
            
            $.ajax({
                // url: 'https://classmiles.com/api/v1/tps_me/',
                url: '<?php echo AIR_API;?>tps_me/'+class_id+'?p='+p,
                type: 'POST',
                data: {                    
                    id_user : id_user,
                    linkback : '<?php echo BASE_URL(); ?>'
                },
                success: function(response)
                {
                    var code    = response['code'];
                    if (code == 200) {
                        var link    = response['link'];
                        alert(link);
                    }
                }
            });
        });

        $(document.body).on('click', '.joinComingClass' ,function(e){
            var class_id = $(this).attr('class_id');

            $.ajax({
                url: 'https://classmiles.com/api/v1/joinClass/access_token/'+access_token,
                type: 'POST',
                data: {                    
                    id_user : id_user,
                    class_id : class_id
                },
                success: function(response)
                {
                    var code    = response['code'];
                    window.scrollTo(0,0);
                    if (code == 200) {
                        $("#alertJoinSuccess").css('display','block');                        
                        setTimeout(function(){
                            location.reload();
                        },3000);
                    }
                    else
                    {
                        $("#alertJoinFailed").css('display','block');
                        setTimeout(function(){
                            location.reload();
                        },3000);   
                    }
                }
            });
        });

        $(document.body).on('click', '.joinLiveClass' ,function(e){
            var class_id = $(this).attr('class_id');

            $.ajax({
                url: 'https://classmiles.com/api/v1/joinClass/access_token/'+access_token,
                type: 'POST',
                data: {                    
                    id_user : id_user,
                    class_id : class_id
                },
                success: function(response)
                {
                    var code    = response['code'];
                    window.scrollTo(0,0);
                    if (code == 200) {
                        $("#alertJoinSuccess").css('display','block');
                        setTimeout(function(){
                            location.reload();
                        },3000);
                    }
                    else
                    {
                        $("#alertJoinFailed").css('display','block');
                        setTimeout(function(){
                            location.reload();
                        },3000);   
                    }
                }
            });
        });

    });

</script>
    
    
    