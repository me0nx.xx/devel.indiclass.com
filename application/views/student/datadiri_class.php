<div id="container" class="effect aside-float aside-bright mainnav-lg">
        
    <!--NAVBAR-->
    <!--===================================================-->
    <header id="navbar">
        <?php $this->load->view('inc/navbar_student'); ?>
    </header>
    <!--===================================================-->
    <!--END NAVBAR-->

    <div class="boxed">

        <!--CONTENT CONTAINER-->
        <!--===================================================-->
        <div id="content-container">
            
            <div id="page-content">
                <div class="panel">
                    <div class="panel-body">
                        <div class="fixed-fluid">

                            <div class="fixed-md-200 pull-sm-left fixed-right-border">
            
                                <!-- Simple profile -->
                                <div class="text-center">
                                    <div class="pad-ver">
                                        <img src="../assets/indiclass/img/profile-photos/1.png" class="img-lg img-circle" alt="Profile Picture">
                                    </div>
                                    <h4 class="text-lg text-overflow mar-no"><?php echo $this->session->userdata('user_name'); ?></h4>
                                </div>
                                <hr>
            
                                <!-- Profile Details -->
                                <p class="pad-ver text-main text-sm text-uppercase text-bold">About Me</p>
                                <p><i class="demo-pli-map-marker-2 icon-lg icon-fw"></i> San Jose, CA</p>
                                <p><a href="#" class="btn-link"><i class="demo-pli-internet icon-lg icon-fw"></i> http://www.themeon.net</a></p>
                                <p><i class="demo-pli-old-telephone icon-lg icon-fw"></i>(123) 456 1234</p>
                                <p class="text-sm text-center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                                            
                            </div>
                            <div class="fluid">
                                <p class="pad-ver text-main text-sm text-uppercase text-bold">About Me</p>
                                <p><i class="demo-pli-map-marker-2 icon-lg icon-fw"></i> San Jose, CA</p>
                                <p><a href="#" class="btn-link"><i class="demo-pli-internet icon-lg icon-fw"></i> http://www.themeon.net</a></p>
                                <p><i class="demo-pli-old-telephone icon-lg icon-fw"></i>(123) 456 1234</p>
                                <p class="text-sm text-center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
            
            
                                <button class="btn btn-primary btn-block mar-ver">Load More</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--===================================================-->
        <!--END CONTENT CONTAINER-->
        
        <!--MAIN NAVIGATION-->
        <!--===================================================-->
        <nav id="mainnav-container">
            <?php $this->load->view('inc/sidebar_student');?>
        </nav>
        <!--===================================================-->
        <!--END MAIN NAVIGATION-->

    </div>

    <!-- FOOTER -->
    <!--===================================================-->
    <footer id="footer">
        <?php $this->load->view('inc/bottom/bottom_student'); ?>
    </footer>
    <!--===================================================-->
    <!-- END FOOTER -->

    <!-- SCROLL PAGE BUTTON -->
    <!--===================================================-->
    <button class="scroll-top btn">
        <i class="pci-chevron chevron-up"></i>
    </button>
    <!--===================================================-->
    <div class="mainnav-backdrop"></div>

</div>


    
    
    