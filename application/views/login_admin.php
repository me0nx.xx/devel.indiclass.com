<div class="site_preloader flex_center">
    <div class="site_preloader_inner">
      <div class="rect1"></div>
      <div class="rect2"></div>
      <div class="rect3"></div>
      <div class="rect4"></div>
      <div class="rect5"></div>
    </div>
</div>

<div class="styler">
    <div class="single_styler clearfix sticky_header">
        <h4>Header style</h4>
        <select class="wide">
            <option value="static">Enable header top</option>
            <option value="diseable_ht">Diseable header top</option>
        </select>
    </div>
    <div class="single_styler primary_color clearfix">
        <h4>Primary Color</h4>
        <div class="clearfix">
            <input id="primary_clr" type="text" class="form-control color_input" value="#f9bf3b" />
            <label class="form-control" for="primary_clr"><i class="fa fa-angle-down"></i></label>
        </div>
    </div>
    <div class="single_styler secound_color clearfix">
        <h4>Secoundary Color</h4>
        <div class="clearfix">
            <input id="secound_clr" type="text" class="form-control color_input" value="#19b5fe" />
            <label class="form-control" for="secound_clr"><i class="fa fa-angle-down"></i></label>
        </div>
    </div>
    <div class="single_styler layout_mode clearfix">
        <h4>Layout Mode</h4>
        <select class="wide">
            <option value="wide">Wide</option>
            <option value="boxed">Boxed</option>
            <option value="wide_box">Wide Boxed</option>
        </select>
    </div>
    <div class="layout_mode_dep">
        <div class="single_styler body_bg_style clearfix">
            <h4>Body Background style</h4>
            <select class="wide">
                <option value="img_bg">Image Background</option>
                <option value="solid">Solid Color</option>
            </select>
        </div>
        <div class="single_styler body_solid_bg clearfix">
            <h4>Select Background Color</h4>
            <div class="clearfix">
                <input id="body_bg_clr" type="text" class="form-control color_input" value="#d3d3d3" />
                <label class="form-control" for="body_bg_clr"><i class="fa fa-angle-down"></i></label>
            </div>
        </div>
    </div>
</div>
   
<div class="main_wrap">
    <?php $this->load->view('inc/navbar');?>
</div>

 <!-- 10. form_area -->
<div class="form_area sp single_page">
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6 form_h">
                <div class="contact_form">
                    <input type="text" id="type_regis" value="ortu" hidden> 
                    <div class="form-group clearfix">
                        <center>
                            <label style="color: gray; float: center; display: block; font-size: 22px;" id="alert_name_belakang">Admin Login</label>
                        </center>
                    </div>
                    <div class="form-group clearfix">
                        <label style="color: red; float: left; display: none; font-size: 18px;" id="alert_name_belakang">Password can not be empty</label>
                    </div>
                    <hr>
                    <div class="form-group clearfix">
                        <p>Email*</p>
                        <input type="email" required placeholder="Masukan email" name="email" id="email">
                        <label style="color: red; float: left; display: none;" id="alert_email">Email tidak boleh kosong</label>
                    </div>
                    <div class="form-group clearfix">
                        <p>Kata Sandi*</p>
                        <input type="password" placeholder="Masukan kata sandi" name="password" id="password">
                        <label style="color: red; float: left; display: none;" id="alert_password">Password tidak boleh kosong</label>
                    </div> 
                    <div class="form-group clearfix">
                        <button type="button" class="button hvr-bounce-to-right btn-block btn-sm" style="background-color: #1E8BC3;" id="button_login"> Masuk</button>                        
                    </div>                 
                    <hr>
                    <div class="form-group clearfix">
                        <center>
                            <p>Belum terdaftar?</p>
                            <a href="<?php echo BASE_URL();?>Daftar" style="cursor: pointer;"><label style="color: #19B5FE; float: center; display: block; font-size: 20px; cursor: pointer;">Daftar Akun</label></a>
                        </center>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        $(document.body).on('click', '#button_login' ,function(e){
            var email       = $('#email').val();
            var password    = $('#password').val();
            if (email=="") {
                $("#alert_password").css('display','none');
                $("#alert_email").css('display','block');
            }
            else if(password == "")
            {
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','block');                
            }
            else
            {                
                $.ajax({
                    url: '<?php echo AIR_API;?>channel_login',
                    type: 'POST',
                    data: {
                        username: email,
                        password : password,
                        channel_id : '52'
                    },
                    success: function(response)
                    {
                        var a = JSON.stringify(response);
                        if (response['code'] == 1) {                            
                            id_user = response['data']['id_user'];
                            user_name = response['data']['user_name'];
                            user_image = response['data']['user_image'];
                            email = response['data']['email'];
                            usertype_id = response['data']['usertype_id'];
                            status = response['data']['status'];
                            channel_id = response['data']['channel_id'];
                            access_token = response['access_token'];  
                            if (usertype_id == 'student') {
                                window.location.replace('<?php echo base_url();?>Admin/Logout');
                            }   
                            else
                            {                       
                                $.ajax({
                                    url: '<?php echo BASE_URL();?>First/setSession',
                                    type: 'POST',
                                    data: {
                                        id_user: id_user,
                                        user_name: user_name,
                                        user_image: user_image,
                                        email: email,
                                        usertype_id: usertype_id,
                                        status: status,
                                        channel_id: channel_id,
                                        access_token: access_token
                                    },
                                    success: function(response)
                                    {
                                        window.location.replace('<?php echo BASE_URL();?>Admin');
                                    }
                                });
                            }
                            $("#alertWrong").css('display','none');
                        }
                        else
                        {
                            $("#alertWrong").css('display','block');
                        }
                    }
                });                
            }            
        });

    });
</script>