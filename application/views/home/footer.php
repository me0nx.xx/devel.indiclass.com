<div class="container">
  <div class="col-md-12 col-sm-12">
    <!-- <div class="col-md-2"></div> -->
    <div class="col-md-6 col-sm-6 ms-footer-col" style="margin-top: 1%; margin-left: 0%;" align="left">
      <div class="ms-footbar-block">              
        <!-- <h3 class="ms-footbar-title"><?php echo $this->lang->line('bannertentang')?></h3>         -->
        <!-- <p>
            <a href="<?php echo base_url();?>syarat-ketentuan">
              <?php echo $this->lang->line('bannersyarat')?>
            </a>
        </p> -->
        <!-- <hr> -->
        <address class="no-mb" style=" margin-top: 0%;">
          <p style="font-size: 16px; color: #ff5100"><b>Indiclass - Belajar kini lebih mudah</b></p>
          <p style="font-size: 14px;">Indiclass adalah sebuah layanan edukasi online dari PT. Telkom Indonesia dengan teknologi tatap muka jarak jauh.</p>
          <!-- <p>
            <i class="color-danger-light zmdi zmdi-pin mr-1"></i> Jl. Godean Km.15, Klepu Sendangmulyo Minggir Sleman D.I.Yogyakarta 55562 Indonesia </p>
          <p> -->
          <!--   <i class="color-info-light zmdi zmdi-email mr-1"></i>
            <a href="mailto:info@classmiles.com">info@classmiles.com</a>
          </p> -->
          <!-- <p>
            <i class="color-royal-light zmdi zmdi-phone mr-1"></i>
            +62-274-2820775, +62-21-22538277
          </p>           -->
        </address>
      </div>      
    </div>
    <div class="col-md-6 col-sm-6 ms-footer-col" style="margin-top: 1%; margin-left: 0%;">
      <div class="ms-footbar-block">              
        <address class="no-mb" style="">
          <p style="font-size: 16px; color: #5fbe20"><b>Kontak Kami</b></p>
          <p style="">
            <i class="color-danger-light zmdi zmdi-pin mr-1"></i>&nbsp;&nbsp;Telkom Landmark Tower, Jl. Jendral Gatot Subroto Kav. 52
            <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jakarta Selatan, DKI Jakarta, 12710
            Indonesia </p>
          <p style="font-size: 13px;">
            <i class=" zmdi zmdi-email mr-1"></i>
            <a style="color: #ff2179" href="mailto:support@indiclass.id"><u>info@indiclass.id</u></a>
            <i class=" zmdi zmdi-phone mr-1" style="margin-left: 5%;"></i>
            <u style="color: #ff2179">147</u>            
          </p>
        </address>
      </div>      
    </div>
  </div>
</div>
<div class="col-md-12 col-sm-12" align="center" style="line-height: 100%; margin-top: 1%; background-color: #EEEEEE;">  
    <div class="col-md-12 col-sm-12">
        <p>Copyright &copy; Indiclass 2018</p>
    </div>
</div>