<div class="container container-full">
  <div class="ms-title">
    <a href="<?php echo base_url(); ?>">
      <!-- <img src="assets/img/demo/logo-header.png" alt=""> -->
      <img style="margin-top: 5%; width: 220px; height: 75px; " src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL.'logo/logolanding.png'; ?>" alt="">  
       
    </a>
  </div>
  <div class="header-right">
    <!-- SOCIAL -->
<!--     <a href="#" target="_blank" class="btn-ms-menu btn-circle btn-circle-primary color-white animated zoomInDown animation-delay-10">
      <i class="zmdi zmdi-google"></i>
    </a> -->
    <a href="https://www.facebook.com/classmiles" target="_blank" class="btn-ms-menu btn-circle btn-circle-primary color-white animated zoomInDown animation-delay-10">
      <i class="zmdi zmdi-facebook"></i>
    </a>
    <a href="https://twitter.com/classmiles" target="_blank" class="btn-ms-menu btn-circle btn-circle-primary color-white animated zoomInDown animation-delay-10">
      <i class="zmdi zmdi-twitter"></i>
    </a>
  
    <a href="#" id="btn_setindonesialp" class="btn_setindonesialp animated zoomInDown animation-delay-10">

      <img class="btn_setindonesialp m-t-5 animated zoomInDown animation-delay-10" width="34px" height="20px" style="cursor: pointer;  margin-left: 12px; margin-right: 12px;" src="<?php if ($this->session->userdata('lang') == "indonesia") { echo CDN_URL.STATIC_IMAGE_CDN_URL.'language/flaginggris.png'; } else { echo CDN_URL.STATIC_IMAGE_CDN_URL.'language/flagindo.png'; } ?>" /> 
    </a>

    <a data-toggle="modal"  data-target ="#login_modal" class="btn btn-raised btn-xs color-primary btn-white  animated zoomInDown animation-delay-10">
        <label  style="color:gray; font-size: 13px; padding-left: 6px; padding-right: 6px; padding-top: 5px; cursor: pointer;"><?php echo $this->lang->line('navbartop2')?></label>
    </a>

    <a href="<?php echo base_url(); ?>Register" class="btn btn-raised btn-xs btn-success animated zoomInDown animation-delay-10">
      <label style="color:white; font-size: 13px; padding-left: 6px; padding-right: 6px; padding-top: 5px; cursor: pointer;"><?php echo $this->lang->line('navbartop1')?></label>
    </a>

  </div>
</div>



<script type="text/javascript">
   $('.btn_setindonesialp').click(function(e){
              e.preventDefault();    
              var lang  = "<?php echo $this->session->userdata('lang'); ?>";
              if (lang == "indonesia") {         
                $.get('<?php echo base_url('set_lang/english'); ?>',function(hasil){  location.reload(); });
              } else {
                  $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
              }
               
          });
         
</script>