<div class="navbar-wrapper">
	<div class="navbar navbar-main" id="fixed-navbar">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 column-header">
					<div class="navbar-header">
						<!-- Brand -->
						<a href="#" class="navbar-brand">
							<img src="../assets/img/logos.png" style="width: 150px; height: 60px; margin-top: -10%;" alt="Indiclass">
						</a>
						<!-- Mobile Navigation -->
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
				            <span class="sr-only">Toggle navigation</span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
						</button>
					</div><!-- /navbar header -->   

					<!-- Main Navigation - Explained in Documentation -->
					<nav class="navbar-collapse collapse navHeaderCollapse" role="navigation">
						<ul class="nav navbar-nav navbar-right">
							<li class="<?php if($sideactive == 'index'){ echo '';} else { echo ''; }?>">
					        	<a href="<?php echo BASE_URL();?>">Beranda</a>					        	
					        </li>
					        <li class="<?php if($sideactive == 'tentangkami'){ echo '';} else { echo ''; }?>">
					        	<a href="<?php echo BASE_URL();?>Tentang-Indiclass">Tentang Kami</a>					        	
					        </li>
					        <li class="<?php if($sideactive == 'siswa'){ echo '';} else { echo ''; }?>">
					        	<a href="<?php echo BASE_URL();?>Siswa">Siswa</a>					        	
					        </li>
					        <li class="<?php if($sideactive == 'pengajar'){ echo '';} else { echo ''; }?>">
					        	<a href="<?php echo BASE_URL();?>Pengajar">Pengajar</a>					        	
					        </li>
					        <li class="<?php if($sideactive == 'kontakkami'){ echo '';} else { echo ''; }?>">
					        	<a href="<?php echo BASE_URL();?>Kontak-kami">Kontak Kami</a>					        	
					        </li>
						</ul>
					</nav><!-- /nav -->
				</div>
			</div>
		</div><!-- /container header -->   
	</div><!-- /navbar -->
</div><!-- /navbar wrapper -->

<?php 
	if($sideactive!="index"){
		?>
		<div class="header">
		    <div class="container">
		        <div class="row">
		            <!-- Page Title -->
		            <div class="col-sm-6 col-xs-12">
		                <h1><?php if($sideactive=="index"){ echo "Beranda";} else if($sideactive=="tentangkami"){ echo "Tentang Kami";} else if($sideactive=="siswa"){ echo "Siswa";} else if($sideactive=="pengajar"){ echo "Pengajar";} else if($sideactive=="kontakkami"){ echo "Kontak Kami";} else{ echo "Indiclass"; } ?></h1>
		            </div>

		            <!-- Breadcrumbs -->
		            <div class="col-sm-6">
		                <ol class="breadcrumb">
		                    <li><span class="fa fa-home breadcrumb-home"></span><a href="<?php echo BASE_URL();?>">Beranda</a></li>                    
		                    <li><?php if($sideactive=="index"){ echo "Beranda";} else if($sideactive=="tentangkami"){ echo "Tentang Kami";} else if($sideactive=="siswa"){ echo "Siswa";} else if($sideactive=="pengajar"){ echo "Pengajar";} else if($sideactive=="kontakkami"){ echo "Kontak Kami";} else if($sideactive=="signup"){ echo "Daftar Siswa";} else if($sideactive=="signup_tutor"){ echo "Daftar Pengajar";} else{ echo "Indiclass"; } ?></li>
		                </ol>
		            </div><!-- /column -->
		        </div><!-- /row -->  
		    </div><!-- /container -->  
		</div><!-- /page header --> 
		<?php
	}
?>