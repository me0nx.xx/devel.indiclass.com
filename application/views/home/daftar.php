<div id="royal_preloader"></div>	

<!-- Begin Boxed or Fullwidth Layout -->
<div id="bg-boxed">
    <div class="boxed">

		<!-- Begin Header -->
		<header>
			<?php $this->load->view('home/top_nav');?>
		</header><!-- /header -->
		<!-- End Header -->

		<!-- Begin Content -->
		<div class="content-40mg">
			<div class="container">

				<div class="row">

					<!-- Begin Register -->
					<div class="col-sm-12 mt30-xs">
						<div class="panel no-margin panel-default">
						    <div class="panel-heading">Mendaftar Akun Siswa</div>
						    <div class="panel-body">

						    	<div class="col-md-4 col-xs-4 col-sm-4">
				                    <div class="form_text text-center">
				                        <div id="selected_signup1">
				                            <h4 class="cPrimary" id="focus1">Siswa daftar disini</h4>
				                            <div class="col-md-12" style="background-color: transparent; cursor: pointer; margin-top: 5%;">
				                                <center>
				                                    <img src="https://cdn.classmiles.com/sccontent/baru/logo_baru/Mahasiswa-Umum.png" style="height: 45%; width: 100%;">
				                                    <!-- <h4 style="color: #6C7A89; margin-top: 5%;">General</h4> -->
				                                </center>                    
				                            </div>
				                        </div>
				                        
				                        <div id="selected_signup1" style="margin-top: 300px;">                            
				                            <a href="<?php echo BASE_URL();?>DaftarPengajar">
				                                <hr>
				                                <br><br>
				                                <h4 class="cPrimary" id="focus1">Daftar Tutor</h4>
				                                <div class="col-md-12" style="background-color: transparent; cursor: pointer; margin-top: 0%;">
				                                    <center>
				                                        <!-- <img src="https://cdn.classmiles.com/sccontent/baru/logo_baru/Mahasiswa-Umum.png" style="height: 45%; width: 45%;"> -->
				                                        <h4 style="color: #6C7A89; margin-top: 5%;">Anda ingin mendaftar menjadi pengajar ? <br/><br/>Klik disini</h4>
				                                    </center>                    
				                                </div>
				                            </a>
				                        </div>
				                    </div>
				                </div>
				                <div class="col-md-8 col-xs-8 col-sm-8">
				                	<!-- NAMA -->
				                	<div class="col-md-6 col-xs-6 col-sm-6">						        
							            <div class="form-group">
							                <div class="input-group">
							                    <div class="input-group-addon"><span class="fa fa-user fa-7x"></span></div>
							                    <input type="text" class="form-control" id="nama_depan" placeholder="Isi nama depan">							                    
							                </div>
							                <label style="color: red; float: left; font-weight: 100; display: none;" id="alert_nama_depan">Nama depan tidak boleh kosong</label>
							            </div>
							        </div>
							        <div class="col-md-6 col-xs-6 col-sm-6">
							            <div class="form-group">
							            	<div class="input-group">
							                    <div class="input-group-addon"><span class="fa fa-user fa-7x"></span></div>
							                	<input type="text" class="form-control" id="nama_belakang" placeholder="Isi nama belakang">
							                </div>
							                <label style="color: red; float: left; font-weight: 100; display: none;" id="alert_name_belakang">Nama belakang tidak boleh kosong</label>
							            </div>
							        </div>
							        <!-- EMAIL -->
							        <div class="col-md-12 col-xs-12 col-sm-12">
							            <div class="form-group">
							            	<div class="input-group">
							                    <div class="input-group-addon"><span class="fa fa-envelope fa-7x"></span></div>
							                	<input type="email" class="form-control" id="email" placeholder="Isi email anda">
							                </div>
							                <label style="color: red; float: left; font-weight: 100; display: none;" id="alert_email">Email tidak boleh kosong</label>
							            </div>
						            </div>
						            <!-- KATA SANDI -->
						            <div class="col-md-6 col-xs-6 col-sm-6">						        
							            <div class="form-group">
							                <div class="input-group">
							                    <div class="input-group-addon"><span class="fa fa-key fa-7x"></span></div>
							                    <input type="password" class="form-control" id="password" placeholder="Isi kata sandi">							                    
							                </div>
							                <label style="color: red; float: left; font-weight: 100; display: none;" id="alert_password">Kata sandi tidak boleh kosong</label>
							            </div>
							        </div>
							        <div class="col-md-6 col-xs-6 col-sm-6">
							            <div class="form-group">
							            	<div class="input-group">
							                    <div class="input-group-addon"><span class="fa fa-key fa-7x"></span></div>
							                	<input type="password" class="form-control" id="password_cek" placeholder="Isi konfirm kata sandi">
							                </div>
							                <label style="color: red; float: left; font-weight: 100; display: none;" id="alert_password_cek">Konfirm kata sandi tidak boleh kosong</label>
							            </div>
							        </div>
							        <!-- TANGGAL LAHIR -->
							        <div class="col-md-4 col-xs-4 col-sm-4">						        
							            <div class="form-group">
							            	<div class="input-group">
							                    <div class="input-group-addon"><span class="fa fa-calendar fa-7x"></span></div>
								                <select class="form-control checkout-form-border" id="date">
								                	<option disabled selected value="">Pilih Tanggal</option>
													<?php 
				                                        for ($date=01; $date <= 31; $date++) {
				                                            ?>                                                  
				                                            <option value="<?php echo $date ?>"><?php echo $date ?></option>                                                  
				                                            <?php 
				                                        }
				                                    ?>
												</select>
											</div>
							                <label style="color: red; float: left; font-weight: 100; display: none;" id="alert_tanggal">Tanggal tidak boleh kosong</label>
							            </div>
							        </div>
							        <div class="col-md-4 col-xs-4 col-sm-4">
							            <div class="form-group">
							            	<div class="input-group">
							                    <div class="input-group-addon"><span class="fa fa-calendar fa-7x"></span></div>
								                <select class="form-control checkout-form-border" id="month">
								                	<option disabled selected value="">Pilih bulan</option>
				                                    <option value="01">January</option>
				                                    <option value="02">February</option>
				                                    <option value="03">March</option>
				                                    <option value="04">April</option>
				                                    <option value="05">May</option>
				                                    <option value="06">June</option>
				                                    <option value="07">July</option>
				                                    <option value="08">August</option>
				                                    <option value="09">September</option>
				                                    <option value="10">October</option>
				                                    <option value="11">November</option>
				                                    <option value="12">December</option>
												</select>
											</div>
							                <label style="color: red; float: left; font-weight: 100; display: none;" id="alert_bulan">Bulan tidak boleh kosong</label>
							            </div>
							        </div>
							        <div class="col-md-4 col-xs-4 col-sm-4">
							            <div class="form-group">
							            	<div class="input-group">
							                    <div class="input-group-addon"><span class="fa fa-calendar fa-7x"></span></div>
								                <select class="form-control checkout-form-border" id="year">
								                	<option disabled selected>Pilih tahun</option>
				                                    <?php 
				                                    for ($i=1980; $i <= 2018; $i++) {
				                                        ?>                                                 
				                                          <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
				                                        <?php 
				                                    } 
				                                    ?>
												</select>
											</div>
							                <label style="color: red; float: left; font-weight: 100; display: none;" id="alert_tahun">Tahun tidak boleh kosong</label>
							            </div>
							        </div>
							        <!-- JENIS KELAMIN & COUNTRY -->
				                	<div class="col-md-6 col-xs-6 col-sm-6">						        
							            <div class="form-group">
							                <div class="input-group">
							                    <div class="input-group-addon"><span class="fa fa-venus-mars fa-7x"></span></div>
								                <select class="form-control checkout-form-border" id="gender">
								                	<option disabled selected value="">Pilih jenis kelamin</option>
				                                    <option value="Male">Pria</option>
				                                    <option value="Female">Wanita</option>
												</select>
											</div>
							                <label style="color: red; float: left; font-weight: 100; display: none;" id="alert_jenis_kelamin">Jenis kelamin tidak boleh kosong</label>
							            </div>
							        </div>
							        <div class="col-md-6 col-xs-6 col-sm-6">
							            <div class="form-group">
							            	<div class="input-group">
							                    <div class="input-group-addon"><span class="fa fa-flag fa-7x"></span></div>
								                <select class="form-control checkout-form-border" id="name_country">
								                	<option disabled selected value="">Pilih Kota</option>
				                                    
												</select>
											</div>
							                <label style="color: red; float: left; font-weight: 100; display: none;" id="alert_nama_negara">Harap pilih warga negara</label>
							            </div>
							        </div>
							        <!-- KODE AREA & NOMER HP -->
				                	<div class="col-md-6 col-xs-6 col-sm-6">						        
							            <div class="form-group">
							                <div class="input-group">
							                    <div class="input-group-addon"><span class="fa fa-flag fa-7x"></span></div>
								                <select class="form-control checkout-form-border" id="kode_area">
								                	<option disabled selected value="" >Pilih kode area</option> 
				                                    
												</select>
											</div>
							                <label style="color: red; float: left; font-weight: 100; display: none;" id="alert_kode_area">Harap pilih kode area</label>
							            </div>
							        </div>
							        <div class="col-md-6 col-xs-6 col-sm-6">
							            <div class="form-group">
							            	<div class="input-group">
							                    <div class="input-group-addon"><span class="fa fa-phone fa-7x"></span></div>
							                    <input class="form-control" type="text" placeholder="Contoh : 382 437" onkeypress="return hanyaAngka(event)" name="no_hape" id="no_hape">
							                </div>
							                <label style="color: red; float: left; font-weight: 100; display: none;" id="alert_no_hape">Nomer hp tidak boleh kosong</label>
							            </div>
							        </div>
						            <hr class="mt20 mb20">
						            <div class="col-md-12 col-xs-12 col-sm-12 mt20">
						            	<button type="button" class="btn btn-rw btn-primary" id="daftar"><i class="fa fa-share-square-o"></i> Daftar Sekarang</button>
						            </div>
						        </div>

						    </div>
						</div>
					</div>
					<!-- End Register -->

					

				</div><!-- /row -->

			</div><!-- /container -->
		</div><!-- /content -->
		<!-- End Content -->

		<div class="form_area sp single_page">
	        <div class="container">
	            <div class="row">
	                
	            </div>
	        </div>
	    </div>

		<!-- Begin Footer -->
		<footer class="footer-light">
		    <?php $this->load->view('home/bottom');?>
		</footer><!-- /footer -->
		<!-- End Footer --> 

	</div><!-- /boxed -->
</div><!-- /bg boxed-->
<!-- End Boxed or Fullwidth Layout -->

<div class="modal" tabindex="-1" id="alert_joinmodal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="alert_title_joinmodal"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="alert_message_joinmodal"></p>
            </div>
            <div class="modal-footer" id="footer1">                    
                <button class="btn btn-default" id="tolakChannel">Tidak</button>
                <button class="btn btn-info" id="joinChannel">Ya</button>                    
            </div>   
            <div class="modal-footer" id="footer2" style="display: none;">
                <button class="btn btn-info" id="okChannel">Ok</button>
            </div>     
        </div>
    </div>
</div>

<script>
    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }
    function validAngka(a)
    {
        if(!/^[0-9.]+$/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
      function validPhone(a)
    {
        if(!/(^0$)|(^[1-9]\d{0,12}$)/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
</script>
<script type="text/javascript">

    $(document).ready(function() {
        var channel_id = '52';

        $.ajax({
            url: '<?php echo AIR_API;?>ListCountry',
            type: 'POST',
            success: function(response)
            {
                var a = JSON.stringify(response);
                var geoNow = window.localStorage.getItem('geo_location');                                    
                for (var ii = 0; ii < response.list_country.length; ii++) {
                    var nicename = response.list_country[ii]['nicename'];
                    var phonecode = response.list_country[ii]['phonecode'];
                    var iso = response.list_country[ii]['iso'];
                    
                    if (geoNow == iso) {
                        $('#name_country').append("<option value='"+nicename+"' selected='true'>"+nicename+"</option>");
                        $('#kode_area').append("<option value='"+nicename+"' selected='true'>"+nicename+" +"+phonecode+"</option>");
                    }
                    else
                    {
                        $('#name_country').append("<option value='"+nicename+"'>"+nicename+"</option>");
                        $('#kode_area ').append("<option value='"+nicename+"'>"+nicename+" +"+phonecode+"</option>");
                    }
                }
            }
        });

        //CEK PASSWORD UMUM
        $('#password').on('keyup',function(){
            if($('#password_cek').val() != $(this).val()){
                $('#daftar').attr('disabled','disabled');                                      
                $('#alert_password_cek').html('Password does not match').css('display', 'block');
            }else{                      
                $('#alert_password_cek').html('').css('display', 'none');
                $("#daftar").removeAttr('disabled');
            }
        });

        $('#password_cek').on('keyup',function(){
            if($('#password').val() != $(this).val() ){
                $('#daftar').attr('disabled','disabled');
                $('#alert_password_cek').html('Password does not match').css('display', 'block');
            }else{
                $('#alert_password_cek').html('').css('display', 'none');
                $("#daftar").removeAttr('disabled');
            }
        });

        function goToByScroll(id){            
              // Scroll
            $('html,body').animate({
                scrollTop: $("#"+id).offset().top-430},
                'slow');
        }        

        $("#daftar").click(function(){
            $("#daftar").attr('disabled','disabled');            
            var name_depan     = $("#nama_depan").val();
            var name_belakang  = $("#nama_belakang").val();
            var email          = $("#email").val();
            var password       = $("#password").val();
            var password_cek   = $("#password_cek").val();
            var tanggal        = $("#date").val();            
            var bulan          = $("#month").val();
            var tahun          = $("#year").val();
            var jenis_kelamin  = $("#gender").val();
            var nama_negara    = $("#name_country").val();            
            var kode_area      = $("#kode_area").val();
            var no_hape        = $("#no_hape").val();  
            if (name_depan == "") {
                $("#alert_nama_depan").css('display','block');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("nama_depan");
            }
            else if (name_belakang == "") {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','block');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("nama_belakang");
            }
            else if (email == "") {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','block');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("email");
            }
            else if (password == "") {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','block');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("password");
            }
            else if (password_cek == "") {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','block');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("password_cek");
            }
            else if (tanggal == null) {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','block');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("date");
            }
            else if (bulan == null) {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','block');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("month");
            }
            else if (tahun == null) {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','block');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("year");
            }
            else if (jenis_kelamin == null) {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','block');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("gender");
            }
            else if (nama_negara == null) {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','block');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("name_country");
            }
            else if (kode_area == null) {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','block');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#daftar").removeAttr('disabled');
                goToByScroll("kode_area");
            }
            else if (no_hape == "") {
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_jenjang").css('display','none');
                $("#alert_no_hape").css('display','block');
                $("#daftar").removeAttr('disabled');
                goToByScroll("no_hape");
            }
            else
            {                	
                $("#alert_nama_depan").css('display','none');
                $("#alert_name_belakang").css('display','none');
                $("#alert_email").css('display','none');
                $("#alert_password").css('display','none');
                $("#alert_password_cek").css('display','none');
                $("#alert_tanggal").css('display','none');
                $("#alert_bulan").css('display','none');
                $("#alert_tahun").css('display','none');
                $("#alert_jenis_kelamin").css('display','none');
                $("#alert_nama_negara").css('display','none');
                $("#alert_kode_area").css('display','none');
                $("#alert_no_hape").css('display','none');
                $("#alert_jenjang").css('display','none');

                $("#daftar").attr('disabled','disabled');
                $("#daftar").attr('background-color','grey');
                $.ajax({
                    url: '<?php echo AIR_API;?>Channel_daftarMurid',
                    type: 'POST',
                    data: {
                        channel_id: channel_id,
                        first_name: name_depan,
                        last_name: name_belakang,
                        email: email,
                        kata_sandi: password,
                        tanggal_lahir: tanggal,
                        bulan_lahir: bulan,
                        tahun_lahir: tahun,
                        jenis_kelamin: jenis_kelamin,
                        kode_area: kode_area,
                        nama_negara: nama_negara,
                        no_hape: no_hape,
                        type: 'umum',
                        linkweb : '<?php echo BASE_URL();?>'
                    },
                    success: function(response)
                    {                             
                        var code = response['code'];
                        
                        if (code == 200) {
                            $("#alert_title_modal").text("General Registration Success");
                            $("#alert_message_modal").text("Thank you for registering, please check your email for activation link.");
                            $("#alert_modal").modal("show");
                            setTimeout(function(){
                                location.reload();
                            },5000);
                        }
                        else if(code == -102){
                            var id_user = response['id_user'];
                            var user_type = response['user_type'];
                            
                            $("#alert_title_joinmodal").text("Pemberitahuan");
                            if (user_type == 'tutor') {
                                $("#alert_message_joinmodal").text("Email atau akun Anda sudah terdaftar di Classmiles sebagai PENGAJAR. Dan website ini ber-afiliasi dengan Classmiles. Silakan gunakan email atau akun lain untuk mendaftar sebagai SISWA di website ini");                                
                                $("#footer1").css('display','none');
                                $("#footer2").css('display','block');
                            }
                            else
                            {
                                $("#alert_message_joinmodal").text("Email atau akun Anda sudah terdaftar di Classmiles. Dan website ini ber-afiliasi dengan Classmiles. Apakah Anda ingin menggunakan email atau akun yang sama untuk bisa masuk ke website ini?");

                                $("#joinChannel").attr('id_user',id_user);
                                $("#joinChannel").attr('user_type', user_type);                                
                                $("#footer2").css('display','none');
                                $("#footer1").css('display','block');
                            }                                                        
                            $("#alert_joinmodal").modal("show");
                        }
                        else if(code == -100){
                            $("#alert_title_modal").text("Registration failed"); 
                            $("#alert_message_modal").text("Please try again.");
                            $("#alert_modal").modal("show");
                            setTimeout(function(){
                                location.reload();
                            },5000);
                        }
                        else
                        {
                            $("#alert_title_modal").text("There is an error");
                            $("#alert_message_modal").text("Please try again.");
                            $("#alert_modal").modal("show");
                            setTimeout(function(){
                                location.reload();
                            },5000);
                        }                        
                    }
                });
            }
        });
		
		$(document.body).on('click', '#joinChannel' ,function(e){
            var id_user = $(this).attr('id_user');
            var user_type = $(this).attr('user_type');
            $("#alert_joinmodal").modal('hide');
            $.ajax({
                url: '<?php echo AIR_API;?>Channel_joindaftarMurid',
                type: 'POST',
                data: {
                    channel_id: channel_id,
                    id_user: id_user,
                    user_type: user_type
                },
                success: function(response)
                {                             
                    var code = response['code'];
                    
                    if (code == 200) {
                        $("#alert_title_modal").text("Sukses"); 
                        $("#alert_message_modal").text("Terima kasih. Anda telah terdaftar di Fisipro.com. Silahkan login untuk memulai kelas.");
                        $("#alert_modal").modal("show");
                        setTimeout(function(){
                            location.reload();
                        },5000);
                    }
                    else
                    {
                        $("#alert_title_modal").text("Registrasi gagal"); 
                        $("#alert_message_modal").text("Akun anda telah terdaftar menjadi siswa di channel ini.");
                        $("#alert_modal").modal("show");
                        setTimeout(function(){
                            location.reload();
                        },5000);
                    }
                }
            });
        });

        $(document.body).on('click', '#tolakChannel' ,function(e){
            location.reload();
        });

        $(document.body).on('click', '#okChannel' ,function(e){
            location.reload();
        });

	});

</script>