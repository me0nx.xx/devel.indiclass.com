<div id="royal_preloader"></div>	

<!-- Begin Boxed or Fullwidth Layout -->
<div id="bg-boxed">
    <div class="boxed">

		<!-- Begin Header -->
		<header>
			<?php $this->load->view('home/top_nav');?>
		</header><!-- /header -->
		<!-- End Header -->

		<!-- Begin Content -->
		<div class="content-40mg">
			<div class="container">
				<div class="row">

					<!-- Begin Content Section -->
					<section class="col-lg-12 col-md-12 col-sm-12">
						<!-- Content 1 -->
						<p>Bagaimana prosedur menjadi bagian di Indiclass ? disini akan dijelaskan step by step agar anda bisa menjadi siswa Indiclass.</p>

						<!-- Accordion -->                        
						<div class="panel-group mt20 no-margin" id="accordion">
						    <!-- Item 1 -->
						    <div class="panel panel-default">
						        <div class="panel-heading">
						          	<h4 class="panel-title">
						            	<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
						            		1. Daftar?
						            	</a>
						          	</h4>
						        </div>
						        <div id="collapseOne" class="panel-collapse collapse in">
						            <div class="panel-body">
					            		<!-- <img src="images/help.png" class="img-responsive pull-left shadow1 mr20" style="width:200px;" alt="Help"> -->

					            		<p><h4><strong>Bagaimana cara mendaftar ?</strong></h4></p>

										<p class="no-margin">Mengunjungi halaman daftar Indiclass, lalu mengisi data diri hingga mendapatkan email verifikasi dari pihak kami.</p>
						            </div>
						        </div>
						    </div><!-- /panel -->

						    <!-- Item 2 -->
						    <div class="panel panel-default">
						        <div class="panel-heading">
						          	<h4 class="panel-title">
							            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
							            	2. Terima kasih telah mendaftar
							            </a>
						          	</h4>
						        </div>
						        <div id="collapseTwo" class="panel-collapse collapse">
						            <div class="panel-body">
						                <h4><span class="fa fa-envelope-o bordered-icon-sm mr15"></span> Verifikasi Email.</h4>
											<p class="no-margin">Setelah mengisi data diri untuk pendaftaran. Anda akan diminta memverifikasikan data diri anda yang sudah kami kirimkan ke email anda.</p>
						            </div>
						        </div>
						    </div><!-- /panel -->

						    <!-- Item 3 -->
						    <div class="panel panel-default">
						        <div class="panel-heading">
						          	<h4 class="panel-title">
							            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
							            	3. Siswa Login
							            </a>
						          	</h4>
						        </div>
						        <div id="collapseThree" class="panel-collapse collapse">
						            <div class="panel-body">
										<p>Masuk untuk mengikuti berbagai macam kelas virtual yang sudah kami sediakan.</p>										
						            </div>
						        </div>
						    </div><!-- /panel -->

						    <!-- Item 4 -->
						    <div class="panel panel-default">
						        <div class="panel-heading">
						          	<h4 class="panel-title">
							            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
							            	4. Portal Siswa (siswa dashboard)
							            </a>
						          	</h4>
						        </div>
						        <div id="collapseFour" class="panel-collapse collapse">
						            <div class="panel-body">
   										<p>Anda akan diarahkan ke portal siswa, yang anda bisa atur semua jadwal atau ingin mengikuti kelas dari pengajar mana saja.</p>   										
						            </div>
						        </div>
						    </div><!-- /panel -->
						    
						</div><!-- /accordion -->      
						<!-- End Content 1 -->
					</section>
					<!-- End Content Section -->					

				</div><!-- /row -->
			</div><!-- /container -->
		</div><!-- /content -->
		<!-- End Content -->

		<!-- Begin Footer -->
		<footer class="footer-light">
		    <?php $this->load->view('home/bottom');?>
		</footer><!-- /footer -->
		<!-- End Footer --> 

	</div><!-- /boxed -->
</div><!-- /bg boxed-->
<!-- End Boxed or Fullwidth Layout -->
