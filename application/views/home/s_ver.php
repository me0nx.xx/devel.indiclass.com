<script type="text/javascript">
    var a = 0;
    setTimeout(function(){
        a = 1;
        if (a == 0) {
            window.location.reload();
        }
    },200);
</script>
<div id="ms-preload" class="ms-preload">
    <div id="status">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>

<?php
    if ($data == 1) {
        ?>
        <div class="bg-full-page bg-primary back-fixed" style="background-image: url('http://www.jcpportraits.com/sites/jcpportraits.com/files/styles/portrait_background/public/background/1606/background_teal-white_floor_0.jpg?itok=0_sl0RaM'); background-repeat: no-repeat; background-size: 100% 100%; overflow: hidden;">
            <div class="mw-500 absolute-center" style="">
                <div class="card animated zoomInUp animation-delay-7 color-primary withripple">
                    <div class="card-block">
                        <div class="text-center color-dark">
                            <img src="https://cdn.dribbble.com/users/4874/screenshots/1776423/inboxiconanimation_30.gif" style="height: 120px; width: 150px;">
                            <h2>Pemberitahuan</h2>
                            <!-- <h2>Page Not Found</h2> -->
                            <p class="lead lead-sm">Email anda sudah terverifikasi. Silahkan masuk untuk mencoba fitur classmiles.</p>
                            <a class="btn btn-info btn-raised buttonlogin">
                            Masuk</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    else if($data == 900)
    {
        ?>
        <div class="bg-full-page bg-primary back-fixed" style="background-image: url('http://www.jcpportraits.com/sites/jcpportraits.com/files/styles/portrait_background/public/background/1606/background_teal-white_floor_0.jpg?itok=0_sl0RaM'); background-repeat: no-repeat; background-size: 100% 100%; overflow: hidden;">
            <div class="mw-500 absolute-center" style="">
                <div class="card animated zoomInUp animation-delay-7 color-primary withripple">
                    <div class="card-block">
                        <div class="text-center color-dark">
                            <img src="https://cdn.dribbble.com/users/4874/screenshots/1776423/inboxiconanimation_30.gif" style="height: 120px; width: 150px;">
                            <h2>Konfirmasi Email</h2>
                            <!-- <h2>Page Not Found</h2> -->
                            <p class="lead lead-sm">Selamat, E-mail
                            <br>Anda sudah berhasil dikonfirmasi. Silahkan masuk untuk mencoba fitur classmiles.</p>
                            <a class="btn btn-info btn-raised buttonlogin">
                            Masuk</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    else if($data == 901)
    {
        ?>
        <div class="bg-full-page bg-primary back-fixed" style="background-image: url('http://www.jcpportraits.com/sites/jcpportraits.com/files/styles/portrait_background/public/background/1606/background_teal-white_floor_0.jpg?itok=0_sl0RaM'); background-repeat: no-repeat; background-size: 100% 100%; overflow: hidden;">
            <div class="mw-500 absolute-center" style="">
                <div class="card animated zoomInUp animation-delay-7 color-primary withripple">
                    <div class="card-block">
                        <div class="text-center color-dark">
                            <img src="https://cdn.dribbble.com/users/4874/screenshots/1776423/inboxiconanimation_30.gif" style="height: 120px; width: 150px;">
                            <h2>Konfirmasi Email</h2>
                            <!-- <h2>Page Not Found</h2> -->
                            <p class="lead lead-sm">Selamat, E-mail
                            <br>Anda sudah berhasil dikonfirmasi. Mohon segera melengkapi data diri, riwayat pendidikan dan pengalaman mengajar yang selanjutnya diajukan untuk permintaan persetujuan menjadi Tutor Classmiles.</p>
                            <a class="btn btn-info btn-raised buttonlogin">
                            Masuk</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
?>  

<script type="text/javascript">
    $(document).ready(function() {
        
        $(document).on("click", ".buttonlogin", function () {            
            $.ajax({
                url: '<?php echo base_url(); ?>Master/setsessionver',
                type: 'GET',   
                data: {
                    code: '109'
                },             
                success: function(data)
                { 
                    window.location='<?php echo base_url();?>';
                }
            });
        });

    });
</script>
