<div id="royal_preloader"></div>	

<!-- Begin Boxed or Fullwidth Layout -->
<div id="bg-boxed">
    <div class="boxed">

		<!-- Begin Header -->
		<header>			
			<?php $this->load->view('home/top_nav');?>
		</header><!-- /header -->
		<!-- End Header -->

		<!-- Begin Map -->
		<!-- <div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 no-padding" style="margin-bottom: -7px;">
					<iframe style="border: 0px none; border-color:#fff; width:100%;" height="350" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d106730.4224695816!2d106.79724852781186!3d-6.2117796065896576!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3e945e34b9d%3A0x5371bf0fdad786a2!2sJakarta%2C+Daerah+Khusus+Ibukota+Jakarta!5e0!3m2!1sid!2sid!4v1536806821033"></iframe>
				</div>
			</div>
		</div> -->
		<!-- End Map -->

		<!-- Begin Contact -->
		<section class="mt40 mb40">
			<div class="container">

				<!-- Form + Sidebar -->
				<div class="row">
					<div class="col-sm-8">
						<div class="heading mb20"><h4><span class="fa fa-address-book mr15"></span>Kontak</h4></div>
						<p class="mb20">Halo! Apakah Anda sudah menjadi Siswa Indiclass? Jika Anda memiliki pertanyaan, usulan, atau masalah terkait Indiclass, silakan atau memiliki pertanyaan umum. Email kami ke info@indiclass.com.</p>	
						<iframe style="border: 0px none; border-color:#fff; width:100%;" height="350" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d106730.4224695816!2d106.79724852781186!3d-6.2117796065896576!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3e945e34b9d%3A0x5371bf0fdad786a2!2sJakarta%2C+Daerah+Khusus+Ibukota+Jakarta!5e0!3m2!1sid!2sid!4v1536806821033"></iframe>					
					</div>
					<div class="col-sm-4 mt30-xs">
						<div class="content-box content-box-primary mb30">
					        <span class="fa fa-phone fa-lg text-white border-white bordered-icon-static-sm mb10"></span>
					        <h2 class="text-white no-margin">+62-274-370937</h2>
					    </div>
						<div class="panel panel-primary no-margin">
						    <div class="panel-heading">
						        <h3 class="panel-title"><span class="fa fa-home"></span> Information</h3>
						    </div>
						    <div class="panel-body">
						        <address class="no-margin">
		                            <strong>Telkom Landmark Tower, Lantai 39.</strong><br>
		                            Jl. Jendral Gatot Subroto Kav. 52<br>
		                            RT.6/RW.1, Kuningan Barat, Mampang Prapatan<br>
		                            Jakarta Selatan, DKI Jakarta, 12710<br>
		                            Indonesia<br/>
		                            <!-- <abbr title="Phone">P:</abbr> (123) 456-7890 <br> -->
		                            Mail: <a href="#">info@indiclass.com</a>
		                        </address>
						    </div>
						</div>
					</div>
				</div>

			</div><!-- /container -->
		</section><!-- /section -->
		<!-- End Contact -->

		<!-- Begin Footer -->
		<footer class="footer-light">
		    <?php $this->load->view('home/bottom');?>
		</footer><!-- /footer -->
		<!-- End Footer --> 

	</div><!-- /boxed -->
</div><!-- /bg boxed-->
<!-- End Boxed or Fullwidth Layout -->
