<?php $this->load->view('inc/navbar'); ?>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('inc/sideadmin'); ?>
		<?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
		?>
	</aside>

	<section id="content">
		<div class="container">            
			<div class="block-header">
				<h2><?php echo $this->lang->line('aktivasiuser');?></h2>
			</div> <!-- akhir block header    -->        		
			<!-- <div class="card">
				
				<div class="card-header">
					<h2><?php echo $this->lang->line('aktivasiuser'); ?></h2>
				</div>
				
				<?php 
				foreach ($alldatatutor as $row => $v) {
					?>
					<div class="col-sm-4 m-t-20" >
						<div class="card profile-tutor" id="card_<?php echo $v['id_user']; ?>"  style="cursor: hand; cursor: pointer;">
							data-toggle="modal" href="#modalDefault"
							<div class="pv-header" data-toggle="modal" id="modalWider">
								<img src="<?php echo base_url('aset/img/user/'.$v['user_image']); ?>" class="pv-main" alt="">
							</div>

							<div class="pv-body">
								<h2><?php echo $v['user_name']; ?></h2>
							</div>
						</div>
					</div>
					<?php
				}
				?>

			</div> -->	

			<div class="card m-t-20 p-20" style="">

                <div class="row" style="margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Jenis kelamin</th>
                                        <th>Umur</th>
                                        <th>Kota asal</th>
                                        <th>No HP</th>
                                        <th>Referral Code</th>
                                        <th>Photo</th>                                        
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;
                                    foreach ($alldatatutor as $row => $v) { 
                                    	$idkabupaten 	= $v['id_kabupaten'];
                                    	$namakab 		= $this->db->query("SELECT kabupaten_name FROM master_kabupaten WHERE kabupaten_id='$idkabupaten'")->row_array()['kabupaten_name'];
                                    	$foto  			= $v['user_image'];
                                    	$referralid		= $v['referral_id'];
                                    	$referralcode	= $this->db->query("SELECT referral_code FROM master_referral WHERE referral_id='$referralid'")->row_array()['referral_code'];
                                    	 
                                        ?>   

                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['user_name']; ?></td>
                                            <td><?php echo $v['email']; ?></td>
                                            <td><?php echo $v['user_gender']; ?></td>
                                            <td><?php echo $v['user_age']; ?></td>
                                            <td><?php echo $namakab; ?></td>
                                            <td><?php echo "+".$v['user_countrycode'].$v['user_callnum']; ?></td>
                                            <td align="center"><?php if($referralcode==""){ echo "-"; }else{echo $referralcode;}?></td>
                                            <td>
		                                    	<img onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$foto; ?>" class="pv-main" style='width: 100px; height: 100px;' alt="">
                                            </td>
                                            <td>
                                                <a data-toggle="modal" class="card profile-tutor" href="#descTutor" alt="<?php echo $foto;?>" id="card_<?php echo $v['id_user']; ?>"><button class="btn btn-default" title="Detail tutor"><i class="zmdi zmdi-search"></i></button></a>
                                            </td>                                                                                           
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div> 
                                            
            </div> 		

			<!-- Modal Default -->	
			<div class="modal fade" id="descTutor" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">														
							<div class="pull-left">
								<h3 class="modal-title">Profile Tutor</h3>
							</div>
							<div class="pull-right">
								<!-- <button disabled="disable" class="bgm-orange c-white" ><i class="zmdi zmdi-face-add"></i> <?php //echo $this->lang->line('followed'); ?></button> -->
								<!--<button class="btn bgm-deeporange"><i class="zmdi zmdi-face-add"></i>Unfollow</button>-->
								<button type="button" class="btn bgm-gray" data-dismiss="modal">X</button>
							</div>							
							<hr style="margin-top:6%;">
							
						</div>
						<div class="modal-body">
							<div class="col-sm-12">
								<div class="col-sm-4">
									<img id="modal_image" onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" src="" width="95%" height="180%" alt="">
								</div>
								<div class="col-sm-8" style="margin-top:-20px;">
									<!-- <h3><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('summary'); ?></h3>
									<label id="modal_self_desc"></label> -->
									<h4><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('basic_info'); ?></h4>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('full_name'); ?></dt>
										<dd id="modal_user_name"></dd>
									</dl>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('placeofbirth'); ?></dt>
										<dd id="modal_birthplace"></dd>
									</dl> 
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('birthday'); ?></dt>
										<dd id="modal_birthdate"></dd>
									</dl>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('age'); ?></dt>
										<dd id="modal_age"></dd>
									</dl>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('mobile_phone'); ?></dt>
										<dd id="modal_callnum"></dd>
									</dl>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('gender'); ?></dt>
										<dd id="modal_gender"></dd>
									</dl>
									<dl class="dl-horizontal">
										<dt><?php echo $this->lang->line('religion'); ?></dt>
										<dd id="modal_religion"></dd>
									</dl>
								</div>
							</div>

							<div class="col-sm-12 table-responsive">
								<br><br>
								<h4><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('latbelpend'); ?></h4>
								<table class="table table-bordered table-hover">
									<thead>
										<th><?php echo $this->lang->line('educational_level'); ?></th>
										<th><?php echo $this->lang->line('educational_competence'); ?></th>
										<th><?php echo $this->lang->line('educational_institution'); ?></th>
										<th><?php echo $this->lang->line('institution_address'); ?></th>
										<th><?php echo $this->lang->line('graduation_year'); ?></th>
									</thead>
									<tbody id="modal_eduback">
										
									</tbody>
								</table>

								<br/>
								<h4><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('pengalaman_mengajar'); ?></h4>
								<table class="table table-bordered table-hover">
									<thead>
										<th><?php echo $this->lang->line('description_experience'); ?></th>
										<th colspan="2"><?php echo $this->lang->line('year_experience'); ?></th>
										<th><?php echo $this->lang->line('place_experience'); ?></th>
										<th><?php echo $this->lang->line('information_experience'); ?></th>
									</thead>
									<tbody id="modal_teachexp">
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tbody>
								</table>
							</div>
							<div class="col-md-12">
								<h4><i class="zmdi zmdi-comment m-r-5"></i> Kesimpulan anda :</h4>								
								<textarea class="form-control" rows="6" name="conclusion" id="conclusion" placeholder="isi disini" style="overflow-y: auto; font-size: 13px;"></textarea>
								<label class="c-red" style="display: none;" id="alertconclusion">Harap isi kesimpulan anda terlebih dahulu !!!</label>
							</div>
						</div>
						<br>
						<div class="modal-footer m-r-30" >
							<button type="button" id="" class="btn btn-danger modal_decline" style="margin-top:40px;"><?php echo $this->lang->line('decline'); ?></button>
							<button type="button" id="" class="btn btn-primary modal_approve" style="margin-top:40px;"><?php echo $this->lang->line('approve'); ?></button>
						</div>    				
					</div>
				</div>
			</div>

		</section>
	</section>

	<footer id="footer">
		<?php $this->load->view('inc/footer'); ?>
	</footer>
	<script type="text/javascript">
		$('.card.profile-tutor').click(function(){
			var ids = $(this).attr('id');
			var alt = $(this).attr('alt');
			$("#modal_image").attr('src','');
			var lastChar = alt.substr(alt.length - 1);
			ids = ids.substr(5,10);
			$.get('<?php echo base_url(); ?>first/getTutorProfile/'+ids,function(hasil){
				hasil = JSON.parse(hasil);								
				$('#modal_image').attr('src','<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>'+alt);							
				$('.modal_approve').attr('id',ids);
				$('.modal_decline').attr('id',ids);				
				$('#modal_self_desc').html(hasil['self_description']);
				$('#modal_user_name').html(hasil['user_name']);
				$('#modal_birthplace').html(hasil['user_birthplace']);
				$('#modal_birthdate').html(hasil['user_birthdate']);
				$('#modal_age').html(hasil['user_age']);
				$('#modal_callnum').html(hasil['user_callnum']);
				$('#modal_gender').html(hasil['user_gender']);
				$('#modal_religion').html(hasil['user_religion']);				
				$('#modal_eduback').html('');
				$('#modal_teachexp').html('');
				if(hasil['education_background'] != ''){
					for (var i = 0; i<hasil['education_background'].length; i++) {
						$('#modal_eduback').append('<tr>'+
							'<td>'+hasil['education_background'][i]['educational_level']+'</td>'+
							'<td>'+hasil['education_background'][i]['educational_competence']+'</td>'+
							'<td>'+hasil['education_background'][i]['educational_institution']+'</td>'+
							'<td>'+hasil['education_background'][i]['institution_address']+'</td>'+
							'<td>'+hasil['education_background'][i]['graduation_year']+'</td>'+
							'</tr>');
					}
				}
				if(hasil['teaching_experience'] != ''){
					for (var i = 0; i<hasil['teaching_experience'].length; i++) {
						$('#modal_teachexp').append('<tr>'+
							'<td>'+hasil['teaching_experience'][i]['description_experience']+'</td>'+
							'<td>'+hasil['teaching_experience'][i]['year_experience1']+'</td>'+
							'<td>'+hasil['teaching_experience'][i]['year_experience2']+'</td>'+
							'<td>'+hasil['teaching_experience'][i]['place_experience']+'</td>'+
							'<td>'+hasil['teaching_experience'][i]['information_experience']+'</td>'+
							// '<td>'+hasil['teaching_experience'][i]['graduation_year']+'</td>'+
							'</tr>');
					}
				}
				$('#modalDefault').modal('show');
			});

			$('.modal_approve').click(function(){
				$('.modal_approve').css('disabled','true');
				var newids = $(this).attr('id');				
				var conclusion = $("#conclusion").val();
				if (conclusion == "") {
					$("#alertconclusion").css('display','block');
				}
				else
				{
					$.ajax({
			            url: '<?php echo base_url(); ?>/first/approveTheTutor',
			            type: 'GET',   
			            data: {
			                id_user: newids,
			                text: conclusion
			            },             
			            success: function(data)
			            { 
			            	$("#descTutor").modal("hide");
			            	notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Approve Tutor success");
	                        setTimeout(function(){
	                            window.location.reload();
	                        },3000);
			            }
		        	});
				}
				// $.get('<?php echo base_url(); ?>first/approveTheTutor?id_user='+newids+'&text='+conclusion,function(s){
					
				// });
			});
			$('.modal_decline').click(function(){
				$('.modal_decline').css('disabled','true');
				var newids = $(this).attr('id');
				var conclusion = $("#conclusion").val();
				if (conclusion == "") {
					$("#alertconclusion").css('display','block');
				}
				else
				{
					$.ajax({
			            url: '<?php echo base_url(); ?>/first/declineTheTutor',
			            type: 'GET',   
			            data: {
			                id_user: newids,
			                text: conclusion
			            },             
			            success: function(data)
			            { 
			            	$("#descTutor").modal("hide");
			            	notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Reject Tutor success");
	                        setTimeout(function(){
	                            window.location.reload();
	                        },3000);
			            }
		        	});
				}
			});

		});

		$('table.display').DataTable( {
	        fixedHeader: {
	            header: true,
	            footer: true
	        }
	    } );
	    $('.data').DataTable(); 
	</script>