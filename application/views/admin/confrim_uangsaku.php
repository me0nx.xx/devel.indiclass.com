<style type="text/css">
    /* Flashing */
    .hover13:hover img {
        opacity: 1;
        -webkit-animation: flash 1.5s;
        animation: flash 1.5s;
    }
    @-webkit-keyframes flash {
        0% {
            opacity: .4;
        }
        100% {
            opacity: 1;
        }
    }
    @keyframes flash {
        0% {
            opacity: .4;
        }
        100% {
            opacity: 1;
        }
    }
    .hover11 img {
        opacity: 1;     
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .hover11:hover img {
        opacity: 0.5;
    }
</style>
<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar');

     ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/sideadmin'); ?>
        <?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
        ?>
    </aside>

    <section id="content">
        <div class="container">
            
            <div class="block-header">
                <h2>Konfirmasi Uang Saku</h2>

                <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>admin"><?php echo $this->lang->line('homeadminn'); ?></a></li>
                            <li class="active">Konfirmasi Uang Saku </li>
                        </ol>
                    </li>
                </ul>
            </div> <!-- akhir block header    --> 

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="card col-md-12" style="margin-top: 4%;">
                <div class="card-header">
                    <h2>Data Confrim Transaction</h2>
                </div>

                <div class="card-body card-padding">
                    <div class="row">

                        <div class="col-md-12">
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                    	<th>No</th> 
                                        <th>Order ID</th>
                                        <th>Nama</th>
                                        <th width="120px">Total Payment</th>
                                        <th width="120px">Total Transfer</th>
                                        <th>Bank</th>
                                        <th>No Rekening</th>
                                        <th>Metode Payment</th>
                                        <th>Bukti Payment</th>
                                        <th>Time</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>                                
                                <tbody>
                                <?php 
                                	$no = 1;
                                    $gettrftransaction = $this->db->query("SELECT lt.credit, ltc.*, mb.bank_name, tu.user_name FROM log_trf_confirmation as ltc LEFT JOIN master_bank as mb ON ltc.bank_id=mb.bank_id LEFT JOIN tbl_user as tu ON ltc.id_user=tu.id_user LEFT JOIN log_transaction as lt ON lt.trx_id=ltc.trx_id WHERE ltc.accepted='0'")->result_array();
                                    foreach ($gettrftransaction as $row => $r) {                                        
                                        $jumlahpayment = number_format($r['credit'], 0, ".", ".");
                                        $totaltransfer = number_format($r['jumlah'], 0, ".", ".");                                        
                                ?>
                                    <tr>
                                    	<td><?php echo($no); ?></td>
                                        <td><?php echo $r['trx_id']; ?></td>
                                        <td><?php echo $r['user_name']; ?></td>
                                        <td>Rp. <?php echo $jumlahpayment; ?></td>
                                        <td>Rp. <?php echo $totaltransfer; ?></td>
                                        <td><?php echo $r['bank_name'] ?></td>
                                        <td><?php echo $r['norek_tr']; ?></td>
                                        <td><?php echo $r['metod_tr']; ?></td>
                                        <td class="hover11"><a href="#" class="pop"><img src="<?php echo base_url('aset/img/buktipembayaran/'.$r['bukti_tr']); ?>" width="100px" height="100px"></a></td>
                                        <td><?php echo $r['time']; ?></td>
                                        <td><center><a onclick="return confirm('Apakah anda yakin Data tersebut sudah benar ?')" href="<?php echo base_url('Process/confirm_bank?trx_id='.$r['trx_id']);?>"><button class="btn btn-success"><i class="zmdi zmdi-check zmdi-lg"></i> Done</button></a></center></td>
                                    </tr>            
                                </tbody>
                                <?php
                                	$no++;
                                    }
                                ?>
                            </table>
                        </div>
                        <label class="c-red m-t-15">* Mohon untuk perhatikan antara Total Payment dan Total Transfer. Bila Total Transfer tidak sama dengan Total Payment harap Tidak menekan tombol Done</label>
                        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
						    <div class="modal-content">              
						      <div class="modal-body">
						      	<br><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><br><br><br>
						        <img src="" class="imagepreview" style="width: 100%;" ><br><br><br>
						      </div>
						    </div>
						  </div>
						</div>

                    </div>
                </div>
            </div>

            <div class="card col-md-12" style="margin-top: 4%;">
                <div class="card-header">
                    <h2>Check Transaction</h2>
                </div>

                <div class="card-body card-padding">
                    <div class="row">

                        <div class="col-md-12">
                            <table class="display table table-striped table-bordered data">
                                <thead>
                                    <tr>   
                                        <th>No</th>         
                                        <th>Order ID</th>
                                        <th>Nama User</th>
                                        <th>Payment Method</th>
                                        <th>Credit</th>
                                        <th>Time Transaction</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                <?php 
                                    $no = 1;
                                    $getdatatransaction = $this->db->query("SELECT lt.*, tu.user_name FROM log_transaction as lt INNER JOIN tbl_user as tu ON lt.id_user=tu.id_user")->result_array();
                                    foreach ($getdatatransaction as $row => $t) {                                        
                                        $hasiljumlah = number_format($t['credit'], 0, ".", ".");
                                        $status = $t['flag'];
                                ?>
                                    <tr>       
                                        <td><?php echo($no); ?></td>         
                                        <td><?php echo $t['trx_id']; ?></td>
                                        <td><?php echo $t['user_name']; ?></td>
                                        <td><?php if($t['payment_method'] == 'bank_transfer'){ echo 'Transfer Bank';}else{ echo 'Credit Card';} ?></td>
                                        <td>Rp. <?php echo $hasiljumlah; ?></td>
                                        <td><?php echo $t['timestamp']; ?></td>
                                        <td><?php if($status==0){ echo '<button class="btn btn-danger">Pending Transaction</button>';}else{ echo '<button class="btn btn-success">Transaction Successful</button>';} ?></td>
                                    </tr>   
                                    <?php
                                    $no++;
                                    }
                                ?>                                                                  
                                </tbody>
                                
                            </table>                            
                        </div>                        

                    </div>
                </div>
            </div>

        </div>
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function() {
        $('table.display').DataTable( {
            fixedHeader: {
                header: true,
                footer: true
            }
        } );
        $('.data').DataTable();

        $('.pop').on('click', function() {
			$('.imagepreview').attr('src', $(this).find('img').attr('src'));
			$('#imagemodal').modal('show');   
		});	
    } );
</script>
    