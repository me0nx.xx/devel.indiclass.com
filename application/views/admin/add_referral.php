<style type="text/css">
    .hover11 img {
        opacity: 1;     
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .hover11:hover img {
        opacity: 0.5;
    }
</style>
<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sideadmin'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>Referral Add</h2>
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>            

            <div class="card m-t-20 p-20" style="">
                <div class="card-header">
                    <div class="pull-left"><h2>List Referral</h2></div>
                    <div class="pull-right"><a data-toggle="modal" href="#modaladdreferral"><button class="btn btn-success">+ Tambah Referral</button></a></div>
                <br><br>
                <hr>

                <div class="row" style="margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <?php
                                    $select_all = $this->db->query("SELECT * FROM master_referral")->result_array();
                                    ?>
                                    <tr>
                                        <th data-column-id="no">No</th>
                                        <th data-column-id="name">Referral Code</th>                                        
                                        <th data-column-id="jenjang">Start Date</th>  
                                        <th data-column-id="name">Expired Date</th>
                                        <th data-column-id="Description">Description</th>                                                                            
                                        <th data-column-id="Type">Type</th>
                                        <th data-column-id="aksi" style="text-align: right;" >Action</th>
                                        <th data-column-id="aksi" style="text-align: left;" ></th>

                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;
                                    foreach ($select_all as $row => $v) {
                                        
                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['referral_code']; ?></td>
                                            <td><?php echo $v['start_datetime'] ?></td>
                                            <td><?php echo $v['end_datetime']; ?></td>
                                            <td><?php echo $v['description']; ?></td>                                                                                    
                                            <td><?php echo $v['type']; ?></td>
                                            <td>
                                                <a href="<?php echo base_url(); ?>Admin/ReferralEdit?id=<?php echo $v['referral_id'] ?>"><button class="btn bgm-bluegray" title="Edit Referral"><i class="zmdi zmdi-edit"></i></button></a>
                                                
                                            </td>
                                            <td>
                                                <a data-toggle="modal" rtp="<?php echo $v['referral_id'] ?>" class="hapusreferral" data-target-color="green" href="#modalDelete"><button class="btn bgm-bluegray" title="Hapus Referral"><i class="zmdi zmdi-delete"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div> 
                                            
            </div> 

            <!-- Modal TAMBAH -->  
            <div class="modal" id="modaladdreferral" tabindex="-1" role="dialog" style="top: 13%;" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bgm-cyan">
                            <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                            <h4 class="modal-title c-white">Tambah Referral</h4>
                        </div>
                        <div class="modal-body m-t-20">
                            <form method="post" action="<?php echo base_url('Admin/add_referral'); ?>" enctype="multipart/form-data">
                            <div class="row p-15">    
                                
                                <div class="col-sm-12 col-md-12 col-xs-12">                                                    
                                    <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <input type="text" name="referral_code" required class="input-sm form-control fg-input">
                                            <label class="fg-label">Referral Code</label>
                                        </div>
                                    </div>
                                </div>                        

                                <div class="col-md-12">
                                    <div class="form-group p-r-15">
                                        <label class="fg-label">Start date</label>
                                        <div class="dtp-container fg-line m-l-5" >
                                            <input type="text" class="form-control" id="dateon" data-date-format="YYYY-MM-DD" name="start_datetime" placeholder="<?php echo $this->lang->line('searchdate'); ?>"/>
                                        </div>
                                    </div>
                                </div> 

                                <div class="col-md-12">
                                    <div class="form-group p-r-15">
                                        <label class="fg-label">Expired date</label>
                                        <div class="dtp-container fg-line m-l-5" >
                                            <input type="text" class=" form-control" id="dateong" data-date-format="YYYY-MM-DD" name="finish_datetime" placeholder="<?php echo $this->lang->line('searchdate'); ?>"/>
                                        </div>
                                    </div>
                                </div>                                                        

                                <div class="col-sm-12 col-md-12 col-xs-12">
                                    <div class="form-group fg-float">
                                        <div class="fg-line">                                            
                                            <textarea name="description" class="input-sm form-control fg-input" rows="4" ></textarea>
                                            <label class="fg-label">Description</label>
                                        </div>
                                    </div>
                                </div>  

                                <div class="col-sm-12 col-md-12 col-xs-12">        
                                <label class="fg-label c-gray">Type</label>                                        
                                    <div class="form-group fg-float">                                    
                                        <div class="fg-line">
                                            <select required name="type" class="select2 col-md-6 input-sm fg-input form-control" style="width: 100%;">
                                                <option value='register'>Register</option>
                                                <option value='class'>Class</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 m-t-20">
                                    <button class="btn btn-success btn-block">Simpan</button>
                                </div>
                            </div>
                            </form>                            

                            <br>
                        </div>
                    </div>
                </div>
            </div>                             

            <!-- Modal DELETE -->  
            <div class="modal fade" id="modalDelete" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Hapus Referral</h4>
                            <hr>
                            <p><label class="c-gray f-15">Apakah anda yakin ingin menghapus</label></p>                            
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" data-id="" id="hapusdatareferral" rtp="">Ya</button>                            
                        </div>                        
                    </div>
                </div>
            </div>
            
        </div>
    </section>

</section>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>aset/js/magicsuggest/magicsuggest.css" /> 
<script type="text/javascript" src="<?php echo base_url();?>aset/js/magicsuggest/magicsuggest.js"></script>  
<script type="text/javascript">

    var date = null;
    var tgll = new Date();  
    var formattedDatee = moment(tgll).format('YYYY-MM-DD'); 

    $(document).ready(function(){

        $('#hapusdatareferral').click(function(e){
            var rtp = $(this).attr('rtp');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Admin/deletereferral",
                type:"POST",
                data: {
                    rtp: rtp
                },
                success: function(html){             
                    // alert("Berhasil menghapus data siswa");
                    $('#modalDelete').modal('hide');
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Berhasil Menghapus data Referral");
                    setTimeout(function(){
                        location.reload(); 
                    },2000);
                                        
                } 
            });
        });
    });

    $(document).on("click", ".hapusreferral", function () {
         var myBookId = $(this).attr('rtp');
         $('#hapusdatareferral').attr('rtp',myBookId);
    });   

    $(function () {
        var tgl = new Date();  
        var formattedDate = moment(tgl).format('YYYY-MM-DD');         
        
        $('#dateon').datetimepicker({  
            minDate: moment(formattedDate, 'YYYY-MM-DD')
        });

        $('#dateong').datetimepicker({  
            minDate: moment(formattedDate, 'YYYY-MM-DD')
        });
        
    });    

    $('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable(); 

   
</script>

