<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sideadmin'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2><?php echo $this->lang->line('accountlist'); ?></h2>
                <!-- <ul class="actions hidden-xs">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('home'); ?></a></li>
                        <li><?php echo $this->lang->line('accountlist'); ?></li>
                    </ol>                    
                </ul> -->
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="card m-t-20 p-20" style="">
                <div class="card-header">
                    <div class="pull-left"><h2>Edit Data Channel</h2></div>
                    <div class="pull-right"><a href="<?php echo base_url();?>Admin/Register"><button class="btn btn-danger"><i class="zmdi zmdi-arrow-right"></i> Batal</button></a></div>
                <br><br>
                <hr>

                <div class="row" style="margin-top: 5%;">
                <?php
                    $idchannel = $_GET['id'];                    
                    $getchannel = $this->db->query("SELECT * FROM master_channel WHERE channel_id='$idchannel'")->result_array();
                    if (empty($getchannel)) {
                        ?>
                    <center>Tidak ada Data Channel</center>
                    <?php
                    }
                    else
                    {                        
                        foreach ($getchannel as $row => $v) {                            
                        ?>
                    <div class="col-md-2"></div>                        
                    <div class="col-md-8" >
                        <div class="card">
                            <form method="post" action="<?php echo base_url('Process/editchannel'); ?>" enctype="multipart/form-data">
                            <div class="row p-15">    
                                
                                <input type="text" name="channel_id" id="channel_id" class="c-black" value="<?php echo $idchannel; ?>" hidden/>
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Nama Channel</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <input type="text" name="channel_name" id="channel_name" value="<?php echo $v['channel_name']; ?>" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Email</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <input type="text" name="channel_email" value="<?php echo $v['channel_email']; ?>" id="channel_email" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>                                
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">No Hp</label>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group fg-line" style="margin-left: 11%;">                                                       
                                            <select required name="channel_country_code" class="select2 form-control">
                                                <option disabled selected value=''><?php echo $this->lang->line('countrycode'); ?></option>                                                
                                                <?php
                                                    // $allbank = $this->db->query("SELECT * FROM master_bank")->result_array();
                                                    // foreach ($allbank as $row => $d) {
                                                    //     if($d['bank_id'] == $v['bank_id']){
                                                    //         echo "<option value='".$d['bank_id']."' selected='true'>".$d['bank_name']."</option>";
                                                    //     }
                                                    //     if ($d['bank_id'] != $v['bank_id']) {
                                                    //         echo "<option value='".$d['bank_id']."'>".$d['bank_name']."</option>";
                                                    //     }                                                            
                                                    // }
                                                ?>
                                                <option value="+62" <?php if($v['channel_country_code']=="+62"){ echo "selected='true'";} ?>>Indonesia +62</option>
                                                <option value="+60" <?php if($v['channel_country_code']=="+60"){ echo "selected='true'";} ?>>Malaysia +60</option>
                                                <option value="+65" <?php if($v['channel_country_code']=="+65"){ echo "selected='true'";} ?>>Singapore +65</option>                                       
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group fg-float" >                       
                                            <div class="fg-line c-gray">
                                                <input type="text" name="channel_callnum" value="<?php echo $v['channel_callnum']; ?>" id="channel_callnum" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Alamat</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">
                                                <!-- <input type="text" name="channel_email" value="<?php echo $v['channel_email']; ?>" id="channel_email" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" /> -->
                                                <textarea name="channel_address" id="channel_address" rows="5" required class="input-sm form-control fg-input" style="border: 1px solid #BDBDBD; padding: 3px;"><?php echo $v['channel_address']; ?></textarea>    
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 m-t-20">
                                    <button class="btn btn-success btn-block">Simpan</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    <?php 
                        }                       
                    }
                    ?>                                 

                </div> 
                                            
            </div> 

            
            <!-- Modal EDIT -->  
            <div class="modal fade" id="modalEdit" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Edit Akun Rekening</h4>
                            <hr>
                        </div>                        
                        <div class="modal-body">
                            <div class="card">
                                <div class="row p-15">   
                                    <input type="text" name="id_rekening" id="id_rekening" class="c-black" value=""/>                                     
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Nama Akun</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group fg-float" style="margin-left: 4%;">                       
                                                <div class="fg-line c-gray">
                                                    <input type="text" name="editnamaakun" id="editnamaakun" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Nomer Rekening</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group fg-float" style="margin-left: 4%;">                       
                                                <div class="fg-line c-gray">
                                                    <input type="text" name="editnorekening" id="editnorekening" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Nama Bank</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group fg-float" style="margin-left: 4%;">                       
                                                <div class="fg-line c-gray">
                                                    <div class="select">
                                                        <select required name="editnamabank" id="editnamabank" class="selectpicker" data-live-search="true" style="z-index: 10;">
                                                        <?php
                                                           $allbank = $this->db->query("SELECT * FROM master_bank")->result_array();
                                                           foreach ($allbank as $row => $v) {
                                                                echo '<option value="'.$v['bank_id'].'">'.$v['bank_name'].'</option>';
                                                            }
                                                        ?>                                                    
                                                        </select>
                                                    </div>
   
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Cabang</label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group fg-float" style="margin-left: 4%;">                       
                                                <div class="fg-line c-gray">
                                                    <input type="text" name="editcabangbank" id="editcabangbank" required class="input-sm form-control fg-input " style="border: 1px solid #BDBDBD; padding: 3px;" />    
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn bgm-green" id="editdatarekening">Simpan</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>

        </div>
    </section>

</section>
<script type="text/javascript">

    // $('#simpandatarekening').click(function(){
    //     alert('test');
    // });
    $(document).ready(function(){
        $('#simpandatarekening').click(function(e){
            var namaakun = $("#namaakun").val();
            var norekening = $("#norekening").val();
            var namabank = $("#namabank").val();
            var cabangbank = $("#cabangbank").val();
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/addrekening",
                type:"post",
                data: "namaakun="+namaakun+"&norekening="+norekening+"&namabank="+namabank+"&cabangbank="+cabangbank,
                success: function(html){             
                    alert("Berhasil Menambahkan akun rekening bank");
                    $('#modalTambah').modal('hide');
                    location.reload();                     
                } 
            });
         });

        $('#hapusdatarekening').click(function(e){
            var id_rekening = $("#id_rekeningg").val();
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/deleterekening",
                type:"post",
                data: "id_rekening="+id_rekening,
                success: function(html){             
                    alert("Berhasil menghapus data rekening");
                    $('#modalDelete').modal('hide');
                    location.reload();                     
                } 
            });
        });
    });
</script>
