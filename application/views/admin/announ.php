<style type="text/css">
    .loading {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        background: rgba(50, 50, 50, 0.7);
        z-index: 10;
        display: none;
    }
    .gif{
        top: 150px;
        margin-top: 170px;
        z-index: 999;
    }


</style>
<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar');

     ?>
</header>
<div class="loading">
    <center>
    <img class="gif" src="<?php echo base_url();?>aset/img/clock-loading.gif" width="400px">
    </center>
</div>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/sideadmin'); ?>
        <?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
        ?>
    </aside>

    <section id="content">
        <div class="container">
            
            <div class="block-header">
                <h2>Support</h2>

                <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>admin"><?php echo $this->lang->line('homeadminn'); ?></a></li>
                            <li class="active">Support </li>
                            <li id="checksupport"><?php echo $this->session->userdata('checksupport');?> </li>
                        </ol>
                    </li>
                </ul>
            </div> <!-- akhir block header    --> 

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="card col-md-12" style="margin-top: 4%;">
                <div class="card-header">
                    <h2>Announcement List</h2>
                    <div class="pull-right" style="margin-top: -2%;"><a data-toggle="modal" href="#Tambah"><button class="btn btn-success">+ Tambah</button></a></div>
                </div>

                <div class="card-body card-padding">
                    <div class="row">

                        <div class="col-md-12 table-responsive">
                            <table class="display table table-striped table-bordered data">
                                <thead>
                                    <tr>   
                                        <th>No</th>
                                        <th>Text</th>                                                                            
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                
                                <tbody> 
                                    <?php
                                        $no = 1;                                        
                                        // $query2 = $this->db->query("SELECT lm.payment_type, lm.status_message, lo.* FROM log_midtrans_transaction as lm INNER JOIN log_order as lo ON lm.order_id=lo.order_id WHERE lo.id_user='$user' ORDER BY lo.timestamp DESC")->result_array();
                                        $query2 = $this->db->query("SELECT * FROM tbl_announcement ORDER BY time_create DESC")->result_array();
                                        if (empty($query2)) {
                                            ?>
                                        <tr>
                                            <td colspan='8'><center>Tidak ada data</center></td>
                                        </tr>
                                        <?php
                                        }
                                        else
                                        {     
                                        foreach ($query2 as $row => $v) {                                            
                                          
                                    ?>                               
                                    <tr>       
                                        <td><?php echo($no); ?></td>                                        
                                        <th><?php echo($v['text_announ']); ?></th>                                        
                                        <td>
                                            <a href="<?php echo base_url(); ?>admin/editannoun?id=<?php echo $v['id'] ?>"><button class="btn btn-warning" title="Edit">Ubah</button></a>
                                            <a data-toggle="modal" data-id="<?php echo $v['id']; ?>" class="hapusdatae" data-target-color="green" href="#Delete"><button class="btn btn-danger" title="Hapus">Hapus</button></a>
                                        </td>
                                    </tr>
                                    <?php
                                        $no++;
                                        }                                         
                                    } 
                                    ?>                                                                  
                                </tbody>
                                
                            </table>                            
                        </div>                        

                    </div>
                </div>
            </div>

            <!-- Modal TAMBAH -->  
            <div class="modal fade" id="Tambah" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Tambah Announcement</h4>
                            <hr>
                        </div>                        
                        <div class="modal-body">
                            <div class="card">
                            <div class="row p-15">                                 
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <label class="fg-label f-14 c-gray" style="margin-top: 3px;">Text</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group fg-float" style="margin-left: 4%;">                       
                                            <div class="fg-line c-gray">                                                
                                                <textarea class="form-control fg-input input-sm" rows="4" name="isitext" id="isitext" style="border: 1px solid #BDBDBD; padding: 3px;"></textarea>  
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Batal</button>
                            <button type="button" class="btn bgm-green" id="simpandata">Simpan</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>

            <!-- Modal DELETE -->  
            <div class="modal fade" id="Delete" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Hapus Data</h4>
                            <hr>
                            <p><label class="c-gray f-15">Apakah anda yakin ingin menghapus</label></p>
                            <input type="text" name="id_announ" id="id_announ" class="c-black" value="" style="display: none;" />
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" id="hapusdatatext">Ya</button>
                            
                        </div>                        
                    </div>
                </div>
            </div> 

        </div>    
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<!-- <div class="page-loader bgm-blue">
    <div class="preloader pls-white pl-xxl">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p class="c-white f-14">Please wait...</p>
    </div>
</div> -->



<script type="text/javascript">
    $(document).ready(function() {
        
        $('#simpandata').click(function(e){
            var isitext = $("#isitext").val();            
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/addtext_announ",
                type:"post",
                data: "isitext="+isitext,
                success: function(html){                                 
                    $('#Tambah').modal('hide');
                    location.reload();                     
                } 
            });
        });

        $('#hapusdatatext').click(function(e){
            var id_announ = $("#id_announ").val();            
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/deletetext_announ",
                type:"post",
                data: "id_announ="+id_announ,
                success: function(html){                                 
                    $('#Delete').modal('hide');
                    location.reload();                     
                } 
            });
        });
            
    } );
    $(document).on("click", ".hapusdatae", function () {
         var myBookId = $(this).data('id');
         $(".modal-header #id_announ").val( myBookId );
    }); 
</script>
    