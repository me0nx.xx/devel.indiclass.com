<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar');

     ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/sideadmin'); ?>
        <?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
        ?>
    </aside>

    <section id="content">
        <div class="container">
            
            <div class="block-header">
                <h2>Konfirmasi Uang Saku</h2>

                <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>admin"><?php echo $this->lang->line('homeadminn'); ?></a></li>
                            <li class="active">Konfirmasi Uang Saku </li>
                        </ol>
                    </li>
                </ul>
            </div> <!-- akhir block header    --> 

            <div class="card col-md-12" style="margin-top: 4%;">
                <div class="card-header">
                    <h2>Data Transaction</h2>
                </div>

                <div class="card-body card-padding">
                    <div class="row">

                        <div class="col-md-12">
                            <table class="display table table-striped table-bordered data">
                                <thead>
                                    <tr>   
                                        <th>No</th>         
                                        <th>Order ID</th>
                                        <th>Nama User</th>
                                        <th>Payment Method</th>
                                        <th>Credit</th>
                                        <th>Time Transaction</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                <?php 
                                    $no = 1;
                                    $getdatatransaction = $this->db->query("SELECT lt.*, tu.user_name FROM log_transaction as lt INNER JOIN tbl_user as tu ON lt.id_user=tu.id_user")->result_array();
                                    foreach ($getdatatransaction as $row => $t) {                                        
                                        $hasiljumlah = number_format($t['credit'], 0, ".", ".");
                                        $status = $t['flag'];
                                ?>
                                    <tr>       
                                        <td><?php echo($no); ?></td>         
                                        <td><?php echo $t['trx_id']; ?></td>
                                        <td><?php echo $t['user_name']; ?></td>
                                        <td><?php if($t['payment_method'] == 'bank_transfer'){ echo 'Transfer Bank';}else{ echo 'Credit Card';} ?></td>
                                        <td>Rp. <?php echo $hasiljumlah; ?></td>
                                        <td><?php echo $t['timestamp']; ?></td>
                                        <td><?php if($status==0){ echo '<button class="btn btn-danger">Pending Transaction</button>';}else{ echo '<button class="btn btn-success">Transaction Successful</button>';} ?></td>
                                    </tr>   
                                    <?php
                                    $no++;
                                    }
                                ?>                                                                  
                                </tbody>
                                
                            </table>                            
                        </div>                        

                    </div>
                </div>
            </div>

        </div>
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function() {
        $('table.display').DataTable( {
            fixedHeader: {
                header: true,
                footer: true
            }
        } );
        $('.data').DataTable();
    } );
</script>
    