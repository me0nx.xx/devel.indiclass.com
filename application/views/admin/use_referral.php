<style type="text/css">
    .hover11 img {
        opacity: 1;     
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .hover11:hover img {
        opacity: 0.5;
    }
</style>
<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sideadmin'); ?>
    </aside>

    <section id="content">
        <div class="container">

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>            

            <div class="card m-t-20 p-20" style="">
                <div class="card-header">
                    <div class="pull-left"><h2>Referral use</h2></div>
                <br><br>
                <hr>

                <div class="row" style="margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <?php
                                    $select_all = $this->db->query("SELECT tu.id_user, tu.email, tu.user_name, tu.user_nationality, tu.user_callnum, tu.user_countrycode, mr.* FROM tbl_user as tu INNER JOIN master_referral as mr ON tu.referral_id=mr.referral_id")->result_array();
                                    ?>
                                    <tr>
                                        <th>No</th>
                                        <th>ID User</th>                                        
                                        <th>Nama user</th>                                        
                                        <th>Email</th>
                                        <th>No Handphone</th>
                                        <th>Asal</th>
                                        <th>Referral code</th>                                        
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;
                                    foreach ($select_all as $row => $v) {
                                        
                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['id_user']; ?></td>
                                            <td><?php echo $v['user_name'];?></td>
                                            <td><?php echo($v['user_countrycode'].' '.$v['user_callnum']); ?></td>
                                            <td><?php echo $v['email']; ?></td>
                                            <td><?php echo $v['user_nationality']; ?></td>
                                            <td><?php echo $v['referral_code']; ?></td>                                            
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div> 
            </div> 

        </div>
    </section>

</section>

<script type="text/javascript">

    $(document).on("click", ".hapussubject", function () {
         var myBookId = $(this).attr('rtp');
         $('#hapusdatasubject').attr('rtp',myBookId);

    });   
    $('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable(); 

</script>

