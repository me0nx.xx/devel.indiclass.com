<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="margin-top: 3.5%; z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sideadmin'); ?>
    </aside>

    <section id="content">
        <div class="container invoice">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>Edit Referral</h2>
            </div>
                    
            <?php
                $referral_id = $_GET['id'];
                $getreferral = $this->db->query("SELECT * FROM master_referral WHERE referral_id='$referral_id'")->row_array();
            ?>
            <div class="card">
                <div class="card-header ch-alt">
                    <div class="pull-right f-14">Referral ID : <?php echo $_GET['id']; ?></div>
                    <div class="pull-left f-14"><a href="<?php echo base_url();?>Admin/addReferral"><button class="btn btn-danger"><i class="zmdi zmdi-arrow-left"></i> Back</button></a></div><br>
                </div>
                
                <div class="card-body card-padding" style="background: white;">                
                    <div class="row">                        
                        <form method="post" action="<?php echo base_url('Admin/edtreferral'); ?>" enctype="multipart/form-data">
                            <div class="col-md-3 m-t-25"></div>
                            <div class="col-md-6 m-t-25 m-b-25" style="border: 1px solid gray; border-radius: 5px; padding: 15px;">
                                
                                <input type="text" name="referral_id" id="referral_id" value="<?php echo $referral_id; ?>" hidden/>                                
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12">Referral Code</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <input type="text" name="referral_code" placeholder="Isi Referral" value="<?php echo $getreferral['referral_code'] ?>" required class="input-sm form-control fg-input">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12">Start Date</label>
                                    <div class="col-sm-9">
                                        <div class="dtp-container fg-line m-l-5" >
                                            <input type="text" class="form-control" id="dateon" data-date-format="YYYY-MM-DD" name="start_datetime" placeholder="<?php echo $this->lang->line('searchdate'); ?>" value="<?php echo $getreferral['start_datetime']; ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12">Expired Date</label>
                                    <div class="col-sm-9">
                                        <div class="dtp-container fg-line m-l-5" >
                                            <input type="text" class=" form-control" id="dateong" data-date-format="YYYY-MM-DD" name="finish_datetime" placeholder="<?php echo $this->lang->line('searchdate'); ?>" value="<?php echo $getreferral['end_datetime']; ?>"/>
                                        </div>
                                    </div>
                                </div>  
                                <br><br>                                
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12">Description</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <textarea name="description" class="input-sm form-control fg-input" rows="4" ><?php echo $getreferral['description']?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label f-12">Type</label>
                                    <div class="col-sm-9">
                                        <div class="fg-line">
                                            <select required name="type" class="select2 col-md-6 form-control">                                
                                                <option disabled selected>Pilih Type</option>
                                                <option value="register" <?php if($getreferral['type']=="register"){ echo "selected='true'";} ?>>Register</option>                                                  
                                                <option value="class" <?php if($getreferral['type']=="class"){ echo "selected='true'";} ?>>Class</option>
                                            </select>    
                                        </div>
                                    </div>
                                </div>
                                <br><br><br>
                                <button class="btn btn-md c-white btn-block m-t-10 bgm-blue" style="border: 1px solid gray;">Ubah</button>
                            </div>
                            <div class="col-md-3 m-t-25"></div>
                                                    
                        </form>

                    </div>
                </div>
                
            </div>
            
        </div>
    </section>

</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(function () {
        var tgl = new Date();  
        var formattedDate = moment(tgl).format('YYYY-MM-DD');         
        
        $('#dateon').datetimepicker({  
            minDate: moment(formattedDate, 'YYYY-MM-DD')
        });

        $('#dateong').datetimepicker({  
            minDate: moment(formattedDate, 'YYYY-MM-DD')
        });
        
    });
</script>