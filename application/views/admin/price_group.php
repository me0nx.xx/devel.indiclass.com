<main class="main">

    <header class="header">
       	<?php $this->load->view("inc/navbar_adm"); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <header class="content__title">
            <h1>Set Price Group</h1>
            <small>Details of Group Price channel</small>

            <div class="actions">
                <a data-toggle="modal" href="#modalAddPriceGroup"><button class="btn btn-success">+ Add Price Group</button></a>
            </div> 
        </header>

        <div class="row">
            <div class="card col-md-12 col-xs-12">
                <div class="card-block">
                    <div class="row table-responsive">
                        <div id="tables_priceGroup"></div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>   

<!-- Modal modalAddTutor -->  
<div class="modal fade show" id="modalAddPriceGroup">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">                        
                <h4 class="modal-title c-white pull-left">Add Price Group</h4>
                <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
            </div>
            <div class="modal-body m-t-20">
                <form id="frmdata">
                    <div class="col-md-12" style="padding: 0;">
                        <div class="col-md-12">
                            <label>Search Tutor</label><br>
                            <!-- <input name='ms' class="col-md-8 m-t-5" style="height: 35px;" id="ms"> -->
                            <div class="col-md-12 m-t-10">                                          
                                <select required id="tutor_id" class="select2 tutorSelect" placeholder="choose subject" style="width: 100%;">
                                    <option selected disabled="disabled">Choose Tutor</option>
                                </select>                                           
                            </div>
                        </div>
                        <br>
                        <div class="col-md-12">
                            <label>Select Subject</label><br>
                            <!-- <input name='ms' class="col-md-8 m-t-5" style="height: 35px;" id="ms"> -->
                            <div class="col-md-12 m-t-10">                                          
                                <select required id="subject" class="select2 subjectSelect" placeholder="choose subject" style="width: 100%;">
                                    <option selected disabled="disabled">Choose Subject</option>
                                </select>                                           
                            </div>
                        </div>

                        <div class="col-md-1"></div>  
                        <div class="col-md-12"><br><br>
                            <label>Point Price</label><br>
                            <input type="number" id="pointGroup" min="1" class="form-control" placeholder="Enter point price">                                    
                        </div>                                  
                    </div>
                </form>
                <br>
            </div>
            <div class="modal-footer">
                <button type="button"  class="btn btn-success btn-block m-t-15" id="savePriceGroup">Add Price</button>                            
            </div>
        </div>
    </div>
</div>

<div class="animated fadeIn modal fade" style="color: white;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" >
        <div class="modal-content" id="modal_konten">
            <div class="modal-body" align="center">
                <label id="text_modal"></label><br>
                <button  id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var access_token = "<?php echo $this->session->userdata('access_token');?>";
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
    var dataSet = [];

    $(document).ready(function() {
        
        $.ajax({
            url: 'https://rltclass.com.sg/Rest/myListGroupPrice/access_token/'+access_token,
            type: 'POST',
            success: function(response)
            {
                var a = JSON.stringify(response);                 
                var code = response['code'];                
                if (code == 200) {                     
                    for (var i = 0; i < response.data.length; i++) {
                        var class_type  = response['data'][i]['class_type'];
                        var name_tutor = response['data'][i]['name_tutor'];
                        var subject_name  = response['data'][i]['subject_name'];
                        var jenjang  = response['data'][i]['jenjang'];
                        var uid   = response['data'][i]['uid'];
                        var subject_id   = response['data'][i]['subject_id'];
                        var tutor_id = response['data'][i]['tutor_id'];
                        var point_price = response['data'][i]['point_price'];

                        var hapusPrice = "<button class='deletePrice btn waves-effect' style='background-color: #607D8B;' data-uid='"+uid+"' title='Delete Price'><i class='zmdi zmdi-delete zmdi-hc-fw' style='color:#fff;'></i></button>";
                        dataSet.push([i+1, name_tutor, subject_name, point_price, hapusPrice]);
                    }                        

                    $('#tables_priceGroup').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="priceGroup"></table>' );
             
                    $('#priceGroup').dataTable( {
                        "data": dataSet,
                        "columns": [
                            { "title": "No"},
                            { "title": "Tutor"},
                            { "title": "Name Subject"},                            
                            { "title": "Price"},
                            { "title": "Action"}
                        ]
                    });
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
                else
                {
                    $('#tables_priceGroup').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="priceGroup"></table>' );
             
                    $('#priceGroup').dataTable( {                        
                        "columns": [
                            { "title": "No"},
                            { "title": "Tutor"},
                            { "title": "Name Subject"},                            
                            { "title": "Price"},
                            { "title": "Action"}    
                        ]
                    });
                }
            }
        }); 
        
        $(".subjectSelect").select2({
            dropdownParent: $("#modalAddPriceGroup")
        });

        $(".tutorSelect").select2({
            dropdownParent: $("#modalAddPriceGroup")
        });

        $.ajax({
            url: '<?php echo AIR_API;?>listTutor/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id
            },
            success: function(response)
            {
                var a = JSON.stringify(response);                 
                var code = response['code'];                
                if (code == 200) {
                    for (var i = 0; i < response['data'].length; i++) {
                        var tutor_id    = response['data'][i]['tutor_id'];
                        var user_name   = response['data'][i]['user_name'];
                        var email       = response['data'][i]['email'];                        

                        $(".tutorSelect").append("<option value='"+tutor_id+"'>"+user_name+" - "+email+"</option>");
                    }
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
            }
        });

        $.ajax({
            url: '<?php echo AIR_API;?>channel_listMapel/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id
            },
            success: function(response)
            {
                var a = JSON.stringify(response);                 
                var code = response['code'];                
                if (code == 200) {
                    for (var i = 0; i < response['data'].length; i++) {
                        var subject_id      = response['data'][i]['subject_id'];
                        var subject_name    = response['data'][i]['subject_name'];
                        var jenjangnamelevel= response['data'][i]['jenjangnamelevel'];                        

                        $(".subjectSelect").append("<option value='"+subject_id+"'>"+subject_name+" - "+jenjangnamelevel+"</option>");
                    }
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
            }
        });

        $(document).on("click","#savePriceGroup",function() {
            var tutor_id = $("#tutor_id").val();
            var subject_id = $("#subject").val();
            var point_price = $("#pointGroup").val();
            $.ajax({
                url: '<?php echo BASE_URL();?>Rest/addPriceGroup/access_token/'+access_token,
                type: 'POST',
                data: {
                    tutor_id: tutor_id,
                    subject_id: subject_id,
                    point_price: point_price,
                    class_type : 'group',
                    channel_id: channel_id
                },
                success: function(response)
                {
                    var a = JSON.stringify(response);                 
                    var code = response['code'];                
                    if (code == 200) {
                        $('#modalAddPriceGroup').modal('hide');
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#text_modal").html("Successfully Add Price Point");
                        $("#button_ok").click( function(){
                            window.location.reload();
                        });
                    }
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>Admin/Logout';
                    }
                    else if (code == -301) {
                        $('#modalAddPriceGroup').modal('hide');
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#ff3333');
                        $("#text_modal").html("There class already point price!!!");
                        $("#button_ok").click( function(){
                            window.location.reload();
                        });
                    }
                    else
                    {
                        $('#modalAddPriceGroup').modal('hide');
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#ff3333');
                        $("#text_modal").html("There is an error!!!");
                        $("#button_ok").click( function(){
                            window.location.reload();
                        });
                    }
                }
            });
        });

        $(document).on("click",".deletePrice",function() {
            var uid = $(this).data('uid');
            bootbox.confirm({                
                message: "Are you sure delete price private?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancel'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Delete'
                    }
                },
                callback: function (result) {                    
                    if (result == true) {
                        $.ajax({
                            url: '<?php echo BASE_URL();?>Rest/deletePriceGroup/access_token/'+access_token,
                            type: 'POST',
                            data: {
                                uid: uid,                    
                                channel_id: channel_id
                            },
                            success: function(response)
                            {
                                var a = JSON.stringify(response);                 
                                var code = response['code'];                
                                if (code == 200) {
                                    $('#modalAddPriceGroup').modal('hide');
                                    $("#modal_alert").modal('show');
                                    $("#modal_konten").css('background-color','#32c787');
                                    $("#text_modal").html("Successfully Delete");
                                    $("#button_ok").click( function(){
                                        window.location.reload();
                                    });
                                }
                                else if (code == -400) {
                                    window.location.href='<?php echo base_url();?>Admin/Logout';
                                }
                                else
                                {
                                    $('#modalAddPriceGroup').modal('hide');
                                    $("#modal_alert").modal('show');
                                    $("#modal_konten").css('background-color','#ff3333');
                                    $("#text_modal").html("There is an error!!!");
                                    $("#button_ok").click( function(){
                                        window.location.reload();
                                    });
                                }
                            }
                        });
                    }
                }
            });
        });

        $.ajax({
            url: '<?php echo AIR_API;?>pointChannel/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id
            },
            success: function(response)
            {
                var a = JSON.stringify(response);                 
                var code = response['code'];                
                if (code == 200) {
                    var active_point = response['data']['active_point'];
                    $("#chart_totalpoint").text(active_point);
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
            }
        });

    });
</script>