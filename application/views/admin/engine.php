<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar');

    ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/sideadmin'); ?>
        <?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
        ?>
    </aside>

    <section id="content">
        <div class="container">

            <div class="block-header">
                <h2><?php echo $this->lang->line('homeadminn');?></h2>

                <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                        	<li><a href="#"><?php echo $this->lang->line('homeadminn'); ?></a></li>
                         <li class="active"><?php echo $this->lang->line('schedule_engine'); ?></li>                                                       
                     </ol>
                 </li>
             </ul>
         </div> <!-- akhir block header    -->        
         <br/>
         <div class="card">
         <button id="start_engine" class="waves-effect bgm-cyan c-white btn btn-success btn-block form control" name="" value="Start Engine">Start Engine</button>
       </div>
   </div>
</section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">
    $(document).ready(function(){
        $('#start_engine').click(function(){
           $.ajax({
            type: 'GET',
            url: 'https://classmiles.com/tools/schedule_engine',
            success: function(hasil){
                alert('Done');
            }
        });
       });
        

    });
</script>