<style type="text/css">
    .hover11 img {
        opacity: 1;     
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .hover11:hover img {
        opacity: 0.5;
    }
</style>
<header id="header" style="z-index: 10; position: fixed; width: 100%; padding: 0;" class="clearfix" data-current-skin="lightblue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/sideadmin'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header" style="margin-bottom: 50px;">
                <h2>Payment Multicast</h2>
            </div>

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>            

            <div class="card m-t-20 p-20" style="">
                <div class="card-header">
                    <div class="pull-left"><h2>List Payment</h2></div>                    
                <br><br>
                <hr>

                <div class="row" style="margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <?php
                                    $select_all = $this->db->query("SELECT tp.*, tc.name, tc.tutor_id, tc.description, tc.start_time, tc.finish_time, tu.user_name FROM tbl_purchaseclass as tp INNER JOIN tbl_class as tc ON tp.class_id=tc.class_id INNER JOIN tbl_user as tu ON tu.id_user=tp.id_requester")->result_array();
                                    ?>
                                    <tr>
                                        <th data-column-id="no">No</th>
                                        <th data-column-id="classid">Class ID</th>
                                        <th data-column-id="name">Subject Name</th>                                        
                                        <th data-column-id="jenjang">Description</th>  
                                        <th data-column-id="userrequest">User Request</th>
                                        <th data-column-id="starttime">Start Time</th>                                                                              
                                        <th data-column-id="finishtime">Finish Time</th>
                                        <th data-column-id="status">Status</th>
                                        <th data-column-id="aksi" style="text-align: right;" >Action</th>
                                        <th data-column-id="aksi" style="text-align: left;" ></th>

                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;
                                    foreach ($select_all as $row => $v) {                                                                            
                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['class_id']; ?></td>
                                            <td><?php echo $v['name']; ?></td>
                                            <td><?php echo $v['description']; ?></td>
                                            <td><?php echo $v['user_name']; ?></td>                                            
                                            <td><?php echo $v['start_time']; ?></td>
                                            <td><?php echo $v['finish_time']; ?></td>
                                            <td><?php echo $v['status']; ?></td>
                                            <?php 
                                            if ($v['status'] == -1) {
                                                ?>
                                                <td>
                                                    <button disabled class="btn btn-success" title="Confirm Transaction"><i class="zmdi zmdi-check"></i></button>
                                                </td>
                                                <td>
                                                    <button disabled class="btn btn-danger" title="Reject Transaction"><i class="zmdi zmdi-block"></i></button>
                                                </td>
                                                <?php
                                            }
                                            else if($v['status'] == 1){
                                                ?>
                                                <td>
                                                    <button disabled class="btn btn-info" title="Sudah Terkonfirmasi"><i class="zmdi zmdi-badge-check"></i></button>
                                                </td>
                                                <?php
                                            }
                                            else
                                            {
                                                ?>
                                                <td>
                                                    <a data-toggle="modal" rtp="<?php echo $v['trx_id'] ?>" class_id="<?php echo $v['class_id'];?>" tutor_id="<?php echo $v['tutor_id'];?>" requester="<?php echo $v['id_requester'];?>" href="#modalConfirm" class="confirmtransaction"><button class="btn btn-success" title="Confirm Transaction"><i class="zmdi zmdi-check"></i></button></a>
                                                </td>
                                                <!-- href="<?php echo base_url(); ?>Admin/ConfrimPaymentMulticast?id=<?php echo $v['trx_id'] ?>" -->
                                                <td>
                                                    <a data-toggle="modal" rtp="<?php echo $v['trx_id'] ?>" class="hapustransaksi" data-target-color="green" href="#modalDelete"><button class="btn btn-danger" title="Reject Transaction"><i class="zmdi zmdi-block"></i></button></a>
                                                </td>
                                                <?php
                                            }
                                            ?>
                                            
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div> 
                                            
            </div>                                 

            <!-- Modal CONFIRM -->  
            <div class="modal" id="modalConfirm" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true" style="margin-top: 10%;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Konfirmasi Transaksi</h4>
                            <hr>
                            <p><label class="c-gray f-15">Harap periksa kembali data transaksi bank sebelum konfirmasi. Jika sudah silahkan klik tombol Konfirmasi</label></p>                            
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" data-id="" id="konfirmasidatatransaksi" rtp="">Konfirmasi</button>                            
                        </div>                        
                    </div>
                </div>
            </div>     

            <!-- Modal DELETE -->  
            <div class="modal" id="modalDelete" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true" style="margin-top: 10%;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Hapus Transaksi</h4>
                            <hr>
                            <p><label class="c-gray f-15">Apakah anda yakin ingin menghapus</label></p>                            
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tidak</button>
                            <button type="button" class="btn bgm-green" data-id="" id="hapusdatatransaksi" rtp="">Ya</button>                            
                        </div>                        
                    </div>
                </div>
            </div>            

        </div>
    </section>

</section>

<script type="text/javascript">

    $(document).ready(function(){

        $('#konfirmasidatatransaksi').click(function(e){
            var rtp = $(this).attr('rtp');
            var tutor_id = $(this).attr('tutor_id');
            var requester = $(this).attr('requester');
            var class_id = $(this).attr('class_id');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/ConfirmPaymentMulticast",
                type:"POST",
                data: {
                    rtp: rtp,
                    tutor_id:tutor_id,
                    id_user:requester,
                    class_id:class_id
                },
                success: function(html){             
                    // alert("Berhasil menghapus data siswa");
                    $('#modalDelete').modal('hide');
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Success Confirm Transaction");
                    setTimeout(function(){
                        location.reload(); 
                    },2000);
                                        
                } 
            });
        });

        $('#hapusdatatransaksi').click(function(e){
            var rtp = $(this).attr('rtp');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/deletePaymentMulticast",
                type:"POST",
                data: {
                    rtp: rtp
                },
                success: function(html){             
                    // alert("Berhasil menghapus data siswa");
                    $('#modalDelete').modal('hide');
                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Success reject Transaction");
                    setTimeout(function(){
                        location.reload(); 
                    },2000);
                                        
                } 
            });
        });
    });

    $(document).on("click", ".hapustransaksi", function () {
         var myBookId = $(this).attr('rtp');
         $('#hapusdatatransaksi').attr('rtp',myBookId);
    });   

    $(document).on("click", ".confirmtransaction", function () {
         var myBookId = $(this).attr('rtp');
         var tutor_id = $(this).attr('tutor_id');
         var requester = $(this).attr('requester');
         var class_id = $(this).attr('class_id');
         $('#konfirmasidatatransaksi').attr('rtp',myBookId);
         $('#konfirmasidatatransaksi').attr('tutor_id',tutor_id);
         $('#konfirmasidatatransaksi').attr('requester',requester);
         $('#konfirmasidatatransaksi').attr('class_id',class_id);

    });

    $('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable(); 

   
</script>

