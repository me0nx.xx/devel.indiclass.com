<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');

	?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('inc/sideadmin'); ?>
		<?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
		?>
	</aside>

	<section id="content">
		<div class="container">            
			<div class="block-header">
				<h2><?php echo $this->lang->line('tambahadminkuotakelas');?></h2>                
			</div>      		

			<div class="card m-t-20 p-20" style="">

                <div class="card-header">
                    <div class="pull-left"><h2>List Quota</h2></div>
                    <div class="pull-right"><a data-toggle="modal" href="#modaladdkuota"><button class="btn btn-success">+ Add Quota</button></a></div>
                <br><br>
                <hr>

                <div class="row" style="margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12">
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>                                        
                                        <th>Volume Participants</th>
                                        <th>Price</th>                                        
                                        <th>Type</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;

									$getdata = $this->db->query("SELECT * FROM `master_paidmulticast_volume`")->result_array();
                                    foreach ($getdata as $row => $v) { 
                                        $id            = $v['id'];
                                        $prices        = number_format($v['price']);
                                        if ($v['type'] == 'paid_multicast') {
                                            $type      = '<span style="color:#009688;">Paid Multicast Credit</span>';
                                        }
                                        else
                                        {
                                            $type      = '<span style="color:#00BCD4;">Open Multicast Credit</span>';
                                        }
                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['volume']; ?></td>
                                            <td><?php echo 'Rp. '.$prices; ?></td>                                            
                                            <td><strong><?php echo $type;?></strong></td>                                            
                                            <td>
                                                <!-- <button data-toggle="modal" idv="<?php echo $this->encryption->encrypt($id); ?>" class="approverequest btn bgm-bluegray btn-block" data-target-color="green" href="#modalApprove">Edit</button><br><br> -->
                                                <button data-toggle="modal" idv="<?php echo $this->encryption->encrypt($id); ?>" title='Delete Quota' class="rejectrequest btn btn-danger" data-target-color="green" href="#modalDelete"><i class="zmdi zmdi-delete"></i></button>
                                            </td>
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div> 
                                            
            </div>

            <!-- Modal TAMBAH -->  
            <div class="modal" id="modaladdkuota" tabindex="-1" role="dialog" style="top: 13%;" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bgm-cyan">
                            <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                            <h4 class="modal-title c-white">Add Quota</h4>
                        </div>
                        <div class="modal-body m-t-20">

                            <div class="row p-15">    
                                
                                <div class="col-md-12">
                                    <div class="form-group p-r-15">
                                        <label class="fg-label">Volume Participants</label>
                                        <div class="dtp-container fg-line m-l-5" >
                                            <input type="text" class="form-control" name="volume" id="volume" placeholder="Enter volume participants"/>
                                        </div>
                                    </div>
                                </div>                        

                                <div class="col-md-12">
                                    <div class="form-group p-r-15">
                                        <label class="fg-label">Price</label>
                                        <div class="dtp-container fg-line m-l-5" >                                            
                                            <input type="text" id="price" required class="input-sm form-control fg-input nominal" placeholder=" Masukan Harga anda" minlength="9" step="500" maxlength="14" min="10000" max="500000"/>
                                            <input type="text" id="nominalsave" hidden /> 
                                        </div>
                                    </div>
                                </div> 

                                <div class="col-sm-12 col-md-12 col-xs-12">        
                                    <label class="fg-label c-gray">Type</label>                                        
                                    <div class="form-group fg-float">                                    
                                        <div class="fg-line">
                                            <select required name="type" id="type" class="select2 col-md-6 input-sm fg-input form-control" style="width: 100%;">
                                                <option value='open_multicast'>Open Multicast</option>
                                                <option value='paid_multicast'>Paid Multicast</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 m-t-20">
                                    <button class="btn btn-success btn-block saveKuota">Save</button>
                                </div>
                            </div>                                                    

                            <br>
                        </div>
                    </div>
                </div>
            </div>                             

            <!-- Modal DELETE -->  
            <div class="modal fade" id="modalDelete" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Delete Quota</h4>
                            <hr>
                            <p><label class="c-gray f-15">Are you sure?</label></p>                            
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn bgm-green" data-id="" id="hapusquota" rtp="">Yes</button>                            
                        </div>                        
                    </div>
                </div>
            </div>

        </div>
	</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>


<script type="text/javascript">

    $(document).ready(function(){

        $(document).on("click", ".approverequest", function () {
            var myBookId = $(this).attr('idv');
            $('#approve_request').attr('idv',myBookId);
        });

        $(document).on("click", ".rejectrequest", function () {
            var myBookId = $(this).attr('idv');
            $('#hapusquota').attr('rtp',myBookId);
        });

        $('.nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

        $(".nominal").keyup(function() {        
            var clone = $(this).val();
            var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
            $("#nominalsave").val(cloned);
        });

        $('table.display').DataTable( {
            fixedHeader: {
                header: true,
                footer: true
            }
        } );
        $('.data').DataTable(); 

        $('.saveKuota').click(function(e){
            var volume  = $("#volume").val();
            var price   = $("#nominalsave").val();
            var type    = $("#type").val();
            $("#modaladdkuota").modal("hide");
            $.ajax({
                url :"<?php echo base_url() ?>Rest/save_multicast_volume",
                type:"post",
                data: {
                    volume : volume,
                    price : price,
                    type : type
                },
                success: function(response){             
                    if (response['code'] == 200) {
                        $('#modalApprove').modal('hide');
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',response['message']);
                        setTimeout(function(){
                            location.reload();
                        },2500);                        
                    }
                    else
                    {
                        $('#modalApprove').modal('hide');
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',response['message']);
                        setTimeout(function(){
                            location.reload();
                        },2500);
                    }                    
                } 
            });
        });

        $('#hapusquota').click(function(e){
            var idv = $(this).attr('rtp');
            $("#modalDelete").modal("hide");
            $.ajax({
                url :"<?php echo base_url() ?>Rest/delete_multicast_volume",
                type:"post",
                data: {
                    idv: idv
                },
                success: function(response){             
                    // alert("Berhasil menghapus data siswa");                    
                    if (response['code'] == 200) {
                        $('#modalReject').modal('hide');
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',response['message']);
                        setTimeout(function(){
                            location.reload();
                        },2500);                        
                    }
                    else
                    {
                        $('#modalReject').modal('hide');
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',response['message']);
                        setTimeout(function(){
                            location.reload();
                        },2500);
                    }
                } 
            });
        });

    });

</script>