<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');

	?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('inc/sideadmin'); ?>
		<?php
             //$msg2 = echo $this->session->flashdata('msgSucces');
             //echo '<script>alert($msg2);</script>';
		?>
	</aside>

	<section id="content">
		<div class="container">            
			<div class="block-header">
				<h2><?php echo $this->lang->line('adminkuotakelas');?></h2>
			</div>      		

			<div class="card m-t-20 p-20" style="">

                <div class="row" style="margin-top: 2%; overflow-y: auto;">
                
                    <div class="col-md-12">
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tutor Name</th>
                                        <th>Volume Participants</th>
                                        <th>Price</th>
                                        <th>Date Requested</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;

									$getdata = $this->db->query("SELECT tmv.* , ts.user_name FROM tbl_multicast_volume AS tmv INNER JOIN tbl_user AS ts ON tmv.id_tutor=ts.id_user WHERE ts.usertype_id ='tutor' ORDER BY tmv.created_at DESC")->result_array();
                                    foreach ($getdata as $row => $v) { 
                                        $idv                = $v['idv'];
                                    	$prices				= number_format($v['price']);
                                    	$date_request	    = $v['date_request'];
                                    	$user_utc 			= $v['utc'];
                                    	$server_utcc        = $this->Rumus->getGMTOffset();
							            $intervall          = $user_utc - $server_utcc;
							            $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$v['created_at']);
							            $tanggal->modify("+".$intervall ." minutes");
							            $tanggal            = $tanggal->format('Y-m-d H:i:s');
							            $datelimit          = date_create($tanggal);
							            $dateelimit         = date_format($datelimit, 'd/m/y');
							            $harilimit          = date_format($datelimit, 'd');
							            $hari               = date_format($datelimit, 'l');
							            $tahunlimit         = date_format($datelimit, 'Y');
							            $waktulimit         = date_format($datelimit, 'H:s');   
							            $datelimit          = $dateelimit;
							            $sepparatorlimit    = '/';
							            $partslimit         = explode($sepparatorlimit, $datelimit);
							            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
                                        $codestatus         = $v['status'];
                                    	if ($codestatus == 0) {
                                    		$status 	= "<span class='c-orange'>Menunggu verifikasi</span>";
                                    	}
                                    	else if($codestatus == 1)
                                    	{
                                    		$status 	= "<span class='c-green'>Request diterima</span>";
                                    	}
                                    	else if($codestatus == -1){
                                    		$status 	= "<span class='c-red'>Request ditolak</span>";
                                    	}
                                    	else if($codestatus == 100){
                                    		$status 	= "<span class='c-success'>Telah terpakai</span>";
                                    	}

                                        if ($v['type'] == 'paid_multicast') {
                                            $type           = 'Paid Multicast Credit';
                                        }
                                        else
                                        {
                                            $type           = 'Open Multicast Credit';
                                        }
                                        ?>       
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><?php echo $v['user_name']; ?></td>
                                            <td><?php echo $v['volume']; ?></td>
                                            <td><?php echo 'Rp. '.$prices; ?></td>
                                            <td><?php echo $date_request; ?></td>
                                            <td><strong><?php echo $type;?></strong></td>
                                            <td><?php echo $status; ?></td>
                                            <td>
                                                <?php
                                                    if ($codestatus == 0) {
                                                        ?>
                                                        <button data-toggle="modal" idv="<?php echo $this->encryption->encrypt($idv); ?>" class="approverequest btn btn-success btn-block" data-target-color="green" href="#modalApprove">Approve</button><br><br>
                                                        <button data-toggle="modal" idv="<?php echo $this->encryption->encrypt($idv); ?>" class="rejectrequest btn btn-danger btn-block" data-target-color="green" href="#modalReject">Reject</button>
                                                        <?php
                                                    }
                                                    else
                                                    {
                                                        echo '-';
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div> 
                                            
            </div>

            <!-- Modal Approve -->  
            <div class="modal fade" id="modalApprove" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Approve Request Quota</h4>
                            <hr>
                            <p><label class="c-gray f-15">Are you sure approve request?</label></p>                            
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">No</button>
                            <button type="button" class="btn bgm-green" data-id="" id="approve_request" rtp="">Approve</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>

            <!-- Modal DELETE -->  
            <div class="modal fade" id="modalReject" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title c-black">Reject Request Quota</h4>
                            <hr>
                            <p><label class="c-gray f-15">Are you sure reject?</label></p>                            
                        </div>                                                
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">No</button>
                            <button type="button" class="btn bgm-green" data-id="" id="reject_request" rtp="">Reject</button>
                            
                        </div>                        
                    </div>
                </div>
            </div>

        </div>
	</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>


<script type="text/javascript">

    $(document).ready(function(){

        $(document).on("click", ".approverequest", function () {
            var myBookId = $(this).attr('idv');
            $('#approve_request').attr('idv',myBookId);
        });

        $(document).on("click", ".rejectrequest", function () {
            var myBookId = $(this).attr('idv');
            $('#reject_request').attr('idv',myBookId);
        });

        $('table.display').DataTable( {
            fixedHeader: {
                header: true,
                footer: true
            }
        } );
        $('.data').DataTable(); 

        $('#approve_request').click(function(e){
            var idv = $(this).attr('idv');

            $.ajax({
                url :"<?php echo base_url() ?>Rest/approveRequestKuota",
                type:"post",
                data: {
                    idv: idv
                },
                success: function(response){             
                    if (response['code'] == 200) {
                        $('#modalApprove').modal('hide');
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',response['message']);
                        setTimeout(function(){
                            location.reload();
                        },2500);                        
                    }
                    else
                    {
                        $('#modalApprove').modal('hide');
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',response['message']);
                        setTimeout(function(){
                            location.reload();
                        },2500);
                    }                    
                } 
            });
        });

        $('#reject_request').click(function(e){
            var idv = $(this).attr('idv');

            $.ajax({
                url :"<?php echo base_url() ?>Rest/rejectRequestKuota",
                type:"post",
                data: {
                    idv: idv
                },
                success: function(response){             
                    // alert("Berhasil menghapus data siswa");                    
                    if (response['code'] == 200) {
                        $('#modalReject').modal('hide');
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',response['message']);
                        setTimeout(function(){
                            location.reload();
                        },2500);                        
                    }
                    else
                    {
                        $('#modalReject').modal('hide');
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',response['message']);
                        setTimeout(function(){
                            location.reload();
                        },2500);
                    }
                } 
            });
        });

    });

</script>