<main class="main">    

    <header class="header">
       	<?php $this->load->view("inc/navbar_adm"); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <header class="content__title">
            <h1>Modify Class</h1>
            <small>Add tutor in your program class</small>

            <div class="actions">
                <a href="<?php echo base_url();?>Admin/listProgram"><button class="btn btn-secondary"><i class="zmdi zmdi-arrow-right"></i> Back</button></a>
            </div>
        </header>

        <div class="row">         
            <div class="animated fadeIn modal fade" style="color: white;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog" >
                    <div class="modal-content" id="modal_konten">
                        <div class="modal-body" align="center">
                            <label id="text_modal">Halloo</label><br>
                            <button  id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                        </div>
                    </div>
                </div>
            </div>   
            <div class="card col-md-12 col-xs-12">
                <div class="card-block">    
                    <dl class="col-md-12" id="box_edt_program" style="display: none;">
                        <div class="row">
                            <dt class="col-md-3" style="font-size: 15px;">Program name</dt>
                            <dd class="col-md-6"><input type="text" name="program_name" id="txt_edt_programname" class="form-control" style="background-color: #EFEFEF; padding: 10px;"></dd>
                            <dt class="col-md-2" style="font-size: 15px;"></dt>
                            <dd class="col-sm-1"><i class="zmdi zmdi-close" style="cursor: pointer;" title="Close" id="clEdtProgram"></i></dd>                        

                            <dt class="col-md-3" style="font-size: 15px;">Group name</dt>
                            <dd class="col-md-6">
                                <div class="form-group">
                                    <select class="select2 form-control" required id="select_group" style="width: 100%;">
                                        <option disabled="disabled" selected="" value="0">Choose Group</option>                            
                                    </select>   
                                </div>
                            </dd>
                            <dt class="col-md-3" style="font-size: 15px;"></dt>

                            <!-- <dt class="col-md-3" style="font-size: 15px;">Sesi</dt>
                            <dd class="col-md-6"><input type="text" name="program_name" id="txt_edt_programsesi" class="form-control" style="background-color: #EFEFEF; padding: 10px;"></dd>
                            <dt class="col-md-3" style="font-size: 15px;"></dt> -->

                            <dt class="col-md-3" style="font-size: 15px;"></dt>
                            <dd class="col-md-6"><button class="btn btn-success" id="updt_Program">Update</button></dd>
                            <dt class="col-md-3" style="font-size: 15px;"></dt>
                        </div>
                    </dl>
                    <dl class="col-md-12" id="box_show_program" style="display: block;">
                        <div class="row">
                            <dt class="col-sm-3" style="font-size: 15px;">Program name</dt>
                            <dd class="col-sm-8" id="modifyName"></dd>
                            <dd class="col-sm-1"><i class="zmdi zmdi-edit" style="cursor: pointer;" title="Edit Data Program" id="edtProgram"></i></dd>

                            <dt class="col-sm-3" style="font-size: 15px;">Group Name</dt>
                            <dd class="col-sm-9 font-weight-bold" id="modifyGroup"></dd>
                            <dd class="col-sm-9 offset-sm-3">
                                <dl class="row" id="modifyListGroup">
                                    
                                </dl>
                            </dd>
                            
                            <dt class="col-sm-3" style="font-size: 15px;">Number of Sessions</dt>
                            <dd class="col-sm-9" id="modifySesi"></dd>
                        </div>
                    </dl>
                    <hr>
                    <div class="row table-responsive">
                        <div class="pull-right">
                            <button style="float: right;" class="btn btn-success" id="addSession"><i class="zmdi zmdi-plus"></i> Add Session</button>
                        </div>
                        <div id="tables_modifyClass"></div>
                    </div>
                    <button class="btn btn-success btn-block waves-effect" id="saveCreateClass" data-programid="" style="padding: 1%; margin-top: 3%;">Save & Request Tutor</button>

                </div>
            </div>            
        </div>

        <!-- Modal modalAddTutor -->  
        <div class="modal fade show" id="modalAddProgram">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">                        
                        <h4 class="modal-title c-white pull-left">Add Program</h4>
                        <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                    </div>
                    <div class="modal-body m-t-20">
                        
                        <br>
                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-success waves-effect btn-block p-10" style="margin-top: 2%;" id="saveGroup">Save</button>                            
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade show" id="modalDeleteProgram" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>Remove Program</b></h3>
                    </div>
                    <div class="modal-body">                        
                        <label class="c-gray f-15">Are you sure you want to remove?</label>                         
                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-danger waves-effect" id="proses_hapusprogram" data-id="" rtp="">Yes</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade show" id="modalEditWaktu" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>Change Time</b></h3>
                    </div>
                    <div class="modal-body">
                        <form class="new-event__form">            
                            <div class="input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                <div class="form-group">
                                    <!-- <input type='text' class='form-control new-event__title' id='timeee' placeholder='Waktu' /> -->
                                    <input type="text" id="eadit_timeee" class="form-control datetime-picker floating-label " placeholder="Select Time">
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="input-group" style="margin-top: 5%;">
                                <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                <div class="form-group" style="margin-top: 3%;">
                                    <select class="select2 form-control" required id="edit_durasi_sesi" style="width: 100%;">
                                        <option disabled="disabled" selected="" value="0">Select Duration</option>
                                        <option value="900">15 Minutes</option>
                                        <option value="1800">30 Minutes</option>
                                        <option value="2700">45 Minutes</option>
                                        <option value="3600">1 Hours</option>
                                        <option value="5400">1 Hours 30 Minutes</option>
                                        <option value="7200">2 Hours</option>
                                        <option value="9000">2 Hours 30 Minutes</option>
                                        <option value="10800">3 Hours</option>
                                        <option value="12600">3 Hours 30 Minutes</option>
                                        <option value="14400">4 Hours</option>
                                    </select>   
                                </div>
                            </div> 
                            

                            <input type="hidden" class="event_date" />
                            <input type="hidden" class="event_time" />
                            <input type="hidden" class="event_duration" />
                        </form>
                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-success waves-effect" id="btnEditWaktu" data-id="" rtp="">Change</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade show" id="modalAddSession" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>Add Session</b></h3>
                        <hr>
                    </div>
                    <div class="modal-body">
                        <form class="new-event__form">                                        
                            <div class="input-group">                                
                                <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                <div class="form-group">
                                    <!-- <input type='text' class='form-control new-event__title' id='timeee' placeholder='Waktu' /> -->
                                    <input type="text" id="add_date" class="form-control date-picker floating-label " placeholder="Select Date Session">
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="input-group" style="margin-top: 5%;">                                
                                <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                <div class="form-group">
                                    <!-- <input type='text' class='form-control new-event__title' id='timeee' placeholder='Waktu' /> -->
                                    <input type="text" id="add_time" class="form-control time-picker floating-label " placeholder="Select Time Session">
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                            <div class="input-group" style="margin-top: 5%;">
                                <span class="input-group-addon"><i class="zmdi zmdi-time-interval"></i></span>
                                <div class="form-group" style="margin-top: 3%;">
                                    <select class="select2 form-control" required id="add_durasi_sesi" style="width: 100%;">
                                        <option disabled="disabled" selected="" value="0">Select Duration Session</option>
                                        <option value="900">15 Minutes</option>
                                        <option value="1800">30 Minutes</option>
                                        <option value="2700">45 Minutes</option>
                                        <option value="3600">1 Hours</option>
                                        <option value="5400">1 Hours 30 Minutes</option>
                                        <option value="7200">2 Hours</option>
                                        <option value="9000">2 Hours 30 Minutes</option>
                                        <option value="10800">3 Hours</option>
                                        <option value="12600">3 Hours 30 Minutes</option>
                                        <option value="14400">4 Hours</option>
                                    </select>   
                                </div>
                            </div> 
                            

                            <input type="hidden" class="event_date" />
                            <input type="hidden" class="event_time" />
                            <input type="hidden" class="event_duration" />
                        </form>
                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-success waves-effect" id="btnAddSession" data-id="" rtp="">Add Session</button>
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>      

<script type="text/javascript">
    var access_token = "<?php echo $this->session->userdata('access_token');?>";
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
    var dataSet = [];
    var user_utc = new Date().getTimezoneOffset();
    user_utc = -1 * user_utc;  
    var countSesi = 0;
    var schedule_at = "";
    var schedule_dt = [];

    $(document).ready(function() {        

        var pid = "<?php echo $_GET['pid'];?>";        

        $.ajax({
            url: '<?php echo AIR_API;?>channel_listModifyClass/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id,
                program_id: pid,
                user_utc: '420'
            },
            success: function(response)
            {
                var a = JSON.stringify(response);                 
                var code = response['code'];                  
                if (code == 200) {
                    // for (var i = 0; i < response.data.length; i++) {
                        var program_id      = response['data'][0]['program_id'];
                        var program_name    = response['data'][0]['program_name'];
                        var sesi            = response['data'][0]['sesi'];
                        var group_id        = response['data'][0]['group_id'];
                        var group_name      = response['data'][0]['group_name'];                        
                        var listParti       = response['data'][0]['listParti'];
                        var group_parti     = response['data'][0]['listGroupParti'];
                        schedule_at         = response['data'][0]['schedule'];
                        schedule_at         = JSON.parse(schedule_at); 
                        countSesi = sesi;

                        $("#saveCreateClass").data('programid',program_id);
                        $("#modifyName").text(program_name);
                        $("#modifyGroup").text(group_name+' :');
                        $("#modifySesi").text(sesi);
                        $("#edtProgram").attr('program_name',program_name);
                        $("#edtProgram").attr('group_id',group_id);
                        $("#edtProgram").attr('sesi',sesi);
                        // for (var a = 0; a < group_parti.length; a++) {
                        //     var aa = group_parti[a]['user_name'];                            
                        //     $("#modifyListGroup").append('<dd class="col-sm-12">- '+aa+'</dd>');                            
                        // }
                        if (listParti != null) {
                            for (var i = 0; i < listParti.length; i++) {
                                var schedule_tanggal= listParti[i]['schedule_tanggal'];
                                var schedule_waktu  = listParti[i]['schedule_waktu'];
                                var schedule_durasi = listParti[i]['schedule_durasi'];
                                var eventDurasi     = listParti[i]['eventDurasi'];
                                var schedule_tutorid= listParti[i]['schedule_tutorid'];
                                var schedule_status = listParti[i]['schedule_status'];                            
                                if (schedule_status == 1) {
                                	var htmldisabled = "disabled='disabled'";
                                	var tutordisabled = "";
                                	var checkinalert = "1";
                                    var status = '<label style="color:#4183D7;">Class has been created</label>';
                                }
                                else if (schedule_status == 2) {
                                    var htmldisabled = "disabled='disabled'";
                                    var tutordisabled = "";
                                    var checkinalert = "1";
                                    var status = '<label style="color:#F5AB35;">Waiting approve From tutor</label>';
                                }
                                else
                                {
                                	var htmldisabled = "";
                                	var tutordisabled = "disabled='disabled'";
                                	var checkinalert = "0";
                                    var status = '<label>Not set class</label>';
                                }
                                var inputDate = '<input id="time" type="time">';
                                var combo           = '<select class="form-control select_listtutor" '+htmldisabled+' data-checkinalert='+checkinalert+' required id="select_listtutor'+(i+1)+'" data-programid="'+program_id+'" data-sesi="'+sesi+(i+1)+'" data-tanggal="'+schedule_tanggal+'" data-waktu="'+schedule_waktu+'" data-durasi="'+eventDurasi+'" data-status="'+schedule_status+'" style="width: 100%;"><option disabled="disabled" selected="" value="0">Select Tutor</option><option value="-1">Select Later</option></select><label style="color:red; display:none;" id="alertCombo'+(i+1)+'">Please choose or choose later!!!</label>';
                                var chooseMapel     = '<select class="form-control select_mapel" '+htmldisabled+' data-checkinalert='+checkinalert+' data-status="'+schedule_status+'" required id="select_mapel'+(i+1)+'" style="width: 100%;"><option disabled="disabled" selected="" value="0">Pilih Mata Pelajaran</option><option value="-1">Select Later</option></select><label style="color:red; display:none;" id="alertMapel'+(i+1)+'">Please choose or choose later!!!</label>';
                                var enterDesc       = '<textarea rows="5" name="topikpelajaran" '+htmldisabled+' data-checkinalert='+checkinalert+' id="select_desc'+(i+1)+'" data-status="'+schedule_status+'" class="form-control" required placeholder="Put the lesson topic here"></textarea><label style="color:red; display:none;" id="alertDesc'+(i+1)+'">Please fill it !!!</label>';
                                var editButton = "<button "+htmldisabled+" class='modifyDate btn waves-effect' style='background-color: #607D8B;' data-programid='"+program_id+"' data-sesi="+sesi+(i+1)+" data-tanggal="+schedule_tanggal+" data-waktu="+schedule_waktu+" data-durasi="+eventDurasi+" data-status="+schedule_status+" title='Edit Date'><i class='zmdi zmdi-edit zmdi-hc-fw' style='color:#fff;'></i></button>";
                                var changeTutorButton = "<button "+tutordisabled+" class='modifyTutor btn waves-effect' style='background-color: #F5AB35;' data-programid='"+program_id+"' data-sesi="+i+" data-tanggal="+schedule_tanggal+" data-waktu="+schedule_waktu+" data-durasi="+eventDurasi+" data-status="+schedule_status+" title='Change Tutor'><i class='zmdi zmdi-refresh zmdi-hc-fw' style='color:#fff;'></i></button>";
                                var deleteButton = "<button class='btn waves-effect btn-danger deleteDateSesi' data-programid='"+program_id+"' data-sesi="+i+" data-tanggal="+schedule_tanggal+" data-waktu="+schedule_waktu+" data-durasi="+eventDurasi+" data-status="+schedule_status+" title='Delete Sesi'><i class='zmdi zmdi-delete zmdi-hc-fw' style='color:#fff;'></i></button>";
                                
                                dataSet.push([i+1, 'sesi '+(i+1), schedule_tanggal, schedule_waktu, schedule_durasi, combo, chooseMapel, enterDesc, status, editButton , changeTutorButton, deleteButton]);                            
                            }
                        }
                        select_listtutor(); 
                        select_mapel();                       
                    // }
                        $('.new-event__title').datetimepicker   
                        ({
                            sideBySide: true, 
                            showClose: true,                   
                            format: 'HH:mm',
                            stepping: 15,
                            icons: {
                                time: 'fa fa-clock-o',
                                date: 'fa fa-calendar',
                                up: 'fa fa-angle-up',
                                down: 'fa fa-angle-down',
                                previous: 'fa fa-angle-left',
                                next: 'fa fa-angle-right',
                                today: 'fa fa-dot-circle-o',
                                clear: 'fa fa-trash',
                                close: 'fa fa-times'
                            }
                        });

                    $('#tables_modifyClass').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="modifyClass"></table>' );
             
                    $('#modifyClass').dataTable( {
                        "data": dataSet,
                        "columns": [
                            { "title": "No", "width" : 5},
                            { "title": "Session", "width" : 20},
                            { "title": "Date", "width" : 20},
                            { "title": "Time", "width" : 20},
                            { "title": "Duration", "width" : 20},                            
                            { "title": "Tutor", "width" : 100},
                            { "title": "Lesson", "width" : 70},
                            { "title": "Topic", "width" : 70},
                            { "title": "Status", "width" : 30},
                            { "title": "", "width" : 30},
                            { "title": "", "width" : 30},
                            { "title": "", "width" : 30}
                        ]
                    });
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
                else
                {
                    $('#tables_modifyClass').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="modifyClass"></table>' );
             
                    $('#modifyClass').dataTable( {                        
                        "columns": [
                            { "title": "No", "width" : 5},
                            { "title": "Session", "width" : 20},
                            { "title": "Date", "width" : 20},
                            { "title": "Time", "width" : 20},
                            { "title": "Duration", "width" : 20},                            
                            { "title": "Tutor", "width" : 100},
                            { "title": "Lesson", "width" : 70},
                            { "title": "Topic", "width" : 70},
                            { "title": "Status", "width" : 30},
                            { "title": "", "width" : 30},
                            { "title": "", "width" : 30},
                            { "title": "", "width" : 30}
                        ]
                    });
                }
            }
        });             
	
		function select_listtutor(){
			$.ajax({
	            url: '<?php echo AIR_API;?>listTutor/access_token/'+access_token,
	            type: 'POST',
	            data: {
	                channel_id: channel_id
	            },
	            success: function(response)
	            {
	                var a = JSON.stringify(response); 
	                if (response['code'] == -400) {
	                    window.location.href='<?php echo base_url();?>Admin/Logout';
	                }
	                for (var i = 0; i < response.data.length; i++) {
	                    var email = response['data'][i]['email'];
	                    var user_name = response['data'][i]['user_name'];
	                    var tutor_id = response['data'][i]['tutor_id'];
	                    // echo '<option value="'.$v['tutor_id'].'">'.$v['user_name'].' - '.$v['email'].'</option>';                    
	                    $(".select_listtutor").append("<option value='"+tutor_id+"'>"+user_name+" - "+email+"</option>");	                    
	                }
	                $(".select_listtutor").select2();
	            }
	        });	        
		}

		function select_mapel(){			
	        $.ajax({
	            url: '<?php echo AIR_API;?>channel_listMapel/access_token/'+access_token,
	            type: 'POST',
	            data: {
	                channel_id: channel_id
	            },
	            success: function(response)
	            {
	                var a = JSON.stringify(response); 
	                if (response['code'] == -400) {
	                    window.location.href='<?php echo base_url();?>Admin/Logout';
	                }
	                for (var i = 0; i < response.data.length; i++) {
	                    var subject_id = response['data'][i]['subject_id'];
	                    var subject_name = response['data'][i]['subject_name'];
	                    var jenjangnamelevel = response['data'][i]['jenjangnamelevel'];
	                    // echo '<option value="'.$v['tutor_id'].'">'.$v['user_name'].' - '.$v['email'].'</option>';                    
	                    $(".select_mapel").append("<option value='"+subject_id+"'>"+subject_name+" - "+jenjangnamelevel+"</option>");	                   
	                }
	                $(".select_mapel").select2();
	            }
	        });
	    }         

        $(document).on('click', '#saveCreateClass', function(){   
            $(".page-loader").fadeIn();            
        	var data_arr = [];     	
        	var data_arrlater = []; 
           
        	for (var i = 1; i <= countSesi; i++) {        		
        		var combo       = $("#select_listtutor"+i).val();
                var chooseMapel = $("#select_mapel"+i).val();
                var enterDesc   = $("#select_desc"+i).val();
                var program_id  = $("#select_listtutor"+i).data('programid'); // 5  
                var tanggal     = $("#select_listtutor"+i).data('tanggal');
                var waktu       = $("#select_listtutor"+i).data('waktu');
                var durasi      = $("#select_listtutor"+i).data('durasi'); 
                var status      = $("#select_listtutor"+i).data('status');
                var checkalert  = $("#select_listtutor"+i).data('checkinalert'); 

                if (status == 1) {                    
                }   
                else
                {            
                	if (checkalert == 0) {              
	                    if (combo == null) {

	                        if (chooseMapel != null) {
	                            $("#alertMapel"+i).css('display','none');
	                        }
	                        if (enterDesc != null) {
	                            $("#alertDesc"+i).css('display','none');
	                        }
	                    	$("#alertCombo"+i).css('display','block');
	                        // window.location.reload();
                            $(".page-loader").fadeOut();
	                        return false;
	                    }
	                    if (chooseMapel == null) {
	                        if (combo != null) {
	                            $("#alertCombo"+i).css('display','none');
	                        }
	                        if (enterDesc != null) {
	                            $("#alertDesc"+i).css('display','none');
	                        }
	                    	$("#alertMapel"+i).css('display','block');
	                        // window.location.reload();
	                        $(".page-loader").fadeOut();
                            return false;
	                    }
                        if (enterDesc == "") {
                            if (combo != null) {
                                $("#alertCombo"+i).css('display','none');
                            }
                            if (enterDesc != null) {
                                $("#alertMapel"+i).css('display','none');
                            }
                            $("#alertDesc"+i).css('display','block');
                            // window.location.reload();
                            $(".page-loader").fadeOut();
                            return false;
                        }
	                    if(combo != -1)
	                    {                        
	                    	$("#alertCombo"+i).css('display','none');
	                    }
	                    if(chooseMapel != -1)
	                    {
	                    	$("#alertMapel"+i).css('display','none');                        
	                    }
	                    if(combo != null)
	                    {
	                    	$("#alertCombo"+i).css('display','none');
	                    }
	                    if(chooseMapel != null)
	                    {
	                    	$("#alertMapel"+i).css('display','none');                        
	                    }
	                    if (enterDesc != "") {
	                    	$("#alertDesc"+i).css('display','none');                        
	                    }

	                    if(combo != null && combo != -1 && chooseMapel != null && chooseMapel != -1 && enterDesc != "")
	                    {                	
	                    	$("#alertCombo"+i).css('display','none');
	                    	$("#alertMapel"+i).css('display','none');
	                    	$("#alertDesc"+i).css('display','none');
	                    	// createClassNowModify(program_id,combo,chooseMapel,enterDesc,tanggal,waktu,durasi,'0');
	                    	data_arr.push({program_id: program_id, tutor_select: combo, mapel_select: chooseMapel, enterDesc: enterDesc, tanggal: tanggal, waktu: waktu+":00", waktuu: waktu, durasi: durasi,channel_id:channel_id,user_utc:user_utc });
	                    }
	                    else
	                    {   	                    	
	                    	// createClassLaterModify(program_id,tanggal,waktu,durasi,i);
	                    	data_arrlater.push({program_id: program_id, tanggal: tanggal, waktu: waktu+":00", waktuu: waktu, durasi: durasi, channel_id: channel_id, user_utc: user_utc});
	                    }
                    }
                }
        	}        	        
        	createClassNowModify(data_arr);
        	createClassLaterModify(data_arrlater);        	
        });
    
        function createClassNowModify(data_arr='') {         
            console.log(data_arr)  ;
        	$.ajax({
                url: '<?php echo AIR_API;?>channel_createNowClass/access_token/'+access_token,
                type: 'POST',
                data: {
                    data_arr: data_arr
                },
                success: function(response)
                {
                	var a = JSON.stringify(response);                	
                	var code = response['code'];

                	if (code == 200) {                     	                        
                        $("#modal_alert").modal('show');                
                        $("#modal_konten").css('background-color','#32c787');    
                        $("#text_modal").html(response['message']);
                        $(".page-loader").fadeOut();
                        $("#button_ok").click( function(){
                            location.reload();
                        });                        
                	}
                    else if (code == 402) {                        
                        $("#modal_alert").modal('show');                
                        $("#modal_konten").css('background-color','#ff3333');    
                        $("#text_modal").html(response['message']);
                        $(".page-loader").fadeOut();
                        $("#button_ok").click( function(){
                            location.reload();
                        });                        
                        /*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',response['message']);                        
                        setTimeout(function(){
                            window.location.reload();
                        },2000);*/
                    }
                    else
                    {
                    	$("#modal_alert").modal('show');                
                        $("#modal_konten").css('background-color','#ff3333');    
                        $("#text_modal").html('Error, Please try again');
                        $(".page-loader").fadeOut();
                        $("#button_ok").click( function(){
                            location.reload();
                        }); 
                    }
                }
            });
        }

        function createClassLaterModify(data_arrlater='') {
            // alert("HAI");
        	$.ajax({
                url: '<?php echo AIR_API;?>channel_createLaterClass/access_token/'+access_token,
                type: 'POST',
                data: {
                    data_arrlater: data_arrlater
                },
                success: function(response)
                {
                	var a = JSON.stringify(response);                	
                	var code = response['code'];
                	if (code == 200) {
                        // if (countSesi == last) {
                            $("#modal_alert").modal('show');                
                            $("#modal_konten").css('background-color','#32c787');    
                            $("#text_modal").html(response['message']);
                            $(".page-loader").fadeOut();
                            $("#button_ok").click( function(){
                                location.reload();
                            });
                        // }
                        // else{
                        //     $(".page-loader").fadeOut();
                        //     return 1;
                        // }
                        /*notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',response['message']);                        
                        setTimeout(function(){
                            window.location.reload();
                        },2000);*/
                	}
                }
            });
        }

        $(document).on('click', '.deleteProgram', function(){
            var programid    = $(this).data('programid');
            $('#proses_hapusprogram').attr('rtp',programid);
            $("#modalDeleteProgram").modal("show");
        });

        $(document).on('click', '.modifyTutor', function(){
            var programid    = $(this).data('programid');
            var key    		 = $(this).data('sesi');
            var eventDate    = $(this).data('tanggal');
            var eventTime    = $(this).data('waktu');
            var eventDurasi  = $(this).data('durasi');
            $.ajax({
                url :"<?php echo AIR_API;?>channel_changeTutorModify/access_token/"+access_token,
                type:"post",
                data: {
                    program_id: programid,
                    channel_id: channel_id,
                    key: key,
                    eventDate: eventDate,
                    eventTime: eventTime,
                    eventDurasi: eventDurasi
                },
                success: function(response){   
                    var code = response['code'];
                    if (code == 200) {
                    	$("#modal_alert").modal('show');                
                        $("#modal_konten").css('background-color','#32c787');    
                        $("#text_modal").html("Successfully Cancel Request Tutor");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                    }   
                    else{         

                        $("#modal_alert").modal('show');                
                        $("#modal_konten").css('background-color','#ff33333');    
                        $("#text_modal").html("There is an error!!!");
                        $("#button_ok").click( function(){
                            location.reload();
                        });           
                    }          
                }
            });
        });

        $(document).on('click', '.modifyDate', function(){
            var program_id  = $(this).data('programid');
            var tanggal     = $(this).data('tanggal');
            var waktu       = $(this).data('waktu');
            var durasi      = $(this).data('durasi'); 
            var status      = $(this).data('status');
            
            $(".event_time").val(waktu);
            $(".event_duration").val(durasi);
            $(".event_date").val(tanggal);
            $('#btnEditWaktu').attr('rtp',program_id);
            $("#modalEditWaktu").modal("show");
        });   

        function GetEndDate(start_date,duration){ // start_date = "05-25-2017 05:00"        
            var d = fecha.parse(start_date, 'YYYY-MM-DD HH:mm');

            d.setSeconds(d.getSeconds() + duration);
            return fecha.format(d, 'YYYY-MM-DD HH:mm');;
        }

        $(document).on('click', '#btnEditWaktu', function(){
            
            var date = new Date();
            var waktu_sekarang =  moment(date).format("YYYY-MM-DD HH:mm");     
            var currentHours = date.getHours();
            var jam_now = ("0" + currentHours).slice(-2) + ":" + date.getMinutes();
            var waktutanggal = $("#eadit_timeee").val();
            var waktuBaru =  moment(waktutanggal).format("HH:mm");    
            var durasiBaru =  $("#edit_durasi_sesi").val();    
            var tanggalBaru =  moment(waktutanggal).format("YYYY-MM-DD");  

            var waktuLama = $(".event_time").val();
            var durasiLama = $(".event_duration").val();
            var tanggalLama = $(".event_date").val();
            // alert(tanggalLama);
            var pid = "<?php echo $_GET['pid'];?>";  
            if (waktutanggal == null || waktutanggal =="") {
                $("#modalEditWaktu").modal("hide");
                $("#modal_alert").modal('show');                
                $("#modal_konten").css('background-color','#ff3333');    
                $("#text_modal").html("Data can not be empty !!!");
                $("#button_ok").click( function(){
                    $("#modalEditWaktu").modal("show");
                });
            }
            else if (waktu_sekarang > waktutanggal ) {
                $("#modalEditWaktu").modal("hide");
                $("#modal_alert").modal('show');                
                $("#modal_konten").css('background-color','#ff3333');    
                $("#text_modal").html("The date you chose is past !!!!");
                $("#button_ok").click( function(){
                    $("#modalEditWaktu").modal("show");
                });
            }
            else{
                $.ajax({
                    url: '<?php echo AIR_API;?>channel_listModifyClass/access_token/'+access_token,
                    type: 'POST',
                    data: {
                        channel_id: channel_id,
                        program_id: pid,
                        user_utc: '420'
                    },
                    success: function(response)
                    {

                        var a = JSON.stringify(response);                 
                        var code = response['code'];                  
                        if (code == 200) {

                            // for (var i = 0; i < response.data.length; i++) {
                                var program_id      = response['data'][0]['program_id'];
                                var program_name    = response['data'][0]['program_name'];
                                var sesi            = response['data'][0]['sesi'];
                                var group_name      = response['data'][0]['group_name'];                        
                                var listParti       = response['data'][0]['listParti'];
                                var group_parti     = response['data'][0]['listGroupParti'];
                                countSesi = sesi;
                                checkRange = -1;
                                for (var i = 0; i < listParti.length; i++) {

                                    var schedule_tanggal= listParti[i]['schedule_tanggal'];
                                    var schedule_waktu  = listParti[i]['schedule_waktu'];
                                    var schedule_durasi = listParti[i]['schedule_durasi'];
                                    var eventDurasi     = listParti[i]['eventDurasi'];
                                    var schedule_tutorid= listParti[i]['schedule_tutorid'];
                                    var schedule_status = listParti[i]['schedule_status'];   

                                    datebefore      = schedule_tanggal+" "+schedule_waktu;
                                    datebeforeEnd   = GetEndDate(datebefore,eventDurasi); 

                                    newdatebefore      = waktutanggal;
                                    newdatebeforeEnd   = GetEndDate(newdatebefore,durasiBaru); 
                                    // return false;  
                                    checkRange = -1;
                                    if ( ( Date.parse(newdatebefore) >= Date.parse(datebeforeEnd) ) || ( Date.parse(newdatebeforeEnd) <= Date.parse(datebefore) ) ) {
                                        checkRange = 1;
                                    }                                                              

                                }      
                                   
                                if (checkRange == -1) {
                                    $("#modalEditWaktu").modal("hide");
                                    $("#modal_alert").modal('show');                
                                    $("#modal_konten").css('background-color','#ff3333');    
                                    $("#text_modal").html("Sorry, the date and time are already class");
                                    $("#button_ok").click( function(){
                                        $("#modalEditWaktu").modal("show");
                                    });
                                }   
                                else if (checkRange ==1) {

                                    var eventDateBefore = tanggalLama;
                                    var eventTimeBefore = waktuLama;
                                    var eventDurasiBefore = durasiLama;
                                    var eventDateAfter = tanggalBaru;
                                    var eventTimeAfter = waktuBaru;
                                    var eventDurasiAfter = durasiBaru;
                                    // alert(eventDateBefore+" "+eventTimeBefore+" "+eventDurasiBefore);
                                    // alert(eventDateAfter+" "+eventTimeAfter+" "+eventDurasiAfter);
                                    // return false;

                                     $.ajax({
                                        url: '<?php echo AIR_API;?>Channel_UpdateProgram/access_token/'+access_token,
                                        type: 'POST',
                                        data: {
                                            eventDateAfter : eventDateAfter,
                                            eventTimeAfter : eventTimeAfter,
                                            eventDurasiAfter : eventDurasiAfter,
                                            eventDateBefore : eventDateBefore,
                                            eventTimeBefore : eventTimeBefore,
                                            eventDurasiBefore : eventDurasiBefore,
                                            channel_id: channel_id,
                                            program_id: pid,

                                            user_utc: '420'
                                        },
                                        success: function(response)
                                        {
                                                          
                                            if (code == 200) {
                                                $("#modalEditWaktu").modal("hide");
                                                $("#modal_alert").modal('show');                
                                                $("#modal_konten").css('background-color','#32c787');    
                                                $("#text_modal").html("Successfully Changing Data ");
                                                $("#button_ok").click( function(){
                                                    location.reload();
                                                });
                                            }

                                           
                                        }
                                    });

                                }
                            // }
                             
                     
                        }
                       
                    }
                });   
            }        

        }); 

        $('#edit_timeee').datetimepicker   
        ({
            sideBySide: true, 
            showClose: true,                   
            format: 'HH:mm',
            stepping: 15,
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-angle-up',
                down: 'fa fa-angle-down',
                previous: 'fa fa-angle-left',
                next: 'fa fa-angle-right',
                today: 'fa fa-dot-circle-o',
                clear: 'fa fa-trash',
                close: 'fa fa-times'
            }
        });

        $(document).on("click", "#proses_hapusprogram", function(){   
            $('#modalDeleteProgram').modal('hide');
            var program_id = $(this).attr('rtp');
            $.ajax({
                url :"<?php echo AIR_API;?>deleteProgram/access_token/"+access_token,
                type:"post",
                data: {
                    rtp: program_id,
                    channel_id: channel_id
                },
                success: function(response){   
                    var code = response['code'];
                    if (code == 200) {
                        $("#modal_alert").modal('show');                
                        $("#modal_konten").css('background-color','#32c787');    
                        $("#text_modal").html("Successfully deleted the Program");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                        /*notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Berhasil menghapus Program");                        
                        setTimeout(function(){
                            window.location.reload();
                        },2000);*/
                    }   
                    else{         

                        $("#modal_alert").modal('show');                
                        $("#modal_konten").css('background-color','#ff33333');    
                        $("#text_modal").html("There is an error!!!");
                        $("#button_ok").click( function(){
                            location.reload();
                        });           
                        /*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Terjadi kesalahan!!!");
                        setTimeout(function(){
                            window.location.reload();
                        },2000);*/
                    }                  
                } 
            });
        });

        $(document).on("click", "#edtProgram", function(){
            $("#box_show_program").css('display','none');
            $("#box_edt_program").css('display','block');
            var group_id = $(this).attr('group_id');
            // var sesi = $(this).attr('sesi');
            var program_name = $(this).attr('program_name');
            $("#txt_edt_programname").val(program_name);
            // $("#txt_edt_programsesi").val(sesi);
            listGroup(group_id);
        });

        $(document).on("click", "#clEdtProgram", function(){
            $("#box_edt_program").css('display','none');
            $("#box_show_program").css('display','block');

        });

        $('#select_group').select2();
        $("#add_durasi_sesi").select2();

        function listGroup(txgroup_id = '')
        {
            $.ajax({
                url: '<?php echo AIR_API;?>listGroup/access_token/'+access_token,
                type: 'POST',
                data: {
                    channel_id: channel_id
                },
                success: function(response)
                {
                    var a = JSON.stringify(response); 
                    if (response['code'] == -400) {
                        window.location.href='<?php echo base_url();?>Admin/Logout';
                    }

                    for (var i = 0; i < response.data.length; i++) {
                        var group_id = response['data'][i]['group_id'];
                        var group_name = response['data'][i]['group_name'];
                        if (group_id == txgroup_id) {
                            $("#select_group").append("<option selected='true' value='"+group_id+"'>"+group_name+"</option>");  
                        }
                        else
                        {
                            $("#select_group").append("<option value='"+group_id+"'>"+group_name+"</option>");
                        }
                        
                    }
                    
                }
            });
        }

        $(document).on("click", "#updt_Program", function(){
            var program_name = $("#txt_edt_programname").val();
            var group_id = $("#select_group").val()

            $.ajax({
                url :"<?php echo AIR_API;?>update_programById/access_token/"+access_token,
                type:"post",
                data: {
                    program_name: program_name,                    
                    group_id : group_id,
                    channel_id : channel_id,
                    program_id : pid,
                    user_utc: '420'
                },
                success: function(response){   
                    var code = response['code'];
                    if (code == 200) {
                        $("#box_edt_program").css('display','none');
                        $("#box_show_program").css('display','block');

                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#text_modal").html("Successfully Update Program");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                    }
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>Admin/Logout';
                    }
                }
            });

        });

        $(document).on('click', '#addSession', function(){               
                        
            $("#modalAddSession").modal("show");            

        });

        function GetEndDate(start_date,duration){ // start_date = "05-25-2017 05:00"        
            var d = fecha.parse(start_date, 'YYYY-MM-DD HH:mm');

            d.setSeconds(d.getSeconds() + duration);
            return fecha.format(d, 'YYYY-MM-DD HH:mm');
        }

        function GetExpiredDate(start_date,minutes){ // start_date = "05-25-2017 05:00"        
            var d = fecha.parse(start_date, 'YYYY-MM-DD HH:mm:ss');

            d.setMinutes(d.getMinutes() - minutes);
            return fecha.format(d, 'YYYY-MM-DD HH:mm:ss');
        }    

        function GetDateHour(start_date,duration){ // start_date = "05-25-2017 05:00"        
            var d = fecha.parse(start_date, 'YYYY-MM-DD HH:mm:ss');

            d.setSeconds(d.getSeconds() + duration);
            return fecha.format(d, 'YYYY-MM-DD HH:mm:ss');
        } 

        $(document).on('click', '#btnAddSession', function(){               
            
            var eventDate       = $("#add_date").val();
            var eventTime       = $("#add_time").val();
            var eventDurasi     = $("#add_durasi_sesi").val();
            var date            = new Date();
            var waktu_sekarang  = moment(date).format("YYYY-MM-DD HH:mm");     
            var waktu_expired   = moment(date).format("YYYY-MM-DD HH:mm:ss");
            var tutor_id        = "";
            var subject_id      = "";
            var status          = "0";       
            var desc            = "";
            var expiredd        = GetExpiredDate(waktu_expired,user_utc);
            var expired         = GetDateHour(expiredd,'3600');
            
            $("#modalAddSession").modal("hide");                  
            schedule_dt.push({"eventDate" : eventDate, "eventTime" : eventTime, "eventDurasi" : eventDurasi, "tutor_id" : tutor_id, "subject_id" : subject_id, "desc" : desc, "status" : status, "expired" : expired});
            var concat = "";

            if (schedule_at != null) {
                concat = schedule_at.concat(schedule_dt);   
            }
            else
            {
                concat = schedule_dt;
            }
            
            $.ajax({
                url :"<?php echo AIR_API;?>addSessionFromModifyClass/access_token/"+access_token,
                type:"post",
                data: {
                    date: eventDate,                    
                    time : eventTime,                    
                    duration : eventDurasi,
                    channel_id : channel_id,
                    program_id : pid,
                    schedule : concat 
                },
                success: function(response){   
                    var code = response['code'];
                    if (code == 200) {                        
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#text_modal").html("Successfully Add Session");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                    }
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>Admin/Logout';
                    }
                }
            });                        

        });

        $(document).on('click', '.deleteDateSesi', function(){ 
            var key = $(this).data('sesi');
            var schedule_now = "";
            $.ajax({
                url: '<?php echo AIR_API;?>channel_listModifyClass/access_token/'+access_token,
                type: 'POST',
                data: {
                    channel_id: channel_id,
                    program_id: pid,
                    user_utc: '420'
                },
                success: function(response)
                {
                    var a = JSON.stringify(response);                 
                    var code = response['code'];                  
                    if (code == 200) {
                        schedule_now  = response['data'][0]['schedule'];
                        schedule_now = JSON.parse(schedule_now); 
                    }
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>Admin/Logout';
                    }

                    if (schedule_now != "") {
                        schedule_now.splice(key, 1);
                        console.log(schedule_now);
                        $.ajax({
                            url :"<?php echo AIR_API;?>deleteSessionFromModifyClass/access_token/"+access_token,
                            type:"post",
                            data: {
                                channel_id : channel_id,
                                program_id : pid,
                                schedule : schedule_now 
                            },
                            success: function(response){   
                                var code = response['code'];
                                if (code == 200) {
                                    console.warn(schedule_dt);
                                    $("#modal_alert").modal('show');
                                    $("#modal_konten").css('background-color','#32c787');
                                    $("#text_modal").html("Successfully Delete Session");
                                    $("#button_ok").click( function(){
                                        location.reload();
                                    });
                                }
                                else if (code == -400) {
                                    window.location.href='<?php echo base_url();?>Admin/Logout';
                                }
                            }
                        });
                    }
                    else
                    {
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#CF000F');
                        $("#text_modal").html("Failed");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                    }
                }
            });

            
        });

    });
</script>