<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');

	?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('inc/sideadmin'); ?>
	</aside>

	<section id="content">
		<div class="container">            
			<div class="block-header">
				<h2><?php echo $this->lang->line('verify_subject'); ?></h2>
			</div>

			<div class="card m-t-20 p-20" style="">
                <div class="row" style="margin-top: 2%; overflow-y: auto;">                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Foto</th>
                                        <th>Nama</th>
                                        <th>Email</th>                                        
                                        <th>Subject</th>
                                        <th>Jenjang</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;
                                    $allAcc = $this->db->query("SELECT tu.user_name, tu.email, tu.user_image, tb.*, ms.subject_name, ms.jenjang_id FROM tbl_user as tu INNER JOIN tbl_booking as tb ON tu.id_user=tb.id_user INNER JOIN master_subject as ms ON tb.subject_id=ms.subject_id where tb.tutor_id=0 AND (tb.status='unverified' OR tb.status='requested') ORDER BY tb.status DESC")->result_array();
                                    foreach ($allAcc as $row => $v) { 
                                        $jenjangnamelevel = array();                                        
                                        $jenjangid = json_decode($v['jenjang_id'], TRUE);
                                        if ($jenjangid != NULL) {
                                            if(is_array($jenjangid)){
                                                foreach ($jenjangid as $key => $value) {                                    
                                                    $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();   
                                                    if ($selectnew['jenjang_level'] == "0") {
                                                        $jenjangnamelevel[] = $selectnew['jenjang_name'];
                                                    }
                                                    else{
                                                        $jenjangnamelevel[] = $selectnew['jenjang_name'].' - '.$selectnew['jenjang_level'];
                                                    }
                                                    
                                                }   
                                            }else{
                                                $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();   
                                                if ($selectnew['jenjang_level'] == "0") {
                                                    $jenjangnamelevel[] = $selectnew['jenjang_name'];
                                                }
                                                else{
                                                    $jenjangnamelevel[] = $selectnew['jenjang_name'].' - '.$selectnew['jenjang_level'];
                                                }
                                            }                                       
                                        }
                                    	$user_name 		= $v['user_name'];
                                    	$subject_name 	= $v['subject_name'];
                                    	$email 			= $v['email'];
                                    	$status 		= $v['status'];
                                    	$foto  			= $v['user_image'];	
                                        ?>
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td>
		                                    	<img onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$foto; ?>" class="pv-main" style='width: 100px; height: 100px;' alt="">
                                            </td>
                                            <td><?php echo $user_name; ?></td>
                                            <td><?php echo $email; ?></td>
                                            <td><?php echo $subject_name;?></td>
                                            <td><?php echo implode(", ", $jenjangnamelevel);?></td>                                             
                                            <td>
                                            	<?php 
                                            	if($status=='unverified'){ echo "<label class='c-red f-13'>Unverified</label>";}else if($status=='verified'){ echo "<label class='c-green f-13'>Verified</label>"; } else if($status=='rejected'){ echo "<label class='c-red f-13'>Rejected</label>"; } else if($status=='requested'){ echo "<label class='c-red f-13'>Waiting your verification</label>"; }
                                            	?>
                                            </td>                                           
                                            <td>
                                            	<?php 
                                            	if($status=='requested'){?>
                                                <a class="card profile-tutor btnaction" uid="<?php echo $v['uid'];?>" action="unverified"><button class="btn btn-success" title="Verified"><i class="zmdi zmdi-check-all"></i> Verify</button></a>
                                                <?php 
                                            	}else if($status=='verified'){?>
                                            	<a class="card profile-tutor btnaction" uid="<?php echo $v['uid'];?>" action="reject"><button class="btn btn-danger" title="Rejected"><i class="zmdi zmdi-close"></i> Reject</button></a>
                                            	<?php 
                                            	}else if($status=='unverified'){?>
                                                <a class="card profile-tutor" uid="<?php echo $v['uid'];?>"><button class="btn btn-default" title="Unverified"> - </button></a>
                                                <?php 
                                                }else if($status=='rejected'){?>
                                            	<a class="card profile-tutor" href="#"><button class="btn btn-default btn-block" title="Detail tutor"><i class="zmdi zmdi-minus"></i></button></a>
                                            	<?php
                                            	}
                                            	?>
                                            </td>                                            
                                        </tr>
                                        <?php 
                                        $no++;
                                    }
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div>
            </div> 		

		</div>
	</section>

</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	$('.card.btnaction').click(function(){
		var ids 	= $(this).attr('uid');
		var action = $(this).attr('action');	
		if (action=='unverified') {			
			$.ajax({
				url: '<?php echo base_url();?>Admin/approve_vts',
				data: {
	                booking_id: ids
	            },            
				type: 'GET',
				success: function(response){					
					response = JSON.parse(response);					
					if(response['status'] == true){
						notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Success Verified");
						setTimeout(function(){
                            window.location.reload();
                        },1500);
					}
					else
					{
						notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Failed Verified");
						setTimeout(function(){
                            window.location.reload();
                        },1500);	
					}
				}
			});
		}
		else if(action=='reject')
		{
			$.ajax({
				url: '<?php echo base_url();?>Admin/reject_vts',
				data: {
	                booking_id: ids
	            },            
				type: 'GET',
				success: function(response){					
					response = JSON.parse(response);
					if(response['status']){
						notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Success Reject");
						setTimeout(function(){
                            window.location.reload();
                        },1500);
					}
					else
					{
						notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Failed Reject");
						setTimeout(function(){
                            window.location.reload();
                        },1500);	
					}
				}
			});
		}
	});

	$('table.display').DataTable({
        fixedHeader: {
            header: true,
            footer: true
        }
    });

    $('.data').DataTable(); 

</script>