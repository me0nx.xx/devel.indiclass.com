<main class="main">

    <header class="header">
       	<?php $this->load->view("inc/navbar_adm"); ?>
    </header>

    <aside class="sidebar">
        <?php $this->load->view('inc/side_adm'); ?>
    </aside>

    <section class="content">
        <header class="content__title">
            <h1>List Package</h1>
            <small>Detail list of Package on your channel!</small>

            <div class="actions">
                <a data-toggle="modal" href="#modalAddPackage"><button class="btn btn-success">+ Add Package</button></a>
            </div>        
        </header>

        <div class="row">
            <div class="card col-md-12 col-xs-12">
                <div class="card-block">
                    <div class="row table-responsive">
                        <div id="tables_listPackage"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="animated fadeIn modal fade" style="color: white;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" >
                <div class="modal-content" id="modal_konten">
                    <div class="modal-body" align="center">
                        <label id="text_modal">Halloo</label><br>
                        <button  id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal modalAddPackage -->  
        <div class="modal fade show" id="modalAddPackage">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">                        
                        <h4 class="modal-title c-white pull-left">Add Package</h4>
                        <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                    </div>
                    <div class="modal-body m-t-20">
                        <form id="frmdata">
                            <div class="row col-md-12">
                            <div class="col-md-6">
                            	<div class="form-group">
                                    <label>Package Name</label>
                                    <input type="text" id="val_packagename" style="background-color: #ECF0F1; padding: 10px;" class="form-control floating-label" placeholder="Type Here...">
                                    <i class="form-group__bar"></i>
                                </div>
							</div>  
                            <div class="col-md-6">
                            	<div class="form-group">
                                    <label>Package Credit</label>
                                    <input type="number" min="1" step="1" style="background-color: #ECF0F1; padding: 10px;" id="val_packagecredit" class="form-control floating-label" placeholder="Type Here...">
                                    <i class="form-group__bar"></i>
                                </div>
							</div>
                            <div class="col-md-6">
                                <label>Package Price Basic</label>
                                <div class="input-group">                                    
                                    <span class="input-group-addon">$</span>
                                    <div class="form-group">
                                        <input type="number" min="1" step="1" class="form-control" id="val_packageprice_basic" style="background-color: #ECF0F1; padding: 10px;" placeholder="Type Here..." data-mask="000.000.000.000.000,00">
                                        <i class="form-group__bar"></i>
                                    </div>                                    
                                </div>
							</div>  
                            <div class="col-md-6">
                                <label>Package Price Premium</label>
                                <div class="input-group">                                    
                                    <span class="input-group-addon">$</span>
                                    <div class="form-group">
                                        <input type="number" min="1" step="1" class="form-control" id="val_packageprice_premium" style="background-color: #ECF0F1; padding: 10px;" placeholder="Type Here..." data-mask="000.000.000.000.000,00">
                                        <i class="form-group__bar"></i>
                                    </div>                                    
                                </div>
                            </div>                            
                            </div>                            
                        </form>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button"  class="btn btn-success btn-block m-t-15" id="savePackage">Add</button>                            
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade show" id="modalDeletePackage" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" style="color: #000;"><b>Delete Package</b></h3>
                    </div>
                    <div class="modal-body">                        
                        <label class="c-gray f-15">Are you sure you want to delete?</label>                         
                    </div>
                    <div class="modal-footer" style="background-color: #e5e5e5;">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">No</button>
                        <button type="button" data-dismiss="modal" class="btn btn-danger waves-effect" id="proses_hapuspackage" data-id="" rtp="">Yes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade show" id="modalStatusPackage">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">                        
                        <h4 class="modal-title c-white pull-left">Update Package</h4>
                        <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                    </div>
                    <div class="modal-body m-t-20">
                        <form id="frmdata">
                        	<input type="hidden" id="txt_idpackage" name="">
                            <div class="row col-md-12">
                                <div class="col-md-6">
                                	<div class="form-group">
	                                    <label>Package Name</label>
	                                    <input type="text" id="txt_namepackage" class="form-control floating-label " style="background-color: #ECF0F1; padding: 10px;" placeholder="Type Here...">
	                                    <i class="form-group__bar"></i>
	                                </div>
								</div>  
                                <div class="col-md-6">
                                	<div class="form-group">
	                                    <label>Package Credit</label>
	                                    <input type="number"  id="txt_creditpackage" class="form-control floating-label" style="background-color: #ECF0F1; padding: 10px;" placeholder="Type Here...">
	                                    <i class="form-group__bar"></i>
	                                </div>
								</div>
                                <div class="col-md-6">
                                    <label>Package Price Basic</label>
                                    <div class="input-group">                                    
                                        <span class="input-group-addon">$</span>
                                        <div class="form-group">
                                            <input type="number" min="1" step="1" class="form-control" id="txt_pricepackage_basic" style="background-color: #ECF0F1; padding: 10px;" placeholder="Type Here..." data-mask="000.000.000.000.000,00">
                                            <i class="form-group__bar"></i>
                                        </div>                                    
                                    </div>
								</div> 
                                <div class="col-md-6">
                                    <label>Package Price Premium</label>
                                    <div class="input-group">                                    
                                        <span class="input-group-addon">$</span>
                                        <div class="form-group">
                                            <input type="number" class="form-control" id="txt_pricepackage_premium" style="background-color: #ECF0F1; padding: 10px;" placeholder="Type Here..." data-mask="000.000.000.000.000,00">
                                            <i class="form-group__bar"></i>
                                        </div>                                    
                                    </div>
                                </div>
                            </div>                               
                        </form>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button"  class="btn btn-success btn-block m-t-15" id="updatePackage">Update Package</button>                            
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>        
<script type="text/javascript">
    var access_token = "<?php echo $this->session->userdata('access_token');?>";
    var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
    var dataSet = [];

    $(document).ready(function() {
        $.ajax({
            url: '<?php echo base_url();?>Rest/list_package/access_token/'+access_token,
            type: 'POST',
            data: {
                channel_id: channel_id
            },
            success: function(response)
            {
                var a = JSON.stringify(response);  
                var code = response['code'];                
                if (code == 200) {
                    for (var i = 0; i < response.data.length; i++) {
                        var id_package = response['data'][i]['id_package'];
                        var name_package = response['data'][i]['name_package'];
                        var credit_package = response['data'][i]['credit_package'];
                        var price_package_basic = response['data'][i]['price_package_basic'];
                        var price_package_premium = response['data'][i]['price_package_premium'];

                        var action = "<button class='edit_package btn waves-effect' style='background-color: #607D8B;' data-name_package='"+name_package+"' data-credit_package='"+credit_package+"' data-id_package='"+id_package+"' data-price_package_basic='"+price_package_basic+"' data-price_package_premium='"+price_package_premium+"' data-id_package='"+id_package+"'><i class='zmdi zmdi-settings' style='color:#fff;'></i></button> <button class='action_package btn waves-effect' style='background-color: #607D8B;' data-id_package='"+id_package+"'><i class='zmdi zmdi-delete' style='color:#fff;'></i></button>";
                        dataSet.push([i+1, name_package, credit_package, price_package_basic, price_package_premium, action],);
                    }

                    $('#tables_listPackage').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="packageList"></table>' );
             
                    $('#packageList').dataTable( {
                        "data": dataSet,
                        "columns": [
                            { "title": "No"},
                            { "title": "Package Name"},
                            { "title": "Package Credit"},
                            { "title": "Package Price Basic"},
                            { "title": "Package Price Premium"},
                            { "title": "Action" }
                        ]
                    });  
                }
                else if (code == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
                else
                {
                    $('#tables_listPackage').html( '<table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="packageList"></table>' );
             
                    $('#packageList').dataTable( {                        
                        "columns": [
                           { "title": "No"},
                            { "title": "Package Name"},
                            { "title": "Package Credit"},
                            { "title": "Package Price Basic"},
                            { "title": "Package Price Premium"},
                            { "title": "Action" }
                        ]
                    }); 
                }
            }
        });             

        $(document).on('click', '.action_package', function(){
            var id_package    = $(this).data('id_package');
            $('#proses_hapuspackage').attr('rtp',id_package);
            $("#modalDeletePackage").modal("show");
        });
        $(document).on('click', '.edit_package', function(){
            $("#body_status").empty();
            var id_package    = $(this).data('id_package');
            var name_package    = $(this).data('name_package');
            var credit_package    = $(this).data('credit_package');
            var price_package_basic    = $(this).data('price_package_basic');
            var price_package_premium    = $(this).data('price_package_premium');
            $("#txt_idpackage").val(id_package);
            $("#txt_namepackage").val(name_package);
            $("#txt_creditpackage").val(credit_package);
            $("#txt_pricepackage_basic").val(price_package_basic);
            $("#txt_pricepackage_premium").val(price_package_basic);
            $("#modalStatusPackage").modal("show");
            // alert(iduser);

        });

        $('#proses_hapuspackage').click(function(e){
            var id_package = $(this).attr('rtp');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url();?>Rest/del_package/access_token/"+access_token,
                type:"post",
                data: {
                    id_package: id_package
                },
                success: function(response){   
                    var code = response['code'];
                    if (code == 200) {

                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#text_modal").html("Successfully deleted Package");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                        /*notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Successfully deleted Student data");
                        $('#modalDeleteStudent').modal('hide');
                        setTimeout(function(){
                            window.location.reload();
                        },2000);*/
                    }   
                    else if (code == -400) {
                        window.location.href='<?php echo base_url();?>Admin/Logout';
                    }
                    else{
                        $('#modalDeletePackage').modal('hide');
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#ff3333');
                        $("#text_modal").html("There is an error!!!");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                        /*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"There is an error!!!");
                        setTimeout(function(){
                            window.location.reload();
                        },2000);*/
                    }                  
                } 
            });
        });

        $('#savePackage').click(function(e){           
        	var name_package = $("#val_packagename").val();
        	var price_package_basic = $("#val_packageprice_basic").val();
            var price_package_premium = $("#val_packageprice_premium").val();
        	var credit_package = $("#val_packagecredit").val();
            if (name_package == null || name_package == "") {
                $('#modalAddPackage').modal('hide');
                $("#modal_alert").modal('show');
                $("#modal_konten").css('background-color','#ff3333');
                $("#text_modal").html("Please Fill In The Blank...");
                $("#button_ok").click( function(){
                    $('#modalAddPackage').modal('show');
                	$("#modal_alert").modal('hide');
                });
            }
            else{
                $.ajax({
                    url : '<?php echo base_url(); ?>Rest/set_package/access_token/'+access_token,
                    type:"post",
                    data: {
                        name_package : name_package,
                        price_package_basic : price_package_basic,
                        price_package_premium : price_package_premium,
                        credit_package : credit_package
                    },
                    success: function(response){          
                        var code = response['code'];
                        if (code == 200) {
                            $('#modalAddPackage').modal('hide');
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#32c787');
                            $("#text_modal").html("Successfully add Package");
                            $("#button_ok").click( function(){
                                location.reload();
                            });
                           /* notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Successfully add Student data");
                            setTimeout(function(){
                                window.location.reload();
                            },2000);*/
                        }
                        else if (code == -400) {
                            window.location.href='<?php echo base_url();?>Admin/Logout';
                        }
                        else
                        {
                            $('#modalAddPackage').modal('hide');
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#ff33333');
                            $("#text_modal").html("There is an error!!!");
                            $("#button_ok").click( function(){
                                location.reload();
                            });
                            /*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"There is an error!!!");
                            setTimeout(function(){
                                window.location.reload();
                            },2000);*/
                        }
                        
                    } 
                });
            }
        });
        $('#updatePackage').click(function(e){           
        	var name_package = $("#txt_namepackage").val();
        	var id_package = $("#txt_idpackage").val();
        	var price_package_basic = $("#txt_pricepackage_basic").val();
            var price_package_premium = $("#txt_pricepackage_premium").val();
        	var credit_package = $("#txt_creditpackage").val();
            if (name_package == null || name_package == "") {
                $('#modalStatusPackage').modal('hide');
                $("#modal_alert").modal('show');
                $("#modal_konten").css('background-color','#ff3333');
                $("#text_modal").html("Please Fill In The Blank...");
                $("#button_ok").click( function(){
                    $('#modalStatusPackage').modal('show');
                    $("#modal_alert").modal('hide');
                });
            }
            else{
                $.ajax({
                    url : '<?php echo base_url(); ?>Rest/updt_package/access_token/'+access_token,
                    type:"post",
                    data: {
                        id_package : id_package,
                        name_package : name_package,
                        price_package_basic : price_package_basic,
                        price_package_premium : price_package_premium,
                        credit_package : credit_package
                    },
                    success: function(response){          
                        var code = response['code'];
                        if (code == 200) {
                            $('#modalStatusPackage').modal('hide');
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#32c787');
                            $("#text_modal").html("Successfully Update Package");
                            $("#button_ok").click( function(){
                                location.reload();
                            });
                           /* notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Successfully add Student data");
                            setTimeout(function(){
                                window.location.reload();
                            },2000);*/
                        }
                        else if (code == -400) {
                            window.location.href='<?php echo base_url();?>Admin/Logout';
                        }
                        else
                        {
                            $('#modalAddPackage').modal('hide');
                            $("#modal_alert").modal('show');
                            $("#modal_konten").css('background-color','#ff33333');
                            $("#text_modal").html("There is an error!!!");
                            $("#button_ok").click( function(){
                                location.reload();
                            });
                            /*notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"There is an error!!!");
                            setTimeout(function(){
                                window.location.reload();
                            },2000);*/
                        }
                        
                    } 
                });
            }
        });

    });

</script>
