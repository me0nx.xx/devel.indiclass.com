<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1" style="padding: 0; margin-top: 3%;" >
    
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
    </aside>
    
    <section id="content" style="padding: 0;"> 

        <div class="container" style="margin-top: 7%;">
            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>

            <div class="tn-justified f-16 row" style="background-color: white;" id="choose_child">
                <div class="lv-header hidden-xs" style="background-color: white;">
                    <div class="c-teal judul_register m-t-10 m-b-10" style="font-weight: bold; font-family: 'Trebuchet MS'; font-style:normal;">Memilih Tambahan Mata Pelajaran anak</div>
                    <small class="text-lowercase" id="levelregister">( silahkan pilih Tambahan mata pelajaran untuk anak sesuai keinganan anda )</small>
                </div><br>

                <div class="card-body card-padding p-l-20 p-r-20">
                    <div class="contacts clearfix row">
                        <?php 
                            $parentid   = $this->session->userdata('id_user');
                            $allchild   = $this->db->query("SELECT tu.*, tpk.* FROM tbl_profile_kid as tpk INNER JOIN tbl_user as tu ON tpk.kid_id=tu.id_user WHERE tpk.parent_id='$parentid'")->result_array();
                            foreach ($allchild as $row => $child) {                 
                            ?>
                                <div class="col-md-3 kotakchild" data-id="<?php echo $child['id_user'];?>" data-name="<?php echo $child['user_name'];?>">
                                    <div class="c-item kotakchild">
                                        <a href="" class="ci-avatar kotakchild">
                                            <img src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$child['user_image'];?>" alt="">
                                        </a>        
                                        <div class="c-info kotakchild">
                                            <strong><?php echo $child['user_name'];?></strong>
                                            <small><?php echo $child['user_gender'].', '.$child['user_birthplace'].$child['user_birthdate'] ?></small><br>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="hiddenondemand" style="display: none; margin-top: -3%;">

            <div class="col-md-12" style="background-color: #8fc7a6; width: 100%; height: auto; margin-bottom: 2%;">
        
                <div class="mini-charts" style="margin-top: 4%;">
                    <div class="row">
                        <div class="col-sm-6 col-md-2">
                            
                        </div>
                    <div id="menu_web" class="">
                        <div class="col-sm-12 col-md-4 col-xs-12 m-t-10" id="privateclick">
                            <a id="privateclassclick" style="cursor: pointer;">
                                <div class="mini-charts-item bgm-orange" id="kotaknyaprivate">
                                    <div class="clearfix">
                                        <div class="chart stats-bar-2"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>class_icon/Private_revisiw.png" class="img-responsive" style="height: 65px; width: 65px; margin-left: 9px;" alt=""></div>
                                        <div class="count">
                                            <h2 class="m-l-20 m-t-10">Private Class</h2>
                                            <small class="m-l-20 m-t-5">Only you and tutor in class</small>                                            
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        
                        <div class="col-sm-12 col-md-4 col-xs-12 m-t-10">
                            <a class="groupclassclick" style="cursor: default;">
                            <div class="mini-charts-item bgm-orange" id="kotaknyagroup">
                                <div class="clearfix">
                                    <div class="chart stats-bar-2"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>class_icon/Group_revisiw.png" class="img-responsive" style="filter: grayscale(); height: 65px; width: 65px; margin-left: 9px;" alt=""></div>
                                    <div class="count">                                            
                                        <h2 class="m-l-20 m-t-10">Group Class</h2>
                                        <small class="m-l-20 m-t-5">4 students in class</small>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                        
                        <div class="col-sm-6 col-md-2">
                            
                        </div>
                        
                    </div>
                </div>            
            </div>        

            <br>
            <div class="container">            

                <div class="tn-justified f-16 row text-center m-l-5" style="background-color: white; width: 98%; margin-top: 17%;">
                    <div class="lv-header hidden-xs" style="background-color: white;">
                        <div class="c-teal judul_register m-t-10 m-b-10" style="font-weight: bold; font-family: 'Trebuchet MS'; font-style:normal;">Memilih Tambahan Mata Pelajaran anak</div>
                        <small class="text-lowercase" id="levelregister">( silahkan pilih Tambahan mata pelajaran untuk anak sesuai keinganan anda )</small>
                    </div>
                </div>
                <br>
                <div class="block-header">
                    <h2><?php  echo $this->lang->line('ondemand');?></h2>
                    <ul class="actions hidden-xs">
                        <li>
                            <ol class="breadcrumb">
                                <li><a class="f-16 c-bluegray" style="margin-top: -3%; cursor: default;" href="#">Anak yang dipilih : <label id="namadipilih"></label> . <label class="c-orange" style="cursor: pointer;" id="chooseother">Pilih lainnya</label></a></li>
                            </ol>
                        </li>
                    </ul>
                </div><!-- akhir block header -->

                <br>
                <!-- <?php if($this->session->flashdata('mes_alert')){ ?>
                <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <?php echo $this->session->flashdata('mes_message'); ?>
                </div>
                <?php } ?> -->
                <br>

                <div ng-show="loding" class="page-loader" style="background-color:black; opacity: 0.5;">
                    <div class="preloader pl-lg pls-white">
                        <svg class="pl-circular" viewBox="25 25 50 50">
                            <circle class="plc-path" cx="50" cy="50" r="20" />
                        </svg>

                        <p style="margin-left: -18px;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
                    </div>
                </div>

                <div id="box_privateclass">
                     
                    <div class="row">
                        <!-- <div class="col-md-12"> -->
                        <div class="col-md-4" style="z-index: 5;">
                            <div class="card">
                                <div class="card-header ch-alt">
                                    <h2>Cari Private Class</h2>
                                </div>

                                <div class="card-body card-padding">
                                <form>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-storage"></i></span>
                                                <select class="select2 form-control" required id="subject_id" style="width: 100%;">
                                                    <?php
                                                        $id = $this->session->userdata('child_id');
                                                        $allsub = $this->db->query("SELECT ms.*, mj.* FROM master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE mj.jenjang_name LIKE '%TK%' OR mj.jenjang_name LIKE '%SD%' ORDER BY mj.jenjang_level ASC")->result_array();
                                                        echo '<option disabled="disabled" selected="" value="0">Pilih Pelajaran</option>'; 
                                                        foreach ($allsub as $row => $v) {                                            
                                                            echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.$v['jenjang_name'].' '.$v['jenjang_level'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                                <input type="text" name="subjecta" id="subjecton" hidden="true">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="input-group form-group p-r-15">
                                                <!-- <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span> -->
                                                <!-- <div class="dtp-container fg-line m-l-5">                                                
                                                    <input id="dateon" type='text' required class="form-control" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
                                                </div> -->
                                                <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                                <div class="dtp-container fg-line m-l-5">
                                                    
                                                    <input type="text" required class=" form-control" id="dateon" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>"/>
                                                </div>
                                            </div>
                                        </div>  

                                        <div class="col-md-12">                                    
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                                <div class="dtp-container fg-line m-l-5" id="tempatwaktuu">
                                                    <input type='text' class='form-control' id='timeeeeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />
                                                </div>                                            
                                            </div>
                                        </div>
                                      
                                        <div class="col-md-12">
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-time-restore-setting"></i></span>
                                                <div class="dtp-container fg-line">                                                
                                                    <select required class="form-control" required id="duration" >
                                                        <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
                                                        <option value="900">15 Minutes</option>
                                                        <option value="1800">30 Minutes</option>
                                                        <option value="2700">45 Minutes</option>
                                                        <option value="3600">1 Hours</option>
                                                        <option value="5400">1 Hours 30 Minutes</option>
                                                        <option value="7200">2 Hours</option>
                                                        <option value="9000">2 Hours 30 Minutes</option>
                                                        <option value="10800">3 Hours</option>
                                                        <option value="12600">3 Hours 30 Minutes</option>
                                                        <option value="14400">4 Hours</option>
                                                        <option value="16200">4 Hours 30 Minutes</option>
                                                        <option value="18000">5 Hours</option>
                                                        <option value="19800">5 Hours 30 Minutes</option>
                                                        <option value="21600">6 Hours</option>
                                                        <option value="23400">6 Hours 30 Minutes</option>
                                                        <option value="25200">7 Hours</option>
                                                        <option value="27000">7 Hours 30 Minutes</option>
                                                        <option value="28800">8 Hours</option>
                                                        <option value="30600">8 Hours 30 Minutes</option>
                                                        <option value="32400">9 Hours</option>
                                                        <option value="34200">9 Hours 30 Minutes</option>
                                                        <option value="36000">10 Hours</option>
                                                        <option value="37800">10 Hours 30 Minutes</option>
                                                        <option value="39600">11 Hours</option>
                                                        <option value="41400">11 Hours 30 Minutes</option>
                                                        <option value="43200">12 Hours</option>
                                                        <option value="45000">12 Hours 30 Minutes</option>
                                                        <option value="46800">13 Hours</option>
                                                        <option value="48600">13 Hours 30 Minutes</option>
                                                        <option value="50400">14 Hours</option>
                                                        <option value="52200">14 Hours 30 Minutes</option>
                                                        <option value="54000">15 Hours</option>
                                                        <option value="55800">15 Hours 30 Minutes</option>
                                                        <option value="57600">16 Hours</option>
                                                        <option value="59400">16 Hours 30 Minutes</option>
                                                        <option value="61200">17 Hours</option>
                                                        <option value="63000">17 Hours 30 Minutes</option>
                                                        <option value="64800">18 Hours</option>
                                                        <option value="66600">18 Hours 30 Minutes</option>
                                                        <option value="68400">19 Hours</option>
                                                        <option value="70200">19 Hours 30 Minutes</option>
                                                        <option value="72000">20 Hours</option>
                                                        <option value="73800">20 Hours 30 Minutes</option>
                                                        <option value="75600">21 Hours</option>
                                                        <option value="77400">21 Hours 30 Minutes</option>
                                                        <option value="79200">22 Hours</option>
                                                        <option value="81000">22 Hours 30 Minutes</option>
                                                        <option value="82800">23 Hours</option>
                                                        <option value="84600">23 Hours 30 Minutes</option>
                                                        <option value="86400">24 Hours</option>
                                                    </select>
                                                    <input type="text" name="durationa" id="durationon" hidden="true">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button ng-click="getondemandprivate()" id="button_search" class="btn btn-primary" style="margin-left: 10%; width: 80%;"><?php echo $this->lang->line('button_search'); ?></button>
                                        </div>
                                    </div>

                                </div>
                                </form>
                            </div>
                        </div>

                        <div class="col-md-8">                        
                            <div class="card">
                                
                                <div class="card-header ch-alt" ng-show="resulte">
                                    <h2>Hasil Pencarian : {{hasil}} </h2>
                                    <div class="pull-right f-14" style="margin-top: -3%;"><label>{{subjects}}</label></div>
                                </div>
                                <div class="row">
                                <div class="card-body card-padding">                                                                
                                    <div class="alert alert-danger alert-dismissible text-center" role="alert" ng-if="firste">              
                                        <label><?php echo $this->lang->line('nodata'); ?></label>
                                    </div>
                                    <div class="col-md-12" ng-repeat=" x in datas">
                                        <div class="media-demo col-md-12 m-b-20">
                                            <div class="media">
                                                <div class="pull-left">                                                    
                                                    <img class="media-object img-responsive m-t-5" ng-src="{{x.user_image}}" style="height: 120px; width: 120px;" alt="">
                                                </div>
                                                <div class="media-body">
                                                    <div class="col-md-4" style="border-right: 1px solid #ececec;">                                                     
                                                        <p class="f-13">Nama : <br>{{x.user_name}}</p>
                                                        <p class="f-13" style="margin-top: -5%;">Waktu : <br>
                                                        <label ng-if="x.exact == '1'">{{times}}</label>                                                  
                                                        </p>                                                                                                        
                                                    </div> 
                                                    <div class="col-md-5" style="border-right: 1px solid #ececec;">
                                                        <p class="f-13">
                                                            Harga : <br>Rp. {{x.hargaakhir}}
                                                        </p> 
                                                    </div>                                               
                                                    <div class="col-md-3">                         
                                                        <button gh="{{x.gh}}" harga="{{x.harga}}" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" dates="{{dates}}" start_time="{{times}}" subject_id="{{x.subject_id}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>                                       
                                                        <button ng-if="x.exact == '0'" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" date="{{x.date}}" start_time="{{x.start_time}}" subject_id="{{subjects}}" tutor_id="{{x.tutor_id}}" class="btn-ask btn bgm-green btn-icon-text btn-block"><i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>

                                </div>
                            </div>
                            </div>

                        </div> 
                    </div>
                </div>

                <div id="box_groupclass">
                    <div class="row">
                        <!-- <div class="col-md-12"> -->
                        <div class="col-md-4" style="z-index: 5;">
                            <div class="card">
                                <div class="card-header ch-alt">
                                    <h2>Cari Group Class</h2>
                                </div>

                                <div class="card-body card-padding">
                                <form>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-storage"></i></span>
                                                <select class="select2 form-control" required id="subject_idg" style="width: 100%;">
                                                    <?php
                                                        $id = $this->session->userdata("id_user");
                                                        $allsub = $this->db->query("SELECT ms.*, mj.* FROM master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE mj.jenjang_name LIKE '%TK%' OR mj.jenjang_name LIKE '%SD%' ORDER BY mj.jenjang_level ASC")->result_array();
                                                        echo '<option disabled="disabled" selected="" value="0">Pilih Pelajaran</option>'; 
                                                        foreach ($allsub as $row => $v) {                                            
                                                            echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.$v['jenjang_name'].' '.$v['jenjang_level'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                                <input type="text" name="subjecta" id="subjectong" hidden="true">
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-12">
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                                <div class="dtp-container fg-line m-l-5">                                                
                                                    <input id="dateong" type='text' required class="form-control date-picker" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="col-md-12">
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                                <div class="dtp-container fg-line m-l-5">                                                
                                                    <input type="text" required class="form-control" id="dateong" data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>"/>
                                                </div>
                                            </div>
                                        </div> 
                                        <!-- <div class="col-md-12">
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                                <div class="dtp-container fg-line">                                                
                                                    <select class='select2 form-control' required id="timeg">
                                                        <option disabled selected><?php echo $this->lang->line('searchtime'); ?></option>
                                                        <?php                 
                                                            for ($i=0; $i < 24; $i++) { 
                                                              for($y = 0; $y<=45; $y+=15){  
                                                                echo "<option value=".sprintf('%02d',$i).":".sprintf('%02d',$y).">".sprintf('%02d',$i).":".sprintf('%02d',$y)."</option>";                                            
                                                            }
                                                        }
                                                        ?>
                                                    </select>  
                                                    <input type="text" name="timea" id="timeong" hidden="true">                              
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="col-md-12">                                    
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                                <!-- <div class="dtp-container fg-line m-l-5">
                                                    <input type="text" class="form-control" id="timeeee" placeholder="<?php echo $this->lang->line('searchtime'); ?>" />
                                                </div> -->
                                                <div class="dtp-container fg-line m-l-5" id="tempatwaktuuu">
                                                    <input type='text' class='form-control' id='timeeeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="durasigroup">
                                            <div class="input-group form-group p-r-15">
                                                <span class="input-group-addon"><i class="zmdi zmdi-time-restore-setting"></i></span>
                                                <div class="dtp-container fg-line">                                                
                                                    <select required class="form-control" id="durationg" >
                                                        <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
                                                        <option value="900">15 Minutes</option>
                                                        <option value="1800">30 Minutes</option>
                                                        <option value="2700">45 Minutes</option>
                                                        <option value="3600">1 Hours</option>
                                                        <option value="5400">1 Hours 30 Minutes</option>
                                                        <option value="7200">2 Hours</option>
                                                        <option value="9000">2 Hours 30 Minutes</option>
                                                        <option value="10800">3 Hours</option>
                                                        <option value="12600">3 Hours 30 Minutes</option>
                                                        <option value="14400">4 Hours</option>
                                                        <option value="16200">4 Hours 30 Minutes</option>
                                                        <option value="18000">5 Hours</option>
                                                        <option value="19800">5 Hours 30 Minutes</option>
                                                        <option value="21600">6 Hours</option>
                                                        <option value="23400">6 Hours 30 Minutes</option>
                                                        <option value="25200">7 Hours</option>
                                                        <option value="27000">7 Hours 30 Minutes</option>
                                                        <option value="28800">8 Hours</option>
                                                        <option value="30600">8 Hours 30 Minutes</option>
                                                        <option value="32400">9 Hours</option>
                                                        <option value="34200">9 Hours 30 Minutes</option>
                                                        <option value="36000">10 Hours</option>
                                                        <option value="37800">10 Hours 30 Minutes</option>
                                                        <option value="39600">11 Hours</option>
                                                        <option value="41400">11 Hours 30 Minutes</option>
                                                        <option value="43200">12 Hours</option>
                                                        <option value="45000">12 Hours 30 Minutes</option>
                                                        <option value="46800">13 Hours</option>
                                                        <option value="48600">13 Hours 30 Minutes</option>
                                                        <option value="50400">14 Hours</option>
                                                        <option value="52200">14 Hours 30 Minutes</option>
                                                        <option value="54000">15 Hours</option>
                                                        <option value="55800">15 Hours 30 Minutes</option>
                                                        <option value="57600">16 Hours</option>
                                                        <option value="59400">16 Hours 30 Minutes</option>
                                                        <option value="61200">17 Hours</option>
                                                        <option value="63000">17 Hours 30 Minutes</option>
                                                        <option value="64800">18 Hours</option>
                                                        <option value="66600">18 Hours 30 Minutes</option>
                                                        <option value="68400">19 Hours</option>
                                                        <option value="70200">19 Hours 30 Minutes</option>
                                                        <option value="72000">20 Hours</option>
                                                        <option value="73800">20 Hours 30 Minutes</option>
                                                        <option value="75600">21 Hours</option>
                                                        <option value="77400">21 Hours 30 Minutes</option>
                                                        <option value="79200">22 Hours</option>
                                                        <option value="81000">22 Hours 30 Minutes</option>
                                                        <option value="82800">23 Hours</option>
                                                        <option value="84600">23 Hours 30 Minutes</option>
                                                        <option value="86400">24 Hours</option>
                                                    </select>
                                                    <input type="text" name="durationa" id="durationong" hidden="true" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button ng-click="getondemandgroup()" id="button_search" class="btn btn-primary" style="margin-left: 10%; width: 80%;"><?php echo $this->lang->line('button_search'); ?></button>
                                        </div>
                                    </div>

                                </div>
                                </form>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="card">

                                <div class="card-header ch-alt" ng-show="resulte">
                                    <h2>Hasil Pencarian : {{ghasil}} </h2>
                                    <div class="pull-right f-14" style="margin-top: -3%;"><label>{{subjects}}</label></div>
                                </div>

                                <div class="card-body card-padding">
                                    <div class="row">
                                        <div class="alert alert-danger alert-dismissible text-center" role="alert" ng-if="firste">                                
                                            <label><?php echo $this->lang->line('nodata'); ?></label>
                                        </div>
                                        <div class="col-md-12" ng-repeat=" x in datasg">
                                            <div class="media-demo col-md-12 m-b-20">
                                                <div class="media">
                                                    <div class="pull-left">                                                    
                                                        <img class="media-object img-responsive m-t-5" ng-src="{{x.user_image}}" style="height: 120px; width: 120px;" alt="">
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="col-md-4" style="border-right: 1px solid #ececec;">                                                    	
                                                            <p class="f-13">Nama : <br>{{x.user_name}}</p>
                                                            <p class="f-13" style="margin-top: -5%;">Waktu : <br>
                                                            <label ng-if="x.exact == '0'">{{x.start_time}}</label>
                                                            <label >{{times}}</label>                                                        
                                                            </p>
                                                            <p class="f-13" style="margin-top: -5%;">
                                                                Harga : <br>Rp. {{x.harga}}
                                                            </p>                                                        
                                                        </div>
                                                        <div class="col-md-5" style="border-right: 1px solid #ececec;">
                                                            <p>Metode Pembayaran</p>
                                                            <div class="radio m-b-15">
                                                                <label>
                                                                    <input type="radio" id="radiobutton1" idtutor="{{x.tutor_id}}" class='{{x.tutor_id}} a' name="bebanmetod" value="bayar_sendiri">
                                                                    <i class="input-helper"></i>
                                                                    Bayar Sendiri
                                                                </label>
                                                            </div>
                                                            
                                                            <div class="radio m-b-15">
                                                                <label>
                                                                    <input type="radio" id="radiobutton2" idtutor="{{x.tutor_id}}" class='{{x.tutor_id}} b' name="bebanmetod" value="bagi_rata">
                                                                    <i class="input-helper"></i>
                                                                    Bagi Rata
                                                                </label>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-3">                         
                                                            <a class="openmodaladd {{x.tutor_id}} btn bgm-green btn-icon-text btn-block" gh="{{x.gh}}" harga="{{x.harga}}" image="{{x.user_image}}" username="{{x.user_name}}" duration="{{durations}}" avtime_id="{{x.avtime_id}}" dates="{{dates}}" start_time="{{times}}" subject_id="{{x.subject_id}}" tutor_id="{{x.tutor_id}}">
                                                                <i class="zmdi zmdi-check"></i> <?php echo $this->lang->line('ikutidemand');?>                                                                                        
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                        
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>                 

                <!-- Modal Default -->  
                <div class="modal" id="modaladdfriends" tabindex="-1" role="dialog" style="top: 13%;" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bgm-cyan">
                                <button type="button" class="close" data-dismiss="modal"><label class="c-white" style="cursor: pointer;">&times;</label></button>
                                <h4 class="modal-title c-white">Undang Teman Grup anda</h4>
                            </div>
                            <div class="modal-body m-t-20">
                                <form id="frmdata">
                                    <div class="col-md-12" style="padding: 0;">
                                        <div class="col-md-12">
                                        <input type="text" name="data_subject_id" id="data_subject_id" hidden />
                                        <input type="text" name="data_start_time" id="data_start_time" hidden/>
                                        <input type="text" name="data_avtime_id" id="data_avtime_id" hidden/>
                                        <input type="text" name="data_duration" id="data_duration" hidden/>
                                        <input type="text" name="data_metod" id="data_metod" hidden/>
                                            <label>Cari Temanmu</label><br>
                                            <!-- <input name='ms' class="col-md-8 m-t-5" style="height: 35px;" id="ms"> -->
                                            <div class="col-md-12 m-t-10">                                        	
    	                                		<select required name="tagorang[]" id="tagorang" multiple class="select2 form-control" style="width: 100%;">												
    											</select>											
    										</div>
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-12"><br>
                                            <p class="c-black">Catatan</p>                            
                                            <ol>
                                                <li>Minimal Undangan 1 Orang</li>
                                                <li>Maksimal Undangan 3 Orang</li>
                                                <li>Pastikan anda memiliki Saldo yang cukup</li>
                                                <li>Pastikan teman anda memiliki Saldo yang cukup</li>
                                                <li>Kami akan memotong Saldo anda sementara.</li>
                                                <li>Maksimal teman anda mensetujui ajakan anda 2 Jam.</li>
                                            </ol>

                                        </div>
                                    </div>
                                </form>
                            </div>
                            <hr>
                            <div class="modal-footer">
                                <button type="button" class="btn-req-group-demand btn btn-success btn-block m-t-15" demand-link-group="">Proses</button>                            
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal Small -->    
                <!-- <div class="modal" id="modalconfrim" tabindex="-1" style="top: 13%;" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header bgm-cyan">
                                <h4 class="modal-title c-white">Confrim</h4>                            
                            </div>
                            <div class="modal-body m-t-15">
                                <center>
                                <img id="image" style="border-radius: 1%; height: 50%; width: 50%;" class="m-b-5" alt=""><br>
                                <label><h3 id="namemodal"></h3></label><br>
                                <label id="timestart"></label>
                                <hr>
                                </center>
                                <label class="f-13"><?php echo $this->lang->line('checkdemand'); ?></label>
                            </div>
                            <div class="modal-footer">
                                <hr>
                                <button type="button" class="btn-req-demand btn btn-success" demand-link=""><?php echo $this->lang->line('ikutidemand');?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('cancel');?></button>
                            </div>
                        </div>
                    </div>
                </div>     -->          

                <div class="modal" style="margin-top: 4%;" id="modalconfrim" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">                          
                            <button type="button" class="close" data-dismiss="modal"><i class="zmdi zmdi-close"></i></button>
                            <!-- <center> -->
                                <h4 class="modal-title c-black f-20"><?php echo $this->lang->line('titledemandconfrim'); ?></h4>
                            <!-- </center> -->
                            <hr>
                            <div id="showalertpayment" style="display: none;">
                                <div class="alert alert-success" role="alert"><ul><li><?php echo $this->lang->line('successfullydemand');?></li></ul></div>
                                <!-- <p class="m-t-20">
                                    <label class="c-gray"><?php echo $this->lang->line('successfullydemand');?></label>
                                </p> -->
                            </div>
                            <div id="showdetailtutor">
                                <div class="col-md-12 c-gray m-10">
                                    <label><?php echo $this->lang->line('checkdemand'); ?></label>
                                </div>
                                <div class="col-md-12 m-t-10">
                                    <div class="media-demo col-md-12 m-b-20">
                                        <div class="media">
                                            <div class="pull-left">                                                    
                                                <img class="media-object img-responsive m-t-5" id="image" style="height: 120px; width: 120px;" alt="">
                                            </div>
                                            <div class="media-body">
                                                <div class="col-md-8 c-gray" style="border-right: 1px solid #ececec;">                                                     
                                                    <p class="f-13"><?php echo $this->lang->line('demandname'); ?> : <br><label id="namemodal"></label></p>
                                                    <br>
                                                    <p class="f-13" style="margin-top: -5%;"><?php echo $this->lang->line('demandtime'); ?> : <br>
                                                        <label id="timestart"></label>                                                  
                                                    </p>                                                                                                        
                                                </div> 
                                                <div class="col-md-4 c-gray">
                                                    <p class="f-13"><?php echo $this->lang->line('demanddate'); ?> : <br>
                                                        <label id="tanggalkelas"></label>                                                  
                                                    </p>
                                                    <br>
                                                    <p class="f-13" style="margin-top: -5%;">
                                                        <?php echo $this->lang->line('demandprice'); ?> : <br><label id="hargakelas"></label> 
                                                    </p> 
                                                </div>
                                            </div>
                                            <div class="c-gray m-t-5">                                            
                                                <textarea rows="4" class="form-control" name="topikprivate" id="topikprivate" style="width: 100%;" placeholder="Isi topik yang ingin Anda pelajari ..."></textarea>
                                                <label class="c-red" style="display: none;" id="alerttopik" >Harap isi topik pelajaran terlebih dahulu !</label>
                                            </div>
                                        </div>
                                    </div>                                                          
                                </div>  
                            </div>
                                                                   
                        </div> 
                        <div class="modal-footer">
                            <div id="afterchoose" style="display: none;">
                                <button type="button" class="btn-next-demand btn btn-success pull-left"><i class="zmdi zmdi-chevron-left"></i> Lanjut pilih kelas lain</button>
                                <button type="button" class="btn-view-demand btn btn-danger" demand-link=""><?php echo $this->lang->line('confrimpayment');?> <i class="zmdi zmdi-chevron-right"></i></button>  
                            </div>
                            <div id="beforechoose">
                                <button type="button" class="btn-req-demand btn btn-success btn-block" demand-link=""><?php echo $this->lang->line('confrimdemand');?></button>
                            </div>
                        </div>                                                                   
                    </div>
                </div>
            </div>

            </div><!-- akhir container -->            
        </div>
    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
<!-- <script type="text/javascript" src="<?php echo base_url();?>aset/timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>aset/timepicker/jquery.timepicker.css" /> -->
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>aset/js/magicsuggest/magicsuggest.css" /> 
    <script type="text/javascript" src="<?php echo base_url();?>aset/js/magicsuggest/magicsuggest.js"></script>    
    <script type="text/javascript">
        $(document).ready(function() {

            var date = null;
            var tgll = new Date();  
            var formattedDatee = moment(tgll).format('YYYY-MM-DD'); 
            var a = [];
            var b = [];            
            var c = [];
            var d = [];
            var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
            var child_id = null;
            $('.kotakchild.col-md-3').click(function(){
                child_id    = $(this).data('id');
                window.localStorage.setItem('child_id', child_id);
                var username    = $(this).data('name');
                $.ajax({
                    url: '<?php echo base_url();?>Master/setsessionchildid',
                    type: 'GET',
                    data: {
                        child_id: child_id
                    },
                    success: function(data)
                    { 
                        data = JSON.parse(data);                        
                        if(data['code'] == 1){
                            $("#choose_child").css('display','none');
                            $("#hiddenondemand").css('display','block');
                            $("#namadipilih").text(username);       
                        }
                    }
                });                 
            });

            $("#chooseother").click(function(){                 
                $("#hiddenondemand").css('display','none');
                $("#namadipilih").text("");
                $("#choose_child").css('display','block');
                child_id = null;
            });

            $('#dateon').on('dp.change', function(e) {
                date = moment(e.date).format('YYYY-MM-DD');                
                var hoursnoww = tgll.getHours(); 
                // alert(tgl);
                if (hoursnoww == 23) {                    
                    notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',"Minimum an hour required between request time and classroom schedule.");
                    $("#button_search").attr('disabled');
                }
                else
                {
                    if (date == formattedDatee) {
                        a = [];
                        $("#tempatwaktuu").html("");
                        $("#tempatwaktuu").append("<input type='text' class='form-control' id='timeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeee").removeAttr('disabled');
                        for (var i = hoursnoww+1; i < 24; i++) {                                                
                            a.push(i);
                        }
                        console.warn(a);
                        $('#timeee').datetimepicker({                    
                            sideBySide: true, 
                            showClose: true,                   
                            format: 'HH:mm',
                            stepping: 15,
                            enabledHours: a,
                        });
                    }
                    else
                    {
                        $("#tempatwaktuu").html("");
                        $("#tempatwaktuu").append("<input type='text' class='form-control' id='timeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeee").removeAttr('disabled');
                        b = [];
                        for (var i = 0; i < 24; i++) {                                                
                            b.push(i);
                        }
                        console.warn(b);
                        $('#timeee').datetimepicker({                    
                            sideBySide: true,                    
                            format: 'HH:mm',
                            showClose: true,
                            stepping: 15,
                            enabledHours: b,
                        });
                    }
                }
            });

            $('#dateong').on('dp.change', function(e) {
                date = moment(e.date).format('YYYY-MM-DD');                
                var hoursnoww = tgll.getHours(); 
                // alert(tgl);
                
                if (hoursnoww == 23) {
                    notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',"Minimum an hour required between request time and classroom schedule.");
                }
                else
                {
                    if (date == formattedDatee) {
                        c = [];
                        $("#tempatwaktuuu").html("");
                        $("#tempatwaktuuu").append("<input type='text' class='form-control' id='timeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeeee").removeAttr('disabled');
                        for (var i = hoursnoww+1; i < 24; i++) {                                                
                            c.push(i);
                        }
                        console.warn(c);
                        $('#timeeee').datetimepicker({                    
                            sideBySide: true, 
                            showClose: true,                   
                            format: 'HH:mm',
                            stepping: 15,
                            enabledHours: c,
                        });
                    }
                    else
                    {
                        $("#tempatwaktuuu").html("");
                        $("#tempatwaktuuu").append("<input type='text' class='form-control' id='timeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                        $("#timeeee").removeAttr('disabled');
                        d = [];
                        for (var i = 0; i < 24; i++) {                                                
                            d.push(i);
                        }
                        console.warn(d);
                        $('#timeeee').datetimepicker({                    
                            sideBySide: true,                    
                            format: 'HH:mm',
                            showClose: true,
                            stepping: 15,
                            enabledHours: d,
                        });
                    }
                }
            });

            $(function () {
                var tgl = new Date();  
                var formattedDate = moment(tgl).format('YYYY-MM-DD');         
                
                $('#dateon').datetimepicker({  
                    minDate: moment(formattedDate, 'YYYY-MM-DD')
                });

                $('#dateong').datetimepicker({  
                    minDate: moment(formattedDate, 'YYYY-MM-DD')
                });
                
            });

            $('select.select2').each(function(){
                
                if($(this).attr('id') == 'tagorang'){
                	$(this).select2({
						delay: 2000,
						tokenSeparators: [','],
						maximumSelectionLength: 3,					
						ajax: {
							dataType: 'json',
							type: 'GET',
							url: '<?php echo base_url(); ?>ajaxer/getNameuser',
							data: function (params) {
								return {
								  term: params.term,
								  page: params.page || 1
								};
							},
							processResults: function(data){
								return {
									results: data.results,
									pagination: {
										more: data.more
									}						
								};
							}					
						}
					});  
                }
				                  
                
            });

            var namaa = [];
            var ininama;
            $.ajax({
				url: '<?php echo base_url(); ?>Rest/getnameuser',
				type: 'POST',				
				success: function(data)
				{	
					var a = JSON.stringify(data['response']);
                    var b = JSON.parse(a);
                    ininama = b;
                    // console.warn(b);
                    // console.warn(b.length);
                    for (i = 0; i < b.length; i++) {
                        var maskid = "";
                        var myemailId =  b[i].email;
                        var prefix= myemailId.substring(0, myemailId .lastIndexOf("@"));
                        var postfix= myemailId.substring(myemailId .lastIndexOf("@"));

                        for(var j=0; j<prefix.length; j++){
                            if(j >= prefix.length - 4 ) {   
                                maskid = maskid + "*";////////
                            }
                            else {
                                maskid = maskid + prefix[j].toString();
                            }
                        }
                        maskid =maskid +postfix;
                        namaa.push(b[i].user_name+" ("+maskid+")");
                    }
				}
			});	

            $(document).on("click", ".openmodaladd", function () {
                var that = $(this);
                var a = $(this).attr('tutor_id');
                var b = $("."+a).attr('idtutor');
                var c = $(".openmodaladd."+a).attr('tutor_id');
                var iduser = "<?php echo $this->session->userdata('id_user');?>";                  
                var tokenjwttt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
                // var hargamapel = $(this).attr('hargamapel'); 
                var gh = $(this).attr('gh');
                var date = $(this).attr('dates');

                if ($('.'+b).is(":checked") || $('.'+b).is(":checked")) {                    
                    $.ajax({
                        url: '<?php echo base_url(); ?>Rest/isEnoughBalance/access_token/'+tokenjwttt,
                        type: 'POST',
                        data: {
                            id_user: iduser,
                            gh : gh                        
                        },               
                        success: function(data)
                        {             
                            balanceuser = data['balance'];
                            // alert(hargamapel);
                            // alert(balanceuser);

                            if (data['status'])
                            {
                                var metod = $("input[name='bebanmetod']:checked").val();
                                var user_utc = new Date().getTimezoneOffset();
                                user_utc = -1 * user_utc;                                
                                
                                if (a == c) {
                                    var subject_idg = that.attr('subject_id');
    							    var start_timeg = that.attr('start_time');
    							    var avtime_idg = that.attr('avtime_id');
    							    var durationg = that.attr('duration');
                                    var user_name = that.attr('username');
                                    var image_user = that.attr('image');
                                    var tutor_id = c;
    							    // var io = $(".openmodaladd").attr('durationgg');
    							    // alert(durationgg);
                                    
                                    $(".modal-body #data_subject_id").val( subject_idg );
                                    $(".modal-body #data_start_time").val( start_timeg );
                                    $(".modal-body #data_avtime_id").val( avtime_idg );
                                    $(".modal-body #data_duration").val( durationg );
                                    $(".modal-body #data_metod").val( metod );                                
                                    
                                    $('.btn-req-group-demand').attr('demand-link-group','<?php echo base_url(); ?>process/demand_me_grup_kids?start_time='+start_timeg+"&tutor_id="+tutor_id+"&subject_id="+subject_idg+"&duration="+durationg+"&metod="+metod+"&date="+date+"&user_utc="+user_utc+"&child_id="+child_id);
                                    $("#modaladdfriends").modal('show');
                                }
                            }
                            
                            else
                            {
                                notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Saldo anda tidak mencukupi! Mohon isi dahulu saldo anda");
                            }
                        }
                    }); 
                }
                else
                {
                    notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut',"Mohon pilih metode pembayaran terlebih dahulu!");
                }
                
            });
            $(document.body).on('click', '.btn-req-group-demand' ,function(e){ 
                 // var a = $("#tags3").val();
                 // var array;
                 var idtage = $("#tagorang").val()+ '';

                 // alert(idtag);
                 
                 // alert(link);
                //  array = idtage.split(",");

                // for (var i = 0; i < array.length; i++) {
                    
                //     console.log(i+" id "+array[i]);
                // }       
                var link = $(".btn-req-group-demand").attr('demand-link-group')+"&id_friends="+idtage;
                $.get(link,function(data){
                    location.reload();
                });
            });   

            });


    $('#subject_id').change(function(e){
        $('#subjecton').val($(this).val());
    });
    // $('#time').change(function(e){        
    //     $('#timeon').val($(this).val());
    // });

    $('#duration').change(function(e){
        $('#durationon').val($(this).val());
    });

    $('#subject_idg').change(function(e){
        $('#subjectong').val($(this).val());
    });
    // $('#timeg').change(function(e){
    //     $('#timeong').val($(this).val());
    // });
    $('#durationg').change(function(e){
        $('#durationong').val($(this).val());
    });

    var cekkotak = 0;
    $("#btntambah").click(function(){
        if (cekkotak == 0) 
        {            
            $("#kotakpilihanteman").append("<div class='col-md-8 m-b-25' style='padding: 0;' id='kotakpilihanteman2'><div class='col-md-12'><label for='nama'>Cari Temanmu</label><input type='text' class='form-control typeahead' id='nama' data-provide='typeahead' autocomplete='off'></div></div><div class='col-md-4'><button class='btn btn-info waves-effect btn-sm m-t-20' id='btntambah'><i class='zmdi zmdi-plus'></i></button><button class='btn btn-danger waves-effect btn-sm m-t-20' id='btnkurang'><i class='zmdi zmdi-minus'></i></button></div>");
            cekkotak+1;
        }        
    });

    /*$('.btn-confirm').click(function(e){
        alert('yes');
       
    });*/
    $('#button_search').click(function(){
        
    })
    $(document.body).on('click', '.btn-ask' ,function(e){
        var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc;

        var subject_id = $(this).attr('subject_id');
        var start_time = $(this).attr('start_time');
        var date = $(this).attr('dates');
        var duration = $(this).attr('duration');    
        var tutor_id = $(this).attr('tutor_id');

        var subject_idg = $(this).attr('subject_idg');
        var start_timeg = $(this).attr('start_timeg');
        var avtime_idg = $(this).attr('avtime_idg');
        var durationg = $(this).attr('durationg'); 

        var user_name = $(this).attr('username');
        var image_user = $(this).attr('image');
        var harga = $(this).attr('harga');
        var hargaakhir = $(this).attr('hargaakhir');
        var gh = $(this).attr('gh');
        var iduser = "<?php echo $this->session->userdata('id_user');?>";
        var tokenjwtt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";

        $('#namemodal').text(user_name);
        $('#timestart').text("Rp. "+harga);
        $('#image').attr("src", image_user);
        $('#hargakelas').text("Rp. "+hargaakhir);
        $('#image').attr("src", image_user);        

        if (subject_id == '' && start_time == '' && avtime_id == '' && duration == '') {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Harap isi data dengan lengkap");
        }
        else
        {
            var childid = window.localStorage.getItem('child_id');
            // $.ajax({
            //     url: '<?php echo base_url(); ?>/Rest/isEnoughBalance/access_token/'+tokenjwtt,
            //     type: 'POST',
            //     data: {
            //         id_user: iduser,
            //         gh : gh
            //     },               
            //     success: function(data)
            //     {
            //         balanceuser = data['balance'];
            //                 // alert(harga);
            //                 // alert(balanceuser);
            //         // console.log(data);
            //         if (data['status'])
            //         {
                        $('.btn-req-demand').attr('demand-link','<?php echo base_url(); ?>process/demand_me_kids?start_time='+start_time+"&tutor_id="+tutor_id+"&subject_id="+subject_id+"&duration="+duration+"&date="+date+"&user_utc="+user_utc+"&child_id="+childid+"&hr="+harga);
                        $('#modalconfrim').modal('show');
            //         } else {
            //             notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
            //         }
            //     }
            // });
        }
    });
    $(document.body).on('click', '.btn-req-demand' ,function(e){
        var topic= $("#topikprivate").val();
        var link = $(this).attr('demand-link');
        link     = link+"&topic="+topic; 
        if (topic == "") {
            $("#alerttopik").css('display','block');
        }
        else
        {               
            $('.btn-req-demand').css('disabled','true');
            $.get(link,function(response){
                var a = JSON.parse(response);               
                if (a['status'] == 1) {
                    // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"<?php echo $this->lang->line('successfullydemand');?>");                
                    $("#beforechoose").css('display','none');
                    $("#showdetailtutor").css('display','none');
                    $("#showalertpayment").css('display','block');
                    $("#afterchoose").css('display','block');
                }
                else
                {
                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"<?php echo $this->lang->line('faileddemand');?>");
                    setTimeout(function(){              
                        location.reload();
                    },4000);
                }
            });
        }
    });    

    $(document.body).on('click','.btn-next-demand', function(e){
        $('#modalconfrim').modal("hide");
    }); 

    $(document.body).on('click','.btn-view-demand', function(e){
        location.reload();
    });

    $("#box_privateclass").css('opacity','100');
    $("#box_groupclass").css('opacity','0');    
    $("#kotaknyagroup").removeClass('bgm-orange');
    $("#kotaknyagroup").addClass('bgm-gray');    
    $("#privateclassclick").click(function(){ 
        $("#kotaknyagroup").removeClass('bgm-orange');
        $("#kotaknyagroup").addClass('bgm-gray');
        $("#kotaknyaprivate").removeClass('bgm-gray');
        $("#kotaknyaprivate").addClass('bgm-orange');

        $("#box_groupclass").css('display','none');
        $("#privateclick").css('filter','grayscale();');
        $("#groupklik").css('filter','none');
        $("#box_privateclass").css('display','block');
    });
    $("#groupclassclick").click(function(){
        $("#kotaknyaprivate").removeClass('bgm-orange');
        $("#kotaknyaprivate").addClass('bgm-gray'); 
        $("#kotaknyagroup").removeClass('bgm-gray');
        $("#kotaknyagroup").addClass('bgm-orange');
        $("#box_privateclass").css('display','none');
        $("#box_groupclass").css('opacity','100');        
        $("#durasigroup").addClass('col-md-12');
        $("#box_groupclass").css('display','block');
    });


</script>