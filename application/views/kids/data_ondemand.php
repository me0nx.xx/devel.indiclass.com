<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="container">
            
            <div class="tn-justified f-16 row" style="background-color: white;" id="choose_child">
                <div class="lv-header hidden-xs" style="background-color: white;">
                    <div class="c-teal judul_register m-t-10 m-b-10" style="font-weight: bold; font-family: 'Trebuchet MS'; font-style:normal;">Memilih Data Tambahan Mata Pelajaran anak</div>
                    <small class="text-lowercase" id="levelregister">( silahkan pilih Data tambahan mata pelajaran untuk anak sesuai keinganan anda )</small>
                </div><br>

                <div class="card-body card-padding p-l-20 p-r-20">
                    <div class="contacts clearfix row">
                        <?php 
                            $parentid   = $this->session->userdata('id_user');
                            $allchild   = $this->db->query("SELECT tu.*, tpk.* FROM tbl_profile_kid as tpk INNER JOIN tbl_user as tu ON tpk.kid_id=tu.id_user WHERE tpk.parent_id='$parentid'")->result_array();
                            foreach ($allchild as $row => $child) {                 
                            ?>
                                <div class="col-md-3 kotakchild" data-id="<?php echo $child['id_user'];?>" data-name="<?php echo $child['user_name'];?>">
                                    <div class="c-item kotakchild">
                                        <a href="" class="ci-avatar kotakchild">
                                            <img src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$child['user_image'];?>" alt="">
                                        </a>        
                                        <div class="c-info kotakchild">
                                            <strong><?php echo $child['user_name'];?></strong>
                                            <small><?php echo $child['user_gender'].', '.$child['user_birthplace'].$child['user_birthdate'] ?></small><br>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                        ?>
                    </div>
                </div>
            </div>

            <div id="hiddendataondemand" style="display: none;">

                <div class="tn-justified f-16 row text-center m-l-5" style="background-color: white; width: 98%;">
                    <div class="lv-header hidden-xs" style="background-color: white;">
                        <div class="c-teal judul_register m-t-10 m-b-10" style="font-weight: bold; font-family: 'Trebuchet MS'; font-style:normal;">Memilih Data Tambahan Mata Pelajaran anak</div>
                        <small class="text-lowercase" id="levelregister">( silahkan pilih Data tambahan mata pelajaran untuk anak sesuai keinganan anda )</small>
                    </div>
                </div>
                <br>
                <div class="block-header">
                    <h2>Data <?php echo $this->lang->line('ondemand'); ?></h2>
                    <ul class="actions hidden-xs">
                        <li>
                            <ol class="breadcrumb">
                                <li><a class="f-16 c-bluegray" style="margin-top: -3%; cursor: default;" href="#">Anak yang dipilih : <label id="namadipilih"></label> . <label class="c-orange" style="cursor: pointer;" id="chooseother">Pilih lainnya</label></a></li>
                            </ol>
                        </li>
                    </ul>
                </div> <!-- akhir block header    --> 

                <div class="col-md-12">
                    <div class="card m-t-20">
                        <div class="card-header">
                            
                            <h2><?php echo $this->lang->line('transactionlist'); ?> Request anda</h2>
                            <ul class="tab-nav pull-right" role="tablist">                                                                      
                                <li role="presentation" class="pull-right">
                                    <a class="col-xs-4" href="#tab-3" id="friendinvitaion" aria-controls="tab-3" role="tab" data-toggle="tab">
                                        Friend invitation
                                    </a>
                                </li>
                                <li role="presentation" class="pull-right">
                                    <a class="col-xs-4" href="#tab-2" id="datagroupclass" aria-controls="tab-2" role="tab" data-toggle="tab">
                                        Group Class
                                    </a>
                                </li>
                                <li role="presentation" class="pull-right active" id="dataprivateclass" style="margin-right: 3%;">
                                    <a class="col-xs-4" href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab">
                                        Private Class
                                    </a>
                                </li> 
                            </ul>
                            <hr style="margin-top: 2%;">
                        </div>


                        <div class="card-body card-padding" style="margin-top: -2%;">                   
                            
                            <div class="tab-content p-20">

                                <div role="tabpanel" class="tab-pane animated fadeIn in active" id="tab-1">
                                    <div class="card-body card-padding table-responsive">
                                    <label class="f-15" style="margin-top: -3%; margin-bottom: 1%;">Detail Private Class</label>
                                    <table class="display table table-striped table-bordered data">
                                        <thead>
                                            <tr>
                                                <th class="bgm-teal c-white" style="width: 10px">No</th>
                                                <th class="bgm-teal c-white" style="width: 15px">Nama Tutor</th>
                                                <th class="bgm-teal c-white" style="width: 15px">Mata Pelajaran</th>
                                                <!-- <th class="bgm-teal c-white" style="width: 30px">Metode Pembayaran</th> -->
                                                <!-- <th class="bgm-teal c-white" style="width: 20px">Harga</th> -->
                                                <th class="bgm-teal c-white" style="width: 20px">Jam Permintaan</th>
                                                <th class="bgm-teal c-white" style="width: 20px">Durasi</th>                                        
                                                <th class="bgm-teal c-white" style="width: 20px">Tanggal Permintaan</th>
                                                <th class="bgm-teal c-white" style="width: 20px">Status</th>                                        
                                            </tr>
                                        </thead>
                                        <tbody id="tempatdataprivateclass">                                            
                                        </tbody>
                                    </table>
                                  </div>  
                                </div>
                                
                                <div role="tabpanel" class="tab-pane animated fadeIn in" id="tab-2">
                                    <div class="card-body card-padding table-responsive">
                                    <label class="f-15" style="margin-top: -3%; margin-bottom: 1%;">Detail Group Class</label>
                                    <table class="displayy table table-striped table-bordered dataa">
                                        <thead>
                                            <tr>
                                                <th class="bgm-teal c-white" style="width: 10px">No</th>
                                                <th class="bgm-teal c-white" style="width: 15px">Nama Tutor</th>
                                                <th class="bgm-teal c-white" style="width: 15px">Mata Pelajaran</th>
                                                <th class="bgm-teal c-white" style="width: 30px">Metode Pembayaran</th>
                                                <th class="bgm-teal c-white" style="width: 20px">Harga</th>
                                                <th class="bgm-teal c-white" style="width: 20px">Jam Permintaan</th>
                                                <th class="bgm-teal c-white" style="width: 20px">Durasi</th>
                                                <th class="bgm-teal c-white" style="width: 20px">Teman Group</th>
                                                <th class="bgm-teal c-white" style="width: 20px">Tanggal Permintaan</th>
                                                <th class="bgm-teal c-white" style="width: 20px">Status</th>
                                                <!-- <th class="bgm-teal c-white" style="width: 20px">#</th> -->
                                            </tr>
                                        </thead>
                                        <tbody id="tempatdatagroupclass">
                                                                      
                                        </tbody>
                                    </table>
                                  </div>  
                                </div>

                                <div role="tabpanel" class="tab-pane animated fadeIn in" id="tab-3">
                                    <div class="card-body card-padding table-responsive">
                                    <label class="f-15" style="margin-top: -3%; margin-bottom: 1%;">Detail Friend Invite</label>
                                    <table class="displayyy table table-striped table-bordered dataaa">
                                        <thead>
                                            <tr>
                                                <th class="bgm-teal c-white" style="width: 10px">No</th>
                                                <th class="bgm-teal c-white" style="width: 20px">Teman Pengajak</th>
                                                <th class="bgm-teal c-white" style="width: 15px">Nama Tutor</th>
                                                <th class="bgm-teal c-white" style="width: 15px">Mata Pelajaran</th>
                                                <th class="bgm-teal c-white" style="width: 30px">Metode Pembayaran</th>
                                                <th class="bgm-teal c-white" style="width: 20px">Harga</th>
                                                <th class="bgm-teal c-white" style="width: 20px">Jam Permintaan</th>
                                                <th class="bgm-teal c-white" style="width: 20px">Durasi</th>                                            
                                                <th class="bgm-teal c-white" style="width: 20px">Tanggal Permintaan</th>
                                                <th class="bgm-teal c-white" style="width: 300px">Status</th>
                                                <th class="bgm-teal c-white text-center" style="width: 20px">#</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tempatfriendinvitation">
                                                                    
                                        </tbody>
                                    </table>
                                  </div>  
                                </div>                        

                            </div>
                        </div>   
                    </div>            
                </div>            
            </div>

    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<!-- <div class="page-loader bgm-blue">
    <div class="preloader pls-white pl-xxl">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p class="c-white f-14">Please wait...</p>
    </div>
</div> -->



<script type="text/javascript">    
    $(document).ready(function() {

        var methodpembayaran = null;
        var hargakelas = null;
        var datetime = null;
        var iduser = "<?php echo $this->session->userdata('id_user'); ?>";
        var balanceuser = null;
        var requestid = null;
        var user = "<?php echo $this->session->userdata('id_user'); ?>";        
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        var child_id = null;

        $('.kotakchild.col-md-3').click(function(){
            var childd_id    = $(this).data('id');
            window.localStorage.setItem('child_id', childd_id);
            var username    = $(this).data('name');
            $.ajax({
                url: '<?php echo base_url();?>Master/setsessionchildid',
                type: 'GET',
                data: {
                    child_id: childd_id
                },
                success: function(data)
                { 
                    data = JSON.parse(data);
                    if(data['code'] == 1){
                        child_id = data['child_id'];
                        $("#choose_child").css('display','none');
                        $("#hiddendataondemand").css('display','block');
                        $("#namadipilih").text(username);    
                        dataprivateclass();   
                    }
                }
            });                 
        });
        $("#chooseother").click(function(){            
            $.ajax({
                url: '<?php echo base_url();?>Master/unsetsessionchildid',
                type: 'GET',
                // data: {
                //     child_id: childd_id
                // },
                success: function(data)
                { 
                    data = JSON.parse(data);
                    if(data['code'] == 1){
                        child_id = null;
                        // $("#hiddendataondemand").css('display','none');
                        // $("#namadipilih").text("");
                        // $("#choose_child").css('display','block');   
                        location.reload();         
                    }
                }
            });                   
        });

        $("#dataprivateclass").click(function(){
            dataprivateclass();
        });

        function dataprivateclass(){
            $("#tempatdataprivateclass").empty();
            $.ajax({
                url: 'https://classmiles.com/Rest/get_dataprivateclass/access_token/'+tokenjwt,
                type: 'POST',   
                data: {
                    id_user: child_id
                },             
                success: function(data)
                {                  
                    // hargakelas = data['data']['harga_kelas'];
                    if (data['code'] == 300) {
                        var kosong = "<tr><td colspan='11'><center>Tidak ada Data</center></td></tr>";
                        $("#tempatdataprivateclass").append(kosong);
                    }
                    else
                    {             
                        var no = 0;       
                        for (var i = data.response.length-1; i >=0;i--) {
                            var request_id = data['response'][i]['request_id'];                            
                            var tutor_name = data['response'][i]['user_name_tutor'];
                            var subject_name = data['response'][i]['subject_name'];
                            var time_requested = data['response'][i]['time_requested'];
                            var duration_requested = data['response'][i]['duration_requested'];
                            var temangroup = null;
                            var date_requested = data['response'][i]['date_requested'];
                            var status = data['response'][i]['approve'];
                            var buttonnih = null;
                            // var cekstatusdia = data['response'][i]['id_friends']['id_friends'];
                            // var myStatus = null;

                            no = no+1; 
                            if (status == 0)
                            {
                                status = "Belum disetujui"; 
                                var kotak = "<tr><td>"+no+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+time_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td>"+status+"</td></tr>";
                            }
                            else
                            {
                                status = "Telah disetujui";
                                var kotak = "<tr><td>"+no+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+time_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td>"+status+"</td></tr>";
                            } 
                                                        
                            $("#tempatdataprivateclass").append(kotak);
                            $('.data').DataTable();
                        }
                    }
                }
            });
        }

        $("#datagroupclass").click(function(){
            $("#tempatdatagroupclass").empty();
            $.ajax({
                url: 'https://classmiles.com/Rest/get_datagroupclass/access_token/'+tokenjwt,
                type: 'POST',   
                data: {
                    id_user: child_id
                },             
                success: function(data)
                {                  
                    // hargakelas = data['data']['harga_kelas'];
                    if (data['code'] == 300) {
                        var kosong = "<tr><td colspan='11'><center>Tidak ada Data</center></td></tr>";
                        $("#tempatdatagroupclass").append(kosong);
                    }
                    else
                    {             
                        var no = 0;       
                        for (var i = data.response.length-1; i >=0;i--) {
                            var request_id = data['response'][i]['request_id'];                            
                            var tutor_name = data['response'][i]['user_name'];
                            var subject_name = data['response'][i]['subject_name'];
                            var method_pembayaran = data['response'][i]['method_pembayaran'];
                            var harga_kelas = data['response'][i]['harga_kelas'];
                            var time_requested = data['response'][i]['time_requested'];
                            var duration_requested = data['response'][i]['duration_requested'];
                            var temangroup = null;
                            var date_requested = data['response'][i]['date_requested'];
                            var status = data['response'][i]['approve'];
                            var buttonnih = null;
                            // var cekstatusdia = data['response'][i]['id_friends']['id_friends'];
                            // var myStatus = null;
                            methodpembayaran = method_pembayaran;
                            if (method_pembayaran=="bayar_sendiri") {
                                harga_kelas = "Rp. 0";
                                method_pembayaran = "Bayar Sendiri";
                            }
                            else
                            {
                                method_pembayaran = "Bagi Rata";
                            }

                            no = no+1; 
                            if (status == 0)
                            {
                                status = "Menunggu disetujui oleh tutor"; 
                                var kotak = "<tr><td>"+no+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+harga_kelas+"</td><td>"+time_requested+"</td><td>"+duration_requested+"</td><td></td><td>"+date_requested+"</td><td>"+status+"</td></tr>";
                            }
                            else
                            {
                                status = "Telah disetujui";
                                var kotak = "<tr><td>"+no+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+harga_kelas+"</td><td>"+time_requested+"</td><td>"+duration_requested+"</td><td></td><td>"+date_requested+"</td><td>"+status+"</td></tr>";
                            } 
                            // for (var z = cekstatusdia.length - 1; z >= 0; z--) {
                            //     if (cekstatusdia[z]['id_user'] == iduser) 
                            //     {
                            //         myStatus = cekstatusdia[z]['status'];
                            //     }
                            // }
                            // alert(JSON.stringify(cekstatusdia));
                                                        
                            $("#tempatdatagroupclass").append(kotak);
                            $('.dataa').DataTable();
                        }
                    }
                    // methodpembayaran = data['response']['method_pembayaran'];
                    // if (methodpembayaran == "beban_sendiri") 
                    // {
                    //     successandsave();
                    // }else{
                    //     ceksaldo();
                    // }
                }
            });
        });

        $("#friendinvitaion").click(function(){
            $("#tempatfriendinvitation").empty();
            $.ajax({
                url: 'https://classmiles.com/Rest/get_friendinvitation/access_token/'+tokenjwt,
                type: 'POST',   
                data: {
                    id_user: child_id
                },             
                success: function(data)
                {                   
                    var a = JSON.stringify(data['response']);
                    var b = JSON.parse(a);
                    // hargakelas = data['data']['harga_kelas'];
                    if (data['code'] == 300) {
                        var kosong = "<tr><td colspan='11'><center>Tidak ada Data</center></td></tr>";
                        $("#tempatfriendinvitation").append(kosong);
                    }
                    else
                    {            
                        var no = 0;        
                        for (var i = data.response.length-1; i >=0;i--) {
                            var request_id = data['response'][i]['request_id'];
                            var requester_name = data['response'][i]['requester_name'];
                            var tutor_name = data['response'][i]['tutor_name'];
                            var subject_name = data['response'][i]['subject_name'];
                            var method_pembayaran = data['response'][i]['method_pembayaran'];
                            var harga_kelas = data['response'][i]['harga_kelas'];
                            var duration_requested = data['response'][i]['duration_requested'];
                            var date_requested = data['response'][i]['date_requested'];
                            var approve = data['response'][i]['approve'];
                            var buttonnih = null;
                            var cekstatusdia = data['response'][i]['id_friends']['id_friends'];
                            var myStatus = null;
                            methodpembayaran = method_pembayaran;
                            if (method_pembayaran=="bayar_sendiri") {
                                harga_kelas = "Rp. 0";
                                method_pembayaran = "Bayar Sendiri";
                            }
                            else
                            {
                                method_pembayaran = "Bagi Rata";
                            }
                            for (var z = cekstatusdia.length - 1; z >= 0; z--) {
                                if (cekstatusdia[z]['id_user'] == iduser) 
                                {
                                    myStatus = cekstatusdia[z]['status'];
                                }
                            }
                            // alert(JSON.stringify(cekstatusdia));
                            no = no+1; 
                            if (myStatus == 0) {
                                buttonnih = "<button class='aa btn btn-success btn-block' id='kaka' request_id='"+request_id+"' gh='"+harga_kelas+"' title='Terima'>Terima</button><br><a data-toggle='modal' class='m-t-5' request_id='"+request_id+"' href='#Delete'><button class='aa-t btn btn-danger btn-block' request_id='"+request_id+"' title='Hapus'>Tolak</button></a>";
                                var kotak = "<tr><td>"+no+"</td><td>"+requester_name+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+harga_kelas+"</td><td>"+date_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td>Menunggu Persetujuan Teman</td><td>"+buttonnih+"</td></tr>";
                            }   
                            else{                    
                                if (approve == 1) {
                                    buttonnih = "<button class='btn btn-gray f-16 btn-block' disabled>-</button>";
                                    var kotak = "<tr><td>"+no+"</td><td>"+requester_name+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+harga_kelas+"</td><td>"+date_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td>Permintaan Kelas sudah di setujui oleh tutor. Jadwal kelas sudah di <a href='https://classmiles.com'>My Class</a></td><td>"+buttonnih+"</td></tr>";
                                }
                                else
                                {
                                    // buttonnih = "<button class='aa btn btn-success btn-block' id='kaka' request_id='"+request_id+"' gh='"+harga_kelas+"' title='Terima'>Terima</button><br><a data-toggle='modal' class='m-t-5' request_id='"+request_id+"' href='#Delete'><button class='aa-t btn btn-danger btn-block' request_id='"+request_id+"' title='Hapus'>Tolak</button></a>";
                                    buttonnih = "<button class='btn btn-gray f-16 btn-block' disabled>-</button>";
                                    var kotak = "<tr><td>"+no+"</td><td>"+requester_name+"</td><td>"+tutor_name+"</td><td>"+subject_name+"</td><td>"+method_pembayaran+"</td><td>"+harga_kelas+"</td><td>"+date_requested+"</td><td>"+duration_requested+"</td><td>"+date_requested+"</td><td>Permintaan Sudah diterima oleh tutor, tunggu hingga tutor kami Menerima.</td><td>"+buttonnih+"</td></tr>";
                                }
                            }
                            
                            $("#tempatfriendinvitation").append(kotak);
                            $('.dataaa').DataTable();
                        }
                    }
                    // methodpembayaran = data['response']['method_pembayaran'];
                    // if (methodpembayaran == "beban_sendiri") 
                    // {
                    //     successandsave();
                    // }else{
                    //     ceksaldo();
                    // }
                }
            });
        });

        $(document).on("click", ".aa", function () {    
            var childid = window.localStorage.getItem('child_id');   
            requestid = $(this).attr("request_id");
            var hargakelas = parseInt($(this).attr("gh"));  
            // alert(hargakelas);
            if (methodpembayaran == "bagi_rata") 
            {                    
                $.ajax({
                    url: '<?php echo base_url(); ?>Rest/ceksaldo/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        id_user: iduser
                    },               
                    success: function(data)
                    {             
                        balanceuser = data['balance'];
                        
                        if (balanceuser >= hargakelas)
                        {
                            // alert(balanceuser);
                            $.ajax({
                                url: 'https://classmiles.com/Rest/approve_ondemandgroup',
                                type: 'POST',
                                data: {
                                    request_id: requestid,
                                    id_user : childid
                                },               
                                success: function(data)
                                {                    
                                    if (data['status'] == true) 
                                    {
                                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Anda telah mensetujui permintaan group class dari teman anda.");
                                        setTimeout(function(){
                                            window.location.reload();
                                        },3000);
                                        
                                    }
                                }
                            });
                        }
                        else
                        {
                            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Saldo anda tidak mencukupi. Mohon untuk mengisi saldo terlebih dahulu.");
                            setTimeout(function(){
                                window.location.replace('<?php echo base_url(); ?>/Topup');
                            },3000);
                        }
                    }
                });
            }
            else
            {
                $.ajax({
                    url: 'https://classmiles.com/Rest/approve_ondemandgroup/access_token/'+tokenjwt,
                    type: 'POST',
                    data: {
                        request_id: requestid,
                        id_user : child_id
                    },               
                    success: function(data)
                    {                    
                        if (data['status'] == true) 
                        {
                            notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Anda telah mensetujui permintaan group class dari teman anda.");
                            setTimeout(function(){
                                window.location.reload();
                            },3000);
                            
                        }
                    }
                });
            } 
        });

        $(document).on("click", ".aa-t", function () {       
            // alert($(this).attr("request_id"));
            var childid = window.localStorage.getItem('child_id'); 
            requestid = $(this).attr("request_id");            
            $.ajax({
                url: 'https://classmiles.com/Rest/approve_ondemandgroup/t/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    request_id: requestid,
                    id_user : childid

                },               
                success: function(data)
                {                  

                    if (data['status'] == true) 
                    {
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Anda telah mensetujui permintaan group class dari teman anda.");
                        setTimeout(function(){
                            window.location.reload();
                        },3000);
                    }
                }
            });

        });

        function successandsave()
        {
            var childid = window.localStorage.getItem('child_id'); 
            $.ajax({
                url: 'https://classmiles.com/Rest/approve_ondemandgroup/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    request_id: requestid,
                    id_user : childid

                },               
                success: function(data)
                {                    
                    if (data['status'] == true) 
                    {
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Anda telah mensetujui permintaan group class dari teman anda.");
                        window.location.reload();
                    }
                }
            });
        }

        $('table.display').DataTable( {
            fixedHeader: {
                header: true,
                footer: true
            }
        } );
        $('table.displayy').DataTable( {
            fixedHeader: {
                header: true,
                footer: true
            }
        } );  
              
    });
</script>
    