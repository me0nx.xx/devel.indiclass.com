<style type="text/css">
    .bp-header:hover
    {
        cursor: pointer;
    }
    .ci-avatar img {
        opacity: 1;     
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .ci-avatar:hover img {
        opacity: 0.6;
    }
    #modalDefault{
    	 max-height: 100vh;
    }
</style>
<!-- <header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #db1f00;"> -->
    <?php $this->load->view('inc/navbar'); ?>
<!-- </header> -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow m-t-5">
        <?php $this->load->view('inc/side'); 
 		$id_user = $this->session->userdata('id_user_kids');
        if ($id_user == null || $id_user=="") {
            $id_user  = $this->session->userdata('id_user');
        }
            // $id_user = $this->session->userdata('id_user');
        ?>
    </aside>
    <section id="content">

        <div class="container">

            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>
            <div class="pull-right" style="width: 300px; display: none;">
            	<div class="col-sm-12" style="padding: 0;">
                    <div class="form-group">
                        <div class="fg-line">
                            <input onkeyup="cariPelajaran();" style="padding: 5px;" id="cari_pelajaran" type="text" class="form-control" placeholder="Type subject here...">
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel">
                <ul class="tab-nav" role="tablist" data-tab-color="teal">
                    <li class="active"><a href="#list_pelajaran" aria-controls="list_pelajaran" role="tab" data-toggle="tab"><?php echo $this->lang->line('title_pg_subject'); ?></a></li>
                    <li><a href="#list_tutorr" aria-controls="list_tutor" role="tab" data-toggle="tab">Tutor</a></li>
                </ul>
              
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="list_pelajaran">
                    	<?php
				            if(empty($allsub))
				            {
				                echo '<div class="alert alert-info">'.$this->lang->line('no_subjects').'</div>';
				            }
				            else
				            {
				                foreach ($allsub as $row => $v) {
				                    $alltutor = $this->db->query("SELECT ts.usertype_id, ts.user_name, ts.first_name, ts.user_image, tb.* FROM tbl_booking as tb INNER JOIN tbl_user as ts ON tb.id_user=ts.id_user INNER JOIN tbl_profile_tutor as tpt ON ts.id_user=tpt.tutor_id WHERE ts.usertype_id='tutor' AND tb.subject_id='$v[subject_id]'")->result_array();
				                    if (empty($alltutor)) {
				                    	?>
				                    	<!-- <div class="card">
					                        <div class="lv-header-alt clearfix m-b-5">
					                            <h2 class="lvh-label f-16">
					                            <img class="m-r-5" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL.'class_icon/'. $v['icon'];?>" style="height: 25px; width: 25px;">
					                        	<?php echo $v['subject_name'];?></h2>                   
					                            <div class="lvh-search">
					                                <input type="text" placeholder="Search typing..." class="lvhs-input">
					                                
					                                <i class="lvh-search-close">&times;</i>
					                            </div>
					                        </div>                        
					                        
					                        <div class="card-body card-padding">
					                        	<center>
				                    				<div class=""><?php echo $this->lang->line('no_tutor_here').$v['subject_name']; ?></div>
				                    			</center>
				                    		</div>
				                    	</div> -->
				                    	<?php
				                    }
				                    else
				                    {
				                    	?>
					                    <div class="card" id="kotak_pelajaran">
					                    	<a style="display: none;" id="label_subject"><?php echo  $v['subject_name'] ;?></a>
					                        <div class="lv-header-alt clearfix m-b-5">

					                            <h2 class="lvh-label f-16 ">
					                            <img class="m-r-5" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL.'class_icon/'. $v['icon'];?>" style="height: 25px; width: 25px;">
					                            <?php 
					                            $bahasa =  $this->session->userdata('lang');
					                            if ($bahasa=="english") {echo $v['english'];}else{echo $v['subject_name'];}
					                            // echo $v['subject_name'];?></h2>
					                            
					                        </div>                        
					                        
					                        <div class="card-body card-padding">
					                            
					                            <div class="contacts clearfix row">
					                            <?php
					                                foreach ($alltutor as $row_tutor => $va) 
					                                {
					                                    $tutor_id = $va['id_user'];	                                    
					                                ?>
					                                <div class="col-md-2 col-sm-4 col-xs-6">
					                                    <?php
					                                        if ($this->session->userdata('status')==0) 
					                                        {
					                                            ?>
					                                            <div class="c-item">
					                                            	<?php 
					                                                	if ($va['status'] == "verified") {?>
					                                            		<div class="ribbon"><span>Verified</span></div>
					                                            	<?php
					                                                }?>
					                                                <a href="#" class="ci-avatar showModal" id="card_<?php echo $tutor_id; ?>">	                                                
					                                                    <img onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$va['user_image'];?>" style="height: 20vh; width: 100%;" alt="">	
					                                                </a>
					                        
					                                                <div class="c-info">
					                                                    <strong><?php echo($va['first_name']); ?></strong>
					                                                    <!-- <small>cathy.shelton31@example.com</small> -->
					                                                </div>
					                        
					                                                <!-- <div class="c-footer">
					                                                    <button class="waves-effect"><i class="zmdi zmdi-person-add"></i> Add</button>
					                                                </div> -->
					                                            </div>
					                                        <?php
					                                        }
					                                        else
					                                        {
					                                        ?>	               	                                        	                        
					                                            <div class="c-item">
					                                            	<?php 
					                                                	if ($va['status'] == "verified") {?>
					                                            		<div class="ribbon"><span>Verified</span></div>
					                                            	<?php
					                                                }?>
					                                                <a href="#" class="ci-avatar showModal" id="card_<?php echo $tutor_id; ?>">                                                	
					                                                    <img onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>user/empty.jpg';" src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$va['user_image']; ?>" style="height: 22vh; width: 100%;" alt="">		
					                                                </a>
					                        
					                                                <div class="c-info">
					                                                    <strong><?php echo($va['user_name']); ?></strong>	                                                    
					                                                </div>
					                        
					                                                <?php
					                                                    $mybooking = $this->db->query("SELECT * FROM tbl_booking WHERE id_user='$id_user'")->result_array();
					                                                    $flag = 0;
					                                                    
					                                                    foreach ($mybooking as $rowbook => $valbook) {
					                                                        if ($tutor_id==$valbook['tutor_id'] && $va['subject_id']==$valbook['subject_id']) {
					                                                            $flag = 1;
					                                                        }
					                                                    }

					                                                    if ($flag==1) {
					                                                    ?>
					                                                        <div class="c-footer">
					                                                            <!-- <a id="buttonunfollow" class=" waves-effect bgm-red c-white btn btn-danger btn-block unfollow_ajax f-12" id="'.$v['subject_id'].'" href="<?php echo base_url('/master/unsavebooking?id_user='.$id_user.'&subject_id='.$va['subject_id'].'&tutor_id='.$va['id_user'].'&subject_name='.$v['subject_name']);?>">
					                                                                <?php echo $this->lang->line('unfollow'); ?>
					                                                            </a> -->
					                                                            <button id="buttonunfollow" subject_id='<?php echo $va['subject_id'];?>' tutorid='<?php echo $va['id_user'];?>' subject_name='<?php echo $v['subject_name'];?>' class="waves-effect bgm-red c-white btn btn-danger btn-block unfollow_ajax f-12"><?php echo $this->lang->line('unfollow'); ?></button>
					                                                        </div>
					                                                    <?php
					                                                    }
					                                                    else
					                                                    {
					                                                    ?>
					                                                        <div class="c-footer">
					                                                            <!-- <a id="buttonfollow" class="waves-effect bgm-green c-white btn btn-success btn-block follow_ajax" href="<?php echo base_url('/master/savebooking?id_user='.$id_user.'&subject_id='.$va['subject_id'].'&tutor_id='.$va['id_user'].'&subject_name='.$v['subject_name']); ?>">
					                                                            <?php echo $this->lang->line('follow'); ?>
					                                                            </a> -->
					                                                            <button id="buttonfollow" subject_id='<?php echo $va['subject_id'];?>' tutorid='<?php echo $va['id_user'];?>' subject_namee='<?php echo $v['subject_name'];?>' class="waves-effect bgm-green c-white btn btn-success btn-block follow_ajax f-12"><?php echo $this->lang->line('follow'); ?></button>
					                                                        </div>
					                                                    <?php
					                                                    }
					                                                ?>
					                                            </div>
					                                        <?php
					                                        }
					                                        ?>
					                                </div>
					                                <?php 
					                                }
					                            ?>                                
					                            </div>
					                
					                            <!-- <div class="load-more">
					                                <a href=""><i class="zmdi zmdi-refresh-alt"></i> Load More...</a>
					                            </div> -->
					                        </div>
					                    </div>
					                    <?php
				                    }
				                }
				            }
			            ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="list_tutorr">
                        <div class="row">
                        	<div id="list_tutor"></div>
                        </div>
                    </div>
                </div>
            </div>

            
                        
        </div>  
        <script type="text/javascript">
        	function cariPelajaran() {
			    var input, filter, ul, li, a, i;
			    input = document.getElementById("cari_pelajaran");
			    filter = input.value.toUpperCase();
			    ul = document.getElementById("kotak_pelajaran");
			    li = ul.getElementsByTagName("div");
			    for (i = 0; i < li.length; i++) {
			        a = li[i].getElementsByTagName("a")[0];
			        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
			            li[i].style.display = "";
			        } else {
			            li[i].style.display = "none";

			        }
			    }
			}
        </script>

        <!-- Modal Default -->  
        <div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style=" background: #f7f7f7;">
                    <div class="modal-header bgm-teal">                                                     
                        <div class="pull-left">
                            <h3 class="modal-title c-white"><?php echo $this->lang->line('profil'); ?></h3>
                            
                        </div>
                        <div class="pull-right">                                        
                            <button type="button" class="btn bgm-white" data-dismiss="modal">X</button>
                        </div>
                    </div>

                    <div class="modal-body" style="margin-top:30px;">
                        <div class="col-sm-12">

                            <div class="col-sm-4">
                                <img class="z-depth-3-bottom" id="modal_image" width="100%" height="100%" alt=""><br><br>
                                <div class="card" style="padding: 10px;">
                                    <label style="font-size:15px;">Profil Tutor</label>
                                    <hr style="margin-top: -1px;">
                                    <label><i class="zmdi zmdi-account"></i>  :  <label style="" id="modal_user_name"></label></label><br>
                                    <label><i class="zmdi zmdi-male-female"></i>  :  <label style="" id="modal_gender"></label></label>                                                                                       
                                </div>
                                <div class="card" style="padding: 10px; margin-top: -15px; display: none;">
                                    <label style="font-size:15px;"><?php echo $this->lang->line('gender');?></label>
                                    <hr style="margin-top: -1px;">
                                    <label style="font-size:13px; margin-top: -5px;"  id="modal_gender"></label>
                                </div>
                                <div class="card" style="padding: 10px; margin-top: -15px;">
                                    <label style="font-size:15px;">Pendidikan Terakhir</label>
                                    <hr style="margin-top: -1px;">
                                    <label style="font-size:13px; margin-top: -5px;"  id="modal_last_edu"></label>
                                </div>
                            </div>

                            <div class="col-sm-8" style="margin-top:0px;">                                              
                                <div class="col-sm-12">
                                    <div class="card" style="padding: 10px; margin-top: 10px;">
                                        <label style="font-size:16px;"><?php echo $this->lang->line('description_tutor');?></label>
                                        <hr style="margin-top: -1px;">
                                        <label id="modal_self_desc" style="width:100%; text-align:justify;">
                                        </label><br><br>
                                    </div>
                                </div>
                                <div class="col-lg-12 m-l-25">
                                    <div class="col-sm-1" style="margin-left:-20px;">
                                        <img src="<?php echo base_url('aset/img/icons/topi.png').'?'.time() ?>" width="30px" height="30px" alt="">
                                    </div>
                                    <div style="display: none;" id="modal_eduback" class="col-sm-5 m-t-5">                                                 
                                    </div>
                                    <!-- <div class="col-sm-1" style="margin-left:-30px;">
                                        <img src="<?php echo base_url('aset/img/icons/waktu.png').'?'.time() ?>" width="27px" height="27px" alt="">
                                    </div>
                                    <div id="modal_teachexp" class="col-sm-6 m-t-5">
                                    </div> -->
                                </div>                                                                                  

                                <div class="col-lg-12 m-t-10">
                                    <div class="col-sm-12 m-t-5">
                                        <div class="table-responsive">      
                                        <center>
                                            <table style="height:10px;" class="table">
                                                <thead>
                                                    <center>    
                                                        <th class="bgm-teal c-white">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<?php echo $this->lang->line('competency');?></th>
                                                    <th class="bgm-teal c-white"></th>                                                              
                                                    </center>
                                                </thead>
                                                <tbody id="modal_competency" style=" height: 10px;">                                                 
                                                </tbody>
                                            </table>
                                        </center>
                                        </div>
                                    </div>
                                </div>

                            </div><!-- AKHIR col-sm-8 -->

                        </div><!-- AKHIR col-sm-12 -->
                    </div><!-- AKHIR modal-body -->

                    <div class="modal-footer m-r-30" >
                    </div>                  
                    <br><br><br>

                </div><!-- AKHIR modal-content -->
            </div><!-- AKHIR modal-dialog -->
        </div>
        <!-- AKHIR modal fade -->

    </section>
</section>
                
<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>                

<script type="text/javascript">
    
    $(document).on('click', ".unfollow_ajax", function(e){
		e.preventDefault();
    // $(".unfollow_ajax").click(function(){    	
    	var id_user = "<?php echo $this->session->userdata('id_user_kids'); ?>";

        if (id_user == null || id_user=="") {
            id_user  = "<?php echo $this->session->userdata('id_user'); ?>";
        }
    	var subject_id 	= $(this).attr('subject_id');
    	var tutor_id 	= $(this).attr('tutorid');
    	var subject_name= $(this).attr('subject_name');
    	var that 		= $(this);
    	$.ajax({
			url: '<?php echo base_url();?>Master/unsavebooking',
			type: 'GET',
			data: {
				id_user: id_user,
				tutor_id: tutor_id,
				subject_id: subject_id,
				subject_name: subject_name
			},
			success: function(data)
			{ 
				data = JSON.parse(data);
				if(data['code'] == 1){
					notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-green c-white btn btn-success btn-block follow_ajax f-12');
					that.html('<?php echo $this->lang->line('follow'); ?>');				
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
      	});  
    });

    $(document).on('click', ".follow_ajax", function(e){
		e.preventDefault();
    // $(".follow_ajax").click(function(){
    	var id_user = "<?php echo $this->session->userdata('id_user_kids'); ?>";

        if (id_user == null || id_user=="") {
            id_user  = "<?php echo $this->session->userdata('id_user'); ?>";
        }
    	
    	var subject_id 	= $(this).attr('subject_id');
    	var tutor_id 	= $(this).attr('tutorid');
    	var subject_namee= $(this).attr('subject_namee');
    	var that 		= $(this);
    	$.ajax({
			url: '<?php echo base_url();?>Master/savebooking',
			type: 'GET',
			data: {
				id_user: id_user,
				tutor_id: tutor_id,
				subject_id: subject_id,
				subject_namee: subject_namee
			},
			success: function(data)
			{ 		
				data = JSON.parse(data);		
				if(data['code'] == 1){
					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect bgm-red c-white btn btn-danger btn-block unfollow_ajax f-12');
					that.html('<?php echo $this->lang->line('unfollow'); ?>');								
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
      	}); 
    });

    $('.showModal.ci-avatar').click(function(){
        var ids = $(this).attr('id');
        ids = ids.substr(5,10);
        $.get('<?php echo base_url(); ?>master/onah?id_user='+ids,function(hasil){
            hasil = JSON.parse(hasil);
            $('.modal_approve').attr('id',ids);
            $('.modal_decline').attr('id',ids);
            $('#modal_image').attr('src','<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>'+hasil['user_image']); 
            $('#modal_image').attr('onerror','this.src="<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=";');

            if (hasil['self_description'] == '') {
                $('#modal_self_desc').html("<?php echo $this->lang->line('nodescription'); ?>");
            }
            else
            {
                $('#modal_self_desc').html(hasil['self_description']);
            }
            $('#modal_user_name').html(hasil['user_name']);
            $('#modal_birthplace').html(hasil['user_birthplace']);
            $('#modal_birthdate').html(hasil['user_birthdate']);
            $('#modal_age').html(hasil['user_age']);
            $('#modal_callnum').html(hasil['user_callnum']);
            // $('#modal_gender').html(hasil['user_gender']);
            // alert(hasil['user_gender']);
            if (hasil['user_gender'] == 'Male') {
                $('#modal_gender').html("<?php echo $this->lang->line('male');?>");
            }
            else if(hasil['user_gender'] == 'Female')
            {
                $('#modal_gender').html("<?php echo $this->lang->line('women');?>");
            }
            $('#modal_religion').html(hasil['user_religion']);
            $('#modal_competency').html(hasil['competency']);
            $('#modal_eduback').html("Pendidikan Terakhir : "+" "+hasil['last_education']+ " " + hasil['nama_lembaga']);
            $('#modal_last_edu').html(hasil['last_education']+ " " + hasil['nama_lembaga']);
            // $('#modal_teachexp').html("Pengalaman Mengajar "+hasil['year_experience'] +" Tahun" );

            if(hasil['competency'] != ''){
                for (var i = 0; i<hasil['competency'].length; i++) {
                	var subject_name = hasil['competency'][i]['subject_name'];
		            var jenjang_name = hasil['competency'][i]['jenjang_name'];
		            var jenjang_level = hasil['competency'][i]['jenjang_level'];
		            var last_education = hasil['competency'][i]['last_education'];
		            var nama_lembaga = hasil['competency'][i]['nama_lembaga'];
		            if (jenjang_name == null) {
		            	jenjang_name = "";
		            	jenjang_level = "";
		            }
		            if (jenjang_level== "0") {
		            	jenjang_level = "";
		            }

                    $('#modal_competency').append('<tr>'+
                        '<td>'+subject_name+'</td>'+
                        '<td>'+jenjang_name+'  '+jenjang_level+'</td>'+                     
                        // '<td>'+hasil['education_background'][i]['institution_address']+'</td>'+
                        // '<td>'+hasil['education_background'][i]['graduation_year']+'</td>'+
                        '</tr>');
                }
            }
            $('#modalDefault').modal('show');
        });
        $('.modal_approve').click(function(){
            var newids = $(this).attr('id');
            $.get('<?php echo base_url(); ?>first/approveTheTutor/'+newids,function(s){
                location.reload();
            });
        });
        $('.modal_decline').click(function(){
            var newids = $(this).attr('id');
            $.get('<?php echo base_url(); ?>first/declineTheTutor/'+newids,function(s){
                location.reload();
            });
        });

        $("#show1").click(function(){            
            $("#contoh2").css('display','none');
            $("#contoh1").css('display','block');
        });
        $("#show2").click(function(){            
            $("#contoh1").css('display','none');
            $("#contoh2").css('display','block');    
        });

    });

    
</script>
<script type="text/javascript">
	var access_token_jwt = "<?php echo $this->session->userdata('access_token_jwt')?>";
	var iduser 	= "<?php echo $this->session->userdata('id_user');?>";
	// alert(iduser);
	$.ajax({
		url: '<?php echo base_url();?>Rest/allSubject_bytutor/access_token/'+access_token_jwt,
		type: 'GET',
		data: {
			id_user: iduser
			
		},
		success: function(response)
		{ 
			// var dataa = JSON.parse(response);
			var status_tutor = "";
			console.warn("test class "+response['status']);
			for (var i = 0; i< response.data.length;i++) {
                var idtutor = response['data'][i]['tutor_id'];
                var nametutor = response['data'][i]['tutor_name'];
                var imgtutor = response['data'][i]['tutor_image'];
                var countrytutor = response['data'][i]['tutor_country'];
                var alt_img ="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';";
                var status = response['data'][i]['status'];

                if (status == "verified") {
                	var status_tutor =	"<div class='ribbon'><span>Verified</span></div>";
                }
 

                var list_pelajaran ="<a href='#' class='ci-avatar showModal' id='card_"+idtutor+"'><section class='col-md-2 col-sm-4 col-xs-6 bgm-white' style='height:200px;'><center><a href='#' class='ci-avatar showModal' id='card_"+idtutor+"'>"+status_tutor+"<img class='ci-avatar showModal' id='gambar' src='<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+imgtutor+"' onerror="+alt_img+" style='margin-top:10px; width:20vh; height:20vh;'><br><h3 style='color: gray; font-size:12px; '>"+nametutor+"</h3><br></center></section></a>";

                $("#list_tutor").append(list_pelajaran);
                
            }
			// alert('hh');
			// if(data['code'] == 1){
			// 	notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut',data['message']);
			// 	that.attr('class','waves-effect bgm-green c-white btn btn-success btn-block follow_ajax f-12');
			// 	that.html('<?php echo $this->lang->line('follow'); ?>');				
			// }else{
			// 	notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
			// }
		}
  	});  
</script>