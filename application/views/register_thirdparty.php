<style type="text/css">
    body
    {
        background-color: white;
    }
    @media screen and (orientation:landscape) {
        #navbar_atas
        {
        }
        body
        {
            background-color: white;
        }
        #logo_mobile{
            margin-top: -5%;
        }
        #menu_register{
            margin-top: 10%;
        }
    }
    @media screen and (orientation:portrait) {
        #logo_mobile{
            margin-top: -9%;
        }
        #menu_register{
            margin-top: 20%;
        }
    }
    p.wrap {
        width: 200px; 
        border: 1px solid #000000;
        word-wrap: break-word;
    }
    .judul_register{
        font-size: 15px;
    }
    .brand {
        position: absolute;
        left: 50%;
        margin-left: -70px !important;  /* 50% of your logo width */
        display: block;
    }
    .toggle-switch .ts-label {
        min-width: 130px;
    }
    html, body {
        /*margin: 0px;
        padding: 0px;*/
        /*height: 100%;*/
        overflow: auto;
    }
    @media screen and (min-width: 450px){
        #navbar_atas_1{
            background-color: #008080; 
            /*background:rgba(0,80,80,0.7);*/
        }
    }
    @media screen and (min-width: 850px){
        body{
            background-size: cover; 
            background-repeat: no-repeat; 
            background-attachment: fixed; 
            height: 100% width:100%;
            background-color: white;
            /*background-image:url(../aset/img/blur/img3.jpg);*/
        }
        .gender{
            margin-top: 5%;
        }
    }
    @media screen and (max-width: 450px){
        body {
            background-color: white;
        }
    }
</style>
<style type="text/css"> 
    input::-webkit-input-placeholder {
        color: grey !important;
    }

    #navbar_atas_m{
        background-color: #008080; 
        /*background:rgba(0,80,80,0.7);"*/
    }
    #button_murid
    {
        width: 105%;
    }
    #button_tutor
    {
        width: 105%;
    }
    #button_tutor2
    {
        margin-left: -10px;
    }
    #level_siswa
    {
        font-size: 10px;
    }
    #level_umum
    {
        font-size: 10px;
    }  
    @media screen and (min-width: 770px){
        #menu_register
        {
            margin-top: 5%;
        }
    }
    @media screen and (max-width: 1125px){
        #levelregister
        {
            font-size: 15px;
        }
    }
    @media screen and (max-width: 1074px){
        #levelregister
        {
            font-size: 13px;
        }
    }
    @media screen and (max-width: 1112px){
        #level_siswa
        {
            font-size: 9px;
        }
        #level_umum
        {
            font-size: 9px;
        }
    }
    @media screen and (max-width: 1037px){
        #level_siswa
        {
            font-size: 8px;
        }
        #level_umum
        {
            font-size: 8px;
        }
    }
    @media screen and (max-width: 767px){
        #nav_mobile
        {
            display: inline;
        }
        .gender{
            margin-top: 10%;
        }
        #level_siswa
        {
            font-size: 10px;
        }
        #level_umum
        {
            font-size: 10px;
        }
        #button_murid
        {
            width: 100%;
        }
        #button_tutor
        {
            width: 100%;
        }
        #button_tutor2
        {
            margin-left: 0%;
        }
    }
    @media screen and (max-width: 360px){
        #level_siswa
        {
            font-size: 12px;
        }
        #level_umum
        {
            font-size: 12px;
        }
        .gender{
            margin-top: 20%;
        }
    }
    @media screen and (max-width: 320px){
        .captcha
        {
            transform:scale(0.80);-webkit-transform:scale(0.80);transform-origin:0 0;-webkit-transform-origin:0 0
        }
    }
</style>
<script type="text/javascript">
    var CaptchaCallback = function() {
        var cektype = "<?php echo $typeregister;?>";
        if (cektype=="student") {
            grecaptcha.render('RecaptchaField3', {'sitekey' : '6LcEsSUTAAAAAJPFvRwClTlzAcQFQ6IsAiERxOLV'});
        }
        else
        {            
            grecaptcha.render('captchaUmum', {'sitekey' : '6LcEsSUTAAAAAJPFvRwClTlzAcQFQ6IsAiERxOLV'});  
        }
        
    };
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://apis.google.com/js/api:client.js"></script>
<script type="text/javascript">
    $(document).on('keyup', '[data-min_max]', function(e){
  	    var min = parseInt($(this).data('min'));
  	    var max = parseInt($(this).data('max'));
  	    var val = parseInt($(this).val());
  	    if(val > max)
  	    {
  	        $(this).val(max);
  	        return false;
  	    }
  	    else if(val < min)
  	    {
  	        $(this).val(min);
  	        return false;
  	    }
    });
    $(document).on('keydown', '[data-toggle=just_number]', function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode == 65 && e.ctrlKey === true) ||
            (e.keyCode == 67 && e.ctrlKey === true) ||
            (e.keyCode == 88 && e.ctrlKey === true) ||
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var code = null;        
        var cekk = setInterval(function(){
            code = "<?php echo $this->session->userdata('code');?>";
            cek();
        },500);

        function cek(){
            if (code == "121") {
                $("#register_modal").modal('show');
                $("#alertverifikasiemail").css('display','block');
                code == null;
                clearInterval(cekk);
                $.ajax({
                    url: 'https://classmiles.com/Rest/clearsession',
                    type: 'POST',
                    data: {
                        code: code
                    },
                    success: function(response)
                    { 
                        console.warn(response);
                    }
                });
            }
        }
    });
</script>
<div class=" modal fade" data-modal-color="teal" id="register_modal" tabindex="-1" data-backdrop="static" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <center>
                <div class="modal-body" style="margin-top: 75%;" ><br>
                    
                   <div class="alert " style="display: block; ?>" id="alertverifikasiemail">
                        <?php echo $this->lang->line('successfullyregistered');?>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" style="">OK</button>
                </div>
            </center>
        </div>
    </div>
</div>
<div class="pull-right hidden-xs  header-inner" >
    <nav id="navbar_atas_1" class=" navbar navbar-default navbar-fixed-top" >
        <div class="col-sm-1">
        </div>
        <a id="logo_atas" class="navbar-brand " data-scroll href="<?php echo base_url(); ?>" style="text-align: center;">     
            <img style="margin-top: -9px;margin-left: -17px;" src="<?php echo base_url(); ?>aset/img/logo/logoclass6.png" alt="">
        </a>
        <div class=" nav navbar-nav navbar-right" style="margin-top: 16px; margin-right: 180px;">
            <a href="<?php echo base_url(); ?>" id="home_atas" type="button" class="c-white f-20" style=" cursor: pointer;">
                <i class="zmdi zmdi-home"></i>
            </a>
            <a hidden id="back_menu_atas" type="button" class="c-white f-20" onclick="show('register_menu')" style="cursor: pointer;">
                <i class="zmdi zmdi-arrow-left"></i>
            </a>
            <a hidden id="back_student_atas" type="button" class="c-white f-20" onclick="show('register_murid')" style="cursor: pointer;">
                <i class="zmdi zmdi-arrow-left"></i>
            </a>
        </div>
        <div class="col-sm-1"></div>  
        </div>
    </nav>
</div>
<div id="menu_register" >
  <?php
      function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
          $output = NULL;
          if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
              $ip = $_SERVER["REMOTE_ADDR"];
              if ($deep_detect) {
                  if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                  if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                      $ip = $_SERVER['HTTP_CLIENT_IP'];
              }
          }
          $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
          $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
          $continents = array(
              "AF" => "Africa",
              "AN" => "Antarctica",
              "AS" => "Asia",
              "EU" => "Europe",
              "OC" => "Australia (Oceania)",
              "NA" => "North America",
              "SA" => "South America"
          );
          if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
              $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
              if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                  switch ($purpose) {
                      case "location":
                          $output = array(
                              "city"           => @$ipdat->geoplugin_city,
                              "state"          => @$ipdat->geoplugin_regionName,
                              "country"        => @$ipdat->geoplugin_countryName,
                              "country_code"   => @$ipdat->geoplugin_countryCode,
                              "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                              "continent_code" => @$ipdat->geoplugin_continentCode
                          );
                          break;
                      case "address":
                          $address = array($ipdat->geoplugin_countryName);
                          if (@strlen($ipdat->geoplugin_regionName) >= 1)
                              $address[] = $ipdat->geoplugin_regionName;
                          if (@strlen($ipdat->geoplugin_city) >= 1)
                              $address[] = $ipdat->geoplugin_city;
                          $output = implode(", ", array_reverse($address));
                          break;
                      case "city":
                          $output = @$ipdat->geoplugin_city;
                          break;
                      case "state":
                          $output = @$ipdat->geoplugin_regionName;
                          break;
                      case "region":
                          $output = @$ipdat->geoplugin_regionName;
                          break;
                      case "country":
                          $output = @$ipdat->geoplugin_countryName;
                          break;
                      case "countrycode":
                          $output = @$ipdat->geoplugin_countryCode;
                          break;
                  }
              }
          }
          return $output;
      }
  ?>
	<div class="col-sm-3"></div>
	<div class="col-sm-6" style=" background-color: white; border-color: white; border-style: solid;">
			
      <?php 
          if ($typeregister == "student") {
          ?>
          <div id="register_student" style="background-color: white; margin-top: 5%; display: block;">
              <div class="lv-header hidden-xs" style="background-color: white; height: 65px;">
                  <div class="c-teal judul_register m-t-10 m-b-10" style="font-weight: bold; font-family: 'Trebuchet MS'; font-style:normal;">
                    <?php echo $this->lang->line('sebagai'); ?> <?php echo $this->lang->line('sstudent'); ?>            
                  </div>
                  <br>
              </div>
              <div id="tabber" class="form-wizard-basic fw-container" style="margin-left: 5%; margin-right: 5%;" >
                  <div class="">
                      <div class="tab-pane " id="tab1" >
                          <form role="form" action="<?php  echo base_url(); ?>master/daftarbaru" method="post">
                              <div class="row">
                                  <div class="col-sm-2 text-center"></div>
                                  <div class="col-sm-12 text-center" style="margin-top: -3%;">
                                      
                                  </div>
                                  <input type="text" name="tab" style="display: none;" value="1">
                                  <div class="col-md-6">
                                      <div class="form-group" >
                                          <div class="fg-line">
                                              <input type="text" name="first_name" required class="input-sm form-control fg-input" value="<?php if(isset($form_cache)){ echo $form_cache['first_name'];} ?>">
                                              <label class="fg-label"><?php echo $this->lang->line('first_name'); ?></label>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <div class="fg-line">
                                              <input type="text" name="last_name" required class="input-sm form-control fg-input" value="<?php if(isset($form_cache)){ echo $form_cache['last_name'];} ?>">
                                              <label class="fg-label"><?php echo $this->lang->line('last_name'); ?></label>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-12 m-t-5">
                                      <div class="form-group">
                                          <div class="fg-line">
                                              <input type="email" name="email" id="email" class="input-sm form-control fg-input" value="<?php echo $email;?>" readonly style="background-color:#F5F5F5;">
                                              <label class="fg-label">Email</label>
                                          </div>
                                          <!-- <small id="capemail" style="color:red; display: none;"><?php echo $this->lang->line('invalidemail');?></small> -->
                                      </div>
                                  </div>
                                  <div class="col-md-12 m-t-5">
                                      <div class="form-group">
                                          <div class="fg-line">
                                              <input type="password" name="kata_sandi" id="kata_sandi_student" required class="input-sm form-control fg-input">
                                              <label class="fg-label"><?php echo $this->lang->line('password'); ?></label>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-12 m-t-5">
                                      <div class="form-group">
                                          <div class="fg-line">
                                              <input type="password" name="konf_kata_sandi" id="konf_kata_sandi_student" required class="input-sm form-control fg-input">
                                              <label class="fg-label" ><?php echo $this->lang->line('confirm_pass'); ?></label>
                                          </div>
                                          <small id="cek_sandi" style="color: red;"></small>
                                      </div>
                                  </div>
                                  <div class="col-md-12">
                                      <label><?php echo $this->lang->line('birthday'); ?> :</label>
                                  </div>
                                  <div class="col-xs-3 col-sm-3">
                                      <select placeh required name="tanggal_lahir" id="tanggal_lahir" class="select form-control" data-live-search="true">
                                          <option disabled selected  value=''><?php echo $this->lang->line('datebirth'); ?></option>
                                          <?php 
                                              for ($date=01; $date <= 31; $date++) {
                                              ?>                                                  
                                                  <option value="<?php echo $date ?>" <?php if(isset($form_cache) && $form_cache['tanggal_lahir'] == $date){ echo 'selected="true"';} ?>><?php echo $date ?></option>
                                              <?php 
                                              }
                                          ?>
                                      </select>
                                  </div>
                                  <div class="col-xs-5 col-md-5">
                                      <select required name="bulan_lahir" class="select form-control" data-live-search="true">
                                          <option disabled selected value=''><?php echo $this->lang->line('monthbirth'); ?></option>
                                          <option value="01" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "01"){ echo 'selected="true"';} ?>>Januari</option>
                                          <option value="02" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "02"){ echo 'selected="true"';} ?>>Februari</option>
                                          <option value="03" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "03"){ echo 'selected="true"';} ?>>Maret</option>
                                          <option value="04" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "04"){ echo 'selected="true"';} ?>>April</option>
                                          <option value="05" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "05"){ echo 'selected="true"';} ?>>Mei</option>
                                          <option value="06" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "06"){ echo 'selected="true"';} ?>>Juni</option>
                                          <option value="07" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "07"){ echo 'selected="true"';} ?>>Juli</option>
                                          <option value="08" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "08"){ echo 'selected="true"';} ?>>Agustus</option>
                                          <option value="09" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "09"){ echo 'selected="true"';} ?>>September</option>
                                          <option value="10" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "10"){ echo 'selected="true"';} ?>>Oktober</option>
                                          <option value="11" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "11"){ echo 'selected="true"';} ?>>Nopember</option>
                                          <option value="12" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "12"){ echo 'selected="true"';} ?>>Desember</option>
                                      </select>
                                  </div>
                                  <div class="col-xs-4 col-md-4">
                                      <select required name="tahun_lahir" id="tahun_lahir" class="select form-control" data-live-search="true">
                                          <option disabled selected value=''><?php echo $this->lang->line('yearbirth'); ?></option>
                                          <?php 
                                              for ($i=1960; $i <= 2016; $i++) {
                                              ?>                                                 
                                                  <option value="<?php echo $i; ?>" <?php if(isset($form_cache) && $form_cache['tahun_lahir'] == $i){ echo 'selected="true"';} ?>><?php echo $i; ?></option>
                                              <?php 
                                              } 
                                          ?>
                                      </select>
                                  </div>
                                  <div class="col-md-12 gender">
                                      <div class="fg-line">
                                          <label class="radio radio-inline ">
                                              <input type="radio" name="jenis_kelamin" value="Male" <?php if(isset($form_cache) && $form_cache['jenis_kelamin'] == "Male"){ echo 'selected="true"';} ?>>
                                              <i class="input-helper"></i><?php echo $this->lang->line('male'); ?>  
                                          </label>
                                          <label class="radio radio-inline " >
                                              <input type="radio" name="jenis_kelamin" value="Female" <?php if(isset($form_cache) && $form_cache['jenis_kelamin'] == "Female"){ echo 'selected="true"';} ?>>
                                              <i class="input-helper"></i><?php echo $this->lang->line('women'); ?>  
                                          </label>
                                      </div>
                                  </div>
                                  <div class="col-md-12 m-t-20">
                                      <label><?php echo $this->lang->line('mobile_phone'); ?> :</label>
                                  </div>
                                  <div class="col-xs-6 col-md-5">
                                      <select required name="kode_area" class="selectpicker form-control" data-live-search="true">
                                          <?php                       
                                              $nama = ip_info("Visitor", "Country Code");
                                              $db = $this->db->query("SELECT * FROM `master_country` WHERE iso='$nama'")->row_array();
                                              $phone = $db['phonecode'];
                                              $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                                              foreach ($allsub as $row => $d) {
                                                  if($phone == $d['phonecode']){
                                                      echo "<option value='".$d['phonecode']."' selected='true'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                  }
                                                  if ($phone != $d['phonecode']) {
                                                      echo "<option value='".$d['phonecode']."'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                  }                                                            
                                              }
                                          ?>
                                      </select>
                                  </div>
                                  <div class="col-xs-6 col-md-7" >
                                      <div class="fg-line">
                                          <input type="text" id="no_hape" minlength="9" maxlength="13" name="no_hape" onkeyup="validPhone(this)"  onkeyup="validAngka(this)" onkeypress="return hanyaAngka(event)" required class="form-control " placeholder="87XXXXXX" value="<?php if(isset($form_cache)){ echo $form_cache['no_hape'];} ?>">
                                      </div>
                                  </div>                  
                                  <div class="col-md-12 m-t-20">
                                      <div class="fg-line">
                                          <input type="text" name="tulisan_jenjang_id" id="tulisan_jenjang_id" style="display: none" value="<?php if(isset($form_cache)){ echo $form_cache['tulisan_jenjang_id'];} ?>">
                                          <input type="text" name="tab" style="display: none" value="1">
                                          <select required name="jenjang_id" class="select2 form-control" id='jenjang_level' data-placeholder="<?php echo $this->lang->line('select_class'); ?>">
                                              <option value=""></option>
                                              <?php 
                                              if(isset($form_cache)){
                                                  echo '<option value="'.$form_cache['jenjang_id'].'" selected="true">'.$form_cache['tulisan_jenjang_id'].'</option>';
                                              } 
                                              ?>
                                          </select>
                                      </div>
                                  </div>
                                  <center>
                                      <div class="col-xs-12 captcha" ><br><br>
                                          <div class="fg-line" id="RecaptchaField3">
                                          </div>
                                      </div>
                                  </center>
                                  <div class="col-md-12 m-t-5">
                                      <label id='cek_syarat' >
                                          <input type="checkbox" value="oke" id="term_agreement">
                                          <?php echo $this->lang->line('syarat');?> <a target="_blank" class="c-blue" href="<?php echo base_url(); ?>syarat-ketentuan"><?php echo $this->lang->line('syarat1'); ?></a> <?php echo $this->lang->line('syarat2'); ?>
                                      </label>
                                  </div>
                                  <div class="col-sm-12 m-t-15">
                                      <button type="submit" class="btn bgm-teal btn-block" id="submit_signup_student"><?php echo $this->lang->line('btnsignup'); ?></button><br><br><br>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>  
          </div>
          <?php
          }
          else
          {
          ?>
          <div id="register_thirdparty" style="display: block;">        
              <div class="lv-header hidden-xs" style="background-color: white; height: 65px;">
                  <div class="c-teal judul_register m-t-10 m-b-10" style="font-weight: bold; font-family: 'Trebuchet MS'; font-style:normal;"><?php echo $this->lang->line('sebagai'); ?> <?php echo $this->lang->line('nonstudent'); ?>                
                  </div>
                  <br> 
              </div>

              <div id="tabber" class="form-wizard-basic fw-container" style="margin-left: 5%; margin-right: 5%;" >
                  <div class="">
                      <div class="tab-pane" id="tab2">
                          <form role="form" action="<?php  echo base_url(); ?>master/daftarbaru" method="post">
                              <div class="row">
                                  <div class="col-sm-2 text-center"></div>
                                  <div class="col-sm-12 text-center" style="margin-top: -3%;"></div>
                                      <input type="text" name="tab" style="display: none;" value="2">
                                      <div class="col-sm-6">
                                          <div class="form-group fg-float">
                                              <div class="fg-line">
                                                  <input type="text" name="first_name" required class="input-sm form-control fg-input" value="<?php if(isset($form_cache)){ echo $form_cache['first_name'];} ?>">
                                                  <label class="fg-label"><?php echo $this->lang->line('first_name'); ?></label>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-6">
                                          <div class="form-group fg-float">
                                              <div class="fg-line">
                                                  <input type="text" name="last_name" required class="input-sm form-control fg-input" value="<?php if(isset($form_cache)){ echo $form_cache['last_name'];} ?>">
                                                  <label class="fg-label"><?php echo $this->lang->line('last_name'); ?></label>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-12 m-t-5">
                                          <div class="form-group fg-float">
                                              <div class="fg-line">
                                                  <input type="email" name="email" id="email_thirdparty" style="background-color:#F5F5F5;" class="email_thirdparty input-sm form-control fg-input" value="<?php echo $email;?>" readonly>
                                                  <label class="fg-label">Email</label>
                                              </div>
                                              <!-- <small id="capemaill" style="color:red; display: none;"><?php echo $this->lang->line('invalidemail');?></small> -->
                                          </div>
                                      </div>
                                      <div class="col-sm-12 m-t-5">
                                          <div class="form-group fg-float">
                                              <div class="fg-line">
                                                  <input type="password" name="kata_sandi" id="kata_sandi" required class="input-sm form-control fg-input">
                                                  <label class="fg-label"><?php echo $this->lang->line('password'); ?></label>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-12 m-t-5">
                                          <div class="form-group fg-float">
                                              <div class="fg-line">
                                                  <input type="password" name="konf_kata_sandi" id="konf_kata_sandi" required class="input-sm form-control fg-input">
                                                  <label class="fg-label"><?php echo $this->lang->line('confirm_pass'); ?></label>
                                              </div>
                                              <small id="cek_sandi_umum" style="color: red;"></small>
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                      <label><?php echo $this->lang->line('birthday'); ?> :</label>
                                      </div>
                                      <div class="col-xs-3 col-sm-3">
                                          <select placeh required name="tanggal_lahir" id="tanggal_lahir" class="select form-control" data-live-search="true">
                                              <option disabled selected  value=''><?php echo $this->lang->line('datebirth'); ?></option>
                                              <?php 
                                                  for ($date=01; $date <= 31; $date++) {
                                                  ?>                                                  
                                                      <option value="<?php echo $date ?>" <?php if(isset($form_cache) && $form_cache['tanggal_lahir'] == $date){ echo 'selected="true"';} ?>><?php echo $date ?></option>                                                  
                                                  <?php 
                                                  }
                                              ?>
                                          </select>
                                      </div>
                                      <div class="col-xs-5 col-md-5">
                                          <select required name="bulan_lahir" class="select form-control" data-live-search="true">
                                              <option disabled selected value=''><?php echo $this->lang->line('monthbirth'); ?></option>
                                              <option value="01" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "01"){ echo 'selected="true"';} ?>>Januari</option>
                                              <option value="02" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "02"){ echo 'selected="true"';} ?>>Februari</option>
                                              <option value="03" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "03"){ echo 'selected="true"';} ?>>Maret</option>
                                              <option value="04" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "04"){ echo 'selected="true"';} ?>>April</option>
                                              <option value="05" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "05"){ echo 'selected="true"';} ?>>Mei</option>
                                              <option value="06" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "06"){ echo 'selected="true"';} ?>>Juni</option>
                                              <option value="07" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "07"){ echo 'selected="true"';} ?>>Juli</option>
                                              <option value="08" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "08"){ echo 'selected="true"';} ?>>Agustus</option>
                                              <option value="09" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "09"){ echo 'selected="true"';} ?>>September</option>
                                              <option value="10" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "10"){ echo 'selected="true"';} ?>>Oktober</option>
                                              <option value="11" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "11"){ echo 'selected="true"';} ?>>Nopember</option>
                                              <option value="12" <?php if(isset($form_cache) && $form_cache['bulan_lahir'] == "12"){ echo 'selected="true"';} ?>>Desember</option>
                                          </select>
                                      </div>
                                      <div class="col-xs-4 col-md-4">
                                          <select required name="tahun_lahir" id="tahun_lahir" class="select form-control" data-live-search="true">
                                              <option disabled selected value=''><?php echo $this->lang->line('yearbirth'); ?></option>
                                                  <?php 
                                                  for ($i=1960; $i <= 2016; $i++) {
                                                  ?>                                                 
                                                      <option value="<?php echo $i; ?>" <?php if(isset($form_cache) && $form_cache['tahun_lahir'] == $i){ echo 'selected="true"';} ?>><?php echo $i; ?></option>
                                                  <?php 
                                                  } 
                                              ?>
                                          </select>
                                      </div>
                                      <div class="col-md-12 gender" >
                                          <div class="fg-line">
                                              <label class="radio radio-inline ">
                                                  <input type="radio" name="jenis_kelamin" value="Male" <?php if(isset($form_cache) && $form_cache['jenis_kelamin'] == "Male"){ echo 'selected="true"';} ?>>
                                                  <i class="input-helper"></i><?php echo $this->lang->line('male'); ?>  
                                              </label>

                                              <label class="radio radio-inline " >
                                                  <input type="radio" name="jenis_kelamin" value="Female" <?php if(isset($form_cache) && $form_cache['jenis_kelamin'] == "Female"){ echo 'selected="true"';} ?>>
                                                  <i class="input-helper"></i><?php echo $this->lang->line('women'); ?>  
                                              </label>
                                          </div>
                                      </div>
                                      <div class="col-md-12 m-t-20">
                                          <label><?php echo $this->lang->line('mobile_phone'); ?> :</label>
                                      </div>
                                      <div class="col-xs-6 col-md-5">
                                          <select required name="kode_area" class="selectpicker form-control" data-live-search="true">
                                              <?php                       
                                                  $nama = ip_info("Visitor", "Country");
                                                  $db = $this->db->query("SELECT * FROM `master_country` WHERE nicename='$nama'")->row_array();
                                                  $phone = $db['phonecode'];
                                                  $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                                                  foreach ($allsub as $row => $d) {
                                                      if($phone == $d['phonecode']){
                                                          echo "<option value='".$d['phonecode']."' selected='true'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                      }
                                                      if ($phone != $d['phonecode']) {
                                                          echo "<option value='".$d['phonecode']."'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                      }                                                            
                                                  }
                                              ?>
                                          </select>
                                      </div>
                                      <div class="col-xs-6 col-md-7">
                                          <div class="fg-line">
                                              <input type="text" id="no_hape" minlength="9" maxlength="13" name="no_hape" onkeyup="validPhone(this)"  onkeyup="validAngka(this)" onkeypress="return hanyaAngka(event)"  required class="form-control " placeholder="87XXXXXX" value="<?php if(isset($form_cache)){ echo $form_cache['no_hape'];} ?>">
                                          </div>
                                      </div> 
                                      <center>
                                          <div class="col-xs-12 captcha" ><br><br>
                                              <div class="fg-line" id="captchaUmum">
                                              </div>
                                          </div>
                                      </center>
                                      <div class="col-md-12 m-t-5">
                                          <label>
                                              <input type="checkbox" value="oke" id="term_agreementt">
                                              <?php echo $this->lang->line('syarat');?> <a target="_blank" class="c-blue" href="<?php echo base_url(); ?>syarat-ketentuan"><?php echo $this->lang->line('syarat1'); ?></a> <?php echo $this->lang->line('syarat2'); ?>
                                          </label>
                                      </div>
                                      <div class="col-sm-12 m-t-20">
                                          <button class="btn bgm-teal btn-block" type="submit" id="submit_signup"><?php echo $this->lang->line('btnsignup'); ?></button><br><br><br>
                                      </div>
                                </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
          <?php
          }
      ?>

	</div>
</div>


<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ REGISTER MOBILE ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<script>
	function hanyaAngka(evt) {
	  var charCode = (evt.which) ? evt.which : event.keyCode
	   if (charCode > 31 && (charCode < 48 || charCode > 57))

	    return false;
	  return true;
	}
	function validAngka(a)
	{
		if(!/^[0-9.]+$/.test(a.value))
		{
		a.value = a.value.substring(0,a.value.length-31);
		}
	}
      function validPhone(a)
    {
        if(!/(^0$)|(^[1-9]\d{0,12}$)/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
</script>
<script type="text/javascript">
  	$(document).ready(function(){
    		$("#nav_login").click(function(){
      			$.ajax({
                url: 'https://classmiles.com/Rest/createsession',
                type: 'POST',
                success: function(response)
                { 
                    console.warn(response);
                    console.warn("<?php echo $this->session->userdata('code');?>");
                    window.location.replace('/');
                }
            });
    		});
    		$("#nav_loginn").click(function(){
      			$.ajax({
                url: 'https://classmiles.com/Rest/createsession',
                type: 'POST',
                success: function(response)
                { 
                    console.warn(response);
                    console.warn("<?php echo $this->session->userdata('code');?>");
                    window.location.replace('/');
                }
            });
    		});
  		  // Untuk Mobile
    		$("#nav_m_login").click(function(){
    			$.ajax({
                    url: 'https://classmiles.com/Rest/createsession',
                    type: 'POST',
                    success: function(response)
                    { 
                        console.warn(response);
                        console.warn("<?php echo $this->session->userdata('code');?>");
                        window.location.replace('/');
                    }
                });
    		});
    		$("#nav_m_loginn").click(function(){
      			$.ajax({
                url: 'https://classmiles.com/Rest/createsession',
                type: 'POST',
                success: function(response)
                { 
                    console.warn(response);
                    console.warn("<?php echo $this->session->userdata('code');?>");
                    window.location.replace('/');
                }
            });
    		});
    });
</script>
<script type="text/javascript">
	$(document).ready(function(){

		$('#kata_sandi').on('keyup',function(){
			if($('#konf_kata_sandi').val() != $(this).val()){
				$('#submit_signup').attr('disabled','');
        $('#term_agreementt').removeAttr('checked');
        
        $('#cek_sandi_umum').html('<?php echo $this->lang->line('passwordnotmatch'); ?>').css('color', 'red');
			}else{
				// $('#submit_signup').removeAttr('disabled');
        $('#cek_sandi_umum').html('').css('color', 'red');
			}
		});
		$('#konf_kata_sandi').on('keyup',function(){
			if($('#kata_sandi').val() != $(this).val() ){
				$('#submit_signup').attr('disabled','');
        $('#term_agreementt').removeAttr('checked');
        $('#cek_sandi_umum').html('<?php echo $this->lang->line('passwordnotmatch'); ?>').css('color', 'red');
			}else{
				// $('#submit_signup').removeAttr('disabled');
        
        $('#cek_sandi_umum').html('').css('color', 'red');
			}
		});
		$('#kata_sandi_student').on('keyup',function(){
      if($('#konf_kata_sandi_student').val() != $(this).val()){
        $('#submit_signup_student').attr('disabled','');
        $('#term_agreement').removeAttr('checked');
        $('#cek_sandi').html('<?php echo $this->lang->line('passwordnotmatch'); ?>').css('color', 'red');
      }
			else if($('#konf_kata_sandi_student').val() != $(this).val()){
				$('#submit_signup_student').attr('disabled','');
        $('#term_agreement').removeAttr('checked');
        $('#cek_sandi').html('<?php echo $this->lang->line('passwordnotmatch'); ?>').css('color', 'red');
			}else{
        $('#cek_sandi').html('').css('color', 'red');
			}
		});
		$('#konf_kata_sandi_student').on('keyup',function(){
			if($('#kata_sandi_student').val() != $(this).val() ){
				$('#submit_signup_student').attr('disabled','');
        // $('#cek_syarat').attr('hidden','');
        $('#term_agreement').removeAttr('checked');
        $('#cek_sandi').html('<?php echo $this->lang->line('passwordnotmatch'); ?>').css('color', 'red');
        
			}else{
				// $('#submit_signup_student').removeAttr('disabled');
        $('#cek_sandi').html('').css('color', 'red');
        $('#cek_syarat').removeAttr('hidden');
        
			}
		});

		$('#term_agreementt').change(function(){
        if($('#kata_sandi_student').val() == $('#konf_kata_sandi_student').val() && $('#term_agreementt').is(':checked') == false){
            $('#submit_signup').attr('disabled','');
        }else{
            $('#submit_signup').removeAttr('disabled');
        }
    });

    $('#term_agreement').change(function(){
        if($('#kata_sandi_student').val() == $('#konf_kata_sandi_student').val() &&  $('#term_agreement').is(':checked') == false){
            $('#submit_signup_student').attr('disabled','');
        }else{
            $('#submit_signup_student').removeAttr('disabled');
        }
    });

		var g_startup = 0;
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		      var target = $(e.target).attr("href"); // activated tab
		      if(target == "#tab1"){
		        // alert(g_startup);
		        if(g_startup == 2){
		        	g_startup = 1;
		          // alert($('#recap_2').html());
		          $('#renderer').appendTo('#recap_1');  
		      }

		}
		else if(target == "#tab2")
		{
		    // alert(g_startup);
		    if(g_startup == 1 || g_startup == 0){
		        g_startup = 2;
		        $('#renderer').appendTo('#recap_2');  
		    }
		}
		var g_startup = 0;
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		      var target = $(e.target).attr("href"); // activated tab
		      if(target == "#tabm1"){
		        // alert(g_startup);
		        if(g_startup == 2){
		        	g_startup = 1;
		          // alert($('#recap_2').html());
		          $('#renderer').appendTo('#recap_m1');  
		      }

		}
		else if(target == "#tabm2")
		{
		    // alert(g_startup);
		    if(g_startup == 1 || g_startup == 0){
		        g_startup = 2;
		        $('#renderer').appendTo('#recap_m2');  
		    }
		}
		});
		$('#jenjang_level').change(function(e){
			$('#tulisan_jenjang_id').val($(this).find("option:selected").text());
		});
		var vuvu = $('#tabber').tabs();

		<?php 
		if(isset($form_cache) && $form_cache['tab'] == "2"){
			echo "
			$( '#tabber').tabs({ active: 1 });
			if(g_startup == 1 || g_startup == 0){
				g_startup = 2;
				$('#renderer').appendTo('#recap_2');  
			}
			$('#li_tab2').attr('class','ui-state-default ui-corner-top ui-tabs-active ui-state-active active');
			$('#li_tab1').attr('class','ui-state-default ui-corner-top');
			";
		}
		?>
	});

	});
</script>
<script type="text/javascript">
 	$('.btn_setindonesialp').click(function(e){
      e.preventDefault();    
      var lang  = "<?php echo $this->session->userdata('lang'); ?>";
      if (lang == "indonesia") {         
          $.get('<?php echo base_url('set_lang/english'); ?>',function(hasil){  location.reload(); });
      } 
      else 
      {
          $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
      }              
	});
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn_setindonesia').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
        });
        $('#btn_setenglish').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/english'); ?>',function(){ location.reload(); });
        });

        $('#lightblue').click(function(e){
        	e.preventDefault();
        	$.get('<?php $this->session->set_userdata('color','lightblue'); ?>',function(){  });
		});
		$('#bluegray').click(function(e){
			e.preventDefault();
			$.get('<?php $this->session->set_userdata('color','bluegray'); ?>',function(){ location.reload(); });						
		});
		$('#cyan').click(function(e){	
			e.preventDefault();
			$.get('<?php $this->session->set_userdata('color','cyan'); ?>',function(){ location.reload(); });			
		});
		$('#teal').click(function(e){
			e.preventDefault();
			$.get('<?php $this->session->set_userdata('color','teal'); ?>',function(){ location.reload(); });			
		});
		$('#orange').click(function(e){
			e.preventDefault();
			$.get('<?php $this->session->set_userdata('color','orange'); ?>',function(){ location.reload(); });			
		});
		$('#blue').click(function(e){
			e.preventDefault();
			$.get('<?php $this->session->set_userdata('color','blue'); ?>',function(){ location.reload(); });			
		});
        $('#btn_callsupport').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url(); ?>process/callsupport',function(ret){
                ret = JSON.parse(ret);
                if(ret['status'] == true){
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',ret['message']);
                }else{
                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',ret['message']);
                }
                

            });
        });
    });
</script>


