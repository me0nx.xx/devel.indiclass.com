<link rel="icon" href="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>favicon.ico">
<style type="text/css">
    .toggle-switch .ts-label {
      min-width: 130px;
    }
    html, body {
      /*margin: 0px;
      padding: 0px;*/
      /*height: 100%;*/
      overflow: auto;
    }
     @media screen and (min-width: 450px){
        body{
            background-color: white;
            /*background-size: cover; 
            background-repeat: no-repeat; 
            background-attachment: fixed; 
            height: 100% width:100%;
            background-image:url(../aset/img/blur/img3.jpg);*/
        }
      }
</style>
<script type="text/javascript">

    $(window).on('load',function(){
        $('#cekTutorModal').modal('show');
        $('#submit_signup').removeAttr('disabled');
    });
    function show(elementId) { 
        document.getElementById("register_web").style.display="none";
        document.getElementById(elementId).style.display="block";
    }
</script>
<style type="text/css">
    input::-webkit-input-placeholder {
        color: grey !important;
        }
  @media screen and (max-width: 320px){
        .captcha
        {
            transform:scale(0.80);-webkit-transform:scale(0.80);transform-origin:0 0;-webkit-transform-origin:0 0
        }
    }

    @media screen and (max-width: 450px){

     body {
            background-color: white;
        }

        #register_web
        {
           display: none;
        }
        #register_mobile
        {
            display: inline;
        }
    }
        
    @media screen and (max-width: 770px){
            #Nav_registrasi
            {
               display: none;
            }
            .captcha
            {
                margin-left: -7.5%;
            }
        }

    </style>
<script>
    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }
    function validAngka(a)
    {
        if(!/^[0-9.]+$/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
    function validPhone(a)
    {
        if(!/(^0$)|(^[1-9]\d{0,12}$)/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://apis.google.com/js/api:client.js"></script>
<script type="text/javascript">
    var CaptchaCallback = function() {
        grecaptcha.render('RecaptchaField9', {'sitekey' : '6Le_mnwUAAAAAKloUTgurltTfRPvkj2UqVY-_9tE'});
        // grecaptcha.render('RecaptchaField6', {'sitekey' : '6Le_mnwUAAAAAKloUTgurltTfRPvkj2UqVY-_9tE'});
    };
</script>
<div class="modal fade" data-backdrop="static" style="margin-top: 30px;" id="cekTutorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
    <div id='modal_tutor' style="" class="modal-success modal-dialog" role="document">
      <div class="modal-content" style="">
        <div class="modal-header">
          <h4 class=" modal-title" id="myModalLabel"><?php echo $this->lang->line('wlogin'); ?> Indiclass.</h4>
        </div>
        <div class="modal-body">
          <p><?php echo $this->lang->line('pesanregistertutor1'); ?></p>
            <p><?php echo $this->lang->line('pesanregistertutor2'); ?></p>
        </div>
        <div class="modal-footer">
             <button style="" type="button" class="btn btn-default"><a href="<?php echo base_url(); ?>Register"><?php echo $this->lang->line('back'); ?></button>
          <button style="" type="button" class="btn btn-info" data-dismiss="modal"><?php echo $this->lang->line('lanjutkan'); ?></button>
        </div>
      </div>
    </div>
</div>

<div id="Nav_registrasi" class="pull-right hidden-xs header-inner" >
    <nav class=" navbar navbar-default navbar-fixed-top" style="background-color: #ea2127;">
        <div class="col-sm-1">
        </div>
        <a class="navbar-brand" data-scroll href="<?php echo base_url(); ?>">          
            <img style="margin-top: -9px;margin-left: -17px; height: 30px;" src="https://indiclass.id/aset/img/indiLogo2.png" alt="">
        </a>
        
    </nav>
</div>
<div class="modal fade" data-modal-color="red"  id="tutor_modal" tabindex="-1" aria-labelledby="myModalLabel" tabindex="-1" data-backdrop="static" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <center>
                <div class="modal-body" style="margin-top: 30%;" > 
                    <div class="alert" style="display: block; ?>" id="alertwrongemail">
                        <?php echo $this->lang->line('tutoraccountregistered');?>
                    </div>
                    <div class="alert" style="display: block; ?>" id="alertdata">
                        Data Belum Lengkap!!!
                    </div>
                    <div class="alert" style="display: none; ?>" id="alertverifikasiemail">
                        <?php echo $this->lang->line('youremail2');?> 
                    </div>
                </div>
                <div class="modal-footer">
                  <button class="btn btn-link" data-dismiss="modal" data-toggle="modal" aria-label="Close" >OK</button>
                </div>
            </center>
        </div>
    </div>
</div>
<div class="" id="register_web">
    <div class="col-sm-3">    
    </div>    
    <div class="col-sm-6" style="margin-top:7%;">
        <div class="" style="">
            <div class="lv-header" style="background-color: white; height: 65px;">
                <div class="c-teal f-20 m-t-10 m-b-10" style="font-family: 'Trebuchet MS'; font-style:normal;"><?php echo $this->lang->line('signup_tutor'); ?></div>
                <div class=""  style="text-align-last: left;  margin-top: -26px;" >
                    <a href="<?php echo base_url(); ?>Register" id="back_home" type="button" class="c-teal f-19 m-l-10" style="cursor: pointer;">
                        <i class="zmdi zmdi-arrow-left"></i>
                    </a>
                </div> 
            </div>
            <div class="card-body" style="margin-left:7%; margin-right:7%;">
                <div class="tab-content">
                    <!-- <?php if($this->session->flashdata('mes_alert')){ ?>
                    <div id="alertwrongemail" class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <?php echo $this->session->flashdata('mes_message'); ?>
                    </div>
                    <?php } ?> -->
  
                    <div class="row">
                        <form role="form" action="<?php echo base_url('master/daftartutor'); ?>" method="post">
                            <div class="col-sm-6">
                                <div class="form-group fg-float">
                                    <div class="fg-line">
                                        <input type="text" name="first_name" required class="input-sm form-control fg-input">
                                        <label class="fg-label"><?php echo $this->lang->line('first_name'); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group fg-float">
                                    <div class="fg-line">
                                        <input type="text" name="last_name" required class="input-sm form-control fg-input">
                                        <label class="fg-label"><?php echo $this->lang->line('last_name'); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 m-t-5">
                                <div class="form-group fg-float">
                                    <div class="fg-line">
                                        <input type="email" name="email" required class="input-sm form-control fg-input">
                                        <label class="fg-label">Email</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 m-t-5">
                                <div class="form-group fg-float">
                                    <div class="fg-line">
                                        <input type="password" name="kata_sandi" id="kata_sandi" required class="input-sm form-control fg-input">
                                        <label class="fg-label"><?php echo $this->lang->line('password'); ?></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 m-t-5">
                                <div class="form-group fg-float ">
                                    <div class="fg-line">
                                        <input type="password" name="konf_kata_sandi" id="konf_kata_sandi" required class="input-sm form-control fg-input">
                                        <label class="fg-label"><?php echo $this->lang->line('confirm_pass'); ?></label>
                                    </div>
                                    <small id="cek_sandi" style="color: red;"></small>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label><?php echo $this->lang->line('birthday'); ?> :</label>
                            </div>
                            <div class="col-sm-4 ">
                                <div class="fg-line">
                                    <select required name="tanggal_lahir" id="tanggal_lahir" class="select2 form-control">
                                        <option disabled selected value=''><?php echo $this->lang->line('datebirth'); ?></option>
                                        <?php 
                                            for ($date=01; $date <= 31; $date++) {                                                      
                                                                                                        
                                        ?>                                                  
                                        <option value="<?php echo $date ?>"><?php echo $date ?></option>                                                    
                                        <?php 
                                            }
                                        ?>  
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="fg-line">
                                    <select required name="bulan_lahir" class="select2 form-control">
                                            <option disabled selected value=''><?php echo $this->lang->line('monthbirth'); ?></option>
                                            <option value="01">January</option>
                                            <option value="02">February</option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>                                                            
                                        </select>
                                </div>                                        
                            </div>
                            <div class="col-sm-4">
                                <div class="fg-line">                                           
                                    <select required name="tahun_lahir" id="tahun_lahir" class="select2 form-control">
                                        <option disabled selected value=''><?php echo $this->lang->line('yearbirth'); ?></option>
                                        <?php 
                                            for ($i=1960; $i <= 2016; $i++) {                                                                            
                                        ?>                                                  
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php 
                                            } 
                                        ?>
                                    </select>                                              
                                </div>
                            </div>
                            <div class="col-md-12 m-t-30 m-b-10">
                                <div class="fg-line">
                                   <label class="radio radio-inline ">
                                        <input type="radio" name="jenis_kelamin" required value="Male" <?php if(isset($form_cache) && $form_cache['jenis_kelamin'] == "Male"){ echo 'selected="true"';} ?>>
                                        <i class="input-helper"></i><?php echo $this->lang->line('male'); ?>  
                                    </label>
                                    
                                    <label class="radio radio-inline " >
                                        <input type="radio" name="jenis_kelamin" required value="Female" <?php if(isset($form_cache) && $form_cache['jenis_kelamin'] == "Female"){ echo 'selected="true"';} ?>>
                                        <i class="input-helper"></i><?php echo $this->lang->line('women'); ?>  
                                    </label>
                                </div>                                        
                            </div>
                            <div class="col-md-12 m-t-20">
                                    <label><?php echo $this->lang->line('kewarganegaraan'); ?> :</label>
                                </div>
                                <div class="col-xs-12 col-md-12">
                                    <select required name="nama_negara" class="selectpicker form-control" data-live-search="true">
                                      <?php                       
                                        $nama_negara = ip_info("Visitor", "Country Code");
                                        $db = $this->db->query("SELECT * FROM `master_country` WHERE iso='$nama_negara'")->row_array();
                                        $negara = $db['phonecode'];
                                        $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                                        foreach ($allsub as $row => $d) {
                                            if($negara == $d['phonecode']){
                                                echo "<option value='".$d['nicename']."' selected='true'>".$d['nicename']."</option>";
                                            }
                                            if ($negara != $d['phonecode']) {
                                                echo "<option value='".$d['nicename']."'>".$d['nicename']."</option>";
                                            }                                                            
                                        }
                                        ?>
                                    </select>
                                </div>
                            <div class="col-md-12 m-t-10">
                                <label><?php echo $this->lang->line('mobile_phone'); ?> :</label>
                            </div>
                            <div class="col-md-4">
                                <div class="fg-line">                                                       
                                    <select required name="kode_area" class="select2 form-control">
                                            <?php                       
                                              $nama = ip_info("Visitor", "Country");
                                              $db = $this->db->query("SELECT * FROM `master_country` WHERE nicename='$nama'")->row_array();
                                              $phone = $db['phonecode'];
                                              $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                                              foreach ($allsub as $row => $d) {
                                                  if($phone == $d['phonecode']){
                                                      echo "<option value='".$d['phonecode']."' selected='true'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                  }
                                                  if ($phone != $d['phonecode']) {
                                                      echo "<option value='".$d['phonecode']."'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                  }                                                            
                                              }
                                              ?>
                                        </select>
                                </div>
                                <!-- <span class="zmdi zmdi-flag form-control-feedback"></span> -->
                            </div>
                            <div class="col-sm-8">
                                <div class="fg-line">
                                    <input type="text" id="no_hape" minlength="9" maxlength="13" name="no_hape" onkeyup="validPhone(this)" required class="form-control " placeholder="87XXXXXX">
                                </div>
                            </div>
                            <div class="col-sm-12 m-t-20">
                                <div  class="fg-line">
                                    <input id="kotak_referral" placeholder="<?php echo $this->lang->line('kodereferral'); ?>" style=" text-transform: uppercase;" type="text" required class="input-sm form-control fg-input">
                                    <input type="text" class="" id="valkodeid" name="kode_referral" hidden>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label style="">
                                    <input type="checkbox" value="oke" id="check_kode_referral">
                                    <?php echo $this->lang->line('tidakpunyareferral'); ?>
                                </label>
                            </div>
                            <div class="col-sm-6">
                                <div class="col-sm-12" style="text-align: right;">
                                    <small id="validasi_referral" >
                                    </small>
                                </div>
                            </div>
                            <center>
                                <div class="col-sm-12 m-t-10 captcha" id="cek_captcha">
                                     <div class="fg-line ">
                                        <div class="fg-line" id="RecaptchaField9"></div>
                                    </div>
                                </div>
                            </center> 
                            <div class="col-sm-12 m-b-20 " >
                                <label >
                                    <input type="checkbox" value="oke" id="term_agreement">
                                    <?php echo $this->lang->line('syarat');?> <a target="_blank" class="c-blue"  href="<?php echo base_url(); ?>syarat-ketentuan"><?php echo $this->lang->line('syarat1'); ?></a> <?php echo $this->lang->line('syarat2'); ?>
                                </label>
                            </div>   
                            <div class="col-sm-12 m-t-20">
                                <button type="submit" id="submit_signup" disabled="disabled" class="btn bgm-teal btn-block"><?php echo $this->lang->line('btnsignup'); ?></button>
                            </div>
                            <div class="col-sm-12 m-t-20 text-center">
                                <label class="fg-label f-14 m-r-20"><?php echo $this->lang->line('alreadyaccount'); ?> <a id="clickhere" style="cursor: pointer;"><?php echo $this->lang->line('here'); ?>
                                </a></label>
                                <hr>
                            </div>
                        </form>
                    </div>
                           
                    <!-- akhir student -->                            
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">                                                 
    </div>  
</div>

<script type="text/javascript">
        $(document).ready(function(){
            // $("#modal").click(function()
            // {
            //     $("#tutor_modal").modal('show');    
            // });
            
            var code = null;
            var cekk = setInterval(function(){
                code = "<?php echo $this->session->userdata('code');?>";

                cek();
            },500);
            $(function() {

                $('#check_kode_referral').bind('change', function (v) {

                    if($(this).is(':checked')) {
                        document.getElementById("kotak_referral").value = null;
                        $('#kotak_referral').attr('disabled','');
                        $("#validasi_referral").html("");
                        $("#valkodeid").val('');

                    } else {
                        $('#kotak_referral').removeAttr('disabled','');
                    }
                });
            });
            $('#kotak_referral').on('keyup',function(){
                if($(this).val() != null){
                    var codereferral = $(this).val();
                    var user_utc = new Date().getTimezoneOffset();
                    user_utc = -1 * user_utc;
                    var tgl = "<?php echo date("Y-m-d H:i:s"); ?>";                    
                    $.ajax({
                        url: '<?php echo base_url(); ?>/Rest/checkReferral',
                        type: 'POST',
                        data: {
                            codereferral: codereferral,
                            dateuser:tgl,
                            type:'register',
                            user_device:'web',
                            user_utc:user_utc
                        },
                        success: function(response)
                        { 
                            var responsecode = response['code'];
                            var referral_id  = response['data'];
                            console.warn("idnya "+referral_id);
                            if(responsecode == 200){
                                $("#validasi_referral").css("color","green");
                                $("#validasi_referral").html("<?php echo $this->lang->line('referralbenar'); ?>"); 
                                $("#valkodeid").val(referral_id);
                            }else{
                                $("#validasi_referral").css("color","red");
                                $("#validasi_referral").html("<?php echo $this->lang->line('referralsalah'); ?>"); 
                                $("#valkodeid").val('');
                            }
                        }
                    });
                    
                }
                else if ($(this).val() == null){
                    $("#validasi_referral").html("");
                }
            });
            function cek(){
                if (code == "202") {
                    $("#tutor_modal").modal('show');
                    $("#alertverifikasiemail").css('display','none');
                    $("#alerttutorregistered").css('display','none');
                    $("#alertdata").css('display','none');
                    $("#alertwrongemail").css('display','block');
                    code == null;
                    clearInterval(cekk);
                    $.ajax({
                        url: '<?php echo base_url(); ?>/Rest/clearsession',
                        type: 'POST',
                        data: {
                            code: code
                        },
                        success: function(response)
                        { 
                            console.warn(response);
                        }
                    });
                }
                else if (code == "203") {
                    $("#tutor_modal").modal('show');
                    $("#alerttutorregistered").css('display','none');
                    $("#alertwrongemail").css('display','none');
                    $("#alertdata").css('display','none');
                    $("#alertverifikasiemail").css('display','block');
                    code == null;
                    clearInterval(cekk);
                    $.ajax({
                        url: '<?php echo base_url(); ?>/Rest/clearsession',
                        type: 'POST',
                        data: {
                            code: code
                        },
                        success: function(response)
                        { 
                            console.warn(response);
                        }
                    });
                }
                else if (code == "333") {
                    $("#tutor_modal").modal('show');
                    $("#alerttutorregistered").css('display','none');
                    $("#alertwrongemail").css('display','none');
                    $("#alertdata").css('display','block');
                    $("#alertverifikasiemail").css('display','none');
                    code == null;
                    clearInterval(cekk);
                    $.ajax({
                        url: '<?php echo base_url(); ?>/Rest/clearsession',
                        type: 'POST',
                        data: {
                            code: code
                        },
                        success: function(response)
                        { 
                            console.warn(response);
                        }
                    });
                }
                else if (code == "204") {
                    // $("#tutor_modal").modal('show');
                    $("#alertverifikasiemail").css('display','none');
                    $("#alertwrongemail").css('display','none');
                    $("#alertdata").css('display','none');
                    $("#alerttutorregistered").css('display','block');
                    code == null;
                    clearInterval(cekk);
                    $.ajax({
                        url: '<?php echo base_url(); ?>/Rest/clearsession',
                        type: 'POST',
                        data: {
                            code: code
                        },
                        success: function(response)
                        { 
                            console.warn(response);
                        }
                    });
                }
            }
        });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#kata_sandi').on('keyup',function(){
            if($('#konf_kata_sandi').val() != $(this).val()){
                $('#submit_signup').attr('disabled','');
               
            }else{
                $('#submit_signup').removeAttr('disabled');
                $('#cek_sandi').html('').css('color', 'red');
                $('#cek_sandi_m').html('').css('color', 'red');
            }
        });
        $('#konf_kata_sandi').on('keyup',function(){
            if($('#kata_sandi').val() != $(this).val()){
                $('#submit_signup').attr('disabled','');
                $('#cek_sandi').html('<?php echo $this->lang->line('passwordnotmatch'); ?>').css('color', 'red');
                $('#cek_sandi_m').html('<?php echo $this->lang->line('passwordnotmatch'); ?>').css('color', 'red');
            }else{
                $('#submit_signup').removeAttr('disabled');
                $('#cek_sandi').html('').css('color', 'red');
                $('#cek_sandi_m').html('').css('color', 'red');
            }
        });
         $('#kata_sandi').on('keyup',function(){
            if($('#konf_kata_sandi_m').val() != $(this).val()){
                $('#submit_signup').attr('disabled','');
               
            }else{
                $('#submit_signup').removeAttr('disabled');
                $('#cek_sandi').html('').css('color', 'red');
                $('#cek_sandi_m').html('').css('color', 'red');
            }
        });
        $('#konf_kata_sandi_m').on('keyup',function(){
            if($('#kata_sandi_m').val() != $(this).val()){
                $('#submit_signup').attr('disabled','');
                $('#cek_sandi').html('<?php echo $this->lang->line('passwordnotmatch'); ?>').css('color', 'red');
                $('#cek_sandi_m').html('<?php echo $this->lang->line('passwordnotmatch'); ?>').css('color', 'red');
            }else{
                $('#submit_signup').removeAttr('disabled');
                $('#cek_sandi').html('').css('color', 'red');
                $('#cek_sandi_m').html('').css('color', 'red');
            }
        });
        $("#clickhere").click(function(){
            $.ajax({
                url: '<?php echo base_url(); ?>/Rest/createsession',
                type: 'POST',
                success: function(response)
                { 
                    console.warn(response);
                    console.warn("<?php echo $this->session->userdata('code');?>");
                    window.location.replace('/');
                }
            });
        });
        $('#cek_captcha').change(function(){
            if($('#RecaptchaField9').is(':checked') == false){
                $('#submit_signup').attr('disabled','');
            }else{
                $('#submit_signup').removeAttr('disabled');
            }
        });
    })
</script>
<script type="text/javascript">
   $('.btn_setindonesialp').click(function(e){
              e.preventDefault();    
              var lang  = "<?php echo $this->session->userdata('lang'); ?>";
              if (lang == "indonesia") {         
                $.get('<?php echo base_url('set_lang/english'); ?>',function(hasil){  location.reload(); });
              } else {
                  $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
              }
               
          });
          </script>
          <script type="text/javascript">
    $(document).ready(function(){
        $('#btn_setindonesia').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/indonesia'); ?>',function(hasil){  location.reload(); });
        });
        $('#btn_setenglish').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url('set_lang/english'); ?>',function(){ location.reload(); });
        });

        $('#lightblue').click(function(e){
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','lightblue'); ?>',function(){  });
        });
        $('#bluegray').click(function(e){
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','bluegray'); ?>',function(){ location.reload(); });                       
        });
        $('#cyan').click(function(e){   
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','cyan'); ?>',function(){ location.reload(); });           
        });
        $('#teal').click(function(e){
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','teal'); ?>',function(){ location.reload(); });           
        });
        $('#orange').click(function(e){
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','orange'); ?>',function(){ location.reload(); });         
        });
        $('#blue').click(function(e){
            e.preventDefault();
            $.get('<?php $this->session->set_userdata('color','blue'); ?>',function(){ location.reload(); });           
        });
        $('#btn_callsupport').click(function(e){
            e.preventDefault();
            $.get('<?php echo base_url(); ?>process/callsupport',function(ret){
                ret = JSON.parse(ret);
                if(ret['status'] == true){
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',ret['message']);
                }else{
                    notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',ret['message']);
                }
                

            });
        });
    });
</script>

<?php

function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}

?>