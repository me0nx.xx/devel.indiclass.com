<style type="text/css">
    .modal-backdrop.in {
        opacity: 0.9;
    }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;  
        opacity: 0.7;      
        background: url('https://2aih25gkk2pi65s8wfa8kzvi-wpengine.netdna-ssl.com/gre/wp-content/plugins/magoosh-lazyload-comments-better-ui/assets/img/loading.gif') center no-repeat #000;
        background-size: 150px 150px;
    }
    .modal-content  {
        -webkit-border-radius: 0px !important;
        -moz-border-radius: 0px !important;
        border-radius: 10px !important; 
    }
    .bp-header:hover
    {
        cursor: pointer;
    }
    .ci-avatar {
        background-color: rgba(255, 255, 255, 1);
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .ci-avatar:hover {
        background-color: rgba(52, 73, 94, 0.2);
    }
    /*.modal-body{
        max-height: 80vh;
    }
    .modal-dialog{
        width: 28%;        
    }
    @media screen and (max-width: 1250px) {
        .modal-dialog{
            width: 35%;
        }
    }
    @media screen and (max-width: 950px) {
        .modal-dialog{
            width: 40%;
        }
    }
    @media screen and (max-width: 780px) {
        .modal-dialog{
            width: 95%;
        }
    }*/
</style>

<div ng-show="loding" class="page-loader" style="background-color:black; opacity: 0.5;">
    <div class="preloader pl-lg pls-white">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p style="margin-left: -18px;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
    </div>
</div>

<!-- <header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #db1f00;"> -->
    <?php $this->load->view('inc/navbar'); ?>
<!-- </header> -->

<section id="main">

    <aside id="sidebar" class="sidebar c-overflow" style="z-index: 1; position: fixed;">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <div class="se-pre-con" style="display: none;"></div>

    <section id="content">

        <div class="container invoice">
            
            <div class="block-header">
                <h2>Keranjang Order<small>Silakan pilih metode pembayaran pada halaman berikutnya, periksa kembali keranjang belanja Anda dan klik tombol "Pilih Metode Pembayaran" di bawah.</small></h2>
            </div>

            <?php       
                $order_id = null;
                $UnixCode = null;         
                if (!isset($_GET['order_id']) != null) {
                    ?>
                    <div class="alert alert-info" role="alert">Tidak ada data</div>
                    <?php 
                }
                else
                {                
                    $order_id           = $_GET['order_id'];
                    $order              = $this->db->query("SELECT * FROM tbl_order as tor INNER JOIN tbl_invoice as ti ON tor.order_id=ti.order_id WHERE tor.order_id='$order_id'")->row_array();                    
                    if (empty($order)) {
                        $this->session->set_flashdata('mes_alert','info');
                        $this->session->set_flashdata('mes_display','block');
                        $this->session->set_flashdata('mes_message','Maaf, Order tersebut tidak dapat kami temukan diserver kami.');
                        redirect('/');
                    }
                    else
                    {
                        $invoice_id         = $order['invoice_id'];
                        $datetrans          = $order['last_update']; 
                        $UnixCode           = $order['unix_code'];
                        $order_status       = $order['order_status'];
                        $hargakelas         = $order['total_price']-$UnixCode;
                        $hargakelas         = number_format($hargakelas, 0, ".", ".");
                        $hasiljumlah        = number_format($order['total_price'], 0, ".", ".");

                        $user_utc           = $this->session->userdata('user_utc');
                        $server_utcc        = $this->Rumus->getGMTOffset();
                        $intervall          = $user_utc - $server_utcc;
                        $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$datetrans);
                        $tanggaltransaksi->modify("+".$intervall ." minutes");
                        $tanggall           = $tanggaltransaksi->format('d-m-Y');    
                        $tanggaltransaksi   = $tanggaltransaksi->format('Y-m-d H:i:s');                    
                        $dateee             = $tanggaltransaksi;
                        $datetime           = new DateTime($dateee);
                        $datetime->modify('+1 hours');
                        $limitdateee        = $datetime->format('Y-m-d H:i:s');

                        $datelimit          = date_create($limitdateee);
                        $dateelimit         = date_format($datelimit, 'd/m/y');
                        $harilimit          = date_format($datelimit, 'd');
                        $hari               = date_format($datelimit, 'l');
                        $tahunlimit         = date_format($datelimit, 'Y');
                        $waktulimit         = date_format($datelimit, 'H:s');
                                                                
                        $datelimit          = $dateelimit;
                        $sepparatorlimit    = '/';
                        $partslimit         = explode($sepparatorlimit, $datelimit);
                        $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));                        
                    ?>
                    <div class="card">
                        <div class="card-header ch-alt text-center">
                            <h2>Menunggu Pembayaran</h2>
                        </div>
                        
                        <div class="card-body card-padding">     
                            <div class="clearfix"></div>
                            
                            <div class="row p-0 m-b-25">
                                <div class="col-xs-6">
                                    <div class="bgm-blue brd-2 p-15">
                                        <div class="c-white m-b-5">Invoice</div>
                                        <h2 class="m-0 c-white f-300"><?php echo $invoice_id;?></h2>
                                    </div>
                                </div>                        
                                <div class="col-xs-3">
                                    <div class="bgm-amber brd-2 p-15">
                                        <div class="c-white m-b-5">Tanggal</div>
                                        <h2 class="m-0 c-white f-300"><?php echo $tanggall;?></h2>
                                    </div>
                                </div>                                            
                                <div class="col-xs-3">
                                    <div class="bgm-green brd-2 p-15">
                                        <div class="c-white m-b-5">Total Bayar</div>
                                        <h2 class="m-0 c-white f-300">Rp <?php echo $hasiljumlah;?></h2>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="clearfix"><hr></div>
                            
                            <?php
                            if ($order_status == -2) {
                                ?>
                                <div class="row p-0">
                                    <div class="col-md-12 text-center">
                                        <h5 class="c-gray"><?php echo $this->lang->line('notification');?> :</h5>
                                        <h3 class="c-red"><?php echo $this->lang->line('notif_classexpired');?></h3>                                        
                                        <h5 class="c-red"><?php echo $this->lang->line('notif_classexpired2');?></h5>
                                    </div>
                                </div>
                                <?php
                            }
                            else
                            {
                                ?>
                                <div class="row p-0">
                                    <div class="col-md-2 pull-left m-r-20">
                                        <button class="btn btn-success btn-lg btn-block waves-effect f-14">                                
                                            <i class="zmdi zmdi-arrow-back"></i>
                                            <a href="<?php echo base_url();?>" class="c-white">Kelas Saya</a>
                                        </button>
                                    </div>
                                    <div class="col-md-3 pull-right m-r-5">
                                        <button class="btn btn-danger btn-lg btn-block waves-effect f-14" id="btnchoosemethod" data-price="<?php echo $hasiljumlah; ?>" data-order="<?php echo $order_id;?>">                                
                                            Pilih Metode Pembayaran
                                            <i class="zmdi zmdi-arrow-forward"></i>
                                        </button>
                                    </div>
                                </div>
                                <?php
                            }                            
                            ?>
                        </div>                                
                    </div>

                    <div class="card">
                        <div class="card-body card-padding">
                            <table class="table i-table m-t-25 m-b-25">
                                <thead class="text-uppercase">
                                    <th></th>
                                    <th width="120" style="text-align: center;">#</th>
                                    <th class="c-gray">Tutor</th>
                                    <th class="c-gray">Pelajaran</th>
                                    <th class="c-gray">Tanggal Kelas</th>
                                    <th class="highlight">Harga</th>
                                </thead>
                                
                                <tbody>
                                    <?php
                                        function limit_words($string, $word_limit){
                                            $words = explode(" ",$string);
                                            return implode(" ",array_splice($words,0,$word_limit));
                                        }
                                        $getdataorder = $this->db->query("SELECT tod.*, trm.class_id as class_id, tc.name as subject_name, tr.subject_id as tr_subject_id, tr.tutor_id as tr_tutor_id, tr.approve as tr_approve, tr.date_requested as tr_date_requested, tr.topic as tr_topic, trg.subject_id as trg_subject_id, trg.tutor_id as trg_tutor_id, trg.approve as trg_approve, trg.date_requested as trg_date_requested, trg.topic as trg_topic, tc.subject_id as trm_subject_id, tc.tutor_id as trm_tutor_id, trm.status as trm_approve, tc.start_time as trm_date_requested, tc.description as trm_topic,trp.status as trp_approve, tpp.description as trp_topic, trp.created_at as trp_date_requested FROM tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tc ON tc.class_id=trm.class_id ) ON trm.request_id=tod.product_id LEFT JOIN (tbl_request_program as trp INNER JOIN tbl_price_program as tpp ON trp.program_id=tpp.list_id) ON tod.product_id=trp.request_id WHERE tod.order_id='$order_id'")->result_array();

                                            $no = 1;
                                            foreach ($getdataorder as $row => $v) {
                                                $startimeclass  = $v['tr_date_requested'] != null ? $v['tr_date_requested'] : ( $v['trg_date_requested'] != null ? $v['trg_date_requested'] : ( $v['trm_date_requested'] != null ? $v['trm_date_requested'] : $v['trp_date_requested']));                                                
                                                $subject_id     = $v['tr_subject_id'] != null ? $v['tr_subject_id'] : ( $v['trg_subject_id'] != null ? $v['trg_subject_id'] : ( $v['trm_subject_id'] != null ? $v['trm_subject_id'] : ''));
                                                $topic          = $v['tr_topic'] != null ? $v['tr_topic'] : ( $v['trg_topic'] != null ? $v['trg_topic'] : ( $v['trm_topic'] != null ? $v['trm_topic'] : $v['trp_topic']));
                                                $tutor_id       = $v['tr_tutor_id'] != null ? $v['tr_tutor_id'] : ( $v['trg_tutor_id'] != null ? $v['trg_tutor_id'] : $v['trm_tutor_id']);

                                                if ($subject_id == '') {
                                                    $subject_name   = "";
                                                    $tutor_name     = "";
                                                    $tutor_image    = "";
                                                }
                                                else
                                                {
                                                    $subject_name       = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
                                                    $tutor_name         = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_name'];
                                                    $tutor_image        = $this->db->query("SELECT user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_image'];
                                                }                                                
                                                $waktumulai         = DateTime::createFromFormat ('Y-m-d H:i:s',$startimeclass);
                                                $waktumulai->modify("+".$intervall ." minutes");
                                                $waktumulai         = $waktumulai->format('d-m-Y H:i:s'); 
                                                $priceperclass      = $v['init_price'];  
                                                $priceperclass      = number_format($priceperclass, 0, ".", ".");
                                            ?>
                                            <thead>
                                                <tr>
                                                    <td width="1%"><?php echo $no;?></td>
                                                    <td width="7%"><center><img src="<?php echo CDN_URL.USER_IMAGE_CDN_URL.$tutor_image; ?>" style="height: 90%; width: 90%;"></center></td>
                                                    <td width="7%"><?php echo $tutor_name;?></td>
                                                    <td width="20%">
                                                        <h5 class="text-uppercase f-400"><?php echo $subject_name;?></h5>
                                                        <p class="text-muted"><?php echo limit_words($topic, 10);?></p>
                                                    </td>
                                                    
                                                    <td><?php echo $waktumulai;?></td>
                                                    <td class="highlight">Rp <?php echo $priceperclass;?></td>
                                                </tr>                                                                                                            
                                            </thead> 
                                            <?php
                                        $no++;
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="4"></td>
                                        <td>Kode Unik</td>
                                        <td class="highlight">Rp <?php echo $UnixCode;?></td>
                                    </tr>  
                                    <tr>
                                        <td colspan="4"></td>
                                        <td>Total Pembayaran</td>
                                        <td class="highlight">Rp <?php echo $hasiljumlah;?></td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <div class="clearfix"></div>
                                                        
                        </div>

                        <footer class="m-t-15 p-20">
                            <ul class="list-inline text-center list-unstyled">
                                <li class="m-l-5 m-r-5"><small>info@classmiles.com</small></li>
                                <li class="m-l-5 m-r-5"><small>+62-21-53660786</small></li>
                                <li class="m-l-5 m-r-5"><small>www.classmiles.com</small></li>
                            </ul>
                        </footer>
                    </div>
                    <?php 
                    }
                }
            ?>
            
        </div>

        <!-- Modal Pembayaran -->  
        <div class="modal fade in modalchoose" id="mdlchoosemethod" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" style="border-radius: 5%;">
                <div class="modal-content" style="background-color: rgba(248, 248, 247, 1);">
                    <div class="modal-header" style="background-color: #3498db;">                        
                        <h4 class="modal-title m-l-5 c-white pull-left">Pembayaran</h4>
                        <button type="button" class="close c-white" id="closechoosemethod">&times;</button>
                    </div>
                    <div>
                        <div class="modal-body m-t-15">
                            <div class="card-header bgm-white">
                                <p class="lead" style="color:#22313F; line-height: 1.42857143; font-family: open sans,sans-serif; font-size: 22px; padding-left: 17px; padding-top: 10px; padding-bottom: 10px;"><sup style="font-size: 12px;">Jumlah</sup><br>Rp. <label id="totaljumlah"></label></p>
                            </div>                
                        </div>
                        <input type="text" name="" id="orderidd" hidden>
                        <input type="text" name="" id="pilihan" hidden>
                    </div>
                    <div id="label">
                        <div class="modal-body">
                            <div class="card-header" style="background-color: rgba(248, 248, 247, 1);">
                                <p style="font-size: 15px;">Pilih metode pembayaran</p>
                            </div>
                        </div>
                    </div>
                    <div id="mandiri" data-bankid="008">
                        <div class="modal-body" style="padding: 0; margin: 0;">                            
                            <table class="table i-table waves-effect ci-avatar" style="cursor: pointer;">
                                <tbody>
                                    <td height="13">
                                        <img class="m-t-5" src="https://upload.wikimedia.org/wikipedia/en/thumb/f/fa/Bank_Mandiri_logo.svg/1280px-Bank_Mandiri_logo.svg.png" style="height: 27px; width: 80px;">
                                    </td>
                                    <td height="13" class="f-16" style="text-align: right;">
                                        <label class="m-t-15 pull-right" style="cursor: pointer;">Transfer Bank Mandiri</label>
                                    </td>
                                </tbody>
                            </table>
                        </div>
                    </div>  
                    <div id="paypal" data-bankid="000" style="margin-bottom: 15%;">
                        <div class="modal-body" style="padding: 0; margin: 0;">
                            <table class="table i-table waves-effect ci-avatar" style="cursor: pointer;">
                                <tbody>
                                    <td height="13">
                                        <img class="m-t-5" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL.'baru/PayPalLogo.png';?>" style="height: 40px; width: 80px;">
                                    </td>
                                    <td height="13" class="f-16" style="text-align: right;">
                                        <label class="m-t-15 pull-right" style="cursor: pointer;">Transfer Paypal</label>
                                    </td>
                                </tbody>
                            </table>
                        </div>
                    </div> 
                    <div id="rincianmandiri" style="display: none;">
                        <div class="modal-header" style="background-color: #ffffff; border-bottom: 1px solid #EEEEEE;">
                            <h4 class="modal-title m-l-5 c-black pull-left lead">Transfer Mandiri</h4> 
                            <label class="c-black pull-right" id="backmandiri"><img src="http://www.myiconfinder.com/uploads/iconsets/256-256-dc0fa3f38f18c9d1b4e2c99967401405-Arrow.png" style="width: 20px; height: 20px; cursor: pointer;" title="Kembali"></label>                           
                        </div>                        
                        <div class="modal-body bgm-white" style="margin: 0;"> 
                            <div class="p-t-10">
                                <div class="form-group fg-line">
                                    <label class="c-gray f-11" style="font-family: Helvetica;">No. Rekening Anda</label>
                                    <input type="text" name="norekening" id="norekening" maxlength="30" class="form-control f-14">
                                    <small class="m-t-5" style="color: #BFBFBF;">Masukan nomor rekening sesuai buku tabungan</small>
                                </div>                                
                                <div class="form-group fg-line">
                                    <label for="exampleInputEmail1" class="c-gray f-11" style="font-family: Helvetica;">Nama Pemilik Rekening</label>
                                    <input type="text" name="namarekening" id="namarekening" class="form-control f-14">
                                    <small class="m-t-5" style="color: #BFBFBF;">Masukan nama pemilik rekening sesuai buku tabungan</small>
                                </div>                                
                            </div>
                            <div class="p-l-5 p-r-5 p-t-15">
                                <ul>
                                    <li>Periksa kembali data pembayaran Anda sebelum melanjutkan transaksi.</li>
                                    <li>Harap bayar sesuai dengan jumlah yang tertera</li>
                                    <li>Melakukan transfer bank hanya tertuju atas nama PT Meetaza Prawira Media</li>
                                </ul>
                            </div>                            
                        </div>                        
                    </div>
                    <div id="rincianpaypal" style="display: none;">
                        <div class="modal-header" style="background-color: #ffffff; border-bottom: 1px solid #EEEEEE;">
                            <h4 class="modal-title m-l-5 c-black pull-left lead">Transfer Paypal</h4> 
                            <label class="c-black pull-right" id="backpaypal"><img src="http://www.myiconfinder.com/uploads/iconsets/256-256-dc0fa3f38f18c9d1b4e2c99967401405-Arrow.png" style="width: 20px; height: 20px; cursor: pointer;" title="Kembali"></label>                           
                        </div>                        
                        <div class="modal-body bgm-white" style="margin: 0;"> 
                            <div class="p-l-5 p-r-5 p-t-15">
                                <ul>
                                    <li>Periksa kembali data pembayaran Anda sebelum melanjutkan transaksi.</li>
                                    <li>Harap bayar sesuai dengan jumlah yang tertera</li>
                                    <li>Melakukan transfer bank hanya tertuju atas nama PT Meetaza Prawira Media</li>
                                </ul>
                            </div>                            
                        </div>                        
                    </div>    
                    <div class="modal-footer" style="margin: 0; display: none;" id="footerbutton">
                        <input type="text" id="txt_choosemethod" hidden>
                        <button type="button" class="btn btn-danger btn-lg waves-effect btn-block f-13" id="btnbayarr" data-orderid="" style="display: block;">Bayar</button>
                        <a id="lnk_click"><button type="button" class="btn btn-danger btn-lg waves-effect btn-block f-13" id="btnbayarpaypal" data-orderid="" style="display: none;">Bayar</button></a>
                    </div>                 
                
                </div>
            </div>
        </div>
        
        <!-- <button class="btn btn-float bgm-red m-btn" data-action="print"><i class="zmdi zmdi-print"></i></button> -->
    </section>

</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function(){
        
        var bankidpilihan = null;
        var userid = "<?php echo $this->session->userdata('id_user');?>";
        $("#norekening").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $(document).on("click", "#btnchoosemethod", function () {
        	var orderid 	= $(this).data('order');
            var price     = $(this).data('price');
        	$("#pilihan").val("");
        	$("#orderidd").val("");
            $("#totaljumlah").text("");
        	$('#orderidd').val(orderid);
            $('#totaljumlah').text(price);
            angular.element(document.getElementById('btnchoosemethod')).scope().loadings();
            setTimeout(function(){                
                angular.element(document.getElementById('btnchoosemethod')).scope().loadingsclose();
            },1000); 
            setTimeout(function(){
            	               
                $("#mdlchoosemethod").modal('show');                
            },1200);                  
        });

        $(document).on("click", "#mandiri", function () { 
            $("#txt_choosemethod").val('mandiri');
            $("#paypal").css('display','none');
            $("#mandiri").css('display','block');
            $("#btnbayarpaypal").css('display','none');
            $("#btnbayar").css('display','block');
            $("#footerbutton").css('display','block');
            bankidpilihan = $(this).data('bankid');
            $.ajax({
                url: '<?php echo base_url(); ?>master/get_rekuser',
                type: 'GET',
                data: {
                    bank_id : bankidpilihan
                },               
                success: function(data)
                { 
                    var sdata = JSON.parse(data);                    
                    if (sdata['status'] == 1) {
                        var nama_akun = sdata['nama_akun'];
                        var nomer_rekening = sdata['norek'];                        

                        $("#pilihan").val('bank_transfer');
                        $("#label").css('display','none');
                        $("#mandiri").css('display','none');
                        $("#rincianmandiri").css('display','block');
                        $("#footerbutton").css('display','block');
                        $("#btnbayarr").css('display','block');
                        $("#namarekening").val(nama_akun);
                        $("#norekening").val(nomer_rekening);
                    }
                    else
                    {
                        $("#pilihan").val('bank_transfer');
                        $("#label").css('display','none');
                        $("#mandiri").css('display','none');
                        $("#rincianmandiri").css('display','block');
                        $("#footerbutton").css('display','block');
                    }
                }
            });                    	
        });

        $(document).on("click", "#paypal", function () {
            $("#txt_choosemethod").val('paypal');
            $("#label").css('display','none');
            $("#mandiri").css('display','none');
            $("#rincianmandiri").css('display','none');
            $("#rincianpaypal").css('display','block');            
            $("#btnbayarr").css('display','none');
            $("#btnbayarpaypal").css('display','block');
            $("#footerbutton").css('display','block');
            var order_id        = $("#orderidd").val();           
            $('#lnk_click').attr('href','<?php echo BASE_URL();?>process/submitMethodPaymentPaypal/'+order_id);
        });

        $(document).on("click", "#backmandiri", function () {
            $("#footerbutton").css('display','none');
            $("#rincianmandiri").css('display','none');
            $("#label").css('display','block');
            $("#mandiri").css('display','block');  
            $("#paypal").css('display','block');              
            $("#btnbayarpaypal").css('display','none');
            $("#btnbayar").css('display','block');
        });

        $(document).on("click", "#backpaypal", function () {
            $("#footerbutton").css('display','none');
            $("#rincianpaypal").css('display','none');
            $("#btnbayarpaypal").css('display','none');
            $("#btnbayar").css('display','block');
            $("#label").css('display','block');
            $("#mandiri").css('display','block');
            $("#paypal").css('display','block');            
        });

        $(document).on("click", "#closechoosemethod", function () {
        	$("#footerbutton").css('display','none');
            $("#rincianmandiri").css('display','none');
            $("#btnbayarpaypal").css('display','none');
            $("#label").css('display','block');
            $("#mandiri").css('display','block');            
            $("#mdlchoosemethod").modal('hide');
        });

        $(document).on("click", "#btnbayarr", function () {
            var choosemethod    = $("#txt_choosemethod").val();
            var order_id        = $("#orderidd").val();
            var method          = $("#pilihan").val();         	
            var nomer_rekening  = $("#norekening").val();
            var nama_akun       = $("#namarekening").val();

            $('#mdlchoosemethod').modal('hide');   
            angular.element(document.getElementById('backmandiri')).scope().loadings();   
        	$.ajax({
                url: '<?php echo base_url(); ?>process/submitMethodPayment',
                type: 'POST',
                data: {
                	order_id : order_id,
                    payment_type: method,
                    bank_id : bankidpilihan,
                    nama_akun : nama_akun,
                    nomer_rekening : nomer_rekening                    
                },               
                success: function(data)
                {                     
                	var sdata = JSON.parse(data);
                	if (sdata['status'] == 1) {
                        angular.element(document.getElementById('backmandiri')).scope().loadingsclose();
                		window.location.href="<?php echo base_url();?>DetailPayment?order_id="+order_id;
                	}
                	else
                	{
                        angular.element(document.getElementById('backmandiri')).scope().loadingsclose();
                		notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',"Terjadi kesalahan, silahkan coba kembali");
                		setTimeout(function(){
                			location.reload();
                		},2000);                		
                	}
                }
            });
            
        });

    });
</script>