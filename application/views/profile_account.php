<!-- <header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #db1f00;"> -->
    <?php $this->load->view('inc/navbar'); ?>
<!-- </header> -->

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <?php if(isset($mes_alert)){ ?>
                <div class="alert alert-<?php echo $mes_alert; ?>" style="display: <?php echo $mes_display; ?>">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <?php echo $mes_message; ?>
                </div>
                <?php } ?>

            <div class="block-header">
                <h2><?php echo $this->lang->line('profil'); ?></h2>

                <!-- <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                            <li><?php echo $this->lang->line('profil'); ?></li>
                            <li class="active"><?php echo $this->lang->line('tab_account'); ?></li>
                        </ol>
                    </li>
                </ul> -->
            </div><!-- akhir block header -->            
            <div class="bs-item z-depth-5" style="margin-top: 5%;">
                <div class="card" id="profile-main">

                    <?php
                        $this->load->view('inc/sideprofile');
                    ?>

                    <div class="pm-body clearfix">
                        <ul class="tab-nav tn-justified" role="tablist">
                            <li class="waves-effect"><a href="<?php echo base_url('About'); ?>"><?php echo $this->lang->line('tab_about'); ?></a></li>
                            <li class="active  waves-effect"><a href="<?php echo base_url('Profile-Account'); ?>"><?php echo $this->lang->line('tab_account'); ?></a></li> 
                            <li class="waves-effect"><a href="<?php echo base_url('Profile'); ?>"><?php echo $this->lang->line('tab_pr'); ?></a></li> 
                            <li id="tab_school" class="waves-effect"><a href="<?php echo base_url('Profile-School'); ?>"><?php echo $this->lang->line('tab_ps'); ?></a></li>
                            <!-- <li class="waves-effect"><a href="<?php echo base_url('first/profile_forgot'); ?>"><?php echo $this->lang->line('tab_cp'); ?></a></li> -->                                      
                        </ul>                                                                                   

                        <br>
                        <div class="card-padding card-body">
                              
                            <form action="<?php echo base_url('/master/saveEditAccountStudent') ?>" method="post">
                                <div class="col-sm-12">

                                    <blockquote class="m-b-25">
                                        <p><?php echo $this->lang->line('tab_account'); ?></p>
                                    </blockquote>
                                    <br>

                                    <input name="id_user" type="hidden" value="<?php if(isset($alluser)){ echo $alluser['id_user']; } ?>">
                                	
                                    <div class="input-group fg-float">
                                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                        <div class="fg-line">
                                            <input type="text" name="first_name" required class="input-sm form-control fg-input" value="<?php if(isset($alluser)){ echo $alluser['first_name']; } ?>">
                                            <label class="fg-label"><?php echo $this->lang->line('first_name'); ?></label>
                                        </div>                                    
                                    </div>

                                    <br/>
                                    <div class="input-group fg-float">
                                        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                                        <div class="fg-line">
                                            <input type="text" name="last_name" required class="input-sm form-control fg-input" value="<?php if(isset($alluser)){ echo $alluser['last_name']; } ?>">
                                            <label class="fg-label"><?php echo $this->lang->line('last_name'); ?></label>
                                        </div>                                    
                                    </div>

                                    <br>

                                    <div class="input-group fg-float">
                                        <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                                        <div class="fg-line">
                                            <input type="text" name="email" readonly required class="form-control" value="<?php if(isset($alluser)){ echo $alluser['email']; } ?>" style="cursor:not-allowed;" placeholder="<?php echo $this->lang->line('email_address'); ?>">
                                            <label class="fg-label">Email</label>
                                        </div>
                                    </div>                                                                                        

                                    <br/>
                                    <label class="fg-label" style="margin-left: 5%;"><small><?php echo $this->lang->line('datebirth'); ?></small></label>
                                    <div class="input-group fg-float">
                                    	<span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                    	<div class="col-sm-4 m-t-10">
                                    		<div class="fg-line">
                                    			<select name="tanggal_lahir" id="tanggal_lahir" class="select2 form-control">
                                                        <option disabled selected><?php echo $this->lang->line('datebirth'); ?></option>
                                                        <?php 
                                                        if(isset($alluser)){
                                                            $tgl = substr($alluser['user_birthdate'], 8,2);
                                                            $bln = substr($alluser['user_birthdate'], 5,2);
                                                            $thn = substr($alluser['user_birthdate'], 0,4);
                                                        }

                                                            for ($date=01; $date <= 31; $date++) {
                                                                echo '<option value="'.$date.'" ';
                                                                if(isset($alluser) && $tgl == $date){ echo "selected='true'";}
                                                                echo '>'.$date.'</option>';
                                                            }
                                                        ?>  
                                                    </select>
                                    		</div>
                                    	</div>
                                    	<div class="col-sm-4 m-t-10">
                                    		<div class="fg-line">
                                    			<select name="bulan_lahir" class="select2 form-control">
                                                        <option disabled selected><?php echo $this->lang->line('monthbirth'); ?></option>
                                                        <option value="01" <?php if(isset($alluser) && $bln == "01"){ echo "selected='true'";} ?>><?php echo $this->lang->line('jan'); ?></option>
                                                        <option value="02" <?php if(isset($alluser) && $bln == "02"){ echo "selected='true'";} ?>><?php echo $this->lang->line('feb'); ?></option>
                                                        <option value="03" <?php if(isset($alluser) && $bln == "03"){ echo "selected='true'";} ?>><?php echo $this->lang->line('mar'); ?></option>
                                                        <option value="04" <?php if(isset($alluser) && $bln == "04"){ echo "selected='true'";} ?>><?php echo $this->lang->line('apr'); ?></option>
                                                        <option value="05" <?php if(isset($alluser) && $bln == "05"){ echo "selected='true'";} ?>><?php echo $this->lang->line('mei'); ?></option>
                                                        <option value="06" <?php if(isset($alluser) && $bln == "06"){ echo "selected='true'";} ?>><?php echo $this->lang->line('jun'); ?></option>
                                                        <option value="07" <?php if(isset($alluser) && $bln == "07"){ echo "selected='true'";} ?>><?php echo $this->lang->line('jul'); ?></option>
                                                        <option value="08" <?php if(isset($alluser) && $bln == "08"){ echo "selected='true'";} ?>><?php echo $this->lang->line('ags'); ?></option>
                                                        <option value="09" <?php if(isset($alluser) && $bln == "09"){ echo "selected='true'";} ?>><?php echo $this->lang->line('sep'); ?></option>
                                                        <option value="10" <?php if(isset($alluser) && $bln == "10"){ echo "selected='true'";} ?>><?php echo $this->lang->line('okt'); ?></option>
                                                        <option value="11" <?php if(isset($alluser) && $bln == "11"){ echo "selected='true'";} ?>><?php echo $this->lang->line('nov'); ?></option>
                                                        <option value="12" <?php if(isset($alluser) && $bln == "12"){ echo "selected='true'";} ?>><?php echo $this->lang->line('des'); ?></option>  
                                                </select> 
                                    		</div>
                                    		<!-- <span class="zmdi zmdi-triangle-down form-control-feedback"></span> -->
                                    	</div>
                                    	<div class="col-sm-4 m-t-10">                                	
                                    		<div class="fg-line">
                                    			<select name="tahun_lahir" id="tahun_lahir" class="select2 form-control">
                                                    <option disabled selected><?php echo $this->lang->line('yearbirth'); ?></option>
                                                    <?php 
                                                        for ($i=1960; $i <= 2016; $i++) {                                                                            
                                                            echo '<option value="'.$i.'" ';
                                                            if(isset($alluser) && $thn == $i){ echo "selected='true'";}
                                                            echo '>'.$i.'</option>';
                                                        } 
                                                    ?>
                                                </select>

                                    		</div>
                                    	</div>
                                    </div>
                                    
                                    <br>
                                    
                                    <div class="input-group fg-float m-t-10">
                                        <span class="input-group-addon"><i class="zmdi zmdi-phone"></i></span>
                                        <div class="col-md-3">                                        
                                            <div class="fg-line">                                           
                                                <!-- <select required name="kode_area" class="select2 form-control">
                                                    <option disabled selected value=''><?php echo $this->lang->line('countrycode'); ?></option>
                                                    <option value="+62" <?php if($ambildepan == "+62"){ echo "selected='true'";} ?>>Indonesia +62</option>
                                                    <option value="+60" <?php if($ambildepan == "+60"){ echo "selected='true'";} ?>>Malaysia +60</option>
                                                    <option value="+65" <?php if($ambildepan == "+65"){ echo "selected='true'";} ?>>Singapore +65</option>
                                                </select> -->
                                                <select required name="kode_area" class="select2 form-control">
                                                
                                                <?php
                                                    $allsub = $this->db->query("SELECT nicename,phonecode FROM master_country ORDER BY id ASC")->result_array();
                                                    foreach ($allsub as $row => $d) {
                                                        if($d['phonecode'] == $alluser['user_countrycode']){
                                                            echo "<option value='".$d['phonecode']."' selected='true'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                        }
                                                        if ($d['phonecode'] != $alluser['user_countrycode']) {
                                                            echo "<option value='".$d['phonecode']."'>".$d['nicename']." +".$d['phonecode']."</option>";
                                                        }                                                            
                                                    }
                                                ?> 
                                                </select>
                                                <label class="fg-label"><?php echo $this->lang->line('countrycode'); ?></label>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="fg-line">
                                                <input type="text" id="user_callnum" name="user_callnum" minlength="9" maxlength="13" onmousemove="validPhone(this)" onkeyup="validPhone(this)"  onkeyup="validAngka(this)" onkeypress="return hanyaAngka(event)" required class="input-sm form-control fg-input" value="<?php echo $alluser['user_callnum'];?>">
                                                <label class="fg-label"><?php echo $this->lang->line('mobile_phone'); ?></label>
                                            </div>
                                        </div>                            
                                    </div>

                                    <br><br>
                                    <blockquote class="m-b-25">
                                        <p><?php echo $this->lang->line('tab_cp'); ?></p>
                                    </blockquote>
                                    
                                    <div class="input-group fg-float">
                                        <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                                        <div class="fg-line">
                                            <input type="password" name="oldpass" class="input-sm form-control fg-input">
                                            <label class="fg-label"><?php echo $this->lang->line('old_pass'); ?></label>
                                        </div>                                    
                                    </div>

                                    <br>

                                    <div class="input-group fg-float">
                                        <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                                        <div class="fg-line">
                                            <input type="password" name="newpass" id="newpass" class="input-sm form-control fg-input">
                                            <label class="fg-label"><?php echo $this->lang->line('new_pass'); ?></label>
                                        </div>                                    
                                    </div>
                                    
                                    <br>    

                                    <div class="input-group fg-float">
                                        <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
                                        <div class="fg-line">
                                            <input type="password" name="conpass" id="conpass" class="input-sm form-control fg-input" onChange="checkPasswordMatch();">
                                            <label class="fg-label"><?php echo $this->lang->line('confirm_pass'); ?></label>
                                        </div>
                                        <small class="help-block" id="divCheckPasswordMatch"></small>                                    
                                    </div>                                

                                    <br>                                
                                                                                                                                                                 
                                    <br><br>
                                </div>                                                    
                                <div class="card-padding card-body">                                                                            
                                    <button type="submit" name="simpanedit" id="simpanedit" class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); ?></button>                                
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>

        </div><!-- akhir container -->
    </section>            
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<!-- Page Loader -->

<script type="text/javascript">
    function checkPasswordMatch() {
        var password = $("#newpass").val();
        var confirmPassword = $("#conpass").val();

        if (password != confirmPassword)
        {
            $("#divCheckPasswordMatch").html("<p style='color:red'><?php echo $this->lang->line('passwordnotmatch'); ?></p>");   
            $('#simpanedit').attr('disabled','');
        }
        else
        {
            $("#divCheckPasswordMatch").html("<p style='color:green'></p>");
            $('#simpanedit').removeAttr('disabled');
        }       
    }

    $(document).ready(function () {
       $("#conpass").keyup(checkPasswordMatch);
    });
</script>
<script type="text/javascript">
        $(document).ready(function(){
            var code_cek = null;
            var status_login = "<?php echo $this->session->userdata('status_user');?>";
            var cekk = setInterval(function(){
            // console.warn(status_login);
                
                cek();
            },500);
            // alert('aasdfsd');
            function cek(){
                if (status_login == "umum") {
                    $("#tab_school").css('display','none');
                }
            }

            
        });
</script>
<script>
    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }
    function validAngka(a)
    {
        if(!/^[0-9.]+$/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
      function validPhone(a)
    {
        if(!/(^0$)|(^[1-9]\d{0,12}$)/.test(a.value))
        {
        a.value = a.value.substring(0,a.value.length-31);
        }
    }
</script>