
<div class="site_preloader flex_center">
    <div class="site_preloader_inner">
      <div class="rect1"></div>
      <div class="rect2"></div>
      <div class="rect3"></div>
      <div class="rect4"></div>
      <div class="rect5"></div>
    </div>
</div>

<div class="styler">
    <div class="single_styler clearfix sticky_header">
        <h4>Header style</h4>
        <select class="wide">
            <option value="static">Enable header top</option>
            <option value="diseable_ht">Diseable header top</option>
        </select>
    </div>
    <div class="single_styler primary_color clearfix">
        <h4>Primary Color</h4>
        <div class="clearfix">
            <input id="primary_clr" type="text" class="form-control color_input" value="#f9bf3b" />
            <label class="form-control" for="primary_clr"><i class="fa fa-angle-down"></i></label>
        </div>
    </div>
    <div class="single_styler secound_color clearfix">
        <h4>Secoundary Color</h4>
        <div class="clearfix">
            <input id="secound_clr" type="text" class="form-control color_input" value="#19b5fe" />
            <label class="form-control" for="secound_clr"><i class="fa fa-angle-down"></i></label>
        </div>
    </div>
    <div class="single_styler layout_mode clearfix">
        <h4>Layout Mode</h4>
        <select class="wide">
            <option value="wide">Wide</option>
            <option value="boxed">Boxed</option>
            <option value="wide_box">Wide Boxed</option>
        </select>
    </div>
    <div class="layout_mode_dep">
        <div class="single_styler body_bg_style clearfix">
            <h4>Body Background style</h4>
            <select class="wide">
                <option value="img_bg">Image Background</option>
                <option value="solid">Solid Color</option>
            </select>
        </div>
        <div class="single_styler body_solid_bg clearfix">
            <h4>Select Background Color</h4>
            <div class="clearfix">
                <input id="body_bg_clr" type="text" class="form-control color_input" value="#d3d3d3" />
                <label class="form-control" for="body_bg_clr"><i class="fa fa-angle-down"></i></label>
            </div>
        </div>
        <div class="single_styler body_bg_img clearfix">
            <h4>Select Background Image</h4>
            <div class="clearfix">
                <div class="single_bg" data-bg="assets/img/styler/07.jpg"></div>
                <div class="single_bg" data-bg="assets/img/styler/08.jpg"></div>
                <div class="single_bg" data-bg="assets/img/styler/1.png"></div>
                <div class="single_bg" data-bg="assets/img/styler/2.png"></div>
                <div class="single_bg" data-bg="assets/img/styler/3.png"></div>
                <div class="single_bg" data-bg="assets/img/styler/4.png"></div>
                <div class="single_bg" data-bg="assets/img/styler/5.png"></div>
                <div class="single_bg" data-bg="assets/img/styler/6.png"></div>
            </div>
        </div>
    </div>
</div>
   
<div class="main_wrap">
   
    <!-- 01. header_area -->
    <header class="header_area">
        <div class="header_top sbb">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <span class="header_info"> The Rote Less Travelled | Singapore</span>
                    </div>
                    <div class="col-md-8">
                        <ul class="header_link">                            
                            <li><i class="fa fa-phone"></i><a href="#">(65) 81035356</a></li>
                            <li><i class="fa fa-clock-o"></i><span>Mon - Fri, 10am - 8pm</span></li>
                            <li><a href=""> Login </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="sticky-anchor"></div>
        <div class="header_btm">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-xs-6">
                        <a href="index.html" class="logo">
                            <img src="http://rlt.com.sg/wp-content/uploads/2015/07/rlt-logo-black-margin@2x.png" style="height: 30%; width: 30%; margin-top: -2%;">
                        </a>
                    </div>
                    <div class="col-sm-9 menu_col col-xs-6">
                        <nav class="menu-container">
                            <ul class="menu">
                                <li class="current"><a href="index.html">Home</a></li>
                                <li><a href="about.html">About Us</a>
                                    <ul class="sub-menu">
                                        <li><a href="about.html">Our Story</a></li>
                                        <li><a href="jobs.html">Jobs</a></li>
                                    </ul>
                                </li>
                                <li><a href="service.html">Services</a>
                                    <ul class="sub-menu">
                                        <li><a href="single-service-1.html">Business Consultaion</a></li>
                                        <li><a href="single-service-2.html">Fund Rising</a></li>
                                        <li><a href="single-service-3.html">Startup Consultation</a></li>
                                        <li><a href="single-service-4.html">Finance Analytics</a></li>
                                        <li><a href="single-service-5.html">UX Consultation</a></li>
                                        <li><a href="single-service-6.html">Web Development</a></li>
                                        <li><a href="single-service-7.html">Business Management</a></li>
                                        <li><a href="single-service-8.html">Digital Marketing</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Pages</a>
                                    <ul class="sub-menu mega-menu">
                                        <li class="menu-column">
                                            <div><a href="index.html">Home</a></div>
                                            <div><a href="about.html">About Us</a></div>
                                            <br>
                                            <div><a href="jobs.html">Jobs</a></div>
                                            <div><a href="get-quote.html">Get A Quote</a></div>
                                            <div><a href="service.html">Our Service</a></div>
                                            <div><a href="single-service.html">Service Details</a></div>
                                        </li>
                                        <li class="menu-column">
                                            <div><a href="works-two-column.html">work / portfolio (style - 1)</a></div>
                                            <div><a href="works-three-column.html">work (style - 2)</a></div>
                                            <div><a href="works-four-column.html">work (style - 3)</a></div>
                                            <div><a href="single-work.html">work Details (Single Portfolio)</a></div>
                                            <br>
                                            <div><a href="blog-two-column.html">Blog (Style - 1)</a></div>
                                            <div><a href="blog-one-column.html">Blog (Style - 2)</a></div>
                                        </li>
                                        <li class="menu-column">
                                            <div><a href="blog-left-sidebar.html">Blog (Style - 3)</a></div>
                                            <div><a href="blog-full-width.html">Blog (Style - 4)</a></div>
                                            <div><a href="blog-single.html">Blog Details (Single Blog)</a></div>
                                            <br>
                                            <div><a href="testimonial.html">Testimonial</a></div>
                                            <div><a href="contact.html">Contact</a></div>
                                            <div><a href="404.html">404/Error Page</a></div>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="blog-two-column.html">Blog</a>
                                    <ul class="sub-menu">
                                        <li><a href="blog-one-column.html">Blog (1 Column)</a></li>
                                        <li><a href="blog-two-column.html">Blog (2 Column)</a></li>
                                        <li><a href="blog-left-sidebar.html">Blog (Left Sidebar)</a></li>
                                        <li><a href="blog-full-width.html">Blog (Full Width)</a></li>
                                        <li><a href="blog-single.html">Blog Details (Single)</a></li>
                                    </ul>
                                </li>
                                
                                <li><a href="#">Dropdown</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">Level A-1</a></li>
                                        <li><a href="#">Level A-2</a></li>
                                        <li><a href="#">Level A-3</a>
                                            <ul class="sub-menu">
                                                <li><a href="#">Level B-1</a></li>
                                                <li><a href="#">Level B-2</a></li>
                                                <li><a href="#">Level B-3</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="#">Level C-1</a></li>
                                                        <li><a href="#">Level C-2</a></li>
                                                        <li><a href="#">Level C-3</a></li>
                                                        <li><a href="#">Level C-4</a></li>
                                                        <li><a href="#">Level C-5</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#">Level B-4</a></li>
                                                <li><a href="#">Level B-5</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Level A-4</a></li>
                                        <li><a href="#">Level A-5</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.html">Contact</a></li>
                            </ul>
                        </nav>
                        <div class="search_icon">
                            <i class="fa fa-search"></i>
                        </div>
                        <div class="search_form">
                            <form action="#">
                               <div class="search_close">
                                   <i class="fa fa-close"></i>
                               </div>
                                <input type="search" placeholder="Type any text to search...">
                                <button type="submit">Search</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- 01. /header_area -->
    
    <!-- 02. home_area -->
    <div class="home_area">
        <div class="slider_preloader flex_center">
           <div class="slider_preloader_inner"></div>
        </div>
        <div class="home_slider">
            <div class="single_slide overlay" style="background-image: url('http://rlt.com.sg/wp-content/uploads/2015/11/rlt-kwik-edgeslider-new.jpg'">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="home_content">
                                <div class="cell">
                                    <h1>We help you to <br>grow your business.</h1>
                                    <p>We always try to bring the beautiful smile of our <br>customers with our awesome works.</p>
                                    <a href="#" class="button hvr-bounce-to-right home_btn">Get a free consultation</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single_slide overlay" style="background-image: url('http://rlt.com.sg/wp-content/uploads/2015/09/header-coaching-new.jpg')">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="home_content">
                                <div class="cell">
                                    <h1>Expert advisors are waiting for you.</h1>
                                    <p>We always try to bring the beautiful smile of our <br>customers with our awesome works.</p>
                                    <a href="#" class="button hvr-bounce-to-right home_btn">Get a free consultation</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 02. /home_area -->
    
    
    <!-- 03. cta_area -->
    <div class="cta_area wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="cta">
                        <h2>Super service with the best quality works.</h2>
                    </div>
                </div>
                <div class="col-md-3 text-right">
                    <a href="#" class="button hvr-bounce-to-right pbg"><i class="fa fa-long-arrow-right"></i> Contact us today</a>
                </div>
            </div>
        </div>
    </div>
    <!-- 03. /cta_area -->
    
    <!-- 04. service_area -->
    <div class="service_area sp sbb">
        <div class="container">
            <div class="row  section_title wow fadeInUp">
                <div class="col-md-6 sth">
                    <h3>We don’t compromise with quality. <br> Best services are guaranteed for you.</h3>
                </div>
                <div class="col-md-6 sth flex_center">
                    <span class="section_btn text-right">
                        <a href="#" class="button-2">Check all services <i class="fa fa-caret-right"></i></a>
                    </span>
                </div>
            </div>
            <div class="row service">
                <div class="col-md-3 col-sm-6 single_service wow fadeInUp">
                    <div>
                        <div class="service_img">
                            <img src="assets/img/03.jpg" alt="">
                        </div>
                        <div class="service_content">
                            <h4>Business Consultaion</h4>
                            <p>Over fact all son tell this any his. Insisted  confined of weddings to returned debating  rendered. Keeps order fully so do party.</p>
                            <a href="single-service-1.html" class="button-2">See details <i class="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 single_service wow fadeInUp">
                    <div>
                        <div class="service_img">
                            <img src="assets/img/04.jpg" alt="">
                        </div>
                        <div class="service_content">
                            <h4>Fund Rising</h4>
                            <p>Over fact all son tell this any his. Insisted  confined of weddings to returned debating  rendered. Keeps order fully so do party.</p>
                            <a href="single-service-2.html" class="button-2">See details <i class="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 single_service wow fadeInUp">
                    <div>
                        <div class="service_img">
                            <img src="assets/img/05.jpg" alt="">
                        </div>
                        <div class="service_content">
                            <h4>Startup Consultation</h4>
                            <p>Over fact all son tell this any his. Insisted  confined of weddings to returned debating  rendered. Keeps order fully so do party.</p>
                            <a href="single-service-3.html" class="button-2">See details <i class="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 single_service wow fadeInUp">
                    <div>
                        <div class="service_img">
                            <img src="assets/img/06.jpg" alt="">
                        </div>
                        <div class="service_content">
                            <h4>Finance Analytics</h4>
                            <p>Over fact all son tell this any his. Insisted  confined of weddings to returned debating  rendered. Keeps order fully so do party.</p>
                            <a href="single-service-4.html" class="button-2">See details <i class="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 04. /service_area -->
    
    <!-- 05. about_area -->
    <div class="about_area sp">
        <div class="container">
            <div class="row about">
                <div class="single_about col-sm-6 wow fadeInUp">
                    <div>
                        <div class="about_img abh">
                            <img src="assets/img/07.jpg" alt="">
                        </div>
                        <div class="about_content abh">
                            <h4>About our company</h4>
                            <p>It sportsman earnestly ye preserved an on. Moment led family sooner cannot her window  pulled any. Or raillery if improved landlord to speaking hastened differed one for history.</p>
                            <div>
                                <a href="about.html" class="button-2">Discover more <i class="fa fa-caret-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="single_about col-sm-6 wow fadeInUp" data-wow-delay=".2s">
                    <div>
                        <div class="about_img abh">
                            <img src="assets/img/08.jpg" alt="">
                        </div>
                        <div class="about_content abh">
                            <h4>About our histry</h4>
                            <p>It sportsman earnestly ye preserved an on. Moment led family sooner cannot her window  pulled any. Or raillery if improved landlord to speaking hastened differed one for history.</p>
                            <div>
                                <a href="about.html" class="button-2">Discover more <i class="fa fa-caret-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 05. /about_area -->
    
    <!-- 06. cta_area2 -->
    <div class="cta_area2 overlay-2 wow fadeInUp" style="background-image: url(assets/img/09.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h1>We provide the best service for any business</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- 06. /cta_area2 -->
    
    <!-- 07. step_area -->
    <div class="step_area sp"> 
        <div class="container">
            <div class="row  section_title">
                <div class="col-md-6">
                    <h3>Many service providers are there. <br> But why you should choose us?</h3>
                </div>
            </div>
            <div class="row step">
                <div class="single_step col-sm-4 wow fadeInUp">
                    <div>
                        <h3>Creative</h3>
                        <p>Shade being under his bed her. Much read on as draw. Blessing for ignorant exercise any yourself unpacked.</p>
                    </div>
                </div>
                <div class="single_step col-sm-4 wow fadeInUp" data-wow-delay=".2s">
                    <div>
                        <h3>Innovative</h3>
                        <p>Noise is round to in it quick timed doors.  Written address greatly get attacks inhabit  pursuit of things.</p>
                    </div>
                </div>
                <div class="single_step col-sm-4 wow fadeInUp" data-wow-delay=".4s">
                    <div>
                        <h3>Fast</h3>
                        <p>Effects present letters inquiry no removed or friends. Desire behind latter me though in. Supposing fastest act.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 07. /step_area -->
    
    <!-- 08. video_area -->
    <div class="video_area sp grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-6 vdo_h flex_center wow zoomIn" data-wow-duration=".5s"  data-wow-delay=".4s">
                    <div class="video_bg vdo_h overlay-3" style="background-image: url(assets/img/10.jpg)">
                        <span>
                            <a class="video_icon_wrap videoBtn" href="http://www.youtube.com/watch?v=0O2aH4XLbto"> <i class="fa fa-play video_icon"></i> <span>Watch our video</span> </a>
                        </span>
                    </div>
                </div>
                <div class="col-md-6 vdo_h flex_center wow fadeInUp">
                    <div class="benifit video_dsc">
                        <h3>Benefits of working with us</h3>
                        <p>Arrived compass prepare an on as. Reasonable particular on my it in  sympathize. Size now easy eat hand how. Unwilling he departure elsewhere dejection at. Heart large seems may purse means few blind. Exquisite newspaper attending on certainty oh suspicion of. He less do quit evil is.We help companies at every stage of growth, developing custom solutions.</p>
                        <ul class="benifit_list">
                            <li>We help align your brand strategy with key objectives.</li>
                            <li>We help our customers to build better business for future.</li>
                            <li>We design best working path for startups.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 08. /video_area -->
    
    <!-- 09. faq_area -->
    <div class="faq_area sp">
        <div class="container">
            <div class="col-md-6 faq wow fadeInUp">
                <h3>Frequently Asked Question</h3>
                <div class="accordion_wrap">
                    <dl class="accordion style2">
                        <dt class="active">What type of businsess consultation do you give?</dt>
                        <dd>My possible peculiar together to. Desire so better am cannot he up before points. Remember mistaken opinions it pleasure of debating.</dd>
                        <dt>How does the process work for our business?</dt>
                        <dd>My possible peculiar together to. Desire so better am cannot he up before points. Remember mistaken opinions it pleasure of debating.</dd>
                        <dt>What type of financial limit should I set before starting?</dt>
                        <dd>My possible peculiar together to. Desire so better am cannot he up before points. Remember mistaken opinions it pleasure of debating.</dd>
                    </dl>
                </div>
            </div>
            <div class="col-md-6 according  wow fadeInUp" data-wow-delay=".2s">
                <h3>We are doing smart services</h3>
                <p>Preserved defective offending he daughters on or. Rejoiced prospect yet material  servants out answered men admitted. Sportsmen certainty prevailed suspected am as. Add stairs admire all answer the nearer yet length.</p>
                <div class="panel with-nav-tabs panel-default">
                    <div class="panel-heading">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab1default" data-toggle="tab">Financial Advantage</a></li>
                                <li><a href="#tab2default" data-toggle="tab">Business Consultancy</a></li>
                                <li><a href="#tab3default" data-toggle="tab">Startup Problems</a></li>
                            </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1default">
                                <p>Going as by do known noise he wrote round leave. Warmly put branch people  narrow see. Winding its waiting yet parlors married own feeling. Marry fruit do  spite jokes an times. Whether at it unknown warrant herself winding if. Him same  none name sake had post love. An busy feel form.</p>
                            </div>
                            <div class="tab-pane fade" id="tab2default">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea inventore modi reprehenderit sapiente, similique dolore possimus eos qui delectus repellat excepturi quod est accusantium esse neque expedita tenetur error. Eligendi laborum ea, nesciunt repudiandae qui vitae perspiciatis porro autem vel.</p>
                            </div>
                            <div class="tab-pane fade" id="tab3default">
                                <p> Minus maxime molestiae, suscipit pariatur corporis, accusantium facere hic officia fuga aut nisi odio ut cupiditate exercitationem necessitatibus quibusdam non tempore alias. Eos dicta eum illum repellat provident necessitatibus culpa molestias.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 09. /faq_area -->
    
    <!-- 10. form_area -->
    <div class="form_area overlay not_sp" style="background-image: url(assets/img/11.jpg)" data-stellar-background-ratio="0.2">
        <div class="container">
            <div class="row">
                <div class="col-md-6 form_h fadeInLeft wow">
                    <div class="form_text text-center">
                        <h4 class="cPrimary">Get a free consultation</h4>
                        <h1>We design the best path to  get a successful business which  can make you profitable.</h1>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-1 form_h">
                    <div class="contact_form fadeInRight wow">
                        <form action="#">
                            <div class="form-group clearfix">
                                <p>Your name*</p>
                                <input type="text" placeholder="Enter your full name">
                            </div>
                            <div class="form-group clearfix">
                                <p>Email address*</p>
                                <input type="email" placeholder="eg: john@email.com">
                            </div>
                            <div class="form-group clearfix">
                                <p>Phone number</p>
                                <input type="text" placeholder="eg: (+33) 382 437">
                            </div>
                            <div class="form-group clearfix">
                                <p>I would like to discuss</p>
                                <select class="wide">
                                  <option data-display="Select">Starting a startup</option>
                                  <option value="1">Some option</option>
                                  <option value="2">Another option</option>
                                  <option value="3" disabled>A disabled option</option>
                                  <option value="4">Potato</option>
                                </select>
                            </div>
                            <div class="form-group clearfix">
                                <button type="submit" class="button hvr-bounce-to-right"><i class="fa fa-long-arrow-right"></i> Get a free consultaion</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 10. /form_area -->
    
    <!-- 11. testimonial_area -->
    <div class="testimonial_area sp wow fadeInUp">
        <div class="container">
            <div class="row  section_title">
                <div class="col-md-6 sth">
                    <h3>We take care of all our customers. <br>See what our customers say about us.</h3>
                </div>
            </div>
            <div class="tst_slider">
                <div class="single_slide">
                    <p>“Looking started he up perhaps against. How  remainder all additions get elsewhere resources.  One missed shy wishes supply design answer  formed. Prevent on present hastily passage an  subject in be.”</p>
                    <h4>Farhan Rizvi</h4>
                    <span class="position">
                        Creative Director, Dhrubok
                    </span>
                </div>
                <div class="single_slide">
                    <p>“Settled opinion how enjoyed greater joy adapted too shy. Now properly surprise expenses interest nor  replying she she. Bore tall nay many many time yet less. Doubtful for answered one fat indulged marga sir shutters together.”</p>
                    <h4>Bushra Ahsani</h4>
                    <span class="position">
                        CEO, Home Service
                    </span>
                </div>
                <div class="single_slide">
                    <p>“Looking started he up perhaps against. How  remainder all additions get elsewhere resources.  One missed shy wishes supply design answer  formed. Prevent on present hastily passage an  subject in be.”</p>
                    <h4>David Ramon</h4>
                    <span class="position">
                        Managing DIrector, Start One Ltd.
                    </span>
                </div>
                <div class="single_slide">
                    <p>“Settled opinion how enjoyed greater joy adapted too shy. Now properly surprise expenses interest nor  replying she she. Bore tall nay many many time yet less. Doubtful for answered one fat indulged marga sir shutters together.”</p>
                    <h4>Ohidul Islam</h4>
                    <span class="position">
                        Developer, Dhrubok
                    </span>
                </div>
            </div>
        </div>
    </div>
    <!-- 11. /testimonial_area -->
    
    <!-- 12. brand_area -->
    <div class="brand_area grey-bg sp wow fadeInUp">
        <div class="container">
           <div class="row">
               <div class="col-md-12 text-center">
                   <h4>We featured on</h4>
               </div>
           </div>
            <div class="brand_slider">
                <a title="http://example.com" href="#" class="single_brand single_slide">
                    <img src="assets/img/12.jpg" alt="">
                </a>
                <a title="http://example.com" href="#" class="single_brand single_slide">
                    <img src="assets/img/13.jpg" alt="">
                </a>
                <a title="http://example.com" href="#" class="single_brand single_slide">
                    <img src="assets/img/14.jpg" alt="">
                </a>
                <a title="http://example.com" href="#" class="single_brand single_slide">
                    <img src="assets/img/15.jpg" alt="">
                </a>
                <a title="http://example.com" href="#" class="single_brand single_slide">
                    <img src="assets/img/16.jpg" alt="">
                </a>
                <a title="http://example.com" href="#" class="single_brand single_slide">
                    <img src="assets/img/17.jpg" alt="">
                </a>
            </div>
        </div>
    </div>
    <!-- 12. /brand_area -->
    
    <!-- 13. news_area -->
    <div class="news_area sp">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 single_news wow fadeInUp">
                    <span>
                        <a href="#" class="news_img">
                            <img src="assets/img/18.jpg" alt="">
                        </a>
                        <span class="news_content">
                            <span class="entry-date">
                                25th June, 2016
                            </span>
                            <a class="h3" href="#">How I started my startup with only $100 and got success.</a>
                        </span>
                    </span>
                </div>
                <div class="col-sm-4 single_news wow fadeInUp" data-wow-delay=".2s">
                    <span>
                        <a href="#" class="news_img">
                            <img src="assets/img/19.jpg" alt="">
                        </a>
                        <span class="news_content">
                            <span class="entry-date">
                                25th June, 2016
                            </span>
                            <a class="h3" href="#">How I started my startup with only $100 and got success.</a>
                        </span>
                    </span>
                </div>
                <div class="col-sm-4 single_news wow fadeInUp" data-wow-delay=".4s">
                    <span>
                        <a href="#" class="news_img">
                            <img src="assets/img/20.jpg" alt="">
                        </a>
                        <span class="news_content">
                            <span class="entry-date">
                                25th June, 2016
                            </span>
                            <a class="h3" href="#">How I started my startup with only $100 and got success.</a>
                        </span>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <!-- 13. /news_area -->
    
    
    <!-- 03. cta_area -->
    <div class="cta_area wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="cta">
                        <h2>Start Your Course With RLT ?</h2>
                    </div>
                </div>
                <div class="col-md-3 text-right">
                    <a href="#" class="button hvr-bounce-to-right pbg"><i class="fa fa-long-arrow-right"></i> Sign Up Now</a>
                </div>
            </div>
        </div>
    </div>
    <!-- 03. /cta_area -->

