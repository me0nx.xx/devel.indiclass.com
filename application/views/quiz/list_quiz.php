<style type="text/css">
    #scroolbarcategory::-webkit-scrollbar {
        width: 5px;
    }
    #scroolbarcategory::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
        -webkit-border-radius: 7px;
        border-radius: 7px;
    }
    #scroolbarcategory::-webkit-scrollbar-thumb {
        -webkit-border-radius: 7px;
        border-radius: 7px;
        background: #a6a5a5;
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
    }
    #scroolbarcategory::-webkit-scrollbar-thumb:window-inactive {
        background: rgba(0,0,0,0.4); 
    }

    #scroolbarresult::-webkit-scrollbar {
        width: 7px;
    }
    #scroolbarresult::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 5px rgba(0,0,0,0.1); 
        -webkit-border-radius: 7px;
        border-radius: 7px;
    }
    #scroolbarresult::-webkit-scrollbar-thumb {
        -webkit-border-radius: 7px;
        border-radius: 7px;
        background: #a6a5a5;
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1); 
    }
    #scroolbarresult::-webkit-scrollbar-thumb:window-inactive {
        background: rgba(0,0,0,0.4); 
    }
</style>
<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <div class="container">
            
            <div class="block-header">
                <h1 class="lead" style="font-size: 16pt;"><?php echo $this->lang->line('quizlist'); ?></h1>
                
                <ul class="actions">
                    <li>
                        <div class="col-md-4 pull-right">
                            <div class="input-group fg-float">
                                <div class="fg-line">    
                                    <input type="text" class="form-control">
                                    <label class="fg-label">Search</label>
                                </div>
                                <span class="input-group-addon last"><i class="zmdi zmdi-search"></i></span>
                            </div>
                        </div>
                    </li>
                </ul>
                
            </div> <!-- akhir block header    --> 

            <div class="col-md-12 col-xs-12">
                
                <div class="row">
                    <div class="col-md-3 col-xs-3 col-sm-3">
                        <div class="card m-t-20">
                            <div class="card-header">                        
                                <h2>Category</h2>
                                <small><i class="zmdi zmdi-filter-list"></i> Filter your category</small>                        
                                <br><hr>
                            </div>

                            <!-- <div class="card-body card-padding" style="margin-top: -10%;">
                                <div>
                                    <label class="m-b-10">Filter by</label>
                                    <select class='select2 form-control' required id="searchtemplate">                                        
                                        <option disabled="disabled" selected="" value="">Select Filter</option>
                                        <option value="date_published">Date Published</option>
                                        <option value="aplabetical">Alphabetical</option>                                        
                                    </select>  
                                </div>
                                <br>
                            </div> -->
                            
                            <div class="card-body card-padding" id="scroolbarcategory" style="margin-top: -11%; height: 600px; overflow-y: auto;">
                                <div>
                                    <label class="m-b-10">Category</label>
                                </div>                                
                                <div class="listview lv-user waves-effect" style="width: 100%;">    
                                    <?php
                                    if (!isset($_GET['category'])) {
                                        $styleme = "background-color: rgba(236, 240, 241, 0.6);";
                                    }
                                    else
                                    {
                                        $styleme = "";
                                    }
                                    ?>                                
                                    <div class="lv-item media active" style="<?php echo $styleme;?>">
                                        <div class="lv-avatar pull-left">
                                            <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/logo_baru/allsubject.png" alt="">
                                        </div>
                                        <div class="media-body">
                                            <a href="<?php echo BASE_URL();?>Quiz">
                                                <div class="lv-title m-t-5 f-14 c-gray" style="word-wrap: break-word; width: 100%;white-space: -moz-pre-wrap; white-space: pre-wrap;">All Subject</div>                                            
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <?php                            
                                // if (isset($_GET['category'])) 
                                // {
                                    $jenjang_id     = $this->session->userdata('jenjang_id');
                                    $getcategory    = $this->db->query("SELECT * FROM master_subject WHERE jenjang_id='$jenjang_id'")->result_array();
                                    foreach ($getcategory as $row => $data) {
                                        $category = "";
                                        if (isset($_GET['category'])) {
                                            $category = $_GET['category'];
                                        }
                                        if ($category == $data['subject_name']) {
                                            $style = "background-color: rgba(236, 240, 241, 0.6);";
                                        }
                                        else
                                        {
                                            $style = "";
                                        }
                                    ?>
                                    <div class="listview lv-user waves-effect" style="width: 100%;">
                                        <div class="lv-item media active" style="<?php echo $style;?>">
                                            <div class="lv-avatar pull-left">
                                                <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL.'class_icon/'. $data['icon'];?>" alt="">
                                            </div>
                                            <div class="media-body">
                                                <a href="<?php echo BASE_URL();?>Quiz?category=<?php echo $data['subject_name']?>">
                                                    <div class="lv-title m-t-5 f-14 c-gray" style="word-wrap: break-word; width: 100%;white-space: -moz-pre-wrap; white-space: pre-wrap;"><?php echo $data['subject_name'];?></div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                // }
                                ?>
                            </div>
                        </div>  
                    </div>
                    <div class="col-md-9 col-xs-9 col-sm-9">
                        <div class="card m-t-20">

                            <?php
                            $myjenjang = $this->session->userdata('jenjang_id');
                            if (isset($_GET['category'])) {
                                $category = $_GET['category'];
                                $resultdata = $this->db->query("SELECT mbs.*, ms.subject_name, ms.jenjang_id, mj.jenjang_name, mj.jenjang_level FROM master_subject as ms INNER JOIN master_banksoal as mbs ON ms.subject_id=mbs.subject_id INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id where ms.jenjang_id='$myjenjang' AND ms.subject_name='$category' AND mbs.list_soal != '[]'")->result_array();
                            }
                            else
                            {                                        
                                $resultdata = $this->db->query("SELECT mbs.*, ms.subject_name, ms.jenjang_id, mj.jenjang_name, mj.jenjang_level FROM master_subject as ms INNER JOIN master_banksoal as mbs ON ms.subject_id=mbs.subject_id INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id where ms.jenjang_id='$myjenjang' AND mbs.list_soal != '[]'")->result_array();                               
                            }
                            ?>
                            <div class="card-body card-padding" id="scroolbarresult" style="height: auto; max-height: 1000px; overflow-y: auto;">
                                <?php
                                    if (empty($resultdata)) {
                                        ?>
                                        <div class="row">
                                            <center>
                                                <div class="alert alert-info" role="alert">Maaf, kuis belum tersedia untuk jenjang anda.</div>
                                            </center>
                                        </div>
                                        <?php
                                    }
                                    else
                                    {
                                        $ck = 0;
                                        foreach ($resultdata as $key => $v) {
                                            $bsid           = $v['bsid'];
                                            $name           = $v['name'];
                                            $desc           = substr($v['description'],0,300);     
                                            $participant    = $v['participant'];
                                            $subject_name   = $v['subject_name'];
                                            $jenjang_name   = $v['jenjang_name'];
                                            $jenjang_level  = $v['jenjang_level'];
                                            $seconds        = $v['duration'];
                                            $hours          = floor($seconds / 3600);
                                            $mins           = floor($seconds / 60 % 60);
                                            $secs           = floor($seconds % 60);
                                            $duration       = sprintf('%02d:%02d', $hours, $mins);
                                            if ($participant == NULL) {
                                                $participant = 0;
                                            }
                                            $list_soal      = $v['list_soal'];
                                            $list_soal      = json_encode($list_soal,true);
                                            $this->session->set_userdata('bsid', $bsid);
                                            $validity       = strtotime($v['validity']);
                                            $invalidity     = strtotime($v['invalidity']);

                                            $server_utc     = $this->Rumus->getGMTOffset();
                                            $utc            = $this->session->userdata('user_utc');
                                            $intervall      = $utc - $server_utc;
                                            $datenow        = date('Y-m-d H:i:s');

                                            $waktu_sekarang = DateTime::createFromFormat ('Y-m-d H:i:s',$datenow);
                                            $waktu_sekarang->modify("+".$intervall ." minutes");
                                            $waktu_sekarang = $waktu_sekarang->format('Y-m-d H:i:s');
                                            $date_from_user = strtotime($waktu_sekarang);
                                            if ( ( $date_from_user >= $validity ) && ( $date_from_user <= $invalidity ) ) {                                                                                        
                                                $ck = 1;
                                                ?>                                        
                                                <div class="row" style="height: 200px; max-height: 200px;">
                                                    <div class="col-md-4 p-5 z-depth-1-top">
                                                        <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/coursess.png" alt="" style="width: 100%; height: 100%;">
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="media">
                                                            <div class="pull-right">
                                                                <a href="#">
                                                                    <img class="media-object" src="img/profile-pics/6.jpg" alt="">
                                                                </a>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="media-body rating-list" style="height: 150px; max-height: 150px;">
                                                                <h4 class="media-heading f-20 m-t-5 lead"><?php echo $name;?></h4>
                                                                <!-- <div class="rl-star m-b-5" style="margin-top: -2%;">
                                                                    <i class="zmdi zmdi-star active"></i>
                                                                    <i class="zmdi zmdi-star active"></i>
                                                                    <i class="zmdi zmdi-star active"></i>
                                                                    <i class="zmdi zmdi-star"></i>
                                                                    <i class="zmdi zmdi-star"></i>                                                    
                                                                    <label class="m-l-15"><i class="zmdi zmdi-account m-r-5"></i> Ramdhani </label>
                                                                </div> -->
                                                                <label class="f-11"><i class="zmdi zmdi-library"></i> <?php if ($jenjang_level == 0) {
                                                                	echo $subject_name.', '.$jenjang_name;	
                                                                }
                                                                else{
                                                                echo $subject_name.', '.$jenjang_name.'-'.$jenjang_level;	
                                                                } ?> </label>
                                                                <p><?php echo $desc;?></p>
                                                            </div>
                                                            <div class="media-footer">
                                                                <div class="col-md-6">
                                                                    <i class="zmdi zmdi-accounts"></i> <?php echo $participant;?> Students
                                                                    <i class="zmdi zmdi-time m-l-10"></i> <?php echo $duration;?> Hours
                                                                    <i class="zmdi zmdi-accounts"></i> <?php echo count($list_soal);?>
                                                                </div>
                                                                <div class="col-md-6 pull-right">
                                                                    <a href="<?php echo BASE_URL();?>Courses?name=<?php echo $name;?>&bsid=<?php echo $bsid;?>"><button onclick="startQuiz.start();" class="btn btn-default btn-icon-text waves-effect pull-right"><i class="zmdi zmdi-collection-text"></i> Apply Now</button></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br><hr><br> 
                                                <?php 
                                            }                                           
                                        }
                                        if ($ck == 0 ) {                                                                                    
                                            ?>
                                            <div class="row">
                                                <center>
                                                    <div class="alert alert-info" role="alert">Maaf, tidak ada kuis untuk saat ini.</div>
                                                </center>
                                            </div>
                                            <?php
                                        }
                                    }
                                ?>                               

                            </div>  
                        </div>            
                    </div>
                </div>

            </div>            

    </section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>

<script type="text/javascript">
    $(document).ready(function() {
        // $('#searchtemplate').change(function(e){
        //     alert($(this).val());
        // });
    });
</script>
<script type="text/javascript">
            
    var bsid = "<?php echo $this->session->userdata('bsid');?>";
      
    var id_user = "<?php echo $this->session->userdata('id_user');?>";  
    var tgll = null;
    var startQuiz = {
        start : function(){  
            
            $.ajax({
                url: '<?php echo base_url(); ?>V1.0.0/startQuiz',
                type: 'POST',       
                data: {
                    bsid:bsid,
                    id_user:id_user,
                    time_start:tgll
                },         
                success: function(response)
                {            
                    var a = JSON.stringify(response);
                    // console.log(a);
                    // return false;
                                  
                }
            });

            
        }
    };
    $(".reload").click(function(){
        window.location.replace('<?php echo base_url();?>Quiz');
    });
</script>
    