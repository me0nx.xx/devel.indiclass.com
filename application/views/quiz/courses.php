<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1" class="bgm-white">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/side'); ?>
    </aside>

    <section id="content">
        <?php
        $bsid                   = $_GET['bsid'];
        $name                   = $_GET['name'];
        $getdetails_courses     = $this->db->query("SELECT ms.subject_name, mj.jenjang_name, mj.jenjang_level, mbs.* FROM master_banksoal as mbs INNER JOIN master_subject as ms ON mbs.subject_id=ms.subject_id INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE mbs.bsid='$bsid' AND mbs.name='$name'")->row_array(); 
        if (empty($getdetails_courses)) {
            redirect('/Quiz');
        }
        else
        {
        $subject_name           = $getdetails_courses['subject_name'];
        $jenjang_name           = $getdetails_courses['jenjang_name'];
        $jenjang_level          = $getdetails_courses['jenjang_level'];
        $desc                   = substr($getdetails_courses['description'],0,500);
        $seconds                = $getdetails_courses['duration'];
        $hours                  = floor($seconds / 3600);
        $mins                   = floor($seconds / 60 % 60);
        $secs                   = floor($seconds % 60);
        $duration               = sprintf('%02d:%02d', $hours, $mins);
        $template_type          = $getdetails_courses['template_type'];
        $list_soal              = json_decode($getdetails_courses['list_soal'],true);
        $jumlah_soal            = count($list_soal);
        if ($template_type == "multiple_choice") {
            $template_type      = "Multiple Choice";
        }
        else
        {
            $template_type      = "Essay";
        }
        $validity               = DateTime::createFromFormat ('Y-m-d H:i:s',$getdetails_courses['validity']);
        $validity               = $validity->format('d-m-Y');
        $datevalidity           = date_create($validity);
        $dateevalidity          = date_format($datevalidity, 'd/m/y');
        $harivalidity           = date_format($datevalidity, 'd');
        $hari                   = date_format($datevalidity, 'l');
        $tahunvalidity          = date_format($datevalidity, 'Y');
        $waktuvalidity          = date_format($datevalidity, 'H:s');   
        $datevalidity           = $dateevalidity;
        $sepparatorvalidity     = '/';
        $partsvalidity          = explode($sepparatorvalidity, $datevalidity);
        $bulanvalidity          = date("F", mktime(0, 0, 0, $partsvalidity[1], $partsvalidity[2], $partsvalidity[0]));

        $invalidity             = DateTime::createFromFormat ('Y-m-d H:i:s',$getdetails_courses['invalidity']);
        $invalidity             = $invalidity->format('d-m-Y');
        $dateinvalidity         = date_create($invalidity);
        $dateeinvalidity        = date_format($dateinvalidity, 'd/m/y');
        $hariinvalidity         = date_format($dateinvalidity, 'd');
        $hari                   = date_format($dateinvalidity, 'l');
        $tahuninvalidity        = date_format($dateinvalidity, 'Y');
        $waktuinvalidity        = date_format($dateinvalidity, 'H:s');   
        $dateinvalidity         = $dateeinvalidity;
        $sepparatorinvalidity   = '/';
        $partsinvalidity        = explode($sepparatorinvalidity, $dateinvalidity);
        $bulaninvalidity        = date("F", mktime(0, 0, 0, $partsinvalidity[1], $partsinvalidity[2], $partsinvalidity[0]));
        ?>
        <div class="block-header" style="background: #f6f6f6; min-height:200; margin: 0; padding: 0; margin-top: -2%; height: 100px;">
            <div class="container">
                <label class="m-t-20 m-l-20 lead" style="font-size: 2.4em; margin-top: 11%;"><?php echo $name;?></label> 
            </div>
        </div>
        <div class="container">

            <div class="col-md-12 col-xs-12">
                
                <div class="row">                    
                    
                    <div class="col-md-9 col-xs-9 col-sm-9">
                        <div class="card m-t-20 bgm-purple c-white" style="border-radius: 5px;">

                            <div class="card-body card-padding">
                                <?php
                                    
                                ?>
                                <div class="row" style="height: 200px; max-height: 200px;">                                    
                                    <div class="col-md-8">
                                        <div class="media">
                                            <div class="clearfix"></div>
                                            <div class="media-body rating-list" style="height: 150px; max-height: 150px;">
                                                <button class="btn btn-primary" style="background-color: rgba(25, 181, 254, 0.54);"><?php echo $subject_name;?></button>
                                                <h4 class="media-heading f-20 m-t-20 lead c-white"><?php echo $name;?></h4>
                                                <!-- <div class="rl-star m-b-5" style="margin-top: -2%;">
                                                    <i class="zmdi zmdi-star active"></i>
                                                    <i class="zmdi zmdi-star active"></i>
                                                    <i class="zmdi zmdi-star active"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>                                                    
                                                    <label class="m-l-15"><i class="zmdi zmdi-account m-r-5"></i> Ramdhani </label>
                                                </div> -->
                                                <p><?php echo $desc;?></p>
                                            </div>                                           
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-1"></div> -->
                                    <div class="col-md-3 p-5 z-depth-1-top m-t-20 m-l-20">
                                        <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/coursess.png" alt="" style="width: 100%; height: 100%;">
                                    </div>                                    
                                </div>

                            </div>  
                        </div>  

                        <div class="card-body" style="border-bottom: 2px solid #D2D7D3;">
                            <h1 class="lead" style="font-size: 18pt;">Information Details</h1>
                        </div>

                        <div class="card m-t-20">                        
                            <div style="margin-top: 5%; margin-bottom: 5%;">
                                <div class="col-md-6 col-xs-6">
                                    <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>quiz/book.png" style="height: 6%; width: 6%;">
                                    <label class="f-13">Course to</label>
                                    <br>
                                    <label class="m-t-10 f-19"><?php if ($jenjang_level == "0") {
                                        echo $jenjang_name;
                                    } else{
                                      echo $jenjang_name.' - '.$jenjang_level;  
                                    } ?></label>
                                    <hr>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>quiz/currency-usd.png" style="height: 6%; width: 6%;">
                                    <label class="f-13">Course cost</label>
                                    <br>
                                    <label class="m-t-10 f-19">Free</label>
                                    <hr>
                                </div>
                            </div>

                        </div>    
                        <br><br><br>
                        <div class="card m-t-20">
                            <div style="margin-top: 5%; margin-bottom: 5%;">
                                <div class="col-md-6 col-xs-6">
                                    <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>quiz/clock.png" style="height: 6%; width: 6%;">
                                    <label class="f-13">Duration</label>
                                    <br>
                                    <label class="m-t-10 f-19"><?php echo $duration;?> Hours</label>
                                    <hr>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>quiz/format-list-bulleted-type.png" style="height: 6%; width: 6%;">
                                    <label class="f-13">Course type</label>
                                    <br>
                                    <label class="m-t-10 f-19"><?php echo $template_type;?></label>
                                    <hr>
                                </div>
                            </div>
                        </div>
                        <br><br><br>
                        <div class="card m-t-20">
                            <div style="margin-top: 5%; margin-bottom: 5%;">
                                <div class="col-md-6 col-xs-6">
                                    <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>quiz/calendar-clock.png" style="height: 6%; width: 6%;">
                                    <label class="f-13">Time</label>
                                    <br>
                                    <label class="m-t-10 f-19"><?php echo $harivalidity.' '.$bulanvalidity.' '.$tahunvalidity;?> - </label>  <label class="m-t-10 f-19"> <?php echo $hariinvalidity.' '.$bulaninvalidity.' '.$tahuninvalidity;?></label>
                                    <hr><hr>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>quiz/format-list-bulleted-type.png" style="height: 6%; width: 6%;">
                                    <label class="f-13">Total Questions</label>
                                    <br>
                                    <label class="m-t-10 f-19"><?php echo $jumlah_soal;?></label>
                                    <hr>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-3 col-xs-3 col-sm-3">
                        <div class="card m-t-20 z-depth-1">
                            <div class="card-header">                        
                                <h2>Free Course</h2>
                                <div class="m-t-5 m-b-10" style="width: 20%; height: 3px; background-color: #2ECC71;"></div>
                            </div>
                            
                            <div class="card-body card-padding" style="margin-top: -5%;">
                                <div>
                                    <label class="m-b-20 f-13">This course is free</label>
                                    <a href="<?php echo base_url() ; ?>Start?bsid=<?php echo $bsid;?>">
                                        <button onclick="startQuiz.start();" class="btn btn-primary btn-block" style="height: 50px; border-radius: 6px;">Start Free Course</button>
                                    </a>
                                </div>
                                <br>                                
                            </div>
                        </div>  
                    </div>

                </div>

            </div>   
        </div>         
        <?php
        }
        ?>
    </section>
</section>

<footer id="footer" class="bgm-white m-b-20 m-t-10">
    <?php $this->load->view('inc/footer'); ?>
</footer>

    