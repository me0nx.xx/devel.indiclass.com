<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>

<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('./inc/sidetutor'); ?>
    </aside>

    <section id="content">
        <div class="container">

            <div class="block-header">
                <h2><?php echo $this->lang->line('profil'); ?></h2>

                <ul class="actions">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>first/"><?php echo $this->lang->line('hometutor'); ?></a></li>
                            <li><?php echo $this->lang->line('profil'); ?></li>
                            <li class="active"><?php echo $this->lang->line('tab_cp'); ?></li>
                        </ol>
                    </li>
                </ul>
            </div><!-- akhir block header -->            
            
            <div class="bs-item z-depth-5">
                <div class="card" id="profile-main">

                    <?php
                        $this->load->view('inc/sideprofile');
                    ?>

                    <div class="pm-body clearfix">
                        <ul class="tab-nav tn-justified" role="tablist">
                            <li class="waves-effect"><a href="<?php echo base_url('tutor/about'); ?>"><?php echo $this->lang->line('tab_about'); ?></a></li>
                            <li class="waves-effect"><a href="<?php echo base_url('tutor/profile_account'); ?>"><?php echo $this->lang->line('tab_account'); ?></a></li>
                            <li class="waves-effect"><a href="<?php echo base_url('tutor/profile'); ?>"><?php echo $this->lang->line('tab_pr'); ?></a></li>
                            <li class="active waves-effect"><a href="<?php echo base_url('tutor/profile_forgot'); ?>"><?php echo $this->lang->line('tab_cp'); ?></a></li>                                        
                        </ul>      
                        <br>
                        <?php if($this->session->flashdata('mes_alert')){ ?>
                        <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <?php echo $this->session->flashdata('mes_message'); ?>
                        </div>
                        <?php } ?>
                        <div class="card-body card-padding">
                            <form method="POST" action="<?php  echo base_url(); ?>master/aksi_ubahpass" role="form">
                                <div class="form-group fg-float">
                                    <div class="fg-line">
                                        <input type="password" name="oldpass" class="input-lg form-control fg-input">
                                        <label class="fg-label" style="color:#5e5e5e;" for="exampleInputPassword1"><?php echo $this->lang->line('old_pass'); ?></label>
                                    </div>
                                </div>  
                                <br>
                                <div class="form-group fg-float">
                                    <div class="fg-line">
                                        <input type="password" name="newpass" class="input-lg form-control fg-input">
                                        <label class="fg-label" style="color:#5e5e5e;" for="exampleInputPassword1"><?php echo $this->lang->line('new_pass'); ?></label>
                                    </div>
                                </div> 
                                <br>      
                                <div class="form-group fg-float">
                                    <div class="fg-line">
                                        <input type="password" name="conpass" class="input-lg form-control fg-input">
                                        <label class="fg-label" style="color:#5e5e5e;" for="exampleInputPassword1"><?php echo $this->lang->line('confirm_pass'); ?></label>
                                    </div>
                                </div>                                                                                    

                                <input type="hidden" name="mail" class="form-control input-sm" value="<?php echo $this->session->userdata('email'); ?>" id="exampleInputPassword1" placeholder="Confrim Password">

                                <br>
                                <button type="submit" class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); ?></button>
                            </form>                                                                            

                        </div>
                        <br><br><br><br><br><br><br><br>
                        <br><br><br><br>

                    </div>
                </div>
            </div>
        </div><!-- akhir container -->
    </section>            
</section>

<footer id="footer">
   <?php $this->load->view('inc/footer'); ?>
</footer>


<!-- Page Loader -->
        <!-- <div class="page-loader">
            <div class="preloader pls-blue">
                <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20" />
                </svg>

                <p>Please wait...</p>
            </div>
        </div> -->
