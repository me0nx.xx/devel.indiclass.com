<!-- <header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #db1f00;"> -->
    <?php $this->load->view('inc/navbar'); ?>
<!-- </header> -->
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('inc/sideadmin'); ?>
	</aside>

	<section id="content">
		<div class="container">            
			<div class="block-header">
				<h2>Assessor</h2>
			</div>

			<div class="card m-t-20 p-20" style="">
                <div class="row" style="margin-top: 2%; overflow-y: auto;">                
                    <div class="col-md-12" >
                        <div class="card-body card-padding table-responsive" style="background-color:#EEEEEE;">
                            <br><br>
                            <table id="" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Email</th>                                        
                                        <th>Photo</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>  
                                    <?php
                                    $no=1;
                                    // foreach ($alldatatutor as $row => $v) { 
                                    // 	$idkabupaten 	= $v['id_kabupaten'];
                                    // 	$namakab 		= $this->db->query("SELECT kabupaten_name FROM master_kabupaten WHERE kabupaten_id='$idkabupaten'")->row_array()['kabupaten_name'];
                                    // 	$foto  			= $v['user_image'];                                    	                                    	
                                        ?>
                                        <tr>
                                            <td><?php echo($no); ?></td>
                                            <td><!-- <?php echo $v['user_name']; ?> --></td>
                                            <td><!-- <?php echo $v['email']; ?> --></td>
                                            <td>
		                                    	
                                            </td>
                                            <td>
                                                <a class="card profile-tutor" href=""><button class="btn btn-default" title="Detail tutor"><i class="zmdi zmdi-search"></i></button></a>
                                            </td>
                                        </tr>
                                        <?php 
                                        $no++;
                                    // }
                                    ?>      
                                </tbody>   
                            </table>                           
                        </div>
                    </div>
                </div>
            </div> 		

		</div>
	</section>

</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	$('table.display').DataTable({
        fixedHeader: {
            header: true,
            footer: true
        }
    });

    $('.data').DataTable(); 

</script>