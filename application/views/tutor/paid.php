<!-- <header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #db1f00;"> -->
    <?php $this->load->view('inc/navbar'); ?>
<!-- </header> -->
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('./inc/sidetutor'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header">
                <h2>Atur Kelas Berbayar</h2>

                <!-- <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>tutor"><?php echo $this->lang->line('hometutor'); ?></a></li>                            
                            <li class="active"><?php echo $this->lang->line('selectsubjecttutor'); ?></li>
                        </ol>
                    </li>
                </ul> -->
            </div> <!-- akhir block header    -->        
            <br>
            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>
            <!-- disini untuk dashboard murid -->               
            <div class="card col-md-12">
                <form class="form-horizontal" role="form">
                    <!-- <div class="card-header">
                        <h2>Atur Kelas <small>Mengatur Jadwal Kelas Berbayar</small></h2><hr>                        
                    </div> -->
                    
                    <div class="card-body">
                        <ul class="tab-nav tn-justified tn-icon" role="tablist">
                            <li role="presentation" class="active">
                                <a class="col-sx-6" href="#tab-1" aria-controls="tab-1" role="tab" data-toggle="tab" aria-expanded="false">
                                	<img src="https://cdn.classmiles.com/sccontent/baru/default.png" height="75px" width="75px"><br><br>
                                    Default
                                </a>
                            </li>
                            <li role="presentation" class="">
                                <a class="col-xs-6" href="#tab-2" aria-controls="tab-2" role="tab" data-toggle="tab" aria-expanded="true">
                                    <img src="https://cdn.classmiles.com/sccontent/baru/credit.png" height="75px" width="75px"><br><br>
                                    Class Credit
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content p-20">
                            <div role="tabpanel" class="tab-pane animated fadeIn active" id="tab-1">
                                
                                <div class="card-header">
	                                <h2>Atur Kelas
	                                    <small>Kelas Berbayar Default</small>
	                                </h2>
	                            </div>

                                <div class="card-body card-padding"> 

			                        <div class="form-group">
			                            <label for="inputEmail3" class="col-sm-3 control-label">Tanggal</label>
			                            <div class="col-sm-6">
			                                <div class="dtp-container fg-line m-l-5">                                                
			                                    <input id="tanggal" type='text' required class="form-control"  data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
			                                </div>
			                            </div>
			                        </div>

			                        <div class="form-group">
			                            <label for="inputEmail3" class="col-sm-3 control-label">Waktu</label>
			                            <div class="col-sm-6">
			                                <div class="dtp-container fg-line" id="tempatwaktuu">
			                                    <input type='text' class='form-control' id='timeeeeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />
			                                </div>
			                            </div>
			                        </div> 

			                        <div class="form-group">
			                            <label for="inputEmail3" class="col-sm-3 control-label">Durasi</label>
			                            <div class="col-sm-6">
			                            <div class="dtp-container fg-line">                                                
			                                <select class='select2 form-control' required id="durasi">
			                                    <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
			                                    <option value="900">15 Minutes</option>
			                                    <option value="1800">30 Minutes</option>
			                                    <option value="2700">45 Minutes</option>
			                                    <option value="3600">1 Hours</option>
			                                    <option value="5400">1 Hours 30 Minutes</option>
			                                    <option value="7200">2 Hours</option>
			                                    <option value="9000">2 Hours 30 Minutes</option>
			                                    <option value="10800">3 Hours</option>
			                                    <option value="12600">3 Hours 30 Minutes</option>
			                                    <option value="14400">4 Hours</option>
			                                    <option value="16200">4 Hours 30 Minutes</option>
			                                    <option value="18000">5 Hours</option>
			                                    <option value="19800">5 Hours 30 Minutes</option>
			                                    <option value="21600">6 Hours</option>
			                                    <option value="23400">6 Hours 30 Minutes</option>
			                                    <option value="25200">7 Hours</option>
			                                    <option value="27000">7 Hours 30 Minutes</option>
			                                    <option value="28800">8 Hours</option>
			                                    <option value="30600">8 Hours 30 Minutes</option>
			                                    <option value="32400">9 Hours</option>
			                                    <option value="34200">9 Hours 30 Minutes</option>
			                                    <option value="36000">10 Hours</option>
			                                    <option value="37800">10 Hours 30 Minutes</option>
			                                    <option value="39600">11 Hours</option>
			                                    <option value="41400">11 Hours 30 Minutes</option>
			                                    <option value="43200">12 Hours</option>
			                                    <option value="45000">12 Hours 30 Minutes</option>
			                                    <option value="46800">13 Hours</option>
			                                    <option value="48600">13 Hours 30 Minutes</option>
			                                    <option value="50400">14 Hours</option>
			                                    <option value="52200">14 Hours 30 Minutes</option>
			                                    <option value="54000">15 Hours</option>
			                                    <option value="55800">15 Hours 30 Minutes</option>
			                                    <option value="57600">16 Hours</option>
			                                    <option value="59400">16 Hours 30 Minutes</option>
			                                    <option value="61200">17 Hours</option>
			                                    <option value="63000">17 Hours 30 Minutes</option>
			                                    <option value="64800">18 Hours</option>
			                                    <option value="66600">18 Hours 30 Minutes</option>
			                                    <option value="68400">19 Hours</option>
			                                    <option value="70200">19 Hours 30 Minutes</option>
			                                    <option value="72000">20 Hours</option>
			                                    <option value="73800">20 Hours 30 Minutes</option>
			                                    <option value="75600">21 Hours</option>
			                                    <option value="77400">21 Hours 30 Minutes</option>
			                                    <option value="79200">22 Hours</option>
			                                    <option value="81000">22 Hours 30 Minutes</option>
			                                    <option value="82800">23 Hours</option>
			                                    <option value="84600">23 Hours 30 Minutes</option>
			                                    <option value="86400">24 Hours</option>                                    
			                                </select>                                                             
			                            </div>
			                            </div>
			                        </div> 			                        

			                        <div class="form-group">
			                            <label for="inputEmail3" class="col-sm-3 control-label">Pelajaran</label>
			                            <div class="col-sm-6">
			                            <div class="dtp-container fg-line">                                                
			                                <select class='select2 form-control' required id="pelajaran">
			                                    <?php
			                                        echo '<option disabled="disabled" selected="" value="0">Pilih Pelajaran</option>'; 
			                                        $id = $this->session->userdata("id_user");
			                                        $sub = $this->db->query("SELECT * FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id=ms.subject_id WHERE tb.id_user='$id' AND tb.tutor_id='0'  AND (tb.status='verified' OR tb.status='requested' OR tb.status='unverified') ")->result_array();
			                                        foreach ($sub as $row => $v) {

			                                            $jenjangnamelevel = array();
			                                            $jenjangid = json_decode($v['jenjang_id'], TRUE);
			                                            if ($jenjangid != NULL) {
			                                                if(is_array($jenjangid)){
			                                                    foreach ($jenjangid as $key => $value) {                                    
			                                                        $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();   
			                                                        if ($selectnew['jenjang_level']=='0') {
			                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']; 
			                                                            }
			                                                            else{
			                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
			                                                            }
			                                                    }   
			                                                }else{
			                                                    $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();   
			                                                    if ($selectnew['jenjang_level']=='0') {
			                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']; 
			                                                            }
			                                                            else{
			                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
			                                                            }                                                         
			                                                }                                       
			                                            }

			                                            // $allsub = $this->db->query("SELECT tb.*, mj.*, ms.subject_name FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$id' AND tb.tutor_id='0' AND (tb.status='verified' OR tb.status='unverified') order by ms.subject_id ASC")->result_array();                                                
			                                            // foreach ($allsub as $row => $v) {                                            
			                                                echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.implode(", ",$jenjangnamelevel).'</option>';
			                                            // }
			                                        }
			                                    ?>
			                                </select>                                                               
			                            </div>
			                            </div>
			                        </div> 

			                        <div class="form-group">
			                            <label for="inputEmail3" class="col-sm-3 control-label">Topik Pelajaran</label>
			                            <div class="col-sm-6">
			                            <div class="dtp-container fg-line">                                                                                
			                                <!-- <input type="text" name="topikpelajaran" class="form-control" required placeholder="Masukan Topik pelajaran"> -->
			                                <textarea rows="5" name="topikpelajaran" id="topikpelajaran" class="form-control" required placeholder="Masukan Topik Pelajaran"></textarea>        
			                            </div>
			                            </div>
			                        </div> 

			                        <div class="form-group">
			                            <label for="inputEmail3" class="col-sm-3 control-label">Harga Kelas</label>
			                            <div class="col-sm-6">
			                            <div class="dtp-container fg-line">                                                                                
			                                <input type="text" id="nominal" required class="input-sm form-control fg-input nominal" placeholder=" Masukan Harga anda" minlength="9" step="500" maxlength="14" min="10000" max="500000"/>
			                                <input type="text" id="nominalsave" hidden />                                
			                            </div>
			                            </div>
			                        </div>   
			                        <input type="text" name="user_utcc" id="user_utcc" value="<?php echo $this->session->userdata('user_utc'); ?>" hidden>                                        

			                        <br><hr>
			                        <div class="form-group">
			                            <div class="col-sm-offset-2 col-sm-7">
			                                <!-- <a class="waves-effect c-white btn btn-primary btn-block simpanprice"><i class="zmdi zmdi-face-add"></i>Buat Kelas</a> -->
			                                <button type="submit" class="waves-effect c-white btn btn-primary btn-block " id="simpankelas">Buat Kelas</button>
			                            </div>
			                        </div>
			                    </div>

                            </div>

                            <div role="tabpanel" class="tab-pane animated fadeIn" id="tab-2">
                                
                                <div class="card-header">
	                                <h2>Atur Kelas
	                                    <small>Kelas Berbayar Class Credit</small>
	                                </h2>
	                            </div>

                                <div class="card-body card-padding"> 

                                	<div class="form-group">
			                            <label for="inputEmail3" class="col-sm-3 control-label">Kuota Participant</label>
			                            <div class="col-sm-6">
				                            <div class="dtp-container fg-line">                                                
				                                <select class='select2 form-control' required id="kuota_participants" style="width: 100%;">
				                                    <?php
				                                        echo '<option disabled="disabled" selected="" value="0">Pilih Kuota</option>'; 
				                                        $id = $this->session->userdata("id_user");
				                                        $sub = $this->db->query("SELECT * FROM tbl_multicast_volume WHERE id_tutor='$id' AND status=1 AND type = 'paid_multicast'")->result_array();
				                                        foreach ($sub as $row => $v) {
				                                        	$price 			= number_format($v['price'], 0, ".", ".");;
	        												$volume 		= $v['volume'];
	        												$user_utc 		= $v['utc'];
	        												$datenow 		= $v['date_request'];
	        												// $server_utc 	= $this->Rumus->getGMTOffset();
															// $interval 		= $user_utc - $server_utc;
															// $date_requested = DateTime::createFromFormat('Y-m-d H:i:s', $datenow );
															// $date_requested->modify("+".$interval ." minutes");		
															// $date_requested = $date_requested->format('Y-m-d H:i:s');

				                                        	echo '<option value="'.$v['idv'].'">'.$datenow.' | Rp. '.$price.' - '.$volume.' Participant</option>';
				                                        }
				                                    ?>
				                                </select>
				                            </div>
				                        </div>
			                        </div>

			                        <div class="form-group">
			                            <label for="inputEmail3" class="col-sm-3 control-label">Tanggal</label>
			                            <div class="col-sm-6">
			                                <div class="dtp-container fg-line m-l-5">                                                
			                                    <input id="credit_tanggal" type='text' required class="form-control"  data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
			                                </div>
			                            </div>
			                        </div>

			                        <div class="form-group">
			                            <label for="inputEmail3" class="col-sm-3 control-label">Waktu</label>
			                            <div class="col-sm-6">
			                                <div class="dtp-container fg-line" id="credit_tempatwaktuu">
			                                    <input type='text' class='form-control' id='credit_timeeeeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />
			                                </div>
			                            </div>
			                        </div> 

			                        <div class="form-group">
			                            <label for="inputEmail3" class="col-sm-3 control-label">Durasi</label>
			                            <div class="col-sm-6">
			                            <div class="dtp-container fg-line">                                                
			                                <select class='select2 form-control' required id="credit_durasi" style="width: 100%;">
			                                    <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
			                                    <option value="900">15 Minutes</option>
			                                    <option value="1800">30 Minutes</option>
			                                    <option value="2700">45 Minutes</option>
			                                    <option value="3600">1 Hours</option>
			                                    <option value="5400">1 Hours 30 Minutes</option>
			                                    <option value="7200">2 Hours</option>
			                                    <option value="9000">2 Hours 30 Minutes</option>
			                                    <option value="10800">3 Hours</option>
			                                    <option value="12600">3 Hours 30 Minutes</option>
			                                    <option value="14400">4 Hours</option>
			                                    <option value="16200">4 Hours 30 Minutes</option>
			                                    <option value="18000">5 Hours</option>
			                                    <option value="19800">5 Hours 30 Minutes</option>
			                                    <option value="21600">6 Hours</option>
			                                    <option value="23400">6 Hours 30 Minutes</option>
			                                    <option value="25200">7 Hours</option>
			                                    <option value="27000">7 Hours 30 Minutes</option>
			                                    <option value="28800">8 Hours</option>
			                                    <option value="30600">8 Hours 30 Minutes</option>
			                                    <option value="32400">9 Hours</option>
			                                    <option value="34200">9 Hours 30 Minutes</option>
			                                    <option value="36000">10 Hours</option>
			                                    <option value="37800">10 Hours 30 Minutes</option>
			                                    <option value="39600">11 Hours</option>
			                                    <option value="41400">11 Hours 30 Minutes</option>
			                                    <option value="43200">12 Hours</option>
			                                    <option value="45000">12 Hours 30 Minutes</option>
			                                    <option value="46800">13 Hours</option>
			                                    <option value="48600">13 Hours 30 Minutes</option>
			                                    <option value="50400">14 Hours</option>
			                                    <option value="52200">14 Hours 30 Minutes</option>
			                                    <option value="54000">15 Hours</option>
			                                    <option value="55800">15 Hours 30 Minutes</option>
			                                    <option value="57600">16 Hours</option>
			                                    <option value="59400">16 Hours 30 Minutes</option>
			                                    <option value="61200">17 Hours</option>
			                                    <option value="63000">17 Hours 30 Minutes</option>
			                                    <option value="64800">18 Hours</option>
			                                    <option value="66600">18 Hours 30 Minutes</option>
			                                    <option value="68400">19 Hours</option>
			                                    <option value="70200">19 Hours 30 Minutes</option>
			                                    <option value="72000">20 Hours</option>
			                                    <option value="73800">20 Hours 30 Minutes</option>
			                                    <option value="75600">21 Hours</option>
			                                    <option value="77400">21 Hours 30 Minutes</option>
			                                    <option value="79200">22 Hours</option>
			                                    <option value="81000">22 Hours 30 Minutes</option>
			                                    <option value="82800">23 Hours</option>
			                                    <option value="84600">23 Hours 30 Minutes</option>
			                                    <option value="86400">24 Hours</option>                                    
			                                </select>                                                             
			                            </div>
			                            </div>
			                        </div> 			                        

			                        <div class="form-group">
			                            <label for="inputEmail3" class="col-sm-3 control-label">Pelajaran</label>
			                            <div class="col-sm-6">
			                            <div class="dtp-container fg-line">                                                
			                                <select class='select2 form-control' required id="credit_pelajaran" style="width: 100%;">
			                                    <?php
			                                        echo '<option disabled="disabled" selected="" value="0">Pilih Pelajaran</option>'; 
			                                        $id = $this->session->userdata("id_user");
			                                        $sub = $this->db->query("SELECT * FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id=ms.subject_id WHERE tb.id_user='$id' AND tb.tutor_id='0'  AND (tb.status='verified' OR tb.status='requested' OR tb.status='unverified') ")->result_array();
			                                        foreach ($sub as $row => $v) {

			                                            $jenjangnamelevel = array();
			                                            $jenjangid = json_decode($v['jenjang_id'], TRUE);
			                                            if ($jenjangid != NULL) {
			                                                if(is_array($jenjangid)){
			                                                    foreach ($jenjangid as $key => $value) {                                    
			                                                        $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();   
			                                                        if ($selectnew['jenjang_level']=='0') {
			                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']; 
			                                                            }
			                                                            else{
			                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
			                                                            }
			                                                    }   
			                                                }else{
			                                                    $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();   
			                                                    if ($selectnew['jenjang_level']=='0') {
			                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']; 
			                                                            }
			                                                            else{
			                                                                $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
			                                                            }                                                         
			                                                }                                       
			                                            }

			                                            // $allsub = $this->db->query("SELECT tb.*, mj.*, ms.subject_name FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$id' AND tb.tutor_id='0' AND (tb.status='verified' OR tb.status='unverified') order by ms.subject_id ASC")->result_array();                                                
			                                            // foreach ($allsub as $row => $v) {                                            
			                                                echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.implode(", ",$jenjangnamelevel).'</option>';
			                                            // }
			                                        }
			                                    ?>
			                                </select>                                                               
			                            </div>
			                            </div>
			                        </div> 

			                        <div class="form-group">
			                            <label for="inputEmail3" class="col-sm-3 control-label">Topik Pelajaran</label>
			                            <div class="col-sm-6">
			                            <div class="dtp-container fg-line">                                                                                
			                                <!-- <input type="text" name="topikpelajaran" class="form-control" required placeholder="Masukan Topik pelajaran"> -->
			                                <textarea rows="5" name="topikpelajaran" id="credit_topikpelajaran" class="form-control" required placeholder="Masukan Topik Pelajaran"></textarea>        
			                            </div>
			                            </div>
			                        </div> 

			                        <div class="form-group">
			                            <label for="inputEmail3" class="col-sm-3 control-label">Harga Kelas</label>
			                            <div class="col-sm-6">
			                            <div class="dtp-container fg-line">                                                                                
			                                <input type="text" id="credit_nominal" required class="input-sm form-control fg-input nominal" placeholder=" Masukan Harga anda" minlength="9" step="500" maxlength="14" min="10000" max="500000"/>
			                                <input type="text" id="credit_nominalsave" hidden />                                
			                            </div>
			                            </div>
			                        </div>   			                                                              

			                        <br><hr>
			                        <div class="form-group">
			                            <div class="col-sm-offset-2 col-sm-7">
			                                <!-- <a class="waves-effect c-white btn btn-primary btn-block simpanprice"><i class="zmdi zmdi-face-add"></i>Buat Kelas</a> -->
			                                <button type="submit" class="waves-effect c-white btn btn-primary btn-block " id="credit_simpankelas">Buat Kelas</button>
			                            </div>
			                        </div>
			                    </div>

                            </div>
                        </div>
                    </div>

                    
                </form>

            </div>

        </div>                                

    </div>
</section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

$(document).ready(function(){

    var tgll = new Date();  
    var formattedDatee = moment(tgll).format('YYYY-MM-DD');
    var a = [];
    var b = [];            
    var c = [];
    var d = [];

    $(function () {
        var tgl = new Date();  
        var formattedDate = moment(tgl).format('YYYY-MM-DD');         
        
        $('#tanggal').datetimepicker({  
            minDate: moment(formattedDate, 'YYYY-MM-DD')
        });

        $('#credit_tanggal').datetimepicker({  
            minDate: moment(formattedDate, 'YYYY-MM-DD')
        });
        
    });

    $('#tanggal').on('dp.change', function(e) {
        date = moment(e.date).format('YYYY-MM-DD');                
        var hoursnoww = tgll.getHours(); 
        // alert(tgl);
        if (date == formattedDatee) {
            a = [];
            $("#tempatwaktuu").html("");
            $("#tempatwaktuu").append("<input type='text' class='form-control' id='time' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
            $("#time").removeAttr('disabled');
            for (var i = hoursnoww+1; i < 24; i++) {                                                
                a.push(i);
            }
            console.warn(a);
            $('#time').datetimepicker({                    
                sideBySide: true, 
                showClose: true,                   
                format: 'HH:mm',
                stepping: 15,
                enabledHours: a,
            });
        }
        else
        {
            $("#tempatwaktuu").html("");
            $("#tempatwaktuu").append("<input type='text' class='form-control' id='time' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
            $("#time").removeAttr('disabled');
            b = [];
            for (var i = 0; i < 24; i++) {                                                
                b.push(i);
            }
            console.warn(b);
            $('#time').datetimepicker({                    
                sideBySide: true,                    
                format: 'HH:mm',
                showClose: true,
                stepping: 15,
                enabledHours: b,
            });
        }
        
    });

    $('#credit_tanggal').on('dp.change', function(e) {
        date = moment(e.date).format('YYYY-MM-DD');                
        var hoursnoww = tgll.getHours(); 
        // alert(tgl);
        if (date == formattedDatee) {
            a = [];
            $("#credit_tempatwaktuu").html("");
            $("#credit_tempatwaktuu").append("<input type='text' class='form-control' id='credit_time' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
            $("#credit_time").removeAttr('disabled');
            for (var i = hoursnoww+1; i < 24; i++) {                                                
                a.push(i);
            }
            console.warn(a);
            $('#credit_time').datetimepicker({                    
                sideBySide: true, 
                showClose: true,                   
                format: 'HH:mm',
                stepping: 15,
                enabledHours: a,
            });
        }
        else
        {
            $("#credit_tempatwaktuu").html("");
            $("#credit_tempatwaktuu").append("<input type='text' class='form-control' id='credit_time' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
            $("#credit_time").removeAttr('disabled');
            b = [];
            for (var i = 0; i < 24; i++) {                                                
                b.push(i);
            }
            console.warn(b);
            $('#credit_time').datetimepicker({                    
                sideBySide: true,                    
                format: 'HH:mm',
                showClose: true,
                stepping: 15,
                enabledHours: b,
            });
        }
        
    });

    $('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable();

    $('.nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

    $(".nominal").keyup(function() {        
        var clone = $(this).val();
        var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
        $("#nominalsave").val(cloned);
    });

    $('.credit_nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

    $(".nominal").keyup(function() {        
        var clone = $(this).val();
        var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
        $("#credit_nominalsave").val(cloned);
    });
    
    $("#simpankelas").click(function(e){
        $("#simpankelas").attr('disabled', true);
        var rate_value = null;
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        var idtutorni = "<?php echo $this->session->userdata('id_user');?>";
        var tanggal = $("#tanggal").val();      
        var time = $("#time").val()+":00";        
        var durasi = $("#durasi").val();
        var template = 'all_featured';        
        var nominal = $("#nominalsave").val();
        var pelajaran = $("#pelajaran").val();
        var topikpelajaran = $("#topikpelajaran").val();
        var user_utc = $("#user_utcc").val();
        if (tanggal=="") {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih tanggal kelas terlebih dahulu!!!");
        }
        else if (time=="undefined:00") {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih waktu kelas terlebih dahulu!!!");
        }
        else if (durasi == null) {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih durasi kelas terlebih dahulu!!!");
        }
        else if (template == null) {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih template kelas terlebih dahulu!!!");
        }
        else if (pelajaran == null) {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih pelajaran kelas terlebih dahulu!!!");
        }
        else if (topikpelajaran == "") {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon mengisi topik pelajarn kelas terlebih dahulu!!!");
        }
        else
        {
            $.ajax({
                url: '<?php echo base_url(); ?>Rest/createclass_paid/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    id_tutor: idtutorni,
                    tanggal: tanggal,
                    time: time,    
                    durasi:durasi,                
                    template: template,
                    pelajaran: pelajaran,
                    topikpelajaran: topikpelajaran,
                    harga: nominal,
                    user_device: 'web',
                    user_utc: user_utc
                },
                success: function(response)
                {                   
                    // alert(response['code'])
                    if (response['code'] == "200") {
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil Menambah Kelas");
                        setTimeout(function(){
                            window.location.replace("<?php echo base_url();?>");
                        },1000);
                    }else if(response['code'] == '403') {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', 'Maaf, anda tidak membuat kelas di waktu bersamaan.');
                        $("#simpankelas").removeAttr('disabled');
                        // setTimeout(function(){
                        //     location.reload();
                        // },2000);
                    }
                    else
                    {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Gagal Menambah Kelas");
                        setTimeout(function(){
                            location.reload();
                        },3000);
                    }
                }
            });
        }
        
    }); 

    $("#credit_simpankelas").click(function(e){
        $("#credit_simpankelas").attr('disabled', true);
        var rate_value = null;
        var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
        var idtutorni = "<?php echo $this->session->userdata('id_user');?>";
        var kuota_participants = $("#kuota_participants").val();
        var tanggal = $("#credit_tanggal").val();      
        var time = $("#credit_time").val()+":00";        
        var durasi = $("#credit_durasi").val();
        var template = 'all_featured';        
        var nominal = $("#credit_nominalsave").val();
        var pelajaran = $("#credit_pelajaran").val();
        var topikpelajaran = $("#credit_topikpelajaran").val();
        var user_utc = '<?php echo $this->session->userdata('user_utc');?>';
        if (kuota_participants==null) {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Jika tidak terdapat pilihan Kuota Participant. Mohon untuk membeli kuota kelas di menu.");
        }
        else if (tanggal=="") {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih tanggal kelas terlebih dahulu!!!");
        }
        else if (time=="undefined:00") {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih waktu kelas terlebih dahulu!!!");
        }
        else if (durasi == null) {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih durasi kelas terlebih dahulu!!!");
        }
        else if (template == null) {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih template kelas terlebih dahulu!!!");
        }
        else if (pelajaran == null) {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih pelajaran kelas terlebih dahulu!!!");
        }
        else if (topikpelajaran == "") {
            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon mengisi topik pelajarn kelas terlebih dahulu!!!");
        }
        else
        {
            $.ajax({
                url: '<?php echo base_url(); ?>Rest/createclass_paid/access_token/'+tokenjwt,
                type: 'POST',
                data: {
                    id_tutor: idtutorni,
                    kuota_participants : kuota_participants,
                    tanggal: tanggal,
                    time: time,    
                    durasi:durasi,                
                    template: template,
                    pelajaran: pelajaran,
                    topikpelajaran: topikpelajaran,
                    harga: nominal,
                    user_device: 'web',
                    user_utc: user_utc
                },
                success: function(response)
                {                   
                    // alert(response['code'])
                    if (response['code'] == "200") {
                        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil Menambah Kelas");
                        setTimeout(function(){
                            window.location.replace("<?php echo base_url();?>");
                        },1000);
                    }else if(response['code'] == '403') {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', 'Maaf, anda tidak membuat kelas di waktu bersamaan.');
                        $("#simpankelas").removeAttr('disabled');
                        // setTimeout(function(){
                        //     location.reload();
                        // },2000);
                    }
                    else
                    {
                        notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Gagal Menambah Kelas");
                        setTimeout(function(){
                            location.reload();
                        },3000);
                    }
                }
            });
        }
        
    });

}); 
    
</script>