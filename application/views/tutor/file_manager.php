<style type="text/css">
	/* CSS3 */

	/* The whole thing */
	.custom-menu {
	    display: none;
	    z-index: 1000;
	    position: absolute;
	    overflow: hidden;
	    border: 1px solid #CCC;
	    white-space: nowrap;	    
	    font-family: sans-serif;
	    background: #FFF;
	    color: #333;
	    border-radius: 5px;
	    padding: 0;
	}

	/* Each of the items in the list */
	.custom-menu li {
	    padding: 14px 40px;
	    cursor: pointer;
	    list-style-type: none;
	    transition: all .3s ease;
	    user-select: none;
	}

	.custom-menu li:hover {
	    background-color: #DEF;
	}

	/* The whole thing */
	.custom-choosemenu {
	    display: none;
	    z-index: 1000;
	    position: absolute;
	    overflow: hidden;
	    border: 1px solid #CCC;
	    white-space: nowrap;	    
	    font-family: sans-serif;
	    background: #FFF;
	    color: #333;
	    border-radius: 5px;
	    padding: 0;
	}

	/* Each of the items in the list */
	.custom-choosemenu li {
	    padding: 14px 40px;
	    cursor: pointer;
	    list-style-type: none;
	    transition: all .3s ease;
	    user-select: none;
	}

	.custom-choosemenu li:hover {
	    background-color: #DEF;
	}
</style>
<!-- <header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #db1f00;"> -->
    <?php $this->load->view('inc/navbar'); ?>
<!-- </header> -->

<div ng-show="loding" class="page-loader" style="background-color:black; opacity: 0.5;">
    <div class="preloader pl-lg pls-white">
        <svg class="pl-circular" viewBox="25 25 50 50">
            <circle class="plc-path" cx="50" cy="50" r="20" />
        </svg>

        <p style="margin-left: -18px;" class="f-17 m-t-20"><?php echo $this->lang->line('loading'); ?></p>
    </div>
</div>

<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('./inc/sidetutor'); ?>
	</aside>

	<section id="content">
		<div class="container">
			<div class="block-header">
				<ol class="breadcrumb">
                    <li><a href="#">My Files</a></li>
                    <li class="active" style="width: 300px;" id="nameFolder_select"></li>
                </ol>				
			</div>     
			<br>

			<img class="img-circle" id="btn_show" src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL; ?>baru/logo_plus.jpg" data-toggle="tooltip" data-placement="right" data-original-title="Create new class" title="Add" style="cursor: pointer; position: fixed; bottom:5%; right:8%; width: 50px; height: 50px; z-index: 5;">

			<?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>

			<div class="card w-post" id="box_folder">
				<div class="card-header">
                    <h2>Folders</h2> 
                    <ul class="actions" id="actions" style="display: none; margin-right: 2%;">                                               
                        <li>
                        	<a folder="" id="actions_hapus" style="cursor: pointer;"><i class="zmdi zmdi-delete zmdi-hc-3x"></i></a>
                        </li>
                    </ul>
                    <hr>            
                </div>
                <div class="card-body card-padding">
                	<div class="row" id="box_listfolder">
                		        
                    </div>
                </div>
			</div>

			<div class="card" id="box_file" style="display: none;">
				<div class="card-header">					
					<h2><i class="zmdi zmdi-arrow-back zmdi-hc-lg" style="cursor: pointer;" id="back_choosefolder"></i> Files</h2>
					<ul class="actions" id="actions2" style="display: none; margin-right: 2%;">                        
                        <li>
                        	<a folder="" file="" id="actions_lihat2" style="margin-right: 5%; cursor: pointer;"><i class="zmdi zmdi-eye zmdi-hc-3x"></i></a>
                        </li>
                        <li>
                        	<a folder="" file="" id="actions_hapus2" style="cursor: pointer;"><i class="zmdi zmdi-delete zmdi-hc-3x"></i></a>
                        </li>
                    </ul>
					<hr>
				</div>
				<div class="card-body card-padding">
					<div class="contacts clearfix row" id="box_listfile">                        
                       
                    </div>
				</div>
			</div>		
          	
          	<!-- OPEN FILE -->
          	<input type="file" id="choose_file" name="choose_file[]" style="display:none" accept="image/x-png,image/gif,image/jpeg">
          	<input type="file" id="choose_folder" name="choose_folder" webkitdirectory mozdirectory style="display:none">
          	<textarea id="base64box" style="display: none;"></textarea>

          	<ul class='custom-menu'>
			  	<li data-action="folder_baru" id="right_folder" style="width: 250px;"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL ?>baru/filemanager/add.png" style="height: 1%; width: 13%; margin-right: 2%;"> Folder Baru</li>
			  	<li data-action="upload_file" id="right_file" style="width: 250px; display: none;"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL ?>baru/filemanager/file.png" style="height: 1%; width: 13%; margin-right: 2%;"> Upload File</li>
			  	<!-- <li data-action="upload_folder" style="width: 250px;"><img src="<?php echo CDN_URL.STATIC_IMAGE_CDN_URL ?>baru/filemanager/folderupload.png" style="height: 1%; width: 13%; margin-right: 2%;"> Upload Folder</li> -->
			</ul>

			<!-- Modal FOLDER BARU -->  
            <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="mdl_folderbaru" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header" style="height: 10px;">
                            <h4 class="modal-title c-black">Folder Baru</h4>
                            <!-- <h4 class="pull-right" style="margin-top: -3%;">Saldo anda : Rp. 1 M</h4>                             -->
                        </div> 
                        <div class="modal-body">
                            <hr>
                            <p><input type="text" name="name_folder" id="name_folder" class="c-black form-control" required style="border: 1px solid #EEEEEE; padding: 5px;" value="" placeholder="Nama Folder disini ..." /></p>
                        </div>                                               
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Batal</button>
                            <a id="proccess" href=""><button type="button" class="btn bgm-green" id="confrim_create">Buat</button></a>
                        </div>                        
                    </div>
                </div>
            </div>

            <!-- Modal DELETE FOLDER -->  
            <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="mdl_alertdeletefolder" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header" style="height: 10px;">
                            <h4 class="modal-title c-black">Pemberitahuan</h4>                            
                        </div> 
                        <div class="modal-body">
                            <hr>
                            <p class="c-black f-16">Anda yakin ingin menghapus Folder tersebut?</p>
                            <p class="c-gray">Notes : Jika anda menghapsu folder, maka file yang terdapat dalam folder tersebut akan terhapus semua</p>
                            <input type="text" id="name_folder_delete" hidden="true">
                        </div>                                               
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Batal</button>
                            <a id="proccess" href=""><button type="button" class="btn bgm-green" id="confrim_delete_folder">Hapus</button></a>
                        </div>                        
                    </div>
                </div>
            </div>

            <!-- Modal DELETE FILE -->  
            <div class="modal" style="margin-top: 13%; margin-left: 13%; margin-right: 13%;" id="mdl_alertdeletefile" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header" style="height: 10px;">
                            <h4 class="modal-title c-black">Pemberitahuan</h4>                            
                        </div> 
                        <div class="modal-body">
                            <hr>
                            <p class="c-black c-16">Anda yakin ingin menghapus file tersebut?</p>
                            <input type="text" id="file_folder" hidden="true">
                            <input type="text" id="file_f" hidden="true">
                        </div>                                               
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Batal</button>
                            <a id="proccess" href=""><button type="button" class="btn bgm-green" id="confrim_delete_file">Hapus</button></a>
                        </div>                        
                    </div>
                </div>
            </div>

            <!-- Modal VIEW FILE -->  
            <div class="modal" id="mdl_viewfile" data-modal-color="" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header" style="height: 10px;">
                            <h4 class="modal-title c-black" id="view_namefile"></h4>                            
                        </div> 
                        <div class="modal-body">
                            <hr>
                            <img src="" id="view_filesrc" style="height: 100%; width: 100%;">
                        </div>                                               
                        <div class="modal-footer">
                            <button type="button" class="btn bgm-white c-gray" data-dismiss="modal">Tutup</button>                            
                        </div>                        
                    </div>
                </div>
            </div>

		</div>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	var folder_active = null;
	var tutor = "<?php echo $this->session->userdata('id_user');?>";
	$.ajax({
		url: "<?php echo BASE_URL();?>FileManager/listFolder",
		type: 'GET',
		success: function(data){
			data = JSON.parse(data);
			list_folder = data['list_folder'];			
			if(data['status'] == 1){
				var box_listfolder = "";
				for (var i = 0; i < list_folder.length; i++) {					
					box_listfolder = "<div class='col-md-4 col-xs-4'>"
	                	+"<div class='wi-comments'>"
	                        +"<div class='list-group waves-effect eventClickFolder' id='good_button_"+i+"' data-no='"+i+"' data-folder='"+list_folder[i]+"'>"
	                            +"<div class='list-group-item media' id='box_block_active_"+i+"'>"
	                            	+"<div class='col-md-3 col-xs-3'>"
	                                	+"<a href='#' class='pull-left'><img src='<?php echo CDN_URL.STATIC_IMAGE_CDN_URL ?>baru/foldermanager.png' alt='' class='lgi-img' draggable='true' data-bukket-ext-bukket-draggable='true' style='height: 100%; width: 100%; margin-top: 16%;'></a>"
	                                +"</div>"
	                                +"<div class='col-md-9 col-xs-9'>"
		                                +"<div class='media-body'>"
		                                	+"<br>"
		                                    +"<a href='#' class='lgi-heading f-13 c-gray'>"+list_folder[i]+"</a>"
		                                +"</div>"
	                                +"</div>"
	                            +"</div>"
	                        +"</div>"
	                    +"</div>"
                    +"</div>";
                    $("#box_listfolder").append(box_listfolder);
				}
				
			}else{
				box_listfolder = "<div class='col-md-12 col-xs-12'><div class='alert alert-info' role='alert'><center>Tidak ada folder, silahkan untuk tambah folder</center></div></div>";	
				$("#box_listfolder").append(box_listfolder);
			}
		}
	});

	var DELAY = 700,
	    clicks = 0,
	    timer = null;
	var idac = "";

	$(document.body).on('click', '.eventClickFolder' ,function(e){
		// var idinin = $(this).attr('id');				
		$("#box_block_active_"+idac).css('background-color','');
		var idinin = $(this).data('no');	
		var folder = $(this).data('folder');        
		clicks++;  //count clicks

        if(clicks === 1) {
        	timer = setTimeout(function() {
        		$("#actions_lihat").attr('folder', folder);
        		$("#actions_hapus").attr('folder', folder);
        		$("#actions").css('display','block');                
        		$("#box_block_active_"+idinin).css('background-color','#ececec');
        		idac = idinin;
                clicks = 0;  //after action performed, reset counter
            }, DELAY);
		}
		else {
            clearTimeout(timer);
            $("#actions_lihat").removeAttr('folder');
        	$("#actions_hapus").removeAttr('folder');
			folder_active = folder;
			$("#actions").css('display','none');
			$("#box_folder").css('display','none');
			$("#nameFolder_select").text(folder);
			$("#right_folder").css('display','none');		
			$("#box_listfile").empty();	
			var box_listfile = "";	
			$.ajax({
				url: "<?php echo BASE_URL();?>FileManager/list_file",
				data: {
					folder: folder
				},
				type: 'POST',
				success: function(data){
					data = JSON.parse(data);
					list_file = data['list_file'];
					if (data['status'] == 1) {
						$("#box_file").css('display','block');
						
						for (var i = 0; i < list_file.length; i++) {					
							box_listfile = "<div class='col-md-2 col-sm-2 col-xs-6 eventDeleteFile' data-no='"+i+"' data-file='"+list_file[i]+"' data-folder='"+folder+"'>"
	                            +"<div class='c-item' id='box_file_active_"+i+"'>"
	                                +"<a href='#' class='ci-avatar'><center>"
	                                    +"<img src='https://classmiles.com/ClassDrive/indiclass/"+tutor+"/"+folder_active+""+list_file[i]+"' style='height:150px; width:100px; margin-top:2%;' alt='' draggable='true' data-bukket-ext-bukket-draggable='true'></center>"
	                                +"</a>"

	                                +"<div class='c-footer'>"
	                                    +"<button class='waves-effect'><i class='zmdi zmdi-image'></i> "+list_file[i];
	                                    +"</button>"
	                                +"</div>"
	                            +"</div>"
	                        +"</div>";
	                        $("#box_listfile").append(box_listfile);
						}
													
					}
					else
					{
						box_listfile = "<div class='col-md-12 col-xs-12'><div class='alert alert-info' role='alert'><center>Tidak ada file disini, silahkan untuk menambah file</center></div></div>";
						$("#box_listfile").append(box_listfile);
						$("#box_folder").css('display','none');
						$("#box_file").css('display','block');								
					}
				}
			});

            clicks = 0;  //after action performed, reset counter
        }
	});

	var DELAY2 = 700,
	    clicks2 = 0,
	    timer2 = null;
	var idacc = "";
	$(document.body).on('click', '.eventDeleteFile' ,function(e){
		var namefile= $(this).data('file');
		var folder 	= $(this).data('folder');	
		var idini 	= $(this).data('no');
		$("#box_file_active_"+idacc).css('background-color','');
		clicks2++;  //count clicks
		if(clicks2 === 1) {
        	timer2 = setTimeout(function() {
        		$("#actions_lihat2").attr('folder', folder);
        		$("#actions_lihat2").attr('file', namefile);
        		$("#actions_hapus2").attr('folder', folder);
        		$("#actions_hapus2").attr('file', namefile);
        		$("#actions2").css('display','block');        		
        		$("#box_file_active_"+idini).css('background-color','#ececec');
        		idacc = idini;
                clicks2 = 0;  //after action performed, reset counter
            }, DELAY2);
		}
		else {
            clearTimeout(timer2);  
            clicks2 = 0;
            var tutor = "<?php echo $this->session->userdata('id_user');?>";	
			$("#view_namefile").text(namefile);
			$("#view_filesrc").attr('src',"https://classmiles.com/ClassDrive/"+tutor+"/"+folder+""+namefile+"");
			$("#mdl_viewfile").modal("show");
        }
	});	

	$(document.body).on('click', '#actions_hapus' ,function(e){
		var namefolder = $(this).attr('folder');		
		$("#name_folder_delete").val(namefolder);
		$("#mdl_alertdeletefolder").modal("show");
	});

	$(document.body).on('click', '#confrim_delete_folder' ,function(e){
		var namefolder = $('#name_folder_delete').val();
		angular.element(document.getElementById('confrim_delete_folder')).scope().loadings();
		$.ajax({
			url: "<?php echo BASE_URL();?>FileManager/deleteFolder",
			data: {
				namefolder: namefolder
			},
			type: 'POST',
			success: function(data){
				data = JSON.parse(data);
				if (data['status'] == 1) {	
					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Folder terhapus");					
					setTimeout(function(){						
						window.location.replace("<?php echo base_url();?>tutor/FileManager");
					},2000);
				}
				else
				{			
					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Gagal menghapus Folder");		
					setTimeout(function(){						
						angular.element(document.getElementById('confrim_delete_folder')).scope().loadingsclose();
						window.location.replace("<?php echo base_url();?>tutor/FileManager");
					},2000);			
				}
			}
		});
	});

	$(document.body).on('click', '#actions_hapus2' ,function(e){
		var namefolder = $(this).attr('folder');		
		var file = $(this).attr('file');		
		$("#file_folder").val(namefolder);
		$("#file_f").val(file);
		$("#mdl_alertdeletefile").modal("show");
	});

	$(document.body).on('click', '#confrim_delete_file' ,function(e){		
		var namefolder = $('#file_folder').val();		
		var file = $('#file_f').val();
		$("#mdl_alertdeletefile").modal('hide');
		angular.element(document.getElementById('confrim_delete_file')).scope().loadings();
		$.ajax({
			url: "<?php echo BASE_URL();?>FileManager/deleteImage",
			data: {
				folder: namefolder,
				filename: file
			},
			type: 'POST',
			success: function(data){
				data = JSON.parse(data);
				if (data['status'] == 1) {	
					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "File terhapus");				
					setTimeout(function(){						
						window.location.replace("<?php echo base_url();?>tutor/FileManager");
					},2000);
				}
				else
				{					
					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Gagal menghapus file");
					setTimeout(function(){	
						angular.element(document.getElementById('confrim_delete_file')).scope().loadingsclose();					
						window.location.replace("<?php echo base_url();?>tutor/FileManager");
					},2000);			
				}
			}
		});
	});

	$(document.body).on('click', '#confrim_create' ,function(e){
		var namefolder = $('#name_folder').val();
		$("#mdl_folderbaru").modal('hide');
		angular.element(document.getElementById('confrim_create')).scope().loadings();
		$.ajax({
			url: "<?php echo BASE_URL();?>FileManager/createFolder",
			data: {
				namefolder: namefolder
			},
			type: 'POST',
			success: function(data){
				notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil Menambah Folder Baru");
				setTimeout(function(){
					angular.element(document.getElementById('confrim_create')).scope().loadingsclose();
					window.location.replace("<?php echo base_url();?>tutor/FileManager");
				},2000);				
			}
		});
	});

	$(document.body).on('click', '#actions_lihat2' ,function(e){
		var tutor = "<?php echo $this->session->userdata('id_user');?>";
		var namefolder = $(this).attr('folder');		
		var file = $(this).attr('file');	
		$("#view_namefile").text(file);
		$("#view_filesrc").attr('src',"https://classmiles.com/ClassDrive/indiclass/"+tutor+"/"+namefolder+""+file+"");
		$("#mdl_viewfile").modal("show");
	});

	$(document.body).on('click', '#back_choosefolder' ,function(e){
		$("#box_file").css('display','none');
		$("#box_folder").css('display','block');
		$("#nameFolder_select").text("");
		$("#right_folder").css('display','block');
		folder_active = null;
	});
	
	$("#btn_show").click(function(){
		if (folder_active == null) {
			xtop = event.pageY-100;
			$("#right_file").css('display','none');
		}
		else
		{
			xtop = event.pageY-100;
			$("#right_file").css('display','block');
		}
		$(".custom-menu").finish().toggle(100).

		// In the right position (the mouse)		
		css({
			top: xtop + "px",
			left: (event.pageX-250) + "px"
		});
	});
	
	$(document).bind("contextmenu", function (event) {
		if (folder_active == null) {
			$("#right_file").css('display','none');
		}
		else
		{
			$("#right_file").css('display','block');
		}

		// Avoid the real one
		event.preventDefault();		

		// Show contextmenu
		$(".custom-menu").finish().toggle(100).

		// In the right position (the mouse)
		css({
			top: event.pageY + "px",
			left: event.pageX + "px"
		});

	});

	// If the document is clicked somewhere
	$(document).bind("mousedown", function (e) {
		// If the clicked element is not the menu
		if (!$(e.target).parents(".custom-menu").length > 0) {

			// Hide it
			$(".custom-menu").hide(100);
		}
	});

	// If the menu element is clicked
	$(".custom-menu li").click(function(){

		// This is the triggered action name
		var actionclick = $(this).attr('data-action');
		if (actionclick == "folder_baru") {
			$("#mdl_folderbaru").modal("show");
		}
		else if (actionclick == "upload_file") {
			document.getElementById("choose_file").click();
		}
		else if (actionclick == "upload_folder") {			
			document.getElementById("choose_folder").click();
		}		
		// Hide it AFTER the action was triggered
		$(".custom-menu").hide(100);
	});

	var inps = document.getElementById("choose_folder");
	[].forEach.call(inps, function(inp) {
	  	inp.onchange = function(e) {
	    	console.log(this.files);
	  	};
	});	

	$("#choose_folder").change( function() {
		var names = [];
	    for (var i = 0; i < $(this).get(0).files.length; ++i) {
	        names.push($(this).get(0).files[i].name);
	    }
	    console.log(names);
	    // $("input[name=file]").val(names);
	});

	function getBase64(file) {
		var reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = function (e) {
			// console.log(reader.result);
			var cuk = e.target.result;	
			// cuk = cuk.split(";base64,")[1];  
			$("#base64box").empty();   
			$("#base64box").val(cuk);
		};
		reader.onerror = function (error) {
			console.log('Error: ', error);
		};
	}

	$('#choose_file').on("change", function(){
		angular.element(document.getElementById('choose_file')).scope().loadings();
		var file = document.getElementById('choose_file').files;
		var imgfile = null;
        var filename = file[0].name;        
        var filesize = file[0].size;         
        if (file.length > 0) {
        	if(file && filesize < 2000000) {
        		getBase64(file[0]);
        		setTimeout(function(){
		    		imgfile = $("#base64box").val();		   
					$.ajax({
						url: '<?php echo base_url();?>FileManager/saveTempImage',
						type: 'POST',
						data: {							
							imgfile : imgfile,
							imgname : filename,
							folder : folder_active
			            }, 
						success: function(data){
							notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Upload file berhasil");
							setTimeout(function(){
								angular.element(document.getElementById('choose_file')).scope().loadingsclose();
								window.location.replace("<?php echo base_url();?>tutor/FileManager");
							},2000);						
						}
					});
		    	},1500);
        	}
	        else
	        {
	        	console.warn('kebesaran');
	        }
        }
        else
        {
        	console.warn('error');
        }		
	});


</script>