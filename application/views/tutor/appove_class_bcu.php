<div class="d-flex flex-column h-100">
    
    <!-- Navbar -->
        <?php $this->load->view('inc/navbar_tutor');?>
    <!-- // END Navbar -->

    <div class="mdk-drawer-layout js-mdk-drawer-layout flex" data-fullbleed data-push data-has-scrolling-region>
        <div class="mdk-drawer-layout__content mdk-drawer-layout__content--scrollable">
            <div class="container">

                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="instructor-dashboard.html">Home</a></li>
                    <li class="breadcrumb-item active">Courses</li>
                </ol>
                <h1 class="page-heading h2">Request Courses</h1>
                
                <div class="row" id="box_listRequestTutor">                
                    
                </div>

                <div id="box_listclassesnull"></div>
            </div>        

        </div>

        <div class="modal fade" id="modal_alert" style="margin-top: 10%;"  data-backdrop="static" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content" id="modal_konten">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" id="colorJudul"><b>The Rote Less Travelled</b></h3>
                    </div>
                    <div class="modal-body">
                        <label id="text_modal"></label>
                    </div>
                    <div class="modal-footer">
                        <button  id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                    </div>                
                </div>
            </div>
        </div>

        <div class="modal fade show" id="modal_approve_tutor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: block;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Approval</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
                    </div>
                    <div class="modal-body">
                        <p>You get a class request from <b id='nama_channel'></b></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal_approve_tutor" style="margin-top: 10%;"  data-backdrop="static" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="height: 10px;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="zmdi zmdi-close"></i></span></button>
                        <h4 class="modal-title c-black">Persetujuan</h4>
                    </div> 
                    <div class="modal-body">
                        <div class="row">
                            <hr>
                            <div class="header" align="center">
                                <label class="c-black f-17">Anda mendapat permintaan kelas dari Channel <b id='nama_channel'></b></label><br><br>  
                            </div>
                            <div class="col-md-12 c-black" style="">
                                <div class="col-md-3">
                                    <img class="img-rounded" id='logo_channel' onerror="<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=" style="height: 120px; width: 120px; background-color: #008080;">
                                </div>
                                <div class="col-md-3" style="padding-right: 0;">
                                    <label>Nama Program</label>
                                    <label>Waktu Permintaan</label><br>
                                    <label>Mata Pelajaran</label><br>
                                    <label>Topik</label><br>
                                    <label>Durasi Kelas</label><br>
                                </div>
                                <div class="col-md-5" style="padding: 0;">
                                    <b>: </b><label id="nama_program"></label><br>
                                    <b>: </b><label id="waktu_permintaan"></label><br>
                                    <b>: </b><label id="mata_pelajaran"></label><br>
                                    <b>: </b><label id="topik"></label><br>
                                    <b>: </b><label id="durasi_kelas"></label><br>
                                </div>
                            </div>
                        </div>
                    </div>                                               
                    <div class="modal-footer">
                        <button type="button" reqid="" ids="" id="" class="btn btn-link reject_req c-red">Tolak</button>
                        <button type="button" reqid="" ids="" id="" class="btn approve_req bgm-green">Setuju</button>
                    </div>                        
                </div>
            </div>
        </div>

        <?php $this->load->view('inc/sidebar_tutor');?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        // $(".select2").select2();        
        var tgl = new Date(); 
        var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc; 
        var access_token = "<?php echo $this->session->userdata('access_token');?>";
        var id_user = "<?php echo $this->session->userdata('id_user');?>";

        $("#modal_approve_tutor").modal('show');
        $.ajax({
            url: 'https://air.classmiles.com/v1/requestClassTutorByChannel/',
            type: 'POST',       
            data: {
                tutor_id: id_user
            },         
            success: function(response)
            {           
                // console.log(response);
                var box_listRequestTutor = "";
                if (response.data == null) {
                }
                else
                {                                                                                
                    $("#modal_approve_tutor").modal('show');                                        
                  
                    var program_id   = response.data['program_id'];
                    var channel_id   = response.data['channel_id'];
                    var sesi   = response.data['sesi'];
                    var group_id   = response.data['group_id'];
                    var program_name   = response.data['program_name'];
                    var program_id   = response.data['program_id'];
                    var channel_name = response.data['channel_name'];
                    var schedule = response.data['schedule'];
                    var channel_logo = "<?php echo CDN_URL.USER_IMAGE_CDN_URL; ?>"+response.data['channel_logo'];                        
                    for (var i = 0; i < schedule.length; i++) {
                        var eventDate   = schedule[i]['eventDate'];
                        var eventTime   = schedule[i]['eventTime'];
                        var eventDurasi = schedule[i]['eventDurasi'];
                        var tutor_id    = schedule[i]['tutor_id'];
                        var subject_id  = schedule[i]['subject_id'];
                        var desc        = schedule[i]['desc'];
                        var jenjangnamelevel  = schedule[i]['jenjangnamelevel'];
                        var subject_name= schedule[i]['subject_name'];
                        var status      = schedule[i]['status'];

                        var hours = parseInt( eventDurasi / 3600 );
                        var minutes = parseInt( (eventDurasi - (hours * 3600)) / 60 );
                        var seconds = Math.floor((eventDurasi - ((hours * 3600) + (minutes * 60))));
                        if (hours == 0) {
                            var durasi = (minutes < 10 ? "0" + minutes : minutes) + " Menit";   
                        }
                        else if (minutes == 0) {
                            var durasi = (hours < 10 ? "" + hours : hours) + " Jam ";
                        }
                        else{
                            var durasi = (hours < 10 ? "" + hours : hours) + " Jam " + (minutes < 10 ? "0" + minutes : minutes) + " Menit"; 
                        }

                        if (tutor_id == id_user && status == 2) {
                            box_listRequestTutor += " <div class='col-md-4'>"+
                                "<div class='card'>"+
                                    "<div class='card-header bg-white'>"+
                                        "<div class='row'>"+
                                            "<div class='col-md-4'>"+
                                                "<img src='"+channel_logo+"' width='70' class='rounded-circle'>"+
                                            "</div>"+
                                            "<div class='col-md-8'>"+
                                                "<h4 class='card-title'><a href='#'>"+program_name+"</a></h4>"+
                                                "<small class='text-muted'>"+subject_name+" - "+jenjangnamelevel+"</small><br>"+
                                                "<small class='text-muted'>"+desc+"</small><br>"+
                                                "<small class='text-muted'>"+eventDate+" "+eventTime+"</small> | <small class='text-muted'>"+durasi+"</small>"+
                                            "</div>"+
                                        "</div>"+
                                    "</div>"+
                                    "<div class='progress rounded-0'>"+
                                        "<div class='progress-bar progress-bar-striped bg-success' role='progressbar' style='width: 100%' aria-valuenow='40' aria-valuemin='0' aria-valuemax='100'></div>"+
                                    "</div>"+
                                    "<div class='card-footer bg-white'>"+
                                        "<a href='#' class='btn btn-danger btn-sm reject_req' data-eventdate='"+eventDate+"' data-eventtime='"+eventTime+"' data-eventdurasi='"+eventDurasi+"' data-programid='"+program_id+"' data-channelid='"+channel_id+"' data-subjectid='"+subject_id+"'>Reject <i class='fa fa-close btn__icon--right'></i></a>"+
                                        "<a href='#' class='btn btn-success btn-sm approve_req' data-eventdate='"+eventDate+"' data-eventtime='"+eventTime+"' data-eventdurasi='"+eventDurasi+"' data-programid='"+program_id+"' data-channelid='"+channel_id+"' data-subjectid='"+subject_id+"' style='margin-left:10px;'>Continue <i class='material-icons btn__icon--right'>check</i></a>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
                        }
                    }                                            

                    $("#box_listRequestTutor").append(box_listRequestTutor); 

                    if (box_listRequestTutor == "") {
                        box_listRequestTutor += "<div class='alert alert-info' role='alert'>"+
                            "<strong>No Request!</strong> This alert needs your attention, but it's not super important."+
                        "</div>";
                        $("#box_listRequestTutor").append(box_listRequestTutor); 
                    }                                           
                }
            }
        });
    
        $(document).on("click", ".reject_req", function () {
            var program_id      = $(this).data('programid');
            var channel_id      = $(this).data('channelid');
            var subject_id      = $(this).data('subjectid');
            var eventDate       = $(this).data('eventdate');
            var eventTime       = $(this).data('eventtime');
            var eventDurasi     = $(this).data('eventdurasi');

            $.ajax({
                url: 'https://air.classmiles.com/v1/reject_RequestFromTutor/',
                type: 'POST',       
                data: {
                    tutor_id: id_user,
                    program_id: program_id,
                    channel_id: channel_id,
                    eventDate: eventDate,
                    eventTime: eventTime,
                    eventDurasi: eventDurasi
                },         
                success: function(response)
                {
                    var a = JSON.stringify(response);             
                    $("#modal_alert").modal('show');
                    $("#modal_konten").css('background-color','#32c787');
                    $("#colorJudul").css('color','#FFFFFF');
                    $("#text_modal").css('color','#FFFFFF');
                    $("#text_modal").html("Sukses, Anda telah MENOLAK permintaan kelas, dan akan kami sampaikan kepada Admin Channel");
                    $("#button_ok").click( function(){
                        location.reload();
                    });
                }
            });
        });

        $(document).on("click", ".approve_req", function () {
            var program_id      = $(this).data('programid');
            var channel_id      = $(this).data('channelid');
            var subject_id      = $(this).data('subjectid');
            var eventDate       = $(this).data('eventdate');
            var eventTime       = $(this).data('eventtime');
            var eventDurasi     = $(this).data('eventdurasi');

            $.ajax({
                url: 'https://air.classmiles.com/v1/accept_RequestFromTutor/',
                type: 'POST',       
                data: {
                    tutor_id: id_user,
                    program_id: program_id,
                    channel_id: channel_id,
                    subject_id: subject_id,
                    eventDate: eventDate,
                    eventTime: eventTime,
                    eventDurasi: eventDurasi,
                    user_utc: user_utc
                },         
                success: function(response)
                {
                    var a = JSON.stringify(response); 
                    if (response['code'] == "200") {                        
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','#32c787');
                        $("#colorJudul").css('color','#FFFFFF');
                        $("#text_modal").css('color','#FFFFFF');
                        $("#text_modal").html("Sukses, persetujuan Anda akan kami sampaikan kepada Admin Channel");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                    }
                    else if(response['code'] == "403"){                        
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','red');
                        $("#colorJudul").css('color','#FFFFFF');
                        $("#text_modal").css('color','#FFFFFF');
                        $("#text_modal").html("Mohon maaf, kelas tersebut bentrok dengan jadwal anda yang lain.");
                        $("#button_ok").click( function(){
                            location.reload();
                        });
                    }
                    else if(response['code'] == "-100"){                        
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','red');
                        $("#colorJudul").css('color','#FFFFFF');
                        $("#text_modal").css('color','#FFFFFF');
                        $("#text_modal").html("Mohon maaf, terjadi kesalahan.");
                        $("#button_ok").click( function(){
                            location.reload();
                        });   
                    }
                    else if(response['code'] == "-400"){                        
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','red');
                        $("#colorJudul").css('color','#FFFFFF');
                        $("#text_modal").css('color','#FFFFFF');
                        $("#text_modal").html("Mohon maaf, terjadi kesalahan.");
                        $("#button_ok").click( function(){
                            location.reload();
                        }); 
                    }
                    else
                    {                        
                        $("#modal_alert").modal('show');
                        $("#modal_konten").css('background-color','red');
                        $("#colorJudul").css('color','#FFFFFF');
                        $("#text_modal").css('color','#FFFFFF');
                        $("#text_modal").html("Mohon maaf, terjadi kesalahan.");
                        $("#button_ok").click( function(){
                            location.reload();
                        }); 
                    }
                }
            });
        });            

    });
</script>