<style>
    .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 5px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 250px;
        height: 250px;
    }

    .cropit-preview-image-container {
        cursor: move;
    }

    .cropit-preview-background {
        opacity: .2;
        cursor: auto;
    }

    .image-size-label {
        margin-top: 10px;
    }
</style>
<div class="d-flex flex-column h-100">
    
    <!-- Navbar -->
        <?php $this->load->view('inc/navbar_tutor');?>
    <!-- // END Navbar -->

    <div class="mdk-drawer-layout js-mdk-drawer-layout flex" data-fullbleed data-push data-has-scrolling-region>
        <div class="mdk-drawer-layout__content mdk-drawer-layout__content--scrollable">
            <div class="container">

                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="student-dashboard.html">Home</a></li>
                    <li class="breadcrumb-item active">Data diri</li>
                </ol>

                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="#first" data-toggle="tab">Data diri</a>
                        </li>
                    </ul>
                    <div class="tab-content card-body">
                        <div class="tab-pane active" id="first">                            
                            <div class="form-group row">
                                <label for="avatar" class="col-sm-3 c ol-form-label">Foto</label>
                                <div class="col-sm-9 col-md-9">
                                    <div class="media">
                                        <div class="media-left">
                                            <img src="" onerror="this.src='<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>dXNlci9lbXB0eS5qcGc=';" id="profile_image" style="width: 150px; height: 150px;">
                                        </div>
                                        <div class="image-editor">
                                            <input accept="image/x-png,image/gif,image/jpeg"  type="file" required name="inputFile" id="inputFile" data-placeholder="No Selected" class="filestyle cropit-image-input">
                                            <input type="text" name="image_new" id="image_new" hidden>

                                            <center>
                                              <div class="cropit-preview"></div>
                                            </center>
                                            <div class="image-size-label">
                                                <label class="c-gray uploadImage">Resize image</label> 
                                                <button class="zoomOut btn btn-sm btn-default"><i class="fa fa-minus-square"></i>  Zoom Out</button>
                                                <button class="zoomIn btn btn-sm btn-default"><i class="fa fa-plus-square"></i>  Zoom In</button>
                                            </div>
                                            <input type="range" class="cropit-image-zoom-input">
                                        </div> 
                                    </div>                                           
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="website" class="col-sm-3 col-form-label">Nama Depan & Nama Belakang</label>
                                <div class="col-sm-9 col-md-9">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input-group">                                                        
                                                <input type="text" class="form-control" placeholder="Isi nama depan" id="profile_first_name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group">                                                        
                                                <input type="text" class="form-control" placeholder="Isi nama belakang" id="profile_last_name">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="website" class="col-sm-3 col-form-label">Jenis Kelamin & Negara</label>
                                <div class="col-sm-9 col-md-9">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input-group">                                                        
                                                <select class="custom-control custom-select form-control" id="profile_gender">
                                                    <option value="Male">Pria</option>
                                                    <option value="Female">Wanita</option>                                                
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group">                                                        
                                                <select class="custom-control custom-select form-control" id="profile_nationality">
                                                </select>                                                    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="website" class="col-sm-3 col-form-label">Email & Tanggal Lahir</label>
                                <div class="col-sm-9 col-md-9">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input-group">                                                        
                                                <input type="text" class="form-control" placeholder="Email Address" disabled id="profile_email">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group">                                                        
                                                <input class="datepicker form-control" type="text" value="<?php echo date("Y-m-d");?>" id="profile_date">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>       
                            <div class="form-group row">
                                <label for="website" class="col-sm-3 col-form-label">Umur & No Telp</label>
                                <div class="col-sm-9 col-md-9">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="input-group">                                                        
                                                <input type="text" class="form-control" placeholder="Umur" id="profile_age">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="input-group">                                                        
                                                <select class="custom-control custom-select form-control" id="profile_kode_area">
                                                </select>                                                    
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="input-group">                                                       
                                                <input type="text" class="form-control" placeholder="Isi no telp " id="profile_callnum">                                                   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                             
                            <div class="form-group row">
                                <label for="password" class="col-sm-3 col-form-label">Alamat </label>
                                <div class="col-sm-9 col-md-9">
                                    <div class="input-group">                                                
                                        <input type="text" class="form-control" placeholder="Isi alamat" id="profile_user_address">
                                    </div>
                                </div>
                            </div> 
                            <div class="form-group row">
                                <label for="password" class="col-sm-3 col-form-label"> </label>
                                <div class="col-sm-9 col-md-9">
                                    <div class="input-group">                                                
                                        <button class="btn btn-info btn-block" id="btnSavedProfile">Simpan</button>
                                    </div>
                                </div>
                            </div>                                                                
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <div class="animated fadeIn modal fade" style="color: white; margin-top: 5%;" id="modal_alert" data-modal-color="red"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" >
                <div class="modal-content" id="modal_konten">
                    <div class="modal-body" align="center">
                        <label id="text_modal"></label><br>
                        <button  id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                    </div>
                </div>
            </div>
        </div>

        <?php $this->load->view('inc/sidebar_tutor');?>

    </div>
</div>

<script src="../assets/Cropit/dist/jquery.cropit.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-filestyle/2.1.0/bootstrap-filestyle.js"></script>

<script type="text/javascript">
    $(document).ready(function() {        
        var tgl = new Date(); 
        var user_utc = new Date().getTimezoneOffset();
        user_utc = -1 * user_utc; 
        var access_token = "<?php echo $this->session->userdata('access_token');?>";
        var iduser = "<?php echo $this->session->userdata('id_user');?>";
        var channel_id = "<?php echo $this->session->userdata('channel_id');?>";
        var member_ship = "<?php echo $this->session->userdata('member_ship');?>";
        var dataSet = [];

        $('#profile_date').datepicker('setDate', new Date());

        // var status_member = null;
        if (member_ship == 'basic') {
            $("#klik_premium").removeAttr('disabled');
        }
        else
        {
            $("#klik_premium").attr('disabled','true');
        }

        $(document).on('click','.zoomOut',function(){
            changeZoom();
        });

        $(document).on('click','.zoomIn',function(){
            changeZoom(true);
        });

        function changeZoom(increase){
            var elm = $('.image-editor');
            var zoom = elm.cropit('zoom');
            zoom = increase ? zoom + 0.05 : zoom - 0.05;
            elm.cropit('zoom', zoom);
        }

        $('.image-editor').cropit({
            exportZoom: 1.25,
            imageBackground: true,
            imageBackgroundBorderWidth: 20,
        }); 

        $.ajax({
            url: '<?php echo AIR_API;?>detail_student/access_token/'+access_token,
            type: 'POST',
            data: {                
                id_user : iduser
            },
            success: function(response)
            {
                if (response['code'] == -400) {
                    window.location.href='<?php echo base_url();?>Admin/Logout';
                }
                var a = JSON.stringify(response);
                // alert(response['data']['user_name']);

                var user_name       = response['data']['user_name'];
                var first_name      = response['data']['first_name'];
                var last_name       = response['data']['last_name'];
                var email           = response['data']['email'];
                var user_address    = response['data']['user_address'];

                var user_age        = response['data']['user_age'];
                var user_gender     = response['data']['user_gender'];
                var user_birthdate  = response['data']['user_birthdate'];                
                var user_nationality= response['data']['user_nationality'];
                var user_countrycode= response['data']['user_countrycode'];
                var user_callnum    = response['data']['user_callnum'];
                var user_address    = response['data']['user_address'];
                var user_image      = response['data']['user_image'];
                var kotak_image     = "<?php echo CDN_URL.USER_IMAGE_CDN_URL;?>"+user_image;
                $("#profile_first_name").val(first_name);
                $("#profile_last_name").val(last_name);
                $("#profile_date").val(user_birthdate);
                $("#profile_age").val(user_age);
                $("#profile_gender").val(user_gender);
                $("#profile_home").val(user_address);
                $("#profile_email").val(email);
                $("#profile_callnum").val(user_callnum);
                $("#profile_user_address").val(user_address);
                $("#profile_image").attr('src',kotak_image);
                listNationality(user_nationality);
                listCountryCode(user_countrycode);
            }

        });
        
        function listNationality(nice_name='')
        {            
            $.ajax({
                url: '<?php echo AIR_API;?>listNationality/access_token/'+access_token,
                type: 'POST',          
                success: function(response)
                {
                    if (response['code'] == 200) { 
                        $('#profile_nationality')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option disabled selected>Pilih Negara</option>')
                            .val('whatever')
                        ;                   
                        for (var i = 0; i < response['data'].length; i++) {
                            var nicename  = response['data'][i]['nicename'];                            
                            var photo    = response['data'][i]['photo'];
                            if (nicename == nice_name) {
                                $("#profile_nationality").append("<option value='"+nicename+"' selected='selected'>"+nicename+"</option>");
                            }
                            else
                            {
                                $("#profile_nationality").append("<option value='"+nicename+"'>"+nicename+"</option>");
                            }
                        }
                    }
                }
            });
        }

        function listCountryCode(country_code='')
        {            
            $.ajax({
                url: '<?php echo AIR_API;?>listNationality/access_token/'+access_token,
                type: 'POST',          
                success: function(response)
                {
                    if (response['code'] == 200) { 
                        $('#profile_kode_area')
                            .find('option')
                            .remove()
                            .end()
                            .append('<option disabled selected>Pilih Kode Area</option>')
                            .val('whatever')
                        ;                   
                        for (var i = 0; i < response['data'].length; i++) {
                            var nicename  = response['data'][i]['nicename'];                            
                            var phonecode = response['data'][i]['phonecode'];
                            if (phonecode == country_code) {
                                $("#profile_kode_area").append("<option value='"+phonecode+"' selected='selected'>"+nicename+" +"+phonecode+"</option>");
                            }
                            else
                            {
                                $("#profile_kode_area").append("<option value='"+phonecode+"'>"+nicename+" +"+phonecode+"</option>");
                            }
                        }
                    }
                }
            });
        }

        $(document.body).on('click', '#btnSavedProfile' ,function(e){
            var first_name      = $("#profile_first_name").val();
            var last_name       = $("#profile_last_name").val();
            var user_gender     = $("#profile_gender").val();
            var user_nationality= $("#profile_nationality").val();
            var user_birthdate  = $("#profile_date").val();
            var user_age        = $("#profile_age").val();
            var user_countrycode= $("#profile_kode_area").val();
            var user_address    = $("#profile_user_address").val();
            
            var imageData = $('.image-editor').cropit('export');
            if (imageData == 'undefined') {
                $("#modal_alert").modal('show');
                $("#modal_konten").css('background-color','#ff3333');
                $("#text_modal").html("Mohon pilih foto");
                $("#button_ok").click( function(){
                    location.reload();
                });
            }            
            else
            {   
                $("#image_new").val(imageData);
                var profil_img = imageData.split(";base64,")[1];
                $.ajax({
                    url: '<?php echo AIR_API;?>face_detection/',
                    type: 'POST',
                    data: {
                        image : profil_img
                    },
                    success: function(response)
                    {                        
                        if (response['face'] == true) {
                            var image_new = $("#image_new").val();  
                            $.ajax({
                                url: '<?php echo AIR_API;?>savedProfileStudent/access_token/'+access_token,
                                type: 'POST',
                                data: {                        
                                    id_user             : iduser,
                                    first_name          : first_name,
                                    last_name           : last_name,
                                    user_gender         : user_gender,
                                    user_nationality    : user_nationality,
                                    user_birthdate      : user_birthdate,
                                    user_age            : user_age,
                                    user_countrycode    : user_countrycode,
                                    user_address        : user_address,
                                    image_new           : image_new
                                },
                                success: function(response)
                                {
                                    var code    = response['code'];
                                    if (code == 200) {
                                        $("#modal_alert").modal('show');
                                        $("#modal_konten").css('background-color','#32c787');
                                        $("#text_modal").html("Profile Berhasil diperbarui");
                                        $("#button_ok").click( function(){
                                            location.reload();
                                        });
                                    }
                                    else
                                    {
                                        $("#modal_alert").modal('show');
                                        $("#modal_konten").css('background-color','#ff3333');
                                        $("#text_modal").html("Terjadi kesalahan!!!");
                                        $("#button_ok").click( function(){
                                            location.reload();
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            }            
        });

    });
</script>