<!-- <header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #db1f00;"> -->
	<?php $this->load->view('inc/navbar'); ?>
<!-- </header> -->
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('./inc/sidetutor'); ?>
	</aside>

	<section id="content">
		<div class="container">
			<div class="block-header">
				<h2><?php echo $this->lang->line('selectsubjecttutor'); ?></h2>

				<ul class="actions hidden-xs">
					<li>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url();?>tutor"><?php echo $this->lang->line('hometutor'); ?></a></li>                            
							<li class="active"><?php echo $this->lang->line('selectsubjecttutor'); ?></li>
						</ol>
					</li>
				</ul>
			</div> <!-- akhir block header    -->        
			<br>
			<?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>
			<!-- disini untuk dashboard murid -->               
			<div class="card" style="margin-top: 2%;">
				<div class="card-header"></div>
				<div class="card-body card-padding">
				<div class="row">
				<div class="table-responsive">
					<table id="tableTutorChooseSubject" class="display table table-striped table-bordered data" cellspacing="0" width="100%">
						<thead>
							<?php
							$select_all = $this->db->query("SELECT * FROM master_subject ORDER BY subject_id ASC")->result_array();
							?>
							<tr>
								<th data-column-id="no">No</th>
								<th data-column-id="name"><?php echo $this->lang->line('subject'); ?></th>
								<th data-column-id="jenjang"><?php echo $this->lang->line('school_jenjang'); ?></th>
								<th data-column-id="kelas"><?php echo $this->lang->line('selectgrade'); ?></th>
								<th>Status</th>
								<th data-column-id="aksi"></th>
								<th><?php echo $this->lang->line('aksi');?></th>
							</tr>
						</thead>
						<tbody>  

						</tbody>   
					</table>   
				</div>
				</div>
				</div>
			</div>
		</div>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	/*$(document).on('click', "a.choose_ajax", function(e) {
		e.preventDefault();
		$(this).removeClass('choose_ajax');
    	alert('ke choose');
	});
	$(document).on('click', "a.unchoose_ajax", function() {
    	alert('ke unchoose');
	});*/
	/*$('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );*/
    // $('.data').DataTable();
    var ttt2 = $("#tableTutorChooseSubject").DataTable({
	    // "aoColumns": unsortableColumns,
	    "dom": "<'dt-toolbar'<'col-md-2 hidden-xs hidden-sm'l><'col-md-4 perso_status_option'><'col-xs-12 col-sm-6 col-md-6 hidden-xs'f>r>"+
	      "t"+
	      "<'row'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
	    "serverSide" : true,
	    "processing": true,
	    "ajax": {
	        "url": '<?php echo base_url(); ?>serverside_ajax/tutorChooseSubject/<?php echo $this->session->userdata('id_user'); ?>',
	        "type": "POST"
	    }
	    // "order": [0, "desc"]
	});

	$(document).on('click', "a.choose_ajax", function(e){
		e.preventDefault();
		var urls = $(this).attr('href');
		var sid = $(this).attr('id');
		var that = $(this);
		
		$.ajax({
			url: urls,
			type: 'GET',
			success: function(data){
				data = JSON.parse(data);
				if(data['status'] == 1){
					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',data['message']);
					that.attr('class','waves-effect c-white btn btn-success btn-block request_ajax');
					that.attr('href',"<?php echo base_url('/master/requestsub?id_user='.$this->session->userdata('id_user').'&subject_id='); ?>"+sid);
					that.html('<i class="zmdi zmdi-face-add"></i>Request Verification');					
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
		});	
	});
	
	$(document).on('click','a.unchoose_ajax', function(e){
		e.preventDefault();
		var urls = $(this).attr('href');
		var sid = $(this).attr('id');
		var that = $(this);
		
		$.ajax({
			url: urls,
			type: 'GET',
			success: function(data){
				data = JSON.parse(data);
				if(data['status'] == 1){
					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',data['message']);
					// that.attr('class','waves-effect bgm-green c-white btn btn-success btn-block choose_ajax');
					// that.attr('href',"<?php echo base_url('/master/choosesub?id_user='.$this->session->userdata('id_user').'&subject_id='); ?>"+sid);
					// that.html('<i class="zmdi zmdi-face-add"></i><?php echo $this->lang->line('ikutisubject'); ?>');
				}else{
					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				}
			}
		});
	});

	$(document).on('click','a.request_ajax', function(e){
		e.preventDefault();
		var urls = $(this).attr('href');
		var sid = $(this).attr('id');
		var that = $(this);
		
		$.ajax({
			url: urls,
			type: 'GET',
			success: function(data){
				// console.log(data);
				// data = JSON.parse(data);
				// if(data['status'] == 1){
					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Waiting Verification");
					that.attr('class','waves-effect c-gray btn btn-default btn-block');
					that.attr('href',"#");
					that.html('<i class="zmdi zmdi-face-add"></i>Waiting Verification');
				// }else{
				// 	notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut',data['message']);
				// }
			}
		});
		
	});
	
</script>