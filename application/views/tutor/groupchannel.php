<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');

	?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('./inc/sidetutor'); ?>
	</aside>

	<section id="content">
		<div class="container">
			<div class="block-header">
				<h2>Atur Kelas Group Channel</h2>

				<!-- <ul class="actions hidden-xs">
					<li>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url();?>tutor"><?php echo $this->lang->line('hometutor'); ?></a></li>                            
							<li class="active"><?php echo $this->lang->line('selectsubjecttutor'); ?></li>
						</ol>
					</li>
				</ul> -->
			</div> <!-- akhir block header    -->        
			<br>
			<?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>

            <div class="alert alert-warning" style="display: none;" id="alertpoint">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                Maaf, Point tidak mencukupi. Mohon menghubungi admin Classmiles.com untuk Topup Point anda.
            </div>
			<!-- disini untuk dashboard murid -->               
			<div class="card col-md-12">
                <form class="form-horizontal" role="form">
                    <div class="card-header">
                        <h2><small>Mengatur Jadwal Kelas Group Channel</small></h2><hr>                        
                    </div>
                    
                    <div class="card-body card-padding">

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Channel</label>
                            <div class="col-sm-6">
                            <div class="dtp-container fg-line">                                                
                                <select class='select2 form-control' required id="channelid">
                                    <option disabled selected>Pilih Channel</option>
                                </select>                                                              
                            </div>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Tanggal</label>
                        	<div class="col-sm-6">
                                <div class="dtp-container fg-line m-l-5">                                                
                                    <input id="tanggal" type='text' required class="form-control"  data-date-format="YYYY-MM-DD" placeholder="<?php echo $this->lang->line('searchdate'); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Waktu</label>
                            <div class="col-sm-6">
                        	<!-- <div class="dtp-container fg-line">                                                
                                <select class='select2 form-control' required id="time">
                                    <option disabled selected><?php echo $this->lang->line('searchtime'); ?></option>
                                    <?php                 
                                        for ($i=0; $i < 24; $i++) { 
                                          for($y = 0; $y<=45; $y+=15){  
                                            echo "<option value=".sprintf('%02d',$i).":".sprintf('%02d',$y).":00".">".sprintf('%02d',$i).":".sprintf('%02d',$y)."</option>";                                            
                                        }
                                    }
                                    ?>
                                </select>                                                             
                            </div> -->
                            <div class="dtp-container fg-line m-l-5" id="tempatwaktuu">
                                <input type='text' class='form-control' id='timeeeeeee' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />                                
                            </div>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Durasi</label>
                            <div class="col-sm-6">
                                <div class="dtp-container fg-line">   
                                    <?php 
                                        $status = $this->session->userdata('status');                                        
                                        if ($status == 2) {
                                            ?>
                                    <select class='select2 form-control' required id="durasi">
                                        <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
                                        <option value="900">15 Minutes</option>
                                    </select>
                                            <?php       
                                        }
                                        else
                                        {
                                    ?>
                                    <select class='select2 form-control' required id="durasi">
                                        <option disabled selected><?php echo $this->lang->line('duration'); ?></option>
                                        <option value="900">15 Minutes</option>
                                        <option value="1800">30 Minutes</option>
                                        <option value="2700">45 Minutes</option>
                                        <option value="3600">1 Hours</option>
                                        <option value="5400">1 Hours 30 Minutes</option>
                                        <option value="7200">2 Hours</option>
                                        <option value="9000">2 Hours 30 Minutes</option>
                                        <option value="10800">3 Hours</option>
                                        <option value="12600">3 Hours 30 Minutes</option>
                                        <option value="14400">4 Hours</option>
                                        <option value="16200">4 Hours 30 Minutes</option>
                                        <option value="18000">5 Hours</option>
                                        <option value="19800">5 Hours 30 Minutes</option>
                                        <option value="21600">6 Hours</option>
                                        <option value="23400">6 Hours 30 Minutes</option>
                                        <option value="25200">7 Hours</option>
                                        <option value="27000">7 Hours 30 Minutes</option>
                                        <option value="28800">8 Hours</option>
                                        <option value="30600">8 Hours 30 Minutes</option>
                                        <option value="32400">9 Hours</option>
                                        <option value="34200">9 Hours 30 Minutes</option>
                                        <option value="36000">10 Hours</option>
                                        <option value="37800">10 Hours 30 Minutes</option>
                                        <option value="39600">11 Hours</option>
                                        <option value="41400">11 Hours 30 Minutes</option>
                                        <option value="43200">12 Hours</option>
                                        <option value="45000">12 Hours 30 Minutes</option>
                                        <option value="46800">13 Hours</option>
                                        <option value="48600">13 Hours 30 Minutes</option>
                                        <option value="50400">14 Hours</option>
                                        <option value="52200">14 Hours 30 Minutes</option>
                                        <option value="54000">15 Hours</option>
                                        <option value="55800">15 Hours 30 Minutes</option>
                                        <option value="57600">16 Hours</option>
                                        <option value="59400">16 Hours 30 Minutes</option>
                                        <option value="61200">17 Hours</option>
                                        <option value="63000">17 Hours 30 Minutes</option>
                                        <option value="64800">18 Hours</option>
                                        <option value="66600">18 Hours 30 Minutes</option>
                                        <option value="68400">19 Hours</option>
                                        <option value="70200">19 Hours 30 Minutes</option>
                                        <option value="72000">20 Hours</option>
                                        <option value="73800">20 Hours 30 Minutes</option>
                                        <option value="75600">21 Hours</option>
                                        <option value="77400">21 Hours 30 Minutes</option>
                                        <option value="79200">22 Hours</option>
                                        <option value="81000">22 Hours 30 Minutes</option>
                                        <option value="82800">23 Hours</option>
                                        <option value="84600">23 Hours 30 Minutes</option>
                                        <option value="86400">24 Hours</option>
                                    </select>  
                                        <?php 
                                        }
                                    ?>                                                           
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Template</label>
                            <div class="col-sm-6">
                        	<div class="dtp-container fg-line">                                                
                                <select class='select2 form-control' required id="template">
                                    <option disabled selected>Pilih Template</option>
                                    <option value="whiteboard_digital">Multicast Whiteboard Digital</option>
                                    <option value="whiteboard_videoboard">Multicast Whiteboard VideoBoard</option>
                                    <option value="whiteboard_no">Multicast No Whiteboard</option>
                                </select>                                                             
                            </div>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Pelajaran</label>
                            <div class="col-sm-6">
                        	<div class="dtp-container fg-line">                                                
                                <select class='select2 form-control' required id="pelajaran">
                                    <?php
                                        echo '<option disabled="disabled" selected="" value="0">Pilih Pelajaran</option>'; 
                                        $id = $this->session->userdata("id_user");
                                        $sub = $this->db->query("SELECT * FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id=ms.subject_id WHERE tb.id_user='$id' AND tb.tutor_id='0'  AND (tb.status='verified' OR tb.status='requested' OR tb.status='unverified') ")->result_array();
                                        foreach ($sub as $row => $v) {

                                            $jenjangnamelevel = array();
                                            $jenjangid = json_decode($v['jenjang_id'], TRUE);
                                            if ($jenjangid != NULL) {
                                                if(is_array($jenjangid)){
                                                    foreach ($jenjangid as $key => $value) {                                    
                                                        $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$value'")->row_array();   
                                                        if ($selectnew['jenjang_level']=='0') {
                                                            $jenjangnamelevel[] = $selectnew['jenjang_name']; 
                                                        }
                                                        else{
                                                            $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
                                                        } 
                                                    }   
                                                }else{
                                                    $selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='$jenjangid'")->row_array();   
                                                    if ($selectnew['jenjang_level']=='0') {
                                                        $jenjangnamelevel[] = $selectnew['jenjang_name']; 
                                                    }
                                                    else{
                                                        $jenjangnamelevel[] = $selectnew['jenjang_name']." ".$selectnew['jenjang_level'];     
                                                    }                                                         
                                                }                                       
                                            }

                                            // $allsub = $this->db->query("SELECT tb.*, mj.*, ms.subject_name FROM tbl_booking as tb INNER JOIN master_subject as ms ON tb.subject_id= ms.subject_id INNER JOIN master_jenjang as mj ON mj.jenjang_id= ms.jenjang_id WHERE tb.id_user = '$id' AND tb.tutor_id='0' AND (tb.status='verified' OR tb.status='unverified') order by ms.subject_id ASC")->result_array();                                                
                                            // foreach ($allsub as $row => $v) {                                            
                                                echo '<option value="'.$v['subject_id'].'">'.$v['subject_name'].' - '.implode(", ",$jenjangnamelevel).'</option>';
                                            // }
                                        }
                                    ?>
                                </select>                                                               
                            </div>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Topik Pelajaran</label>
                            <div class="col-sm-6">
                        	<div class="dtp-container fg-line">                                                                                
                                <!-- <input type="text" name="topikpelajaran" class="form-control" required placeholder="Masukan Topik pelajaran"> -->
                                <textarea rows="5" name="topikpelajaran" id="topikpelajaran" class="form-control" required placeholder="Masukan Topik Pelajaran"></textarea>        
                            </div>
                            </div>
                        </div> 

                        <!-- <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Siswa</label>
                            <div class="col-sm-6">
                            <div class="dtp-container fg-line">                                                                                
                                <select required name="tagorang[]" id="tagorang" multiple class="select2 form-control" style="width: 100%;">                                                
                                </select>        
                            </div>
                            <label class="fg-line c-red">* Cari Siswa yang akan diikut sertakan di kelas ini, Minimal 1 Orang dan Maksmimal 4 Orang</label>
                            </div>
                        </div> -->
                        <input type="text" name="user_utc" id="user_utc" value="<?php echo $this->session->userdata('user_utc'); ?>" hidden>                   

                        <br><hr>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-7">
                                <!-- <a class="waves-effect c-white btn btn-primary btn-block simpanprice"><i class="zmdi zmdi-face-add"></i>Buat Kelas</a> -->
                                <button type="submit" class="waves-effect c-white btn btn-primary btn-block " id="simpankelas">Buat Kelas</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>

		</div>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

    $(document).ready(function(){

        var tgll = new Date();  
        var formattedDatee = moment(tgll).format('YYYY-MM-DD');
        var a = [];
        var b = [];            
        var c = [];
        var d = [];
        var globalChannelID = null;

        $(function () {
            var tgl = new Date();  
            var formattedDate = moment(tgl).format('YYYY-MM-DD');         
            
            $('#tanggal').datetimepicker({  
                minDate: moment(formattedDate, 'YYYY-MM-DD')
            });
            
        });

        $('#tanggal').on('dp.change', function(e) {
            date = moment(e.date).format('YYYY-MM-DD');                
            var hoursnoww = tgll.getHours(); 
            // alert(tgl);
            if (date == formattedDatee) {
                a = [];
                $("#tempatwaktuu").html("");
                $("#tempatwaktuu").append("<input type='text' class='form-control' id='time' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                $("#time").removeAttr('disabled');
                for (var i = hoursnoww+1; i < 24; i++) {                                                
                    a.push(i);
                }
                console.warn(a);
                $('#time').datetimepicker({                    
                    sideBySide: true, 
                    showClose: true,                   
                    format: 'HH:mm',
                    stepping: 15,
                    enabledHours: a,
                });
            }
            else
            {
                $("#tempatwaktuu").html("");
                $("#tempatwaktuu").append("<input type='text' class='form-control' id='time' disabled placeholder='<?php echo $this->lang->line('searchtime'); ?>' />");
                $("#time").removeAttr('disabled');
                b = [];
                for (var i = 0; i < 24; i++) {                                                
                    b.push(i);
                }
                console.warn(b);
                $('#time').datetimepicker({                    
                    sideBySide: true,                    
                    format: 'HH:mm',
                    showClose: true,
                    stepping: 15,
                    enabledHours: b,
                });
            }
            
        });

    	$('table.display').DataTable( {
            fixedHeader: {
                header: true,
                footer: true
            }
        } );
        $('.data').DataTable();
    	
        $('select.select2').each(function(){
            if($(this).attr('id') == 'tagorang'){
                $("#tagorang").select2({
                    delay: 2000,                    
                    maximumSelectionLength: 4,  
                    allowClear: true,               
                    ajax: {
                        dataType: 'json',
                        type: 'GET',
                        url: '<?php echo base_url(); ?>ajaxer/getStudentListByChannel',
                        data: function (params) {
                            return {
                              term: params.term,
                              page: params.page || 1,
                              channel_id: globalChannelID
                            };
                        },
                        processResults: function(data){
                            return {
                                results: data.results,
                                pagination: {
                                    more: data.more
                                }                       
                            };
                        },                   
                    }
                });  
            }else if($(this).attr('id') == 'channelid'){
                $(this).select2({
                    delay: 2000,                    
                    // maximumSelectionLength: 5,                  
                    ajax: {
                        dataType: 'json',
                        type: 'GET',
                        url: '<?php echo base_url(); ?>ajaxer/getMyListChannel',
                        data: function (params) {
                            return {
                              term: params.term,
                              page: params.page || 1
                            };
                        },
                        processResults: function(data){
                            return {
                                results: data.results,
                                pagination: {
                                    more: data.more
                                }                       
                            };
                        }                   
                    }
                });  
            }
        });
        $('#channelid.select2').on('select2:select',function(evt){
            var that = $(this);
            globalChannelID = $('#channelid.select2 option:selected').val();
        });

    	$("#simpankelas").click(function(e){
            $("#simpankelas").attr('disabled', true);
    		var rate_value = null;
    		// var isChecked = $('.aba').prop('checked');
            var tokenjwt = "<?php echo $this->session->userdata('access_token_jwt'); ?>";
    		var idtutorni = "<?php echo $this->session->userdata('id_user');?>";
    		var tanggal = $("#tanggal").val();	
            var channel_id = $("#channelid").val();	
    		var time = $("#time").val()+":00";
    		var durasi = $("#durasi").val();
    		var template = $("#template").val();
    		var pelajaran = $("#pelajaran").val();
    		var topikpelajaran = $("#topikpelajaran").val();
            // var tagorang = $("#tagorang").val();
    		var user_utc = $("#user_utc").val();
            if (channel_id == null) {
                notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih channel terlebih dahulu!!!");
            }
            else if (tanggal=="") {
                notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih tanggal kelas terlebih dahulu!!!");
            }
            else if (time=="undefined:00") {
                notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih waktu kelas terlebih dahulu!!!");
            }
            else if (durasi == null) {
                notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih durasi kelas terlebih dahulu!!!");
            }
            else if (template == null) {
                notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih template kelas terlebih dahulu!!!");
            }
            else if (pelajaran == null) {
                notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon memilih pelajaran kelas terlebih dahulu!!!");
            }
            else if (topikpelajaran == "") {
                notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Mohon mengisi topik pelajarn kelas terlebih dahulu!!!");
            }
            else
            {
        		$.ajax({
        			url: '<?php echo base_url(); ?>Rest/createclass_groupchannel/access_token/'+tokenjwt,
        			type: 'POST',
        			data: {
        				id_tutor: idtutorni,
                        channel_id: channel_id,
        				tanggal: tanggal,
        				time: time,
        				durasi: durasi,
        				template: template,
        				pelajaran: pelajaran,
        				topikpelajaran: topikpelajaran,
                        // participant: tagorang,
        				user_device: 'web',
        				user_utc: user_utc
        			},
        			success: function(response)
        			{					
        				// alert(response['code'])
        				if (response['code'] == "200") {
        					notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil Menambah Kelas");
        					setTimeout(function(){
        						window.location.replace("<?php echo base_url();?>");
        					},1500);
        				}
                        else if (response['code'] == "-101") {
                            $("#alertpoint").css('display','block');
                            $(window).scrollTop(0);
                            // notify('top','right','fa fa-check','warning','animated fadeInDown','animated fadeOut', "Maaf, Point tidak mencukupi. Mohon menghubungi admin Classmiles.com untuk Topup Point anda.");
                            setTimeout(function(){
                                $("#alertpoint").css('display','none');
                            },5000);
                        }else if(response['code'] == '403') {
                            notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', response['message']);
                            setTimeout(function(){
                                location.reload();
                            },2000);
                        }
        				else
        				{
        					notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Gagal Menambah Kelas");
        				}
        			}
        		});
    	    }
    	});
	});	
	
</script>