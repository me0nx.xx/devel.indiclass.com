<!-- <header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #db1f00;"> -->
    <?php $this->load->view('inc/navbar'); ?>
<!-- </header> -->
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <!-- <aside id="sidebar" class="sidebar c-overflow">

        <?php $this->load->view('./inc/sidetutor'); ?>
    </aside> -->

    <section id="content">
        <div class="container">

            <div class="card bgm-blue">
                <button class="btn btn-primary pull-left"><i class="zmdi zmdi-mail-reply-all"></i>&nbsp Back</button>
            </div> <br>

            <div class="block-header">
                <!-- <h2><?php echo $this->lang->line('profil'); ?></h2>

                <ul class="actions">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>tutor"><?php echo $this->lang->line('hometutor'); ?></a></li>
                            <li><?php echo $this->lang->line('profil'); ?></li>
                            <li class="active"><?php echo $this->lang->line('tab_about'); ?></li>
                        </ol>
                    </li>
                </ul> -->                

            </div><!-- akhir block header -->            
            <div class="bs-item z-depth-5">
                <div class="card" id="profile-main">

                    <div class="pm-overview c-overflow">

                        <div class="pmo-pic">
                            <div class="p-relative">
                                <a href="">
                                    <img class="img-responsive" src="<?php echo base_url('aset/img/user/'.$this->session->userdata('user_image')); ?>" alt=""> 
                                </a>                                                                
                            </div>

                            <div class="pmo-stat">
                                <h2 class="m-0 c-white"><?php echo $profile['educational_level'] ?></h2>
                                <?php echo $profile['educational_institution'] ?>
                            </div>
                        </div>                        

                    </div>

                    <div class="pm-body clearfix">                                                

                        <div class="pmb-block">
                            <div class="pmbb-header">
                            <!-- <blockquote class="m-b-25">
                                <p>Tentang Guru.</p>
                            </blockquote> -->
                            <h2><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('summary'); ?></h2>

                        </div>
                        <div class="pmbb-body p-l-30">
                        <!-- Disini Bio tutor -->
                            <div class="pmbb-view">                            
                            Halo, perkenalkan Nama saya <?php echo $this->session->userdata('nama_lengkap'); ?>, saya telah menyelesaikan pendidikan 
                            <?php echo $profile['educational_level'] ; ?> saya dibidang <?php echo $profile['educational_competence']; ?> <?php echo $profile['educational_institution'] ?>, <?php echo $profile['institution_address']; ?> 
                            </div>
                            <!-- Disini akhir Bio tutor -->                             
                            
                        </div>
                    </div>

                    <div class="pmb-block">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-account m-r-5"></i> <?php echo $this->lang->line('basic_info'); ?></h2>
                            
                        </div>

                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view">
                                <dl class="dl-horizontal">
                                    <dt><?php echo $this->lang->line('full_name'); ?></dt>
                                    <dd><?php echo $profile['user_name']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt><?php echo $this->lang->line('email_address'); ?></dt>
                                    <dd><?php echo $profile['email'] ?></dd>
                                </dl>
<!--                                 <dl class="dl-horizontal">
                                    <dt>Password</dt>
                                    <dd><?php echo $profile['password'] ?></dd>
                                </dl> --> 
                                <dl class="dl-horizontal">
                                    <dt>Birth Place</dt>
                                    <dd><?php echo $profile['user_birthplace'] ?></dd>
                                </dl> 
                                <dl class="dl-horizontal">
                                    <dt>Birth Date </dt>
                                    <dd><?php echo $profile['user_birthdate'] ?></dd>
                                </dl>

                                <dl class="dl-horizontal">
                                    <dt>Age</dt>
                                    <dd><?php echo $profile['user_age'] ?></dd>
                                </dl>
                                <!-- <dl class="dl-horizontal">
                                    <dt>Phone Number</dt>
                                    <dd><?php echo $profile['user_callnum'] ?></dd>
                                </dl> -->
                                <dl class="dl-horizontal">
                                    <dt>Gender</dt>
                                    <dd><?php echo $profile['user_gender'] ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Religion</dt>
                                    <dd><?php echo $profile['user_religion'] ?></dd>
                                </dl>
                                <!-- <dl class="dl-horizontal">
                                    <dt>Address</dt>
                                    <dd><?php echo $profile['user_address'] ?></dd>
                                </dl>  -->                            
                            </div>                         
                        </div>
                    </div>      

                    <div class="pmb-block">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-account m-r-5"></i> Information Teacher</h2>
                            
                        </div>

                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view">
                                <dl class="dl-horizontal">
                                    <dt><?php echo $this->lang->line('educational_level'); ?></dt>
                                    <dd><?php echo $profile['educational_level']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt><?php echo $this->lang->line('educational_competence'); ?></dt>
                                    <dd><?php echo $profile['educational_competence']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt><?php echo $this->lang->line('educational_institution'); ?></dt>
                                    <dd><?php echo $profile['educational_institution']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt><?php echo $this->lang->line('institution_address'); ?></dt>
                                    <dd><?php echo $profile['institution_address']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt><?php echo $this->lang->line('graduation_year'); ?></dt>
                                    <dd><?php echo $profile['graduation_year']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt><?php echo $this->lang->line('description_experience'); ?></dt>
                                    <dd><?php echo $profile['description_experience']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt><?php echo $this->lang->line('year_experience'); ?></dt>
                                    <dd><?php echo $profile['year_experience']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt><?php echo $this->lang->line('place_experience'); ?></dt>
                                    <dd><?php echo $profile['place_experience']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt><?php echo $this->lang->line('information_experience'); ?></dt>
                                    <dd><?php echo $profile['information_experience']; ?></dd>
                                </dl>
                            </div>
                        </div>
                    </div>                                  

                </div>
            </div>
        </div>

    </div><!-- akhir container -->
</section>            
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
