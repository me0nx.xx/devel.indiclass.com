<!-- <header id="header" class="clearfix" style="z-index: 8; position: fixed; width: 100%; padding: 0; background-color: #db1f00;"> -->
    <?php $this->load->view('inc/navbar'); ?>
<!-- </header> -->
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('./inc/sidetutor'); ?>
    </aside>

    <section id="content">
        <div class="container">
            <div class="block-header">
                <h2><?php echo $this->lang->line('request_trial_class'); ?></h2>

                <!-- <ul class="actions hidden-xs">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>tutor"><?php echo $this->lang->line('hometutor'); ?></a></li>                            
                            <li class="active"><?php echo $this->lang->line('selectsubjecttutor'); ?></li>
                        </ol>
                    </li>
                </ul> -->
            </div> <!-- akhir block header    -->        
            <br>
            <?php if($this->session->flashdata('mes_alert')){ ?>
            <div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php echo $this->session->flashdata('mes_message'); ?>
            </div>
            <?php } ?>
            <!-- disini untuk dashboard murid -->               
            <div class="card col-md-12">
                <form class="form-horizontal" role="form">
                    <!-- <div class="card-header">
                        <h2>Atur Kelas <small>Mengatur Jadwal Kelas Berbayar</small></h2><hr>                        
                    </div> -->                 
                                
                    <div class="card-header">
                        <h2>Atur Pembayaran
                            <small>Kelas Berbayar <?php echo $this->lang->line('request_trial_class'); ?></small>
                        </h2>
                        <hr>
                    </div>

                    <div class="card-body card-padding"> 
                    	<div class="col-md-12">
                    	<div class="row">
                    		<div class="col-md-8 col-sm-8 col-xs-8">
                    			<p class="f-14"><i class="zmdi zmdi-shopping-basket"></i> Silahkan Pilih Jumlah participants </p>
                    			<div class="form-group" id="boxVolumeParticipants">
		                            
			                    </div>
                    		</div>
                    		<div class="col-md-4 col-sm-4 col-xs-4" id="boxSideDetail" style="display: none;">                    		                    		

                    			<div class="card pt-inner">
	                                <div class="pti-header bgm-green">
	                                    <h2 id="priceVolume">
	                                        
	                                    </h2>
	                                    <div class="ptih-title" id="countVolume"></div>
	                                </div>

	                                <div class="pti-body">
	                                    <div class="ptib-item" id="detailVolume">
	                                        
	                                    </div>
	                                    <div class="ptib-item">
	                                        Jika anda berminat, silahkan request invoice untuk mendapatkan akses membuat kelas berbayar.
	                                    </div>
	                                </div>

	                                <div class="pti-footer">
	                                    <button type="submit" class="waves-effect c-white btn btn-lg btn-warning btn-block " id="askinvoice">Ask For Invoice <i class="zmdi zmdi-arrow-right"></i></button>
	                                </div>
	                            </div>

                    		</div>
                    	</div>
                        </div>
                        <input type="text" name="user_utcc" id="user_utcc" value="<?php echo $this->session->userdata('user_utc'); ?>" hidden>                                        

                    </div>
                    
                </form>

            </div>

        </div>                                

    </div>
</section>
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

$(document).ready(function(){

    var tgll = new Date();  
    var formattedDatee = moment(tgll).format('YYYY-MM-DD');
    var id_user = "<?php echo $this->session->userdata('id_user');?>"
    var a = [];
    var b = [];            
    var c = [];
    var d = [];    
    var lastid = "";
    var user_utc = new Date().getTimezoneOffset();
    user_utc = -1 * user_utc;

    $.ajax({
        url: '<?php echo base_url(); ?>Rest/list_paidmulticast_volume',
        type: 'POST',
        data: {
            type : 'open_multicast'
        },
        success: function(response)
        { 
        	var code = response['code'];
        	if (code == 200) {

        		var kotak = '';
        		for (var i = 0; i < response['data'].length; i++) {
        			var id 		= response['data'][i]['id'];
	        		var volume 	= response['data'][i]['volume'];
	        		var price 	= response['data'][i]['price'];	
	        		var price_cv= response['data'][i]['price_cv'];	

					kotak += "<div class='col-md-4 col-xs-4 m-b-20'>"+
					  	"<button class='btn btn-default btn-lg btn-block waves-effect chooseVolume' id='choose"+id+"' style='color:#bdc3c7; word-wrap:break-word;' data-id='"+id+"' data-pr='"+price+"' data-prc='"+price_cv+"' data-vr='"+volume+"'>"+volume+" Participants</button>"+
					"</div>";        		
        		}   
        		$("#boxVolumeParticipants").append(kotak);

        	}
        }
    });

    $(document.body).on('click', '.chooseVolume' ,function(e){ 
    	var chooseid 	= $(this).attr('id');
    	var id 			= $(this).data('id');
    	var pr 			= $(this).data('pr');
    	var vr 			= $(this).data('vr');
    	var prc 		= $(this).data('prc');
    	$("#"+chooseid).css('color','#34495e');

    	if (lastid == "") {    	
    		lastid = chooseid;
    	}
    	else
    	{    		
    		$("#"+lastid).css('color','#bdc3c7');
    		lastid = chooseid;    		
    	}

    	$("#priceVolume").html("<small>Rp</small> "+prc);
    	$("#countVolume").html(vr+" Participants");
    	$("#detailVolume").html('Anda bisa membuat kelas dengan kuota siswa '+vr+'.');
    	$("#boxSideDetail").css('display','block');
    	$("#askinvoice").attr("idp",id);
    	$("#askinvoice").attr('price',pr);
    	$("#askinvoice").attr('volume',vr);
    });

    $(document.body).on('click', '#askinvoice' ,function(e){ 
    	var user_utc    = $("#user_utcc").val();
    	var id 			= $(this).attr('idp');
    	var price 		= $(this).attr('price');
    	var volume 		= $(this).attr('volume');    	

    	$.ajax({
	        url: '<?php echo base_url(); ?>Rest/request_invoice_multicast',
	        type: 'POST',
	        data : {
	        	id_tutor : id_user,
	        	price : price,
	        	volume : volume,
	        	type : 'open_multicast',
	        	user_utc : user_utc
	        },
	        success: function(response)
	        { 
	        	var code = response['code'];
	        	if (code == 200) {
	        		notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Success Request Invoice, please check your email");
                    setTimeout(function(){
                        window.location.replace("<?php echo base_url();?>Tutor/RequestTrialClass");
                    },1000);
        		}
        		else
        		{
        			notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Failed request");
                    setTimeout(function(){
                        location.reload();
                    },3000);
        		}
        	}
        });
    });

    $('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        }
    } );
    $('.data').DataTable();

    $('.nominal').maskMoney({prefix:'Rp. ', thousands:'.', decimal:',', precision:0});

    $(".nominal").keyup(function() {        
        var clone = $(this).val();
        var cloned = clone.replace(/[A-Za-z$. ,-]/g, "")
        $("#nominalsave").val(cloned);
    });
    
}); 
    
</script>