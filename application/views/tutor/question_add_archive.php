<style type="text/css">
	.textfield {
		width: 100%;
		height: 35px;
		font-family: Gadget, sans-serif;
		color: #000;
		border: 1px double #09F;
		padding:10px;		
	}
}
</style>
<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('./inc/sidetutor'); ?>
	</aside>

	<section id="content">
		<div class="container">
			<div class="block-header">
				<h2><?php echo $this->lang->line('reportteaching'); ?></h2>				
			</div>
			<br>			

            <?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>

			<div class="card">
                <div class="card-header">
                    <h2><b>+</b> Add Archive Question<small></small></h2>
                    <hr>
                </div>
                
                <div class="card-body card-padding">
                	<div class="row">	                                      

		                <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label f-500 c-black ">Answer Type</label>
                            <div class="col-sm-10">
                                <div class="fg-line">                                    
	                    			<select required class="select2 form-control col-md-12 input-sm fg-input" id="options_type" name="options_type" style="width: 100%;">
                                        <option disabled selected>Choose Type</option>
                                        <option value="essay">Essay</option>
                                        <option value="textual">Textual</option>
                                        <option value="images">Images</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <br><br>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label f-500 c-black ">Question Permission</label>
                            <div class="col-sm-10">
                                <div class="fg-line">                                    
	                    			<select required class="select2 form-control col-md-12 input-sm fg-input" id="options_permission" name="options_permission" style="width: 100%;">
                                        <option disabled selected>Choose Permission</option>
                                        <option value="1">Public</option>
                                        <option value="0">Private Quiz</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <br><br>
	                    
	                    <div class="form-group" hidden>
	                    	<label for="inputEmail3" class="col-sm-2 control-label f-500 c-black ">Gambar</label>
                            <div class="col-sm-10">
                                <div class="fg-line">                                    
	                    			<textarea id="base64box"></textarea>
                                </div>
                            </div>
	                    </div>

	                    <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label f-500 c-black ">Question</label>
                            <div class="col-sm-10">
                                <div class="fg-line">                                    
	                    			<div class="editor"></div>
                                </div>
                            </div>
                        </div> 	                

                        <br><br>

	                    <div class="form-group" id="box_answertextual" style="display: none;">
                            <label for="inputEmail3" class="col-sm-2 control-label f-500 c-black ">Enter Choice</label>
                            <div class="col-sm-10">
                                <div class="fg-line"> 
	                    			<div class="table-responsive">
				                        <table class="table">
				                            <thead>
				                                <tr class="warning c-white">
				                                    <th width="10" class="c-white"><center>No.</center></th>
				                                    <th class="c-white"><center>Choices</center></th>
				                                    <th class="c-white"><center>Answer</center></th>
				                                </tr>
				                            </thead>
				                            <tbody id="body_tableanswer">
				                                
				                            </tbody>
				                        </table>
				                    </div>
                                </div>
                            </div>
                        </div> 

                        <br><br>

                        <div class="form-group" id="box_answerimage" style="display: none;">
                            <label for="inputEmail3" class="col-sm-2 control-label f-500 c-black ">Enter Choice</label>
                            <div class="col-sm-10">
                                <div class="fg-line"> 
	                    			<div class="table-responsive">
				                        <table class="table">
				                            <thead>
				                                <tr class="warning c-white">
				                                    <th width="10" class="c-white"><center>No.</center></th>
				                                    <th class="c-white" colspan="2"><center>Choices</center></th>
				                                    <th class="c-white"><center>Answer</center></th>
				                                </tr>
				                            </thead>
				                            <tbody id="body_tableanswerimage">
				                                
				                            </tbody>
				                        </table>
				                    </div>
                                </div>
                            </div>
                        </div> 

	                	<br><br><br>
	                	<div class="form-group" id="box_adddelete">
	                		<label for="inputEmail3" class="col-sm-2 control-label f-500 c-black "></label>
	                		<div class="col-sm-10">
                                <div class="fg-line">  
                                	<div class="card">
	                					<button class="btn bgm-red pull-right fg-input" style="width: auto; margin-top: 2%; margin-bottom: 8%;" id="btn_eraseAnswer"><b><i class="zmdi zmdi-delete"></i></b> Delete</button>
	                					<button class="btn bgm-green pull-right fg-input m-r-15" style="width: auto; margin-top: 2%; margin-bottom: 8%;" id="btn_AppendAnswer"><b>+</b> Add</button>
	                				</div>
	                			</div>
	                		</div>
	                	</div> 

	                	<br><br>
	                	<div class="form-group" style="margin-top: auto;">
	                		<label for="inputEmail3" class="col-sm-2 control-label f-500 c-black "></label>
	                		<div class="col-sm-10">
                                <div class="fg-line">  
	                				<button id="editor" class="btn bgm-bluegray pull-right form-control fg-input">Save Question</button>
	                			</div>
	                		</div>
	                	</div>    
                    </div>

                </div>

            </div>            

		</div>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	var total_bodyAnswer = 1;
	var total_bodyAnswerImage = 1;
	var bsid = null;
	var options_type = null;    	
	var json_answer_image = [];	
	
	$(".editor").summernote({
		height: 150,
		toolbar: [
		    ['style', ['style']],
		    ['font', ['bold', 'italic', 'underline', 'clear']],
		    ['fontname', ['fontname']],
		    ['color', ['color']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['height', ['height']],
		    ['table', ['table']],
		    ['insert', ['link', 'picture', 'hr']],
		    ['view', ['fullscreen']],
		]
	});

	$('#options_type').on('change', function() {
		$('#body_tableanswer').empty();
		$('#body_tableansweressay').empty();
		$('#body_tableanswerimage').empty();
		total_bodyAnswer = 1;
		total_bodyAnswerEssay = 1;
		total_bodyAnswerImage = 1;
		options_type = this.value;
		if (options_type == "textual") 
        {
        	$('#body_tableanswer').append('<tr id="tr_bodyanswer_'+total_bodyAnswer+'" class="active">'+
		        '<td><center>'+total_bodyAnswer+'</center></td>'+
		        '<td><center><input type="text" name="answer[]"  required class="textfield p-10" placeholder="Add Choices '+total_bodyAnswer+'"></center></td>'+
		        '<td align="center">'+
		        	'<div class="radio m-b-15">'+
		                '<label>'+
		                    '<input type="radio" name="an_value" id="answer_value_'+total_bodyAnswer+'">'+
		                    '<i class="input-helper"></i>'+                        
		                '</label>'+
		            '</div>'+
		        '</td>'+
		    '</tr>');

        	$("#box_adddelete").css('display','block');        	
        	$("#box_answerimage").css('display','none');
        	$("#box_answertextual").css('display','block');
        }
        else if (options_type == "images") 
        {                	
        	$('#body_tableanswerimage').append('<tr id="tr_bodyanswer_'+total_bodyAnswerImage+'" class="active">'+
		        '<td><center>'+total_bodyAnswerImage+'</center></td>'+
		        '<td><center><input type="file" name="answer_images" onchange="readURL(this);" accept="image/x-png,image/gif,image/jpeg" id="answer_images_'+total_bodyAnswerImage+'"/></center></td>'+
		        '<td><center><img id="blah_'+total_bodyAnswerImage+'" src="" alt="your image" style="display:none; height:128px; width:128px;"/></center></td>'+
		        '<td align="center">'+
		        	'<div class="radio m-b-15">'+
		                '<label>'+		                	
		                    '<input type="radio" name="an_value" id="answer_value_image_'+total_bodyAnswerImage+'">'+
		                    '<i class="input-helper"></i>'+                   
		                '</label>'+
		            '</div>'+
		        '</td>'+
		    '</tr>');

        	$("#box_adddelete").css('display','block');		    
        	$("#box_answertextual").css('display','none');
        	$("#box_answerimage").css('display','block');
        }
		else
		{
			$("#box_adddelete").css('display','none');	
			$("#box_answerimage").css('display','none');
        	$("#box_answertextual").css('display','none');
		}	  	
	});

	function getBase64(file) {
		var reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = function (e) {
			// console.log(reader.result);
			var cuk = e.target.result;	
			// cuk = cuk.split(";base64,")[1];  
			$("#base64box").empty();   
			$("#base64box").val(cuk);
		};
		reader.onerror = function (error) {
			console.log('Error: ', error);
		};
	}

	function readURL(input) {
		
		$("#btn_eraseAnswer").attr('disabled','true');
		$("#btn_AppendAnswer").attr('disabled','true');
        var file = document.getElementById('answer_images_'+total_bodyAnswerImage).files;
        var imgfile = null;
        var filename = file[0].name;        
        var filesize = file[0].size;
        var gbsid = "<?php echo mt_rand(100000,999999);?>";
        var data_bsid = null;
        if (file.length > 0) {      
        	if(file && filesize < 2000000) {
        		getBase64(file[0]);
		    	setTimeout(function(){
		    		imgfile = $("#base64box").val();		   
					$.ajax({
						url: '<?php echo base_url();?>process/saveTempImageQuiz',
						type: 'POST',
						data: {
							bsid : gbsid,
							imgfile : imgfile
			            }, 
						success: function(data){
							data = JSON.parse(data);				
							if(data['status'] == 1){
								var link = data['link_image'];
								card_value = 'gambar'+total_bodyAnswerImage;
								json_answer_image.push({imguri: link, value: card_value});
								$("#blah_"+total_bodyAnswerImage).css('display','block');
								$("#blah_"+total_bodyAnswerImage).attr('src',link);
								$("#btn_eraseAnswer").removeAttr('disabled');
								$("#btn_AppendAnswer").removeAttr('disabled');
							}else{

							}
						}
					});
		    	},1500);	
			}
			else
			{  	
				console.warn('kebesaran');
	    	}	    		    
		}
    }

	$('#btn_AppendAnswer').click(function(){		
        if (options_type == "textual") 
        {
        	total_bodyAnswer++;
	        $('#body_tableanswer').append('<tr id="tr_bodyanswer_'+total_bodyAnswer+'" class="active">'+
	            '<td><center>'+total_bodyAnswer+'</center></td>'+
	            '<td><center><input type="text" name="answer[]" required class="textfield p-10" placeholder="Add Choices '+total_bodyAnswer+'"></center></td>'+
	            '<td align="center">'+
	            	'<div class="radio m-b-15">'+
	                    '<label>'+
	                        '<input type="radio" name="an_value" id="answer_value_'+total_bodyAnswer+'">'+
	                        '<i class="input-helper"></i>'+                        
	                    '</label>'+
	                '</div>'+
	            '</td>'+
	        '</tr>');
        }
        else
        {
        	var isigambar = $("#answer_images_"+total_bodyAnswerImage).val();
        	if (isigambar == "") {
        		notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Harap pilih gambar terlebih dahulu!!!");
        	}
        	else
        	{
	        	total_bodyAnswerImage++;
	        	$('#body_tableanswerimage').append('<tr id="tr_bodyanswer_'+total_bodyAnswerImage+'" class="active">'+
			        '<td><center>'+total_bodyAnswerImage+'</center></td>'+
			        '<td><center><input type="file" name="answer_images[]" onchange="readURL(this);" accept="image/x-png,image/gif,image/jpeg" id="answer_images_'+total_bodyAnswerImage+'"></center></td>'+
			        '<td><center><img id="blah_'+total_bodyAnswerImage+'" src="https://cdn.classmiles.com/usercontent/dXNlci9lbXB0eS5qcGc=" alt="your image" style="display:none; height:128px; width:128px;"/></center></td>'+
			        '<td align="center">'+
			        	'<div class="radio m-b-15">'+
			                '<label>'+		                	
			                    '<input type="radio" name="an_value" id="answer_value_image_'+total_bodyAnswerImage+'">'+
			                    '<i class="input-helper"></i>'+                   
			                '</label>'+
			            '</div>'+
			        '</td>'+
			    '</tr>');
		    }
        }
    });

    $(":file").filestyle({placeholder: "No file"}); 

    $('#btn_eraseAnswer').click(function(){    	
    	if (options_type == "textual") 
        {
	        if(total_bodyAnswer>1){
	            $('#tr_bodyanswer_'+total_bodyAnswer).remove();
	            total_bodyAnswer--;	            
	        }
	    }
	    else
	    {
	    	if(total_bodyAnswerImage>1){
	            $('#tr_bodyanswer_'+total_bodyAnswerImage).remove();
	            total_bodyAnswerImage--;	               
	        }
	    }
    });    

    function getUrlVars() {
	    var vars = {};
	    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	        vars[key] = value;
	    });
	    return vars;
	}	

	var getbsid = getUrlVars()["bsid"];
	if (getbsid !== undefined)
	{
 		bsid_get 		= $("#subid").val();
 		$("#options_type").removeAttr('disabled'); 		
	}

	$(document).on('click','#editor', function(e){	
		var options_type 	= $("#options_type").val();
		var options_permission 	= $("#options_permission").val();
		$('.editor').empty();
		var question 		= $('.editor').code();
		var elems 			= $( "[name^='answer']" ).length;		
		var array 			= $( "input[name^='answer']" );
		
		var lengthimage 	= $( "[name^='answer_images']" ).length;		
		var arrayimage 		= $("input[name^='answer_images']");
		if (options_type == "images") {						
			if (options_type==""||question=="<p><br></p>"||lengthimage==""||arrayimage=="") {
				notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut', "Terdapat data yang belum terisi, Harap isi dengan lengkap!!!");
				return false;
			}
			else if (total_bodyAnswerImage <= 1) {
				notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut', "Harap masukan jawaban minimal 2 pilihan, dengan klik 'ADD' !!!");
				return false;
			}
			else
			{					
				if ($("#answer_images_"+total_bodyAnswerImage).val() == "") {
					notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut', "Harap ungguh foto jawaban "+total_bodyAnswerImage);
				}
				else
				{
					// var json_answer = [];
					for(i=1;i <= total_bodyAnswerImage;i++) {
						card_value = 'gambar'+i;
						if (card_value=="") {
							notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut', "Terdapat data yang belum terisi, Harap isi dengan lengkap!!!");
							return false;
						}
						else
						{
							var file = document.getElementById('answer_images_'+total_bodyAnswerImage).files[0];					
							$("#answer_value_image_"+(i)).val(card_value);			
						}
				    }		    
					if($('.radio').find('input[type="radio"]:checked').length > 0)
					{			
						var choice_value = JSON.stringify(json_answer_image);		
						var answer = $('input[type="radio"]:checked').val();
						$.ajax({
							url: '<?php echo base_url();?>process/saveImageQuestionQuizArchive',
							type: 'POST',
							data: {
								options_type : options_type,   
								options_permission: options_permission,             
				                question : question,
				                choice_value : choice_value,
				                answer : answer
				            }, 
							success: function(data){
								data = JSON.parse(data);				
								if(data['status'] == 1){
									notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil menambah soal");
									setTimeout(function(){
										location.reload();
									},1000);
								}else{
									notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Gagal menambah soal");
									setTimeout(function(){
										location.reload();
									},1000);
								}
							}
						});
					}
					else
					{
					   notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut', "Pilih Jawaban terlebih dahulu!!!");
					}
				}
			}
		}
		else if(options_type == "essay"){
			alert('dasf');
			if (options_type==""||question=="<p><br></p>") {
				notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut', "Terdapat data yang belum terisi, Harap isi dengan lengkap!!!");
				return false;
			}			
			else
			{				
				$.ajax({
					url: '<?php echo base_url();?>process/saveEssayQuestionQuizArchive',
					type: 'POST',
					data: {
						options_type : options_type,      
						options_permission: options_permission,          
		                question : question
		            }, 
					success: function(data){
						data = JSON.parse(data);
						if(data['status'] == 1){
							notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil menambah soal Essay");
							setTimeout(function(){
								location.reload();
							},1000);
						}else{
							notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Gagal menambah soal Essay");
							setTimeout(function(){
								location.reload();
							},1000);
						}
					}
				});
			}
		}
		else
		{
			if (options_type==""||question=="<p><br></p>"||elems==""||array=="") {
				notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut', "Terdapat data yang belum terisi, Harap isi dengan lengkap!!!");
				return false;
			}
			else if (total_bodyAnswer <= 1) {
				notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut', "Harap masukan jawaban minimal 2 pilihan!!!");
				return false;
			}
			else
			{
			    for(i=0;i < elems;i++) {
					card_value = array.eq(i).val();
					if (card_value=="") {
						notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut', "Terdapat data yang belum terisi, Harap isi dengan lengkap!!!");
						return false;
					}
					else
					{
						$("#answer_value_"+(i+1)).val(card_value);	       
					}
			    }
			    var json_answer = [];
				$("input[name^='answer']").each(function(){
					json_answer.push({value: $(this).val()});
				});		
				if($('.radio').find('input[type="radio"]:checked').length > 0)
				{
					var answer = $('input[type="radio"]:checked').val();
					var choice_value = JSON.stringify(json_answer);							
				   	$.ajax({
						url: '<?php echo base_url();?>process/saveQuestionQuizArchive',
						type: 'POST',
						data: {
							options_type : options_type,      
							options_permission: options_permission,          
			                question : question,
			                choice_value : choice_value,
			                answer : answer
			            }, 
						success: function(data){
							data = JSON.parse(data);				
							if(data['status'] == 1){
								notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut', "Berhasil menambah soal");
								setTimeout(function(){
									location.reload();
								},1000);
							}else{
								notify('top','right','fa fa-check','danger','animated fadeInDown','animated fadeOut', "Gagal menambah soal");
								setTimeout(function(){
									location.reload();
								},1000);
							}
						}
					});
				}
				else
				{
				   notify('top','right','fa fa-check','info','animated fadeInDown','animated fadeOut', "Pilih Jawaban terlebih dahulu!!!");
				} 
			}   
		}
		
	});
	
</script>