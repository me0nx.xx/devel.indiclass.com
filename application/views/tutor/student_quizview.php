<header id="header" class="clearfix" data-current-skin="blue">
	<?php $this->load->view('inc/navbar');?>
</header>
<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
	<aside id="sidebar" class="sidebar c-overflow">
		<?php $this->load->view('./inc/sidetutor'); ?>
	</aside>

	<section id="content">
		<div class="container">
			<div class="block-header">
				<h2>Result Student</h2>				
			</div> <!-- akhir block header    -->        
			<br>			

            <?php if($this->session->flashdata('mes_alert')){ ?>
			<div class="alert alert-<?php echo $this->session->flashdata('mes_alert'); ?>" style="display: <?php echo $this->session->flashdata('mes_display'); ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<?php echo $this->session->flashdata('mes_message'); ?>
			</div>
			<?php } ?>

			<?php
    			$qd 			= $_GET['qd'];
    			if ($qd == null || $qd == "") {
    				redirect('Tutor/List_QuizResult');
    			}
				$tutor_id		= $this->session->userdata('id_user');
				$select 		= $this->db->query("SELECT tu.user_name,mbs.name, mbs.description, mbs.template_type, ms.subject_name, ms.jenjang_id, tq.* FROM tbl_quiz as tq INNER JOIN master_banksoal as mbs ON tq.bsid=mbs.bsid INNER JOIN master_subject as ms ON ms.subject_id=tq.subject_id INNER JOIN tbl_user as tu ON tu.id_user=tq.id_user WHERE tq.quiz_id = '$qd' AND mbs.tutor_id='$tutor_id'")->row_array();
				$bsid 			= $select['bsid'];
				$jenjang_namelevel = "";
				$jenjangid 		= json_decode($select['jenjang_id'], TRUE);
				if ($jenjangid != NULL) {
					if(is_array($jenjangid)){
						foreach ($jenjangid as $keyy => $v) {								
							$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$v'")->row_array();
							if ($getAllSubject['jenjang_level'] == 0) {
								$jenjang_namelevel = $getAllSubject['jenjang_name'];
							}
							else
							{
								$jenjang_namelevel = $getAllSubject['jenjang_name'].' '.$getAllSubject['jenjang_level'];
							}							
						}
					}
					else
					{
						$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$jenjangid'")->row_array();
						if ($getAllSubject['jenjang_level'] == 0) {
							$jenjang_namelevel = $getAllSubject['jenjang_name'];
						}
						else
						{
							$jenjang_namelevel = $getAllSubject['jenjang_name'].' '.$getAllSubject['jenjang_level'];
						}
					}
				}
				//JUMLAH SOAL 
				$chk 			= $this->db->query("SELECT * FROM master_banksoal WHERE bsid='$bsid'")->row_array();
				$list_soal 		= json_decode($chk['list_soal'],true);
				$jumlah_soal 	= count($list_soal);
				//END JUMLAH SOAL
				$total_benar 	= 0;
				$total_salah 	= 0;
				// if (is_array($select['answer_question'])) {								
					$answer_student	= json_decode($select['answer_question'],true);
					foreach ($answer_student as $ka => $as) {
						if ($as['answer_student'] != null) {
							if ($as['answer_student'] == $as['answer_system']) {
								$total_benar = $total_benar+1;
							}
							else
							{
								$total_salah = $total_salah+1;	
							}
						}
					}
				// }
				$score = $total_benar / $jumlah_soal * 100; 
				$answer_question	= json_decode($select['answer_question'],true);
			?>

			<div class="card">
                <div class="card-header ch-alt text-center">
                    <h2><?php echo $select['subject_name'].' - '.$jenjang_namelevel;?></h2>
                </div>
                
                <div class="card-body card-padding">     
                    <div class="clearfix"></div>
                    
                    <div class="row p-10" style="word-wrap: break-word;">
                    	<p style="font-size: 16px;"><?php echo $select['description'];?></p>
                    	<hr>                    	
                    	<div class="col-md-4 col-xs-4" align="center">
                    		<label>
                                <img src="https://cdn.classmiles.com/sccontent/baru/JumlahSoal.png" style="height: 20%; width: 20%;">
                                <h1 id="result_jumlahsoal"><?php echo $jumlah_soal;?></h1> Jumlah Soal
                            </label>
                    	</div>
                    	<div class="col-md-4 col-xs-4" align="center">
                    		<label>
                                <img src="https://cdn.classmiles.com/sccontent/baru/JawabanBenar.png" style="height: 20%; width: 20%;">
                                <h1 id="result_totalbenar" style="color: green;"><?php echo $total_benar;?></h1> Benar
                            </label>
                    	</div>
                    	<div class="col-md-4 col-xs-4" align="center">
                    		<label>
                                <img src="https://cdn.classmiles.com/sccontent/baru/JawabanSalah.png" style="height: 20%; width: 20%;">
                                <h1 id="result_totalsalah" style="color: red;"><?php echo $total_salah;?></h1> Salah 
                            </label>
                    	</div>
                    	<div class="col-md-12 col-xs-12" align="center">
                            <hr>
                            <center>
                                <label>
                                    <h2 style="color: #95A5A6;">Score Student : <?php echo $select['user_name'];?></h2>
                                    <h1 id="result_score" style="color: #6C7A89;"><?php echo $score;?></h1>
                                </label>
                            </center>
                            <hr>
                        </div>
                    </div>

                                        
                </div>                                
            </div>

			<!-- disini untuk dashboard murid -->               
			<div class="card" style="margin-top: 2%;">
				<div class="card-header">
					<div class="pull-left"><h2>List Question</h2></div>					 
				</div>
				<div class="row" style="margin-top: 2%;">
					<div class="card-body card-padding">
						<div class="table-responsive">							
							<table id="" class="display table table-hover table-bordered data" cellspacing="0" width="100%">
						        <thead>						        	
						            <tr valign="middle">
						                <th rowspan="2" width="5%"><center>#</center></th>
						                <th colspan="3" width="90%"><center>Quiz</center></th>
						            </tr>
						            <tr>
						                <th width="5%"><center>Question</center></th>
						                <th width="50%"><center>Option</center></th>
						                <th width="20%" style="border-right: 1px solid #eeeeee;"><center>Answer Correct</center></th>
						            </tr>
						        </thead>
						        <tbody>
						        	<?php
						        		$no=1;
						        		foreach ($answer_question as $keyaa => $qw) {
						        			$soal_uid 			= $qw['soal_uid'];
						        			$answer_value 		= $qw['answer_student'];
						        			$select_perquiz 	= $this->db->query("SELECT * FROM master_banksoal_archive WHERE soal_uid='$soal_uid'")->row_array();
						        			$options 			= $select_perquiz['options'];
						        			$options_type 		= $select_perquiz['options_type'];
							        	?>									
							            <tr align="center">
							                <td><?php echo $no; ?></td>
							                <td style="word-wrap: break-word;"><?php echo $select_perquiz['text_soal'];?></td>
							                <td id="optionnnn">
							                	<?php 
							                		if (is_array($answer_question)) {
								                		$obj = json_decode($options, true);
								                		if ($options_type == "images") {
								                			?>
								                			<table class="table bgm-gray table-bordered">
								                                <thead>
								                                    <tr>
								                                    	<th>#</th>
								                                        <th>Choices</th>
								                                        <th>Student Choice</th>
								                                    </tr>
								                                </thead>
								                                <tbody>						    
								                                	<?php
								                                	$no_image = 1;
											                		foreach ($obj as $keyaa => $valueee)
											                		{
											                			$imguri = $valueee['imguri'];
											                			$choices_options = $valueee['value'];
											                			$answer_checked = "";
											                			if($answer_value == $choices_options){
												                			 $answer_checked = "checked";
												                		}
												                		?>        	
								                                    <tr>		
								                                    	<td><?php echo($no_image); ?></td> 
								                                        <td><img src='<?php echo $imguri;?>' style='width:150px; height:150px;'/></td>
								                                        <td>
								                                        	<div class="checkbox m-b-15">
																                <label>
																                    <input type="checkbox" name="an_<?php echo $soal_uid;?>" <?php echo $answer_checked;?> disabled>
																                    <i class="input-helper"></i>
																                </label>
																            </div>
																       	</td>
								                                    </tr>							
								                                    <?php
									                                    $no_image++;
									                                }
									                                ?>                            
								                                </tbody>
								                            </table> 
								                        	<?php
								                		}
								                		else if ($options_type == "essay") {
								                			?>
								                			<table class="table bgm-gray table-bordered">
								                                <thead>
								                                    <tr>
								                                    	<th>#</th>								                                        
								                                        <th>Student Answer</th>
								                                    </tr>
								                                </thead>
								                                <tbody>						    
								                                	<?php
								                                	$no_image = 1;
											                		foreach ($answer_question as $keyaa => $valueee)
											                		{											                			
											                			$answer_studentt = $valueee['answer_student'];                			
												                		?>        	
								                                    <tr>		
								                                    	<td><?php echo($no_image); ?></td>
								                                        <td>
								                                        	<label><?php echo $answer_studentt;?></label>
																       	</td>
								                                    </tr>							
								                                    <?php
									                                    $no_image++;
									                                }
									                                ?>                            
								                                </tbody>
								                            </table> 
								                        	<?php
								                		}
								                		else
								                		{
							                				?>        			
								                			<table class="table bgm-gray table-bordered">
								                                <thead>
								                                    <tr>
								                                    	<th>#</th>
								                                        <th>Choices</th>
								                                        <th>Student Choice</th>
								                                    </tr>
								                                </thead>
								                                <tbody>
								                                	<?php
								                                	$no_text = 1;
										                			foreach ($obj as $key => $value) {
											                			$text = $value['value'];
											                			$answer_value_text = "";
											                			if($answer_value == $text){
												                			 $answer_value_text = "checked";
												                		}
												                	?>
								                                    <tr>
								                                    	<td></td>
								                                        <td><label class="lead f-16"><?php echo $no_text;?></label></td>
								                                        <td>
								                                        	<div class="checkbox m-b-15">
																                <label>
																                    <input type="checkbox" name="an_<?php echo $soal_uid;?>" <?php echo $answer_value_text;?> disabled>
																                    <i class="input-helper"></i>
																                </label>
																            </div>
																       	</td>
								                                    </tr>
								                                    <?php
								                                    $no_text++;
									                				}
									                				?>
								                                </tbody>
								                            </table>
								                            <?php
								                        }
								                    }
								                    else
								                    {
								                    	echo 'sddsf'.$options;
								                    }
						                       	?>
							                </td>
							                <td><?php echo $select_perquiz['answer_value'];?></td>
							            </tr>	
						            	<?php
						            	$no++;
						            	}
						            ?>						        
						        </tbody>
						    </table>

						</div>
					</div>
				</div>
			</div> 

		</div>                                

	</div>
</section>
</section>

<footer id="footer">
	<?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">

	$('a[title]').tooltip();

	$('table.display').DataTable( {
        fixedHeader: {
            header: true,
            footer: true
        },
        "aLengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],
        "iDisplayLength": 5
    } );
    $('.data').DataTable();
    $('#mylistquestion').DataTable({
    	"lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
    	"iDisplayLength": 5
    });
    $('#otherlistquestion').DataTable({
    	"lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
    	"iDisplayLength": 5
    });

</script>
<script type="text/javascript">
    $(document).ready(function() {
	    $(function () {
            var tgl = new Date();  
            var formattedDate = moment(tgl).format('YYYY-MM-DD');         
            
            $('#validity').datetimepicker({  
                minDate: moment(formattedDate, 'YYYY-MM-DD')
            });

            $('#invalidity').datetimepicker({  
                minDate: moment(formattedDate, 'YYYY-MM-DD')
            });
            
        });

        $(document).on("click", ".confrimDelete", function () {        	
	        var myBookId = $(this).attr('uid');
	        var bsid = $(this).attr('bsid');
	        $('#deletequiz').attr('uid',myBookId);
	        $('#deletequiz').attr('bsid',bsid);
	        $('#modalconfrim_deletearchive').modal('show');
	    });  

     

	    $('#deletequiz').click(function(e){
            var soal_uid = $(this).attr('uid');
            var bsid = $(this).attr('bsid');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                url :"<?php echo base_url() ?>Process/deleteQuestionQuiz",
                type:"POST",
                data: {
                    soal_uid: soal_uid,
                    bsid:bsid
                },
                success: function(data){
                	var response = JSON.parse(data);                	
                    // alert("Berhasil menghapus data siswa");
                    $('#modalconfrim_deletearchive').modal('hide');
                    notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Successfully delete");
                    setTimeout(function(){
                        location.reload(); 
                    },2000);
                                        
                } 
            });
        });
	});
</script>