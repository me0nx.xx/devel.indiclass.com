<header id="header" class="clearfix" data-current-skin="blue">
    <?php $this->load->view('inc/navbar'); ?>
</header>
<script>
    var total_bodyAccount = 0;
</script>


<!-- catatan untuk mengubah background profile menu sidebar di kiri ke folder "css/app.min.1.css" cari profile-menu > a -->
<section id="main" data-layout="layout-1">
    <aside id="sidebar" class="sidebar c-overflow">
        <?php $this->load->view('inc/sidetutor'); ?>
    </aside>

    <section id="content">
        <div class="container">

            <div class="block-header">
                <h2><?php echo $this->lang->line('profil'); ?></h2>

                <ul class="actions">
                    <li>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('hometutor'); ?></a></li>
                            <li><?php echo $this->lang->line('profil'); ?></li>
                            <li class="active"><?php echo $this->lang->line('tab_account'); ?></li>
                        </ol>
                    </li>
                </ul>
            </div><!-- akhir block header -->            
            <div class="bs-item z-depth-5">
                <div class="card" id="profile-main">

                    <?php
                    $this->load->view('inc/sideprofile');
                    ?>
                    <div class="pm-body clearfix">
                        <ul class="tab-nav tn-justified" role="tablist">
                            <li class="waves-effect"><a href="<?php echo base_url('tutor/about'); ?>"><?php echo $this->lang->line('tab_about'); ?></a></li>
                            <li class="waves-effect"><a href="<?php echo base_url('tutor/profile_account'); ?>"><?php echo $this->lang->line('tab_account'); ?></a></li> 
                            <li class="active waves-effect"><a href="<?php echo base_url('tutor/profile'); ?>"><?php echo $this->lang->line('tab_pr'); ?></a></li>
                            <!-- <li class="waves-effect"><a href="<?php echo base_url('tutor/profile_forgot'); ?>"><?php echo $this->lang->line('tab_cp'); ?></a></li> -->                                      
                        </ul>

                        <div class="card-body card-padding">
                            <form class="form-horizontal form-label-left" id="approvalForm_profile" action="<?php echo base_url('master/saveEditProfileTutor').$this->session->userdata('id_user')); ?> ?>" method="POST">

                                <blockquote class="m-b-25">
                                    <p><b><?php echo $this->lang->line('latbelpend'); ?></b></p>
                                </blockquote>

                                <div class="m-b-10">                                
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <th width="150"><?php echo $this->lang->line('educational_level'); ?></th>
                                            <th><?php echo $this->lang->line('educational_competence'); ?></th>
                                            <th><?php echo $this->lang->line('educational_institution'); ?></th>
                                            <th><?php echo $this->lang->line('institution_address'); ?></th>
                                            <th><?php echo $this->lang->line('graduation_year'); ?></th>
                                        </thead>
                                        <tbody id="body_tableaccount">
                                        <?php 
                                            if ($alluserprofile['education_background'] != '') {

                                                foreach($alluserprofile['education_background'] as $key => $value) { 
                                                   ?>
                                                   <?php if($key != 0){
                                                    echo '<script>
                                                            total_bodyAccount++;
                                                            </script>
                                                        <tr id="tr_bodyaccount_'.$key.'">';
                                                   }else{
                                                        echo '<tr>';
                                                   }
                                                   ?>

                                                    <td>
                                                        <select required class="form-control select2 col-md-7 col-xs-12" name="educational_level"  data-placeholder="<?php echo $this->lang->line('select_level'); ?>">
                                                                <option value=""></option>
                                                                <option value="<?php echo $value['educational_level']; ?>" selected='true'><?php echo $value['educational_level']; ?></option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input data-toggle="tooltip" data-placement="bottom" title="Ex: Computer Science, Biology, etc." type="text" required class="form-control col-md-7 col-xs-12" name="educational_competence" value="<?php echo $value['educational_competence'];  ?>" >
                                                    </td>
                                                    <td>
                                                        <input data-toggle="tooltip" data-placement="bottom" title="Ex: Your school or university name, etc." type="text" required class="form-control col-md-7 col-xs-12" name="educational_institution" value="<?php echo $value['educational_institution'];  ?>">
                                                    </td>
                                                    <td>
                                                        <input data-toggle="tooltip" data-placement="bottom" title="Ex: Jakarta, Bandung, etc." type="text" rows="5" required class="form-control col-md-7 col-xs-12" name="institution_address"  value="<?php echo $value['institution_address'];  ?>">
                                                    </td>
                                                    <td>
                                                        <input data-toggle="tooltip" data-placement="bottom" title="Ex: 2015, 2016 etc." type='text' required class="form-control col-md-7 col-xs-12 yearly" minlength="4" maxlength="4" name="graduation_year" value="<?php echo $value['graduation_year'];  ?>">
                                                    </td>
                                                </tr>
                                                   <?php 
                                                }
                                            }else{
                                                ?>
                                                <tr>
                                                    <td>
                                                        <select required class="form-control select2 col-md-7 col-xs-12" name="educational_level"  data-placeholder="<?php echo $this->lang->line('select_level'); ?>">
                                                                <option value=""></option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input data-toggle="tooltip" data-placement="bottom" title="Ex: Computer Science, Biology, etc." type="text" required class="form-control col-md-7 col-xs-12" name="educational_competence">
                                                    </td>
                                                    <td>
                                                        <input data-toggle="tooltip" data-placement="bottom" title="Ex: Your school or university name, etc." type="text" required class="form-control col-md-7 col-xs-12" name="educational_institution">
                                                    </td>
                                                    <td>
                                                        <input data-toggle="tooltip" data-placement="bottom" title="Ex: Jakarta, Bandung, etc." type="text" rows="5" required class="form-control col-md-7 col-xs-12" name="institution_address" >
                                                    </td>
                                                    <td>
                                                        <input data-toggle="tooltip" data-placement="bottom" title="Ex: 2015, 2016 etc." type='text' required class="form-control col-md-7 col-xs-12 yearly" minlength="4" maxlength="4" name="graduation_year">
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        ?>

                                        </tbody>
                                        <tfoot>
                                            <th>
                                            <button class="btn btn-primary" type="button" id="btn_appendAccount"><i class="zmdi zmdi-plus"></i> </button>
                                            <button class="btn btn-danger" type="button" id="btn_eraseAccount"><i class="zmdi zmdi-minus"></i> </button>
                                            </th>
                                        </tfoot>
                                    </table>
                                </div>

                                <br><br> 
                                <blockquote class="m-b-25">
                                    <p><b><?php echo $this->lang->line('pengalaman_mengajar'); ?></b></p>
                                </blockquote>

                                <div class="m-t-10">                                
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <th><?php echo $this->lang->line('description_experience'); ?></th>
                                            <th><?php echo $this->lang->line('year_experience'); ?></th>
                                            <th><?php echo $this->lang->line('place_experience'); ?></th>
                                            <th><?php echo $this->lang->line('information_experience'); ?></th>
                                        </thead>
                                        <tbody id="body_tableprofile">
                                        <?php 
                                            if ($alluserprofile['teaching_experience'] != '') {

                                                foreach($alluserprofile['teaching_experience'] as $key => $value) { 
                                                   ?>
                                                   <?php if($key != 0){
                                                    echo '<script>
                                                            total_bodyAccount++;
                                                            </script>
                                                        <tr id="tr_bodyaccount_'.$key.'">';
                                                   }else{
                                                        echo '<tr>';
                                                   }
                                                   ?>
                                                <td>
                                                    <input data-toggle="tooltip" data-placement="bottom" title="Ex: Teaching SD etc." type="text" required class="form-control col-md-7 col-xs-12" name="description_experience" id="description_experience1" value="<?php echo $value['description_experience'];  ?>"> 
                                                </td>
                                                <td>
                                                    <div class="col-md-6">
                                                        <input data-toggle="tooltip" minlength="4" maxlength="4" data-placement="bottom" title="Ex: 2014." type='text' required class="form-control col-md-7 col-xs-12 yearly" name="year_experience1" id="year_experience1" value="<?php echo $value['year_experience1'];  ?>">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input data-toggle="tooltip" minlength="4" maxlength="4" data-placement="bottom" title="Ex: 2016." type='text' required class="form-control col-md-7 col-xs-12 yearly" name="year_experience2" id="year_experience2" value="<?php echo $value['year_experience2'];  ?>">
                                                    </div>
                                                </td>
                                                <td>
                                                    <input data-toggle="tooltip" data-placement="bottom" title="Ex: Jakarta, Surabaya, etc." type="text" rows="5" required class="form-control col-md-7 col-xs-12" name="place_experience" id="place_experience1" value="<?php echo $value['place_experience'];  ?>">
                                                </td>
                                                <td>
                                                    <input data-toggle="tooltip" data-placement="bottom" title="Ex: Good." type="text" rows="5" required class="form-control col-md-7 col-xs-12" name="information_experience" id="information_experience1" value="<?php echo $value['information_experience'];  ?>">
                                                </td>
                                            </tr>
                                             <?php 
                                                }
                                            }else{
                                                ?>
                                            <tr>
                                                <td>
                                                    <input data-toggle="tooltip" data-placement="bottom" title="Ex: Teaching SD etc." type="text" required class="form-control col-md-7 col-xs-12" name="description_experience" id="description_experience1">
                                                </td>
                                                <td>
                                                    <div class="col-md-6">
                                                        <input data-toggle="tooltip" minlength="4" maxlength="4" data-placement="bottom" title="Ex: 2014." type='text' required class="form-control col-md-7 col-xs-12 yearly" name="year_experience1" id="year_experience1" placeholder="<?php echo $this->lang->line('from'); ?>">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input data-toggle="tooltip" minlength="4" maxlength="4" data-placement="bottom" title="Ex: 2016." type='text' required class="form-control col-md-7 col-xs-12 yearly" name="year_experience2" id="year_experience2" placeholder="<?php echo $this->lang->line('to'); ?>">
                                                    </div>
                                                </td>
                                                <td>
                                                    <input data-toggle="tooltip" data-placement="bottom" title="Ex: Jakarta, Surabaya, etc." type="text" rows="5" required class="form-control col-md-7 col-xs-12" name="place_experience" id="place_experience1">
                                                </td>
                                                <td>
                                                    <input data-toggle="tooltip" data-placement="bottom" title="Ex: Good." type="text" rows="5" required class="form-control col-md-7 col-xs-12" name="information_experience" id="information_experience1">
                                                </td>
                                            </tr>
                                             <?php
                                            }
                                        ?>

                                        </tbody>
                                        <tfoot>
                                            <th>
                                                <button class="btn btn-primary" type="button" id="btn_appendProfile"><i class="zmdi zmdi-plus"></i> </button>
                                                <button class="btn btn-danger" type="button" id="btn_eraseProfile"><i class="zmdi zmdi-minus"></i> </button>
                                            </th>
                                        </tfoot>
                                    </table>
                                </div>

                                <br><br> 
                                <blockquote class="m-b-25">
                                    <p><b><?php echo $this->lang->line('selfdescription'); ?></b></p>
                                </blockquote>

                                <div class="m-t-10">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <textarea class="form-control" rows="5" placeholder="Ex : I graduated from university ..."><?php echo $alluserprofile['self_description']; ?><?php  ?></textarea>
                                        </div>
                                    </div>

                                </div>

                                <div class="m-t-20">
                                    <button type="submit" name="simpanedit" class="btn btn-primary btn-block"><?php echo $this->lang->line('button_save'); ?></button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- akhir container -->
    </section>            
</section>

<footer id="footer">
    <?php $this->load->view('inc/footer'); ?>
</footer>
<script type="text/javascript">
    $(document).ready(function(){
        var lang_from = '<?php echo $this->lang->line('from'); ?>';
        var lang_to = '<?php echo $this->lang->line('to'); ?>';
        // var total_bodyAccount = 0;
        $('#btn_appendAccount').click(function(){
            total_bodyAccount++;
            $('#body_tableaccount').append('<tr id="tr_bodyaccount_'+total_bodyAccount+'">'+
                '<td><select required class="form-control select2 col-md-7 col-xs-12" name="educational_level" data-placeholder="<?php echo $this->lang->line('select_level'); ?>">'+
                                                                '<option value=""></option>'+
                                                            '</select></td>'+
                '<td><input type="text" required class="form-control col-md-7 col-xs-12" name="educational_competence"></td>'+
                '<td><input type="text" required class="form-control col-md-7 col-xs-12" name="educational_institution"></td>'+
                '<td><input type="text" rows="5" required class="form-control col-md-7 col-xs-12" name="institution_address"></td>'+
                '<td><input type="text" required class="form-control col-md-7 col-xs-12 yearly" minlength="4" maxlength="4" name="graduation_year"></td>'+
                '</tr>');

            $('select.select2').each(function(){
                    if($(this).attr('name') == "educational_level"){
                        $(this).select2({
                              delay: 2000,
                              ajax: {
                                  dataType: 'json',
                                  type: 'GET',
                                  url: '<?php echo base_url(); ?>ajaxer/getJenjangLevel',
                                  data: function (params) {
                                      return {
                                          term: params.term,
                                          page: params.page || 1
                                      };
                                  },
                                  processResults: function(data){
                                    return {
                                      results: data.results,
                                      pagination: {
                                        more: data.more
                                    }
                                };
                            }
                        }
                    });    
                    }
                    
                });

        });
        $('#btn_eraseAccount').click(function(){
            if(total_bodyAccount>0){
                $('#tr_bodyaccount_'+total_bodyAccount).remove();
                total_bodyAccount--;    
            }

        });
        var total_bodyProfile = 0;
        $('#btn_appendProfile').click(function(){
            total_bodyProfile++;
            $('#body_tableprofile').append('<tr id="tr_bodyprofile_'+total_bodyProfile+'">'+
                '<td><input type="text" required class="form-control col-md-7 col-xs-12" name="description_experience"></td>'+
                '<td>'+
                '<div class="col-md-6">'+
                '<input type="text" required class="form-control col-md-7 col-xs-12 yearly" minlength="4" maxlength="4" placeholder="<?php echo $this->lang->line('from'); ?>" name="year_experience1">'+
                '</div>'+
                '<div class="col-md-6">'+
                '<input type="text" required class="form-control col-md-7 col-xs-12 yearly" minlength="4" maxlength="4" placeholder="<?php echo $this->lang->line('to'); ?>" name="year_experience2">'+
                '</div>'+
                '</td>'+
                '<td><input type="text" rows="5" required class="form-control col-md-7 col-xs-12" name="place_experience"></td>'+
                '<td><input type="text" rows="5" required class="form-control col-md-7 col-xs-12" name="information_experience"></td>'+
                '</tr>');
            $('.yearly').on('keypress',function(e){
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        });
            // imginstance.setSelection(0, 0, 512, 512, true);
            $('#btn_eraseProfile').click(function(){
                if(total_bodyProfile>0){
                    $('#tr_bodyprofile_'+total_bodyProfile).remove();
                    total_bodyProfile--;
                }

            });
     /*       var imginstance = $('img#imgprev').imgAreaSelect({
                handles: true,
                instance: true,
                aspectRatio: '1:1',
                x1: 0,
                y1: 0,
                x2: 250,
                y2: 250,
                onSelectEnd: someFunction
            });
            function someFunction(img,selection) {
                alert(img);
            }*/
            $('.yearly').on('keypress',function(e){
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        });

    Webcam.set({
        width: 320,
        height: 240,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
</script>
