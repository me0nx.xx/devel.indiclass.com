<div class="d-flex flex-column h-100">
    
    <!-- Navbar -->
        <?php $this->load->view('inc/navbar_tutor');?>
    <!-- // END Navbar -->

    <div class="mdk-drawer-layout js-mdk-drawer-layout flex" data-fullbleed data-push data-has-scrolling-region>
        <div class="mdk-drawer-layout__content mdk-drawer-layout__content--scrollable">
            <div class="container">

                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="instructor-dashboard.html">Home</a></li>
                    <li class="breadcrumb-item active">Courses</li>
                </ol>
                <h1 class="page-heading h2">Request Courses</h1>
                
                <div class="row" id="box_listRequestTutor">                
                    
                </div>

                <div id="box_listclassesnull"></div>
            </div>        

        </div>

        <div class="modal fade" id="modal_alert" style="margin-top: 10%;"  data-backdrop="static" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content" id="modal_konten">
                    <div class="modal-header">
                        <h3 class="modal-title pull-left" id="colorJudul"><b>The Rote Less Travelled</b></h3>
                    </div>
                    <div class="modal-body">
                        <label id="text_modal"></label>
                    </div>
                    <div class="modal-footer">
                        <button  id="button_ok" type="submit" data-dismiss="modal" class="btn btn-link" style="color: white; margin-top: 3%;">OK</button>   
                    </div>                
                </div>
            </div>
        </div>
            
        <?php $this->load->view('inc/sidebar_tutor');?>
    </div>
</div>

