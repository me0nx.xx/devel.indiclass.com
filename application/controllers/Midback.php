<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH."/libraries/RESTMID_Controller.php";

class Midback extends RESTMID_Controller {

	public $sesi = array('nama' => null,'username' => null,'priv_level' => null);
	
	public function __construct()
	{
		parent::__construct();
		$params = array('server_key' => 'VT-server-31TBIo8Zf4Pa-I7j4pKj8CID', 'production' => true);
		$this->load->library('midtrans');
		$this->midtrans->config($params);
		$this->load->database('default');
	}
	public function index_get($value='')
	{
		$tr = $this->Rumus->RandomString();
		$res = array('error' => array('code' => 400, 'message' => 'Invalid Request', 'transaction' => $tr));
		// echo json_encode($res);
		$this->response($res);
	}
	public function index_post($value='')
	{
		$tr = $this->Rumus->RandomString();
		$res = array('error' => array('code' => 400, 'message' => 'Invalid Request', 'transaction' => $tr));
		// echo json_encode($res);
		$this->response($res);
	}
	public function bad_alias_get($value='')
	{
		$tr = $this->Rumus->RandomString();
		$res = array('error' => array('code' => 406, 'message' => 'Bad Alias', 'transaction' => $tr));
		// echo json_encode($res);
		$this->response($res);
	}
	public function bad_alias_post($value='')
	{
		$tr = $this->Rumus->RandomString();
		$res = array('error' => array('code' => 406, 'message' => 'Bad Alias', 'transaction' => $tr));
		// echo json_encode($res);
		$this->response($res);
	}
	public function main_get()
	{
		$tr = $this->Rumus->RandomString();
		$res = array();
		$res['status'] = true;
		$res['response'] = array('code' => 200, 'message' => 'OAuthServer','info' => 'Classmiles Auth Server', 'transaction' => $tr);
		$this->response($res);
	}
	public function main_post()
	{
		$tr = $this->Rumus->RandomString();
		$res = array();
		$res['status'] = true;
		$res['response'] = array('code' => 200, 'message' => 'OAuthServer','info' => 'Classmiles Auth Server', 'transaction' => $tr);
		$this->response($res);
	}

	public function update_fcm_post()
	{
		$id_user = htmlentities($this->post('id_user'));
		$fcm_token = htmlentities($this->post('fcm_token'));


		$time = date('Y-m-d H:i:s');

		$q = $this->db->simple_query("INSERT INTO master_fcm VALUES('{$fcm_token}','{$id_user}','{$time}')");
		if(!$q){
			$q =$this->db->simple_query("UPDATE master_fcm SET fcm_token='{$fcm_token}', update_time='{$time}' WHERE id_user='{$id_user}'");
			$s = $this->db->affected_rows();
			if($s<1 || !$q){
				$ret['status'] = false;
				$ret['message'] = 'Error Occured!';
				$this->response($ret);
			}
		}
		$ret['status'] = true;
		$ret['message'] = 'Successful';
		$this->response($ret);
	}
	public function login_post()
	{

		if($this->post('email') != '' && $this->post('password') != ''){
			$un = htmlentities($this->post('email'));
			$ps = md5(htmlentities($this->post('password')));


			$data = $this->db->query("SELECT * 
				FROM tbl_user 
				WHERE email =  '$un'
				AND password = '$ps'")->row_array();

			if(!empty($data)){
				if ($data['usertype_id'] == 'student') {
					$data = $this->db->query("SELECT * 
						FROM tbl_user AS tu
						INNER JOIN tbl_profile_student AS tps
						WHERE tu.email =  '$un'
						AND tps.student_id = tu.id_user")->row_array();
					$rets['status'] = true;
					$rets['message'] = "Succeed";
					$rets['data'] = $data;
					$this->response($rets);
				}
				if ($data['usertype_id'] == 'tutor') {
					$data = $this->db->query("SELECT * 
						FROM tbl_user AS tu
						INNER JOIN tbl_profile_tutor AS tps
						WHERE tu.email =  '$un'
						AND tps.tutor_id = tu.id_user")->row_array();
					$data['education_background'] = unserialize($data['education_background']);
					$data['teaching_experience'] = unserialize($data['teaching_experience']);
					$rets['status'] = true;
					$rets['message'] = "Succeed";
					$rets['data'] = $data;
					$this->response($rets);
				}
			}
			$rets['status'] = false;
			$rets['message'] = "Wrong email or password.";
			$this->response($rets);
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete.";
		$this->response($rets);
		
	}

	public function logingoogle_post()
	{

		if($this->post('email') != ''){
			$un = htmlentities($this->post('email'));


			$data = $this->db->query("SELECT * 
				FROM tbl_user 
				WHERE email =  '$un'")->row_array();

			if(!empty($data)){
				if ($data['usertype_id'] == 'student') {
					$data = $this->db->query("SELECT * 
						FROM tbl_user AS tu
						INNER JOIN tbl_profile_student AS tps
						WHERE tu.email =  '$un'
						AND tps.student_id = tu.id_user")->row_array();
					$rets['status'] = true;
					$rets['message'] = "Succeed";
					$rets['data'] = $data;
					$this->response($rets);
				}
				if ($data['usertype_id'] == 'tutor') {
					$data = $this->db->query("SELECT * 
						FROM tbl_user AS tu
						INNER JOIN tbl_profile_tutor AS tps
						WHERE tu.email =  '$un'
						AND tps.tutor_id = tu.id_user")->row_array();
					$data['education_background'] = unserialize($data['education_background']);
					$data['teaching_experience'] = unserialize($data['teaching_experience']);
					$rets['status'] = true;
					$rets['message'] = "Succeed";
					$rets['data'] = $data;
					$this->response($rets);
				}
			}
			$rets['status'] = false;
			$rets['message'] = "Wrong email or password.";
			$this->response($rets);
			$rets['status'] = false;
			$rets['message'] = "Wrong email or not registered";
			$this->response($rets);
		}
		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		$this->response($rets);
		
	}



	public function test_get($value='')
	{
		$ret['1'] = $value;
		$this->response($ret);
	}

	public function enter_room_post() {

		$iduser = $this->post('id_user');
		$value = $this->post('email').time();
		$class_id = $this->post('room');

		$sha = sha1($value);

		if($iduser != '' && $value != '' && $class_id != ''){
			$student = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$class_id' AND id_user='$iduser'")->result_array();
			if(!empty($student)){

				$rets['data'] = $student;
				$rets['status'] = true;
				$rets['message'] = "Data found";
				$this->response($rets);

			}else{
					// $hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
					// echo $hash_id;
				$save = $this->db->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$sha','$class_id','$iduser','TRUE',0)");
				if ($save) {

					$student2 = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$class_id' AND id_user='$iduser'")->result_array();

					if(!empty($student2)){

						$rets['data'] = $student2;
						$rets['status'] = true;
						$rets['message'] = "Data found";
						$this->response($rets);

					}

				} else {

					$rets['data'] = $student;
					$rets['status'] = false;
					$rets['message'] = "Failed to save."."INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$sha','$class_id','$iduser','TRUE',0)";
					$this->response($rets);

				}


			}
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		$this->response($rets);
	}

	

	public function savesub_post() 
	{
		if($this->post('id_user') != '')
		{
			$tutor_id = $this->post('id_user');
			$subject_id = $this->input->post('subject_id');
			$subdiscussion = $this->input->post('subdiscussion');
			$fdate = $this->input->post('fdate');

			$data = $this->db->query("SELECT * FROM log_subdiscussion WHERE subject_id='{$subject_id}' AND tutor_id='{$tutor_id}' AND fdate_week='{$fdate}'")->row_array();
			if(!empty($data)){
				$sid = $data['subdiscussion_id'];
				$this->db->simple_query("UPDATE log_subdiscussion SET subdiscussion='".$subdiscussion."' WHERE subdiscussion_id='{$sid}'");
			}else{
				$this->db->simple_query("INSERT INTO log_subdiscussion(subject_id, tutor_id, fdate_week, subdiscussion) VALUES('{$subject_id}','{$tutor_id}','{$fdate}','".$subdiscussion."')");
			}
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		$this->response($rets);
	}

	public function tutorextraclass_post()
	{
		if($this->post('id_user') != '')
		{
			$id_user = $this->post('id_user');

			$data = $this->db->query("SELECT tr.*, ms.subject_name, tu.user_name,mj.jenjang_name,mj.jenjang_level, tu.user_image FROM tbl_request as tr INNER JOIN (tbl_user as tu INNER JOIN (tbl_profile_student as tps INNER JOIN master_jenjang as mj ON tps.jenjang_id=mj.jenjang_id) ON tu.id_user=tps.student_id) ON tr.id_user_requester=tu.id_user INNER JOIN master_subject as ms ON tr.subject_id=ms.subject_id  WHERE tr.tutor_id='{$id_user}' AND tr.approve=0")->result_array();

			foreach ($data as $key => $v) {
				$j = sprintf('%02d',$v['time_requested']/3600);
				$m = sprintf('%02d',($v['time_requested']%3600)/60);
				$duration = sprintf('%02d',$v['duration_requested']/60);

				$data[$key]['time_requested'] = $j.":".$m;
				$data[$key]['duration_requested'] = $duration;
				// $data[$key]['date'] = date('')
			}
			$rets['status'] = true;
			$rets['message'] = "Succeed";
			$rets['data'] = $data;
			$this->response($rets);
		}

		$rets['status'] = false;
		$rets['message'] = "Field incomplete";
		$this->response($rets);
	}

	public function charge_post()
	{
		$mobile = $this->post();
		$snapToken = $this->midtrans->getSnapToken($mobile);
		error_log($snapToken);
		$rets['token'] = $snapToken;
		$this->response($rets);

	}

	public function mid_add_post()
	{
		$data = $this->post('data');
		$iduser = $this->post('id_user');
		$obj = json_decode($this->post('data'));

		//GET SIGNATURE KEY
    	$serverKey = "VT-server-31TBIo8Zf4Pa-I7j4pKj8CID";
    	$input = $obj->{'order_id'}.$obj->{'status_code'}.$obj->{'gross_amount'}.$serverKey;
    	$signature = openssl_digest($input, 'sha512');    	
		$typepayment = $obj->{'payment_type'};

		if ($typepayment == "echannel") {
			$where = array(
				'transaction_id' => $obj->{'transaction_id'},
				'id_user' => $iduser,
				'order_id' => $obj->{'order_id'},
				'status_code' => $obj->{'status_code'},
				'gross_amount' => $obj->{'gross_amount'},
				'payment_type' => $obj->{'payment_type'},
				'signature_key' => $signature,
				'transaction_status' => $obj->{'transaction_status'},
				'fraud_status' => $obj->{'fraud_status'},
				'status_message' => $obj->{'status_message'},
				'transaction_time' => $obj->{'transaction_time'},
				'bill_key' => $obj->{'bill_key'},
				'biller_code' => $obj->{'bill_key'},
				'pdf_url' => $obj->{'pdf_url'},
			);

			$senddata = $this->db->query("INSERT INTO log_midtrans_transaction (transaction_id,id_user,order_id,status_code,gross_amount,payment_type,bill_key,biller_code,pdf_url,signature_key,transaction_status,fraud_status,status_message,transaction_time) VALUES ('$where[transaction_id]','$where[id_user]','$where[order_id]','$where[status_code]','$where[gross_amount]','$where[payment_type]','$where[bill_key]','$where[biller_code]','$where[pdf_url]','$where[signature_key]','$where[transaction_status]','$where[fraud_status]','success','$where[transaction_time]')");
		}
		else if ($typepayment == 'bank_transfer') {
			$where = array(
				'transaction_id' => $obj->{'transaction_id'},
				'id_user' => $iduser,
				'order_id' => $obj->{'order_id'},
				'status_code' => $obj->{'status_code'},
				'gross_amount' => $obj->{'gross_amount'},
				'payment_type' => $obj->{'payment_type'},
				'signature_key' => $signature,
				'transaction_status' => $obj->{'transaction_status'},
				'fraud_status' => $obj->{'fraud_status'},
				'status_message' => $obj->{'status_message'},
				'transaction_time' => $obj->{'transaction_time'},
				'permata_va_number' => $obj->{'permata_va_number'},				
				'pdf_url' => $obj->{'pdf_url'},
			);

			$senddata = $this->db->query("INSERT INTO log_midtrans_transaction (transaction_id,id_user,order_id,status_code,gross_amount,payment_type,pdf_url,permata_va,signature_key,transaction_status,fraud_status,status_message,transaction_time) VALUES ('$where[transaction_id]','$where[id_user]','$where[order_id]','$where[status_code]','$where[gross_amount]','$where[payment_type]','$where[pdf_url]','$where[permata_va_number]','$where[signature_key]','$where[transaction_status]','$where[fraud_status]','success','$where[transaction_time]')");
		}
		else
		{
			$where = array(
				'transaction_id' => $obj->{'transaction_id'},
				'id_user' => $iduser,
				'order_id' => $obj->{'order_id'},
				'status_code' => $obj->{'status_code'},
				'masked_card' => $obj->{'masked_card'},
				'approval_code' => $obj->{'approval_code'},
				'bank' => $obj->{'bank'},
				'gross_amount' => $obj->{'gross_amount'},
				'payment_type' => $obj->{'payment_type'},
				'signature_key' => $signature,
				'transaction_status' => $obj->{'transaction_status'},
				'fraud_status' => $obj->{'fraud_status'},
				'status_message' => $obj->{'status_message'},
				'transaction_time' => $obj->{'transaction_time'},
			);

			$senddata = $this->db->query("INSERT INTO log_midtrans_transaction (transaction_id, id_user, order_id, status_code, masked_card, approval_code, bank, gross_amount, payment_type, signature_key, transaction_status, fraud_status, status_message, transaction_time) VALUES ('$where[transaction_id]','$where[id_user]','$where[order_id]','$where[status_code]','$where[masked_card]','$where[approval_code]','$where[bank]','$where[gross_amount]','$where[payment_type]','$where[signature_key]','$where[transaction_status]','$where[fraud_status]','success','$where[transaction_time]')");
		}

		$date = date_create($obj->{'transaction_time'});
        $datee = date_format($date, 'd/m/y');
        $hari = date_format($date, 'd');
        $tahun = date_format($date, 'Y');
                                                
        $date = $datee;
        $sepparator = '/';
        $parts = explode($sepparator, $date);
        $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));
        $hasilconvert = number_format($obj->{'gross_amount'}, 0, ".", ".");

        $datetimee = new DateTime($where['transaction_time']);
		$datetimee->modify('+1 day');
		$datelimitt = $datetimee->format('Y-m-d H:i:s');       

        $tbl = $this->db->query("SELECT email, user_name FROM tbl_user WHERE id_user='$iduser'")->row_array();
        $email_tujuan = $tbl['email'];
        $namauser = $tbl['user_name'];
		if ($senddata) {
			if ($typepayment == "echannel") {
				$this->db->query("INSERT INTO log_transaction (trx_id, id_user, code_transaction, keterangan, payment_method, credit, debit, flag, timelimit) VALUES ('$where[order_id]','$where[id_user]','401','Top up Saldo dengan no Transaksi $where[order_id]','$where[payment_type]','$where[gross_amount]','0','0','$datelimitt')");
			}
			else if ($typepayment == "bank_transfer") {
				$this->db->query("INSERT INTO log_transaction (trx_id, id_user, code_transaction, keterangan, payment_method, credit, debit, flag, timelimit) VALUES ('$where[order_id]','$where[id_user]','401','Top up Saldo dengan no Transaksi $where[order_id]','$where[payment_type]','$where[gross_amount]','0','0','$datelimitt')");
			}
			else
			{
				$this->db->query("INSERT INTO log_transaction (trx_id, id_user, code_transaction, keterangan, payment_method, credit, debit, flag, timelimit) VALUES ('$where[order_id]','$where[id_user]','401','Top up Saldo dengan no Transaksi $where[order_id]','$where[payment_type]','$where[gross_amount]','0','1','$datelimitt')");
			}

			//KIRIM EMAIL
			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' =>'info@classmiles.com',
				'smtp_pass' => 'kerjadiCLASSMILES',	
				'mailtype' => 'html',	
				// 'wordwrap' => true,
				'charset' => 'iso-8859-1'
				);

			$this->load->library('email', $config);
			$this->email->initialize($config);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('info@classmiles.com', 'Info Classmiles');
			$this->email->to($email_tujuan);
			$this->email->subject('Classmiles - Rincian Top up');

			if ($typepayment == 'echannel') {
				$templateverifikasi = 
				"
				<!DOCTYPE html>
				<html>
				<head>	
				<style type='text/css'>

			      html,body { height: 100%; margin: 0px; padding: 0px; }
			      #full { background: #eaebed; height: 100% }

			    </style>
				</head>
				<body style='background-color: #eaebed;'>
					<div style='background: #eaebed; height: 100%;'>			
					<div style='width: 2%; float: left;'>
						<div style='width: 100%; background-color: #ffffff; height: 5px; padding-top:5%;'></div>
					</div>
					<div style='width: 96%; float: left; padding-top:5%;'>
						<div style='width: 100%; background-color: #03a9f4; height: 5px;'></div>
						<div style='padding-left: 32px; padding-right: 32px; padding-top: 10px; padding-bottom: 5px; background-color: white;'>
							<!-- <h1>Classmiles</h1><br> -->
							<label style='font-size: 30px; color: black;'>Classmiles</label><br>
							<label style='font-size: 12px; margin-top: -20px;'>Classroom at your hands</label>
							<hr>
						</div>
						<div style='padding: 32px; margin-top: -15px; background-color: #e0e0e0;'>
							<label style='float: left; width: 50%; text-align: left; font-size: 13.2px; margin-top: -10px; padding-bottom:10px;'>".$hari." ".$bulan." ".$tahun."</label>
							<label style='float: left; width: 50%; text-align: right; font-size: 13.2px; margin-top: -10px; padding-bottom:10px;'>ORDER ID : ".$obj->{'order_id'}."</label>
							<br>
						</div>
						<div style='padding-left: 32px; padding-right: 32px; padding-top: 25px; padding-bottom: 15px; background-color: white;'>
							<label style='font-size: 15px;'>Dear ".$namauser.",</label><br>
							<label style='font-size: 13px; margin-top:4px;'>Transaksi top up saldo Anda Pending! Segera melakukan transfer pembayaran untuk tambah saldo sebelum :</label><br><br>			
						</div>
						<center>
							<div style='background: #ffffff; width: 100%; height: 50px; margin'>
								<label style='background-color: #edecec; padding-top: 2.3%; padding-bottom: 2.3%; padding-left: 5%; padding-right: 5%; margin-top: 10%; font-size:14px;'>
								".$datelimitt."
								</label>
							</div>
						</center>
						<div style='padding-left: 25%; padding-right: 25%; padding-top: 18px; padding-bottom: 20px; background-color: white;'>
							<center>
								<label style='font-size:16px; margin-top:2%;'>Jumlah Pembayaran</label><br><br>
								<label style='font-size:24px;'>Rp. ".$hasilconvert."</label><br><br>
								<label style='font-size:12px;'>Perbedaan nilai transfer akan menghambat proses verifikasi</label><br><br>
							</center>
						</div>
						<div style='width: 100%; padding-left:32px; padding-right:32px; background-color: #eaebed; height: 3px;'></div>
						<div style='padding-left: 10%; padding-right: 10%; padding-top: 5%; padding-bottom: 5%; background-color: white;'>
							<label style='float: left; width: 50%; text-align: left; font-size: 15px; margin-top: -12px;'>Deskripsi</label>
							<label style='float: left; width: 50%; text-align: right; font-size: 15px; margin-top: -12px;'>Harga</label>
							<hr>					
							<label style='float: left; width: 50%; text-align: left; font-size: 15px; margin-top: 5px;'>Top up</label>
							<label style='float: left; width: 50%; text-align: right; font-size: 15px; margin-top: 5px;'>Rp. ".$hasilconvert."</label>
							<br><br>
							<hr>
							<label style='float: right; padding-top: 3%;'>Rp. ".$hasilconvert."</label>
							<label style='float: right; padding-top: 3%; padding-right: 3%;'>Total tagihan :</label>				
							<br><br><br>						
							<label style='float: right; padding-top: 3%;'>Rp. ".$hasilconvert."</label>
							<label style='float: right; padding-top: 3%; padding-right: 3%;'><b>Total Bayar :</b></label>
							<br><br><br>
							<hr>
							
						</div>
						<center>
						<div style='padding-left: 5%; padding-right: 5%; padding-bottom: 1%; height:65px; background-color: #eaebed;'>
							<br><label style='padding: 20px; width: 100%;'>Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.</label><br>
						</div>
						<br><br>
						<div style='padding-left: 5%; padding-right: 5%; padding-top: 20px; padding-bottom: 20px; background-color: #19B5FE;'>
							<label style='width: 100%;'><a style='text-align:center; color:white; text-decoration:none;' href='https://www.classmiles.com'><center>Copyright &copy; 2016 Classmiles</center></a></label>
						</div>						
					</div>		
					<div style='width: 2%; float: left;'>
						<div style='width: 100%; background-color: #ffffff; height: 5px; padding-top:5%;'></div>			
					</div>
					</div>
				</body>
				</html>
				"
				;
				/**/
			}
			else if ($typepayment == 'bank_transfer') {
				$templateverifikasi = 
				"
				<!DOCTYPE html>
				<html>
				<head>	
				<style type='text/css'>

			      html,body { height: 100%; margin: 0px; padding: 0px; }
			      #full { background: #eaebed; height: 100% }

			    </style>
				</head>
				<body style='background-color: #eaebed;'>
					<div style='background: #eaebed; height: 100%;'>			
					<div style='width: 2%; float: left;'>
						<div style='width: 100%; background-color: #ffffff; height: 5px; padding-top:5%;'></div>
					</div>
					<div style='width: 96%; float: left; padding-top:5%;'>
						<div style='width: 100%; background-color: #03a9f4; height: 5px;'></div>
						<div style='padding-left: 32px; padding-right: 32px; padding-top: 10px; padding-bottom: 5px; background-color: white;'>
							<!-- <h1>Classmiles</h1><br> -->
							<label style='font-size: 30px; color: black;'>Classmiles</label><br>
							<label style='font-size: 12px; margin-top: -20px;'>Classroom at your hands</label>
							<hr>
						</div>
						<div style='padding: 32px; margin-top: -15px; background-color: #e0e0e0;'>
							<label style='float: left; width: 50%; text-align: left; font-size: 13.2px; margin-top: -10px; padding-bottom:10px;'>".$hari." ".$bulan." ".$tahun."</label>
							<label style='float: left; width: 50%; text-align: right; font-size: 13.2px; margin-top: -10px; padding-bottom:10px;'>ORDER ID : ".$obj->{'order_id'}."</label>
							<br>
						</div>
						<div style='padding-left: 32px; padding-right: 32px; padding-top: 25px; padding-bottom: 15px; background-color: white;'>
							<label style='font-size: 15px;'>Dear ".$namauser.",</label><br>
							<label style='font-size: 13px; margin-top:4px;'>Transaksi top up saldo Anda Pending! Segera melakukan transfer pembayaran untuk tambah saldo sebelum :</label><br><br>			
						</div>
						<center>
							<div style='background: #ffffff; width: 100%; height: 50px; margin'>
								<label style='background-color: #edecec; padding-top: 2.3%; padding-bottom: 2.3%; padding-left: 5%; padding-right: 5%; margin-top: 10%; font-size:14px;'>
								".$datelimitt."
								</label>
							</div>
						</center>
						<div style='padding-left: 25%; padding-right: 25%; padding-top: 18px; padding-bottom: 20px; background-color: white;'>
							<center>
								<label style='font-size:16px; margin-top:2%;'>Jumlah Pembayaran</label><br><br>
								<label style='font-size:24px;'>Rp. ".$hasilconvert."</label><br><br>
								<label style='font-size:12px;'>Perbedaan nilai transfer akan menghambat proses verifikasi</label><br><br>
							</center>
						</div>
						<div style='width: 100%; padding-left:32px; padding-right:32px; background-color: #eaebed; height: 3px;'></div>
						<div style='padding-left: 10%; padding-right: 10%; padding-top: 5%; padding-bottom: 5%; background-color: white;'>
							<label style='float: left; width: 50%; text-align: left; font-size: 15px; margin-top: -12px;'>Deskripsi</label>
							<label style='float: left; width: 50%; text-align: right; font-size: 15px; margin-top: -12px;'>Harga</label>
							<hr>					
							<label style='float: left; width: 50%; text-align: left; font-size: 15px; margin-top: 5px;'>Top up</label>
							<label style='float: left; width: 50%; text-align: right; font-size: 15px; margin-top: 5px;'>Rp. ".$hasilconvert."</label>
							<br><br>
							<hr>
							<label style='float: right; padding-top: 3%;'>Rp. ".$hasilconvert."</label>
							<label style='float: right; padding-top: 3%; padding-right: 3%;'>Total tagihan :</label>				
							<br><br><br>						
							<label style='float: right; padding-top: 3%;'>Rp. ".$hasilconvert."</label>
							<label style='float: right; padding-top: 3%; padding-right: 3%;'><b>Total Bayar :</b></label>
							<br><br><br>
							<hr>
							
						</div>
						<center>
						<div style='padding-left: 5%; padding-right: 5%; padding-bottom: 1%; height:65px; background-color: #eaebed;'>
							<br><label style='padding: 20px; width: 100%;'>Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.</label><br>
						</div>
						<br><br>
						<div style='padding-left: 5%; padding-right: 5%; padding-top: 20px; padding-bottom: 20px; background-color: #19B5FE;'>
							<label style='width: 100%;'><a style='text-align:center; color:white; text-decoration:none;' href='https://www.classmiles.com'><center>Copyright &copy; 2016 Classmiles</center></a></label>
						</div>						
					</div>		
					<div style='width: 2%; float: left;'>
						<div style='width: 100%; background-color: #ffffff; height: 5px; padding-top:5%;'></div>			
					</div>
					</div>
				</body>
				</html>
				"
				;
				/**/
			}
			else
			{
				$templateverifikasi = 
				"
				<!DOCTYPE html>
				<html>
				<head>	
				<style type='text/css'>

			      html,body { height: 100%; margin: 0px; padding: 0px; }
			      #full { background: #eaebed; height: 100% }

			    </style>
				</head>
				<body style='background-color: #eaebed;'>
					<div style='background: #eaebed; height: 100%;'>			
					<div style='width: 2%; float: left;'>
						<div style='width: 100%; background-color: #ffffff; height: 5px; padding-top:5%;'></div>
					</div>
					<div style='width: 96%; float: left; padding-top:5%;'>
						<div style='width: 100%; background-color: #03a9f4; height: 5px;'></div>
						<div style='padding-left: 32px; padding-right: 32px; padding-top: 10px; padding-bottom: 5px; background-color: white;'>
							<!-- <h1>Classmiles</h1><br> -->
							<label style='font-size: 30px; color: black;'>Classmiles</label><br>
							<label style='font-size: 12px; margin-top: -20px;'>Classroom at your hands</label>
							<hr>
						</div>
						<div style='padding: 32px; margin-top: -15px; background-color: #e0e0e0;'>
							<label style='float: left; width: 50%; text-align: left; font-size: 13.2px; margin-top: -10px; padding-bottom:10px;'>".$hari." ".$bulan." ".$tahun."</label>
							<label style='float: left; width: 50%; text-align: right; font-size: 13.2px; margin-top: -10px; padding-bottom:10px;'>ORDER ID : ".$obj->{'order_id'}."</label>
							<br>
						</div>
						<div style='padding-left: 32px; padding-right: 32px; padding-top: 25px; padding-bottom: 15px; background-color: white;'>
							<label style='font-size: 15px;'>Dear ".$namauser.",</label><br>
							<label style='font-size: 13px; margin-top:4px;'>Transaksi top up saldo Anda berhasil! Rincian top up Anda di bawah ini: </label><br><br>
						</div>
						<div style='padding-left: 25%; padding-right: 25%; padding-top: 18px; padding-bottom: 20px; background-color: white;'>
							<center>
								<label style='font-size:20px;'>Total top up Saldo</label><br><br>
								<label style='font-size:24px;'>Rp. ".$hasilconvert."</label><br><br>
							</center>
						</div>
						<div style='width: 100%; padding-left:32px; padding-right:32px; background-color: #eaebed; height: 3px;'></div>
						<div style='padding-left: 10%; padding-right: 10%; padding-top: 5%; padding-bottom: 5%; background-color: white;'>
							<label style='float: left; width: 50%; text-align: left; font-size: 15px; margin-top: -12px;'>Deskripsi</label>
							<label style='float: left; width: 50%; text-align: right; font-size: 15px; margin-top: -12px;'>Harga</label>
							<hr>					
							<label style='float: left; width: 50%; text-align: left; font-size: 15px; margin-top: 5px;'>Top up</label>
							<label style='float: left; width: 50%; text-align: right; font-size: 15px; margin-top: 5px;'>Rp. ".$hasilconvert."</label>
							<br><br>
							<hr>
							<label style='float: right; padding-top: 3%;'>Rp. ".$hasilconvert."</label>
							<label style='float: right; padding-top: 3%; padding-right: 3%;'>Total tagihan :</label>				
							<br><br><br>						
							<label style='float: right; padding-top: 3%;'>Rp. ".$hasilconvert."</label>
							<label style='float: right; padding-top: 3%; padding-right: 3%;'><b>Total Bayar :</b></label>
							<br><br><br>
							<hr>
							
						</div>
						<center>
						<div style='padding-left: 5%; padding-right: 5%; padding-bottom: 1%; height:65px; background-color: #eaebed;'>
							<br><label style='padding: 20px; width: 100%;'>Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.</label><br>
						</div>
						<br><br>
						<div style='padding-left: 5%; padding-right: 5%; padding-top: 20px; padding-bottom: 20px; background-color: #19B5FE;'>
							<label style='width: 100%;'><a style='text-align:center; color:white; text-decoration:none;' href='https://www.classmiles.com'><center>Copyright &copy; 2016 Classmiles</center></a></label>
						</div>						
					</div>		
					<div style='width: 2%; float: left;'>
						<div style='width: 100%; background-color: #ffffff; height: 5px; padding-top:5%;'></div>			
					</div>
					</div>
				</body>
				</html>
				"
				;
				/**/
			}
			/**/				
			$this->email->message($templateverifikasi);

			if ($this->email->send()) 
			{
				$rets['status'] = true;
				$rets['message'] = "Succeed";
				$this->response($rets);
			}
			else
			{
				$rets['status'] = false;
				$rets['message'] = "failed";
				$this->response($rets);
			}	
		}
		else
		{	
			$rets['status'] = false;
			$rets['message'] = "failed";
			$this->response($rets);
		}
	}

	public function mid_gettopup_post()
	{
		$iduser = $this->post('id_user');
		// $ambil = $this->db->query("SELECT lm.payment_type, lm.status_message, lo.* FROM log_midtrans_transaction as lm INNER JOIN log_order as lo ON lm.order_id=lo.order_id WHERE lo.id_user='$iduser' ORDER BY lo.timestamp DESC")->result_array(); 
		$ambil = $this->db->query("SELECT * FROM log_transaction WHERE id_user='$iduser' ORDER BY timestamp DESC")->result_array(); 
		if ($ambil) {
			$rets['status'] = true;
			$rets['message'] = "Succeed";
			$rets['data'] = $ambil;
			$this->response($rets);
		}
		else
		{	
			$rets['status'] = true;
			$rets['message'] = "failed";
			$rets['data'] = $ambil;
			$this->response($rets);
		}
	}

	public function getbalance_post()
	{
		$iduser = $this->post('id_user');
		$get = $this->db->query("SELECT active_balance FROM uangsaku WHERE id_user='$iduser'")->row_array();
		if (empty($get))
    	{
    		$res['status'] = true;
			$res['message'] = 'empty balance';
			$res['data'] = '0';
			$this->response($res);
    	}
    	else
    	{
	    	$res['status'] = true;
			$res['message'] = 'successful';
		    $res['data'] = $get['active_balance'];
			$this->response($res);
		}
	}

	public function mid_addbank_post()
	{
		$orderid = $this->post('order_id');
		$iduser  = $this->post('id_user');
		$nominal = $this->post('nominal');
		
		//add limit 2 day 
		$date = date("Y-m-d H:i:s");
		$datetime = new DateTime($date);
		$datetime->modify('+2 day');
		$limitdate = $datetime->format('Y-m-d H:i:s');
		$processtradd = $this->db->query("INSERT INTO log_transaction (trx_id, id_user, code_transaction, keterangan, payment_method, credit, debit, flag, timelimit) VALUES ('$orderid','$iduser','402','Top up saldo dengan no Transaksi $orderid','bank_transfer','$nominal','0','0','$limitdate')");

		$select = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduser'")->row_array();
		$email_tujuan = $select['email'];
		$namauser = $select['user_name'];

		$dateee = date_create($date);
        $datee = date_format($dateee, 'd/m/y');
        $hari = date_format($dateee, 'd');
        $tahun = date_format($dateee, 'Y');
                                                
        $dateeee = $datee;
        $sepparator = '/';
        $parts = explode($sepparator, $dateeee);
        $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));

        //untuk timelimit 
        $datelimit = date_create($limitdate);
        $dateelimit = date_format($datelimit, 'd/m/y');
        $harilimit = date_format($datelimit, 'd');
        $tahunlimit = date_format($datelimit, 'Y');
        $waktulimit = date_format($datelimit, 'H:s');
                                                
        $datelimit = $dateelimit;
        $sepparatorlimit = '/';
        $partslimit = explode($sepparatorlimit, $datelimit);
        $bulanlimit = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
        $hasilconvert = number_format($nominal, 0, ".", ".");

    	// $processtradd = $this->db->query("INSERT INTO log_bank_transfer (order_id, id_user, jumlah, flag_tr) VALUES ('$orderid','$iduser','$nominal','0')");
    	if ($processtradd) {		
    		$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' =>'info@classmiles.com',
				'smtp_pass' => 'kerjadiCLASSMILES',	
				'mailtype' => 'html',	
				// 'wordwrap' => true,
				'charset' => 'iso-8859-1'
				);

			$this->load->library('email', $config);
			$this->email->initialize($config);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('info@classmiles.com', 'Info Classmiles');
			$this->email->to($email_tujuan);
			$this->email->subject('Classmiles - Rincian Top up');

			$templateverifikasi = 
			"
			<!DOCTYPE html>
			<html>
			<head>	
			<style type='text/css'>

		      html,body { height: 100%; margin: 0px; padding: 0px; }
		      #full { background: #ffffff; height: 100% }

		    </style>
			</head>
			<body style='background-color: #ffffff;'>
				<div style='background: #ffffff; height: 100%;'>			
				<div style='width: 2%; float: left;'>
					<div style='width: 100%; background-color: #ffffff; height: 5px; padding-top:5%;'></div>
				</div>
				<div style='width: 96%; float: left; padding-top:5%;'>
					<div style='width: 100%; background-color: #03a9f4; height: 5px;'></div>
					<div style='padding-left: 32px; padding-right: 32px; padding-top: 10px; padding-bottom: 5px; background-color: white;'>
						<!-- <h1>Classmiles</h1><br> -->
						<label style='font-size: 30px; color: black;'>Classmiles</label><br>
						<label style='font-size: 12px; margin-top: -20px;'>Classroom at your hands</label>
						<hr>
					</div>
					<div style='padding: 32px; margin-top: -15px; background-color: #e0e0e0;'>
						<label style='float: left; width: 50%; text-align: left; font-size: 13.2px; margin-top: -10px;'>".$hari." ".$bulan." ".$tahun."</label>
						<label style='float: left; width: 50%; text-align: right; font-size: 13.2px; margin-top: -10px;'>ORDER ID : ".$orderid."</label>
						<br>
					</div>
					<div style='padding-left: 32px; padding-right: 32px; padding-top: 25px; padding-bottom: 15px; background-color: white;'>
						<label style='font-size: 15px;'>Dear ".$namauser.",</label><br>
						<label style='font-size: 13px; margin-top:4px;'>Anda sudah memilih Top up saldo menggunakan Transfer Bank untuk pemesanan pada tanggal ".$hari." ".$bulan." ".$tahun."</label><br><br>			
					</div>
					<div style='padding-left: 25%; padding-right: 25%; padding-top: 1%; padding-bottom: 1%; background-color: white;'>
						<center>
							<label style='font-size:20px;'>Total top up Saldo</label><br><br>
							<label style='font-size:24px;'>Rp. ".$hasilconvert."</label><br><br>
						</center>
					</div>
					<div style='padding-left: 3%; padding-right: 3%; padding-top: 2%; padding-bottom: 2%; background-color: white;'>
						<center>
							<label style='font-size: 15px;'>Mohon melakukan pembayaran paling lambat tanggal <b>".$harilimit." ".$bulanlimit." ".$tahunlimit."</b>.</label>
						</center>						  
					</div>
					<div style='width: 100%; padding-left:32px; padding-right:32px; background-color: #eaebed; height: 3px;'></div>
					<div style='padding-left: 10%; padding-right: 10%; padding-top: 5%; padding-bottom: 5%; background-color: white;'>
						<label style='float: left; width: 50%; text-align: left; font-size: 15px; margin-top: -12px;'>Deskripsi</label>
						<label style='float: left; width: 50%; text-align: right; font-size: 15px; margin-top: -12px;'>Harga</label>
						<hr>					
						<label style='float: left; width: 50%; text-align: left; font-size: 15px; margin-top: 5px;'>Top up</label>
						<label style='float: left; width: 50%; text-align: right; font-size: 15px; margin-top: 5px;'>Rp. ".$hasilconvert."</label>
						<br><br>
						<hr>
						<label style='float: right; padding-top: 3%;'>Rp. ".$hasilconvert."</label>
						<label style='float: right; padding-top: 3%; padding-right: 3%;'>Total tagihan :</label>				
						<br><br><br>
						<label style='float: right; padding-top: 3%;'>Rp. 001</label>
						<label style='float: right; padding-top: 3%; padding-right: 5%;'>Kode unik :</label>				
						<br><br><br>
						<label style='float: right; padding-top: 3%;'>Rp. ".$hasilconvert."</label>
						<label style='float: right; padding-top: 3%; padding-right: 3%;'><b>Total Bayar :</b></label>
						<br><br><br>
						<hr>
						
					</div>
					<center>
					<div style='padding-left: 5%; padding-right: 5%; padding-bottom: 1%; height:65px; background-color: #eaebed;'>
						<br><label style='padding: 20px; width: 100%;'>Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.</label><br>
					</div>
					<br><br>
					<div style='padding-left: 5%; padding-right: 5%; padding-top: 20px; padding-bottom: 20px; background-color: #19B5FE;'>
						<label style='width: 100%;'><a style='text-align:center; color:white; text-decoration:none;' href='https://www.classmiles.com'><center>Copyright &copy; 2016 Classmiles</center></a></label>
					</div>						
				</div>		
				<div style='width: 2%; float: left;'>
					<div style='width: 100%; background-color: #ffffff; height: 5px; padding-top:5%;'></div>			
				</div>
				</div>
			</body>
			</html>
			"
			;
			/**/				
			$this->email->message($templateverifikasi);

			if ($this->email->send()) 
			{
				$rets['status']  = true;
				$rets['message'] = "Succeed";
				$rets['data']	 = $limitdate;
				$this->response($rets);
			}
			else
			{
				$rets['status'] = false;
				$rets['message'] = "failed";
				$rets['data']	 = "";
				$this->response($rets);
			}
    	}
    	else
    	{
    		$res['status'] = true;
			$res['message'] = 'empty balance';			
			$this->response($res);
    	}
	}

	public function databank_get()
	{
		$getdata = $this->db->query("SELECT * FROM master_bank")->result_array();
		if (empty($getdata)) {
			$res['status'] = true;
			$res['message'] = 'empty data bank';			
			$this->response($res);
		}
		else
		{
			$res['status'] = true;
			$res['message'] = 'successful';
			$res['data'] = $getdata;		    
			$this->response($res);
		}
	}

	public function get_confirmpayment_post()
	{
		$user = $this->post('id_user');
		$getdata = $this->db->query("SELECT ltc.trx_id, ltc.trfc_id ,lt.*, timestamp + INTERVAL 1 DAY as timelimit FROM log_transaction as lt LEFT JOIN log_trf_confirmation as ltc ON ltc.trx_id=lt.trx_id WHERE lt.id_user='$user' AND lt.flag='0' AND (timestamp + INTERVAL 1 DAY)>NOW() ORDER BY timestamp DESC")->result_array();

		if (empty($getdata)) {
			$res['status']  = true;
			$res['message'] = 'empty data';
			$res['data']	= $getdata;			
			$this->response($res);
		}
		else
		{			
			$res['status'] 	= true;
			$res['message'] = 'successful';
			$res['data'] 	= $getdata;		    
			$this->response($res);
		}
	}
	public function payment_confirmation_post($value='')
	{		
		// print_r('<pre>');
		// print_r($this->post());
		// print_r('</pre>');
		// return null;
		// $this->db->simple_query("INSERT INTO test(a) VALUES('".$this->post('bukti_tr')."')");
		// return null;


    	$tokenid 			= $this->post('trx_id');
		$id 				= $this->post('id_user');
    	$bank 				= $this->post("bank_id");    	
    	$nominalsave	 	= $this->post("jumlah");
    	$norekening 	 	= $this->post("norek_tr");
    	$namapemilikbank 	= $this->post("nama_tr");
    	$metodepembayaran 	= $this->post("metod_tr");
    	$buktibayar 		= $id.'_'.$tokenid.'.jpg';
    	$bukti 				= $this->post("bukti_tr");
    	// $this->db->simple_query("INSERT INTO test(a) VALUES('{$bukti}')");

		// $gambar_base64 = $DATA_DARI_POST['bukti_tr'];
    	$processadd = $this->db->query("INSERT INTO log_trf_confirmation (trx_id, id_user, bank_id, jumlah, norek_tr, nama_tr, metod_tr, bukti_tr, accepted) VALUES ('$tokenid','$id','$bank','$nominalsave','$norekening','$namapemilikbank','$metodepembayaran','$buktibayar','0')");
    	if ($processadd) {    	
    		if($bukti != ''){
				$imgname = "https://beta.dev.meetaza.com/classmiles/aset/img/user/".$buktibayar;
				$image = 'data:image/jpeg;base64,' .str_replace(' ','+',$bukti); //replacing ' ' with '+'				
					 
				//Write to disk
				file_put_contents("aset/img/user/".$buktibayar,file_get_contents($image));
                $res['status'] = true;
                $res['img'] = $image;
				$res['message'] = 'Sukses bro dengan gambar';
				$this->response($res);

				// $imsrc = base64_decode($imsrc);
				// $imgname = "aset/img/user/".$buktibayar;
				// $fp = fopen($imgname, 'w');
				// fwrite($fp, $imsrc);
				// if(fclose($fp)){
				// }else{
				// 	$res['status'] = false;
				// 	$res['message'] = 'Gagal bro';			
				// 	$this->response($res);
				// }
	            
        	}

			// $config['image_library'] = 'gd2';
			// $config['source_image']	= "aset/img/user/".$buktibayar.".jpg";
			// $config['width']	= 250;
			// $config['maintain_ratio'] = FALSE;
			// $config['height']	= 250;

			// $this->image_lib->initialize($config); 

			// $this->image_lib->resize();
			
			$res['status'] = true;
			$res['message'] = 'Sukses bro';		    
			$this->response($res);
		}
		else
		{
			$res['status'] = false;
			$res['message'] = 'Gagal bro';			
			$this->response($res);
		}	
	}

	public function update_payment_confirmation_post($value='')
	{		
		// print_r('<pre>');
		// print_r($this->post());
		// print_r('</pre>');
		// return null;
		// $this->db->simple_query("INSERT INTO test(a) VALUES('".$this->post('bukti_tr')."')");
		// return null;


    	$tokenid 			= $this->post('trx_id');
		$id 				= $this->post('id_user');
    	$bank 				= $this->post("bank_id");    	
    	$nominalsave	 	= $this->post("jumlah");
    	$norekening 	 	= $this->post("norek_tr");
    	$namapemilikbank 	= $this->post("nama_tr");
    	$metodepembayaran 	= $this->post("metod_tr");
    	$buktibayar 		= $id.'_'.$tokenid;
    	$bukti 				= $this->post("bukti_tr");

		// $gambar_base64 = $DATA_DARI_POST['bukti_tr'];
    	$processadd = $this->db->query("UPDATE log_trf_confirmation SET id_user='$id',bank_id='$bank',jumlah='$nominalsave',norek_tr='$norekening',nama_tr='$namapemilikbank',metod_tr='$metodepembayaran',bukti_tr='no_image.jpg',accepted = '0' WHERE trx_id='$tokenid'");
    	if ($processadd) {    	
    	
			// file_put_contents("./aset/img/user/".$buktibayar.".jpg",file_get_contents($bukti));

			// $config['image_library'] = 'gd2';
			// $config['source_image']	= "aset/img/user/".$buktibayar.".jpg";
			// $config['width']	= 250;
			// $config['maintain_ratio'] = FALSE;
			// $config['height']	= 250;

			// $this->image_lib->initialize($config); 

			// $this->image_lib->resize();
			
			$res['status'] = true;
			$res['message'] = 'Sukses bro';		    
			$this->response($res);
		}
		else
		{
			$res['status'] = true;
			$res['message'] = 'Gagal bro';			
			$this->response($res);
		}	
	}

	public function edit_payment_confirmation_post()
	{
		$trx_id 	= $this->post('trx_id');
		$a 			= $this->db->query("SELECT ltc.*, lt.credit FROM log_trf_confirmation as ltc INNER JOIN log_transaction as lt ON lt.trx_id=ltc.trx_id WHERE ltc.trx_id='$trx_id'")->row_array();
		if ($a) {		
			$res['status'] 	= true;
			$res['message'] = 'Sukses bro';
			$res['data'] 	= $a;		    
			$this->response($res);
		}
		else
		{
			$res['status'] 	= true;
			$res['message'] = 'Gagal bro';
			$res['data'] 	= $a;			
			$this->response($res);
		}
	}

	public function details_payment_confirmation_post()
	{
		$trx_id 	= $this->post('trx_id');
		$b 			= $this->db->query("SELECT * FROM log_transaction WHERE trx_id='$trx_id'")->row_array();
		if ($b) {			
			$res['status'] 	= true;
			$res['message'] = 'Sukses bro';
			$res['data'] 	= $b;		    
			$this->response($res);
		}
		else
		{
			$res['status'] 	= true;
			$res['message'] = 'Gagal bro';
			$res['data'] 	= $b;			
			$this->response($res);
		}
	}

	public function handling_post(){
		$result = $this->post();

		$order_id 		= $result['order_id'];
		$masked_card 	= $result['masked_card'];
		$bank 			= $result['bank'];
		$approval_code  = $result['approval_code'];
		$gross_amount   = $result['gross_amount'];
		$signature_key	= $result['signature_key'];
		$transaction_status = $result['transaction_status'];
		$status_code	= $result['status_code'];
		$transaction_id = $result['transaction_id'];

		$ckorderid	= $this->db->query("SELECT * FROM log_midtrans_transaction WHERE order_id='$order_id'")->row_array();
		if (!empty($ckorderid)) {
			$dts = $ckorderid['transaction_status'];
			$dga = $ckorderid['gross_amount'];
			
			if ($gross_amount == $dga) {
				$updlog = $this->db->query("UPDATE log_midtrans_transaction SET masked_card='$masked_card',approval_code='$approval_code',bank='$bank',signature_key='$signature_key',transaction_status='$transaction_status' WHERE order_id='$order_id'");
				if ($updlog) {
					$this->db->query("UPDATE log_transaction SET flag='1' WHERE trx_id='$order_id'");
				}

				$getdatatransaction = $this->db->query("SELECT lt.*, tu.email, tu.user_name FROM log_transaction as lt INNER JOIN tbl_user as tu ON lt.id_user=tu.id_user WHERE trx_id='$order_id'")->row_array();

		    	$nominal		= $getdatatransaction['credit'];
		    	$hasilconvert 	= number_format($nominal, 0, ".", ".");
		    	$email_tujuan	= $getdatatransaction['email'];
		    	$username 		= $getdatatransaction['user_name'];
		    	$config = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'ssl://smtp.googlemail.com',
					'smtp_port' => 465,
					'smtp_user' =>'info@classmiles.com',
					'smtp_pass' => 'kerjadiCLASSMILES',	
					'mailtype' => 'html',	
					// 'wordwrap' => true,
					'charset' => 'iso-8859-1'
					);

				$this->load->library('email', $config);
				$this->email->initialize($config);
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");
				$this->email->from('info@classmiles.com', 'Info Classmiles');
				$this->email->to($email_tujuan);
				$this->email->subject('Classmiles - Selamat, Topup Anda BERHASIL');

				$templateverifikasi = 
				"
				<!DOCTYPE html>
				<html>
				<head>	
				<style type='text/css'>

			      	html,body {height: 100%; margin: 0px; padding: 0px; }
			      	#full { background: #ffffff; height: 100% }
			    </style>
				</head>
				<body style='background-color: #ffffff; align-content: center;'>
					<div style='background: #ffffff; height: 100%; width: 100%;'>			
						<div style='width: 2%; float: left;'>
							<div style='width: 100%; background-color: #ffffff; height: 5px; padding-top:5%;'></div>
						</div>
						<div style='width: 96%; float: left; padding-top:5%;'>
							<div style='width: 100%; background-color: #009933; height: 5px;'></div>
							<div style='padding-left: 32px; padding-right: 32px; padding-top: 10px; padding-bottom: 5px; background-color: white;'>
								<!-- <h1>Classmiles</h1><br> -->
								<label style='font-size: 30px; color: black;'>Classmiles</label><br>
								<label style='font-size: 12px; margin-top: -20px;'>Classroom at your hands</label>
								<hr>
							</div>
							<div style='padding: 32px; margin-top: -15px; background-color: #e0e0e0;'>
								<label style='float: left; width: 50%; text-align: left; font-size: 16px; margin-top: -10px;'>Selamat, Transaksi Topup Anda BERHASIL</label>
								<label style='float: left; width: 50%; text-align: right; font-size: 13.2px; margin-top: -10px;'>ORDER ID : ".$order_id."</label>
								<br>
							</div>										
							<div style='width: 30%; padding-left:32px; padding-right:32px; background-color: #eaebed; height: 3px;'></div>
							<div style='padding-left: 10%; padding-right: 10%; padding-top: 5%; padding-bottom: 5%; background-color: white;'>
								<label style='float: left; width: 100%; text-align: left; font-size: 15px;'>Rincian Topup ".$username."</label>
								<br>
								<hr>
								<label style='float: left; width: 50%; text-align: left; font-size: 15px; margin-top: -12px;'>Deskripsi</label>
								<label style='float: left; width: 50%; text-align: right; font-size: 15px; margin-top: -12px;'>Harga</label>
								<hr>					
								<label style='float: left; width: 50%; text-align: left; font-size: 15px; margin-top: 5px;'>Top up</label>
								<label style='float: left; width: 50%; text-align: right; font-size: 15px; margin-top: 5px;'>Rp. ".$hasilconvert."</label>
								<br><br>
								<hr>
								<label style='float: right; padding-top: 3%;'>Rp. ".$hasilconvert."</label>
								<label style='float: right; padding-top: 3%; padding-right: 3%;'>Total tagihan :</label>				
								<br><br><br>						
								<label style='float: right; padding-top: 3%;'>Rp. ".$hasilconvert."</label>
								<label style='float: right; padding-top: 3%; padding-right: 3%;'><b>Total Bayar :</b></label>
								<br><br><br>
								<hr>
								
							</div>
							<center>
							<div style='padding-left: 5%; padding-right: 5%; padding-bottom: 1%; height:65px; background-color: #eaebed;'>
								<br><label style='padding: 20px; width: 100%;'>Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.</label><br>
							</div>
							<br><br>
							<div style='padding-left: 5%; padding-right: 5%; padding-top: 20px; padding-bottom: 20px; background-color: #009933;'>
								<label style='width: 100%;'><a style='text-align:center; color:white; text-decoration:none;' href='https://www.classmiles.com'><center>Copyright &copy; 2016 Classmiles</center></a></label>
							</div>						
						</div>		
						<div style='width: 2%; float: left;'>
							<div style='width: 100%; background-color: #ffffff; height: 5px; padding-top:5%;'></div>			
						</div>
					</div>
				</body>
				</html>
				"
				;
				/**/				
				$this->email->message($templateverifikasi);

				if ($this->email->send()) 
				{
					$res['status'] = true;
					$res['message'] = 'success verify';
					$res['code'] = 200;			
					$this->response($res);
				}
				else
				{
					$res['status'] = false;
					$res['message'] = 'failed send email';
					$res['code'] = 300;			
					$this->response($res);
				}				

			}
		}

		// print_r($order_id);
		// $this->db->query("INSERT INTO coba VALUES('','$a')");
		// $res['status'] 	= true;
		// 	$res['message'] = 'Sukses bro';		    
		// 	$this->response($res);
	}
}