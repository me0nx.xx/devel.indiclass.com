<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'aset/simple_html_dom.php';

class Process extends CI_Controller {

	public $sesi = array("lang" => "indonesia");	

	public function __construct()
	{
		parent::__construct(); $this->load_lang($this->session->userdata('lang'));
		$this->load->database();
		$params = array('server_key' => 'VT-server-31TBIo8Zf4Pa-I7j4pKj8CID', 'production' => true);
		// $params = array('server_key' => 'VT-server-UmFRMvHDqQzze0KcGM8RDc5C', 'production' => false);
		$this->load->library('midtrans');
		$this->midtrans->config($params);
		$this->load->library('email');
		$this->load->helper(array('url'));       
		// $this->load->model('Master_model'); 
        $this->load->library('Mobile_Detect');
        $this->load->library('paypal_lib');        
        $detect = new Mobile_Detect();
        // Check for any mobile device.
        if ($detect->isMobile()){
            define('MOBILE', 'mobile/');
        }else{
            define('MOBILE', '');
        }
	}

	public function demand_me()
	{
		$subject_id = $this->input->get('subject_id');
		$tutor_id = $this->input->get('tutor_id');
		$start_time = $this->input->get('start_time');
		$date = $this->input->get('date');
		$user_utc = $this->input->get('user_utc');		
		$duration = $this->input->get('duration');
        $topic = $this->input->get('topic');
        $hargaakhir = $this->input->get('hr');
        $hargatutor = $this->input->get('hrt');
        // $hargaakhir = ($duration/900)*$harga;
        $id_requester = $this->input->get('id_user');
        if ($id_requester == '') {
            $id_requester = $this->session->userdata('id_user');
        }

		if ($this->session_check() == 1) {
			$res = $this->Process_model->demand_me($subject_id,$tutor_id,$start_time,$date,$duration,$user_utc,$topic,$id_requester,$hargaakhir,$hargatutor);
			$lastrequestid  = $this->db->select('request_id')->order_by('request_id','desc')->limit(1)->get('tbl_request')->row('request_id');

			$getdataclass 	= $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request as tr ON tu.id_user=tr.tutor_id) LEFT JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='private' AND tr.request_id='$lastrequestid'")->row_array();

            // echo $getdataclass['date_requested'];
            // echo "SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='private' AND tr.request_id='$lastrequestid'";
            // return false;
            $emailtutor         = $this->db->query("SELECT email FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['email'];
            $nametutor          = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_name'];
			$iduserrequest		= $getdataclass['id_user_requester'];

            //CEK IF NOT PARENT 
            $ck                 = $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['usertype_id'];
            if ($ck == 'student kid') {
                $getidparent    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$iduserrequest'")->row_array()['parent_id'];
                $namauser       = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$getidparent'")->row_array()['user_name'];
            }
            else
            {
                $namauser       = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['user_name'];
            }
			

            $durasi             = $getdataclass['duration_requested'];

			$hargakelas         = number_format($hargatutor, 0, ".", ".");
            $server_utcc        = $this->Rumus->getGMTOffset();
            $intervall          = $user_utc - $server_utcc;
            $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$getdataclass['date_requested']);
            $tanggal->modify("+".$intervall ." minutes");
            $tanggal            = $tanggal->format('Y-m-d H:i:s');
            $datelimit          = date_create($tanggal);
            $dateelimit         = date_format($datelimit, 'd/m/y');
            $harilimit          = date_format($datelimit, 'd');
            $hari               = date_format($datelimit, 'l');
            $tahunlimit         = date_format($datelimit, 'Y');
            $waktulimit         = date_format($datelimit, 'H:i');   
            $datelimit          = $dateelimit;
            $sepparatorlimit    = '/';
            $partslimit         = explode($sepparatorlimit, $datelimit);
            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
            $seconds 			= $getdataclass['duration_requested'];
		    $hours 				= floor($seconds / 3600);
		    $mins 				= floor($seconds / 60 % 60);
		    $secs 				= floor($seconds % 60);
		    $durationrequested 	= sprintf('%02d:%02d', $hours, $mins);

            //LOG DEMAND
            $myip = $_SERVER['REMOTE_ADDR'];
            $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$lastrequestid','student request private class','$iduserrequest','private','$myip','101')");
            //END LOG DEMAND

			if($res == 1){
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
                curl_setopt($ch, CURLOPT_POST, true);
                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_requestprivate','content' => array("email" => $this->session->userdata('email'), "namauser" => $namauser, "hargakelas" => $hargakelas, "getdataclass" => $getdataclass, "harilimit" => $harilimit, "hari" => $hari, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "nametutor" => $nametutor,  "durationrequested" => $durationrequested, "emailtutor" => $emailtutor  ) )));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;

                //TAMBAH KERANJANG                    
                // $this->db->query("INSERT INTO tbl_keranjang (request_id,id_user,price) VALUES ('$lastrequestid','$id_requester','$hargaakhir')");
                //END KERANJANG
                ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
                $json_notif = "";
                $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$iduserrequest}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
                if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                    $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "30","title" => "Classmiles", "text" => "")));
                }
                if($json_notif != ""){
                    $headers = array(
                        'Authorization: key=' . FIREBASE_API_KEY,
                        'Content-Type: application/json'
                        );
                    // Open connection
                    $ch = curl_init();

                    // Set the url, number of POST vars, POST data
                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                    // Execute post
                    $result = curl_exec($ch);
                    curl_close($ch);
                    // echo $result;
                }
                ////------------ END -------------------------------------//
				
                //NOTIF STUDENT
                $notif_message = json_encode( array("code" => 114));                    
                $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$iduserrequest','','0')");
                $json = array("status" => 1,"message" => "Sukses");
                $load = $this->lang->line('successfullydemand');
                $this->session->set_flashdata('mes_alert','success');
                $this->session->set_flashdata('mes_display','block');
                $this->session->set_flashdata('mes_message',$load);
                echo json_encode($json);
				
			}else{
				//NOTIF STUDENT
				$notif_message = json_encode( array("code" => 115));					
				$this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$iduserrequest','','0')");
				$json = array("status" => -1,"message" => "Gagal");
				$load = $this->lang->line('faileddemand');
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message',$load);
				echo json_encode($json);
			}
			
		}
	}

	public function demand_me_kids()
	{
		$subject_id = $this->input->get('subject_id');
		$tutor_id = $this->input->get('tutor_id');
		$start_time = $this->input->get('start_time');
		$date = $this->input->get('date');
		$user_utc = $this->input->get('user_utc');
		$child_id = $this->input->get('child_id');
		$duration = $this->input->get('duration');
        $topic    = $this->input->get('topic');
        $harga    = $this->input->get('hr');
        $hargaakhir = ($duration*900)/$harga;

		if ($this->session_check() == 1) {
			$res = $this->Process_model->demand_me_kids($subject_id,$tutor_id,$start_time,$date,$duration,$user_utc,$child_id,$topic);
            $lastrequestid  = $this->db->select('request_id')->order_by('request_id','desc')->limit(1)->get('tbl_request')->row('request_id');

            $getdataclass   = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='private' AND tr.request_id='$lastrequestid'")->row_array();

            $emailtutor         = $this->db->query("SELECT email FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['email'];
            $nametutor          = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_name'];
            $iduserrequest      = $getdataclass['id_user_requester'];

            //CEK IF NOT PARENT 
            $ck                 = $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['usertype_id'];
            if ($ck == 'student kid') {
                $getidparent    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$iduserrequest'")->row_array()['parent_id'];
                $namauser       = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$getidparent'")->row_array()['user_name'];
            }
            else
            {
                $namauser       = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['user_name'];
            }

            $durasi             = $getdataclass['duration_requested'];
            $hargaakhir         = ($durasi/900)*$getdataclass['harga_cm'];
            $hargakelas         = number_format($hargaakhir, 0, ".", ".");
            $server_utcc        = $this->Rumus->getGMTOffset();
            $intervall          = $user_utc - $server_utcc;
            $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$getdataclass['date_requested']);
            $tanggal->modify("+".$intervall ." minutes");
            $tanggal            = $tanggal->format('Y-m-d H:i:s');
            $datelimit          = date_create($tanggal);
            $dateelimit         = date_format($datelimit, 'd/m/y');
            $harilimit          = date_format($datelimit, 'd');
            $hari               = date_format($datelimit, 'l');
            $tahunlimit         = date_format($datelimit, 'Y');
            $waktulimit         = date_format($datelimit, 'H:i');   
            $datelimit          = $dateelimit;
            $sepparatorlimit    = '/';
            $partslimit         = explode($sepparatorlimit, $datelimit);
            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
            $seconds            = $getdataclass['duration_requested'];
            $hours              = floor($seconds / 3600);
            $mins               = floor($seconds / 60 % 60);
            $secs               = floor($seconds % 60);
            $durationrequested  = sprintf('%02d:%02d', $hours, $mins);

            //LOG DEMAND
            $myip = $_SERVER['REMOTE_ADDR'];
            $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$lastrequestid','student request private class','$iduserrequest','private','$myip','101')");
            //END LOG DEMAND

            if($res == 1){
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
                curl_setopt($ch, CURLOPT_POST, true);
                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_requestprivateKids','content' => array("email" => $this->session->userdata('email'), "namauser" => $namauser, "hargakelas" => $hargakelas, "getdataclass" => $getdataclass, "harilimit" => $harilimit, "hari" => $hari, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "nametutor" => $nametutor,  "durationrequested" => $durationrequested, "emailtutor" => $emailtutor  ) )));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;

                //TAMBAH KERANJANG                    
                $this->db->query("INSERT INTO tbl_keranjang (request_id,id_user,price) VALUES ('$lastrequestid','$iduserrequest','$hargaakhir')");
                //END KERANJANG
                ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
                $json_notif = "";
                $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$iduserrequest}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
                if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                    $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "30","title" => "Classmiles", "text" => "")));
                }
                if($json_notif != ""){
                    $headers = array(
                        'Authorization: key=' . FIREBASE_API_KEY,
                        'Content-Type: application/json'
                        );
                    // Open connection
                    $ch = curl_init();

                    // Set the url, number of POST vars, POST data
                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                    // Execute post
                    $result = curl_exec($ch);
                    curl_close($ch);
                    // echo $result;
                }
                ////------------ END -------------------------------------//
                
                //NOTIF STUDENT
                $notif_message = json_encode( array("code" => 114));                    
                $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$iduserrequest','','0')");
                $json = array("status" => 1,"message" => "Sukses");
                $load = $this->lang->line('successfullydemand');
                $this->session->set_flashdata('mes_alert','success');
                $this->session->set_flashdata('mes_display','block');
                $this->session->set_flashdata('mes_message',$load);
                echo json_encode($json);
                
            }else{
                //NOTIF STUDENT
                $notif_message = json_encode( array("code" => 115));                    
                $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$iduserrequest','','0')");
                $json = array("status" => -1,"message" => "Gagal");
                $load = $this->lang->line('faileddemand');
                $this->session->set_flashdata('mes_alert','danger');
                $this->session->set_flashdata('mes_display','block');
                $this->session->set_flashdata('mes_message',$load);
                echo json_encode($json);
            }
		}
	}

	public function demand_me_grup()
	{
		$subject_id = $this->input->get('subject_id');
		$tutor_id = $this->input->get('tutor_id');
		$start_time = $this->input->get('start_time');
		$date = $this->input->get('date');
		$user_utc = $this->input->get('user_utc');
        $harga = $this->input->get('harga');
        $harga_tutor = $this->input->get('harga_tutor');
		$id_friends = '';
		$metod = $this->input->get('metod');
		/*$jam = substr($start_time, 0,2)*3600;
		$menit = substr($start_time, 3,2)*60;
		$start_time = $jam + $menit;*/
		$duration = $this->input->get('duration');
		// $duration = $this->input->get('duration')*60;
        $daterequest = $date." ".$start_time.":00";
        $topic = $this->input->get('topic');

		if ($this->session_check() == 1) {
			$res = $this->Process_model->demand_me_grup($subject_id,$tutor_id,$start_time,$date,$duration,$id_friends,$metod,$user_utc,$topic,$harga,$harga_tutor);
            $lastrequestid  = $this->db->select('request_id')->order_by('request_id','desc')->limit(1)->get('tbl_request_grup')->row('request_id');
			// IF 1 = DEMAND SUCCESSFULLY REQUESTED
			// ELSE = DEMAND FAILURE
			if($res == 1){
                $getdataclassgroup  = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request_grup as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='group' AND tr.request_id='$lastrequestid'")->row_array(); 

                $hargaakhir         = number_format("0", 0, ".", ".");
                // $idemail            = explode(",", $id_friends);
                $toTutor            = 0;
                // $idemails = explode(",", $id_friends);  

                $id                 = $this->session->userdata('id_user');
                $iduserrequest      = $getdataclassgroup['id_user_requester'];
                $namauser           = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['user_name'];
                $getdatafriend      = $this->db->query("SELECT id_user,email,user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
                $server_utcc        = $this->Rumus->getGMTOffset();
                $user_utc           = $this->session->userdata('user_utc');
                $intervall          = $user_utc - $server_utcc;
                $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$getdataclassgroup['date_requested']);
                $tanggal->modify("+".$intervall ." minutes");
                $tanggal            = $tanggal->format('Y-m-d H:i:s');
                $datelimit          = date_create($tanggal);
                $dateelimit         = date_format($datelimit, 'd/m/y');
                $harilimit          = date_format($datelimit, 'd');
                $hari               = date_format($datelimit, 'l');
                $tahunlimit         = date_format($datelimit, 'Y');
                $waktulimit         = date_format($datelimit, 'H:i');   
                $datelimit          = $dateelimit;
                $sepparatorlimit    = '/';
                $partslimit         = explode($sepparatorlimit, $datelimit);
                $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
                $seconds            = $getdataclassgroup['duration_requested'];
                $hours              = floor($seconds / 3600);
                $mins               = floor($seconds / 60 % 60);
                $secs               = floor($seconds % 60);
                $durationrequested  = sprintf('%02d:%02d', $hours, $mins);
                
                $json_notif = "";
                $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$id}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
                if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                    $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "20","title" => "Classmiles", "text" => "")));
                }
                if($json_notif != ""){
                    $headers = array(
                        'Authorization: key=' . FIREBASE_API_KEY,
                        'Content-Type: application/json'
                        );
                    // Open connection
                    $ch = curl_init();

                    // Set the url, number of POST vars, POST data
                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                    // Execute post
                    $result = curl_exec($ch);
                    curl_close($ch);
                    // echo $result;
                }
                ////------------ END -------------------------------------//

                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
                curl_setopt($ch, CURLOPT_POST, true);
                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_requestgroup','content' => array("sender_email" => $this->session->userdata('email'), "getdataclassgroup" => $getdataclassgroup, "lastrequestid" => $lastrequestid, "harilimit" => $harilimit, "hari" => $hari, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "durationrequested" => $durationrequested, "namauser" => $namauser, "hargaakhir" => $hargaakhir ) )));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;
			    $json = array("status" => 1,"message" => "Sukses");
                $load = $this->lang->line('successfullydemand');
                $this->session->set_flashdata('mes_alert','success');
                $this->session->set_flashdata('mes_display','block');
                $this->session->set_flashdata('mes_message',$load);
			}else{
				$json = array("status" => -1,"message" => "Gagal");
				$load = $this->lang->line('faileddemand');
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message',$load);
			}
			echo json_encode($json);
		}
	}

    public function checkEmails()
    {
        $email_friends = $this->input->post('id_friends');
        $status = 0;
        $data = array();
        $emails = explode(",", $email_friends);
        foreach($emails as $key => $id) {            
            $getuserid  = $this->db->query("SELECT id_user FROM tbl_user WHERE email='$id' AND usertype_id='student' OR usertype_id = 'student kid'")->row_array();
            $myid = $this->session->userdata('id_user');
            if ($myid == $getuserid['id_user']) {
                $status = -2;
            }
            else
            {
                if (!empty($getuserid)) {
                    $data[]=$getuserid['id_user'];  
                    $status = 1;
                }
                else
                {
                    $status = -1;
                }
            }
        }
        $id_friends = implode(",",$data);
        if ($status == 1) {
            $json = array("status" => 1,"message" => "berhasil bro", "id_friends" => $id_friends, "statuz" => true);
        }
        else if ($status == -2) {
            $json = array("status" => -2,"message" => "Anda tidak dapat mengajak diri sendiri", "id_friends" => null, "statuz" => false);   
        }
        else
        {
            $json = array("status" => -1,"message" => "Maaf terdapat email yang tidak terdaftar di classmiles. silahkan ulangi kembali", "id_friends" => $id_friends, "statuz" => false);                        
        }
        echo json_encode($json);
    }

    public function confrimPaymentFlag()
    {
        $class_id = $this->input->post('class_id');
        $notes = $this->input->post('notes');
        $upd_flag = $this->db->query("UPDATE tbl_class_rating SET flag=1, notes = '$notes' WHERE class_id='$class_id'");
        if ($upd_flag) {
            $json = array("status" => 1,"message" => "Berhasil Konfrim", "statuz" => true);
        }        
        else
        {
            $json = array("status" => -1,"message" => "Terjadi Kesalahan", "statuz" => false);                        
        }
        echo json_encode($json);
    }

	public function demand_me_grup_kids()
	{
		$subject_id = $this->input->get('subject_id');
		$tutor_id = $this->input->get('tutor_id');
		$start_time = $this->input->get('start_time');
		$date = $this->input->get('date');
		$user_utc = $this->input->get('user_utc');
		$id_friends = $this->input->get('id_friends');
		$metod = $this->input->get('metod');
		$child_id = $this->input->get('child_id');
		/*$jam = substr($start_time, 0,2)*3600;
		$menit = substr($start_time, 3,2)*60;
		$start_time = $jam + $menit;*/
		$duration = $this->input->get('duration');
		// $duration = $this->input->get('duration')*60;

		if ($this->session_check() == 1) {
			$res = $this->Process_model->demand_me_grup_kids($subject_id,$tutor_id,$start_time,$date,$duration,$id_friends,$metod,$user_utc,$child_id);
			// IF 1 = DEMAND SUCCESSFULLY REQUESTED
			// ELSE = DEMAND FAILURE
			if($res == 1){
				$json = array("status" => 1,"message" => "Sukses");
				$load = $this->lang->line('successfullydemand');
				$this->session->set_flashdata('mes_alert','success');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message',$load);
			}else{
				$json = array("status" => -1,"message" => "Gagal");
				$load = $this->lang->line('faileddemand');
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message',$load);
			}
			echo json_encode($json);
		}
	}

	public function approve_demand()
	{
		$request_id = $this->input->get('request_id');
		$template = 'all_featured';
		$user_utc = $this->input->get('user_utc');
        $tutor_id =  $this->session->userdata('id_user');

		if ($this->session_check() == 1) {
			$res = $this->Process_model->approve_demand($request_id,$template,$user_utc,$tutor_id);

			// IF 1 = DEMAND APPROVED
			// ELSE = DEMAND REJECTED
            			
			if($res == 1){
				$getdataclass 	= $this->db->query("SELECT ms.subject_name, tr.*, tsp.*, tu.user_name, tu.email FROM tbl_request as tr LEFT JOIN tbl_service_price as tsp ON tr.subject_id=tsp.subject_id INNER JOIN tbl_user as tu ON tu.id_user=tr.tutor_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tr.request_id='$request_id' AND tsp.class_type='private' AND tsp.tutor_id='$tutor_id'")->row_array();

                $date_requested = DateTime::createFromFormat('Y-m-d H:i:s', $getdataclass['date_requested'] );
                $date_requested = $date_requested->format('Y-m-d H:i:s');

                $duration       = $getdataclass['duration_requested'];

                $fn_time = DateTime::createFromFormat('Y-m-d H:i:s', $date_requested);
                $fn_time->modify("+$duration second");
                $fn_time = $fn_time->format('Y-m-d H:i:s');
                
                // REJECT ALL PENDING REQUEST THAT HAS ARSIRAN TIME
                $to_reject = $this->db->query("SELECT * FROM tbl_request WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND approve=0 ")->result_array();

                foreach ($to_reject as $key => $value) {
                    $rq_id = $value['request_id'];
                    $this->db->simple_query("UPDATE tbl_request SET approve=-2 WHERE request_id='$rq_id'");
                    $this->Process_model->reject_request($rq_id, 'private', 'unavailable');
                }
                $to_reject_group = $this->db->query("SELECT * FROM tbl_request_grup WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND approve=0 ")->result_array();

                foreach ($to_reject_group as $key => $value) {
                    $rq_id = $value['request_id'];
                    $this->db->simple_query("UPDATE tbl_request_grup SET approve=-2 WHERE request_id='$rq_id'");
                    $this->Process_model->reject_request($rq_id, 'group', 'unavailable');
                }

				$iduserrequest 		= $getdataclass['id_user_requester'];
				$usertype_id 		= $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['usertype_id'];
                if ($usertype_id == 'student kid') {
                    $getidparent    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$iduserrequest'")->row_array()['parent_id'];
                    $forFcm_id_requester= $getidparent;
                    $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getidparent'")->row_array();
                }
                else
                {
                    $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
                    $forFcm_id_requester= $iduserrequest;
                }

                
                /*if (empty($datauser['email'])) {
                    $getparentid    = $this->db->query("SELECT * FROM tbl_profile_kid WHERE kid_id='$iduserrequest'")->row_array();
                    $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getparentid[parent_id]'")->row_array();
                }*/
    //             $hargahitung        = ($duration/900)*$getdataclass['harga_cm'];
				// $hargakelas         = number_format($hargahitung, 0, ".", ".");
	            $server_utcc        = $this->Rumus->getGMTOffset();
	            $intervall          = $user_utc - $server_utcc;
	            $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$getdataclass['date_requested']);
	            $tanggal->modify("+".$intervall ." minutes");
	            $tanggal            = $tanggal->format('Y-m-d H:i:s');
	            $datelimit          = date_create($tanggal);
	            $dateelimit         = date_format($datelimit, 'd/m/y');
	            $harilimit          = date_format($datelimit, 'd');
	            $hari               = date_format($datelimit, 'l');
	            $tahunlimit         = date_format($datelimit, 'Y');
	            $waktulimit         = date_format($datelimit, 'H:i');   
	            $datelimit          = $dateelimit;
	            $sepparatorlimit    = '/';
	            $partslimit         = explode($sepparatorlimit, $datelimit);
	            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
	            $seconds 			= $getdataclass['duration_requested'];
			    $hours 				= floor($seconds / 3600);
			    $mins 				= floor($seconds / 60 % 60);
			    $secs 				= floor($seconds % 60);
			    $durationrequested 	= sprintf('%02d:%02d', $hours, $mins);

                //LOG DEMAND
                $myip               = $_SERVER['REMOTE_ADDR'];
                $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$request_id','tutor approve private class','$iduserrequest','private','$myip','111')");
                //END LOG DEMAND

				// Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
                curl_setopt($ch, CURLOPT_POST, true);
                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_approve_demand','content' => array("datauser" => $datauser, "getdataclass" => $getdataclass, "hari" => $hari, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "durationrequested" => $durationrequested) )));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;

                $json_notif = "";
                $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$forFcm_id_requester}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
                if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                    $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "30","title" => "Classmiles", "getdataclass" => $getdataclass)));
                }
                if($json_notif != ""){
                    $headers = array(
                        'Authorization: key=' . FIREBASE_API_KEY,
                        'Content-Type: application/json'
                        );
                    // Open connection
                    $ch = curl_init();

                    // Set the url, number of POST vars, POST data
                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                    // Execute post
                    $result = curl_exec($ch);
                    curl_close($ch);
                    // echo $result;
                }


                if ($result) {
                    //MASUK KE INVOICE
                    $iptInvoice = $this->Process_model->createInvoice($request_id,'private');

                    if ($iptInvoice == 1) {                                            
                        $load = $this->lang->line('approvalcomplete');
                        $json = array("status" => 1,"message" => $load);

                        $this->session->set_flashdata('mes_alert','success');
                        $this->session->set_flashdata('mes_display','block');
                        $this->session->set_flashdata('mes_message',$load);

                        $getdataadmin  = $this->db->query("SELECT * FROM tbl_user WHERE usertype_id='admin'")->result_array();
                        foreach ($getdataadmin as $key => $v) {
                            $idadmin = $v['id_user'];
                            $namaadmin = $v['user_name'];
                            $notif_message = json_encode( array("code" => 117, "tutor_name" => $namaadmin) );                        
                            $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$idadmin','https://classmiles.com/admin/PaymentPrivate','0')");
                        }
                    } else{
                        $load = $this->lang->line('approvalfailed');
                        $json = array("status" => -3,"message" => 'Gagal bikin invoice', "data" => $iptInvoice);

                        $this->session->set_flashdata('mes_alert','danger');
                        $this->session->set_flashdata('mes_display','block');
                        $this->session->set_flashdata('mes_message',$load); 
                    }                   
                }else{
                    $load = $this->lang->line('approvalfailed');
                    $json = array("status" => -2,"message" => 'gagal kirim email', "data" => $datauser);

                    $this->session->set_flashdata('mes_alert','danger');
                    $this->session->set_flashdata('mes_display','block');
                    $this->session->set_flashdata('mes_message',$load); 
                }
			}else{
				$load = $this->lang->line('approvalfailed');
				$json = array("status" => -1,"message" => $load);

				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message',$load);
			}
			echo json_encode($json);
		}
	}

	public function approve_demand_group()
    {
        $request_id = $this->input->get('request_id');
        $template = 'all_featured';
        $user_utc = $this->input->get('user_utc');
        $tutorid =   $this->session->userdata('id_user');

        if ($this->session_check() == 1) {
            $res = $this->Process_model->approve_demand_group($request_id,$template,$user_utc, $tutorid);
            
            if($res == 1){
                $getdataclass   = $this->db->query("SELECT ms.subject_name, trg.*, tsp.*, tu.user_name, tu.email FROM tbl_request_grup as trg INNER JOIN tbl_service_price as tsp ON trg.subject_id=tsp.subject_id INNER JOIN tbl_user as tu ON tu.id_user=trg.tutor_id INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id WHERE trg.request_id='$request_id' AND tsp.class_type='group' AND tsp.tutor_id='$tutorid'")->row_array();

                $date_requested = DateTime::createFromFormat('Y-m-d H:i:s', $getdataclass['date_requested'] );
                $date_requested = $date_requested->format('Y-m-d H:i:s');

                $duration       = $getdataclass['duration_requested'];

                $fn_time = DateTime::createFromFormat('Y-m-d H:i:s', $date_requested);
                $fn_time->modify("+$duration second");
                $fn_time = $fn_time->format('Y-m-d H:i:s');

                
                // REJECT ALL PENDING REQUEST THAT HAS ARSIRAN TIME
                $to_reject = $this->db->query("SELECT * FROM tbl_request WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND approve=0 ")->result_array();

                foreach ($to_reject as $key => $value) {
                    $rq_id = $value['request_id'];
                    $this->db->simple_query("UPDATE tbl_request SET approve=-2 WHERE request_id='$rq_id'");
                    $this->Process_model->reject_request($rq_id, 'private', 'unavailable');
                }
                $to_reject_group = $this->db->query("SELECT * FROM tbl_request_grup WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND approve=0 ")->result_array();

                foreach ($to_reject_group as $key => $value) {
                    $rq_id = $value['request_id'];
                    $this->db->simple_query("UPDATE tbl_request_grup SET approve=-2 WHERE request_id='$rq_id'");
                    $this->Process_model->reject_request($rq_id, 'group', 'unavailable');
                }

                $iduserrequest      = $getdataclass['id_user_requester'];
                $usertype_id        = $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['usertype_id'];
                if ($usertype_id == 'student kid') {
                    $getidparent    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$iduserrequest'")->row_array()['parent_id'];
                    $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getidparent'")->row_array();
                    $forFcm_id_requester= $getidparent;
                }
                else
                {
                    $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
                    $forFcm_id_requester= $iduserrequest;
                }
                $harga              = $this->db->query("SELECT price FROM tbl_request_grup WHERE request_id = '$request_id'")->row_array()['price'];
                $hargakelas         = number_format($harga, 0, ".", ".");
                $server_utcc        = $this->Rumus->getGMTOffset();
                $intervall          = $user_utc - $server_utcc;
                $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$getdataclass['date_requested']);
                $tanggal->modify("+".$intervall ." minutes");
                $tanggal            = $tanggal->format('Y-m-d H:i:s');
                $datelimit          = date_create($tanggal);
                $dateelimit         = date_format($datelimit, 'd/m/y');
                $harilimit          = date_format($datelimit, 'd');
                $hari               = date_format($datelimit, 'l');
                $tahunlimit         = date_format($datelimit, 'Y');
                $waktulimit         = date_format($datelimit, 'H:i');   
                $datelimit          = $dateelimit;
                $sepparatorlimit    = '/';
                $partslimit         = explode($sepparatorlimit, $datelimit);
                $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
                $seconds            = $getdataclass['duration_requested'];
                $hours              = floor($seconds / 3600);
                $mins               = floor($seconds / 60 % 60);
                $secs               = floor($seconds % 60);
                $durationrequested  = sprintf('%02d:%02d', $hours, $mins);

                //LOG DEMAND
                $myip               = $_SERVER['REMOTE_ADDR'];
                $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$request_id','tutor approve group class','$tutorid','group','$myip','112')");
                //END LOG DEMAND

                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
                curl_setopt($ch, CURLOPT_POST, true);
                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_approve_demand_group','content' => array("datauser" => $datauser, "getdataclass" => $getdataclass, "hari" => $hari, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "durationrequested" => $durationrequested, "hargakelas" => $hargakelas ) )));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;

                $json_notif = "";
                $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$forFcm_id_requester}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
                if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                    $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "30","title" => "Classmiles", "getdataclass" => $getdataclass)));
                }
                if($json_notif != ""){
                    $headers = array(
                        'Authorization: key=' . FIREBASE_API_KEY,
                        'Content-Type: application/json'
                        );
                    // Open connection
                    $ch = curl_init();

                    // Set the url, number of POST vars, POST data
                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                    // Execute post
                    $result = curl_exec($ch);
                    curl_close($ch);
                    // echo $result;
                }


                if ($result) {

                    $iptInvoice = $this->Process_model->createInvoice($request_id,'group');

                    if ($iptInvoice == 1) {
                        $load = $this->lang->line('approvalcomplete');
                        $json = array("status" => 1,"message" => $load);

                        $this->session->set_flashdata('mes_alert','success');
                        $this->session->set_flashdata('mes_display','block');
                        $this->session->set_flashdata('mes_message',$load);
                        //NOTIF TUTOR
                        $notif_message = json_encode( array("code" => 116, "namauser" => $datauser['user_name']));                    
                        $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$tutorid','','0')");

                        $getdataadmin  = $this->db->query("SELECT * FROM tbl_user WHERE usertype_id='admin'")->result_array();
                        foreach ($getdataadmin as $key => $v) {
                            $idadmin = $v['id_user'];
                            $namaadmin = $v['user_name'];
                            $notif_message = json_encode( array("code" => 118, "tutor_name" => $namaadmin) );                        
                            $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$idadmin','https://classmiles.com/admin/PaymentGroup','0')");
                        } 
                    }
                    else{
                        $load = $this->lang->line('approvalfailed');
                        $json = array("status" => -2,"message" => 'gagal kirim email', "data" => $datauser);
                        
                        $this->session->set_flashdata('mes_alert','danger');
                        $this->session->set_flashdata('mes_display','block');
                        $this->session->set_flashdata('mes_message',$load);
                    }   
                    
                }else{
                    $load = $this->lang->line('approvalfailed');
                    $json = array("status" => -1,"message" => $load);

                    $this->session->set_flashdata('mes_alert','danger');
                    $this->session->set_flashdata('mes_display','block');
                    $this->session->set_flashdata('mes_message',$load);  
                }
            }else{
                $load = $this->lang->line('approvalfailed');
                $json = array("status" => -1,"message" => $load);

                $this->session->set_flashdata('mes_alert','danger');
                $this->session->set_flashdata('mes_display','block');
                $this->session->set_flashdata('mes_message',$load);
            }
            echo json_encode($json);
        }
    }

	public function reject_demand()
	{
		$request_id = $this->input->get('request_id');
        $nametutor  = $this->session->userdata('user_name');
		$reject = $this->Process_model->reject_demand($request_id);
        $user_utc = $this->input->get('user_utc');

        $tutorid =  $this->session->userdata('id_user');
		if ($reject == 1) {
            $getdataclass   = $this->db->query("SELECT ms.subject_name, tr.*, tsp.*, tu.user_name, tu.email FROM tbl_request as tr INNER JOIN tbl_service_price as tsp ON tr.subject_id=tsp.subject_id INNER JOIN tbl_user as tu ON tu.id_user=tr.tutor_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tr.request_id='$request_id' AND tsp.class_type='private' AND tsp.tutor_id='$tutorid'")->row_array();
            $iduserrequest      = $getdataclass['id_user_requester'];
            $duration       = $getdataclass['duration_requested'];
            $usertype_id        = $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['usertype_id'];
            if ($usertype_id == 'student kid') {
                $getidparent    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$iduserrequest'")->row_array()['parent_id'];
                $forFcm_id_requester= $getidparent;
                $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getidparent'")->row_array();
            }
            else
            {
                $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
                $forFcm_id_requester= $iduserrequest;
            }
            $krnj_id            = $this->db->query("SELECT krnj_id FROM tbl_keranjang where request_id='$request_id' OR request_grup_id='$request_id' OR request_multicast_id='$request_id'")->row_array()['krnj_id'];
            $this->db->query("DELETE FROM tbl_keranjang WHERE krnj_id='$krnj_id'");
            $hargahitung        = ($duration/900)*$getdataclass['harga_cm'];
            $hargakelas         = number_format($hargahitung, 0, ".", ".");
            $server_utcc        = $this->Rumus->getGMTOffset();
            $intervall          = $user_utc - $server_utcc;
            $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$getdataclass['date_requested']);
            $tanggal->modify("+".$intervall ." minutes");
            $tanggal            = $tanggal->format('Y-m-d H:i:s');
            $datelimit          = date_create($tanggal);
            $dateelimit         = date_format($datelimit, 'd/m/y');
            $harilimit          = date_format($datelimit, 'd');
            $hari               = date_format($datelimit, 'l');
            $tahunlimit         = date_format($datelimit, 'Y');
            $waktulimit         = date_format($datelimit, 'H:i');   
            $datelimit          = $dateelimit;
            $sepparatorlimit    = '/';
            $partslimit         = explode($sepparatorlimit, $datelimit);
            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
            $seconds            = $getdataclass['duration_requested'];
            $hours              = floor($seconds / 3600);
            $mins               = floor($seconds / 60 % 60);
            $secs               = floor($seconds % 60);
            $durationrequested  = sprintf('%02d:%02d', $hours, $mins);

            ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
            $json_notif = "";
            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$forFcm_id_requester}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "31","title" => "Classmiles", "text" => "")));
            }
            if($json_notif != ""){
                $headers = array(
                    'Authorization: key=' . FIREBASE_API_KEY,
                    'Content-Type: application/json'
                    );
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;
            }
            ////------------ END -------------------------------------//

            //LOG DEMAND
            $myip               = $_SERVER['REMOTE_ADDR'];
            $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$request_id','tutor reject private class','$iduserrequest','private','$myip','121')");
            //END LOG DEMAND

            // Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
            curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_reject_demand','content' => array("datauser" => $datauser, "getdataclass" => $getdataclass, "hari" => $hari, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "durationrequested" => $durationrequested) )));

            // Execute post
            $result = curl_exec($ch);
            curl_close($ch);
            // echo $result;

            //NOTIF STUDENT
            $notif_message = json_encode( array("code" => 119, "tutor_name" => $nametutor));                    
            $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$iduserrequest','https://classmiles.com/Ondemand','0')");
            $json = array("status" => 1,"message" => 'Berhasil menolak kelas private tambahan.'); 

                
		}
		else
		{
			$json = array("status" => 0,"message" => 'Gagal menolak kelas private tambahan. Mohon lakukan kembali.');
		}
		echo json_encode($json);
	}

	public function reject_demand_group()
	{
		$request_id = $this->input->get('request_id');
        $id_user    = $this->session->userdata('id_user');
        $nametutor  = $this->session->userdata('user_name');
        $tutorid =  $this->session->userdata('id_user');
		$reject = $this->Process_model->reject_demand_group($request_id);

		if ($reject == 1) {
            $getdataclass   = $this->db->query("SELECT ms.subject_name, tr.*, tsp.*, tu.user_name, tu.email FROM tbl_request_grup as tr INNER JOIN tbl_service_price as tsp ON tr.subject_id=tsp.subject_id INNER JOIN tbl_user as tu ON tu.id_user=tr.tutor_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tr.request_id='$request_id' AND tsp.class_type='group' AND tsp.tutor_id='$tutorid'")->row_array();
            $iduserrequest      = $getdataclass['id_user_requester'];
            $usertype_id        = $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['usertype_id'];
            if ($usertype_id == 'student kid') {
                $getidparent    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$iduserrequest'")->row_array()['parent_id'];
                $forFcm_id_requester= $getidparent;
                $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getidparent'")->row_array();
            }
            else
            {
                $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
                $forFcm_id_requester= $iduserrequest;
            }
            $krnj_id            = $this->db->query("SELECT krnj_id FROM tbl_keranjang where request_id='$request_id' OR request_grup_id='$request_id' OR request_multicast_id='$request_id'")->row_array()['krnj_id'];
            $this->db->query("DELETE FROM tbl_keranjang WHERE krnj_id='$krnj_id'");

            ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
            $json_notif = "";
            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$forFcm_id_requester}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "31","title" => "Classmiles", "text" => "")));
            }
            if($json_notif != ""){
                $headers = array(
                    'Authorization: key=' . FIREBASE_API_KEY,
                    'Content-Type: application/json'
                    );
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                
            }
            ////------------ END -------------------------------------//

            //LOG DEMAND
            $myip               = $_SERVER['REMOTE_ADDR'];
            $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$request_id','tutor reject group class','$iduserrequest','group','$myip','122')");
            //END LOG DEMAND

            

            $ALL_data_friends = array();
            $id_friends = json_decode($getdataclass['id_friends'], TRUE);

            foreach($id_friends['id_friends'] as $listfriends) {

                $userid_friends = $listfriends['id_user'];
                $data_friends  = $this->db->query("SELECT email,user_name FROM tbl_user WHERE id_user='$userid_friends'")->row_array();  
                $ALL_data_friends[] = $data_friends;
            }

            // Open connection
                $ch = curl_init();
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
            curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_reject_demand_group','content' => array("datauser" => $datauser, "getdata_friends" => $ALL_data_friends) )));

            // Execute post
            $result = curl_exec($ch);
            curl_close($ch);
            // echo $result;
            $json = array("status" => 1,"message" => 'Berhasil menolak kelas group tambahan.'); 
		}
		else
		{
			$json = array("status" => 0,"message" => 'Gagal menolak kelas group tambahan. Mohon lakukan kembali.');
		}
        
		echo json_encode($json);
	}
	
	public function reject_demand_android()
	{
		$request_id = $this->input->get('request_id'); 
        $tutorid =  $this->db->query("SELECT tutor_id FROM tbl_request WHERE request_id='$request_id'")->row_array()['tutor_id'];       
		$reject = $this->Process_model->reject_demand($request_id);            

		if ($reject == 1) {
            $getdataclass   = $this->db->query("SELECT ms.subject_name, tr.*, tsp.*, tu.user_name, tu.email FROM tbl_request as tr INNER JOIN tbl_service_price as tsp ON tr.subject_id=tsp.subject_id INNER JOIN tbl_user as tu ON tu.id_user=tr.tutor_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tr.request_id='$request_id' AND tsp.class_type='private' AND tsp.tutor_id='$tutorid'")->row_array();
            $iduserrequest      = $getdataclass['id_user_requester'];
            $usertype_id        = $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['usertype_id'];
            if ($usertype_id == 'student kid') {
                $getidparent    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$iduserrequest'")->row_array()['parent_id'];
                $forFcm_id_requester= $getidparent;
                $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getidparent'")->row_array();
            }
            else
            {
                $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
                $forFcm_id_requester= $iduserrequest;
            }
            $krnj_id            = $this->db->query("SELECT krnj_id FROM tbl_keranjang where request_id='$request_id' OR request_grup_id='$request_id' OR request_multicast_id='$request_id'")->row_array()['krnj_id'];
            $this->db->query("DELETE FROM tbl_keranjang WHERE krnj_id='$krnj_id'");

            //LOG DEMAND
            $myip               = $_SERVER['REMOTE_ADDR'];
            $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$request_id','tutor reject private class by android','$iduserrequest','private','$myip','123')");
            //END LOG DEMAND

            ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
            $json_notif = "";
            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$forFcm_id_requester}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "31","title" => "Classmiles", "text" => "")));
            }
            if($json_notif != ""){
                $headers = array(
                    'Authorization: key=' . FIREBASE_API_KEY,
                    'Content-Type: application/json'
                    );
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;
            }
            ////------------ END -------------------------------------//
            
            // Open connection
            $ch = curl_init();
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
            curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_reject_demand','content' => array("datauser" => $datauser) )));

            // Execute post
            $result = curl_exec($ch);
            curl_close($ch);
            // echo $result;

			$json = array("status" => true,"message" => 'Berhasil menolak kelas private tambahan.');
		}
		else
		{
			$json = array("status" => false,"message" => 'Gagal menolak kelas private tambahan. Mohon lakukan kembali.');
		}
		echo json_encode($json);
	}

	public function reject_demand_group_android()
	{
		$request_id = $this->input->get('request_id');
        $tutorid =  $this->db->query("SELECT tutor_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['tutor_id'];
		$reject = $this->Process_model->reject_demand_group($request_id);        

		if ($reject == 1) {
            $getdataclass   = $this->db->query("SELECT ms.subject_name, tr.*, tsp.*, tu.user_name, tu.email FROM tbl_request_grup as tr INNER JOIN tbl_service_price as tsp ON tr.subject_id=tsp.subject_id INNER JOIN tbl_user as tu ON tu.id_user=tr.tutor_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tr.request_id='$request_id' AND tsp.class_type='group' AND tsp.tutor_id='$tutorid'")->row_array();
            $iduserrequest      = $getdataclass['id_user_requester'];
            $usertype_id        = $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['usertype_id'];
            if ($usertype_id == 'student kid') {
                $getidparent    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$iduserrequest'")->row_array()['parent_id'];
                $forFcm_id_requester= $getidparent;
                $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getidparent'")->row_array();
            }
            else
            {
                $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
                $forFcm_id_requester= $iduserrequest;
            }
            $krnj_id            = $this->db->query("SELECT krnj_id FROM tbl_keranjang where request_id='$request_id' OR request_grup_id='$request_id' OR request_multicast_id='$request_id'")->row_array()['krnj_id'];
            $this->db->query("DELETE FROM tbl_keranjang WHERE krnj_id='$krnj_id'");

            //LOG DEMAND
            $myip               = $_SERVER['REMOTE_ADDR'];
            $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$request_id','tutor reject group class by android','$iduserrequest','group','$myip','124')");
            //END LOG DEMAND

            ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
            $json_notif = "";
            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$forFcm_id_requester}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "31","title" => "Classmiles", "text" => "")));
            }
            if($json_notif != ""){
                $headers = array(
                    'Authorization: key=' . FIREBASE_API_KEY,
                    'Content-Type: application/json'
                    );
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;
            }
            ////------------ END -------------------------------------//
            $ALL_data_friends = array();
            $id_friends = json_decode($getdataclass['id_friends'], TRUE);

            foreach($id_friends['id_friends'] as $listfriends) {

                $userid_friends = $listfriends['id_user'];
                $data_friends  = $this->db->query("SELECT email,user_name FROM tbl_user WHERE id_user='$userid_friends'")->row_array();  
                $ALL_data_friends[] = $data_friends;
            }

            // Open connection
                $ch = curl_init();
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
            curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_reject_demand_group','content' => array("datauser" => $datauser, "getdata_friends" => $ALL_data_friends) )));

            // Execute post
            $result = curl_exec($ch);
            curl_close($ch);
            // echo $result;

			$json = array("status" => true,"message" => 'Berhasil menolak kelas group tambahan.');
		}
		else
		{
			$json = array("status" => false,"message" => 'Gagal menolak kelas group tambahan. Mohon lakukan kembali.');
		}
		echo json_encode($json);
	}

	public function save_subd()
	{
		$tutor_id = $this->session->userdata('id_user');
		$subject_id = htmlentities($this->input->get('subject_id'));
		$subdiscussion = htmlentities($this->input->post('subdiscussion'));
		$fdate = $this->input->get('fdate');

		$checker = $this->db->query("SELECT * FROM tbl_user WHERE id_user='{$tutor_id}'")->row_array();

		if(empty($checker)){
			$data['status'] = 0;
			$data['message'] = 'Invalid Data!';

			echo json_encode($data);
			return null;
		}

		$data = $this->db->query("SELECT * FROM log_subdiscussion WHERE subject_id='{$subject_id}' AND tutor_id='{$tutor_id}' AND fdate_week='{$fdate}'")->row_array();
		if(!empty($data)){
			$sid = $data['subdiscussion_id'];
			$this->db->simple_query("UPDATE log_subdiscussion SET subdiscussion='".$subdiscussion."' WHERE subdiscussion_id='{$sid}'");
		}else{
			$this->db->simple_query("INSERT INTO log_subdiscussion(subject_id, tutor_id, fdate_week, subdiscussion) VALUES('{$subject_id}','{$tutor_id}','{$fdate}','".$subdiscussion."')");
		}
		$load = $this->lang->line('saved');
		$data['status'] = 1;
		$data['message'] = $load;

		echo json_encode($data);

	}
	public function save_lavtime()
	{
		$tutor_id = $this->session->userdata('id_user');
		$data = json_decode($this->input->post('data'),true);
		// $all_date = $this->input->post('all_date');
		// $all_time = $this->input->post('all_time');

		foreach ($data as $key => $value) {
			$dt = substr($value['start_date'], 0,10);
			$t1 = substr($value['start_date'], 11,5);
			$t1 = (intval(substr($t1, 0,2))*3600)+(intval(substr($t1, 3,2))*60);
			$t2 = substr($value['end_date'], 11,5);
			$t2 = (intval(substr($t2, 0,2))*3600)+(intval(substr($t2, 3,2))*60);
			$lavtime_id = $value['id'];

			// AND date='{$dt}' AND ( (start_time >= '{$t1}' AND start_time < '{$t2}') || (stop_time > '{$t1}' && stop_time <= '{$t2}') )
			$hasil = $this->db->query("SELECT * FROM log_avtime WHERE tutor_id='{$tutor_id}' AND lavtime_id='{$lavtime_id}' ")->row_array();
			if(!empty($hasil)){
				$ids = $hasil['lavtime_id'];
				$this->db->simple_query("UPDATE log_avtime SET start_time='{$t1}', stop_time='{$t2}', date='{$dt}' WHERE lavtime_id='{$ids}'");
			}else{
				$this->db->simple_query("INSERT INTO log_avtime(tutor_id, date, start_time,stop_time) VALUES('{$tutor_id}','{$dt}','{$t1}','{$t2}')");
			}
		}
		
		$load = $this->lang->line('saved');
		$res['status'] = 1;
		$res['message'] = $load;

		echo json_encode($res);

	}
	public function delete_lavtime()
	{
		$lavtime_id = htmlentities($this->input->get('lavtime_id'));
		$tutor_id = $this->session->userdata('id_user');


		$this->db->simple_query("DELETE FROM log_avtime WHERE lavtime_id='{$lavtime_id}' AND tutor_id='{$tutor_id}'");

		$load = $this->lang->line('sukses_delete');
		$res['status'] = 1;
		// $res['message'] = "DELETE FROM log_avtime WHERE lavtime_id='{$lavtime_id}' AND tutor_id='{$tutor_id}'";
		$res['message'] = $load;
		// $res['message'] = $load;
		echo json_encode($res);
	}

	public function choose()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "choosingsubjecttutor";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/choose', $data);
			return null;
		}
		redirect('/');
	}
	
	public function about()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "profiletutor";
			$data['profile'] = $this->Master_model->getTblUserAndProfileTutor();
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/about', $data);
			return null;
		}
		redirect('/');
	}

	public function profile()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "profiletutor";
			$data['alluserprofile'] = $this->Master_model->getTblUserAndProfileTutor();
			// print_r('<pre>');
			// print_r($data['alluserprofile']);
			// print_r('</pre>');
			// return null;
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/profile', $data);
			return null;
		}
		redirect('/');
	}

	public function profile_account()
	{
		if ($this->session_check() == 1) {
			if($this->session->flashdata('rets')){
				$data['rets'] = $this->session->flashdata('rets');
			}
			$data['sideactive'] = "profiletutor";
			$data['provinsi']=$this->Master_model->provinsi();
			$data['kabupaten']=$this->Master_model->getAllKabupaten();
			$data['kecamatan']=$this->Master_model->getAllKecamatan();
			$data['kelurahan']=$this->Master_model->getAllKelurahan();
			$data['levels']=$this->Master_model->jenjangname();
			$data['alldata'] = $this->Master_model->getTblUserAndProfileTutor();
			$this->load->view(MOBILE.'inc/header');

			$this->load->view(MOBILE.'tutor/profile_account', $data);
			return null;
		}
		redirect('/');
	}

	public function profile_forgot()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "profiletutor";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/profile_forgot', $data);
			return null;
		}
		redirect('/');
	}

	public function Approval()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "profileapproval";
			$data['provinsi']=$this->Master_model->provinsi();
			$data['levels']=$this->Master_model->jenjangnamelevel();
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/approval', $data);
			return null;
		}
		redirect('/');
	}

	public function Description()
	{
		$data['sideactive'] = "profileapproval";
		$data['profile'] = $this->Master_model->getTblUserAndProfileTutor();
		// $data['alluserprofile'] = $this->Master_model->getTblUserAndProfileTutor();
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'tutor/profile_desc', $data);
		return null;
	}

	function ambil_data(){

		$modul=$this->input->post('modul');
		$id=$this->input->post('id');

		if($modul=="kabupaten"){
			echo $this->Master_model->kabupaten($id);
		}
		else if($modul=="kecamatan"){
			echo $this->Master_model->kecamatan($id);
		}
		else if($modul=="kelurahan"){
			echo $this->Master_model->kelurahan($id);
		}
	}

	public function load_lang($lang = '')
	{
		$this->lang->load('umum',$lang);
	}

	public function session_check()
	{
		if($this->session->has_userdata('id_user')){
			$new_state = $this->db->query("SELECT status FROM tbl_user where id_user='".$this->session->userdata('id_user')."'")->row_array()['status'];
			if($this->session->userdata('status') != $new_state ){
				$this->session->sess_destroy();
				redirect('/login');
			}
			// $this->sesi['user_name'] = $this->session->userdata('user_name');
			// $this->sesi['username'] = $this->session->userdata('username');
			// $this->sesi['priv_level'] = $this->session->userdata('priv_level');
			return 1;
		}else{
			return 0;
		}
	}
	public function saveEditProfileTutor(){
		$id_user = $this->session->userdata('id_user');

		if($this->input->post()){
			$form_profile = json_decode($this->input->post('form_profile'),true);
			$self_description = $this->input->post('self_description');


			$edu_array = array();
			if(is_array($form_profile['educational_level'])){
				foreach ($form_profile['educational_level'] as $key => $value) {
					$edu_array[$key]['educational_level'] = $form_profile['educational_level'][$key];
					$edu_array[$key]['educational_competence'] = $form_profile['educational_competence'][$key];
					$edu_array[$key]['educational_institution'] = $form_profile['educational_institution'][$key];
					$edu_array[$key]['institution_address'] = $form_profile['institution_address'][$key];
					$edu_array[$key]['graduation_year'] = $form_profile['graduation_year'][$key];
				}
			}else{
				$edu_array[0]['educational_level'] = $form_profile['educational_level'];
				$edu_array[0]['educational_competence'] = $form_profile['educational_competence'];
				$edu_array[0]['educational_institution'] = $form_profile['educational_institution'];
				$edu_array[0]['institution_address'] = $form_profile['institution_address'];
				$edu_array[0]['graduation_year'] = $form_profile['graduation_year'];
			}
		// print_r($edu_array);

			$exp_array = array();
			if(is_array($form_profile['description_experience'])){
				foreach ($form_profile['description_experience'] as $key => $value) {
					$exp_array[$key]['description_experience'] = $form_profile['description_experience'][$key];
					$exp_array[$key]['year_experience1'] = $form_profile['year_experience1'][$key];
					$exp_array[$key]['year_experience2'] = $form_profile['year_experience2'][$key];
					$exp_array[$key]['place_experience'] = $form_profile['place_experience'][$key];
					$exp_array[$key]['information_experience'] = $form_profile['information_experience'][$key];
				}
			}else{
				$exp_array[0]['description_experience'] = $form_profile['description_experience'];
				$exp_array[0]['year_experience1'] = $form_profile['year_experience1'];
				$exp_array[0]['year_experience2'] = $form_profile['year_experience2'];
				$exp_array[0]['place_experience'] = $form_profile['place_experience'];
				$exp_array[0]['information_experience'] = $form_profile['information_experience'];
			}
		// print_r($exp_array);

			$edu_array = serialize($edu_array);
			$exp_array = serialize($exp_array);

			$check_pexist = $this->db->query("SELECT * FROM tbl_profile_tutor WHERE tutor_id='".$id_user."'")->row_array();
			if(!empty($check_pexist)){
				$this->db->simple_query("UPDATE tbl_profile_tutor SET education_background='$edu_array', teaching_experience='$exp_array', self_description='{$self_description}' WHERE tutor_id='".$id_user."'");
			}else{
				$this->db->simple_query("INSERT INTO tbl_profile_tutor(tutor_id,education_background,teaching_experience,self_description) VALUES('$id_user','$edu_array','$exp_array','{$self_description}')");
			}

			$load = $this->lang->line('successfullyprofile');
			$rets['status'] = 1;
			$rets['message'] = $load;
			echo json_encode($rets);
			return null;
		}
		$load = $this->lang->line('erroroccured');
		$rets['status'] = 0;
		$rets['message'] = $load;
		echo json_encode($rets);
		return null;
	}
	// PROSES VERITRANS //
	public function token()
    {
    	$orderrand =  $this->Rumus->gen_order_id();

    	$amount = $this->input->post('amount');

		// Required
		$transaction_details = array(
		  'order_id' => $orderrand,
		  'gross_amount' => $amount,		  
		);

		// Optional
		$customer_details = array(
		  'first_name'    => $this->session->userdata('user_name'),
		  'email'         => $this->session->userdata('email'),
		  'phone'         => $this->session->userdata('no_hp')
		  // 'billing_address'  => $billing_address,
		  // 'shipping_address' => $shipping_address
		);

		
		// $creditcard = array(
		// 	'secure' => true,
		// 	"save_card" => true
		// );

		// Fill transaction details
		$transaction = array(			
		  	'transaction_details' => $transaction_details,		  	
		  	'customer_details' => $customer_details,		  	
		  	'enabled_payments' => array('credit_card')
		);
		//error_log(json_encode($transaction));
		$snapToken = $this->midtrans->getSnapToken($transaction);
		error_log($snapToken);
		echo $snapToken;
    }

    public function mandiri()
    {
    	$orderrand =  $this->Rumus->gen_order_id();

    	$amount = $this->input->post('amount');

		// Required
		$transaction_details = array(
		  'order_id' => $orderrand,
		  'gross_amount' => $amount,		  
		);

		// Optional
		$customer_details = array(
		  'first_name'    => $this->session->userdata('user_name'),
		  'email'         => $this->session->userdata('email'),
		  'phone'         => $this->session->userdata('no_hp')
		);

		
		// $creditcard = array(
		// 	'secure' => true,
		// 	"save_card" => true
		// );

		// Fill transaction details
		$transaction = array(			
		  	'transaction_details' => $transaction_details,		  	
		  	'customer_details' => $customer_details,		  	
		  	'enabled_payments' => array('mandiri_clickpay')
		);
		//error_log(json_encode($transaction));
		$snapToken = $this->midtrans->getSnapToken($transaction);
		error_log($snapToken);
		echo $snapToken;
    }

    public function permata()
    {
    	$orderrand =  $this->Rumus->gen_order_id();

    	$amount = $this->input->post('amount');

		// Required
		$transaction_details = array(
		  'order_id' => $orderrand,
		  'gross_amount' => $amount,		  
		);

		// Optional
		$customer_details = array(
		  'first_name'    => $this->session->userdata('user_name'),
		  'email'         => $this->session->userdata('email'),
		  'phone'         => $this->session->userdata('no_hp')
		  // 'billing_address'  => $billing_address,
		  // 'shipping_address' => $shipping_address
		);

		
		// $creditcard = array(
		// 	'secure' => true,
		// 	"save_card" => true
		// );

		// Fill transaction details
		$transaction = array(			
		  	'transaction_details' => $transaction_details,		  	
		  	'customer_details' => $customer_details,		  	
		  	'enabled_payments' => array('permata_va')
		);
		//error_log(json_encode($transaction));
		$snapToken = $this->midtrans->getSnapToken($transaction);
		error_log($snapToken);
		echo $snapToken;
    }
	
	public function finish()
    {
    	$array =  $this->input->post('result_data');
    	echo "<br><br>";
    	print_r($array);
    	echo "<br><br>";
    	$obj = json_decode($array);

    	$status_cek = $obj->{'transaction_status'};

    	$serverKey = "VT-server-31TBIo8Zf4Pa-I7j4pKj8CID";
    	$input = $obj->{'order_id'}.$obj->{'status_code'}.$obj->{'gross_amount'}.$serverKey;
    	$signature = openssl_digest($input, 'sha512');
    	$iduser = $this->session->userdata('id_user');
    	$namauser = $this->session->userdata('user_name');
		$typepayment = $obj->{'payment_type'};
		if ($typepayment == 'echannel') {
			$where = array(
				'transaction_id' => $obj->{'transaction_id'},
				'id_user' => $iduser,
				'order_id' => $obj->{'order_id'},
				'status_code' => $obj->{'status_code'},
				'gross_amount' => $obj->{'gross_amount'},
				'payment_type' => $obj->{'payment_type'},
				'signature_key' => $signature,
				'transaction_status' => $obj->{'transaction_status'},
				'fraud_status' => $obj->{'fraud_status'},
				'status_message' => $obj->{'status_message'},
				'transaction_time' => $obj->{'transaction_time'},
				'bill_key' => $obj->{'bill_key'},
				'biller_code' => $obj->{'bill_key'},
				'pdf_url' => $obj->{'pdf_url'},
			);
		}
		else if ($typepayment == 'bank_transfer') {
			$where = array(
				'transaction_id' => $obj->{'transaction_id'},
				'id_user' => $iduser,
				'order_id' => $obj->{'order_id'},
				'status_code' => $obj->{'status_code'},
				'gross_amount' => $obj->{'gross_amount'},
				'payment_type' => $obj->{'payment_type'},
				'signature_key' => $signature,
				'transaction_status' => $obj->{'transaction_status'},
				'fraud_status' => $obj->{'fraud_status'},
				'status_message' => $obj->{'status_message'},
				'transaction_time' => $obj->{'transaction_time'},
				'permata_va_number' => $obj->{'permata_va_number'},				
				'pdf_url' => $obj->{'pdf_url'},
			);
		}
		else
		{
			$where = array(
				'transaction_id' => $obj->{'transaction_id'},
				'id_user' => $iduser,
				'order_id' => $obj->{'order_id'},
				'status_code' => $obj->{'status_code'},
				'masked_card' => $obj->{'masked_card'},
				'approval_code' => $obj->{'approval_code'},
				'bank' => $obj->{'bank'},
				'gross_amount' => $obj->{'gross_amount'},
				'payment_type' => $obj->{'payment_type'},
				'signature_key' => $signature,
				'transaction_status' => $obj->{'transaction_status'},
				'fraud_status' => $obj->{'fraud_status'},
				'status_message' => $obj->{'status_message'},
				'transaction_time' => $obj->{'transaction_time'},
			);
		}

		$senddata 		= $this->Process_model->midadd("log_midtrans_transaction",$where);
		$email_tujuan	= $this->session->userdata("email");

		$date = date_create($obj->{'transaction_time'});
        $datee = date_format($date, 'd/m/y');
        $hari = date_format($date, 'd');
        $tahun = date_format($date, 'Y');
                                                
        $date = $datee;
        $sepparator = '/';
        $parts = explode($sepparator, $date);
        $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));
        $hasilconvert = number_format($obj->{'gross_amount'}, 0, ".", ".");

        $datetimee = new DateTime($obj->{'transaction_time'});
		$datetimee->modify('+1 day');
		$datelimitt = $datetimee->format('Y-m-d H:i:s');

		if ($senddata == 0) {	
			$rets['mes_alert'] ='warning';
			$rets['mes_display'] ='block';
			$rets['mes_message'] = 'Transaksi Gagal';			
			redirect('Epocket');
		}
		else
		{		
			// Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
            curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_topup','content' => array("email_tujuan" => $email_tujuan, "typepayment" => $typepayment, "namauser" => $namauser, "hasilconvert" => $hasilconvert,"datelimitt" => $datelimitt ) )));

            // Execute post
            $result = curl_exec($ch);
            curl_close($ch);
            // echo $result;


          if ($result) {
                redirect($obj->{'finish_redirect_url'});
          }
          
			
		}
    }

    public function finishtr()
    {
    	$orderid = $this->Rumus->gen_order_id();
    	$iduser = $this->session->userdata("id_user");
    	$nominal = $this->input->post("nominalsave");
    	$kodeunik = $this->Rumus->RandomUnixCode();
    	$nominalakh = $nominal + $kodeunik;
    	// $method = $this->input->post("transferbank");
    	$namauser = $this->session->userdata("user_name");
    	$email_tujuan = $this->session->userdata("email");
    	$where = array(
    		'order_id' => $orderid,
    		'id_user' => $iduser,
    		'jumlah' => $nominalakh 
    	);
    	$senddata = $this->Process_model->midtradd("log_bank_transfer",$where);
    	$tanggalhariini = $senddata['tanggalhariini'];

    	$date = date_create($tanggalhariini);
        $datee = date_format($date, 'd/m/y');
        $hari = date_format($date, 'd');
        $tahun = date_format($date, 'Y');
                                                
        $date = $datee;
        $sepparator = '/';
        $parts = explode($sepparator, $date);
        $bulan = date("F", mktime(0, 0, 0, $parts[1], $parts[2], $parts[0]));

        //untuk timelimit 
        $dateee = date("Y-m-d H:i:s");
		$datetime = new DateTime($dateee);
		$datetime->modify('+2 day');
		$limitdate = $datetime->format('Y-m-d H:i:s');

        $datelimit = date_create($limitdate);
        $dateelimit = date_format($datelimit, 'd/m/y');
        $harilimit = date_format($datelimit, 'd');
        $tahunlimit = date_format($datelimit, 'Y');
        $waktulimit = date_format($datelimit, 'H:i');
                                                
        $datelimit = $dateelimit;
        $sepparatorlimit = '/';
        $partslimit = explode($sepparatorlimit, $datelimit);
        $bulanlimit = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

        $hasilconvert = number_format($nominalakh, 0, ".", ".");

    	if ($senddata == 0) {
    		$rets['mes_alert'] ='warning';
			$rets['mes_display'] ='block';
			$rets['mes_message'] = 'Konfirmasi Gagal';
    		redirect('Epocket');
    	}
    	else
    	{
    		// Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
            curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_finishtr','content' => array("email_tujuan" => $email_tujuan, "hari" => $hari, "bulan" => $bulan, "tahun" => $tahun, "hasilconvert" => $hasilconvert, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "email" => $email ) )));

            // Execute post
            $result = curl_exec($ch);
            curl_close($ch);
            // echo $result;


          if ($result) {
                $this->session->set_userdata('idorder',$orderid);
                // redirect('first/transfer');
                $data['link'] = base_url()."Transfer";              
                echo json_encode($data);
          }
          
    	}
    }

    public function confrim()
    {
    	$id = $this->session->userdata('id_user');
    	$tokenid = $this->input->post('orderid');
    	$bank = $this->input->post("bank");    	
    	$frombank = $this->db->query("SELECT bank_name FROM master_bank WHERE bank_id='$bank'")->row_array()['bank_name']; 
    	$nominalsave = $this->input->post("nominalsave");
    	// $norekening = $getbankid['bank_id'];
    	$namapemilikbank = $this->input->post("namapemilikbank");
    	$metodepembayaran = $this->input->post("metodepembayaran");
    	$buktibayar = $id.'_'.$tokenid.'.jpg';
        $type = $this->input->post('type');
    	$where = array(
    		'order_id' => $tokenid,
    		'id_user' => $id, 
    		'bank_id' => $bank,
    		'frombank' => $frombank,
    		'jumlah' => $nominalsave,
    		'nama_tr' => $namapemilikbank,
    		'metod_tr' => $metodepembayaran,
    		'bukti_tr' => $buktibayar,
            'type_confirm' => $type
    	);
    	$kirimdata = $this->Process_model->tradd("log_trf_confirmation",$where);
    	
    	if ($kirimdata == 0) {
    		$load = $this->lang->line('completeprofile');
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message',"Konfirmasi Gagal");
			redirect('/');
            // $json = array("status" => -1,"code" => '-202',"message" => 'Konfirmasi Gagal');
    	}
    	else if ($kirimdata == 1)
    	{    		
	    	$folder 	= './aset/img/buktipembayaran/';
			$file_size	= $_FILES['buktibayar']['size'];
			$max_size	= 2000000;
			$file_name	= $id.'_'.$tokenid.'.jpg';

			if($file_size > $max_size){											
				// echo "<script>alert('Ukuran file melebihi batas maximum');</script>";
                $this->session->set_flashdata('mes_alert','danger');
                $this->session->set_flashdata('mes_display','block');
                $this->session->set_flashdata('mes_message',"Ukuran file melebihi batas maximum");
				if ($type=="topup") {
                    redirect('Epocket');
                }
                else
                {
                   // $json = array("status" => -2,"code" => '-202',"message" => 'Ukuran file melebihi batas maximum');
                   redirect('/');
                }
			}
			else
			{
				if (move_uploaded_file($_FILES['buktibayar']['tmp_name'], $folder.$file_name)) {
					$config['image_library'] = 'gd2';
					$config['source_image']	= "./aset/img/buktipembayaran/".$file_name;
					$config['width']	= 250;
					$config['maintain_ratio'] = FALSE;
					$config['height']	= 250;

					$this->image_lib->initialize($config); 

					$this->image_lib->resize();                    
					
                    if ($type=="topup") {
                        redirect('Epocket');
                    }
                    else
                    {
                        $detail_banktujuan  = $this->db->query("SELECT bank_name FROM master_bank WHERE bank_id='$bank'")->row_array()['bank_name'];
                        $getdetail          = $this->db->query("SELECT * FROM tbl_order WHERE order_id='$tokenid'")->row_array();
                        $detail_hargatotal  = $getdetail['total_price'];
                        $detail_hargatotal  = number_format($detail_hargatotal, 0, ".", ".");
                        $detail_nominalsave = number_format($nominalsave, 0, ".", ".");
                        $detail_tanggaltr   = $getdetail['last_update'];
                        $detail_buyerid     = $getdetail['buyer_id'];
                        $detail_buyername   = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$detail_buyerid'")->row_array()['user_name'];
                        $detail_invoice     = $this->db->query("SELECT invoice_id FROM tbl_invoice WHERE order_id='$tokenid'")->row_array()['invoice_id'];

                        //BATAS TANGGAL PEMBAYARAN
                        $dateee             = $detail_tanggaltr;
                        $user_utcc          = $this->session->userdata('user_utc');
                        $server_utcc        = $this->Rumus->getGMTOffset();
                        $intervall          = $user_utcc - $server_utcc;
                        $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$dateee);
                        $tanggaltransaksi->modify("+".$intervall ." minutes");
                        $tanggaltransaksi   = $tanggaltransaksi->format('Y-m-d H:i:s');

                        $datetime           = new DateTime($tanggaltransaksi);
                        $datetime->modify('+1 hours');
                        $limitdateee        = $datetime->format('Y-m-d H:i:s');

                        $akhirtanggal       = date_create($limitdateee);
                        $dateelimit         = date_format($akhirtanggal, 'd/m/y');
                        $hariakhir          = date_format($akhirtanggal, 'd');
                        $hariakhir          = date_format($akhirtanggal, 'l');
                        $tahunakhir         = date_format($akhirtanggal, 'Y');
                        $waktuakhir         = date_format($akhirtanggal, 'H:i');
                                                                
                        $dateakhir          = $dateelimit;
                        $sepparatorakhir    = '/';
                        $partsakhir         = explode($sepparatorakhir, $dateakhir);
                        $bulanakhir         = date("F", mktime(0, 0, 0, $partsakhir[1], $partsakhir[2], $partsakhir[0]));

                        $get_detailorder    = $this->db->query("SELECT * FROM tbl_order_detail WHERE order_id='$tokenid'")->result_array();
                        foreach ($get_detailorder as $resultdata) {
                            $request_id     = $resultdata['product_id'];
                            // $type           = null;
                            // $gettype        = $this->db->query("SELECT * FROM tbl_request WHERE request_id='$request_id'")->row_array();
                            // if (!empty($gettype)) {
                            //     $type = "private";
                            // }
                            // else
                            // {
                            //     $type = "group";
                            // }

                            if ($type == "private") {
                                $tutor_id   = $this->db->query("SELECT tutor_id FROM tbl_request WHERE request_id='$request_id'")->row_array()['tutor_id'];
                                $subject_id = $this->db->query("SELECT subject_id FROM tbl_request WHERE request_id='$request_id'")->row_array()['subject_id'];
                                $getdataclass  = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array(); 

                                $iduserrequest      = $getdataclass['id_user_requester'];  
                                $daterequesttt      = $getdataclass['date_requested'];  
                                $durationrequest    = $getdataclass['duration_requested'];     
                            }
                            else if($type == "group")
                            {
                                $tutor_id   = $this->db->query("SELECT tutor_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['tutor_id'];
                                $subject_id = $this->db->query("SELECT subject_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['subject_id'];
                                $getdataclass   = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request_grup as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array(); 

                                $iduserrequest      = $getdataclass['id_user_requester']; 
                                $daterequesttt      = $getdataclass['date_requested'];   
                                $durationrequest    = $getdataclass['duration_requested'];     
                            } 
                            else
                            {                                
                                $getdataclass   = $this->db->query("SELECT tu.user_name, ms.subject_name, tc.description as topic, tc.start_time, tc.finish_time, trm.created_at as date_requested, trm.id_requester as id_user_requester FROM tbl_class as tc INNER JOIN tbl_user as tu on tu.id_user=tc.tutor_id INNER JOIN master_subject as ms ON ms.subject_id=tc.subject_id INNER JOIN tbl_request_multicast as trm ON trm.class_id=tc.class_id WHERE tc.class_id='$request_id'")->row_array();  
                                $iduserrequest  = $getdataclass['id_user_requester']; 
                                $daterequesttt  = $getdataclass['date_requested']; 
                                $duratioon      = strtotime($getdataclass['start_time']) - strtotime($getdataclass['finish_time']);
                                $durationrequest= $duratioon;  

                                // $harga_k        = $this->db->query("SELECT harga_cm FROM tbl_class_price WHERE class_id='$class_id'")->row_array()['harga_cm']; 
                                // $hargaperkelas  = number_format($hargaperkelas, 0, ".", ".");     
                            }

                            $user_utc           = $this->session->userdata('user_utc');                            
                            $namauser           = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['user_name'];
                            // $durasi             = $getdataclass['duration_requested'];
                            $server_utcc        = $this->Rumus->getGMTOffset();
                            $intervall          = $user_utc - $server_utcc;
                            $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$daterequesttt);
                            $tanggal->modify("+".$intervall ." minutes");
                            $tanggal            = $tanggal->format('Y-m-d H:i:s');
                            $datelimit          = date_create($tanggal);
                            $dateelimit         = date_format($datelimit, 'd/m/y');
                            $harilimit          = date_format($datelimit, 'd');
                            $hari               = date_format($datelimit, 'l');
                            $tahunlimit         = date_format($datelimit, 'Y');
                            $waktulimit         = date_format($datelimit, 'H:i');   
                            $datelimit          = $dateelimit;
                            $sepparatorlimit    = '/';
                            $partslimit         = explode($sepparatorlimit, $datelimit);
                            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
                            $seconds            = $durationrequest;
                            $hours              = floor($seconds / 3600);
                            $mins               = floor($seconds / 60 % 60);
                            $secs               = floor($seconds % 60);
                            $durationrequested  = sprintf('%02d:%02d', $hours, $mins);

                            $kotakdetail = 
                            "<tr>
                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdataclass['user_name']."</td>
                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdataclass['subject_name']."</td>
                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdataclass['topic']."</td>
                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.", Pukul ".$waktulimit."</td>
                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$durationrequested."</td> 
                            </tr>";   
                        }                        

                        // Open connection
                        $ch = curl_init();

                        // Set the url, number of POST vars, POST data
                        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
                        curl_setopt($ch, CURLOPT_POST, true);
                        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_confirm','content' => array("detail_buyername" => $detail_buyername, "detail_banktujuan" => $detail_banktujuan, "detail_hargatotal" => $detail_hargatotal, "detail_nominalsave" => $detail_nominalsave, "frombank" => $frombank, "namapemilikbank" => $namapemilikbank, "metodepembayaran" => $metodepembayaran, "hariakhir" => $hariakhir, "hari" => $hari, "bulanakhir" => $bulanakhir, "tahunakhir" => $tahunakhir, "waktuakhir" => $waktuakhir, "detail_invoice" => $detail_invoice, "kotakdetail" => $kotakdetail) )));

                        // Execute post
                        $result = curl_exec($ch);
                        curl_close($ch);
                        // echo $result;


                      if ($result) {
                            $this->session->set_flashdata('mes_alert','success');
                            $this->session->set_flashdata('mes_display','block');
                            $this->session->set_flashdata('mes_message',"Konfirmasi Pembayaran Berhasil, Silahkan tunggu hingga kami selesai mem-verifikasi konfirmasi data Anda"); 
                            redirect('/');
                            // $json = array("status" => 1,"code" => '-202',"message" => 'Konfirmasi Pembayaran Berhasil, Silahkan tunggu hingga kami selesai mem-verifikasi konfirmasi data Anda');
                      }else{
                            $this->session->set_flashdata('mes_alert','danger');
                            $this->session->set_flashdata('mes_display','block');
                            $this->session->set_flashdata('mes_message',"Terjadi kesalahan");
                            redirect('/');
                            // $json = array("status" => 0,"code" => '-202',"message" => 'Konfirmasi gagal, silahkan coba beberapa saat kembali');    
                      }
                    }
				}
			}
		}

        echo json_encode($json);
    }

    public function edtconfirm()
    {
    	$trx_id 	= $this->input->post('order_id');
    	$id 		= $this->session->userdata('id_user');
    	$bank 		= $this->input->post('bank');
    	$jumlah 	= $this->input->post('nominalsave');
    	$norek_tr 	= $this->input->post('norekeningbank');
    	$nama_tr 	= $this->input->post('namapemilikbank');
    	$metod_tr 	= $this->input->post('metodepembayaran');
    	$bukti_tr 	= $id.'_'.$trx_id.'.jpg';

    	$where = array(
    		'order_id' => $trx_id,
    		'id_user' => $id,
    		'bank_id' => $bank,
    		'jumlah' => $jumlah,
    		'norekeningbank' => $norek_tr,
    		'nama_tr' => $nama_tr,
    		'metod_tr' => $metod_tr,
    		'bukti_tr' => $bukti_tr
    	);
    	$edtconfirm = $this->Process_model->edttr("log_trf_confirmation", $where);

    	if ($edtconfirm == 0) {
    		$load = $this->lang->line('completeprofile');
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message',"Gagal mengubah data");
			redirect('Epocket');
    	}
    	else if ($edtconfirm == 1)
    	{    		
    		if ($_FILES['buktibayar']['name'] == "") {
    			$this->session->set_flashdata('mes_alert','success');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message',"Perubahan data Konfirmasi berhasil, Silahkan tunggu hingga kami selesai mem-verifikasi data Anda");	
				redirect('Epocket');
    		}
    		else
    		{
		    	$folder 	= './aset/img/buktipembayaran/';
				$file_size	= $_FILES['buktibayar']['size'];
				$max_size	= 2000000;
				$file_name	= $bukti_tr;

				if($file_size > $max_size){											
					echo "<script>alert('Ukuran file melebihi batas maximum');</script>";
					redirect('Epocket');
				}
				else
				{
					if (move_uploaded_file($_FILES['buktibayar']['tmp_name'], $folder.$file_name)) {
						$config['image_library'] = 'gd2';
						$config['source_image']	= "./aset/img/buktipembayaran/".$file_name;
						$config['width']	= 250;
						$config['maintain_ratio'] = FALSE;
						$config['height']	= 250;

						$this->image_lib->initialize($config); 

						$this->image_lib->resize();

						$this->session->set_flashdata('mes_alert','success');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message',"Perubahan data Konfirmasi berhasil, Silahkan tunggu hingga kami selesai mem-verifikasi data Anda");	
						redirect('Epocket');
					}
				}
			}
		}
    }

    public function confirm_bank()
    {
    	$trx_id = $_GET['trx_id'];
    	$upd 	= $this->db->query("UPDATE log_trf_confirmation SET accepted='1' WHERE trx_id='$trx_id'");
    	if ($upd) {
    		$this->db->query("UPDATE log_transaction SET flag='1' WHERE trx_id='$trx_id'");
    		
    		$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message',"Konfirmasi Berhasil");
			redirect('Admin/Confrim_transaction');
    	}
    	else
    	{
    		$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message',"Gagal");
    	}
    }

    function testfinish()
    {
    	$array =  $this->input->post('result_data');
    	echo "<br><br>";
    	print_r($array);
    	echo "<br><br>";
    	$obj = json_decode($array);
    	$serverKey = "VT-server-31TBIo8Zf4Pa-I7j4pKj8CID";
    	$input = $obj->{'order_id'}.$obj->{'status_code'}.$obj->{'gross_amount'}.$serverKey;
    	$signature = openssl_digest($input, 'sha512');
		echo "<br><br>";
		echo 'Status Code : '.$obj->{'status_code'};
		echo "<br>";
		echo 'Status Message : '.$obj->{'status_message'};
		echo "<br>";
		echo 'Transaction ID : '.$obj->{'transaction_id'};
		echo "<br>";
		echo 'Order ID : '.$obj->{'order_id'};
		echo "<br>";
		echo 'Gross Amount : '.$obj->{'gross_amount'};
		echo "<br>";
		echo 'Payment Type : '.$obj->{'payment_type'};
		echo "<br>";
		echo 'Transaction Time : '.$obj->{'transaction_time'};
		echo "<br>";
		echo 'Transaction Status : '.$obj->{'transaction_status'};
		echo "<br>";
		echo 'Fraud Status : '.$obj->{'fraud_status'};
		echo "<br>";
		echo 'Approval Code : '.$obj->{'approval_code'};
		echo "<br>";
		echo 'Masked Card : '.$obj->{'masked_card'};
		echo "<br>";
		echo 'Bank : '.$obj->{'bank'};
		echo "<br>";
		echo 'Signature Key : '.$signature;
		echo "<br>";
		echo 'Finish URL : '.$obj->{'finish_redirect_url'};

    }

    public function test()
    {
		
    	$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' =>'info@classmiles.com',
			'smtp_pass' => 'mcwskvounsdcwvch',	
			'mailtype' => 'html',	
			// 'wordwrap' => true,
			'charset' => 'iso-8859-1'
			);

		$this->load->library('email', $config);
		$this->load->view(MOBILE.'inc/header');
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");
		$this->email->from('info@classmiles.com', 'Classmiles');
		$this->email->to('dhanicoc78@gmail.com');
		$this->email->subject('Classmiles - Account Activation');

		$templateverifikasi = 
		'<!DOCTYPE html>
		<html>
		<head>	
		</head>
		<body background="grey">

			<div style="width: 20%; float: left;"><label style="opacity: 0;">sdfsdff</label></div>
			<div style="width: 60%; float: left;">
				<div style="width: 100%; background-color: #03a9f4; height: 5px;"></div>
				<div style="padding-left: 32px; padding-right: 32px; padding-top: 10px; padding-bottom: 5px; background-color: white;">
					<h1>Classmiles</h1><small style="font-size: 12px;">Classroom at your hands</small>
					<hr>
				</div>
				<div style="padding: 32px; margin-top: -15px; background-color: #e0e0e0;">
					<label style="float: left; width: 50%; text-align: left; font-size: 13.2px; margin-top: -8px;">06 Januari 2017 - 15:53:17</label>
					<label style="float: left; width: 50%; text-align: right; font-size: 13.2px; margin-top: -8px;">ORDER ID : 8480314312</label>
				</div>
				<div style="padding-left: 32px; padding-right: 32px; padding-top: 25px; padding-bottom: 15px; background-color: white;">
					<label style="font-size: 15px;">Dear Ramdhani,</label><br>
					<label style="font-size: 13px;">Transaksi top up saldo Anda berhasil! Rincian top up Anda di bawah ini:</label><br><br>			
				</div>
				<div style="padding-left: 25%; padding-right: 25%; padding-top: 25px; padding-bottom: 15px; background-color: white;">
					sdfds
				</div>
			</div>
			<div style="width: 20%; float: left;"></div>
		</body>
		</html>
		';
		/**/				
		$this->email->message($templateverifikasi);

		if ($this->email->send()) 
		{
			redirect("/");
		}	
    }
    public function payment_confirmation($value='')
	{		
		$datas = $this->input->post();
		// print_r('<pre>');
		// print_r($this->input->post());
		// print_r('</pre>');
		// return null;
		// $this->db->simple_query("INSERT INTO test(a) VALUES('".$this->input->post('bukti_tr')."')");
		// return null;


    	$tokenid 			= $datas['trx_id'];
		$id 				= $datas['id_user'];
    	$bank 				= $datas["bank_id"];    	
    	$nominalsave	 	= $datas["jumlah"];
    	$norekening 	 	= $datas["norek_tr"];
    	$namapemilikbank 	= $datas["nama_tr"];
    	$metodepembayaran 	= $datas["metod_tr"];
    	$buktibayar 		= $id.'_'.$tokenid.'.jpg';
    	$bukti 				= $datas["bukti_tr"];
    	// $this->db->simple_query("INSERT INTO test(a) VALUES('{$bukti}')");

		// $gambar_base64 = $DATA_DARI_POST['bukti_tr'];
    	$processadd = $this->db->query("INSERT INTO log_trf_confirmation (trx_id, id_user, bank_id, jumlah, norek_tr, nama_tr, metod_tr, bukti_tr, accepted) VALUES ('$tokenid','$id','$bank','$nominalsave','$norekening',
    		'$namapemilikbank','$metodepembayaran','$buktibayar','0')");
    	if ($processadd) {    	
    		if($bukti != ''){
				$image = 'data:image/jpeg;base64,' .str_replace(' ','+',$bukti); //replacing ' ' with '+'				
					 
				//Write to disk
				file_put_contents("aset/img/buktipembayaran/".$buktibayar,file_get_contents($image));
                $res['status'] = true;
                // $res['img'] = $image;
				$res['message'] = 'Sukses bro dengan gambar';
				// $this->response($res);
				echo json_encode($res);
				return null;

				// $imsrc = base64_decode($imsrc);
				// $imgname = "aset/img/user/".$buktibayar;
				// $fp = fopen($imgname, 'w');
				// fwrite($fp, $imsrc);
				// if(fclose($fp)){
				// }else{
				// 	$res['status'] = false;
				// 	$res['message'] = 'Gagal bro';			
				// 	$this->response($res);
				// }
	            
        	}

			// $config['image_library'] = 'gd2';
			// $config['source_image']	= "aset/img/user/".$buktibayar.".jpg";
			// $config['width']	= 250;
			// $config['maintain_ratio'] = FALSE;
			// $config['height']	= 250;

			// $this->image_lib->initialize($config); 

			// $this->image_lib->resize();
			
			$res['status'] = true;
			$res['message'] = 'Sukses bro';		
			echo json_encode($res);
			return null;    
			$this->response($res);
		}
		else
		{
			$res['status'] = false;
			$res['messag'] = 'Gagal bro';			
			echo json_encode($res);
			return null;
			$this->response($res);
		}	
	}
	public function update_payment_confirmation($value='')
	{		
		$datas = $this->input->post();
		// print_r('<pre>');
		// print_r($this->input->post());
		// print_r('</pre>');
		// return null;
		// $this->db->simple_query("INSERT INTO test(a) VALUES('".$this->input->post('bukti_tr')."')");
		// return null;


    	$tokenid 			= $datas['trx_id'];
		$id 				= $datas['id_user'];
    	$bank 				= $datas["bank_id"];    	
    	$nominalsave	 	= $datas["jumlah"];
    	$norekening 	 	= $datas["norek_tr"];
    	$namapemilikbank 	= $datas["nama_tr"];
    	$metodepembayaran 	= $datas["metod_tr"];
    	$buktibayar 		= $id.'_'.$tokenid.'.jpg';
    	$bukti 				= isset($datas["bukti_tr"]) ? $datas["bukti_tr"] : "";
    	// $this->db->simple_query("INSERT INTO test(a) VALUES('{$bukti}')");

		// $gambar_base64 = $DATA_DARI_POST['bukti_tr'];
    	$processadd = $this->db->query("UPDATE log_trf_confirmation SET id_user='$id',bank_id='$bank',jumlah='$nominalsave',norek_tr='$norekening',nama_tr='$namapemilikbank',metod_tr='$metodepembayaran',bukti_tr='$buktibayar',accepted = '0' WHERE trx_id='$tokenid'");
    	if ($processadd) {    	
    		if($bukti != ''){
				$image = 'data:image/jpeg;base64,' .str_replace(' ','+',$bukti); //replacing ' ' with '+'				
					 
				//Write to disk
				file_put_contents("aset/img/buktipembayaran/".$buktibayar,file_get_contents($image));
                $res['status'] = true;
                // $res['img'] = $image;
				$res['message'] = 'Sukses bro dengan gambar';
				// $this->response($res);
				echo json_encode($res);
				return null;

				// $imsrc = base64_decode($imsrc);
				// $imgname = "aset/img/user/".$buktibayar;
				// $fp = fopen($imgname, 'w');
				// fwrite($fp, $imsrc);
				// if(fclose($fp)){
				// }else{
				// 	$res['status'] = false;
				// 	$res['message'] = 'Gagal bro';			
				// 	$this->response($res);
				// }
	            
        	}

			// $config['image_library'] = 'gd2';
			// $config['source_image']	= "aset/img/user/".$buktibayar.".jpg";
			// $config['width']	= 250;
			// $config['maintain_ratio'] = FALSE;
			// $config['height']	= 250;

			// $this->image_lib->initialize($config); 

			// $this->image_lib->resize();
			
			$res['status'] = true;
			$res['message'] = 'Sukses bro';		
			echo json_encode($res);
			return null;    
			$this->response($res);
		}
		else
		{
			$res['status'] = false;
			$res['messag'] = 'Gagal bro';			
			echo json_encode($res);
			return null;
			$this->response($res);
		}	
	}

	public function update_image_profile_andro($value='')
	{
		$datas = $this->input->post();
		
		$id 				= $datas['id_user'];
        $basename           = $id.$this->Rumus->RandomString();
    	$img_name      		= $basename.'.jpg';
    	$bukti 				= isset($datas["image"]) ? $datas["image"] : "";
    	// $this->db->simple_query("INSERT INTO test(a) VALUES('{$bukti}')");

		// $gambar_base64 = $DATA_DARI_POST['bukti_tr'];
    	$processadd = $this->db->query("SELECT *FROM tbl_user where id_user = '{$id}'")->row_array();
    	if ($processadd) {
            $image = 'data:image/jpeg;base64,' .str_replace(' ','+',$bukti); //replacing ' ' with '+'               
                         
            //Write to disk
            file_put_contents("aset/_temp_images/".$img_name,file_get_contents($image));
            
            $this->Rumus->sendToCDN('user', $img_name, null, 'aset/_temp_images/'.$img_name );
            $res['status'] = true;
            // $res['img'] = $image;
            $res['message'] = 'Sukses bro dengan gambar';

            $updatephoto = $this->db->query("UPDATE tbl_user SET user_image='".base64_encode('user/'.$img_name)."' WHERE id_user='$id'");

            echo json_encode($res);
            return null;
    		/*if ($processadd['user_image'] == "empty.jpg") {
    			if($bukti != ''){
					$image = 'data:image/jpeg;base64,' .str_replace(' ','+',$bukti); //replacing ' ' with '+'				
					
					$updatephoto = $this->db->query("UPDATE tbl_user SET user_image='$img_name' WHERE id_user='$id'");
					//Write to disk
					file_put_contents("aset/img/user/".$img_name,file_get_contents($image));
	                $res['status'] = true;
	                // $res['img'] = $image;
					$res['message'] = 'Sukses bro dengan gambar';
					// $this->response($res);
					echo json_encode($res);
					return null;
		            
	        	}
    		} else {
	    		if($bukti != ''){
					$image = 'data:image/jpeg;base64,' .str_replace(' ','+',$bukti); //replacing ' ' with '+'				
						 
					//Write to disk
					file_put_contents("aset/img/user/".$img_name,file_get_contents($image));
	                $res['status'] = true;
	                // $res['img'] = $image;
					$res['message'] = 'Sukses bro dengan gambar';
					// $this->response($res);
					echo json_encode($res);
					return null;
		            
	        	}
        	}
			
			$res['status'] = false;
			$res['message'] = 'Gagal bro';		
			echo json_encode($res);
			return null;*/
		}
		else
		{
			$res['status'] = false;
			$res['messag'] = 'Gagal bro';			
			echo json_encode($res);
			return null;
			$this->response($res);
		}	
	}

	function addrekening()
	{
		$iduser 	= $this->session->userdata('id_user');
		$namaakun 	= $this->input->post('namaakun');
		$norekening = $this->input->post('norekening');
		$namabank 	= $this->input->post('namabank');
		$cabangbank = $this->input->post('cabangbank');

		$getbankname = $this->db->query("SELECT bank_name FROM master_bank WHERE bank_id='$namabank'")->row_array();
		$where = array(
			'iduser' => $iduser,
			'namaakun' => $namaakun,
			'norekening' => $norekening,
			'bank_id' => $namabank,
			'namabank' => $getbankname['bank_name'],
			'cabangbank' => $cabangbank			
		);

		
		$add = $this->Process_model->bankadd("tbl_rekening", $where);
		if ($add == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Berhasil Menambahkan akun rekening bank');
			redirect("Accountlist");
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal Menambahkan akun rekening bank');
			redirect("Accountlist");
		}
	}

	function editrekening()
	{
		$idrekening = $this->input->post('id_rekening');
		$namaakun = $this->input->post('namaakun');
		$norekening = $this->input->post('norekening');
		$bank_id = $this->input->post('bank_id');
		$cabang = $this->input->post('cabangbank');

		$where = array(
			'id_rekening' => $idrekening,
			'nama_akun' => $namaakun,
			'nomer_rekening' => $norekening,
			'bank_id' => $bank_id,
			'cabang' => $cabang 
		);

		$edtrekening = $this->Process_model->bankedit("tbl_rekening", $where);
		if ($edtrekening == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Berhasil Mengubah akun rekening bank');
			redirect("Accountlist");
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal Mengubah akun rekening bank');
			redirect("Accountlist");
		}
	}

	function deleterekening()
	{
		$id_rekening = $this->input->post('id_rekening');

		$where = array(
			'id_rekening' => $id_rekening, 
		);

		$delete = $this->Process_model->bankdelete("tbl_rekening", $where);
		if ($delete == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Berhasil menghapus akun rekening bank');
			redirect("Accountlist");
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal menghapus akun rekening bank');
			redirect("Accountlist");
		}
	}

	public function callsupport()
	{
		$status = $this->session->userdata('usertype_id');
		$userid = $this->session->userdata('id_user');

		if ($status != "tutor") {
			redirect("tutor");
		}
		else
		{
			$where = array(
				'id_tutor' => $userid, 
				'type' => "Preclass",
				'status' => "0"
			);

			$addsupport = $this->Process_model->support("tbl_support", $where);
			if ($addsupport == 1) {
				echo json_encode(array("status"=>true, "message" => "Silahkan tunggu hingga Support kami menghubungi anda. Terima kasih"));
				return 1;
				/*$this->session->set_flashdata('mes_alert','success');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Silahkan tunggu hingga Support kami menghubungi anda. Terima kasih');
				redirect("tutor");*/
			}
			else if ($addsupport == 2) {
				echo json_encode(array("status"=>true, "message" => "Request kamu sudah masuk ke data kami. Harap tunggu hingga support kami menghubungi anda. Terima kasih"));
				return 1;
				/*$this->session->set_flashdata('mes_alert','warning');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Request kamu sudah masuk ke data kami. Harap tunggu hingga support kami menghubungi anda. Terima kasih');
				redirect("tutor");*/
			}
			else
			{
				echo json_encode(array("status"=>false, "message" => "Gagal menghubungi call support"));
				return 1;
				/*$this->session->set_flashdata('mes_alert','success');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Gagal menghubungi call support');
				redirect("tutor");*/
			}
		}
	}

	public function addtext_announ()
	{
		$isitext 	= $this->input->post("isitext");
		
		$where = array(
			'text_announ' => $isitext			
		);

		$addtext = $this->Process_model->announ_add("tbl_announcement", $where);
		if ($addtext == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Berhasil menambahkan Announcement');
			redirect("admin/Announcement");
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal');
			redirect("admin/Announcement");
		}
	}

	public function edittext_announ()
	{
		$id_announ		= $this->input->post("id_announ");
		$text_announ 	= $this->input->post("text_announ");
		
		$where = array(
			'id_announ' => $id_announ,
			'text_announ' => $text_announ	
		);

		$edittext = $this->Process_model->announ_edit("tbl_announcement", $where);
		if ($edittext == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Berhasil mengubah Announcement');
			redirect("admin/Announcement");
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal');
			redirect("admin/Announcement");
		}
	}

	public function deletetext_announ()
	{
		$id_announ 	= $this->input->post("id_announ");
		
		$where = array(
			'id' => $id_announ			
		);

		$addtext = $this->Process_model->announ_delete("tbl_announcement", $where);
		if ($addtext == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Berhasil menghapus Announcement');
			redirect("admin/Announcement");
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal');
			redirect("admin/Announcement");
		}

	}

	public function add_channel()
	{
		$id_user = $this->session->userdata('id_user');
		$channel_name = $this->input->post('channel_name');
		$channel_email = $this->input->post('channel_email');
		$channel_address = $this->input->post('channel_address');
		$channel_country_code = $this->input->post('channel_country_code');
		$channel_callnum = $this->input->post('channel_callnum');

		$where = array(	
			'channel_name' => $channel_name,
			'channel_email' => $channel_email,
			'channel_address' => $channel_address,
			'channel_country_code' => $channel_country_code,
			'channel_callnum' => $channel_callnum
		);

		$proses = $this->Process_model->add_channel($where, $id_user);
		if ($proses == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Berhasil Menambah Channel');
			redirect("admin/Register");
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal');
			redirect("admin/Register");
		}
	}

	function editchannel()
	{
		$channel_id 			= $this->input->post('channel_id');
		$channel_name 			= $this->input->post('channel_name');
		$channel_email 			= $this->input->post('channel_email');
		$channel_country_code 	= $this->input->post('channel_country_code');
		$channel_callnum 		= $this->input->post('channel_callnum');
		$channel_address 		= $this->input->post('channel_address');
		
		$where = array(
			'channel_id' => $channel_id,
			'channel_name' => $channel_name,
			'channel_email' => $channel_email,
			'channel_country_code' => $channel_country_code,
			'channel_callnum' => $channel_callnum,
			'channel_address' => $channel_address
		);

		$edtchannel = $this->Process_model->channeledit("master_channel", $where);
		if ($edtchannel == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Berhasil Mengubah data Channel');
			redirect("Admin/Register");
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal Mengubah data Channel');
			redirect("Admin/Register");
		}
	}

	public function addtutorchannel()
	{
		$tutor_id = $this->input->post('tutor_id');
		$channelid = $this->input->post('channel_id');
		$where = array(
			'channel_id' => $channelid,
			'tutor_id' =>  $tutor_id
		);
		$proses = $this->Process_model->add_tutorchannel("master_channel_tutor",$where);
		if ($proses == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Tutor Berhasil di tambah');
			// redirect("Channel/tutor_channel");
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal');
			// redirect("Channel/tutor_channel");
		}
	}

	public function addstudentchannel()
	{
		$id_user = $this->input->post('id_user');
		$channelid = $this->input->post('channel_id');
		$where = array(
			'channel_id' => $channelid,
			'id_user' =>  $id_user
		);
		$proses = $this->Process_model->add_studentchannel("master_channel_tutor",$where);
		if ($proses == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Student Berhasil di tambah');
			// redirect("Channel/student_channel");
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal');
			// redirect("Channel/student_channel");
		}
	}

	function deletetutorchannel()
	{
		$rtp = $this->input->post('rtp');
		$base64 = strtr($rtp, '-_.', '+/=');
		$rtp = $this->encryption->decrypt($base64);
		$where = array(
			'tutor_id' => $rtp
		);

		$delete = $this->Process_model->tutorchanneldelete("master_channel_tutor", $where);
		if ($delete == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Berhasil menghapus data Tutor');
			// redirect("Accountlist");
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal menghapus data Tutor');
			// redirect("Accountlist");
		}
	}

	function deletestudentchannel()
	{
		// $id_user = $this->input->post('rtp');
		$rtp = $this->input->post('rtp');
		$channel_id = $this->input->post('channel_id');
		$base64 = strtr($rtp, '-_.', '+/=');
		$rtp = $this->encryption->decrypt($base64);
		$where = array(
			'id_user' => $rtp,
			'channel_id' => $channel_id
		);

		$delete = $this->Process_model->studentchanneldelete("master_channel_student", $where);
		if ($delete == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Berhasil menghapus data siswa');
			// redirect("Channel/student_channel");
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal menghapus data siswa');
			// redirect("Channel/student_channel");
		}
	}

	function deletechannel()
	{
		// $id_user = $this->input->post('rtp');
		$rtp = $this->input->post('rtp');
		$base64 = strtr($rtp, '-_.', '+/=');
		$rtp = $this->encryption->decrypt($base64);
		$where = array(
			'channel_id' => $rtp
		);

		$delete = $this->Process_model->channeldelete("master_channel", $where);
		if ($delete == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Berhasil menghapus Channel');
			// redirect("Channel/student_channel");
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal menghapus Channel');
			// redirect("Channel/student_channel");
		}
	}

	function deletesubject()
	{
		// $id_user = $this->input->post('rtp');
		$rtp = $this->input->post('rtp');
		// $base64 = strtr($rtp, '-_.', '+/=');
		// $rtp = $this->encryption->decrypt($base64);
		$where = array(
			'subject_id' => $rtp
		);

		$delete = $this->Process_model->subjectdelete("master_subject", $where);
		if ($delete == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Berhasil menghapus Subject');
			// redirect("Channel/student_channel");
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal menghapus Subject');
			// redirect("Channel/student_channel");
		}
	}

	public function Showlist()
	{
		$class_id = $this->input->post('class_id');
		$channel_id = $this->input->post('channel_id');
		// $this->session->unset_userdata('idclass');
		// $this->session->unset_userdata('idchannel');
		$this->session->set_userdata('idchannel', $channel_id);
		$this->session->set_userdata('idclass', $class_id);

		$getparticipant = $this->db->query("SELECT tbl_class.participant FROM tbl_class WHERE class_id='$class_id'")->row_array();
		if (!empty($getparticipant)) {
			$json = array("status" => 1,"data" => json_decode($getparticipant['participant']));
		}
		else
		{
			$json = array("status" => 0,"data " => 'Gagal');
		}
		echo json_encode($json);
	}

	public function Getname()
	{
		$id_user = $this->input->post('id_user');

		$getname = $this->db->query("SELECT id_user,user_name FROM tbl_user WHERE id_user='$id_user'")->row_array();

		if (!empty($getname)) {
			$json = array("status" => 1, "data" => $getname['user_name'], "id_user" => $getname['id_user']);
		}
		else
		{
			$json = array("status" => 0,"data " => 'Gagal');
		}
		echo json_encode($json);
	}

	function addStudentClassChannel()
	{
		$participantt 	= $this->input->post('idtage');
		$class_id 		= $this->input->post('class_id');
		$channel_id 	= $this->input->post('channel_id');

		$getparticipant = $this->db->query("SELECT duration,class_type,participant FROM tbl_class WHERE class_id='$class_id'")->row_array();

		// foreach ($participantt as $key => $value) {
		// 	echo $value;
		// }
		if ($getparticipant['class_type'] == "private_channel") {
			$decode = json_decode($getparticipant['participant'],true);
			$length = count($decode['participant']);

			// MENGURANGI POINT CHANNEL //
			$exist = $this->db->query("SELECT point_id, active_point FROM uangpoint_channel WHERE channel_id='$channel_id' ")->row_array();	
			if(!empty($exist)){
				$point = $exist['active_point'];
				$point_id = $exist['point_id'];
				$harga_private15menit = 10;
				$price = $harga_private15menit * ( ($getparticipant['duration'] / 60) / 15 );

				if($point >= $price){			
					if ($length == 1) {
						$json = array("status" => 0,"code" => '405');
					}
					else
					{
						$participant_arr = json_decode($getparticipant['participant'],true);
						if(!array_key_exists('participant', $participant_arr)){
							$participant_arr['participant'] = array();
						}
						// foreach ($participantt as $key => $id) {
							array_push($participant_arr['participant'], array("id_user" => $participantt));
						// }
						$participant_arr = json_encode($participant_arr);
						$update = $this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");

						$new_point = $point-$price;
						$this->db->simple_query("UPDATE uangpoint_channel SET active_point=$new_point WHERE point_id='$point_id'");
						$trx_id = $this->Rumus->getChnPointTrxID();
						$this->db->simple_query("INSERT INTO log_channel_point_transaction VALUES('$trx_id','$channel_id','','$class_id','$point','$price','$new_point',now())");

						$myip = $_SERVER['REMOTE_ADDR'];
						$plogc_id = $this->Rumus->getLogChnTrxID();
						$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Add student private list class ','$participantt','$myip',now())");

						if ($update) {
							$json = array("status" => 1,"code" => '200');
						}
						else
						{
							$json = array("status" => 0,"code" => '404');
						}
					}
				}
				else
				{
					$json = array("status" => 0,"code" => '-201');
					
				}
			}
			else
			{
				$json = array("status" => 0,"code" => '-202');
				
			}
		}
		else if ($getparticipant['class_type'] == "group_channel") {
			$decode = json_decode($getparticipant['participant'],true);
			$length = count($decode['participant']);
			
			$exist = $this->db->query("SELECT point_id, active_point FROM uangpoint_channel WHERE channel_id='$channel_id' ")->row_array();
			if(!empty($exist)){
				$point = $exist['active_point'];
				$point_id = $exist['point_id'];
				$harga_group15menit = 20;
				$price = $harga_group15menit * ( ($getparticipant['duration'] / 60) / 15 );

				if($point >= $price){
					if ($length >= 4) {
						$json = array("status" => 0,"code" => '406');
					}
					else
					{
						$participant_arr = json_decode($getparticipant['participant'],true);
						if(!array_key_exists('participant', $participant_arr)){
							$participant_arr['participant'] = array();
						}
						// foreach ($participantt as $key => $id) {
							array_push($participant_arr['participant'], array("id_user" => $participantt));
						// }
						$participant_arr = json_encode($participant_arr);
						$update = $this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");
						$new_point = $point-$price;
						$this->db->simple_query("UPDATE uangpoint_channel SET active_point=$new_point WHERE point_id='$point_id'");
						$trx_id = $this->Rumus->getChnPointTrxID();
						$this->db->simple_query("INSERT INTO log_channel_point_transaction VALUES('$trx_id','$channel_id','','$class_id','$point','$price','$new_point',now())");

						$myip = $_SERVER['REMOTE_ADDR'];
						$plogc_id = $this->Rumus->getLogChnTrxID();
						$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Add student group list class ','$participantt','$myip',now())");
						if ($update) {
							$json = array("status" => 1,"code" => '200');
						}
						else
						{
							$json = array("status" => 0,"code" => '404');
						}
					}
				}
				else
				{
					$json = array("status" => 0,"code" => '-201');
				}
			}
			else
			{
				$json = array("status" => 0,"code" => '-202');
			}
		}
		else
		{
			$exist = $this->db->query("SELECT point_id, active_point FROM uangpoint_channel WHERE channel_id='$channel_id'")->row_array();
			if(!empty($exist)){
				$point = $exist['active_point'];
				$point_id = $exist['point_id'];
				$harga_multicast15menit = 1;

				$price = $harga_multicast15menit * ( ($getparticipant['duration'] / 60) / 15 );								
				if($point >= $price){	
					$participant_arr = json_decode($getparticipant['participant'],true);
					if(!array_key_exists('participant', $participant_arr)){
						$participant_arr['participant'] = array();
					}
					// foreach ($participantt as $key => $id) {
						array_push($participant_arr['participant'], array("id_user" => $participantt));
					// }
					$participant_arr = json_encode($participant_arr);
					$update = $this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");
					$new_point = $point-$price;
					$this->db->simple_query("UPDATE uangpoint_channel SET active_point=$new_point WHERE point_id='$point_id'");
					$trx_id = $this->Rumus->getChnPointTrxID();
					$this->db->simple_query("INSERT INTO log_channel_point_transaction VALUES('$trx_id','$channel_id','','$class_id','$point','$price','$new_point',now())");
					
					$myip = $_SERVER['REMOTE_ADDR'];
					$plogc_id = $this->Rumus->getLogChnTrxID();
					$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Add student multicast list class ','$participantt','$myip',now())");
					if ($update) {
						$json = array("status" => 1,"code" => '200');
					}
					else
					{
						$json = array("status" => 0,"code" => '404');
					}
				}
				else
				{
					$json = array("status" => 0,"code" => '-201');
				}
			}
			else
			{
				$json = array("status" => 0,"code" => '-202');
			}
		}
		echo json_encode($json);
	}

	function UpdateParticipant()
	{
		$class_id 					= $this->input->post('class_id');
		$id_user 					= $this->input->post('id_user');
		$channel_id					= $this->session->userdata('channel_id');
		$participantt['visible'] 	= "private";
		$participantt['participant'] = array();
		$participantt['participant'] = $this->input->post('list');
		$participantt = json_encode($participantt);
		
		$update = $this->db->simple_query("UPDATE tbl_class SET participant='$participantt' WHERE class_id='$class_id'");		
		if ($update) {
			$myip = $_SERVER['REMOTE_ADDR'];
			$plogc_id = $this->Rumus->getLogChnTrxID();
			$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Delete student multicast list class ','$id_user','$myip',now())");
			$json = array("status" => 1,"code" => '200');
		}
		else
		{
			$json = array("status" => 0,"code" => '404');
		}
		echo json_encode($json);
	}

	function changeStatusClass()
	{
		$pilihan 					= $this->input->post('pilihan');
		$class_id 					= $this->input->post('class_id');
		$channel_id					= $this->input->post('channel_id');

		$update = $this->db->simple_query("UPDATE tbl_class SET channel_status='$pilihan' WHERE class_id='$class_id'");		
		if ($update) {
			$myip = $_SERVER['REMOTE_ADDR'];
			$plogc_id = $this->Rumus->getLogChnTrxID();
			$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$channel_id','Memilih status channel menjadi ','','$myip',now())");
			$json = array("status" => 1,"code" => '200');
		}
		else
		{
			$json = array("status" => 0,"code" => '404');
		}
		echo json_encode($json);
	}

	function getReportingPoint()
	{
		$channel_id = $this->input->post('channel_id');

		$list_point = $this->db->query("SELECT lcpt.*, tc.* FROM log_channel_point_transaction as lcpt INNER JOIN tbl_class as tc ON lcpt.class_id=tc.class_id WHERE lcpt.channel_id='$channel_id' ORDER BY lcpt.ptrx_id DESC")->result_array();

		if ($list_point) {
			$res['status'] = true;
			$res['message'] = 'successful';
			$res['data'] = $list_point;
		}
		else
		{
			$res['status'] = false;
			$res['message'] = 'failed';
			$res['data'] = '';
		}
		echo json_encode($res);
	}

	function ConfirmPaymentMulticast($approver_id='')
	{
		$order_id = $this->input->post('trx_id');
		$tutor_id = $this->input->post('tutor_id');
		$id_user = $this->input->post('id_user');
		$class_id = $this->input->post('class_id');
		
		$update = $this->db->query("UPDATE tbl_request_multicast SET status=1 WHERE class_id='$class_id' AND id_requester='$id_user'");
        $update2 = $this->db->query("UPDATE tbl_order SET order_status=1 WHERE order_id='$order_id'");
		if ($update) {
			$q = $this->db->query("SELECT tc.*, tcp.harga, tcp.harga_cm FROM tbl_class as tc LEFT JOIN tbl_class_price as tcp ON tc.class_id=tcp.class_id WHERE tc.class_id='$class_id' AND tc.participant NOT LIKE '%\"$id_user\":\"$id_user\"%'")->row_array();

            $hargatotal         = $this->db->query("SELECT total_price FROM tbl_order WHERE order_id='$order_id'")->row_array()['total_price'];
			$hargakelas         = number_format($hargatotal, 0, ".", ".");
			$uangtutor 			= $this->db->query("SELECT * FROM uangsaku WHERE id_user=$tutor_id")->row_array();
            $balance_tutor 		= $uangtutor['active_balance'];
            $uangsaku_idtutor 	= $uangtutor['us_id'];
			$price 				= $q['harga'];
			$pricecm 			= $q['harga_cm'];                
            
            $new_balancetutor 	= $balance_tutor+$price;
            $this->db->simple_query("UPDATE uangsaku SET active_balance=$new_balancetutor WHERE us_id='$uangsaku_idtutor'");
            
			$trxid 			= $this->Rumus->getTrxID();
			$this->db->simple_query("INSERT INTO log_class_transaction VALUES('$trxid','$id_user','$class_id','$balance','$pricecm','',now())");

            $updt = 0;
            $parti              = $this->db->query("SELECT * FROM tbl_class WHERE class_id = '$class_id'")->row_array();
            $participant_arr  = json_decode($parti['participant'],true);
            foreach ($participant_arr['participant'] as $keyo => $vl) {
                $idu    = $vl['id_user'];
                if ($idu == $id_user) {
                    $participant_arr['participant'][$keyo]['st'] = 1;
                    $updt = 1;
                    break;
                }
            }
            
            if ($updt == 1) {
                $participant_arr  = json_encode($participant_arr);
                $this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");
                $this->db->simple_query("INSERT INTO tbl_class_rating(class_id, id_user, class_ack) VALUES('$class_id','$id_user',-1)");
            }
			// if(!array_key_exists('participant', $participant_arr)){
			// 	$participant_arr['participant'] = array();
			// }			

            ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
            $json_notif = "";
            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$id_user}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "10","title" => "Classmiles", "text" => "")));
            }
            if($json_notif != ""){
                $headers = array(
                    'Authorization: key=' . FIREBASE_API_KEY,
                    'Content-Type: application/json'
                    );
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;
            }
            ////------------ END -------------------------------------//
			
			$getdatauser 		= $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$id_user'")->row_array();
			$emailuser   		= $getdatauser['email'];
			$usernameuser		= $getdatauser['user_name'];

			$ch 				= curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://classmiles.com/Tools/get_c/'.$class_id);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$result 			= curl_exec($ch);
			curl_close($ch);
			
			$link 				= "https://classmiles.com/class?c=".trim($result);

			// Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
            curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_ConfirmPayment_multicast','content' => array("emailuser" => $emailuser, "usernameuser" => $usernameuser, "q" => $q, "hargakelas" => $hargakelas, "link" => $link) )));

            // Execute post
            $result = curl_exec($ch);
            curl_close($ch);
            // echo $result;


            if ($result) {
                return 1;
            }else{
                return 0;   
            }
          			
		}
		else
		{
			return 0;
		}
	}

    function ConfirmPaymentProgram($approver_id='')
    {
        $order_id 	= $this->input->post('trx_id');        
        $request_id = $this->input->post('rtp');
        $id_user 	= $this->input->post('id_user');    

        $update 	= $this->db->query("UPDATE tbl_request_program SET status=1 WHERE request_id='$request_id' AND id_requester='$id_user'");
        $update2 	= $this->db->query("UPDATE tbl_order SET order_status=1 WHERE order_id='$order_id'");
        if ($update) {
            $q = $this->db->query("SELECT trp.*, tpp.name, tpp.description, tpp.created_at, mc.channel_name FROM tbl_request_program as trp INNER JOIN tbl_price_program as tpp ON trp.program_id=tpp.list_id INNER JOIN master_channel as mc ON mc.channel_id=tpp.channel_id WHERE trp.request_id='$request_id'")->row_array();

            $list_id            = $q['program_id'];
            $hargatotal         = $this->db->query("SELECT total_price FROM tbl_order WHERE order_id='$order_id'")->row_array()['total_price'];
            $hargakelas         = number_format($hargatotal, 0, ".", ".");
            // $uangtutor          = $this->db->query("SELECT * FROM uangsaku WHERE id_user=$tutor_id")->row_array();
            // $balance_tutor      = $uangtutor['active_balance'];
            // $uangsaku_idtutor   = $uangtutor['us_id'];
            $price              = $this->db->query("SELECT price FROM tbl_price_program WHERE list_id = '$list_id'")->row_array()['price'];
            $program_id 		= $this->db->query("SELECT program_id FROM tbl_price_program WHERE list_id = '$list_id'")->row_array()['program_id'];
            
            // $new_balancetutor   = $balance_tutor+$price;
            // $this->db->simple_query("UPDATE uangsaku SET active_balance=$new_balancetutor WHERE us_id='$uangsaku_idtutor'");
            
            $trxid              = $this->Rumus->getTrxID();

            //PROSES MEMASUKAN KE CHANNEL

            //ADD TO GROUP
            $group_id           = $this->db->query("SELECT group_id FROM master_channel_program WHERE program_id = '$program_id'")->row_array()['group_id'];
            $channel_id         = $this->db->query("SELECT channel_id FROM tbl_price_program WHERE list_id = '$list_id'")->row_array()['channel_id'];            
            $gp_id 				= json_decode($group_id,true);
            foreach ($gp_id as $key => $value) {
            	$ck_group           = $this->db->query("SELECT * FROM `master_channel_group` WHERE group_id='$value' AND group_participants LIKE '%$id_user%'")->row_array();            	
            	if (empty($ck_group)) {                                
	                $a = $this->db->query("SELECT * FROM master_channel_group WHERE group_id = '$value'")->row_array();
	                $part = json_decode($a['group_participants'],true);            
	                array_push($part,$id_user);

	                $group_participants = json_encode($part);
	                $this->db->query("UPDATE master_channel_group SET group_participants = '$group_participants' WHERE group_id = '$value'");
	            }	
            }            

            //ADD TO PARTICIPANTS
            $ck_parti           = $this->db->query("SELECT * FROM master_channel_student WHERE id_user = '$id_user' AND channel_id = '$channel_id'")->row_array();
            if (empty($ck_parti)) {
                $this->db->query("INSERT INTO master_channel_student (channel_id, id_user, status, member_ship) VALUES ('$channel_id','$id_user','active','basic') ");
            }
            //END MEMASUKAN KE CHANNEL

            //CEK JIKA SUDAH TERCREATE CLASS
            $user_utc 	= '420';
	        $server_utc = $this->Rumus->getGMTOffset();
	        $interval 	= $user_utc - $server_utc;
	        
            $getschedule 	= $this->db->query("SELECT schedule FROM master_channel_program WHERE program_id = '$program_id'")->row_array()['schedule'];
            $schedule 		= json_decode($getschedule,true);
            foreach ($schedule as $ko => $sch) {
            	$date 		= $sch['eventDate'];
            	$time 		= $sch['eventTime'];
            	$durasi 	= $sch['eventDurasi'];
            	$tutor_id 	= $sch['tutor_id'];
            	$subject_id	= $sch['subject_id'];
            	$status 	= $sch['status'];

            	if ($status == 1) {
            		            							
					$st_time = DateTime::createFromFormat ('Y-m-d H:i:s', $date." ".$time.":00");
					$st_time->modify("-".$interval ." minutes");
					$st_time = $st_time->format('Y-m-d H:i:s');

            		$fn_time = DateTime::createFromFormat('Y-m-d H:i:s', $st_time);
					$fn_time->modify("+$durasi second");
					$fn_time = $fn_time->format('Y-m-d H:i:s');

					$ck_toTblClass	 = $this->db->query("SELECT * FROM tbl_class WHERE start_time = '$st_time' AND finish_time = '$fn_time' AND tutor_id='$tutor_id' AND subject_id = '$subject_id'")->row_array();					
					if (!empty($ck_toTblClass)) {
						
						$class_id 			= $ck_toTblClass['class_id'];						
						$participant_arr    = json_decode($ck_toTblClass['participant'],true);
						$ck 				= 1;
						// echo json_encode($participant_arr);
						foreach ($participant_arr['participant'] as $ke => $rt) {
							if ($rt['id_user'] == $id_user) {
								$ck = 0;
							}
						}

						if ($ck == 1) {							
							if(!array_key_exists('participant', $participant_arr)){
				                $participant_arr['participant'] = array();
				            }
				            array_push($participant_arr['participant'], array("id_user" => $id_user));
				            // echo json_encode($participant_arr);
				            $participant_arr    = json_encode($participant_arr);
				            $this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr', channel_status = '1' WHERE class_id='$class_id'");
				            $this->db->simple_query("INSERT INTO tbl_class_rating(class_id, id_user, class_ack) VALUES('$class_id','$id_user',-1)");
				        }
			            
					}
            	}
            }                       
            // END CEK CLASS

            ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
            $json_notif = "";
            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$id_user}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "10","title" => "Classmiles", "text" => "")));
            }
            if($json_notif != ""){
                $headers = array(
                    'Authorization: key=' . FIREBASE_API_KEY,
                    'Content-Type: application/json'
                    );
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;
            }
            ////------------ END -------------------------------------//
            
            $getdatauser        = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$id_user'")->row_array();
            $emailuser          = $getdatauser['email'];
            $usernameuser       = $getdatauser['user_name'];
            $isi                = 'Pembayaran kelas Anda sudah kami terima.';            

            // Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
            curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_paymentreceived','content' => array("emailuser" => $emailuser, "usernameuser" => $usernameuser, "q" => $q, "hargakelas" => $hargakelas, "tutor" => $q['channel_name'], "mapel" => $q['name'], "waktu" => $q['created_at'], "durasi" => "") )));

            // Execute post
            $result = curl_exec($ch);
            curl_close($ch);
            // echo $result;


            if ($result) {
                return 1;
            }else{
                return 0;   
            }
                    
        }
        else
        {
            return 0;
        }
    }

	function ConfirmPaymentPrivate($approver_id='')
	{
        $order_id = $this->input->post('trx_id');
		$request_id = $this->input->post('rtp');
		$tutor_id = $this->input->post('tutor_id');
		$id_user = $this->input->post('id_user');
        $admin_id = $this->session->userdata('id_user') != null ? $this->session->userdata('id_user') : $approver_id;
		
		$update = $this->db->query("UPDATE tbl_request SET approve=1 WHERE request_id='$request_id'");
		if ($update) {
			$q = $this->db->query("SELECT * FROM tbl_request as tr INNER JOIN tbl_service_price as tsp ON tr.subject_id=tsp.subject_id WHERE tsp.class_type='private' AND tsp.tutor_id='$tutor_id' AND tr.request_id='$request_id'")->row_array();

			$subject_id 		= $q['subject_id'];
			$subject_name 		= $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
            $topic              = $q['topic'];
			$tutorname 			= $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_name'];
			$hargakelas         = number_format($q['harga_cm'], 0, ".", ".");
			$uangtutor 			= $this->db->query("SELECT * FROM uangsaku WHERE id_user=$tutor_id")->row_array();
            $balance_tutor 		= $uangtutor['active_balance'];
            $uangsaku_idtutor 	= $uangtutor['us_id'];
            $durasi             = $q['duration_requested'];
			$price 				= $q['harga'];
            $hargaakhir         = ($durasi/900)*$price;
			$pricecm 			= $q['harga_cm'];
            $template 			= $q['template'];
            $new_balancetutor 	= $balance_tutor+$hargaakhir;
            $user_utc           = $q['user_utc'];
            $this->db->simple_query("UPDATE uangsaku SET active_balance=$new_balancetutor WHERE us_id='$uangsaku_idtutor'");
            
			// $trx_id 			= $this->Rumus->getTrxID();
			// $this->db->simple_query("INSERT INTO log_class_transaction VALUES('$trx_id','$id_user','$class_id','$balance','$pricecm','',now())");

			// CREATE NEW CLASS
			$duration = $q['duration_requested'];
			$start_st = $q['date_requested'];
			$final_st = $q['date_requested'];
			$final_ft = DateTime::createFromFormat('Y-m-d H:i:s', $final_st);
			$final_ft->modify("+$duration second");
			$final_ft = $final_ft->format('Y-m-d H:i:s');

			$participant['visible'] = "private";
			$participant['participant'] = array();
			$participant['participant'][]['id_user'] = $id_user;

			$participant = json_encode($participant);
			$this->db->simple_query("INSERT INTO tbl_class(subject_id,name,description,tutor_id,start_time,finish_time,participant,class_type,template_type) VALUES('{$subject_id}','{$subject_name}','{$topic}','{$tutor_id}','{$start_st}','{$final_ft}','{$participant}','private','{$template}')");
			$class_id           = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];

            //CHECK IF STUDENT KID
            $ck_ifkids  = $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user = '$id_user'")->row_array()['usertype_id'];
            if ($ck_ifkids == 'student kid') {
                $id_ortu    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$id_user'")->row_array()['parent_id'];

                $participant_st     = $this->db->query("SELECT * FROM tbl_class WHERE class_id = '$class_id'")->row_array();
                $participant_arr = json_decode($participant_st['participant'],true);
                if(!array_key_exists('participant', $participant_arr)){
                    $participant_arr['participant'] = array();
                }
                $ada = 0;                   
                foreach ($participant_arr['participant'] as $key => $value) {
                    if ($value['id_user'] == $parent_id) {
                        $ada = 1;
                    }
                }

                if ($ada == 0) {
                    array_push($participant_arr['participant'], array("id_user" => $id_ortu));                          
                }

                $participant_arr = json_encode($participant_arr);
                $this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");
            }

            ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
            $json_notif = "";
            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$id_user}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "10","title" => "Classmiles", "text" => "")));
            }
            if($json_notif != ""){
                $headers = array(
                    'Authorization: key=' . FIREBASE_API_KEY,
                    'Content-Type: application/json'
                    );
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;
            }
            ////------------ END -------------------------------------//

            $this->db->simple_query("INSERT INTO tbl_class_rating(class_id, id_user, class_ack) VALUES('$class_id','$id_user',-1)");

			$getdatauser 		= $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$id_user'")->row_array();
            if (empty($getdatauser['email'])) {
                $getparentid    = $this->db->query("SELECT * FROM tbl_profile_kid WHERE kid_id='$id_user'")->row_array();
                $getdatauser    = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getparentid[parent_id]'")->row_array();
            }
			$emailuser   		= $getdatauser['email'];
			$usernameuser		= $getdatauser['user_name'];
            			
            $server_utcc        = $this->Rumus->getGMTOffset();
            $intervall          = $user_utc - $server_utcc;
            $starttime          = DateTime::createFromFormat ('Y-m-d H:i:s',$start_st);
            $starttime->modify("+".$intervall ." minutes");
            $starttime          = $starttime->format('Y-m-d H:i:s');
            $finishtime         = DateTime::createFromFormat ('Y-m-d H:i:s',$final_ft);
            $finishtime->modify("+".$intervall ." minutes");
            $finishtime         = $finishtime->format('Y-m-d H:i:s');
            
            $datelimit          = date_create($starttime);
            $dateelimit         = date_format($datelimit, 'd/m/y');
            $harilimit          = date_format($datelimit, 'd');
            $hari               = date_format($datelimit, 'l');
            $tahunlimit         = date_format($datelimit, 'Y');
            $waktulimit         = date_format($datelimit, 'H:i');   
            $datelimit          = $dateelimit;
            $sepparatorlimit    = '/';
            $partslimit         = explode($sepparatorlimit, $datelimit);
            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

            $datelimitt          = date_create($finishtime);
            $dateelimitt         = date_format($datelimitt, 'd/m/y');
            $harilimitt          = date_format($datelimitt, 'd');
            $harit               = date_format($datelimitt, 'l');
            $tahunlimitt         = date_format($datelimitt, 'Y');
            $waktulimitt         = date_format($datelimitt, 'H:i');   
            $datelimitt          = $dateelimitt;
            $sepparatorlimitt    = '/';
            $partslimitt         = explode($sepparatorlimitt, $datelimitt);
            $bulanlimitt         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

            $seconds 			= $duration;
		    $hours 				= floor($seconds / 3600);
		    $mins 				= floor($seconds / 60 % 60);
		    $secs 				= floor($seconds % 60);
		    $durationrequested 	= sprintf('%02d:%02d', $hours, $mins);

            //LOG DEMAND
            $myip               = $_SERVER['REMOTE_ADDR'];
            $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$request_id','admin approve payment private class','$admin_id','private','$myip','141')");
            //END LOG DEMAND
            $tanggalkonfirm     = date("Y-m-d H:i:s");
            $this->db->simple_query("UPDATE tbl_order_detail SET class_id='$class_id' WHERE order_id='$order_id'");
            $this->db->simple_query("UPDATE tbl_order SET order_status=1, approval_time='$tanggalkonfirm', approver_id='$admin_id' WHERE order_id='$order_id'");

            $detaillogtr        = $this->db->query("SELECT * FROM log_trf_confirmation WHERE trx_id='$order_id'")->row_array();
            $bank               = $detaillogtr['bank_id'];
            $nominalsave        = $detaillogtr['jumlah'];
            $frombank           = $detaillogtr['norek_tr'];
            $namapemilikbank    = $detaillogtr['nama_tr'];
            $metodepembayaran   = $detaillogtr['metod_tr'];
            $detail_banktujuan  = $this->db->query("SELECT bank_name FROM master_bank WHERE bank_id='$bank'")->row_array()['bank_name'];
            $getdetail          = $this->db->query("SELECT * FROM tbl_order WHERE order_id='$order_id'")->row_array();
            $detail_hargatotal  = $getdetail['total_price'];
            $detail_hargatotal  = number_format($detail_hargatotal, 0, ".", ".");
            $detail_nominalsave = number_format($nominalsave, 0, ".", ".");
            $detail_tanggaltr   = $getdetail['last_update'];
            $detail_buyerid     = $getdetail['buyer_id'];
            $detail_buyername   = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$detail_buyerid'")->row_array()['user_name'];
            $detail_invoice     = $this->db->query("SELECT invoice_id FROM tbl_invoice WHERE order_id='$order_id'")->row_array()['invoice_id'];

            //BATAS TANGGAL PEMBAYARAN
            $dateee             = $detail_tanggaltr;
            $server_utcc        = $this->Rumus->getGMTOffset();
            $intervall          = $user_utc - $server_utcc;
            $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$dateee);
            $tanggaltransaksi->modify("+".$intervall ." minutes");
            $tanggaltransaksi   = $tanggaltransaksi->format('Y-m-d H:i:s');

            $datetime           = new DateTime($tanggaltransaksi);
            $datetime->modify('+1 hours');
            $limitdateee        = $datetime->format('Y-m-d H:i:s');

            $akhirtanggal       = date_create($limitdateee);
            $dateelimit         = date_format($akhirtanggal, 'd/m/y');
            $hariakhir          = date_format($akhirtanggal, 'd');
            $hariakhir          = date_format($akhirtanggal, 'l');
            $tahunakhir         = date_format($akhirtanggal, 'Y');
            $waktuakhir         = date_format($akhirtanggal, 'H:i');
                                                    
            $dateakhir          = $dateelimit;
            $sepparatorakhir    = '/';
            $partsakhir         = explode($sepparatorakhir, $dateakhir);
            $bulanakhir         = date("F", mktime(0, 0, 0, $partsakhir[1], $partsakhir[2], $partsakhir[0]));

            $get_detailorder    = $this->db->query("SELECT * FROM tbl_order_detail WHERE order_id='$order_id'")->result_array();
            foreach ($get_detailorder as $resultdata) {
                $request_id     = $resultdata['product_id'];
                $type           = null;
                $gettype        = $this->db->query("SELECT * FROM tbl_request WHERE request_id='$request_id'")->row_array();
                if (!empty($gettype)) {
                    $type = "private";
                }
                else
                {
                    $type = "group";
                }

                if ($type == "private") {
                    $tutor_id   = $this->db->query("SELECT tutor_id FROM tbl_request WHERE request_id='$request_id'")->row_array()['tutor_id'];
                    $subject_id = $this->db->query("SELECT subject_id FROM tbl_request WHERE request_id='$request_id'")->row_array()['subject_id'];
                    $getdataclass  = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array();          
                }
                else
                {
                    $tutor_id   = $this->db->query("SELECT tutor_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['tutor_id'];
                    $subject_id = $this->db->query("SELECT subject_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['subject_id'];
                    $getdataclass   = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request_grup as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array();          
                } 

                $iduserrequest      = $getdataclass['id_user_requester'];
                $namauser           = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['user_name'];
                $durasi             = $getdataclass['duration_requested'];
                $server_utcc        = $this->Rumus->getGMTOffset();
                $intervall          = $user_utc - $server_utcc;
                $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$getdataclass['date_requested']);
                $tanggal->modify("+".$intervall ." minutes");
                $tanggal            = $tanggal->format('Y-m-d H:i:s');
                $datelimit          = date_create($tanggal);
                $dateelimit_3       = date_format($datelimit, 'd/m/y');
                $harilimit          = date_format($datelimit, 'd');
                $hari               = date_format($datelimit, 'l');
                $tahunlimit         = date_format($datelimit, 'Y');
                $waktulimit         = date_format($datelimit, 'H:i');   
                $datelimit          = $dateelimit_3;
                $sepparatorlimit    = '/';
                $partslimit         = explode($sepparatorlimit, $datelimit);
                $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
                $seconds            = $getdataclass['duration_requested'];
                $hours              = floor($seconds / 3600);
                $mins               = floor($seconds / 60 % 60);
                $secs               = floor($seconds % 60);
                $durationrequested  = sprintf('%02d:%02d', $hours, $mins);

                $kotakdetail = 
                "<tr>
                    <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdataclass['user_name']."</td>
                    <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdataclass['subject_name']."</td>
                    <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdataclass['topic']."</td>
                    <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.", Pukul ".$waktulimit."</td>
                    <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$durationrequested."</td> 
                </tr>";   
            }                        

            $operator_name          = $this->session->userdata('user_name');
            $operator_email         = $this->session->userdata('email');

            if ($detail_banktujuan =="") {

            	$detail_banktujuan =0;
            	$operator_name =0;
            	$operator_email =0;
            	$frombank =0;
            	$namapemilikbank =0;
            	$metodepembayaran =0;
            }

            // Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
            curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_ConfirmPayment_private','content' => array("hargakelas" => $hargakelas, "emailuser" => $emailuser, "usernameuser" => $usernameuser, "tutorname" => $tutorname, "subject_name" => $subject_name, "topic" => $topic, "hari" => $hari, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "harit" => $harit, "harilimitt" => $harilimitt, "bulanlimitt" => $bulanlimitt, "tahunlimitt" => $tahunlimitt, "waktulimitt" => $waktulimitt, "durationrequested" => $durationrequested, "detail_buyername" => $detail_buyername, "detail_banktujuan" => $detail_banktujuan, "detail_hargatotal" => $detail_hargatotal, "operator_name" => $operator_name, "operator_email" => $operator_email, "detail_nominalsave" => $detail_nominalsave, "frombank" => $frombank, "namapemilikbank" => $namapemilikbank,"metodepembayaran" => $metodepembayaran,  "hariakhir" => $hariakhir, "bulanakhir" => $bulanakhir, "tahunakhir" => $tahunakhir, "waktuakhir" => $waktuakhir, "detail_invoice" => $detail_invoice, "kotakdetail" => $kotakdetail ) )));

            // Execute post
            $result = curl_exec($ch);
            curl_close($ch);
            // echo $result;


            if ($result) 
            {
                //NOTIF STUDENT
                $notif_message = json_encode( array("code" => 213));                    
                $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','','0')");

                //NOTIF TUTOR
                $notif_message = json_encode( array("code" => 214, "nama_user" => $detail_buyername, "subject_name" => $subject_name, "starttime" => $start_st));                    
                $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$tutor_id','','0')");
                return 1;
                         
            }
            else
            {
                return 0;    
            }		

		}
		else
		{
			return 0;
		}
	}

    function ConfirmPaymentGroup($approver_id='')
    {
        $order_id = $this->input->post('trx_id');
        $request_id = $this->input->post('rtp');
        $tutor_id = $this->input->post('tutor_id');
        $id_user = $this->input->post('id_user'); 
        $admin_id = $this->session->userdata('id_user') != null ? $this->session->userdata('id_user') : $approver_id;      
        
        $update = $this->db->query("UPDATE tbl_request_grup SET approve=1 WHERE request_id='$request_id'");
        if ($update) {

            $q                  = $this->db->query("SELECT * FROM tbl_request_grup as tr INNER JOIN tbl_service_price as tsp ON tr.subject_id=tsp.subject_id WHERE tsp.class_type='group' AND tsp.tutor_id='$tutor_id' AND tr.request_id='$request_id'")->row_array();            

            $subject_id         = $q['subject_id'];
            $subject_name       = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
            $topic              = $q['topic'];
            $tutorname          = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_name'];
            $hargakelas         = number_format($q['harga_cm'], 0, ".", ".");            
            // $list_student       = json_decode($q['id_friends'],true);
            $uangtutor          = $this->db->query("SELECT * FROM uangsaku WHERE id_user=$tutor_id")->row_array();
            $balance_tutor      = $uangtutor['active_balance'];
            $uangsaku_idtutor   = $uangtutor['us_id'];
            $durasi             = $q['duration_requested'];
            $price              = $q['harga'];
            $hargaakhir         = ($durasi/900)*$price;
            $pricecm            = $q['harga_cm'];                
            $template           = $q['template'];
            $price              = ($q['duration_requested']/900)*$price;
            $new_balancetutor   = $balance_tutor+$hargaakhir;
            $user_utc           = $q['user_utc'];
            $this->db->simple_query("UPDATE uangsaku SET active_balance=$new_balancetutor WHERE us_id='$uangsaku_idtutor'");

            // CREATE NEW CLASS
            $duration           = $q['duration_requested'];
            $start_st           = $q['date_requested'];
            $final_st           = $q['date_requested'];
            $final_ft           = DateTime::createFromFormat('Y-m-d H:i:s', $final_st);
            $final_ft->modify("+$duration second");
            $final_ft           = $final_ft->format('Y-m-d H:i:s');            

            $participant['visible'] = "private";
            $participant['participant'] = array();                   
            // foreach($list_student['id_friends'] as $p) {
                $participant['participant'][0]['id_user'] = $q['id_user_requester'];           
                $participant['participant'][0]['st'] = 1;
            // }
            
            $rating_participant = $participant;
            $participant = json_encode($participant);
            $this->db->simple_query("INSERT INTO tbl_class(subject_id,name,description,tutor_id,start_time,finish_time,participant,class_type,template_type) VALUES('{$subject_id}','{$subject_name}','{$topic}','{$tutor_id}','{$start_st}','{$final_ft}','{$participant}','group','{$template}')");
            $class_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];

            //CHECK IF STUDENT KID
            $ck_ifkids  = $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user = '$id_user'")->row_array()['usertype_id'];
            if ($ck_ifkids == 'student kid') {
                $id_ortu    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$id_user'")->row_array()['parent_id'];

                $participant_st     = $this->db->query("SELECT * FROM tbl_class WHERE class_id = '$class_id'")->row_array();
                $participant_arr = json_decode($participant_st['participant'],true);
                if(!array_key_exists('participant', $participant_arr)){
                    $participant_arr['participant'] = array();
                }
                $ada = 0;                   
                foreach ($participant_arr['participant'] as $key => $value) {
                    if ($value['id_user'] == $parent_id) {
                        $ada = 1;
                    }
                }

                if ($ada == 0) {
                    array_push($participant_arr['participant'], array("id_user" => $id_ortu));                          
                }

                $participant_arr = json_encode($participant_arr);
                $this->db->simple_query("UPDATE tbl_class SET participant='$participant_arr' WHERE class_id='$class_id'");
            }            

            // foreach ($rating_participant['participant'] as $key => $value) {
                $std_id = $q['id_user_requester'];
                $this->db->simple_query("INSERT INTO tbl_class_rating(class_id, id_user, class_ack) VALUES('$class_id','$std_id',-1)");

                ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
                $json_notif = "";
                $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$std_id}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
                if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                    $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "10","title" => "Classmiles", "text" => "")));
                }
                if($json_notif != ""){
                    $headers = array(
                        'Authorization: key=' . FIREBASE_API_KEY,
                        'Content-Type: application/json'
                        );
                    // Open connection
                    $ch = curl_init();

                    // Set the url, number of POST vars, POST data
                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                    // Execute post
                    $result = curl_exec($ch);
                    curl_close($ch);
                    // echo $result;
                }
                ////------------ END -------------------------------------//
            // }

            $getdatauser        = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$id_user'")->row_array();
            $emailuser          = $getdatauser['email'];
            $usernameuser       = $getdatauser['user_name'];
            
            $server_utcc        = $this->Rumus->getGMTOffset();
            $intervall          = $user_utc - $server_utcc;
            $starttime          = DateTime::createFromFormat ('Y-m-d H:i:s',$start_st);
            $starttime->modify("+".$intervall ." minutes");
            $starttime          = $starttime->format('Y-m-d H:i:s');
            $finishtime         = DateTime::createFromFormat ('Y-m-d H:i:s',$final_ft);
            $finishtime->modify("+".$intervall ." minutes");
            $finishtime         = $finishtime->format('Y-m-d H:i:s');

            $datelimit          = date_create($starttime);
            $dateelimit         = date_format($datelimit, 'd/m/y');
            $harilimit          = date_format($datelimit, 'd');
            $hari               = date_format($datelimit, 'l');
            $tahunlimit         = date_format($datelimit, 'Y');
            $waktulimit         = date_format($datelimit, 'H:i');   
            $datelimit          = $dateelimit;
            $sepparatorlimit    = '/';
            $partslimit         = explode($sepparatorlimit, $datelimit);
            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

            $datelimitt          = date_create($finishtime);
            $dateelimitt         = date_format($datelimitt, 'd/m/y');
            $harilimitt          = date_format($datelimitt, 'd');
            $harit               = date_format($datelimitt, 'l');
            $tahunlimitt         = date_format($datelimitt, 'Y');
            $waktulimitt         = date_format($datelimitt, 'H:i');   
            $datelimitt          = $dateelimitt;
            $sepparatorlimitt    = '/';
            $partslimitt         = explode($sepparatorlimitt, $datelimitt);
            $bulanlimitt         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

            $seconds            = $duration;
            $hours              = floor($seconds / 3600);
            $mins               = floor($seconds / 60 % 60);
            $secs               = floor($seconds % 60);
            $durationrequested  = sprintf('%02d:%02d', $hours, $mins);

            //LOG DEMAND
            $myip               = $_SERVER['REMOTE_ADDR'];
            $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$request_id','admin approve payment group class','$admin_id','group','$myip','142')");
            //END LOG demand
            $tanggalkonfirm     = date("Y-m-d H:i:s");
            $this->db->simple_query("UPDATE tbl_order_detail SET class_id='$class_id' WHERE order_id='$order_id'");            
            $this->db->simple_query("UPDATE tbl_order SET approval_time='$tanggalkonfirm', approver_id='$admin_id', order_status=1 WHERE order_id='$order_id'");

            $detaillogtr        = $this->db->query("SELECT * FROM log_trf_confirmation WHERE trx_id='$order_id'")->row_array();
            $bank               = $detaillogtr['bank_id'];
            $nominalsave        = $detaillogtr['jumlah'];
            $frombank           = $detaillogtr['norek_tr'];
            $namapemilikbank    = $detaillogtr['nama_tr'];
            $metodepembayaran   = $detaillogtr['metod_tr'];
            $detail_banktujuan  = $this->db->query("SELECT bank_name FROM master_bank WHERE bank_id='$bank'")->row_array()['bank_name'];
            $getdetail          = $this->db->query("SELECT * FROM tbl_order WHERE order_id='$order_id'")->row_array();
            $detail_hargatotal  = $getdetail['total_price'];
            $detail_hargatotal  = number_format($detail_hargatotal, 0, ".", ".");
            $detail_nominalsave = number_format($nominalsave, 0, ".", ".");
            $detail_tanggaltr   = $getdetail['last_update'];
            $detail_buyerid     = $getdetail['buyer_id'];
            $detail_buyername   = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$detail_buyerid'")->row_array()['user_name'];
            $detail_invoice     = $this->db->query("SELECT invoice_id FROM tbl_invoice WHERE order_id='$order_id'")->row_array()['invoice_id'];

            //BATAS TANGGAL PEMBAYARAN
            $dateee             = $detail_tanggaltr;            
            $server_utcc        = $this->Rumus->getGMTOffset();
            $intervall          = $user_utc - $server_utcc;
            $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$dateee);
            $tanggaltransaksi->modify("+".$intervall ." minutes");
            $tanggaltransaksi   = $tanggaltransaksi->format('Y-m-d H:i:s');

            $datetime           = new DateTime($tanggaltransaksi);
            $datetime->modify('+1 hours');
            $limitdateee        = $datetime->format('Y-m-d H:i:s');

            $akhirtanggal       = date_create($limitdateee);
            $dateelimit         = date_format($akhirtanggal, 'd/m/y');
            $hariakhir          = date_format($akhirtanggal, 'd');
            $hariakhir          = date_format($akhirtanggal, 'l');
            $tahunakhir         = date_format($akhirtanggal, 'Y');
            $waktuakhir         = date_format($akhirtanggal, 'H:i');
                                                    
            $dateakhir          = $dateelimit;
            $sepparatorakhir    = '/';
            $partsakhir         = explode($sepparatorakhir, $dateakhir);
            $bulanakhir         = date("F", mktime(0, 0, 0, $partsakhir[1], $partsakhir[2], $partsakhir[0]));

            $get_detailorder    = $this->db->query("SELECT * FROM tbl_order_detail WHERE order_id='$order_id'")->result_array();
            foreach ($get_detailorder as $resultdata) {
                $request_id     = $resultdata['product_id'];
                $type           = null;
                $gettype        = $this->db->query("SELECT * FROM tbl_request WHERE request_id='$request_id'")->row_array();
                if (!empty($gettype)) {
                    $type = "private";
                }
                else
                {
                    $type = "group";
                }

                if ($type == "private") {
                    $tutor_id   = $this->db->query("SELECT tutor_id FROM tbl_request WHERE request_id='$request_id'")->row_array()['tutor_id'];
                    $subject_id = $this->db->query("SELECT subject_id FROM tbl_request WHERE request_id='$request_id'")->row_array()['subject_id'];
                    $getdataclass  = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array();          
                }
                else
                {
                    $tutor_id   = $this->db->query("SELECT tutor_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['tutor_id'];
                    $subject_id = $this->db->query("SELECT subject_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['subject_id'];
                    $getdataclass   = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request_grup as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array();          
                } 
                
                $iduserrequest      = $getdataclass['id_user_requester'];
                $namauser           = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['user_name'];
                $durasi             = $getdataclass['duration_requested'];
                $server_utcc        = $this->Rumus->getGMTOffset();
                $intervall          = $user_utc - $server_utcc;
                $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$getdataclass['date_requested']);
                $tanggal->modify("+".$intervall ." minutes");
                $tanggal            = $tanggal->format('Y-m-d H:i:s');
                $datelimit          = date_create($tanggal);
                $dateelimit_3       = date_format($datelimit, 'd/m/y');
                $harilimit          = date_format($datelimit, 'd');
                $hari               = date_format($datelimit, 'l');
                $tahunlimit         = date_format($datelimit, 'Y');
                $waktulimit         = date_format($datelimit, 'H:i');   
                $datelimit          = $dateelimit_3;
                $sepparatorlimit    = '/';
                $partslimit         = explode($sepparatorlimit, $datelimit);
                $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
                $seconds            = $getdataclass['duration_requested'];
                $hours              = floor($seconds / 3600);
                $mins               = floor($seconds / 60 % 60);
                $secs               = floor($seconds % 60);
                $durationrequested  = sprintf('%02d:%02d', $hours, $mins);

                $kotakdetail = 
                "<tr>
                    <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdataclass['user_name']."</td>
                    <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdataclass['subject_name']."</td>
                    <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdataclass['topic']."</td>
                    <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.", Pukul ".$waktulimit."</td>
                    <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$durationrequested."</td> 
                </tr>";   
            }                        

            $operator_name          = $this->session->userdata('user_name');
            $operator_email         = $this->session->userdata('email');
            if ($detail_banktujuan =="") {

            	$detail_banktujuan =0;
            	$operator_name =0;
            	$operator_email =0;
            	$frombank =0;
            	$namapemilikbank =0;
            	$metodepembayaran =0;
            }
            // $ALL_data_friends = array();
            // foreach($list_student['id_friends'] as $listfriends) {
            //     $userid_friends = $listfriends['id_user'];
            //     $data_friends  = $this->db->query("SELECT email,user_name FROM tbl_user WHERE id_user='$userid_friends'")->row_array();
            //     $ALL_data_friends[] = $data_friends;

            // }
            
            // Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
            curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_ConfirmPayment_group','content' => array("emailuser" => $emailuser, "usernameuser" => $usernameuser, "tutorname" => $tutorname, "subject_name" => $subject_name, "topic" => $topic, "hari" => $hari, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "harit" => $harit, "harilimitt" => $harilimitt, "bulanlimitt" => $bulanlimitt, "tahunlimitt" => $tahunlimitt, "waktulimitt" => $waktulimitt, "durationrequested" => $durationrequested, "detail_buyername" => $detail_buyername, "detail_banktujuan" => $detail_banktujuan, "detail_hargatotal" => $detail_hargatotal, "operator_name" => $operator_name, "operator_email" => $operator_email, "detail_nominalsave" => $detail_nominalsave, "frombank" => $frombank, "namapemilikbank" => $namapemilikbank, "metodepembayaran" => $metodepembayaran, "hariakhir" => $hariakhir, "bulanakhir" => $bulanakhir, "tahunakhir" => $tahunakhir, "waktuakhir" => $waktuakhir, "detail_invoice" => $detail_invoice, "kotakdetail" => $kotakdetail ) )));

            // Execute post
            $result = curl_exec($ch);
            curl_close($ch);
            // echo $result;


          if ($result) {
                //NOTIF STUDENT
                $notif_message = json_encode( array("code" => 212));                    
                $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','','0')");    
                
                //NOTIF STUDENT
                $notif_message = json_encode( array("code" => 213));                    
                $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$userid_friends','','0')");

                //NOTIF TUTOR
                $notif_message = json_encode( array("code" => 214, "nama_user" => $data_friends['user_name'], "subject_name" => $subject_name, "starttime" => $start_st));                    
                $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$tutor_id','','0')");
                return 1;
          }else{
               return 0;  
          }
        }
        else
        {
            return 0;
        }
    }

    public function ConfirmUnderPayment()
    {
        $order_id = $this->input->post('trx_id');
        $tutor_id = $this->input->post('tutor_id');
        $id_user = $this->input->post('id_user');
        $class_id = $this->input->post('class_id');
        $type = $this->input->post('type');
        $request_id = $this->input->post('rtp');
                
        $update = $this->db->query("UPDATE tbl_order SET order_status=-3 WHERE order_id='$order_id'");
        if ($update) {
            
            $getdatauser        = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$id_user'")->row_array();
            $emailuser          = $getdatauser['email'];
            $usernameuser       = $getdatauser['user_name'];

            $gettblinvoice      = $this->db->query("SELECT * FROM tbl_invoice WHERE order_id='$order_id'")->row_array();
            $invoice_id         = $gettblinvoice['invoice_id'];
            $total_price        = number_format($gettblinvoice['total_price'], 0, ".", ".");
            $underpayment       = number_format($gettblinvoice['underpayment'], 0, ".", ".");

            //NOTIF STUDENT
            $notif_message = json_encode( array("code" => 215));                    
            $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','','0')");

            // Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
            curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_underpayment','content' => array("emailuser" => $emailuser, "usernameuser" => $usernameuser, "invoice_id" => $invoice_id, "total_price" => $total_price, "underpayment" => $underpayment ) )));

            // Execute post
            $result = curl_exec($ch);
            curl_close($ch);
            // echo $result;


            if ($result) 
             {
                return 1;
            }
            else{
                return 0;   
            }  
        }
        else
        {
            return 0;
        }
    }

	function deletePaymentMulticast()
	{
		$trx_id = $this->input->post('rtp');
		
		$delete = $this->db->query("UPDATE tbl_purchaseclass SET status=-1 WHERE trx_id='$trx_id'");        
		if ($delete) {

            //LOG DEMAND
            // $myip               = $_SERVER['REMOTE_ADDR'];
            // $this->db->query("INSERT INTO log_demand (request_id,ket,type,ip_from) VALUES ('$request_id','admin reject payment multicast class','multicast','$myip')");
            //END LOG DEMAND

			return 1;
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal menghapus');
			return 0;
		}
	}

    public function showDetailClass()
    {
        $request_id = $this->input->post('request_id');
        $type       = $this->input->post('type');
        $id_user    = $this->input->post('id_user');
        $user_utc   = $this->session->userdata('user_utc');
        if ($type == "private") {
            $selectdata         = $this->db->query("SELECT tu.user_name, ms.subject_name, tr.* FROM tbl_request as tr INNER JOIN master_subject as ms ON tr.subject_id=ms.subject_id INNER JOIN tbl_user as tu ON tu.id_user=tr.tutor_id WHERE tr.request_id='$request_id'")->row_array();
            $iduserrequest      = $selectdata['id_user_requester'];
            $namauser           = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['user_name'];
            $user_name          = $selectdata['user_name'];
            $subject_name       = $selectdata['subject_name'];
            $date_start         = $selectdata['date_requested'];
            $server_utc         = $this->Rumus->getGMTOffset();
            $interval           = $user_utc - $server_utc;
            $starttime          = DateTime::createFromFormat('Y-m-d H:i:s',$date_start);
            $starttime->modify("+".$interval." minutes");
            $starttime          = $starttime->format('Y-m-d H:i:s');            

            $datelimit          = date_create($starttime);
            $dateelimit         = date_format($datelimit, 'd/m/y');
            $harilimit          = date_format($datelimit, 'd');
            $hari               = date_format($datelimit, 'l');
            $tahunlimit         = date_format($datelimit, 'Y');
            $waktulimit         = date_format($datelimit, 'H:i');   
            $datelimit          = $dateelimit;
            $sepparatorlimit    = '/';
            $partslimit         = explode($sepparatorlimit, $datelimit);
            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

            $seconds            = $selectdata['duration_requested'];
            $hours              = floor($seconds / 3600);
            $mins               = floor($seconds / 60 % 60);
            $secs               = floor($seconds % 60);
            $durationrequested  = sprintf('%02d:%02d', $hours, $mins);

            $json = array("status" => 1,"message" => "sukses", "name_tutor" => $user_name, "nama_pengajak" => $namauser, "subject_name" => $subject_name, "waktu" => $hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.", Pukul ".$waktulimit, "durasi" => $durationrequested);   
        }
        else if($type == "group")
        {
            $selectdatagrup      = $this->db->query("SELECT tu.user_name, ms.subject_name, trg.* FROM tbl_request_grup as trg INNER JOIN master_subject as ms ON trg.subject_id=ms.subject_id INNER JOIN tbl_user as tu ON tu.id_user=trg.tutor_id WHERE trg.request_id='$request_id'")->row_array();
            $iduserrequest      = $selectdatagrup['id_user_requester'];
            $namauser           = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['user_name'];
            $idfriends          = json_decode($selectdatagrup['id_friends'],true);
            // print_r($idfriends);
            $statusapprove = null;
            foreach($idfriends['id_friends'] as $va => $vl) {
                $iduserapprove = $vl['id_user'];                      
                if ($iduserapprove == $id_user) {
                    $statusapprove = $vl['status'];    
                }                
            } 

            $user_name          = $selectdatagrup['user_name'];
            $subject_name       = $selectdatagrup['subject_name'];
            $date_start         = $selectdatagrup['date_requested'];
            $approve            = $selectdatagrup['approve'];
            $server_utc         = $this->Rumus->getGMTOffset();
            $interval           = $user_utc - $server_utc;
            $starttime          = DateTime::createFromFormat('Y-m-d H:i:s',$date_start);
            $starttime->modify("+".$interval ." minutes");
            $starttime          = $starttime->format('Y-m-d H:i:s');            

            $datelimit          = date_create($starttime);
            $dateelimit         = date_format($datelimit, 'd/m/y');
            $harilimit          = date_format($datelimit, 'd');
            $hari               = date_format($datelimit, 'l');
            $tahunlimit         = date_format($datelimit, 'Y');
            $waktulimit         = date_format($datelimit, 'H:i');   
            $datelimit          = $dateelimit;
            $sepparatorlimit    = '/';
            $partslimit         = explode($sepparatorlimit, $datelimit);
            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

            $seconds            = $selectdatagrup['duration_requested'];
            $hours              = floor($seconds / 3600);
            $mins               = floor($seconds / 60 % 60);
            $secs               = floor($seconds % 60);
            $durationrequested  = sprintf('%02d:%02d', $hours, $mins);
            
            if ($statusapprove != null) {
                $json = array("status" => 1,"message" => "sukses", "name_tutor" => $user_name, "nama_pengajak" => $namauser, "subject_name" => $subject_name, "waktu" => $hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.', Pukul '.$waktulimit, "durasi" => $durationrequested, "statusapprove" => $statusapprove, "approve" => $approve);
            }
            else
            {
                $json = array("status" => 1,"message" => "sukses", "name_tutor" => $user_name, "nama_pengajak" => $namauser, "subject_name" => $subject_name, "waktu" => $hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.', Pukul '.$waktulimit, "durasi" => $durationrequested, "statusapprove" => $statusapprove, "approve" => $approve);
            }
                                    
        }
        else
        {
            $json = array("status" => 0,"message" => "failed");          
        }
        echo json_encode($json);
    }

    public function submitMethodPaymentPaypal($order_id)
    {
        // Set variables for paypal form
        $returnURL = base_url().'Paypal/success'; //payment success url
        $cancelURL = base_url().'Paypal/cancel'; //payment cancel url
        $notifyURL = base_url().'Paypal/ipn'; //ipn url
        
        // Get product data
        $product = $this->Product->getOrder($order_id);
        
        // Get current user ID from session
        $userID = $product['buyer_id'];
        $pricepackage = $product['total_price'];
        $tbl_ordtl = $this->db->query("SELECT * FROM tbl_order_detail WHERE order_id = '$order_id'")->row_array();
        $sub_productid  = substr($tbl_ordtl['product_id'],0,1);

        $endpoint = 'live';
        $access_key = 'bb9c37f4dc4f0bd1369be76116c25e14';

        // Initialize CURL:
        $ch = curl_init('http://apilayer.net/api/'.$endpoint.'?access_key='.$access_key.'');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Store the data:
        $json = curl_exec($ch);
        curl_close($ch);

        // Decode JSON response:
        $exchangeRates = json_decode($json, true);
        
        $dollar = $exchangeRates['quotes']['USDIDR'];
        $price_convert = $pricepackage / $dollar;

        if ($price_convert >= 6.8) {
            $fee = $price_convert * 0.05;
            $total = $price_convert+$fee;
        }
        else
        {
            $fee = 0.34;
            $total = $price_convert+$fee;
        }
        echo $price_convert.' <br>';
        echo $fee.' <br>';
        echo $total;
        return false;
        // Add fields to paypal form
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);
        $this->paypal_lib->add_field('item_name', 'Request Class '.$order_id);
        $this->paypal_lib->add_field('custom', $userID);
        $this->paypal_lib->add_field('item_number',  '1');
        $this->paypal_lib->add_field('amount',  $total);
        $this->paypal_lib->add_field('product_id',  $order_id);
          
        // Load paypal form
        $this->paypal_lib->paypal_auto_form();
    }

    // function PaypalSuccess(){
    //     // Get the transaction data
    //     $paypalInfo = $this->input->get();
        
    //     $data['item_number'] = $paypalInfo['item_number']; 
    //     $data['txn_id'] = $paypalInfo["tx"];
    //     $data['payment_amt'] = $paypalInfo["amt"];
    //     $data['currency_code'] = $paypalInfo["cc"];
    //     $data['status'] = $paypalInfo["st"];
        
    //     // Pass the transaction data to view
    //     $data['sideactive'] = 'success_payment';
    //     $this->load->view('inc/header');
    //     $this->load->view('content/success', $data);
    //     $this->load->view('inc/footer');
    // }
     
    // function PaypalCancel(){
    //     // Load payment failed view
    //     $data['sideactive'] = 'cancel_payment';
    //     $this->load->view('inc/header');
    //     $this->load->view('content/cancel', $data);
    //     // $this->load->view('inc/footer');
    // }

    // function PaypalIpn(){
    //     // Paypal return transaction details array
    //     $paypalInfo = $this->input->post();

    //     $data['user_id']        = $paypalInfo['custom'];
    //     $data['product_id']     = $paypalInfo["item_number"];
    //     $data['txn_id']         = $paypalInfo["txn_id"];
    //     $data['payment_gross']  = $paypalInfo["mc_gross"];
    //     $data['currency_code']  = $paypalInfo["mc_currency"];
    //     $data['payer_email']    = $paypalInfo["payer_email"];
    //     $data['payment_status'] = $paypalInfo["payment_status"];

    //     $paypalURL  = $this->paypal_lib->paypal_url;
    //     $result     = $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
        
    //     // Check whether the payment is verified
    //     if(preg_match("/VERIFIED/i",$result)){
    //         // Insert the transaction data into the database
    //         $this->Process_model->addTransaction($data);
    //     }
    // }

    public function submitMethodPayment()
    {
        $id_user = $this->session->userdata('id_user');
        if ($id_user == "") {
            $id_user = $this->input->post('id_user');
        }
        $order_id = $this->input->post('order_id');
        $payment_type = $this->input->post('payment_type');
        $bank_id = $this->input->post('bank_id');
        $nama_akun = $this->input->post('nama_akun');
        $nomer_rekening = $this->input->post('nomer_rekening');
        $user_utc = $this->input->post('user_utc');
        $email = $this->input->post('email');
        if ($user_utc==null) {
            $user_utc = $this->session->userdata('user_utc');
        }
        if ($email==null) {
            $email = $this->session->userdata('email');
        }
        $kotaktd = null;
        $config2 = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' =>'finance@meetaza.com',
            'smtp_pass' => 'xmbmpuwuxlzmcjsl', 
            'mailtype' => 'html',                    
            'charset' => 'iso-8859-1'
        );

        $cekrek = $this->db->query("SELECT * FROM tbl_rekening WHERE id_user='$id_user' AND nomer_rekening='$nomer_rekening' AND bank_id='$bank_id'")->row_array();
        if (empty($cekrek)) {
            $bank_name = $this->db->query("SELECT bank_name FROM master_bank WHERE bank_id='$bank_id'")->row_array()['bank_name'];
            $addbank = $this->db->query("INSERT INTO tbl_rekening (id_user,nama_akun,nomer_rekening,bank_id,nama_bank) VALUES ('$id_user','$nama_akun','$nomer_rekening','$bank_id','$bank_name')");    
            $lastidrek  = $this->db->select('id_rekening')->order_by('id_rekening','desc')->limit(1)->get('tbl_rekening')->row('id_rekening');        
            if ($addbank) {
                $updatemethod = $this->db->query("UPDATE tbl_invoice SET payment_type='$payment_type', id_rekening='$lastidrek' WHERE order_id='$order_id'");

                // $this->db->query("UPDATE tbl_order SET order_status=2 WHERE order_id='$order_id'");

                
                $get_detail     = $this->db->query("SELECT * FROM tbl_order_detail WHERE order_id='$order_id'")->result_array();
                foreach($get_detail as $product_id) {
                    $sub_productid  = substr($product_id['product_id'],0,1);

                    $request_id     = $product_id['product_id'];
                    $type           = $sub_productid == '7' ? 'private' : ( $sub_productid == '8' ? 'group': 'multicast' );
                    $get_datadetail = $this->db->query("SELECT * FROM tbl_order as tor INNER JOIN tbl_invoice as ti ON tor.order_id=ti.order_id WHERE tor.order_id='$order_id'")->row_array();
                    $invoice_id     = $get_datadetail['invoice_id'];
                    $total_price    = $get_datadetail['total_price'];
                    $UnixCode       = $get_datadetail['unix_code'];
                    $hargatotal     = $total_price+$UnixCode;
                    $harga_kelas    = $total_price-$UnixCode;

                    if ($type == "private") {
                        $tutor_id   = $this->db->query("SELECT tutor_id FROM tbl_request WHERE request_id='$request_id'")->row_array()['tutor_id'];
                        $subject_id = $this->db->query("SELECT subject_id FROM tbl_request WHERE request_id='$request_id'")->row_array()['subject_id'];
                        $getdataclass  = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array();

                        $iduserrequest  = $getdataclass['id_user_requester'];  
                        $daterequesttt  = $getdataclass['date_requested'];    
                        $durationrequest= $getdataclass['duration_requested']; 

                        $harga_k            = $this->db->query("SELECT harga_cm FROM tbl_service_price WHERE subject_id='$subject_id' AND class_type='$type' AND tutor_id='$tutor_id'")->row_array()['harga_cm'];
                        $hargaperkelas      = ($durationrequest/900)*$harga_k;    
                        $hargaperkelas      = number_format($hargaperkelas, 0, ".", ".");     
                    }
                    else if($type == "group")
                    {
                        $tutor_id       = $this->db->query("SELECT tutor_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['tutor_id'];
                        $subject_id     = $this->db->query("SELECT subject_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['subject_id'];
                        $getdataclass   = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request_grup as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array();
                        $iduserrequest  = $getdataclass['id_user_requester'];     
                        $daterequesttt  = $getdataclass['date_requested'];      
                        $durationrequest= $getdataclass['duration_requested'];

                        $harga_k            = $this->db->query("SELECT harga_cm FROM tbl_service_price WHERE subject_id='$subject_id' AND class_type='$type' AND tutor_id='$tutor_id'")->row_array()['harga_cm'];
                        $hargaperkelas      = ($durationrequest/900)*$harga_k;    
                        $hargaperkelas      = number_format($hargaperkelas, 0, ".", ".");     
                    } 
                    else if($type == "program")
                    {                        
                        $list_id        = $this->db->query("SELECT program_id FROM tbl_request_program WHERE request_id = '$request_id'")->row_array()['program_id'];
                                                
                        $getdataclass   = $this->db->query("SELECT * FROM tbl_price_program WHERE list_id = '$list_id'")->row_array();
                        $iduserrequest  = $this->db->query("SELECT id_requester FROM tbl_request_program WHERE request_id = '$request_id'")->row_array()['id_requester'];    
                        $daterequesttt  = $getdataclass['created_at'];      
                        $durationrequest= 0;
                                       
                        $kalimat=$getdataclass['description'];
                        $jumlahkarakter=30;
                        $cetak = substr($kalimat,$jumlahkarakter,1);
                        if($cetak !=" "){
                            while($cetak !=" "){
                                $i=1;
                                $jumlahkarakter=$jumlahkarakter+$i;
                                $kalimat= $getdataclass['description'];
                                $cetak = substr($kalimat,$jumlahkarakter,1);
                            }
                        }
                        $cetak = substr($kalimat,0,$jumlahkarakter);

                        $hargaperkelas  = number_format($getdataclass['price'], 0, ".", ".");  
                        $email_username = $getdataclass['name'];
                        $email_subject  = '';
                        $email_topic    = $cetak;
                    } 
                    else
                    {
                        $product_idclass = $this->db->query("SELECT class_id FROM tbl_request_multicast WHERE request_id='$request_id'")->row_array()['class_id'];
                        $tutor_id       = $this->db->query("SELECT tutor_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['tutor_id'];
                        $subject_id     = $this->db->query("SELECT subject_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['subject_id'];
                        $getdataclass   = $this->db->query("SELECT tu.user_name, ms.subject_name, tc.description as topic, tc.start_time, tc.finish_time, trm.created_at as date_requested, trm.id_requester as id_user_requester FROM tbl_class as tc INNER JOIN tbl_user as tu on tu.id_user=tc.tutor_id INNER JOIN master_subject as ms ON ms.subject_id=tc.subject_id INNER JOIN tbl_request_multicast as trm ON trm.class_id=tc.class_id WHERE tc.class_id='$product_idclass'")->row_array();  
                        $iduserrequest  = $getdataclass['id_user_requester']; 
                        $daterequesttt  = $getdataclass['date_requested']; 
                        $duratioon      = strtotime($getdataclass['start_time']) - strtotime($getdataclass['finish_time']);
                        $durationrequest= $duratioon;  

                        $harga_k        = $this->db->query("SELECT harga_cm FROM tbl_class_price WHERE class_id='$product_idclass'")->row_array()['harga_cm']; 
                        $hargaperkelas  = number_format($harga_k, 0, ".", ".");     
                    }                    
                    
                    $iduserrequest      = $this->db->query("SELECT buyer_id FROM tbl_order WHERE order_id='$order_id'")->row_array()['buyer_id'];  
                    $getdataa           = $this->db->query("SELECT user_name, email, usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
                    $usertype_id        = $getdataa['usertype_id'];
                    if ($usertype_id == 'student kid') {
                        $getParent  = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$buyer_id'")->row_array()['parent_id'];
                        $getdata    = $this->db->query("SELECT user_name, email, usertype_id FROM tbl_user WHERE id_user='$getParent'")->row_array();
                        $email      = $getdata['email'];
                        $namauser   = $getdata['user_name'];
                    }
                    else
                    {
                        $email      = $getdataa['email'];
                        $namauser   = $getdataa['user_name'];
                    }

                    $namauser           = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['user_name'];
                    $durasi             = $durationrequest;
                    $total_price        = number_format($total_price, 0, ".", ".");
                    $hargatotal         = number_format($hargatotal, 0, ".", ".");
                    $server_utcc        = $this->Rumus->getGMTOffset();
                    $intervall          = $user_utc - $server_utcc;
                    $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s','2018-02-06 06:15:00');
                    $tanggal->modify("+".$intervall ." minutes");
                    $tanggal            = $tanggal->format('Y-m-d H:i:s');
                    $datelimit          = date_create($tanggal);
                    $dateelimit         = date_format($datelimit, 'd/m/y');
                    $harilimit          = date_format($datelimit, 'd');
                    $hari               = date_format($datelimit, 'l');
                    $tahunlimit         = date_format($datelimit, 'Y');
                    $waktulimit         = date_format($datelimit, 'H:i');   
                    $datelimit          = $dateelimit;
                    $sepparatorlimit    = '/';
                    $partslimit         = explode($sepparatorlimit, $datelimit);
                    $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
                    $seconds            = $durationrequest;
                    $hours              = floor($seconds / 3600);
                    $mins               = floor($seconds / 60 % 60);
                    $secs               = floor($seconds % 60);
                    $durationrequested  = sprintf('%02d:%02d', $hours, $mins);
                    
                    $kotaktd = $kotaktd."<tr>
                        <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdataclass['user_name']."</td>
                        <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdataclass['subject_name']."</td>
                        <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$getdataclass['topic']."</td>
                        <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.", Pukul ".$waktulimit."</td>
                        <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$durationrequested."</td> 
                    </tr>";            
                }                 

                $last_update        = $this->db->query("SELECT payment_due FROM tbl_order WHERE order_id='$order_id'")->row_array()['payment_due'];
                $dateee             = $last_update;
                $server_utcc        = $this->Rumus->getGMTOffset();
                $intervall          = $user_utc - $server_utcc;
                $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$dateee);
                $tanggaltransaksi->modify("+".$intervall ." minutes");
                
                $hariakhirr = $tanggaltransaksi->format('d');
                $hariakhir = $tanggaltransaksi->format('l');
                $tahunakhir = $tanggaltransaksi->format('Y');
                $waktuakhir = $tanggaltransaksi->format('H:i');
                $bulanakhir = $tanggaltransaksi->format('F');                                                

                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_submitMethodPayment','content' => array("email" => $email, "invoice_id" => $invoice_id, "namauser" => $namauser, "hargakelas" => $harga_kelas, "UnixCode" => $UnixCode, "hargatotal" => $total_price, "hariakhir" => $hariakhir, "hariakhirr" => $hariakhirr, "bulanakhir" => $bulanakhir, "tahunakhir" => $tahunakhir, "waktuakhir" => $waktuakhir, "kotaktd" => $kotaktd) )));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                echo $result;


                // if ($result) {
                    $json = array("status" => 1,"message" => "sukses" ,"statuz" => true);
                // }else{
                //     $json = array("status" => 0,"message" => "failed","statuz" => false);
                // }              
            }
            else
            {
                $json = array("status" => 0,"message" => "failed","statuz" => false);
            }
        }
        else
        {
            $idrek = $cekrek['id_rekening'];
            $updatemethod = $this->db->query("UPDATE tbl_invoice SET payment_type='$payment_type', id_rekening='$idrek' WHERE order_id='$order_id'");
            if ($updatemethod) {                
                $this->db->query("UPDATE tbl_order SET order_status=2 WHERE order_id='$order_id'");

              $kotaktd = ""; 
              $get_detail     = $this->db->query("SELECT * FROM tbl_order_detail WHERE order_id='$order_id'")->result_array();
              foreach($get_detail as $product_id) {
                  $sub_productid  = substr($product_id['product_id'],0,1);            

                  $request_id     = $product_id['product_id'];
                  $type           = $sub_productid == '7' ? 'private' : ( $sub_productid == '8' ? 'group': ( $sub_productid == '9' ? 'multicast' : 'program' ));
                  $get_datadetail = $this->db->query("SELECT * FROM tbl_order as tor INNER JOIN tbl_invoice as ti ON tor.order_id=ti.order_id WHERE tor.order_id='$order_id'")->row_array();
                  $invoice_id     = $get_datadetail['invoice_id'];
                  $total_price    = $get_datadetail['total_price'];
                  $UnixCode       = $get_datadetail['unix_code'];
                  $hargatotal     = $total_price+$UnixCode;
                  $harga_kelas    = $total_price-$UnixCode;
                  if ($type == "private") {
                      $tutor_id   = $this->db->query("SELECT tutor_id FROM tbl_request WHERE request_id='$request_id'")->row_array()['tutor_id'];
                      $subject_id = $this->db->query("SELECT subject_id FROM tbl_request WHERE request_id='$request_id'")->row_array()['subject_id'];
                      $getdataclass  = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array();                            
                      $iduserrequest  = $getdataclass['id_user_requester'];  
                      $daterequesttt  = $getdataclass['date_requested'];    
                      $durationrequest= $getdataclass['duration_requested']; 

                      $harga_k            = $this->db->query("SELECT harga_cm FROM tbl_service_price WHERE subject_id='$subject_id' AND class_type='$type' AND tutor_id='$tutor_id'")->row_array()['harga_cm'];
                      $hargaperkelas      = ($durationrequest/900)*$harga_k;    
                      $hargaperkelas      = number_format($hargaperkelas, 0, ".", "."); 

                      $email_username = $getdataclass['user_name'];
                      $email_subject  = $getdataclass['subject_name'];
                      $email_topic    = $getdataclass['topic'];    
                  }
                  else if($type == "group")
                  {
                      $tutor_id       = $this->db->query("SELECT tutor_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['tutor_id'];
                      $subject_id     = $this->db->query("SELECT subject_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['subject_id'];
                      $getdataclass   = $this->db->query("SELECT tu.user_name, tsp.*, tr.*, ms.subject_name FROM (tbl_user as tu INNER JOIN tbl_request_grup as tr ON tu.id_user=tr.tutor_id) INNER JOIN tbl_service_price as tsp ON tsp.subject_id=tr.subject_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tsp.subject_id='$subject_id' AND tsp.tutor_id='$tutor_id' AND tsp.class_type='$type' AND tr.request_id='$request_id'")->row_array();
                      $iduserrequest  = $getdataclass['id_user_requester'];     
                      $daterequesttt  = $getdataclass['date_requested'];      
                      $durationrequest= $getdataclass['duration_requested'];

                      $harga_k            = $this->db->query("SELECT harga_cm FROM tbl_service_price WHERE subject_id='$subject_id' AND class_type='$type' AND tutor_id='$tutor_id'")->row_array()['harga_cm'];
                      $hargaperkelas      = ($durationrequest/900)*$harga_k;    
                      $hargaperkelas      = number_format($hargaperkelas, 0, ".", ".");    

                      $email_username = $getdataclass['user_name'];
                      $email_subject  = $getdataclass['subject_name'];
                      $email_topic    = $getdataclass['topic']; 
                  }
                  else if($type == "program")
                  {                        
                      $list_id        = $this->db->query("SELECT program_id FROM tbl_request_program WHERE request_id = '$request_id'")->row_array()['program_id'];
                                                
                      $getdataclass   = $this->db->query("SELECT * FROM tbl_price_program WHERE list_id = '$list_id'")->row_array();
                      $iduserrequest  = $this->db->query("SELECT id_requester FROM tbl_request_program WHERE request_id = '$request_id'")->row_array()['id_requester'];    
                      $daterequesttt  = $getdataclass['created_at'];      
                      $durationrequest= 0;
                                       
                      $kalimat=$getdataclass['description'];
                      $jumlahkarakter=30;
                      $cetak = substr($kalimat,$jumlahkarakter,1);
                      if($cetak !=" "){
                          while($cetak !=" "){
                              $i=1;
                              $jumlahkarakter=$jumlahkarakter+$i;
                              $kalimat= $getdataclass['description'];
                              $cetak = substr($kalimat,$jumlahkarakter,1);
                          }
                      }
                      $cetak = substr($kalimat,0,$jumlahkarakter);

                      $hargaperkelas  = number_format($getdataclass['price'], 0, ".", ".");  
                      $email_username = $getdataclass['name'];
                      $email_subject  = '';
                      $email_topic    = $cetak;
                  } 
                  else
                  {
                      $product_idclass = $this->db->query("SELECT class_id FROM tbl_request_multicast WHERE request_id='$request_id'")->row_array()['class_id'];
                      // $tutor_id       = $this->db->query("SELECT tutor_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['tutor_id'];
                      // $subject_id     = $this->db->query("SELECT subject_id FROM tbl_request_grup WHERE request_id='$request_id'")->row_array()['subject_id'];
                      $getdataclass   = $this->db->query("SELECT tu.user_name, ms.subject_name, tc.description as topic, tc.start_time, tc.finish_time, trm.created_at as date_requested, trm.id_requester as id_user_requester FROM tbl_class as tc INNER JOIN tbl_user as tu on tu.id_user=tc.tutor_id INNER JOIN master_subject as ms ON ms.subject_id=tc.subject_id INNER JOIN tbl_request_multicast as trm ON trm.class_id=tc.class_id WHERE tc.class_id='$product_idclass'")->row_array();  
                      $iduserrequest  = $getdataclass['id_user_requester']; 
                      $daterequesttt  = $getdataclass['date_requested']; 
                      $duratioon      = strtotime($getdataclass['start_time']) - strtotime($getdataclass['finish_time']);
                      $durationrequest= $duratioon;  

                      $harga_k        = $this->db->query("SELECT harga_cm FROM tbl_class_price WHERE class_id='$product_idclass'")->row_array()['harga_cm']; 
                      $hargaperkelas  = number_format($harga_k, 0, ".", "."); 
                      $email_username = $getdataclass['user_name'];
                      $email_subject  = $getdataclass['subject_name'];
                      $email_topic    = $getdataclass['topic'];
                  } 

                  $iduserrequest      = $this->db->query("SELECT buyer_id FROM tbl_order WHERE order_id='$order_id'")->row_array()['buyer_id'];
                  $user_utc           = $this->session->userdata('user_utc');                    
                  $namauser           = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['user_name'];
                  $durasi             = $durationrequest;
                  $total_price        = number_format($total_price, 0, ".", ".");
                  $hargatotal         = number_format($hargatotal, 0, ".", ".");
                  $harga_kelas        = number_format($harga_kelas, 0, ".", ".");
                  $server_utcc        = $this->Rumus->getGMTOffset();
                  $intervall          = $user_utc - $server_utcc;
                  $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$daterequesttt);
                  $tanggal->modify("+".$intervall ." minutes");
                  $tanggal            = $tanggal->format('Y-m-d H:i:s');
                  $datelimit          = date_create($tanggal);
                  $dateelimit         = date_format($datelimit, 'd/m/y');
                  $harilimit          = date_format($datelimit, 'd');
                  $hari               = date_format($datelimit, 'l');
                  $tahunlimit         = date_format($datelimit, 'Y');
                  $waktulimit         = date_format($datelimit, 'H:i');   
                  $datelimit          = $dateelimit;
                  $sepparatorlimit    = '/';
                  $partslimit         = explode($sepparatorlimit, $datelimit);
                  $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
                  $seconds            = $durationrequest;
                  $hours              = floor($seconds / 3600);
                  $mins               = floor($seconds / 60 % 60);
                  $secs               = floor($seconds % 60);
                  $durationrequested  = sprintf('%02d:%02d', $hours, $mins);
                    
                  $kotaktd = $kotaktd."<tr>
                      <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$email_username."</td>
                      <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$email_subject."</td>
                      <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$email_topic."</td>
                      <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.", Pukul ".$waktulimit."</td>
                      <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$durationrequested."</td> 
                  </tr>";            
              }                 

              $last_update        = $this->db->query("SELECT payment_due FROM tbl_order WHERE order_id='$order_id'")->row_array()['payment_due'];
              $dateee             = $last_update;
              $server_utcc        = $this->Rumus->getGMTOffset();
              $intervall          = $user_utc - $server_utcc;
              $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$dateee);
              $tanggaltransaksi->modify("+".$intervall ." minutes");
                
              $hariakhirr = $tanggaltransaksi->format('d');
              $hariakhir = $tanggaltransaksi->format('l');
              $tahunakhir = $tanggaltransaksi->format('Y');
              $waktuakhir = $tanggaltransaksi->format('H:i');
              $bulanakhir = $tanggaltransaksi->format('F');
                                                
              // Open connection
              $ch = curl_init();

              // Set the url, number of POST vars, POST data
              curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
              curl_setopt($ch, CURLOPT_POST, true);
              // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_submitMethodPayment','content' => array("email" => $email, "invoice_id" => $invoice_id, "namauser" => $namauser, "hargakelas" => $harga_kelas, "UnixCode" => $UnixCode, "hargatotal" => $total_price, "hariakhir" => $hariakhir, "hariakhirr" => $hariakhirr, "bulanakhir" => $bulanakhir, "tahunakhir" => $tahunakhir, "waktuakhir" => $waktuakhir, "kotaktd" => $kotaktd) )));

              // Execute post
              $result = curl_exec($ch);
              curl_close($ch);
              // echo $result;


              // if ($result) {
                    $json = array("status" => 1,"message" => "sukses" ,"statuz" => true);
                // }else{
                //     $json = array("status" => 0,"message" => "failed","statuz" => false);
                // }                
            }
            else
            {
                $json = array("status" => 0,"message" => "failed","statuz" => false);
            }            
        }        
        
        echo json_encode($json);
    }

    function submitOrder()
    {
        $order_id = $this->Rumus->gen_order_id();
        $UnixCode = $this->Rumus->RandomUnixCode();  
        $user_utc = $this->session->userdata('user_utc');
        $total_price = 0;
        $buyer_id = "";
        $hargatotal = 0;
        $date_expired = "";
        if(!empty($_POST['orderKrnj'])) {
            $server_utc         = $this->Rumus->getGMTOffset();
            $intervall          = $user_utc - $server_utc;
            $datenow            = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
            $datenow->modify("+3600 seconds");
            $datenow            = strtotime($datenow->format('Y-m-d H:i:s'));
            
            $datenow            = date('Y-m-d H:i:s', $datenow);
            $date_expired       = $datenow;
            
            
            foreach($_POST['orderKrnj'] as $krnj_id) {
                $v    = $this->db->query("SELECT tk.*, tr.date_requested as start_time_private, trg.date_requested as start_time_grup, tc.start_time as start_time_multicast, trp.created_at as start_time_program FROM tbl_keranjang as tk LEFT JOIN tbl_request as tr ON tk.request_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON tk.request_grup_id=trg.request_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tc ON trm.class_id=tc.class_id ) ON tk.request_multicast_id=trm.request_id LEFT JOIN tbl_request_program as trp ON tk.request_program_id=trp.request_id WHERE krnj_id='$krnj_id'")->row_array();
                $price          = $v['price'];

                $start_time     = $v['start_time_private'] != null ? $v['start_time_private'] : ( $v['start_time_grup'] != null ? $v['start_time_grup'] : ( $v['start_time_multicast'] != null ? $v['start_time_multicast'] : $v['start_time_program'] ));

                if(strtotime($start_time) <= $datenow){
                    $datenow = strtotime($start_time);
                }

                if ($buyer_id == "") {                    
                    $buyer_id   = $buyer_id+$v['id_user'];
                }                
                // $product_id     = $g_keranjang['request_id'];
                $product_id     = $v['request_id'] != null ? $v['request_id'] : ( $v['request_grup_id'] != null ? $v['request_grup_id'] : ( $v['request_multicast_id'] != null ? $v['request_multicast_id'] : $v['request_program_id']));
                $type_product   = $v['request_id'] != null ? 'private' : ( $v['request_grup_id'] != null ? 'group': ( $v['request_multicast_id'] != null ? 'multicast' : 'program' ));
                if ($type_product=='multicast') {
                    $product_idclass = $this->db->query("SELECT class_id FROM tbl_request_multicast WHERE request_id='$product_id'")->row_array()['class_id'];
                    $product_properties = $this->db->query("SELECT description FROM tbl_class WHERE class_id='$product_idclass'")->row_array()['description'];

                    $get_pricetutor = $this->db->query("SELECT harga FROM tbl_class_price WHERE class_id='$product_idclass'")->row_array()['harga'];
                    $total_price    = $total_price+$price;                
                    $save_to_tbl_order_detail = $this->db->query("INSERT INTO tbl_order_detail (order_id,class_id,product_id,product_properties,total_amt,init_price,discount,final_price,final_pricetutor,TYPE) VALUES ('$order_id','$product_idclass','$product_id','$product_properties','','$price','0','$price','$get_pricetutor','$type_product')");  
                }
                else if($type_product == 'program'){
                    $list_id            = $this->db->query("SELECT program_id FROM tbl_request_program WHERE request_id = '$product_id'")->row_array()['program_id'];
                    $product_properties = $this->db->query("SELECT name FROM tbl_price_program WHERE list_id = '$list_id'")->row_array()['name'];

                    $total_price    = $total_price+$price; 
                    $save_to_tbl_order_detail = $this->db->query("INSERT INTO tbl_order_detail (order_id,product_id,product_properties,total_amt,init_price,discount,final_price,final_pricetutor,TYPE) VALUES ('$order_id','$product_id','$product_properties','','$price','0','$price','$price','$type_product')");
                }
                else
                {
                    if ($type_product == "private") {
                        $duration_requested = $this->db->query("SELECT duration_requested FROM tbl_request WHERE request_id='$product_id'")->row_array()['duration_requested'];
                        $subject_id = $this->db->query("SELECT subject_id FROM tbl_request WHERE request_id='$product_id'")->row_array()['subject_id'];
                        $tutorid = $this->db->query("SELECT tutor_id FROM tbl_request WHERE request_id='$product_id'")->row_array()['tutor_id'];
                        $this->db->simple_query("UPDATE tbl_request SET approve=2 WHERE request_id='$product_id'");
                    }
                    else
                    {
                        $duration_requested = $this->db->query("SELECT duration_requested FROM tbl_request_grup WHERE request_id='$product_id'")->row_array()['duration_requested'];
                        $subject_id = $this->db->query("SELECT subject_id FROM tbl_request_grup WHERE request_id='$product_id'")->row_array()['subject_id'];
                        $tutorid = $this->db->query("SELECT tutor_id FROM tbl_request_grup WHERE request_id='$product_id'")->row_array()['tutor_id'];
                        $this->db->simple_query("UPDATE tbl_request_grup SET approve=2 WHERE request_id='$product_id'");
                    }
                    $get_pricetutor = $this->db->query("SELECT harga FROM tbl_service_price where tutor_id='$tutorid' AND subject_id='$subject_id' AND class_type='$type_product'")->row_array()['harga'];

                    $total_pricetutor = ($duration_requested/900)*$get_pricetutor;
                    $get_product = $this->db->query("SELECT tr.topic as tr_topic, trg.topic as trg_topic FROM tbl_request as tr LEFT JOIN tbl_request_grup as trg ON tr.request_id=trg.request_id where tr.request_id='$product_id' OR trg.request_id='$product_id'")->row_array();                    
                    $product_properties = $get_product['tr_topic'] != null ? $get_product['tr_topic'] : $get_product['trg_topic'];

                    $total_price    = $total_price+$price;
                    $save_to_tbl_order_detail = $this->db->query("INSERT INTO tbl_order_detail (order_id,product_id,product_properties,total_amt,init_price,discount,final_price,final_pricetutor,TYPE) VALUES ('$order_id','$product_id','$product_properties','','$price','0','$price','$total_pricetutor','$type_product')");  
                }        
                $this->db->query("DELETE FROM tbl_keranjang WHERE krnj_id='$krnj_id'");
                // echo $product_id.' - '.$type_product.' - '.$product_properties;
            }
            // $datenow-= 300;

            $datenow = date('Y-m-d H:i:s', $datenow);         
        }
        
        // return false;
        $hargatotal    			= $total_price+$UnixCode;
        $invoice_id             = $this->Rumus->gen_invoice_id($order_id);
        $nowdate                = date("Y-m-d H:i:s");

        $save_to_tbl_order      = $this->db->query("INSERT INTO tbl_order (order_id,total_price,unix_code,buyer_id,order_status, payment_due, created_at) VALUES ('$order_id','$hargatotal','$UnixCode','$buyer_id','0','$date_expired','$nowdate')");          
        if ($save_to_tbl_order) {
            $save_to_tbl_invoice    = $this->db->query("INSERT INTO tbl_invoice (invoice_id,order_id,payment_type,total_price,underpayment) VALUES ('$invoice_id','$order_id',null,'$hargatotal','$hargatotal')");

            ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
            $json_notif = "";
            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$buyer_id}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "40","title" => "Classmiles", "text" => "")));
            }
            if($json_notif != ""){
                $headers = array(
                    'Authorization: key=' . FIREBASE_API_KEY,
                    'Content-Type: application/json'
                    );
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;
            }
            ////------------ END -------------------------------------//

            if ($save_to_tbl_invoice) {
                $this->session->set_flashdata('mes_alert','success');
                $this->session->set_flashdata('mes_display','block');
                $this->session->set_flashdata('mes_message','Silahkan transfer ke salah satu rekening berikut');
                redirect('/DetailOrder?order_id='.$order_id);
            }
            else
            {
                $this->session->set_flashdata('mes_alert','danger');
                $this->session->set_flashdata('mes_display','block');
                $this->session->set_flashdata('mes_message','Gagal membayar');
                redirect('/');
            }
        }
        else
        {
            $this->session->set_flashdata('mes_alert','danger');
            $this->session->set_flashdata('mes_display','block');
            $this->session->set_flashdata('mes_message','Gagal membayar');
            redirect('/');
        }
    }

    public function QuizAddNew()
    {
        $name           = $this->input->post('name');
        $description    = $this->input->post('description');
        $subject_id     = $this->input->post('subject_id');
        $validity       = $this->input->post('validity');
        $invalidity     = $this->input->post('invalidity');
        $duration       = $this->input->post('duration');
        $template_type  = $this->input->post('template_type');
        $tutor_id       = $this->session->userdata('id_user');
        $user_utc       = $this->session->userdata('user_utc');

        $validity       = date("Y-m-d H:i:s", strtotime($validity));
        $invalidity     = date("Y-m-d H:i:s", strtotime($invalidity));

        $addquiz   = $this->db->query("INSERT INTO master_banksoal (subject_id, name, description, tutor_id, validity, invalidity, duration, template_type) VALUES ('$subject_id','$name','$description','$tutor_id','$validity','$invalidity','$duration','$template_type')");
        if ($addquiz) {
            // $this->session->set_flashdata('mes_alert','success');
            // $this->session->set_flashdata('mes_display','block');
            // $this->session->set_flashdata('mes_message',"Pembuatan Kuis berhasil, silahkan tambahkan soal ke kuis tersebut."); 
            // redirect('/tutor/Quiz');
            $json = array("status" => 1,"message" => "Pembuatan Kuis berhasil, silahkan tambahkan soal ke kuis tersebut." ,"statuz" => true);
        }
        else
        {
            // $this->session->set_flashdata('mes_alert','failed');
            // $this->session->set_flashdata('mes_display','block');
            // $this->session->set_flashdata('mes_message',"Terjadi kesalahan system, harap coba lagi"); 
            // redirect('/tutor/Quiz');
            $json = array("status" => 0,"message" => "Terjadi kesalahan system saat mengubah data, harap coba lagi","statuz" => false);
        }
        echo json_encode($json);
    }

    public function QuizEdit()
    {
        $bsid           = $this->input->post('bsid');
        $name           = $this->input->post('name');
        $description    = $this->input->post('description');
        $subject_id     = $this->input->post('subject_id');
        $validity       = $this->input->post('validity');
        $invalidity     = $this->input->post('invalidity');
        $duration       = $this->input->post('duration');
        $template_type  = $this->input->post('template_type');
        $tutor_id       = $this->session->userdata('id_user');
        $user_utc       = $this->session->userdata('user_utc');

        $validity       = date("Y-m-d H:i:s", strtotime($validity));
        $invalidity     = date("Y-m-d H:i:s", strtotime($invalidity));

        $addquiz   = $this->db->query("UPDATE master_banksoal SET subject_id='$subject_id',name='$name',description='$description',validity='$validity',invalidity='$invalidity',duration='$duration',template_type='$template_type' WHERE bsid='$bsid' AND tutor_id='$tutor_id'");
        if ($addquiz) {
            // $this->session->set_flashdata('mes_alert','success');
            // $this->session->set_flashdata('mes_display','block');
            // $this->session->set_flashdata('mes_message',"Data Quiz berhasil diubah"); 
            // redirect('/tutor/Quiz');
            $json = array("status" => 1,"message" => "Data Quiz berhasil diubah" ,"statuz" => true);
        }
        else
        {
            // $this->session->set_flashdata('mes_alert','failed');
            // $this->session->set_flashdata('mes_display','block');
            // $this->session->set_flashdata('mes_message',"Terjadi kesalahan system saat mengubah data, harap coba lagi"); 
            // redirect('/tutor/Quiz');
            $json = array("status" => 0,"message" => "Terjadi kesalahan system saat mengubah data, harap coba lagi","statuz" => false);
        }
        echo json_encode($json);
    }

    function deleteQuiz()
    {
        $rtp = $this->input->post('rtp');

        $delete = $this->db->query("DELETE FROM master_banksoal WHERE bsid='$rtp'");
        if ($delete) {
            $json = array("status" => 1,"message" => "sukses" ,"statuz" => true);
        }
        else
        {
            $json = array("status" => 0,"message" => "failed","statuz" => false);
        }
        
        echo json_encode($json);
    }

    public function saveQuestionQuizArchive()
    {                 
        $tutor_id = $this->session->userdata('id_user');
        $question = $this->db->escape_str($this->input->post('question'));
        $options_type = $this->input->post('options_type');        
        $flag = $this->input->post('options_permission');
        $options = $this->db->escape_str($this->input->post('choice_value'));
        $answer_value = $this->db->escape_str($this->input->post('answer'));

        $html = str_get_html('<html lang="fr"><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head><body>'.$question.'</body></hml>');

        foreach ($html->find('img[src]') as $key => $value) {
            $img_name = 'QQ'.$this->Rumus->RandomString().time().'.jpg';
            file_put_contents('./aset/_temp_images/'.$img_name,file_get_contents($value->src));

            $config['image_library'] = 'gd2';
            $config['source_image'] = './aset/_temp_images/'.$img_name;
            $config['quality']    = '10%';
            $config['maintain_ratio'] = TRUE;

            $this->image_lib->initialize($config); 
            $this->image_lib->resize();

            $this->Rumus->sendToCDN('banksoal', $img_name, null, 'aset/_temp_images/'.$img_name );
            $new_name = CDN_URL.USER_IMAGE_CDN_URL.base64_encode('banksoal/'.$img_name);
            $html->find('img[src]', $key)->src = $new_name;
        }
        $question = $html->find('body', 0)->innertext;

        $saveQuiz = $this->db->query("INSERT INTO master_banksoal_archive (text_soal,options_type,options,answer_value,tutor_creator,flag) VALUES ('$question','$options_type','$options','$answer_value','$tutor_id','$flag')");             
        if ($saveQuiz) {
            $json = array("status" => 1,"message" => "sukses" ,"statuz" => true);    
        }
        else
        {
            $json = array("status" => 0,"message" => "failed","statuz" => false);
        }
        echo json_encode($json);
    }

    public function saveTempImageQuiz()
    {
        $bsid = $this->input->post('bsid');
        $img_name = $bsid.$this->Rumus->RandomString().time().'.jpg';
        $imgfile = $this->input->post('imgfile');
        file_put_contents('./aset/_temp_images/'.$img_name,file_get_contents($imgfile));

        $config['image_library'] = 'gd2';
        $config['source_image'] = './aset/_temp_images/'.$img_name;
        $config['width']    = 256;
        $config['maintain_ratio'] = FALSE;
        $config['height']   = 256;

        $this->image_lib->initialize($config); 

        $this->image_lib->resize();
        $this->Rumus->sendToCDN('banksoal', $img_name, null, 'aset/_temp_images/'.$img_name );
        
        // $this->session->set_userdata('user_image_temp', CDN_URL.USER_IMAGE_CDN_URL.base64_encode('banksoal/'.$img_name));
        $json = array("status" => 1,"message" => "sukses" ,"link_image" => CDN_URL.USER_IMAGE_CDN_URL.base64_encode('banksoal/'.$img_name));
        echo json_encode($json);
    }
    
    public function saveImageQuestionQuiz()
    {        
        $bsid = $this->input->post('bsid');                
        $question = $this->db->escape_str($this->input->post('question'));
        $options_type = $this->input->post('options_type');        
        $options = $this->input->post('choice_value');
        $answer_value = $this->input->post('answer');

        $html = str_get_html('<html lang="fr"><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head><body>'.$question.'</body></hml>');

        foreach ($html->find('img[src]') as $key => $value) {
            $img_name = $bsid.'QQ'.$this->Rumus->RandomString().time().'.jpg';
            file_put_contents('./aset/_temp_images/'.$img_name,file_get_contents($value->src));

            $config['image_library'] = 'gd2';
            $config['source_image'] = './aset/_temp_images/'.$img_name;
            $config['quality']    = '10%';
            $config['maintain_ratio'] = TRUE;

            $this->image_lib->initialize($config); 
            $this->image_lib->resize();

            $this->Rumus->sendToCDN('banksoal', $img_name, null, 'aset/_temp_images/'.$img_name );
            $new_name = CDN_URL.USER_IMAGE_CDN_URL.base64_encode('banksoal/'.$img_name);
            $html->find('img[src]', $key)->src = $new_name;
        }
        $question = $html->find('body', 0)->innertext;

        $saveQuiz = $this->db->query("INSERT INTO master_banksoal_detailTEMP (bsid,text_soal,options_type,options,answer_value) VALUES ('$bsid','$question','$options_type','$options','$answer_value')");        
        if ($saveQuiz) {
            $json = array("status" => 1,"message" => "sukses" ,"statuz" => true);    
        }
        else
        {
            $json = array("status" => 0,"message" => "failed","statuz" => false);
        }
        echo json_encode($json);
    }

    public function deleteQuestionQuiz()
    {
        $bsid = $this->input->post('bsid');
        $soal_uid = $this->input->post('soal_uid');

        $select = $this->db->query("SELECT list_soal FROM master_banksoal WHERE bsid='$bsid'")->row_array();
        if (empty($select)) {
            $json = array("status" => 0,"message" => "failed","statuz" => false);   
        }
        else
        {
            $list_soal = $select['list_soal'];            
            $list_soal = json_decode($list_soal, true);
            if (($key = array_search($soal_uid, $list_soal)) !== false) {
                unset($list_soal[$key]);
            }
            $c = json_encode($list_soal);
            $delete = $this->db->query("UPDATE master_banksoal SET list_soal = '$c' WHERE bsid = '$bsid'");
            if ($delete) {
                $json = array("status" => 1,"message" => "sukses" ,"statuz" => true);
            }
            else
            {
                $json = array("status" => 0,"message" => "failed","statuz" => false);
            }
        }
        echo json_encode($json);
    }

    public function saveEssayQuestionQuizArchive()
    {        
        $tutor_id = $this->session->userdata('id_user');              
        $question = $this->input->post('question');
        $options_type = $this->input->post('options_type');        
        $flag = $this->input->post('options_permission');       

        $html = str_get_html('<html lang="fr"><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head><body>'.$question.'</body></hml>');

        foreach ($html->find('img[src]') as $key => $value) {
            $img_name = 'QQ'.$this->Rumus->RandomString().time().'.jpg';
            file_put_contents('./aset/_temp_images/'.$img_name,file_get_contents($value->src));

            $config['image_library'] = 'gd2';
            $config['source_image'] = './aset/_temp_images/'.$img_name;
            $config['quality']    = '10%';
            $config['maintain_ratio'] = TRUE;

            $this->image_lib->initialize($config); 
            $this->image_lib->resize();

            $this->Rumus->sendToCDN('banksoal', $img_name, null, 'aset/_temp_images/'.$img_name );
            $new_name = CDN_URL.USER_IMAGE_CDN_URL.base64_encode('banksoal/'.$img_name);
            $html->find('img[src]', $key)->src = $new_name;
        }
        $question = $html->find('body', 0)->innertext;

        $saveQuiz = $this->db->query("INSERT INTO master_banksoal_archive (text_soal,options_type,tutor_creator,flag) VALUES ('$question','$options_type','$tutor_id','$flag')");        
        if ($saveQuiz) {
            $json = array("status" => 1,"message" => "sukses" ,"statuz" => true);    
        }
        else
        {
            $json = array("status" => 0,"message" => "failed","statuz" => false);
        }
        echo json_encode($json);
    }

    public function saveImageQuestionQuizArchive()
    {        
        $tutor_id = $this->session->userdata('id_user');              
        $question = $this->input->post('question');
        $options_type = $this->input->post('options_type');        
        $flag = $this->input->post('options_permission'); 
        $options = $this->input->post('choice_value');
        $answer_value = $this->input->post('answer');

        $html = str_get_html('<html lang="fr"><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head><body>'.$question.'</body></hml>');

        foreach ($html->find('img[src]') as $key => $value) {
            $img_name = $bsid.'QQ'.$this->Rumus->RandomString().time().'.jpg';
            file_put_contents('./aset/_temp_images/'.$img_name,file_get_contents($value->src));

            $config['image_library'] = 'gd2';
            $config['source_image'] = './aset/_temp_images/'.$img_name;
            $config['quality']    = '10%';
            $config['maintain_ratio'] = TRUE;

            $this->image_lib->initialize($config); 
            $this->image_lib->resize();

            $this->Rumus->sendToCDN('banksoal', $img_name, null, 'aset/_temp_images/'.$img_name );
            $new_name = CDN_URL.USER_IMAGE_CDN_URL.base64_encode('banksoal/'.$img_name);
            $html->find('img[src]', $key)->src = $new_name;
        }
        $question = $html->find('body', 0)->innertext;

        $saveQuiz = $this->db->query("INSERT INTO master_banksoal_archive (text_soal,options_type,options,answer_value,tutor_creator,flag) VALUES ('$question','$options_type','$options','$answer_value','$tutor_id','$flag')");        
        if ($saveQuiz) {
            $json = array("status" => 1,"message" => "sukses" ,"statuz" => true);    
        }
        else
        {
            $json = array("status" => 0,"message" => "failed","statuz" => false);
        }
        echo json_encode($json);
    }

    public function deleteQuestionArchive()
    {
        $soal_uid = $this->input->post('soal_uid');

        $delete = $this->db->query("DELETE FROM master_banksoal_archive WHERE soal_uid='$soal_uid'");
        if ($delete) {
            $json = array("status" => 1,"message" => "sukses" ,"statuz" => true);
        }
        else
        {
            $json = array("status" => 0,"message" => "failed","statuz" => false);
        }
        
        echo json_encode($json);
    }

    public function changeTutorClassChannel()
    {
        $class_id       = $this->input->post('class_id');
        $tutor_select   = $this->input->post('tutor_select');
        $channel_id     = $this->input->post('channel_id');

        $cekclass       = $this->db->query("SELECT * FROM tbl_class WHERE class_id='$class_id' AND channel_id='$channel_id'")->row_array();
        if (empty($cekclass)) {
            $json = array("status" => 0,"message" => "Terjadi kesalahan system saat mengubah data, harap coba lagi","statuz" => false);
        }
        else
        {
            $updttr   = $this->db->query("UPDATE tbl_class SET tutor_id='$tutor_select' WHERE class_id='$class_id' AND channel_id='$channel_id'");
            if ($updttr) {
                $json = array("status" => 1,"message" => "Tutor berhasil terubah" ,"statuz" => true);
            }
            else
            {                
                $json = array("status" => 0,"message" => "Terjadi kesalahan system saat mengubah data, harap coba lagi","statuz" => false);
            }
        }
        echo json_encode($json);
    }

    public function addchooseArchiveQuiz()
    {
        $soal_uid       = $this->input->post('soal_uid');
        $bsid           = $this->input->post('bsid');
        $tutor_id       = $this->session->userdata('id_user');

        $cekquiz       = $this->db->query("SELECT * FROM master_banksoal WHERE bsid='$bsid' AND tutor_id='$tutor_id'")->row_array();
        if (empty($cekquiz)) {
            $json = array("status" => 0,"message" => "Terjadi kesalahan system, harap coba lagi","statuz" => false);
        }
        else
        {
            $list_soal  = json_decode($cekquiz['list_soal'], TRUE);
            if (is_array($list_soal)) {
                if(!in_array($soal_uid, $list_soal)){
                    array_push($list_soal, $soal_uid);
                    $stat = 1;
                }
                else
                {
                    $stat = 3;
                }
            }
            else
            {
                $list_soal = array($soal_uid);                
            }
            $last_list  = json_encode($list_soal);
            $updttr     = $this->db->query("UPDATE master_banksoal SET list_soal='$last_list' WHERE bsid='$bsid' AND tutor_id='$tutor_id'");
            if ($updttr) {
                if ($stat == 3) {
                    $json = array("status" => 2,"message" => "Question Tersebut sudah terdapat pada list Quiz anda" ,"statuz" => true);
                }
                else
                {
                    $json = array("status" => 1,"message" => "Berhasil menambah soal ke quiz anda" ,"statuz" => true);
                }
            }
            else
            {                
                $json = array("status" => 0,"message" => "Terjadi kesalahan system saat mengubah data, harap coba lagi","statuz" => false);
            }
        }
        echo json_encode($json);
    }

	public function haha($value='')
	{
        $bsid = "suka";
        $true_html = $this->db->query("SELECT  * FROM master_banksoal_detailTEMP WHERE soal_uid = 4")->row_array()['text_soal'];
		$html = str_get_html('<html lang="fr"><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head><body>'.$true_html.'</body></hml>');
        $a = $html->find('img[src]');
        // echo count($a);
        // echo $a[0]->src;
        foreach ($a as $key => $value) {
            $img_name = $bsid.'QQ'.$this->Rumus->RandomString().time().'.jpg';
            file_put_contents('./aset/_temp_images/'.$img_name,file_get_contents($value->src));

            $config['image_library'] = 'gd2';
            $config['source_image'] = './aset/_temp_images/'.$img_name;
            $config['quality']    = '10%';
            $config['maintain_ratio'] = TRUE;

            $this->image_lib->initialize($config); 
            $this->image_lib->resize();

            $this->Rumus->sendToCDN('banksoal', $img_name, null, 'aset/_temp_images/'.$img_name );
            $new_name = CDN_URL.USER_IMAGE_CDN_URL.base64_encode('banksoal/'.$img_name);
            $html->find('img[src]', $key)->src = $new_name;
        }
        $this->db->simple_query("UPDATE master_banksoal_detailTEMP SET text_soal='".$html->find('body', 0)->innertext."' WHERE soal_uid=4 ");
        // $html->find('body', 0)->innertext;
	}    

}
