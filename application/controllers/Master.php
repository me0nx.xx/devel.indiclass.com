<?php 
	class Master extends CI_Controller{


		function __construct(){
			parent::__construct();
			$this->load->library('recaptcha');
			$this->load->library('email');
			$this->session->set_userdata('color','blue');
			$this->load->helper(array('Form', 'Cookie', 'String'));
			// $this->load->helper('language');
			// $this->load_lang($this->session->userdata('lang'));
			// $this->load_lang($this->session->userdata('lang'));
			$this->load->library('Mobile_Detect');
			$detect = new Mobile_Detect();
			// Check for any mobile device.
			if ($detect->isMobile()){
				define('MOBILE', 'mobile/');
			}else{
				define('MOBILE', '');
			}
		}

		public function google_login_old()
		{
			$data = json_decode($this->input->post('google_data'),true);
			$check_exist = $this->db->query("SELECT * FROM tbl_user WHERE email='".$data['U3']."'")->row_array();
			if(empty($check_exist)){
				$new['email'] = $data['U3'];
				$new['user_image'] = $data['Paa'];
				$new['user_name'] = $data['ig'];
				$new['first_name'] = $data['ofa'];
				$new['last_name'] = $data['wea'];
				$new['usertype_id'] = $this->input->post('usertype_id');
				$this->session->set_flashdata('google_data',$new);
				redirect('RegisterEmail');
				return null;
			}
			$userutc = $this->input->post('user_utc_g');
			$this->session->set_userdata('user_utc', $userutc);
			$rets = $this->M_login->google_login($data['U3']);
			$this->session->set_flashdata('rets',$rets);

			if ($rets == 0) {
				// $load = $this->lang->line('wrongpassword');
				// $this->session->set_flashdata('mes_alert','danger');
				// $this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message', $load);
				// $this->session->set_userdata('message','wrongpassword');
				//echo '<script>alert("Maaf User Tersebut Tidak ada");</script>';
				$this->session->set_userdata('code', '101');
				redirect('/');
			}
			if ($this->session->userdata('usertype_id') == 'student') {
				
				if ($this->session->userdata('status')==0) {
					// echo $this->session->userdata('status');

					// $load = $this->lang->line('checkemail');
					// $this->session->set_flashdata('mes_alert','warning');
					// $this->session->set_flashdata('mes_display','block');
					// // $this->session->set_flashdata('mes_message',$load);

					// $load = $this->lang->line('youremail3');
					// $load2 = $this->lang->line('here');
					// $load3 = $this->lang->line('emailresend');
					// $this->session->set_flashdata('mes_message', $load."<a href=".base_url('/Master/resendEmaill')."> ".$load2."</a> ".$load3);	
					// $this->session->set_userdata('message', 'checkemail');
					$this->session->set_userdata('code', '189');
					// $this->session->unset_userdata('code'); 
					
					redirect('/');
				}
				else
				{
					$iduser = $this->session->userdata('id_user');
					$cek_data= $this->db->query("SELECT tu.usertype_id, tu.user_religion, tu.user_address, tu.user_image, tps.jenjang_id, tps.school_id FROM tbl_user as tu INNER JOIN tbl_profile_student as tps WHERE tu.id_user='$iduser' AND tps.student_id='$iduser'")->row_array();
					$get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$cek_data['jenjang_id']."'")->row_array()['jenjang_name'];
					$this->session->set_userdata('jenjang_type',$get_jenjangtype);
					
					
					if ($cek_data['jenjang_id'] == '15') {
						$this->session->set_userdata('status_user', 'umum');	
						$cek_anak 	= $this->db->query("SELECT tpk.kid_id, tpk.jenjang_id, tu.user_name, tu.first_name, tu.last_name, tu.user_image from tbl_profile_kid as tpk INNER JOIN tbl_user as tu where tu.id_user = tpk.kid_id and tpk.parent_id='$iduser'")->row_array();
						if (!empty($cek_anak)) {
							 $this->session->set_userdata('punya_anak','yes');
							 // $this->session->set_userdata('jenjang_anak',$cek_anak);
						}
						else{
							$this->session->set_userdata('punya_anak','no');	
						}
					}
					else if ($cek_data['jenjang_id'] != '15') {
						$this->session->set_userdata('status_user', 'pelajar');	
					}

					if($cek_data['school_id']==0 && $cek_data['usertype_id']=='student' && $cek_data['jenjang_id']!='15'){
					$this->session->set_userdata('code_cek', '888');	
					redirect('/Profile-School');			
					}
					if ($cek_data['user_address']==null) {
					$this->session->set_userdata('code_cek', '889');
					redirect('/About');
					}
					$this->session->set_userdata('kids_check','0');
					redirect('/');
				}
			}
			else if($this->session->userdata('usertype_id') == 'student kid'){		
				if ($this->session->userdata('status')==0) {		
					$this->session->set_userdata('code', '189');	
					$this->session->set_userdata('kids_check','0');
					redirect('/');
				}
				else
				{
					$this->session->set_userdata('kids_check','1');
					redirect('/Kids');
				}
			}
			else if($this->session->userdata('usertype_id') == 'tutor'){
				if ($this->session->userdata('status')==3) {
					$load = $this->lang->line('completeprofile');
					
					// $this->session->set_userdata('message', 'completeprofile');
					$this->session->set_flashdata('rets',$rets);
					$this->session->unset_userdata('code');
					$this->session->set_flashdata('mes_alert','warning');
					$this->session->set_flashdata('mes_display','block');
					$this->session->set_flashdata('mes_message',$load);
					// $this->session->set_userdata('inggris');
					$this->session->set_userdata('code', '291');
					redirect('/tutor/approval');
				}else if($this->session->userdata('status')==2){
					$load = $this->lang->line('requestsent');
					
					// $this->session->set_userdata('message', 'requestsent');
					$this->session->set_flashdata('rets',$rets);
					$this->session->unset_userdata('code');
					$this->session->set_flashdata('mes_alert','info');
					$this->session->set_flashdata('mes_display','block');
					$this->session->set_flashdata('mes_message',$load);
					// $this->session->set_userdata('inggris');
					redirect('/tutor');
				}else if($this->session->userdata('status')==0){
					$load = $this->lang->line('checkemail');
					// $rets['mes_alert'] ='warning';
					// $rets['mes_display'] ='block';
					// $rets['mes_message'] =$load;
					// $this->session->set_userdata('message', 'checkemail');
					// $this->session->set_flashdata('rets',$rets);
					// $this->session->unset_userdata('code');
					// $this->session->set_flashdata('mes_alert','warning');
					// $this->session->set_flashdata('mes_display','block');
					// $this->session->set_flashdata('mes_message',$load);
					$this->session->set_userdata('code', '290');
					// $this->session->unset_userdata('code'); 
					redirect('/');
				}
				else
				{
					redirect('/tutor');
					// $this->load->view(MOBILE.'/tutor');					
				}
			}
			else if($this->session->userdata('usertype_id') == "cust_finance")
			{
				// echo "moderator";
				// return false;
				$this->session->set_userdata('code', '898');
				redirect("/Finance");
			}
			else if($this->session->userdata('usertype_id') == "moderator")
			{
				// echo "moderator";
				// return false;
				$this->session->set_userdata('code', '999');
				redirect("/Operator");
			}else if($this->session->userdata('usertype_id') == 'admin'){
				// $this->session->set_userdata('inggris');
				$this->session->unset_userdata('code');
				redirect("/admin");
			}
			/*else if($this->session->userdata('usertype_id') == 'technical_support'){
				// $this->session->set_userdata('inggris');
				$this->session->unset_userdata('code' , '696');
				redirect("/technical_support");
			}*/
			else if($this->session->userdata('usertype_id') == 'tutor_approval'){			
				$this->session->unset_userdata('code' , '696');
				redirect("/admin/");
			}
			else if($this->session->userdata('usertype_id') == 'assessor'){			
				$this->session->unset_userdata('code' , '696');
				redirect("/admin/tutor_approval");
			}
			else if($this->session->userdata('usertype_id') == 'evaluator'){			
				$this->session->unset_userdata('code' , '696');
				redirect("/admin/Evaluator");
			}
			else if($this->session->userdata('usertype_id') == 'user channel'){
				$load = $this->lang->line('completeprofile');
				$rets['mes_alert'] ='success';
				$rets['mes_display'] ='block';
				$rets['mes_message'] = 'blablabla';
				// $this->session->set_userdata('message', 'completeprofile');
				$this->session->set_flashdata('rets',$rets);
				$this->session->unset_userdata('code');
				$this->session->set_userdata('code', '555');
				// $this->session->set_userdata('inggris');
				redirect('/Channel/dashboard');
			}
			else
			{
				// redirect('/');
			}
			$this->session->unset_userdata('code');
		}

		public function google_login()
		{
			$data = json_decode($this->input->post('google_data'),true);
			$check_exist = $this->db->query("SELECT * FROM tbl_user WHERE email='".$data['U3']."'")->row_array();
			$cokies = $this->input->post('bapuks');
			$channel_id = $this->input->post('channel_idd');
			
			// echo $cokies;
			// return false;
			if(empty($check_exist)){
				$new['email'] = $data['U3'];
				$new['user_image'] = $data['Paa'];
				$new['user_name'] = $data['ig'];
				$new['first_name'] = $data['ofa'];
				$new['last_name'] = $data['wea'];
				$new['usertype_id'] = $this->input->post('usertype_id');
				$new['type_regis'] = null;
				$this->session->set_flashdata('google_data',$new);
				redirect('RegisterEmail');
				return null;
			}
			$userutc = $this->input->post('user_utc_g');
			$this->session->set_userdata('user_utc', $userutc);
			$cek = $this->M_login->google_login($data['U3'],$channel_id);
			



			// echo $this->session->userdata('access_token_jwt');
			// return false;
			if ($cek == 0) {
				// $load = $this->lang->line('wrongpassword');
				// $this->session->set_flashdata('mes_alert','danger');
				// $this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message', $load);
				// $this->session->set_userdata('message','wrongpassword');
				//echo '<script>alert("Maaf User Tersebut Tidak ada");</script>';
				$this->session->set_userdata('code', '101');

				redirect('/');
			}
			if ($this->session->userdata('usertype_id') == 'student') {
				
				if ($this->session->userdata('status')==0) {
					// echo $this->session->userdata('status');

					// $load = $this->lang->line('checkemail');
					// $this->session->set_flashdata('mes_alert','warning');
					// $this->session->set_flashdata('mes_display','block');
					// // $this->session->set_flashdata('mes_message',$load);

					// $load = $this->lang->line('youremail3');
					// $load2 = $this->lang->line('here');
					// $load3 = $this->lang->line('emailresend');
					// $this->session->set_flashdata('mes_message', $load."<a href=".base_url('/Master/resendEmaill')."> ".$load2."</a> ".$load3);	
					// $this->session->set_userdata('message', 'checkemail');
					$this->session->set_userdata('code', '189');
					// $this->session->unset_userdata('code'); 
					
					redirect('/');
				}
				else
				{
					$iduser = $this->session->userdata('id_user');
					$cek_data= $this->db->query("SELECT tu.usertype_id, tu.user_religion, tu.user_address, tu.user_image, tps.jenjang_id, tps.school_id FROM tbl_user as tu INNER JOIN tbl_profile_student as tps WHERE tu.id_user='$iduser' AND tps.student_id='$iduser'")->row_array();
					$get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$cek_data['jenjang_id']."'")->row_array()['jenjang_name'];
					$this->session->set_userdata('jenjang_type',$get_jenjangtype);
					
					if ($cek_data['jenjang_id'] == '15') {
						$this->session->set_userdata('status_user', 'umum');	
						$cek_anak 	= $this->db->query("SELECT tpk.kid_id, tpk.jenjang_id, tu.user_name, tu.first_name, tu.last_name, tu.user_image from tbl_profile_kid as tpk INNER JOIN tbl_user as tu where tu.id_user = tpk.kid_id and tpk.parent_id='$iduser'")->row_array();
						if (!empty($cek_anak)) {
							 $this->session->set_userdata('punya_anak','yes');
						}
						else{
							$this->session->set_userdata('punya_anak','no');	
						}
					}
					else if ($cek_data['jenjang_id'] != '15') {
						$this->session->set_userdata('status_user', 'pelajar');	
					}

					if($cek_data['school_id']==0 && $cek_data['usertype_id']=='student' && $cek_data['jenjang_id']!='15'){
					$this->session->set_userdata('code_cek', '888');	
					redirect('/Profile-School');			
					}
					if ($cek_data['user_address']==null) {
					$this->session->set_userdata('code_cek', '889');
					redirect('/About');				
					}
					$this->session->set_userdata('kids_check','0');
					redirect('/');
				}
			}
			else if($this->session->userdata('usertype_id') == 'student kid'){		
				if ($this->session->userdata('status')==0) {		
					$this->session->set_userdata('code', '189');	
					$this->session->set_userdata('kids_check','0');
					redirect('/');
				}
				else
				{
					$this->session->set_userdata('kids_check','1');
					redirect('/Kids');
				}
			}
			else if($this->session->userdata('usertype_id') == 'tutor'){
				if ($this->session->userdata('status')==3) {
					$load = $this->lang->line('completeprofile');
					
					// $this->session->set_userdata('message', 'completeprofile');
					$this->session->set_flashdata('rets',$rets);
					$this->session->unset_userdata('code');
					$this->session->set_flashdata('mes_alert','warning');
					$this->session->set_flashdata('mes_display','block');
					$this->session->set_flashdata('mes_message',$load);
					// $this->session->set_userdata('inggris');
					$this->session->set_userdata('code', '291');
					redirect('/tutor/approval');
				}else if($this->session->userdata('status')==2){
					$load = $this->lang->line('requestsent');
					
					// $this->session->set_userdata('message', 'requestsent');
					$this->session->set_flashdata('rets',$rets);
					$this->session->unset_userdata('code');
					$this->session->set_flashdata('mes_alert','info');
					$this->session->set_flashdata('mes_display','block');
					$this->session->set_flashdata('mes_message',$load);
					// $this->session->set_userdata('inggris');
					redirect('/tutor');
				}else if($this->session->userdata('status')==0){
					$load = $this->lang->line('checkemail');
					// $rets['mes_alert'] ='warning';
					// $rets['mes_display'] ='block';
					// $rets['mes_message'] =$load;
					// $this->session->set_userdata('message', 'checkemail');
					// $this->session->set_flashdata('rets',$rets);
					// $this->session->unset_userdata('code');
					// $this->session->set_flashdata('mes_alert','warning');
					// $this->session->set_flashdata('mes_display','block');
					// $this->session->set_flashdata('mes_message',$load);
					$this->session->set_userdata('code', '290');
					// $this->session->unset_userdata('code'); 
					redirect('/');
				}
				else
				{
					redirect('/tutor');
					// $this->load->view(MOBILE.'/tutor');					
				}
			}
			else if($this->session->userdata('usertype_id') == "cust_finance")
			{
				// echo "moderator";
				// return false;
				$this->session->set_userdata('code', '898');
				redirect("/Finance");
			}
			else if($this->session->userdata('usertype_id') == "moderator")
			{
				// echo "moderator";
				// return false;
				$this->session->set_userdata('code', '999');
				redirect("/Operator");
			}else if($this->session->userdata('usertype_id') == 'admin'){
				// $this->session->set_userdata('inggris');
				$this->session->unset_userdata('code');
				redirect("/admin");
			}
			/*else if($this->session->userdata('usertype_id') == 'technical_support'){
				// $this->session->set_userdata('inggris');
				$this->session->unset_userdata('code' , '696');
				redirect("/technical_support");
			}*/
			else if($this->session->userdata('usertype_id') == 'tutor_approval'){			
				$this->session->unset_userdata('code' , '696');
				redirect("/admin/");
			}
			else if($this->session->userdata('usertype_id') == 'assessor'){			
				$this->session->unset_userdata('code' , '696');
				redirect("/admin/tutor_approval");
			}
			else if($this->session->userdata('usertype_id') == 'evaluator'){			
				$this->session->unset_userdata('code' , '696');
				redirect("/admin/Evaluator");
			}
			else if($this->session->userdata('usertype_id') == 'user channel'){
				$load = $this->lang->line('completeprofile');
				$rets['mes_alert'] ='success';
				$rets['mes_display'] ='block';
				$rets['mes_message'] = 'blablabla';
				// $this->session->set_userdata('message', 'completeprofile');
				$this->session->set_flashdata('rets',$rets);
				$this->session->unset_userdata('code');
				$this->session->set_userdata('code', '555');
				// $this->session->set_userdata('inggris');
				redirect('/Channel/dashboard');
			}
			else
			{
				// redirect('/');
			}
			$this->session->unset_userdata('code');
		}

		public function google_loginn()
		{
			$data = json_decode($this->input->post('google_data'),true);
			$check_exist = $this->db->query("SELECT * FROM tbl_user WHERE email='".$data['U3']."'")->row_array();
			if(empty($check_exist)){
				$new['email'] = $data['U3'];
				$new['user_image'] = $data['Paa'];
				$new['user_name'] = $data['ig'];
				$new['first_name'] = $data['ofa'];
				$new['last_name'] = $data['wea'];
				$new['usertype_id'] = $this->input->post('usertype_id');
				$this->session->set_flashdata('google_data',$new);
				redirect('RegisterEmail');
				return null;
			}
			$this->session->set_userdata('code', '871');	
			redirect('/Register');
			// $userutc = $this->input->post('user_utc_g');
			// $this->session->set_userdata('user_utc', $userutc);
			// $rets = $this->M_login->google_login($data['U3']);
			// $this->session->set_flashdata('rets',$rets);
		}
		public function facebook_login()
		{
			$data = json_decode($this->input->post('fb_data'),true);
			if (empty($data['email'])) {
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message', 'Email Facebook anda tidak mengizinkan!!!');
				redirect('/Register');
			}
			else
			{
				$check_exist = $this->db->query("SELECT * FROM tbl_user WHERE email='".$data['email']."'")->row_array();
				if(empty($check_exist)){
					$new['type_regis'] = $data['type_regis'];
					$new['email'] = $data['email'];
					$new['last_name'] = $data['last_name'];
					$new['first_name'] = $data['first_name'];
					$new['user_image'] = $data['image'];
					$new['user_name'] = $data['name'];
					$new['usertype_id'] = $this->input->post('usertype_id');
					$this->session->set_flashdata('google_data',$new);
					redirect('RegisterEmail');
					return null;
				}
				else{
				$this->session->set_userdata('code', '870');	
				// $userutc = $this->input->post('user_utc_f');
				// $this->session->set_userdata('user_utc', $userutc);
				// $rets = $this->M_login->google_login($data['email']);
				// $this->session->set_flashdata('rets',$rets);
				redirect('/Register');
					
				}
				
			}
		}

		public function facebook_loginn()
		{
			$data = json_decode($this->input->post('fb_data'),true);
			if (empty($data['email'])) {
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message', 'Email Facebook anda tidak mengizinkan!!!');
				$this->session->set_userdata('code', '105');
				redirect('/');
			}
			else
			{
				$check_exist = $this->db->query("SELECT * FROM tbl_user WHERE email='".$data['email']."'")->row_array();
				if(empty($check_exist)){
					$new['email'] = $data['email'];
					$new['last_name'] = $data['last_name'];
					$new['first_name'] = $data['first_name'];
					$new['user_image'] = $data['image'];
					$new['user_name'] = $data['name'];
					$new['usertype_id'] = $this->input->post('usertype_id');
					$this->session->set_flashdata('google_data',$new);
					redirect('RegisterEmail');
					return null;
				}				
				else
				{
					$userutc = $this->input->post('user_utc_f');
					$this->session->set_userdata('user_utc', $userutc);
					$rets = $this->M_login->google_login($data['email']);
					$this->session->set_flashdata('rets',$rets);
					
					$iduser = $this->session->userdata('id_user');
					$cek_data= $this->db->query("SELECT tu.usertype_id, tu.user_religion, tu.user_address, tu.user_image, tps.jenjang_id, tps.school_id FROM tbl_user as tu INNER JOIN tbl_profile_student as tps WHERE tu.id_user='$iduser' AND tps.student_id='$iduser'")->row_array();
					$get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$cek_data['jenjang_id']."'")->row_array()['jenjang_name'];
						$this->session->set_userdata('jenjang_type',$get_jenjangtype);
					
					if ($cek_data['jenjang_id'] == '15') {
						$this->session->set_userdata('status_user', 'umum');
						$cek_anak 	= $this->db->query("SELECT tpk.kid_id, tpk.jenjang_id, tu.user_name, tu.first_name, tu.last_name, tu.user_image from tbl_profile_kid as tpk INNER JOIN tbl_user as tu where tu.id_user = tpk.kid_id and tpk.parent_id='$iduser'")->row_array();
						if (!empty($cek_anak)) {
							 $this->session->set_userdata('punya_anak','yes');
						}
						else{
							$this->session->set_userdata('punya_anak','no');	
						}	
					}
					else if ($cek_data['jenjang_id'] != '15') {
						$this->session->set_userdata('status_user', 'pelajar');	
					}

					if($cek_data['school_id']==0 && $cek_data['usertype_id']=='student' && $cek_data['jenjang_id']!='15'){
						$this->session->set_userdata('code_cek', '888');	
						redirect('/Profile-School');			
					}
					if ($cek_data['usertype_id'] == 'tutor') {
						redirect('/Tutor');
					}
					// if ($cek_data['user_address']==null) {
					// 	$this->session->set_userdata('code_cek', '889');
					// 	redirect('/About');				
					// }
					$this->session->set_userdata('kids_check','0');
					// redirect('/');
					redirect($rets['redirect_path']);		
				}
				
			}
		}

		function unserialize(){

			$res = $this->Master_model->getTblUserAndProfileTutor();

			$serialized_data = $res['education_background'];
			$serialized_data2 = $res['teaching_experience'];
			$var1 = unserialize($serialized_data);
			$var2 = unserialize($serialized_data2);
			print_r('<pre>');
			print_r($var1);
			print_r('</pre>');

			print_r('<pre>');
			print_r($var2);
			print_r('</pre>');
			// $serialized_data = serialize(array('Math', 'Language', 'Science'));  
			// echo  $serialized_data . '<br>';  
			// // Unserialize the data  
			// $var1 = unserialize($serialized_data);  
			// // Show the unserialized data;  
			// var_dump ($var1);  
		}

		function change_user(){
			$id_user = $this->input->post('id_user');
			// echo $id_user;
			// return false;
			$where = array(
				'id_user' => $id_user
			);
			$cek = $this->M_login->cek_user("tbl_user",$where);


		}
		function aksi_login_new(){
			$email = $this->input->post('email');
			$channel_id = $this->input->post('channel_id');
			$kata_sandi = $this->input->post('kata_sandi');
			$cokies = $this->input->post('bapuks');
			$redirect = $this->input->post('redirect');
			$userutc = $this->input->post('user_utc');
			$this->session->set_userdata('user_utc', $userutc);
			$kode = $this->input->post('kode');
			$id_user = $this->db->query("SELECT id_user FROM tbl_user WHERE ( email='$email' || username='$email' )")->row_array()['id_user'];
			$ckChannel_id = $this->db->query("SELECT channel_id FROM master_channel WHERE channel_iduser = '$id_user'")->row_array()['channel_id'];

			$where = array(
				'email' => $email,
				'kata_sandi' => $kata_sandi,
				'channel_id' => $ckChannel_id
			);

			$cek = $this->M_login->cek_login("tbl_user",$where);
			
			if ($cek == 1) {				
				if ($this->session->userdata('usertype_id') == 'student') {
					$student_id = $this->session->userdata('id_user');
					$this->session->set_userdata('kode_cek',$kode);
					$iduser = $this->session->userdata('id_user');
					$cek_data= $this->db->query("SELECT tu.usertype_id, tu.user_religion, tu.user_address, tu.user_image, tps.flag, tps.jenjang_id, tps.school_id FROM tbl_user as tu INNER JOIN tbl_profile_student as tps WHERE tu.id_user='$iduser' AND tps.student_id='$iduser'")->row_array();
					$get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$cek_data['jenjang_id']."'")->row_array()['jenjang_name'];
					$get_flag = $this->db->query("SELECT flag  FROM tbl_profile_student where student_id='".$iduser."'")->row_array()['flag'];
					$this->session->set_userdata('jenjang_type',$get_jenjangtype);
					$this->session->set_userdata('flag',$get_flag);
										
					if ($cek_data['jenjang_id'] == '15') {
						$this->session->set_userdata('status_user', 'umum');	
						$cek_anak 	= $this->db->query("SELECT tpk.kid_id, tpk.jenjang_id, tu.user_name, tu.first_name, tu.last_name, tu.user_image from tbl_profile_kid as tpk INNER JOIN tbl_user as tu where tu.id_user = tpk.kid_id and tpk.parent_id='$iduser'")->row_array();
						if (!empty($cek_anak)) {
							 $this->session->set_userdata('punya_anak','yes');
							 $this->session->set_userdata('parent_id',$iduser);
						}
						else{
							$this->session->set_userdata('punya_anak','no');	
						}
					}
					else if ($cek_data['jenjang_id'] != '15') {
						$flag = $this->db->query("SELECT flag FROM tbl_profile_student WHERE student_id = '$student_id'")->row_array()['flag'];
						$this->session->set_userdata('flag_profile', $flag);
						$this->session->set_userdata('status_user', 'pelajar');	
						if ($cek_data['flag'] == 0) {
							$this->session->set_userdata('flag', '0');	
							$this->session->set_userdata('code_cek', '777');	
						}
					}

					if ($cek_data['user_address']==null) {
						$this->session->set_userdata('code_cek', '889');
						// redirect('/About');				
						$rets['redirect'] ='/About';
					}
					if($cek_data['school_id']==0 && $cek_data['usertype_id']=='student' && $cek_data['jenjang_id']!='15'){
						$this->session->set_userdata('code_cek', '888');	
						// redirect('/Profile-School');		
						$rets['redirect'] ='/Profile-School';	
					}
					
					$this->session->set_userdata('kids_check','0');
					// redirect('/');					
					$rets['redirect'] ='/first/dashboard';	
					
				}else if($this->session->userdata('usertype_id') == 'student kid'){							
					if ($this->session->userdata('status')==0) {		
						$this->session->set_userdata('code', '189');	
						$this->session->set_userdata('kids_check','0');
						if ($redirect == 'channel') {
							// redirect('/c?ch='.$channel_id);	
							$rets['redirect'] ='/c?ch='.$channel_id;
						}
						// redirect('/');
						$rets['redirect'] ='/';
					}
					else
					{
						$this->session->set_userdata('kids_check','1');
						// redirect('/Kids');
						$rets['redirect'] ='/Kids';
					}
				}else if($this->session->userdata('usertype_id') == 'tutor'){					
					if ($this->session->userdata('status')==3) {
						$load = $this->lang->line('completeprofile');
						
						// $this->session->set_userdata('message', 'completeprofile');
						// $this->session->set_flashdata('rets',$rets);
						$this->session->unset_userdata('code');
						$this->session->set_flashdata('mes_alert','warning');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message',$load);
						// $this->session->set_userdata('inggris');
						$this->session->set_userdata('code', '291');
						// redirect('/tutor/approval');
						$rets['redirect'] ='/tutor/approval';
					}else if($this->session->userdata('status')==2){
						$load = $this->lang->line('requestsent');
						
						// $this->session->set_userdata('message', 'requestsent');
						// $this->session->set_flashdata('rets',$rets);
						$this->session->unset_userdata('code');
						$this->session->set_flashdata('mes_alert','info');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message',$load);
						// $this->session->set_userdata('inggris');
						// redirect('/tutor');
						$rets['redirect'] ='/tutor';
					}else if($this->session->userdata('status')==0){
						$load = $this->lang->line('checkemail');
						// $rets['mes_alert'] ='warning';
						// $rets['mes_display'] ='block';
						// $rets['mes_message'] =$load;
						// $this->session->set_userdata('message', 'checkemail');
						// $this->session->set_flashdata('rets',$rets);
						// $this->session->unset_userdata('code');
						// $this->session->set_flashdata('mes_alert','warning');
						// $this->session->set_flashdata('mes_display','block');
						// $this->session->set_flashdata('mes_message',$load);
						$this->session->set_userdata('code', '290');
						// $this->session->unset_userdata('code'); 
						if ($redirect == 'channel') {
							// redirect('/c?ch='.$channel_id);	
							$rets['redirect'] ='/c?ch='.$channel_id;
						}
						// redirect('/');
						$rets['redirect'] ='/';
					}
					else
					{
						// redirect('/tutor');
						$rets['redirect'] ='/tutor';
						// $this->load->view(MOBILE.'/tutor');					
					}
				}else if($this->session->userdata('usertype_id') == "moderator" && $email=='cust_finance@meetaza.com'){
					// echo "cust_finance";
					// return false;
					$this->session->set_userdata('code', '898');
					// redirect("/Finance");
					$rets['redirect'] ='/Finance';
				}else if($this->session->userdata('usertype_id') == "moderator" && $email=='tutor_finance@meetaza.com'){
					// echo "cust_finance";
					// return false;
					$this->session->set_userdata('code', '898');
					// redirect("/Finance");
					$rets['redirect'] ='/Finance';
				}else if($this->session->userdata('usertype_id') == "moderator"){
					// echo "moderator";
					// return false;
					$this->session->set_userdata('code', '999');
					// redirect("/Operator");
					$rets['redirect'] ='/Operator';
				}else if($this->session->userdata('usertype_id') == 'admin'){
					// $this->session->set_userdata('inggris');
					$this->session->unset_userdata('code');
					// redirect("/admin");
					$rets['redirect'] ='/admin';
				}
				/*else if($this->session->userdata('usertype_id') == 'technical_support'){
					// $this->session->set_userdata('inggris');
					$this->session->unset_userdata('code' , '696');
					redirect("/technical_support");
				}*/
				else if($this->session->userdata('usertype_id') == 'tutor_approval'){			
					$this->session->unset_userdata('code' , '696');
					// redirect("/admin/");
					$rets['redirect'] ='/admin/';
				}else if($this->session->userdata('usertype_id') == 'assessor'){			
					$this->session->unset_userdata('code' , '696');
					// redirect("/admin/tutor_approval");
					$rets['redirect'] ='/admin/tutor_approval';
				}else if($this->session->userdata('usertype_id') == 'evaluator'){			
					$this->session->unset_userdata('code' , '696');
					// redirect("/admin/Evaluator");
					$rets['redirect'] ='/admin/Evaluator';
				}else if($this->session->userdata('usertype_id') == 'user channel'){							

					$load = $this->lang->line('completeprofile');
					$rets['mes_alert'] ='success';
					$rets['mes_display'] ='block';
					$rets['mes_message'] = 'blablabla';
					// $this->session->set_userdata('message', 'completeprofile');
					$this->session->set_flashdata('rets',$rets);
					$this->session->unset_userdata('code');
					$this->session->set_userdata('code', '555');
					// $this->session->set_userdata('inggris');
					// redirect('/Channel');
					$post = [
					    'username' => $email,
					    'password' => $kata_sandi
					];
					
					$ch = curl_init();

			    	// Set the url, number of POST vars, POST data
					curl_setopt($ch, CURLOPT_URL, 'https://classmiles.com/air/v1/channel_login');
					curl_setopt($ch, CURLOPT_POST, 1);
					// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

					curl_setopt($ch, CURLOPT_POSTFIELDS,
			            'username='.$email.'&password='.$kata_sandi.'&channel_id='.$ckChannel_id);

			    	// Execute post
					$result = curl_exec($ch);
					curl_close($ch);
					
					$a = json_decode($result);
					$access_token = $a->access_token;
					$this->session->set_userdata('access_token', $access_token);
					// redirect('/Channel');
					$rets['redirect'] ='/Channel';
				}else
				{
					// redirect('/');
					$rets['redirect'] ='/';
				}
				$rets['status_login'] =true;
			}
			else if ($cek == 0) {
				// echo $cek;
				$rets['status_login'] =false;
				$this->session->set_userdata('code', '101');
				// redirect('/');
				$rets['redirect'] ='/';
				// echo $cek;
			}else if ($cek == -2) {
				$rets['redirect'] ='/';
				$rets['status_login'] =false;
				$this->session->set_userdata('code', '189');
				// redirect('/');
				// echo $cek;
			}
			else{
				// echo $cek;
				$rets['status_login'] =false;
				$this->session->set_userdata('code', '101');
				// redirect('/');
				$rets['redirect'] ='/';

			}
			$rets['login_code'] =$cek;
			echo json_encode($rets);
		}

		function aksi_login(){
			//echo '<script>alert("Maaf User Tersebut Tidak ada");</script>';
			$email = $this->input->post('email');
			$channel_id = $this->input->post('channel_id');
			$kata_sandi = $this->input->post('kata_sandi');
			$cokies = $this->input->post('bapuks');
			$redirect = $this->input->post('redirect');
			// echo $cokies;
			// return false;
			$userutc = $this->input->post('user_utc');
			$this->session->set_userdata('user_utc', $userutc);
			$kode = $this->input->post('kode');
			// echo $cokies;
			// return false;
			$id_user = $this->db->query("SELECT id_user FROM tbl_user WHERE ( email='$email' || username='$email' )")->row_array()['id_user'];
			$ckChannel_id = $this->db->query("SELECT channel_id FROM master_channel WHERE channel_iduser = '$id_user'")->row_array()['channel_id'];

			$where = array(
				'email' => $email,
				'kata_sandi' => $kata_sandi,
				'channel_id' => $ckChannel_id
			);

			$cek = $this->M_login->cek_login("tbl_user",$where);
			// echo $this->session->userdata('kode_login');
			// echo $cek;
			// return false;

			// echo $this->session->userdata('access_token_jwt');			
			if ($cek == 1) {
				if ($this->session->userdata('usertype_id') == 'student') {
					$student_id = $this->session->userdata('id_user');
					$this->session->set_userdata('kode_cek',$kode);
					if ($this->session->userdata('status')==0) {

						$this->session->set_userdata('code', '189');
						if ($redirect == 'channel') {
							redirect('/c?ch='.$channel_id);	
						}
						redirect('/');
					}
					else
					{
						$iduser = $this->session->userdata('id_user');
						$cek_data= $this->db->query("SELECT tu.usertype_id, tu.user_religion, tu.user_address, tu.user_image, tps.flag, tps.jenjang_id, tps.school_id FROM tbl_user as tu INNER JOIN tbl_profile_student as tps WHERE tu.id_user='$iduser' AND tps.student_id='$iduser'")->row_array();
						$get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$cek_data['jenjang_id']."'")->row_array()['jenjang_name'];
						$get_flag = $this->db->query("SELECT flag  FROM tbl_profile_student where student_id='".$iduser."'")->row_array()['flag'];
						$this->session->set_userdata('jenjang_type',$get_jenjangtype);
						$this->session->set_userdata('flag',$get_flag);
						

						if ($cek_data['jenjang_id'] == '15') {
							$this->session->set_userdata('status_user', 'umum');
							$this->session->set_userdata('punya_anak','no');							
						}
						else if ($cek_data['jenjang_id'] != '15') {
							$flag = $this->db->query("SELECT flag FROM tbl_profile_student WHERE student_id = '$student_id'")->row_array()['flag'];
							$this->session->set_userdata('flag_profile', $flag);
							$this->session->set_userdata('status_user', 'pelajar');	
							if ($cek_data['flag'] == 0) {
								$this->session->set_userdata('flag', '0');	
								$this->session->set_userdata('code_cek', '777');	
							}
						}
												
						redirect('/');
					}
				}else if($this->session->userdata('usertype_id') == 'tutor'){
					if ($this->session->userdata('status')==3) {
						$load = $this->lang->line('completeprofile');
						
						// $this->session->set_userdata('message', 'completeprofile');
						$this->session->set_flashdata('rets',$rets);
						$this->session->unset_userdata('code');
						$this->session->set_flashdata('mes_alert','warning');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message',$load);
						// $this->session->set_userdata('inggris');
						$this->session->set_userdata('code', '291');
						redirect('/tutor/approval');
					}else if($this->session->userdata('status')==2){
						$load = $this->lang->line('requestsent');
						
						// $this->session->set_userdata('message', 'requestsent');
						$this->session->set_flashdata('rets',$rets);
						$this->session->unset_userdata('code');
						$this->session->set_flashdata('mes_alert','info');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message',$load);
						// $this->session->set_userdata('inggris');
						redirect('/tutor');
					}else if($this->session->userdata('status')==0){
						$load = $this->lang->line('checkemail');
						// $rets['mes_alert'] ='warning';
						// $rets['mes_display'] ='block';
						// $rets['mes_message'] =$load;
						// $this->session->set_userdata('message', 'checkemail');
						// $this->session->set_flashdata('rets',$rets);
						// $this->session->unset_userdata('code');
						// $this->session->set_flashdata('mes_alert','warning');
						// $this->session->set_flashdata('mes_display','block');
						// $this->session->set_flashdata('mes_message',$load);
						$this->session->set_userdata('code', '290');
						// $this->session->unset_userdata('code'); 
						if ($redirect == 'channel') {
							redirect('/c?ch='.$channel_id);	
						}
						redirect('/');
					}
					else
					{
						redirect('/tutor');
						// $this->load->view(MOBILE.'/tutor');					
					}
				}else if($this->session->userdata('usertype_id') == 'admin'){
					// $this->session->set_userdata('inggris');
					$this->session->unset_userdata('code');
					redirect("/admin");
				}
			}
			else if ($cek == 0) {
				$this->session->set_userdata('code', '101');
				redirect('/');
				// echo $cek;
			}
			else{
				$this->session->set_userdata('code', '101');
				redirect('/');

			}
		}

		public function aksi_login_ch(){
			$email = $this->input->get('email');
			$channel_id = $this->input->get('channel_id');
			$allrek = $this->db->query("SELECT * FROM tbl_rekening WHERE id_user='$id_user' AND bank_id='$bank_id' limit 0,1")->row_array();
			if(!empty($allrek)){
				$rets['status'] = 1;
				$rets['message'] = "Succeed";
				$rets['norek'] = $allrek['nomer_rekening'];
				$rets['nama_akun'] = $allrek['nama_akun'];
			}
			else
			{
				$rets['status'] = 0;
				$rets['message'] = "failed";			
				$rets['norek'] = null;
				$rets['nama_akun'] = null;
			}		
			
	        echo json_encode($rets);
		}

		function daftarbaru(){
			$detect = new Mobile_Detect();
			$this->session->set_flashdata('form_cache', $this->input->post());

			// Mendapatkan input recaptcha dari user
			$captcha_answer = $this->input->post('g-recaptcha-response');
			
			// Verifikasi input recaptcha dari user
			$response = $this->recaptcha->verifyResponse($captcha_answer);
			
			if ($captcha_answer) {
				// Code jika sukses

				$first_name 	= $this->input->post('first_name');
				$last_name 		= $this->input->post('last_name');
				$user_name 		= $first_name." ".$last_name;
				$email 			= $this->input->post('email');
				$password 		= $this->input->post('kata_sandi');
				// $konfirmasi_ps = $this->input->post('konf_kata_sandi');
				$tanggal_lahir 	= $this->input->post('tanggal_lahir');
				$bulan_lahir 	= $this->input->post('bulan_lahir');
				$tahun_lahir  	= $this->input->post('tahun_lahir');

				$birthdate 		= $tahun_lahir."-".$bulan_lahir."-".$tanggal_lahir;

				$gender 		= $this->input->post('jenis_kelamin');

				$kode_area 		= $this->input->post('kode_area');
				$nama_negara	= $this->input->post('nama_negara');
				$no_hape 		= $this->input->post('no_hape');
				$user_callnum 	= $kode_area."".$no_hape;
				if ($detect->isMobile()){
					$reg_by 	= "M";
				}else{
					$reg_by 	= "W";
				}
				
				$tab 			= $this->input->post('tab');
				if ($tab == "2") {
					$jenjang_id = "15";
				}
				else
				{
					$jenjang_id = $this->input->post('jenjang_id');
				}

				// if ($typeregis == "ortu" || $typeregis == "umum") {
				// 	$umur = $birthdate;
				// 	$skrg = date_diff(date_create($umur), date_create('today'))->y;
				// 	echo $skrg;
				// 	// echo $typeregis;
				// }
				// return false;		

				$where = array(
					'user_name' => $user_name,
					'first_name' => $first_name,
					'last_name' => $last_name,
					'email' => $email,
					'kata_sandi' => password_hash($password,PASSWORD_DEFAULT),				
					'tanggal_lahir' => $birthdate,
					'user_nationality' => $nama_negara,
					'user_countrycode' => $kode_area,
					'no_hp_murid' => $no_hape,
					'jenis_kelamin' => $gender,
					'jenjang_id' => $jenjang_id,
					'reg_by' => $reg_by
				);

				$res = $this->M_login->daftar_baru("tbl_user",$where);
				// echo $res;
				// return 1;
				if (empty($res)) {
					$this->session->set_flashdata('mes_alert','danger');
					$this->session->set_flashdata('mes_display','block');
					$this->session->set_flashdata('mes_message',$load);
					redirect('/Register');
				}
				$email_tujuan 	= $res['email'];
				$kata_sandi 	= $password;
				$id_user 		= $res['id_user'];
				$random_key 	= $res['r_key'];
				$key 			= $res['key']; 
				$checkbahasa 	= $this->session->userdata('lang');
			
				if (!$key == 0) {
					
					if ($res['key']==2) {
						$load = $this->lang->line('youremail');
						$load2 = $this->lang->line('here');
						$load3 = $this->lang->line('emailresend');
						$this->session->set_flashdata('mes_alert','danger');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message', $load."<a href=".base_url('/first/resendEmail')."> ".$load2."</a> ".$load3);	
						// $this->session->set_userdata('message','dua');
						redirect('Register');
					}else if ($res['key']==3){
						$load = $this->lang->line('accountregistered');
						$load2 = $this->lang->line('here');
						$load3 = $this->lang->line('tologin');
						$this->session->set_flashdata('mes_alert','danger');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message',"$email $load <a href=".base_url('/first')."> $load2 </a> $load3");	
						// $this->session->set_userdata('message','tiga');
						$this->session->set_userdata('emailregister', $email);
						redirect('Register');
					}
					else if($res['key']==-1){
						$load = $this->lang->line('pesanumurumum');
						$this->session->set_flashdata('mes_alert','danger');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message',$load);	
						// $this->session->set_userdata('message','tiga');
						// $this->session->set_userdata('emailregister', $email);
						redirect('Register');
					}	
					else if($res['key']==-2){
						$load = $this->lang->line('pesanumursiswa');
						$this->session->set_flashdata('mes_alert','danger');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message',$load);	
						// $this->session->set_userdata('message','tiga');
						// $this->session->set_userdata('emailregister', $email);
						redirect('Register');
					}					

				}
				else
				{
					// $token = 'dd4a8aff41d413305809c45536abd3af';
					// $domainname = 'https://classmiles.com/moodle';
					// $functionname = 'core_user_create_users';
					// $restformat = 'json';

					// $user = array(
					//     "username" => $id_user, // must be unique.
					//     "password" => $password,
					//     "firstname" => $first_name,
					//     "lastname" => $last_name,
					//     "email" => $email,
					//     "country" => 'ID',
					//     "auth" => 'manual'
					// );

					// $users = array($user); // must be wrapped in an array because it's plural.
					// $param = array("users" => $users); // the paramater to send
					// $ch = curl_init();
					// curl_setopt($ch, CURLOPT_URL, $domainname . '/webservice/rest/server.php'. '?wstoken='.$token . '&wsfunction='.$functionname);
					// curl_setopt($ch, CURLOPT_POST, true);
					// // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					// curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param));

					// // Execute post
					// $result = curl_exec($ch);
					// curl_close($ch);

					// Open connection
		            $ch = curl_init();

		            // Set the url, number of POST vars, POST data
		            curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
		            curl_setopt($ch, CURLOPT_POST, true);
		            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_daftarbaru','content' => array("email_tujuan" => "$email_tujuan", "user_name" => "$user_name", "random_key" => "$random_key") )));

		            // Execute post
		            $result = curl_exec($ch);
		            curl_close($ch);
		            // echo $result;
						
					$this->session->set_userdata('code','121');				
					redirect('Register');
					
				}									
			} else {
				$load = $this->lang->line('captcha');
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message',$load);	
				// $this->session->set_userdata('message', 'captcha');
				redirect('Register');
			}

		}

		function resendEmaill(){

			$iduser = $this->session->userdata('id_user');
			$where = array(
				'id_user' => $iduser
			);
			$res = $this->Master_model->resend_Emaill($where);

			$email = $this->session->userdata('email');
			$qresend = $this->db->query("SELECT * FROM tbl_user WHERE email='$email'")->row_array();		
			$user_name = $qresend['user_name'];
			$random_key = $res;

			



			// Open connection
	        $ch = curl_init();

	        // Set the url, number of POST vars, POST data
	        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
	        curl_setopt($ch, CURLOPT_POST, true);
	        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_resendEmaill','content' => array("email" => "$email", "user_name" => "$user_name", "random_key" => "$random_key") )));

	        // Execute post
	        $result = curl_exec($ch);
	        curl_close($ch);
	        // echo $result;

			if ($result) {
				$this->session->set_userdata('code', '187');
				redirect('/');
			}else{
				$this->session->set_userdata('code', '188');
				redirect('/');	
			}
		}

		function resendEmail(){
			$res = $this->Master_model->resend_Email($this->input->post());

			$email = $this->input->post('emailaddress');
			$qresend = $this->db->query("SELECT * FROM tbl_user WHERE email='$email'")->row_array();		
			$random_key = $res;


			// Open connection
	        $ch = curl_init();

	        // Set the url, number of POST vars, POST data
	        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
	        curl_setopt($ch, CURLOPT_POST, true);
	        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_resendEmail','content' => array("email" => "$email", "user_name" => "$user_name", "random_key" => "$random_key") )));

	        // Execute post
	        $result = curl_exec($ch);
	        curl_close($ch);
	        // echo $result;


			if ($result) {
				$load = $this->lang->line('successfullysent');
				$this->session->set_flashdata('cek',$res);
				$this->session->set_flashdata('mes_alert','success');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message',$load);	
				redirect('/');
			}else{
				$load = $this->lang->line('failed');
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message',$load);	
				// $this->session->set_userdata('message', 'failed');
				redirect('/');	
			}
		}

		function daftartutor()
		{
			$detect = new Mobile_Detect();
			$this->session->set_flashdata('form_cache', $this->input->post());

			// Mendapatkan input recaptcha dari user
			// $captcha_answer = $this->input->post('g-recaptcha-response');

			// Verifikasi input recaptcha dari user
			// $response = $this->recaptcha->verifyResponse($captcha_answer);

			// if ($response['success']) {
				$first_name 	= $this->input->post('first_name');
				$last_name 		= $this->input->post('last_name');			
				$user_name 		= $first_name." ".$last_name;
				$email 			= $this->input->post('email');
				$password 		= $this->input->post('kata_sandi');
				$konfirmasi_ps 	= $this->input->post('konf_kata_sandi');
				$tanggal_lahir 	= $this->input->post('tanggal_lahir');
				$bulan_lahir 	= $this->input->post('bulan_lahir');
				$tahun_lahir 	= $this->input->post('tahun_lahir');
				$birthdate 		= $tahun_lahir."-".$bulan_lahir."-".$tanggal_lahir;
				$gender 		= $this->input->post('jenis_kelamin');
				$nama_negara	= $this->input->post('nama_negara');
				$kode_area 		= $this->input->post('kode_area');
				$no_hape 		= $this->input->post('no_hape');
				$tutor_callnum 	= $kode_area."".$no_hape;
				$kodereferral 	= $this->input->post('kode_referral');
				if ($detect->isMobile()){
					$reg_by 	= "M";
				}else{
					$reg_by 	= "W";
				}

				$where = array(
					'first_name' => $first_name,
					'last_name' => $last_name,
					'nama_lengkap' => $user_name,
					'email' => $email,
					'kata_sandi' => password_hash($password,PASSWORD_DEFAULT),
					'konfirmasi_ks' => $konfirmasi_ps,
					'tanggal_lahir' => $birthdate,
					'user_nationality' => $nama_negara,
					'user_countrycode' => $kode_area,
					'no_hp_tutor' => $no_hape,
					'jenis_kelamin' => $gender,
					'referral_id' => $kodereferral,
					'reg_by' => $reg_by	
					);

				$res = $this->M_login->daftar_tutor("tbl_user",$where);
				$email_tujuan = $res['email'];			
				$kata_sandi = $password;
				$email_name = $res['first_name'];
				$id_user = $res['id_user'];
				$random_key = $res['r_key']; 
				$key = $res['key'];				
				if ($gender == null) {
					$this->session->set_userdata('code', '333');
					redirect('RegisterTutor');
					$this->session->set_flashdata('form_cache', $this->input->post());
				}
				if (!$key == 0) {
					if ($res['key']==2) {
						$load = $this->lang->line('youremail');
						$load2 = $this->lang->line('here');
						$load3 = $this->lang->line('emailresend');
						// $this->session->set_flashdata('mes_alert','danger');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message', $load."<a href=".base_url('/first/resendEmail')."> ".$load2."</a> ".$load3);	
						$this->session->set_userdata('code', '203');
						// $this->session->set_userdata('message','dua');
						redirect('RegisterTutor');
					}else{

						$load = $this->lang->line('tutoraccountregistered');
						$load2 = $this->lang->line('here');
						$load3 = $this->lang->line('tologin');
						$this->session->set_flashdata('mes_alert','danger');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message',"$email $load <a href=".base_url('/first')."> $load2 </a> $load3");	
						$this->session->set_userdata('code', '202');
						// echo $this->session->userdata('code');
						// return false;		
						// $this->session->set_userdata('message','tiga');

						redirect('RegisterTutor');
						$this->session->set_flashdata('form_cache', $this->input->post());
					}				
				}
				else
				{
					
					// Open connection
			        $ch = curl_init();

			        // Set the url, number of POST vars, POST data
			        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
			        curl_setopt($ch, CURLOPT_POST, true);
			        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_daftarTutor','content' => array("email_tujuan" => "$email_tujuan", "user_name" => "$user_name", "random_key" => "$random_key") )));

			        // Execute post
			        $result = curl_exec($ch);
			        curl_close($ch);
			        // echo $result;


		            if ($result) {
		            	$load = $this->lang->line('tutorregistered');
		             	$this->session->set_userdata('code','204');
						$this->session->set_flashdata('rets',$rets);			
						redirect('Register');		
		            }			
				}
				// }else{
				// 	$load = $this->lang->line('captcha');
				// 	$this->session->set_flashdata('mes_alert','danger');
				// 	$this->session->set_flashdata('mes_display','block');
				// 	$this->session->set_flashdata('mes_message',$load);	
				// 	// $this->session->set_userdata('message', 'captcha');
				// 	redirect('RegisterTutor');
				// 	// $this->session->set_userdata('code', '203');
				// 	// echo "<script>$('#alert_modal').modal('show')</script>";
				// }
		}

		function aksi_ubahpass(){
			$oldpass = $this->input->post('oldpass');
			$newpass = $this->input->post('newpass');
			$conpass = $this->input->post('conpass');
			$email = $this->input->post('mail');

			if ($newpass != $conpass) {
				$load = $this->lang->line('failedpassword');
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message',$load);	
				// $this->session->set_userdata('message', 'failedpassword');
				redirect('/first/profile_forgot');
			}

			$where = array(
				'oldpass' => $oldpass,
				'newpass' => $newpass,
				'email' => $email
				);

			$cek = $this->M_forgot->cek_oldpass("tbl_user",$where);
			if ($cek == 0) {
				$load = $this->lang->line('oldpasswordfailed');
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message',$load);
				// $this->session->set_userdata('message', 'oldpasswordfailed');	
				redirect('/first/profile_forgot');
			}
			$changepass = $this->M_forgot->ubahpass("tbl_user", $where);	
			if ($changepass) {
				$load = $this->lang->line('successfullypassword');
				$this->session->set_flashdata('mes_alert','success');
				$this->session->set_flashdata('mes_display','block'); 
				$this->session->set_flashdata('mes_message',$load);
				// $this->session->set_userdata('message', 'successfullypassword');			
				redirect('/logout');
			}
			$load = $this->lang->line('failed');
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block'); 
			$this->session->set_flashdata('mes_message',$load);
			// $this->session->set_userdata('message', 'failed');
			redirect('/first/profile_forgot');

		}

		function savebooking(){
			$id_user = $this->input->get('id_user');
			$tutor_id = $this->input->get('tutor_id');
			$subject_id = $this->input->get('subject_id');
			$subject_namee = $this->input->get('subject_namee');

			$where = array(
				'id_user' => $id_user,
				'tutor_id' => $tutor_id,
				'subject_id' => $subject_id,
				'subject_name' => $subject_namee
				);

			$res = $this->Master_model->save_booking("tbl_booking",$where);
			$rest = array();
			if ($res == 1) {
				$load = $this->lang->line('successfullyselecting');
				// $this->session->set_flashdata('mes_alert','success');
				// $this->session->set_flashdata('mes_display','block'); 
				// $this->session->set_flashdata('mes_message',$load);
				// // $this->session->set_userdata('message', 'successfullyselecting');
				// redirect('Subjects');
				$rest['status'] 	= true;
				$rest['code'] 		= '1';			
				$rest['message'] 	= $load;
			    echo json_encode($rest);
			}else if($res == 0){
				$load = $this->lang->line('followedtutor');
				// $this->session->set_flashdata('mes_alert','danger');
				// $this->session->set_flashdata('mes_display','block'); 
				// $this->session->set_flashdata('mes_message',$load);
				// // $this->session->set_userdata('message', 'followedtutor');
				// redirect('Subjects');
				$rest['status'] 	= true;
				$rest['code'] 		= '2';			
				$rest['message'] 	= $load;
			    echo json_encode($rest);
			}else{
				$load = $this->lang->line('failedselect');
				// $this->session->set_flashdata('mes_alert','info');
				// $this->session->set_flashdata('mes_display','block'); 
				// $this->session->set_flashdata('mes_message',$load);
				// // $this->session->set_userdata('message', 'failedselect');
				// redirect('Subjects');
				$rest['status'] 	= true;
				$rest['code'] 		= '3';
				$rest['message'] 	= $load;
			    echo json_encode($rest);
			}
		}

		function unsavebooking(){
			$id_user = $this->input->get('id_user');
			$tutor_id = $this->input->get('tutor_id');
			$subject_id = $this->input->get('subject_id');
			$subject_name = $this->input->get('subject_name');

			$where = array(
				'id_user' => $id_user,
				'tutor_id' => $tutor_id,
				'subject_id' => $subject_id,
				'subject_name' => $subject_name
				);
			$res = $this->Master_model->unsave_booking("tbl_booking",$where);
			$getname = array();
			$getname = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array();
			if ($res == 1) {
				$load = $this->lang->line('unfollowsuccessfully')." ( ".$getname['user_name']." )";
				// $this->session->set_flashdata('mes_alert','success');
				// $this->session->set_flashdata('mes_display','block'); 
				// $this->session->set_flashdata('mes_message',$load);
				// // $this->session->set_userdata('message', 'successfullyselecting');
				// redirect('Subjects');
				$rest['status'] 	= true;
				$rest['code']	 	= '1';
				$rest['data'] 		= $getname['user_name'];
				$rest['message'] 	= $load;
			    echo json_encode($rest);
			}else{
				$load = $this->lang->line('failedselect');
				// $this->session->set_flashdata('mes_alert','warning');
				// $this->session->set_flashdata('mes_display','block'); 
				// $this->session->set_flashdata('mes_message',$load);
				// // $this->session->set_userdata('message', 'failedselect');
				// redirect('Subjects');
				$rest['status'] 	= true;
				$rest['code'] 		= '0';
				$rest['data'] 		= '';
				$rest['message'] 	= $load;
			    echo json_encode($rest);
			}
		}

		function setsessionver(){
			$code = $this->input->get('code');		
			$this->session->set_userdata('code',$code);
			$rest['status'] 	= true;
			echo json_encode($rest);
		}

		function setsessionchildid(){
			$child_id = $this->input->get('child_id');		
			$child_jenjang = $this->input->get('child_jenjang');		
			$this->session->set_userdata('child_id',$child_id);
			$this->session->set_userdata('child_jenjang',$child_jenjang);
			$rest['status'] 	= true;
			$rest['code']	 	= '1';
			$rest['child_id']	= $child_id;
			echo json_encode($rest);
		}

		function unsetsessionchildid(){
			// $child_id = $this->input->get('child_id');		
			$rest['status'] 	= true;
			$rest['code']	 	= '1';
			// $rest['child_id']	= $child_id;		
			echo json_encode($rest);
		}

		function addsub(){
			$id_user = $this->input->get('id_user');
			$subject_id = $this->input->get('subject_id');

			$where = array(
				'id_user' => $id_user,
				'subject_id' => $subject_id
				);

			$res = $this->Master_model->choose_sub("tbl_booking",$where);
			if($res==1){
				/*$this->session->set_flashdata('mes_alert','success');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Successfully choose subjects');
				$rets = array('status' => 1,'message' => 'Successfully choose subjects');
				*/
				$load = $this->lang->line('successfullychoose');
				$rets = array('status' => 1,'message' => $load);
				//redirect('/tutor/choose');
			}else if($res==0){
				/*
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Failed, you have choosed that subject');
				*/
				$load = $this->lang->line('failedchoosen');
				$rets = array('status' => -1,'message' => $load);
				//redirect('/tutor/choose');
			}else{
				$load = $this->lang->line('erroroccured');
				$rets = array('status' => -1,'message' => $load);
			}
			echo json_encode($rets);
		}

		function choosesub(){
			$id_user = $this->input->get('id_user');
			$subject_id = $this->input->get('subject_id');

			$where = array(
				'id_user' => $id_user,
				'subject_id' => $subject_id
				);

			$res = $this->Master_model->choose_sub("tbl_booking",$where);
			if($res==1){
				/*$this->session->set_flashdata('mes_alert','success');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Successfully choose subjects');
				$rets = array('status' => 1,'message' => 'Successfully choose subjects');
				*/
				$load = $this->lang->line('successfullychoose');
				$rets = array('status' => 1,'message' => $load);
				//redirect('/tutor/choose');
			}else if($res==0){
				/*
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Failed, you have choosed that subject');
				*/
				$load = $this->lang->line('failedchoosen');
				$rets = array('status' => -1,'message' => $load);
				//redirect('/tutor/choose');
			}else{
				$load = $this->lang->line('erroroccured');
				$rets = array('status' => -1,'message' => $load);
			}
			echo json_encode($rets);
		}

		function unchoosesub(){
			$id_user = $this->input->get('id_user');
			$subject_id = $this->input->get('subject_id');

			$where = array(
				'id_user' => $id_user,
				'subject_id' => $subject_id
			);

			$res = $this->Master_model->unchoose_sub("tbl_booking",$where);
			if($res==1){
				/*
				$this->session->set_flashdata('mes_alert','success');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Successfully unfollow subjects');	
				*/
				//redirect('/tutor/choose');
				$load = $this->lang->line('successfullyunfollow');
				$rets = array('status' => 1,'message' => $load);
			}else if($res==0){
				/*
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Failed, you have choosed that subject');
				redirect('/tutor/choose');
				*/
				$load = $this->lang->line('failedunchoose');
				$rets = array('status' => -1,'message' => $load);
			}else{
				$load = $this->lang->line('erroroccured');
				$rets = array('status' => -1,'message' => $load);
			}
			echo json_encode($rets);
		}

		function requestsub(){
			$id_user = $this->input->get('id_user');
			$subject_id = $this->input->get('subject_id');

			$where = array(
				'id_user' => $id_user,
				'subject_id' => $subject_id
			);

			$res = $this->Master_model->request_sub("tbl_booking",$where);
			if($res==1){
				/*
				$this->session->set_flashdata('mes_alert','success');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Successfully unfollow subjects');	
				*/
				//redirect('/tutor/choose');
				$load = $this->lang->line('successfullyunfollow');
				$rets = array('status' => 1,'message' => 'Request successfully');

				$user_tutor = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$id_user'")->row_array();
				$subject = $this->db->query("SELECT * FROM master_subject WHERE subject_id='$subject_id'")->row_array();
				$booked = $this->db->query("SELECT * FROM tbl_booking WHERE subject_id='".$subject['subject_id']."' AND id_user='$id_user' ")->row_array();
				$selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='".$subject['jenjang_id']."'")->row_array();
	 
				$subject_name = $subject['subject_name'];
				$user_nametutor = $user_tutor['user_name'];
				$user_email 	= $user_tutor['email'];
				$emailkedua = "hermawan@meetaza.com";
				$emailketiga = "support@classmiles.com";
				$jenjang_name=$selectnew['jenjang_name'];
				$jenjang_level=$selectnew['jenjang_level'];
				

				// Open connection
		        $ch = curl_init();

		        // Set the url, number of POST vars, POST data
		        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
		        curl_setopt($ch, CURLOPT_POST, true);
		        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_requestSubject','content' => array("emailkedua" => "$emailkedua", "emailketiga" => "$emailketiga", "subject_name" => "$subject_name", "jenjang_level" => "$jenjang_level", "jenjang_name" => "$jenjang_name", "user_email" => "$user_email", "user_nametutor" => "$user_nametutor" ) )));

		        // Execute post
		        $result = curl_exec($ch);
		        curl_close($ch);
		        // echo $result;


	            if ($result) {
	                  $load = $this->lang->line('successfullysent');
	                  $this->session->set_flashdata('cek',$res);
	                  $this->session->set_flashdata('mes_alert','success');
	                  $this->session->set_flashdata('mes_display','block');
	                  $this->session->set_flashdata('mes_message',$load);   
	                  redirect('/');
	            }else{
	                  $load = $this->lang->line('failed');
	                  $this->session->set_flashdata('mes_alert','danger');
	                  $this->session->set_flashdata('mes_display','block');
	                  $this->session->set_flashdata('mes_message',$load);   
	                  redirect('/');    
	            }

			}else if($res==0){
				/*
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Failed, you have choosed that subject');
				redirect('/tutor/choose');
				*/
				$load = $this->lang->line('failedunchoose');
				$rets = array('status' => -1,'message' => $load);
			}else{
				$load = $this->lang->line('erroroccured');
				$rets = array('status' => -1,'message' => $load);
			}
			echo json_encode($rets);
		}

		function choosesub_mobile(){
			$id_user = $this->input->get('id_user');
			$subject_id = $this->input->get('subject_id');

			$where = array(
				'id_user' => $id_user,
				'subject_id' => $subject_id
				);

			$res = $this->Master_model->choose_sub("tbl_booking",$where);
			if($res==1){
				/*$this->session->set_flashdata('mes_alert','success');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Successfully choose subjects');
				$rets = array('status' => 1,'message' => 'Successfully choose subjects');
				*/
				$load = $this->lang->line('successfullychoose');
				$rets = array('status' => 1,'message' => $load);
				redirect('/tutor/choose');
			}else if($res==0){
				/*
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Failed, you have choosed that subject');
				*/
				$load = $this->lang->line('failedchoosen');
				$rets = array('status' => -1,'message' => $load);
				redirect('/tutor/choose');
			}else{
				$load = $this->lang->line('erroroccured');
				$rets = array('status' => -1,'message' => $load);
			}
			echo json_encode($rets);
		}

		function unchoosesub_mobile(){
			$id_user = $this->input->get('id_user');
			$subject_id = $this->input->get('subject_id');

			$where = array(
				'id_user' => $id_user,
				'subject_id' => $subject_id
				);


			$res = $this->Master_model->unchoose_sub("tbl_booking",$where);
			if($res==1){
				/*
				$this->session->set_flashdata('mes_alert','success');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Successfully unfollow subjects');	
				*/
				redirect('/tutor/choose');
				$load = $this->lang->line('successfullyunfollow');
				$rets = array('status' => 1,'message' => $load);
			}else if($res==0){
				/*
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Failed, you have choosed that subject');
				redirect('/tutor/choose');
				*/
				$load = $this->lang->line('failedunchoose');
				$rets = array('status' => -1,'message' => $load);
			}else{
				$load = $this->lang->line('erroroccured');
				$rets = array('status' => -1,'message' => $load);
			}
			echo json_encode($rets);
		}

		function logout(){
			// echo $this->session->set_flashdata('msgLogout','Log Out Success');
			$this->session->sess_destroy();
			delete_cookie('CMKEY');
			$load = $this->lang->line('logsuccess');
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block'); 
			$this->session->set_flashdata('mes_message',$load);			
			$this->session->set_userdata('lang','indonesia');
			


			// Initialize the session.
			// If you are using session_name("something"), don't forget it now!
			session_start();

			// Unset all of the session variables.
			$_SESSION = array();

			// If it's desired to kill the session, also delete the session cookie.
			// Note: This will destroy the session, and not just the session data!
			if (ini_get("session.use_cookies")) {
			    $params = session_get_cookie_params();
			    setcookie(session_name(), '', time() - 42000,
			        $params["path"], $params["domain"],
			        $params["secure"], $params["httponly"]
			    );
			}

			// Finally, destroy the session.
			session_destroy();
			redirect('/');
		}

		public function set_availability($tutor_id)
		{

			// print_r('<pre>');
			// print_r($this->input->post());
			// print_r('</pre>');
			// return null;
			$res = $this->Master_model->set_availability($this->input->post(),$tutor_id);
			// print_r($res);
			$this->session->set_flashdata('rets',$res);

			redirect('/tutor/availability');
		}
		function get_level(){
			$id_jenjang = $this->input->post('id_jenjang');
			$dataJenjanglevel = $this->Master_model->getDataJenjanglevel($id_jenjang);
			echo '<select name="jenjang_level" id="jenjang_level">';
			foreach ($dataJenjanglevel as $a) {
				echo '<option value="'.$a->id_jenjang.'">'.$a->jenjang_level.'</option>';
			}
			echo '</select>';
		}
		public function tutor_submitApproval($id_user = '')
		{
			if($this->input->post() != ''){
				$form_account = json_decode($this->input->post('form_account'),true);
				$form_profile = json_decode($this->input->post('form_profile'),true);
		        $basename           = $sessioniduser.$this->Rumus->RandomString();
		    	$img_name      		= $basename.'.jpg';
		    	$bukti 				= isset($datas["image"]) ? $datas["image"] : "";

				$table_param = null;
				$value_param = null;
				foreach ($form_account as $key => $value) {
					if($key != 'submit' && $key != 'imgdata'){
						$table_param.= $key."='".$value."', ";	
					}
					
				}
			
					file_put_contents('./aset/_temp_images/'.$img_name,file_get_contents("data://".$form_account['imgdata']));

					$config['image_library'] = 'gd2';
					$config['source_image']	= './aset/_temp_images/'.$img_name;
					$config['width']	= 512;
					$config['maintain_ratio'] = FALSE;
					$config['height']	= 512;

					$this->image_lib->initialize($config); 

					$this->image_lib->resize();
					$this->Rumus->sendToCDN('user', $img_name, null, 'aset/_temp_images/'.$img_name );

					$table_param.= "user_image='".base64_encode('user/'.$img_name)."', ";	
					$this->session->set_userdata('user_image', CDN_URL.USER_IMAGE_CDN_URL.base64_encode('user/'.$img_name));
			
				$table_param = trim($table_param,', ');
				$q1 = "UPDATE tbl_user SET ".$table_param." WHERE id_user='".$id_user."'";
				if(!$this->db->simple_query($q1)){
					echo 2;
					return null;
				}

				// print_r($form_profile);
				$edu_array = array();
				if(is_array($form_profile['educational_level'])){
					foreach ($form_profile['educational_level'] as $key => $value) {
						$edu_array[$key]['educational_level'] = $form_profile['educational_level'][$key];
						$edu_array[$key]['educational_competence'] = $form_profile['educational_competence'][$key];
						$edu_array[$key]['educational_institution'] = $form_profile['educational_institution'][$key];
						$edu_array[$key]['institution_address'] = $form_profile['institution_address'][$key];
						$edu_array[$key]['graduation_year'] = $form_profile['graduation_year'][$key];
					}
				}else{
					$edu_array[0]['educational_level'] = $form_profile['educational_level'];
					$edu_array[0]['educational_competence'] = $form_profile['educational_competence'];
					$edu_array[0]['educational_institution'] = $form_profile['educational_institution'];
					$edu_array[0]['institution_address'] = $form_profile['institution_address'];
					$edu_array[0]['graduation_year'] = $form_profile['graduation_year'];
				}
				print_r($edu_array);

				$exp_array = array();
				if(is_array($form_profile['description_experience'])){
					foreach ($form_profile['description_experience'] as $key => $value) {
						$exp_array[$key]['description_experience'] = $form_profile['description_experience'][$key];
						$exp_array[$key]['year_experience1'] = $form_profile['year_experience1'][$key];
						$exp_array[$key]['year_experience2'] = $form_profile['year_experience2'][$key];
						$exp_array[$key]['place_experience'] = $form_profile['place_experience'][$key];
						$exp_array[$key]['information_experience'] = $form_profile['information_experience'][$key];
					}
				}else{
					$exp_array[0]['description_experience'] = $form_profile['description_experience'];
					$exp_array[0]['year_experience1'] = $form_profile['year_experience1'];
					$exp_array[0]['year_experience2'] = $form_profile['year_experience2'];
					$exp_array[0]['place_experience'] = $form_profile['place_experience'];
					$exp_array[0]['information_experience'] = $form_profile['information_experience'];
				}
				print_r($exp_array);

				$edu_array = serialize($edu_array);
				$exp_array = serialize($exp_array);

				$check_pexist = $this->db->query("SELECT * FROM tbl_profile_tutor WHERE tutor_id='".$id_user."'")->row_array();
				if(!empty($check_pexist)){
					$this->db->simple_query("UPDATE tbl_profile_tutor SET education_background='$edu_array', teaching_experience='$exp_array' WHERE tutor_id='".$id_user."'");
				}else{
					$this->db->simple_query("INSERT INTO tbl_profile_tutor(tutor_id,education_background,teaching_experience) VALUES('$id_user','$edu_array','$exp_array')");
				}
				$this->db->simple_query("UPDATE tbl_user SET status=2 WHERE id_user='".$id_user."'");

				//SEND NOTIF
				$idadmin = $this->db->query("SELECT id_user, user_name FROM  tbl_user WHERE usertype_id ='admin'")->row_array();
				$adminid = $idadmin['id_user'];				
				$adminname = $idadmin['user_name'];	

				//NOTIF TUTOR
				$notif_message = json_encode( array("code" => 44));					
				$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','','0')");

				//NOTIF ADMIN
				$notif_message = json_encode( array("code" => 45, "tutor_name" => $adminname));					
				$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','5','https://classmiles.com/admin/tutor_approval','0')");

				// $simpan_notif = $this->Rumus->create_notif($notif='Permintaan Anda telah dikirim, silakan tunggu persetujuan dari Tim Classmiles. Terima kasih.',$notif_mobile='Permintaan Anda telah dikirim, silakan tunggu persetujuan dari Tim Classmiles. Terima kasih.',$notif_type='single',$id_user=$id_user,$link='');

				// $send_notif_admin = $this->Rumus->create_notif($notif='Hey admin '.$adminname.', Terdapat Tutor yang baru registrasi, silahkan cek di menu persetujuan.',$notif_mobile='Hey admin '.$adminname.', Terdapat Tutor yang baru registrasi, silahkan cek di menu persetujuan.',$notif_type='single',$id_user=$adminid,$link='https://classmiles.com/admin/tutor_approval');				
				// END NOTIF

				$user_tutor = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$id_user'")->row_array();
				$user_nametutor = $user_tutor['user_name'];
				$user_email 	= $user_tutor['email'];

				// Open connection
	            $ch = curl_init();

	            // Set the url, number of POST vars, POST data
	            curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/Rest/email_tutor_submit_approval');
	            curl_setopt($ch, CURLOPT_POST, true);
	            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	            // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

	            // Execute post
	            $result = curl_exec($ch);
	            curl_close($ch);
	            // echo $result;

								
			}
		}


		public function tutor_tempApproval($id_user = '')
		{
			if($this->input->post() != ''){
				$form_account = json_decode($this->input->post('form_account'),true);
				$form_profile = json_decode($this->input->post('form_profile'),true);
		        $basename           = $sessioniduser.$this->Rumus->RandomString();
		    	$img_name      		= $basename.'.jpg';
		    	$bukti 				= isset($datas["image"]) ? $datas["image"] : "";

				$table_param = null;
				$value_param = null;
				foreach ($form_account as $key => $value) {
					if($key != 'submit' && $key != 'imgdata'){
						$table_param.= $key."='".$value."', ";	
					}
					
				}
			
					file_put_contents('./aset/_temp_images/'.$img_name,file_get_contents("data://".$form_account['imgdata']));

					$config['image_library'] = 'gd2';
					$config['source_image']	= './aset/_temp_images/'.$img_name;
					$config['width']	= 512;
					$config['maintain_ratio'] = FALSE;
					$config['height']	= 512;

					$this->image_lib->initialize($config); 

					$this->image_lib->resize();
					$this->Rumus->sendToCDN('user', $img_name, null, 'aset/_temp_images/'.$img_name );

					$table_param.= "user_image='".base64_encode('user/'.$img_name)."', ";	
					$this->session->set_userdata('user_image', CDN_URL.USER_IMAGE_CDN_URL.base64_encode('user/'.$img_name));
			
				$table_param = trim($table_param,', ');
				$q1 = "UPDATE tbl_user SET ".$table_param." WHERE id_user='".$id_user."'";
				if(!$this->db->simple_query($q1)){
					echo 2;
					return null;
				}

				// print_r($form_profile);
				$edu_array = array();
				if(is_array($form_profile['educational_level'])){
					foreach ($form_profile['educational_level'] as $key => $value) {
						$edu_array[$key]['educational_level'] = $form_profile['educational_level'][$key];
						$edu_array[$key]['educational_competence'] = $form_profile['educational_competence'][$key];
						$edu_array[$key]['educational_institution'] = $form_profile['educational_institution'][$key];
						$edu_array[$key]['institution_address'] = $form_profile['institution_address'][$key];
						$edu_array[$key]['graduation_year'] = $form_profile['graduation_year'][$key];
					}
				}else{
					$edu_array[0]['educational_level'] = $form_profile['educational_level'];
					$edu_array[0]['educational_competence'] = $form_profile['educational_competence'];
					$edu_array[0]['educational_institution'] = $form_profile['educational_institution'];
					$edu_array[0]['institution_address'] = $form_profile['institution_address'];
					$edu_array[0]['graduation_year'] = $form_profile['graduation_year'];
				}
				print_r($edu_array);

				$exp_array = array();
				if(is_array($form_profile['description_experience'])){
					foreach ($form_profile['description_experience'] as $key => $value) {
						$exp_array[$key]['description_experience'] = $form_profile['description_experience'][$key];
						$exp_array[$key]['year_experience1'] = $form_profile['year_experience1'][$key];
						$exp_array[$key]['year_experience2'] = $form_profile['year_experience2'][$key];
						$exp_array[$key]['place_experience'] = $form_profile['place_experience'][$key];
						$exp_array[$key]['information_experience'] = $form_profile['information_experience'][$key];
					}
				}else{
					$exp_array[0]['description_experience'] = $form_profile['description_experience'];
					$exp_array[0]['year_experience1'] = $form_profile['year_experience1'];
					$exp_array[0]['year_experience2'] = $form_profile['year_experience2'];
					$exp_array[0]['place_experience'] = $form_profile['place_experience'];
					$exp_array[0]['information_experience'] = $form_profile['information_experience'];
				}
				print_r($exp_array);

				$edu_array = serialize($edu_array);
				$exp_array = serialize($exp_array);

				$check_pexist = $this->db->query("SELECT * FROM tbl_profile_tutor WHERE tutor_id='".$id_user."'")->row_array();
				if(!empty($check_pexist)){
					$this->db->simple_query("UPDATE tbl_profile_tutor SET education_background='$edu_array', teaching_experience='$exp_array' WHERE tutor_id='".$id_user."'");
				}else{
					$this->db->simple_query("INSERT INTO tbl_profile_tutor(tutor_id,education_background,teaching_experience) VALUES('$id_user','$edu_array','$exp_array')");
				}
				

				// $this->session->set_userdata('status','3');
				// $load = $this->lang->line('requestsent');
				// $this->session->set_userdata('code','555');
				// $rets['mes_alert'] = 'info';
				// $rets['mes_display'] = 'block';
				// $rets['mes_message'] = $load;
				// $this->session->set_flashdata('mes_alert','warning');
				// $this->session->set_flashdata('mes_display','block');
				// $this->session->set_flashdata('mes_message',$load);
				// $this->session->set_userdata('message', 'requestsent');
				// $this->session->set_flashdata('rets',$rets);
								
				return 1;				
			}
		}

		public function tps_me_backup($session_id='')
		{
			$iduser = $this->session->userdata('id_user');
			$templateweb = $this->input->get('t');
			$linkback = BASE_URL().'/MyClass';
			$student = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
			if(!empty($student)){
				$hash_id = $student['session_id'];
			}else{
				$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
				echo $hash_id;
				$this->db->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
			}	
			$load = $this->lang->line('logsuccess');					
			$dpt_subject_name = $this->db->query("SELECT name FROM tbl_class WHERE class_id='$session_id'")->row_array();
			$action = "$load ".$dpt_subject_name['name'];
			$log_user_class = $this->db->query("INSERT INTO log_user (action, id_user) VALUES ('$action','$iduser')");	
			// header('Location: https://classmiles.com/classroom/broadcast_wd?access_session='.$hash_id);
			if ($templateweb == "whiteboard_digital") {
				header('Location: https://classmiles.com/classroom/broadcast_wd?access_session='.$hash_id);
			}
			else if ($templateweb == "whiteboard_videoboard") {
				header('Location: https://classmiles.com/classroom/broadcast_wv?access_session='.$hash_id);
			}
			else if ($templateweb == "whiteboard_no")
			{
				header('Location: https://classmiles.com/classroom/broadcast?access_session='.$hash_id);
			}

			// header('Location: https://classmiles.com/classroom/#access_session='.$hash_id);
			// header('Location: https://classmiles.com/classroom/#access_session=e2eb87d310b5252d61614438f2c1bc49a94b599b');
			// header('Location: https://classmiles.com/Classroom/session?access_session='.$hash_id);
			// header('Location: https://classmiles.com/classroom/session?access_session='.$hash_id);
			// header('Location: https://classmiles.com/classroom/room?access_session='.$hash_id);
			// header('Location: https://classmiles.com/classroom/room_class_video?access_session='.$hash_id);
			// header('Location: https://classmiles.com/classroom/private_proccess?access_session='.$hash_id);
			exit();
		}

		public function tps_me($session_id='')
		{
			$id_user = $this->session->userdata('id_user');						
			$checkstudentonline = $this->db->query("SELECT * from temp_user_online where id_user = '".$id_user."' ")->row_array();
		    if (!empty($checkstudentonline)) {
		        $this->session->set_flashdata('mes_alert','failed');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Sorry, your now in class');	
				$this->session->set_userdata('tutorinclass', '4044');
				redirect('/');
		    } 
		    else {
				$DB2 = $this->load->database('db2', TRUE); // SAY SEA SERVER CLASSPLAY DB
				// $DB_SG = $this->load->database('db_sg', TRUE); // SAY SEA SERVER CLASSPLAY DB

				$cm_continent = "id";
				$iduser = $this->session->userdata('id_user');
				// $andro 	= $this->input->get('p');
				$andro = $_GET['p'];				
				if ($andro != 'android') {
					$linkback = BASE_URL().'MyClass';
				}
				else
				{
					$linkback = '';
				}

				if ($iduser == null) {				
					$iduser = $this->input->get('id_user');
				}
				$templateweb = $this->input->get('t');			

				$db_res = $this->db->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
				$room_active_janus = $this->db->query("SELECT * FROM temp_active_room WHERE class_id='$session_id'")->row_array();
				if(!empty($room_active_janus)){
					$cm_continent = $room_active_janus['cm_continent'];
				}else{
					// TAKE ALL PARTICIPANT IN THAT CLASS
					$visible = json_decode($db_res['participant'],true);
					$incoming_kuota = 0;
					$local_region = 0;
					$interlocal_region = 0;
					$chosen_janus = "";

					$country_code = $this->db->query("SELECT user_countrycode, usertype_id FROM tbl_user WHERE id_user='".$db_res['tutor_id']."'")->row_array()['user_countrycode'];
					if($country_code == "+62" || $country_code == "62"){
						$local_region++;
					}else{
						$interlocal_region++;
					}
					$incoming_kuota++;

					foreach ($visible['participant'] as $k => $value) {
						$id_user = $value['id_user'];
						$data_ccode_utype = $this->db->query("SELECT user_countrycode, usertype_id FROM tbl_user WHERE id_user='$id_user'")->row_array();
						$country_code = $data_ccode_utype['user_countrycode'];
						$usertype_id = $data_ccode_utype['usertype_id'];
						if($usertype_id != "student kid"){
							if($country_code == "+62" || $country_code == "62"){
								$local_region++;
							}else{
								$interlocal_region++;
							}
						}
						$incoming_kuota++;
					}

					$ttl_active_kuota = $this->db->query("SELECT janus, SUM(kuota) as kuota FROM temp_active_room GROUP BY janus")->result_array();
					if($local_region > $interlocal_region){
						$cm_continent = "id";
						// PRESENTASE TERBESAR INDONESIA
						// $list_janus = array("c3.classmiles.com" => 0,"c1.classmiles.com" => 0,"c2.classmiles.com" => 0);
						// $list_janus = array("c4.classmiles.com" => 0, "c3.classmiles.com" => 0);
						$list_janus = array("c2.classmiles.com" => 0);
						foreach ($ttl_active_kuota as $key => $value) {
							if(in_array($value['janus'], $list_janus)){
								$list_janus[$value['janus']] = $value['kuota'];
							}
						}
						foreach ($list_janus as $key => $value) {
							if($value < 200 && ($value+$incoming_kuota) <= 200){
								$chosen_janus = $key;
								break;
							}
						}
						if($chosen_janus == "")
							$chosen_janus = "c2.classmiles.com"; //JANUS SAMPAH
					}else{
						$cm_continent = "id";
						// $list_janus = array("c102.classmiles.com" => 0);
						$list_janus = array("c2.classmiles.com" => 0);
						foreach ($ttl_active_kuota as $key => $value) {
							if(in_array($value['janus'], $list_janus)){
								$list_janus[$value['janus']] = $value['kuota'];
							}
						}
						foreach ($list_janus as $key => $value) {
							if($value < 200 && ($value+$incoming_kuota) <= 200){
								$chosen_janus = $key;
								break;
							}
						}
						if($chosen_janus == "")
							$chosen_janus = "c2.classmiles.com"; //JANUS SAMPAH
					}
					$this->db->simple_query("INSERT INTO temp_active_room VALUES(NULL,'$cm_continent','$chosen_janus','$session_id',$incoming_kuota,now());");
					$this->janus_server = $chosen_janus;
				}

				if($cm_continent == "id"){

					$hs_id 		= $this->M_login->second_genses($this->session->userdata('email').time());

					// Open connection
			        $ch = curl_init();
			    	// Set the url, number of POST vars, POST data
					curl_setopt($ch, CURLOPT_URL, 'https://id.indiclass.id/V1.0.0/checkParticipants');
					curl_setopt($ch, CURLOPT_POST, 1);
					// curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

					curl_setopt($ch, CURLOPT_POSTFIELDS,
			            'hash_id='.$hs_id.'&session_id='.$session_id.'&id_user='.$iduser);

			    	// Execute post
					$result = curl_exec($ch);
					curl_close($ch);
					
					$a = json_decode($result);
					$hash_id = $a->hash_id;					

					// $student = $DB2->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
					// if(!empty($student)){
					// 	$hash_id = $student['session_id'];
					// }else{
					// 	$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
					// 	echo $hash_id;
					// 	$DB2->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
					// }
					$data_diri = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$iduser'")->row_array();

					$qr = "";
					$qr_data = "";
					$setter = "";
					foreach ($data_diri as $key => $value) {
						if($key == "username" && $value == NULL){

						}else{
							$qr.= $key.", ";
							$qr_data.= "'$value', ";
							$setter.= "$key='$value', ";
						}
					}
					$qr = rtrim($qr, ", ");
					$qr_data = rtrim($qr_data, ", ");
					$setter = rtrim($setter, ", ");
					$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_user($qr) VALUES($qr_data)");
					if(!$insert_tbl_user) {
						$DB2->simple_query("UPDATE tbl_user SET $setter WHERE id_user='$iduser'");
					}

					$classplay_exist = $DB2->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
					if(empty($classplay_exist) || $classplay_exist['subject_id'] == '0' ){
						$qc = "";
						$qc_data = "";
						$setter = "";
						foreach ($db_res as $key => $value) {
							$qc.= $key.", ";
							$qc_data.= "'".$this->db->escape_str($value)."', ";
							$setter.= "$key='$value', ";
						}
						$qc = rtrim($qc, ", ");
						$qc_data = rtrim($qc_data, ", ");
						$setter = rtrim($setter, ", ");
						$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_class($qc) VALUES($qc_data)");
						if(!$insert_tbl_user){
							$DB2->simple_query("UPDATE tbl_class SET $setter WHERE class_id='$session_id'");
						}
					}
					
				}

				$load = $this->lang->line('logsuccess');					
				$dpt_subject_name = $this->db->query("SELECT name FROM tbl_class WHERE class_id='$session_id'")->row_array();
				$action = "$load ".$dpt_subject_name['name'];
				$log_user_class = $this->db->query("INSERT INTO log_user (action, id_user) VALUES ('$action','$iduser')");
				// $this->session->set_userdata('p',$andro);
				// header('Location: https://classmiles.com/classroom/broadcast_wd?access_session='.$hash_id);
				// echo "<script>alert('halo ini di master');</script>";
				// if ($templateweb == "whiteboard_digital") {
				// 	header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast_wd?access_session='.$hash_id.'&p='.$andro);
				// }
				// else if ($templateweb == "whiteboard_videoboard") {
				// 	header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast_wv?access_session='.$hash_id.'&p='.$andro);
				// }
				// else if ($templateweb == "whiteboard_no")
				// {
				// 	header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast?access_session='.$hash_id.'&p='.$andro);
				// }
				// else if ($templateweb == "all_featured")
				// {				
					header('Location: https://'.$cm_continent.'.indiclass.id/classroom/broadcast_all?access_session='.$hash_id.'&p='.$andro.'&back='.$linkback);
				// }				
				exit();
			}
		}

		public function tps_me_bcuuu($session_id='')
		{
			$id_user = $this->session->userdata('id_user');			
			$checkstudentonline = $this->db->query("SELECT * from temp_user_online where id_user = '".$id_user."' ")->row_array();
		    if (!empty($checkstudentonline)) {
		        $this->session->set_flashdata('mes_alert','failed');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Sorry, your now in class');	
				$this->session->set_userdata('tutorinclass', '4044');
				redirect('/');
		    } 
		    else {
				$DB2 = $this->load->database('db2', TRUE); // SAY SEA SERVER CLASSPLAY DB
				$DB_SG = $this->load->database('db_sg', TRUE); // SAY SEA SERVER CLASSPLAY DB

				$cm_continent = "id";
				$iduser = $this->session->userdata('id_user');
				// $andro 	= $this->input->get('p');
				$andro = $_GET['p'];
				if ($andro != 'android') {
					$linkback = BASE_URL().'MyClass';
				}
				else
				{
					$linkback = '';
				}

				if ($iduser == null) {				
					$iduser = $this->input->get('id_user');
				}
				$templateweb = $this->input->get('t');			

				$db_res = $this->db->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
				$room_active_janus = $this->db->query("SELECT * FROM temp_active_room WHERE class_id='$session_id'")->row_array();
				if(!empty($room_active_janus)){
					$cm_continent = $room_active_janus['cm_continent'];
				}else{
					// TAKE ALL PARTICIPANT IN THAT CLASS
					$visible = json_decode($db_res['participant'],true);
					$incoming_kuota = 0;
					$local_region = 0;
					$interlocal_region = 0;
					$chosen_janus = "";

					$country_code = $this->db->query("SELECT user_countrycode, usertype_id FROM tbl_user WHERE id_user='".$db_res['tutor_id']."'")->row_array()['user_countrycode'];
					if($country_code == "+62" || $country_code == "62"){
						$local_region++;
					}else{
						$interlocal_region++;
					}
					$incoming_kuota++;

					foreach ($visible['participant'] as $k => $value) {
						$id_user = $value['id_user'];
						$data_ccode_utype = $this->db->query("SELECT user_countrycode, usertype_id FROM tbl_user WHERE id_user='$id_user'")->row_array();
						$country_code = $data_ccode_utype['user_countrycode'];
						$usertype_id = $data_ccode_utype['usertype_id'];
						if($usertype_id != "student kid"){
							if($country_code == "+62" || $country_code == "62"){
								$local_region++;
							}else{
								$interlocal_region++;
							}
						}
						$incoming_kuota++;
					}

					$ttl_active_kuota = $this->db->query("SELECT janus, SUM(kuota) as kuota FROM temp_active_room GROUP BY janus")->result_array();
					if($local_region > $interlocal_region){
						$cm_continent = "id";
						// PRESENTASE TERBESAR INDONESIA
						// $list_janus = array("c3.classmiles.com" => 0,"c1.classmiles.com" => 0,"c2.classmiles.com" => 0);
						// $list_janus = array("c4.classmiles.com" => 0, "c3.classmiles.com" => 0);
						$list_janus = array("c2.classmiles.com" => 0, "c1.classmiles.com" => 0);
						foreach ($ttl_active_kuota as $key => $value) {
							if(in_array($value['janus'], $list_janus)){
								$list_janus[$value['janus']] = $value['kuota'];
							}
						}
						foreach ($list_janus as $key => $value) {
							if($value < 200 && ($value+$incoming_kuota) <= 200){
								$chosen_janus = $key;
								break;
							}
						}
						if($chosen_janus == "")
							$chosen_janus = "c2.classmiles.com"; //JANUS SAMPAH
					}else{
						$cm_continent = "sg";
						// $list_janus = array("c102.classmiles.com" => 0);
						$list_janus = array("c2.classmiles.com" => 0, "c1.classmiles.com" => 0);
						foreach ($ttl_active_kuota as $key => $value) {
							if(in_array($value['janus'], $list_janus)){
								$list_janus[$value['janus']] = $value['kuota'];
							}
						}
						foreach ($list_janus as $key => $value) {
							if($value < 200 && ($value+$incoming_kuota) <= 200){
								$chosen_janus = $key;
								break;
							}
						}
						if($chosen_janus == "")
							$chosen_janus = "c2.classmiles.com"; //JANUS SAMPAH
					}
					$this->db->simple_query("INSERT INTO temp_active_room VALUES(NULL,'$cm_continent','$chosen_janus','$session_id',$incoming_kuota,now());");
					$this->janus_server = $chosen_janus;
				}
				if($cm_continent == "id"){
					$student = $DB2->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
					if(!empty($student)){
						$hash_id = $student['session_id'];
					}else{
						$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
						echo $hash_id;
						$DB2->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
					}
					$data_diri = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$iduser'")->row_array();

					$qr = "";
					$qr_data = "";
					$setter = "";
					foreach ($data_diri as $key => $value) {
						if($key == "username" && $value == NULL){

						}else{
							$qr.= $key.", ";
							$qr_data.= "'$value', ";
							$setter.= "$key='$value', ";
						}
					}
					$qr = rtrim($qr, ", ");
					$qr_data = rtrim($qr_data, ", ");
					$setter = rtrim($setter, ", ");
					$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_user($qr) VALUES($qr_data)");
					if(!$insert_tbl_user) {
						$DB2->simple_query("UPDATE tbl_user SET $setter WHERE id_user='$iduser'");
					}

					$classplay_exist = $DB2->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
					if(empty($classplay_exist) || $classplay_exist['subject_id'] == '0' ){
						$qc = "";
						$qc_data = "";
						$setter = "";
						foreach ($db_res as $key => $value) {
							$qc.= $key.", ";
							$qc_data.= "'".$this->db->escape_str($value)."', ";
							$setter.= "$key='$value', ";
						}
						$qc = rtrim($qc, ", ");
						$qc_data = rtrim($qc_data, ", ");
						$setter = rtrim($setter, ", ");
						$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_class($qc) VALUES($qc_data)");
						if(!$insert_tbl_user){
							$DB2->simple_query("UPDATE tbl_class SET $setter WHERE class_id='$session_id'");
						}
					}
					
				}else if($cm_continent == 'sg') {
					$student = $DB_SG->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
					if(!empty($student)){
						$hash_id = $student['session_id'];
					}else{
						$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
						echo $hash_id;
						$DB_SG->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
					}
					$data_diri = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$iduser'")->row_array();

					$qr = "";
					$qr_data = "";
					$setter = "";
					foreach ($data_diri as $key => $value) {
						if($key == "username" && $value == NULL){

						}else{
							$qr.= $key.", ";
							$qr_data.= "'$value', ";
							$setter.= "$key='$value', ";
						}
					}
					$qr = rtrim($qr, ", ");
					$qr_data = rtrim($qr_data, ", ");
					$setter = rtrim($setter, ", ");
					$insert_tbl_user = $DB_SG->simple_query("INSERT INTO tbl_user($qr) VALUES($qr_data)");
					if(!$insert_tbl_user) {
						$DB_SG->simple_query("UPDATE tbl_user SET $setter WHERE id_user='$iduser'");
					}

					$classplay_exist = $DB_SG->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
					if(empty($classplay_exist) || $classplay_exist['subject_id'] == '0'){
						$qc = "";
						$qc_data = "";
						$setter = "";
						foreach ($db_res as $key => $value) {
							$qc.= $key.", ";
							$qc_data.= "'$value', ";
							$setter.= "$key='$value', ";
						}
						$qc = rtrim($qc, ", ");
						$qc_data = rtrim($qc_data, ", ");
						$setter = rtrim($setter, ", ");
						$insert_tbl_user = $DB_SG->simple_query("INSERT INTO tbl_class($qc) VALUES($qc_data)");
						if(!$insert_tbl_user){
							$DB_SG->simple_query("UPDATE tbl_class SET $setter WHERE class_id='$session_id'");
						}
					}
				}

				$load = $this->lang->line('logsuccess');					
				$dpt_subject_name = $this->db->query("SELECT name FROM tbl_class WHERE class_id='$session_id'")->row_array();
				$action = "$load ".$dpt_subject_name['name'];
				$log_user_class = $this->db->query("INSERT INTO log_user (action, id_user) VALUES ('$action','$iduser')");
				// $this->session->set_userdata('p',$andro);
				// header('Location: https://classmiles.com/classroom/broadcast_wd?access_session='.$hash_id);
				// echo "<script>alert('halo ini di master');</script>";
				if ($templateweb == "whiteboard_digital") {
					header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast_wd?access_session='.$hash_id.'&p='.$andro);
				}
				else if ($templateweb == "whiteboard_videoboard") {
					header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast_wv?access_session='.$hash_id.'&p='.$andro);
				}
				else if ($templateweb == "whiteboard_no")
				{
					header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast?access_session='.$hash_id.'&p='.$andro);
				}
				else if ($templateweb == "all_featured")
				{				
					header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast_all?access_session='.$hash_id.'&p='.$andro.'&back='.$linkback);
				}				
				exit();
			}
		}

		public function tps_me_wb($session_id='')
		{
			$id_user = $this->session->userdata('id_user');
			// $checkstudentonline = $this->db->query("SELECT * from temp_user_online where id_user = '".$id_user."' ")->row_array();
		 //    if (!empty($checkstudentonline)) {
		 //        $this->session->set_flashdata('mes_alert','failed');
			// 	$this->session->set_flashdata('mes_display','block');
			// 	$this->session->set_flashdata('mes_message','Sorry, your now in class');	
			// 	$this->session->set_userdata('tutorinclass', '4044');
			// 	redirect('/');
		 //    } 
		 //    else {
				$DB2 = $this->load->database('db2', TRUE); // SAY SEA SERVER CLASSPLAY DB
				$DB_SG = $this->load->database('db_sg', TRUE); // SAY SEA SERVER CLASSPLAY DB

				$cm_continent = "id";
				$iduser = $this->session->userdata('id_user');
				// $andro 	= $this->input->get('p');
				$andro = $_GET['p'];
				if ($iduser == null) {				
					$iduser = $this->input->get('id_user');
				}
				$templateweb = $this->input->get('t');			

				$db_res = $this->db->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
				$room_active_janus = $this->db->query("SELECT * FROM temp_active_room WHERE class_id='$session_id'")->row_array();
				if(!empty($room_active_janus)){
					$cm_continent = $room_active_janus['cm_continent'];
				}else{
					// TAKE ALL PARTICIPANT IN THAT CLASS
					$visible = json_decode($db_res['participant'],true);
					$incoming_kuota = 0;
					$local_region = 0;
					$interlocal_region = 0;
					$chosen_janus = "";

					$country_code = $this->db->query("SELECT user_countrycode FROM tbl_user WHERE id_user='".$db_res['tutor_id']."'")->row_array()['user_countrycode'];
					if($country_code == "+62" || $country_code == "62"){
						$local_region++;
					}else{
						$interlocal_region++;
					}
					$incoming_kuota++;

					foreach ($visible['participant'] as $k => $value) {
						$id_user = $value['id_user'];
						$data_ccode_utype = $this->db->query("SELECT user_countrycode, usertype_id FROM tbl_user WHERE id_user='$id_user'")->row_array();
						$country_code = $data_ccode_utype['user_countrycode'];
						$usertype_id = $data_ccode_utype['usertype_id'];
						if($usertype_id != "student kid"){
							if($country_code == "+62" || $country_code == "62"){
								$local_region++;
							}else{
								$interlocal_region++;
							}
						}
						$incoming_kuota++;
						
					}

					$ttl_active_kuota = $this->db->query("SELECT janus, SUM(kuota) as kuota FROM temp_active_room GROUP BY janus")->result_array();
					if($local_region > $interlocal_region){
						$cm_continent = "id";
						// PRESENTASE TERBESAR INDONESIA
						// $list_janus = array("c3.classmiles.com" => 0,"c1.classmiles.com" => 0,"c2.classmiles.com" => 0);
						// $list_janus = array("c4.classmiles.com" => 0, "c3.classmiles.com" => 0);
						$list_janus = array("c2.classmiles.com" => 0, "c1.classmiles.com" => 0);
						foreach ($ttl_active_kuota as $key => $value) {
							if(in_array($value['janus'], $list_janus)){
								$list_janus[$value['janus']] = $value['kuota'];
							}
						}
						foreach ($list_janus as $key => $value) {
							if($value < 200 && ($value+$incoming_kuota) <= 200){
								$chosen_janus = $key;
								break;
							}
						}
						if($chosen_janus == "")
							$chosen_janus = "c2.classmiles.com"; //JANUS SAMPAH
					}else{
						$cm_continent = "sg";
						// $list_janus = array("c102.classmiles.com" => 0);
						$list_janus = array("c2.classmiles.com" => 0, "c1.classmiles.com" => 0);
						foreach ($ttl_active_kuota as $key => $value) {
							if(in_array($value['janus'], $list_janus)){
								$list_janus[$value['janus']] = $value['kuota'];
							}
						}
						foreach ($list_janus as $key => $value) {
							if($value < 200 && ($value+$incoming_kuota) <= 200){
								$chosen_janus = $key;
								break;
							}
						}
						if($chosen_janus == "")
							$chosen_janus = "c2.classmiles.com"; //JANUS SAMPAH
					}
					$this->db->simple_query("INSERT INTO temp_active_room VALUES(NULL,'$cm_continent','$chosen_janus','$session_id',$incoming_kuota,now());");
					$this->janus_server = $chosen_janus;
				}
				if($cm_continent == "id"){
					$student = $DB2->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
					if(!empty($student)){
						$hash_id = $student['session_id'];
					}else{
						$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
						echo $hash_id;
						$DB2->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
					}
					$data_diri = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$iduser'")->row_array();

					$qr = "";
					$qr_data = "";
					$setter = "";
					foreach ($data_diri as $key => $value) {
						if($key == "username" && $value == NULL){

						}else{
							$qr.= $key.", ";
							$qr_data.= "'$value', ";
							$setter.= "$key='$value', ";
						}
					}
					$qr = rtrim($qr, ", ");
					$qr_data = rtrim($qr_data, ", ");
					$setter = rtrim($setter, ", ");
					$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_user($qr) VALUES($qr_data)");
					if(!$insert_tbl_user) {
						$DB2->simple_query("UPDATE tbl_user SET $setter WHERE id_user='$iduser'");
					}

					$classplay_exist = $DB2->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
					if(empty($classplay_exist) || $classplay_exist['subject_id'] == '0' ){
						$qc = "";
						$qc_data = "";
						$setter = "";
						foreach ($db_res as $key => $value) {
							$qc.= $key.", ";
							$qc_data.= "'".$this->db->escape_str($value)."', ";
							$setter.= "$key='$value', ";
						}
						$qc = rtrim($qc, ", ");
						$qc_data = rtrim($qc_data, ", ");
						$setter = rtrim($setter, ", ");
						$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_class($qc) VALUES($qc_data)");
						if(!$insert_tbl_user){
							$DB2->simple_query("UPDATE tbl_class SET $setter WHERE class_id='$session_id'");
						}
					}
					
				}else if($cm_continent == 'sg') {
					$student = $DB_SG->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
					if(!empty($student)){
						$hash_id = $student['session_id'];
					}else{
						$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
						echo $hash_id;
						$DB_SG->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
					}
					$data_diri = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$iduser'")->row_array();

					$qr = "";
					$qr_data = "";
					$setter = "";
					foreach ($data_diri as $key => $value) {
						if($key == "username" && $value == NULL){

						}else{
							$qr.= $key.", ";
							$qr_data.= "'$value', ";
							$setter.= "$key='$value', ";
						}
					}
					$qr = rtrim($qr, ", ");
					$qr_data = rtrim($qr_data, ", ");
					$setter = rtrim($setter, ", ");
					$insert_tbl_user = $DB_SG->simple_query("INSERT INTO tbl_user($qr) VALUES($qr_data)");
					if(!$insert_tbl_user) {
						$DB_SG->simple_query("UPDATE tbl_user SET $setter WHERE id_user='$iduser'");
					}

					$classplay_exist = $DB_SG->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
					if(empty($classplay_exist) || $classplay_exist['subject_id'] == '0' ){
						$qc = "";
						$qc_data = "";
						$setter = "";
						foreach ($db_res as $key => $value) {
							$qc.= $key.", ";
							$qc_data.= "'$value', ";
							$setter.= "$key='$value', ";
						}
						$qc = rtrim($qc, ", ");
						$qc_data = rtrim($qc_data, ", ");
						$setter = rtrim($setter, ", ");
						$insert_tbl_user = $DB_SG->simple_query("INSERT INTO tbl_class($qc) VALUES($qc_data)");
						if(!$insert_tbl_user){
							$DB_SG->simple_query("UPDATE tbl_class SET $setter WHERE class_id='$session_id'");
						}
					}
				}

				$load = $this->lang->line('logsuccess');					
				$dpt_subject_name = $this->db->query("SELECT name FROM tbl_class WHERE class_id='$session_id'")->row_array();
				$action = "$load ".$dpt_subject_name['name'];
				$log_user_class = $this->db->query("INSERT INTO log_user (action, id_user) VALUES ('$action','$iduser')");
				// $this->session->set_userdata('p',$andro);
				// header('Location: https://classmiles.com/classroom/broadcast_wd?access_session='.$hash_id);
				// echo "<script>alert('halo ini di master');</script>";
				if ($templateweb == "whiteboard_digital") {
					header('Location: https://'.$cm_continent.'.classmiles.com/classroom/whiteboard_class?access_session='.$hash_id.'&p='.$andro);
				}
				else if ($templateweb == "whiteboard_videoboard") {
					header('Location: https://'.$cm_continent.'.classmiles.com/classroom/whiteboard_class?access_session='.$hash_id.'&p='.$andro);
				}
				else if ($templateweb == "whiteboard_no")
				{
					header('Location: https://'.$cm_continent.'.classmiles.com/classroom/whiteboard_class?access_session='.$hash_id.'&p='.$andro);
				}
				else if ($templateweb == "all_featured")
				{
					header('Location: https://'.$cm_continent.'.classmiles.com/classroom/whiteboard_class?access_session='.$hash_id.'&p='.$andro);
				}

				exit();
			// }
		}

		public function tps_me_kids($session_id='')
		{
			$id_user = $this->session->userdata('id_user');
			$checkstudentonline = $this->db->query("SELECT * from temp_user_online where id_user = '".$id_user."' ")->row_array();
		    if (!empty($checkstudentonline)) {
		        $this->session->set_flashdata('mes_alert','failed');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Sorry, your now in class');	
				$this->session->set_userdata('tutorinclass', '4044');
				redirect('/');
		    } 
		    else {
				$DB2 = $this->load->database('db2', TRUE); // SAY SEA SERVER CLASSPLAY DB
				$DB_SG = $this->load->database('db_sg', TRUE); // SAY SEA SERVER CLASSPLAY DB

				$cm_continent = "id";
				$iduser = $this->session->userdata('id_user');
				// $andro 	= $this->input->get('p');
				$andro = $_GET['p'];
				if ($iduser == null) {				
					$iduser = $this->input->get('id_user');
				}
				if ($andro != 'android') {
					$linkback = BASE_URL().'MyClass';
				}
				else
				{
					$linkback = '';
				}
				$templateweb = $this->input->get('t');			

				$db_res = $this->db->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
				$room_active_janus = $this->db->query("SELECT * FROM temp_active_room WHERE class_id='$session_id'")->row_array();
				if(!empty($room_active_janus)){
					$cm_continent = $room_active_janus['cm_continent'];
				}else{
					// TAKE ALL PARTICIPANT IN THAT CLASS
					$visible = json_decode($db_res['participant'],true);
					$incoming_kuota = 0;
					$local_region = 0;
					$interlocal_region = 0;
					$chosen_janus = "";

					$country_code = $this->db->query("SELECT user_countrycode FROM tbl_user WHERE id_user='".$db_res['tutor_id']."'")->row_array()['user_countrycode'];
					if($country_code == "+62" || $country_code == "62"){
						$local_region++;
					}else{
						$interlocal_region++;
					}
					$incoming_kuota++;

					foreach ($visible['participant'] as $k => $value) {
						$id_user = $value['id_user'];
						$data_ccode_utype = $this->db->query("SELECT user_countrycode, usertype_id FROM tbl_user WHERE id_user='$id_user'")->row_array();
						$country_code = $data_ccode_utype['user_countrycode'];
						$usertype_id = $data_ccode_utype['usertype_id'];
						if($usertype_id != "student kid"){
							if($country_code == "+62" || $country_code == "62"){
								$local_region++;
							}else{
								$interlocal_region++;
							}
						}
						$incoming_kuota++;
					}

					$ttl_active_kuota = $this->db->query("SELECT janus, SUM(kuota) as kuota FROM temp_active_room GROUP BY janus")->result_array();
					if($local_region > $interlocal_region){
						$cm_continent = "id";
						// PRESENTASE TERBESAR INDONESIA
						// $list_janus = array("c3.classmiles.com" => 0,"c1.classmiles.com" => 0,"c2.classmiles.com" => 0);
						$list_janus = array("c2.classmiles.com" => 0, "c1.classmiles.com" => 0);
						foreach ($ttl_active_kuota as $key => $value) {
							if(in_array($value['janus'], $list_janus)){
								$list_janus[$value['janus']] = $value['kuota'];
							}
						}
						foreach ($list_janus as $key => $value) {
							if($value < 300 && ($value+$incoming_kuota) <= 300){
								$chosen_janus = $key;
								break;
							}
						}
						if($chosen_janus == "")
							$chosen_janus = "c2.classmiles.com"; //JANUS SAMPAH
					}else{
						$cm_continent = "sg";
						// $list_janus = array("c102.classmiles.com" => 0);
						$list_janus = array("c2.classmiles.com" => 0, "c1.classmiles.com" => 0);
						foreach ($ttl_active_kuota as $key => $value) {
							if(in_array($value['janus'], $list_janus)){
								$list_janus[$value['janus']] = $value['kuota'];
							}
						}
						foreach ($list_janus as $key => $value) {
							if($value < 200 && ($value+$incoming_kuota) <= 200){
								$chosen_janus = $key;
								break;
							}
						}
						if($chosen_janus == "")
							$chosen_janus = "c2.classmiles.com"; //JANUS SAMPAH
					}
					$this->db->simple_query("INSERT INTO temp_active_room VALUES(NULL,'$cm_continent','$chosen_janus','$session_id',$incoming_kuota,now());");
					$this->janus_server = $chosen_janus;
				}
				if($cm_continent == "id"){
					$student = $DB2->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
					if(!empty($student)){
						$hash_id = $student['session_id'];
					}else{
						$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
						echo $hash_id;
						$DB2->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
					}
					$data_diri = $this->db->query("SELECT * FROM tbl_user WHERE id_user=$iduser")->row_array();

					$qr = "";
					$qr_data = "";
					$setter = "";
					foreach ($data_diri as $key => $value) {
						if($key == "username" && $value == NULL){

						}else{
							$qr.= $key.", ";
							$qr_data.= "'".$this->db->escape_str($value)."', ";
							$setter.= "$key='$value', ";
						}
					}
					$qr = rtrim($qr, ", ");
					$qr_data = rtrim($qr_data, ", ");
					$setter = rtrim($setter, ", ");
					$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_user($qr) VALUES($qr_data)");
					if(!$insert_tbl_user) {
						$DB2->simple_query("UPDATE tbl_user SET $setter WHERE id_user='$iduser'");
					}

					$classplay_exist = $DB2->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
					if(empty($classplay_exist) || $classplay_exist['subject_id'] == '0' ){
						$qc = "";
						$qc_data = "";
						$setter = "";
						foreach ($db_res as $key => $value) {
							$qc.= $key.", ";
							$qc_data.= "'".$this->db->escape_str($value)."', ";
							$setter.= "$key='$value', ";
						}
						$qc = rtrim($qc, ", ");
						$qc_data = rtrim($qc_data, ", ");
						$setter = rtrim($setter, ", ");
						$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_class($qc) VALUES($qc_data)");
						if(!$insert_tbl_user){
							$DB2->simple_query("UPDATE tbl_class SET $setter WHERE class_id='$session_id'");
						}
					}
					
				}else if($cm_continent == 'sg') {
					$student = $DB_SG->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
					if(!empty($student)){
						$hash_id = $student['session_id'];
					}else{
						$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
						echo $hash_id;
						$DB_SG->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
					}
					$data_diri = $this->db->query("SELECT * FROM tbl_user WHERE id_user=$iduser")->row_array();

					$qr = "";
					$qr_data = "";
					$setter = "";
					foreach ($data_diri as $key => $value) {
						if($key == "username" && $value == NULL){

						}else{
							$qr.= $key.", ";
							$qr_data.= "'$value', ";
							$setter.= "$key='$value', ";
						}
					}
					$qr = rtrim($qr, ", ");
					$qr_data = rtrim($qr_data, ", ");
					$setter = rtrim($setter, ", ");
					$insert_tbl_user = $DB_SG->simple_query("INSERT INTO tbl_user($qr) VALUES($qr_data)");
					if(!$insert_tbl_user) {
						$DB_SG->simple_query("UPDATE tbl_user SET $setter WHERE id_user='$iduser'");
					}

					$classplay_exist = $DB_SG->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
					if(empty($classplay_exist) || $classplay_exist['subject_id'] == '0' ){
						$qc = "";
						$qc_data = "";
						$setter = "";
						foreach ($db_res as $key => $value) {
							$qc.= $key.", ";
							$qc_data.= "'$value', ";
							$setter.= "$key='$value', ";
						}
						$qc = rtrim($qc, ", ");
						$qc_data = rtrim($qc_data, ", ");
						$setter = rtrim($setter, ", ");
						$insert_tbl_user = $DB_SG->simple_query("INSERT INTO tbl_class($qc) VALUES($qc_data)");
						if(!$insert_tbl_user){
							$DB_SG->simple_query("UPDATE tbl_class SET $setter WHERE class_id='$session_id'");
						}
					}
				}

				$load = $this->lang->line('logsuccess');					
				$dpt_subject_name = $this->db->query("SELECT name FROM tbl_class WHERE class_id='$session_id'")->row_array();
				$action = "$load ".$dpt_subject_name['name'];
				$log_user_class = $this->db->query("INSERT INTO log_user (action, id_user) VALUES ('$action','$iduser')");
				// $this->session->set_userdata('p',$andro);
				// header('Location: https://classmiles.com/classroom/broadcast_wd?access_session='.$hash_id);
				// echo "<script>alert('halo ini di master');</script>";
				if ($templateweb == "whiteboard_digital") {
					header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast_wd?access_session='.$hash_id.'&p='.$andro);
				}
				else if ($templateweb == "whiteboard_videoboard") {
					header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast_wv?access_session='.$hash_id.'&p='.$andro);
				}
				else if ($templateweb == "whiteboard_no")
				{
					header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast?access_session='.$hash_id.'&p='.$andro);
				}
				else if ($templateweb == "all_featured")
				{
					header('Location: https://'.$cm_continent.'.classmiles.com/classroom/broadcast_all?access_session='.$hash_id.'&p='.$andro.'&back='.$linkback);
				}

				exit();
			}
		}

		public function tps_me_priv_backup($session_id='')
		{
			$iduser = $this->session->userdata('id_user');
			$templateprivate = $this->input->get('t');
			$student = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
			if(!empty($student)){
				$hash_id = $student['session_id'];
			}else{
				$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
				echo $hash_id;
				$this->db->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
			}	
			$load = $this->lang->line('logsuccess');					
			$dpt_subject_name = $this->db->query("SELECT name FROM tbl_class WHERE class_id='$session_id'")->row_array();
			$action = "$load ".$dpt_subject_name['name'];
			$log_user_class = $this->db->query("INSERT INTO log_user (action, id_user) VALUES ('$action','$iduser')");			
			if ($templateprivate == "whiteboard_digital") {
				header('Location: https://classmiles.com/classroom/privateclas_wd?access_session='.$hash_id);
			}
			else if ($templateprivate == "whiteboard_videoboard") {
				header('Location: https://classmiles.com/classroom/privateclas_wv?access_session='.$hash_id);
			}
			else if ($templateprivate == "whiteboard_no") {
				header('Location: https://classmiles.com/classroom/privateclass?access_session='.$hash_id);
			}
			else if($templateprivate == "null"){
				header('Location: https://classmiles.com/classroom/callsupport?access_session='.$hash_id);
			}

			// header('Location: https://classmiles.com/classroom/#access_session='.$hash_id);
			// header('Location: https://classmiles.com/classroom/#access_session=e2eb87d310b5252d61614438f2c1bc49a94b599b');			
			// header('Location: https://classmiles.com/Classroom/session?access_session='.$hash_id);			
			// header('Location: https://classmiles.com/classroom/session?access_session='.$hash_id);
			// header('Location: https://classmiles.com/classroom/process?access_session='.$hash_id);
			
			exit();
		}

		public function tps_me_priv($session_id='')
		{
			// echo $this->session->userdata('id_user');
			// return false;
			$id_user = $this->session->userdata('id_user');
			$checkstudentonline = $this->db->query("SELECT * from temp_user_online where id_user = '".$id_user."' ")->row_array();
		    if (!empty($checkstudentonline)) {
		        $this->session->set_flashdata('mes_alert','failed');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Sorry, your now in class');	
				$this->session->set_userdata('tutorinclass', '4044');
				redirect('/');
		    } 
		    else {
				$DB2 = $this->load->database('db2', TRUE); // SAY SEA SERVER CLASSPLAY DB
				// $DB_SG = $this->load->database('db_sg', TRUE); // SAY SEA SERVER CLASSPLAY DB

				$cm_continent = "id";
				$iduser = $this->session->userdata('id_user');
				$andro 	= $this->input->get('p');
				if ($iduser == null) {				
					$iduser = $this->input->get('id_user');
				}
				$templateprivate = $this->input->get('t');

				$db_res = $this->db->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
				$room_active_janus = $this->db->query("SELECT * FROM temp_active_room WHERE class_id='$session_id'")->row_array();
				if(!empty($room_active_janus)){
					$cm_continent = $room_active_janus['cm_continent'];
				}else{
					// TAKE ALL PARTICIPANT IN THAT CLASS
					$visible = json_decode($db_res['participant'],true);
					$incoming_kuota = 0;
					$local_region = 0;
					$interlocal_region = 0;
					$chosen_janus = "";

					$country_code = $this->db->query("SELECT user_countrycode FROM tbl_user WHERE id_user='".$db_res['tutor_id']."'")->row_array()['user_countrycode'];
					if($country_code == "+62" || $country_code == "62"){
						$local_region++;
					}else{
						$interlocal_region++;
					}
					$incoming_kuota++;

					foreach ($visible['participant'] as $k => $value) {
						$id_user = $value['id_user'];
						$data_ccode_utype = $this->db->query("SELECT user_countrycode, usertype_id FROM tbl_user WHERE id_user='$id_user'")->row_array();
						$country_code = $data_ccode_utype['user_countrycode'];
						$usertype_id = $data_ccode_utype['usertype_id'];
						if($usertype_id != "student kid"){
							if($country_code == "+62" || $country_code == "62"){
								$local_region++;
							}else{
								$interlocal_region++;
							}
						}
						$incoming_kuota++;
					}

					$ttl_active_kuota = $this->db->query("SELECT janus, SUM(kuota) as kuota FROM temp_active_room GROUP BY janus")->result_array();
					if($local_region > $interlocal_region){
						$cm_continent = "id";
						// PRESENTASE TERBESAR INDONESIA
						// $list_janus = array("c3.classmiles.com" => 0,"c1.classmiles.com" => 0,"c2.classmiles.com" => 0);
						$list_janus = array("c2.classmiles.com" => 0);
						foreach ($ttl_active_kuota as $key => $value) {
							if(in_array($value['janus'], $list_janus)){
								$list_janus[$value['janus']] = $value['kuota'];
							}
						}
						foreach ($list_janus as $key => $value) {
							if($value < 300 && ($value+$incoming_kuota) <= 300){
								$chosen_janus = $key;
								break;
							}
						}
						if($chosen_janus == "")
							$chosen_janus = "c2.classmiles.com"; //JANUS SAMPAH
					}else{
						$cm_continent = "id";
						// $list_janus = array("c102.classmiles.com" => 0);
						$list_janus = array("c2.classmiles.com" => 0);
						foreach ($ttl_active_kuota as $key => $value) {
							if(in_array($value['janus'], $list_janus)){
								$list_janus[$value['janus']] = $value['kuota'];
							}
						}
						foreach ($list_janus as $key => $value) {
							if($value < 200 && ($value+$incoming_kuota) <= 200){
								$chosen_janus = $key;
								break;
							}
						}
						if($chosen_janus == "")
							$chosen_janus = "c2.classmiles.com"; //JANUS SAMPAH
					}
					$this->db->simple_query("INSERT INTO temp_active_room VALUES(NULL,'$cm_continent','$chosen_janus','$session_id',$incoming_kuota,now());");
					$this->janus_server = $chosen_janus;
				}
				if($cm_continent == "id"){
					$student = $DB2->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
					/*print_r($student);
					echo $iduser;
					return false;*/

					if(!empty($student)){
						$hash_id = $student['session_id'];
					}else{
						$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
						echo $hash_id;
						$DB2->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
					}
					$data_diri = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$iduser'")->row_array();

					$qr = "";
					$qr_data = "";
					$setter = "";
					foreach ($data_diri as $key => $value) {
						if($key == "username" && $value == NULL){

						}else{
							$qr.= $key.", ";
							$qr_data.= "'".$this->db->escape_str($value)."', ";
							$setter.= "$key='$value', ";
						}
					}
					$qr = rtrim($qr, ", ");
					$qr_data = rtrim($qr_data, ", ");
					$setter = rtrim($setter, ", ");
					$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_user($qr) VALUES($qr_data)");
					if(!$insert_tbl_user) {
						$DB2->simple_query("UPDATE tbl_user SET $setter WHERE id_user='$iduser'");
					}

					$classplay_exist = $DB2->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
					if(empty($classplay_exist) || $classplay_exist['subject_id'] == '0' ){
						$qc = "";
						$qc_data = "";
						$setter = "";
						foreach ($db_res as $key => $value) {
							$qc.= $key.", ";
							$qc_data.= "'".$this->db->escape_str($value)."', ";
							$setter.= "$key='$value', ";
						}
						$qc = rtrim($qc, ", ");
						$qc_data = rtrim($qc_data, ", ");
						$setter = rtrim($setter, ", ");
						$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_class($qc) VALUES($qc_data)");
						if(!$insert_tbl_user){
							$DB2->simple_query("UPDATE tbl_class SET $setter WHERE class_id='$session_id'");
						}
					}
					
				}
				// else if($cm_continent == 'sg') {
				// 	$student = $DB_SG->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
				// 	if(!empty($student)){
				// 		$hash_id = $student['session_id'];
				// 	}else{
				// 		$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
				// 		echo $hash_id;
				// 		$DB_SG->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
				// 	}
				// 	$data_diri = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$iduser'")->row_array();

				// 	$qr = "";
				// 	$qr_data = "";
				// 	$setter = "";
				// 	foreach ($data_diri as $key => $value) {
				// 		if($key == "username" && $value == NULL){

				// 		}else{
				// 			$qr.= $key.", ";
				// 			$qr_data.= "'$value', ";
				// 			$setter.= "$key='$value', ";
				// 		}
				// 	}
				// 	$qr = rtrim($qr, ", ");
				// 	$qr_data = rtrim($qr_data, ", ");
				// 	$setter = rtrim($setter, ", ");
				// 	$insert_tbl_user = $DB_SG->simple_query("INSERT INTO tbl_user($qr) VALUES($qr_data)");
				// 	if(!$insert_tbl_user) {
				// 		$DB_SG->simple_query("UPDATE tbl_user SET $setter WHERE id_user='$iduser'");
				// 	}

				// 	$classplay_exist = $DB_SG->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
				// 	if(empty($classplay_exist) || $classplay_exist['subject_id'] == '0' ){
				// 		$qc = "";
				// 		$qc_data = "";
				// 		$setter = "";
				// 		foreach ($db_res as $key => $value) {
				// 			$qc.= $key.", ";
				// 			$qc_data.= "'$value', ";
				// 			$setter.= "$key='$value', ";
				// 		}
				// 		$qc = rtrim($qc, ", ");
				// 		$qc_data = rtrim($qc_data, ", ");
				// 		$setter = rtrim($setter, ", ");
				// 		$insert_tbl_user = $DB_SG->simple_query("INSERT INTO tbl_class($qc) VALUES($qc_data)");
				// 		if(!$insert_tbl_user){
				// 			$DB_SG->simple_query("UPDATE tbl_class SET $setter WHERE class_id='$session_id'");
				// 		}
				// 	}
				// }

				$load = $this->lang->line('logsuccess');					
				$dpt_subject_name = $this->db->query("SELECT name FROM tbl_class WHERE class_id='$session_id'")->row_array();
				$action = "$load ".$dpt_subject_name['name'];
				$log_user_class = $this->db->query("INSERT INTO log_user (action, id_user) VALUES ('$action','$iduser')");		
				// if ($templateprivate == "whiteboard_digital") {
				// 	header('Location: https://'.$cm_continent.'.classmiles.com/classroom/privateclas_wd?access_session='.$hash_id.'&p='.$andro);
				// }
				// else if ($templateprivate == "whiteboard_videoboard") {
				// 	header('Location: https://'.$cm_continent.'.classmiles.com/classroom/privateclas_wv?access_session='.$hash_id.'&p='.$andro);
				// }
				// else if ($templateprivate == "whiteboard_no") {
				// 	header('Location: https://'.$cm_continent.'.classmiles.com/classroom/privateclass?access_session='.$hash_id.'&p='.$andro);
				// }
				// else if ($templateprivate == "all_featured")
				// {
				// 	header('Location: https://'.$cm_continent.'.classmiles.com/classroom/privateclas_all?access_session='.$hash_id.'&p='.$andro);
				// }
				// else if($templateprivate == "null"){
				// 	header('Location: https://'.$cm_continent.'.classmiles.com/classroom/callsupport?access_session='.$hash_id.'&p='.$andro);
				// }
				header('Location: https://'.$cm_continent.'.indiclass.id/classroom/privateclas_all?access_session='.$hash_id.'&p='.$andro.'&back=https://indiclass.id/');
				
				// header('Location: https://classmiles.com/classroom/#access_session='.$hash_id);
				// header('Location: https://classmiles.com/classroom/#access_session=e2eb87d310b5252d61614438f2c1bc49a94b599b');			
				// header('Location: https://classmiles.com/Classroom/session?access_session='.$hash_id);			
				// header('Location: https://classmiles.com/classroom/session?access_session='.$hash_id);
				// header('Location: https://classmiles.com/classroom/process?access_session='.$hash_id);
				
				exit();
			}
		}

		public function tps_me_group_backup($session_id='')
		{
			$iduser = $this->session->userdata('id_user');
			$templategroup = $this->input->get('t');
			$student = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
			if(!empty($student)){
				$hash_id = $student['session_id'];
			}else{
				$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
				echo $hash_id;
				$this->db->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
			}	
			$load = $this->lang->line('logsuccess');					
			$dpt_subject_name = $this->db->query("SELECT name FROM tbl_class WHERE class_id='$session_id'")->row_array();
			$action = "$load ".$dpt_subject_name['name'];
			$log_user_class = $this->db->query("INSERT INTO log_user (action, id_user) VALUES ('$action','$iduser')");			

			if ($templategroup == "whiteboard_digital") {
				header('Location: https://classmiles.com/classroom/groupclass_wd?access_session='.$hash_id);
							
			}
			else if($templategroup == "whiteboard_videoboard"){
				header('Location: https://classmiles.com/classroom/groupclass_wv?access_session='.$hash_id);
			}
			else if ($templategroup == "whiteboard_no"){				
				header('Location: https://classmiles.com/classroom/groupclass?access_session='.$hash_id);
			}

			// header('Location: https://classmiles.com/classroom/groupclass?access_session='.$hash_id);
			// header('Location: https://classmiles.com/classroom/#access_session='.$hash_id);
			// header('Location: https://classmiles.com/classroom/#access_session=e2eb87d310b5252d61614438f2c1bc49a94b599b');			
			// header('Location: https://classmiles.com/Classroom/session?access_session='.$hash_id);			
			// header('Location: https://classmiles.com/classroom/session?access_session='.$hash_id);
			// header('Location: https://classmiles.com/classroom/process?access_session='.$hash_id);			
			exit();
		}

		public function tps_me_group($session_id='')
		{
			$id_user = $this->session->userdata('id_user');
			$checkstudentonline = $this->db->query("SELECT * from temp_user_online where id_user = '".$id_user."' ")->row_array();
		    if (!empty($checkstudentonline)) {
		        $this->session->set_flashdata('mes_alert','failed');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Sorry, your now in class');	
				$this->session->set_userdata('tutorinclass', '4044');
				redirect('/');
		    } 
		    else {
				$DB2 = $this->load->database('db2', TRUE); // SAY SEA SERVER CLASSPLAY DB
				// $DB_SG = $this->load->database('db_sg', TRUE); // SAY SEA SERVER CLASSPLAY DB

				$cm_continent = "id";
				$iduser = $this->session->userdata('id_user');
				$andro 	= $this->input->get('p');
				if ($iduser == null) {				
					$iduser = $this->input->get('id_user');
				}
				$templategroup = $this->input->get('t');

				$db_res = $this->db->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
				$room_active_janus = $this->db->query("SELECT * FROM temp_active_room WHERE class_id='$session_id'")->row_array();
				if(!empty($room_active_janus)){
					$cm_continent = $room_active_janus['cm_continent'];
				}else{
					// TAKE ALL PARTICIPANT IN THAT CLASS
					$visible = json_decode($db_res['participant'],true);
					$incoming_kuota = 0;
					$local_region = 0;
					$interlocal_region = 0;
					$chosen_janus = "";

					$country_code = $this->db->query("SELECT user_countrycode FROM tbl_user WHERE id_user='".$db_res['tutor_id']."'")->row_array()['user_countrycode'];
					if($country_code == "+62" || $country_code == "62"){
						$local_region++;
					}else{
						$interlocal_region++;
					}
					$incoming_kuota++;

					foreach ($visible['participant'] as $k => $value) {
						$id_user = $value['id_user'];
						$data_ccode_utype = $this->db->query("SELECT user_countrycode, usertype_id FROM tbl_user WHERE id_user='$id_user'")->row_array();
						$country_code = $data_ccode_utype['user_countrycode'];
						$usertype_id = $data_ccode_utype['usertype_id'];
						if($usertype_id != "student kid"){
							if($country_code == "+62" || $country_code == "62"){
								$local_region++;
							}else{
								$interlocal_region++;
							}
						}
						$incoming_kuota++;
					}

					$ttl_active_kuota = $this->db->query("SELECT janus, SUM(kuota) as kuota FROM temp_active_room GROUP BY janus")->result_array();
					if($local_region > $interlocal_region){
						$cm_continent = "id";
						// PRESENTASE TERBESAR INDONESIA
						// $list_janus = array("c3.classmiles.com" => 0,"c1.classmiles.com" => 0,"c2.classmiles.com" => 0);
						$list_janus = array("c2.classmiles.com" => 0);
						foreach ($ttl_active_kuota as $key => $value) {
							if(in_array($value['janus'], $list_janus)){
								$list_janus[$value['janus']] = $value['kuota'];
							}
						}
						foreach ($list_janus as $key => $value) {
							if($value < 300 && ($value+$incoming_kuota) <= 300){
								$chosen_janus = $key;
								break;
							}
						}
						if($chosen_janus == "")
							$chosen_janus = "c2.classmiles.com"; //JANUS SAMPAH
					}else{
						$cm_continent = "id";
						// $list_janus = array("c102.classmiles.com" => 0);
						$list_janus = array("c2.classmiles.com" => 0);
						foreach ($ttl_active_kuota as $key => $value) {
							if(in_array($value['janus'], $list_janus)){
								$list_janus[$value['janus']] = $value['kuota'];
							}
						}
						foreach ($list_janus as $key => $value) {
							if($value < 200 && ($value+$incoming_kuota) <= 200){
								$chosen_janus = $key;
								break;
							}
						}
						if($chosen_janus == "")
							$chosen_janus = "c2.classmiles.com"; //JANUS SAMPAH
					}
					$this->db->simple_query("INSERT INTO temp_active_room VALUES(NULL,'$cm_continent','$chosen_janus','$session_id',$incoming_kuota,now());");
					$this->janus_server = $chosen_janus;
				}
				if($cm_continent == "id"){
					$student = $DB2->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
					if(!empty($student)){
						$hash_id = $student['session_id'];
					}else{
						$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
						echo $hash_id;
						$DB2->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
					}
					$data_diri = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$iduser'")->row_array();

					$qr = "";
					$qr_data = "";
					$setter = "";
					foreach ($data_diri as $key => $value) {
						if($key == "username" && $value == NULL){

						}else{
							$qr.= $key.", ";
							$qr_data.= "'".$this->db->escape_str($value)."', ";
							$setter.= "$key='$value', ";
						}
					}
					$qr = rtrim($qr, ", ");
					$qr_data = rtrim($qr_data, ", ");
					$setter = rtrim($setter, ", ");
					$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_user($qr) VALUES($qr_data)");
					if(!$insert_tbl_user) {
						$DB2->simple_query("UPDATE tbl_user SET $setter WHERE id_user='$iduser'");
					}

					$classplay_exist = $DB2->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
					if(empty($classplay_exist) || $classplay_exist['subject_id'] == '0' ){
						$qc = "";
						$qc_data = "";
						$setter = "";
						foreach ($db_res as $key => $value) {
							$qc.= $key.", ";
							$qc_data.= "'".$this->db->escape_str($value)."', ";
							$setter.= "$key='$value', ";
						}
						$qc = rtrim($qc, ", ");
						$qc_data = rtrim($qc_data, ", ");
						$setter = rtrim($setter, ", ");
						$insert_tbl_user = $DB2->simple_query("INSERT INTO tbl_class($qc) VALUES($qc_data)");
						if(!$insert_tbl_user){
							$DB2->simple_query("UPDATE tbl_class SET $setter WHERE class_id='$session_id'");
						}
					}
				}
				// }else if($cm_continent == 'id') {
				// 	$student = $DB_SG->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$session_id' AND id_user='$iduser'")->row_array();
				// 	if(!empty($student)){
				// 		$hash_id = $student['session_id'];
				// 	}else{
				// 		$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
				// 		echo $hash_id;
				// 		$DB_SG->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$session_id','$iduser','TRUE',0)");
				// 	}
				// 	$data_diri = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$iduser'")->row_array();

				// 	$qr = "";
				// 	$qr_data = "";
				// 	$setter = "";
				// 	foreach ($data_diri as $key => $value) {
				// 		if($key == "username" && $value == NULL){

				// 		}else{
				// 			$qr.= $key.", ";
				// 			$qr_data.= "'$value', ";
				// 			$setter.= "$key='$value', ";
				// 		}
				// 	}
				// 	$qr = rtrim($qr, ", ");
				// 	$qr_data = rtrim($qr_data, ", ");
				// 	$setter = rtrim($setter, ", ");
				// 	$insert_tbl_user = $DB_SG->simple_query("INSERT INTO tbl_user($qr) VALUES($qr_data)");
				// 	if(!$insert_tbl_user) {
				// 		$DB_SG->simple_query("UPDATE tbl_user SET $setter WHERE id_user='$iduser'");
				// 	}

				// 	$classplay_exist = $DB_SG->query("SELECT * FROM tbl_class WHERE class_id='$session_id'")->row_array();
				// 	if(empty($classplay_exist) || $classplay_exist['subject_id'] == '0' ){
				// 		$qc = "";
				// 		$qc_data = "";
				// 		$setter = "";
				// 		foreach ($db_res as $key => $value) {
				// 			$qc.= $key.", ";
				// 			$qc_data.= "'$value', ";
				// 			$setter.= "$key='$value', ";
				// 		}
				// 		$qc = rtrim($qc, ", ");
				// 		$qc_data = rtrim($qc_data, ", ");
				// 		$setter = rtrim($setter, ", ");
				// 		$insert_tbl_user = $DB_SG->simple_query("INSERT INTO tbl_class($qc) VALUES($qc_data)");
				// 		if(!$insert_tbl_user){
				// 			$DB_SG->simple_query("UPDATE tbl_class SET $setter WHERE class_id='$session_id'");
				// 		}
				// 	}
				// }

				$load = $this->lang->line('logsuccess');					
				$dpt_subject_name = $this->db->query("SELECT name FROM tbl_class WHERE class_id='$session_id'")->row_array();
				$action = "$load ".$dpt_subject_name['name'];
				$log_user_class = $this->db->query("INSERT INTO log_user (action, id_user) VALUES ('$action','$iduser')");		
				// if ($templategroup == "whiteboard_digital") {
				// 	header('Location: https://'.$cm_continent.'.classmiles.com/classroom/groupclass_wd?access_session='.$hash_id.'&p='.$andro);			
				// }
				// else if($templategroup == "whiteboard_videoboard"){
				// 	header('Location: https://'.$cm_continent.'.classmiles.com/classroom/groupclass_wv?access_session='.$hash_id.'&p='.$andro);
				// }
				// else if ($templategroup == "whiteboard_no"){				
				// 	header('Location: https://'.$cm_continent.'.classmiles.com/classroom/groupclass?access_session='.$hash_id.'&p='.$andro);
				// }
				// else if ($templategroup == "all_featured")
				// {
				// 	header('Location: https://'.$cm_continent.'.classmiles.com/classroom/groupclass_all?access_session='.$hash_id.'&p='.$andro);
				// }
				// else if($templategroup == "null"){
				// 	header('Location: https://'.$cm_continent.'.classmiles.com/classroom/callsupport?access_session='.$hash_id.'&p='.$andro);
				// }
				header('Location: https://'.$cm_continent.'.indiclass.id/classroom/groupclass_all?access_session='.$hash_id.'&p='.$andro.'&back=https://indiclass.id/');
				// header('Location: https://classmiles.com/classroom/#access_session='.$hash_id);
				// header('Location: https://classmiles.com/classroom/#access_session=e2eb87d310b5252d61614438f2c1bc49a94b599b');			
				// header('Location: https://classmiles.com/Classroom/session?access_session='.$hash_id);			
				// header('Location: https://classmiles.com/classroom/session?access_session='.$hash_id);
				// header('Location: https://classmiles.com/classroom/process?access_session='.$hash_id);				
				exit();
			}
		}

		public function room_tester($uri='')
		{
			$base64 = strtr($uri, '-_.', '+/=');
			$dec = $this->encryption->decrypt($base64);
			$dec = json_decode($dec,true);
			$tutor_id = $dec['tutor_id'];
			$requestid = $dec['requestid'];
			$now = time();
			$nowp5 = $now + 300;
			$st_time = date('Y-m-d H:i:s',$now);
			$fn_time = date('Y-m-d H:i:s',$nowp5);
			$participant = json_encode(array("visible" => "none"));
			$this->db->simple_query("INSERT INTO tbl_class(subject_id,name,description,tutor_id,class_type,start_time,finish_time,participant) VALUES('404','Room Tester','Room Tester','{$tutor_id}','private','{$st_time}','{$fn_time}','{$participant}')");

			$class_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];

			$this->db->simple_query("UPDATE tbl_support SET status=2, link='https://classmiles.com/master/tps_me_priv/$class_id?t=null' WHERE requestid=$requestid");

			header("Location: https://classmiles.com/master/tps_me_priv/$class_id?t=null");

			/*$student = $this->db->query("SELECT * FROM tbl_participant_session as tps WHERE class_id='$class_id' AND id_user='$tutor_id'")->row_array();
			if(!empty($student)){
				$hash_id = $student['session_id'];
			}else{
				$hash_id = $this->M_login->second_genses($this->session->userdata('email').time());
				echo $hash_id;
				$this->db->simple_query("INSERT INTO tbl_participant_session(session_id,class_id,id_user,present,total_ptime) VALUES('$hash_id','$class_id','$tutor_id','TRUE',0)");
			}	
			$load = $this->lang->line('logsuccess');					
			$dpt_subject_name = $this->db->query("SELECT name FROM tbl_class WHERE class_id='$class_id'")->row_array();
			$action = "$load ".$dpt_subject_name['name'];
			// $log_user_class = $this->db->query("INSERT INTO log_user (action, id_user) VALUES ('$action','$tutor_id')");
			//update request id so admin knows that tutor has started class
			
			
			header('Location: https://classmiles.com/classroom/roomtester?access_session='.$hash_id);*/
		}

		function randomLink(){
			$digit = 30;
			$karakter = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+{}|:<>?';,./[]";
			srand((double)microtime()*1000000);
			$i = 0;
			$key = "";
			while ($i <= $digit-1)
			{
				$num = rand() % 30;
				$tmp = substr($karakter,$num,1);
				$key = $key.$tmp;
				$i++;
			}
			// print_r($pass);
			// return null;
			return $key;
		}
		public function onah()
		{
			$a = $this->Master_model->tutordesc();

			echo json_encode($a);
			
		}

		function sendlinkandroid(){
			$emailforgot = $this->input->post('forgotemail');	
			$where = array(
				'email' => $emailforgot
				);

			$cek = $this->M_forgot->cek_forgot("tbl_user",$where);
			$user_name = $cek['user_name'];
			if ($cek == 0 ) {
				$load = $this->lang->line('failedsendemail');		
				$array_balikann = array("message" => $load, "status" => false);				
				echo json_encode($array_balikann);
				return 0;
			}

			//kirim password via email
			// $r_link = $this->randomLink();				
			// $where['r_link']= $r_link;

			

			// Open connection
	        $ch = curl_init();

	        // Set the url, number of POST vars, POST data
	        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
	        curl_setopt($ch, CURLOPT_POST, true);
	        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_sendLink_android','content' => array("emailforgot" => "$emailforgot", "user_name" => "$user_name", "r_link" => "$r_link") )));

	        // Execute post
	        $result = curl_exec($ch);
	        curl_close($ch);
	        // echo $result;


	        if ($result) {
	          	$load = $this->lang->line('sentlink');
				$array_balikan = array("message" => $load, "status" => true);

				$saveLink = $this->M_forgot->save_link("tbl_user", $where);
				//echo '<script>alert("Password berhasil di ubah. Silahkan cek email anda");</script>';
				echo json_encode($array_balikan);
				return 1;
	        }else{
	              show_error($this->email->print_debuger());	    
	        }
		}


		function sendLink(){
			$emailforgot = $this->input->post('forgotemail');	
			$where = array(
				'email' => $emailforgot
				);

			$cek = $this->M_forgot->cek_forgot("tbl_user",$where);
			$user_name = $cek['user_name'];
			if ($cek == 0 ) {
				$load = $this->lang->line('failedsendemail');
				$this->session->set_userdata('code','301');
				// $this->session->set_userdata('message', 'failedsendemail');
				redirect('/');
			}

				//kirim password via email
			$r_link = $this->randomLink();				
			$where['r_link']= $r_link;



			// Open connection
	        $ch = curl_init();

	        // Set the url, number of POST vars, POST data
	        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
	        curl_setopt($ch, CURLOPT_POST, true);
	        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_sendLink','content' => array("emailforgot" => "$emailforgot", "user_name" => "$user_name", "r_link" => "$r_link") )));

	        // Execute post
	        $result = curl_exec($ch);
	        curl_close($ch);
	        // echo $result;


	        if ($result) {
	          	$load = $this->lang->line('sentlink');
				$array_balikan = array("message" => $load, "status" => true);

				$saveLink = $this->M_forgot->save_link("tbl_user", $where);
				//echo '<script>alert("Password berhasil di ubah. Silahkan cek email anda");</script>';
				redirect('/');
				echo json_encode($array_balikan);
				return 1;
	        }else{
	              show_error($this->email->print_debuger());	    
	        }
		}

		public function aksi_forgot(){
			$res = $this->M_forgot->change_pass($this->input->post());
			if ($res == 1) {

				// $load = $this->lang->line('successfullypassword');
				// $this->session->set_flashdata('mes_alert','success');
				// $this->session->set_flashdata('mes_display','block'); 
				// $this->session->set_flashdata('mes_message',$load);	
				$this->session->set_userdata('code','304');
				// $this->session->set_userdata('message', 'successfullypassword');
				redirect('/');
			}else if($res == 0){

				// $load = $this->lang->line('failedchange');
				// $this->session->set_flashdata('mes_alert','danger');
				// $this->session->set_flashdata('mes_display','block'); 
				// $this->session->set_flashdata('mes_message',$load);	
				$this->session->set_userdata('code','303');
				// $this->session->set_userdata('message', 'failedchange');
				redirect('/');
			}
		}

		public function reg_email()
		{
			$all_data = $this->input->post();
			$email_tujuan = $all_data['email'];
			$user_name = $all_data['user_name'];
			
			$all_data['user_imagee'] = str_replace('s96-c/', '', $all_data['user_image']);
			// print_r($all_data['user_imagee']);
			// return false;
			$all_data['user_birthdate'] = $all_data['tahun_lahir']."-".$all_data['bulan_lahir']."-".$all_data['tanggal_lahir'];
			$cekdata = $this->Master_model->reg_email($all_data);			
			$random_key = $r_key = $this->M_login->randomKey();
			// echo $email_tujuan.$user_name.$random_key;
			// return false;

			// Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
            curl_setopt($ch, CURLOPT_POST, true);
            // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_daftarbaru_google','content' => array("email_tujuan" => "$email_tujuan", "user_name" => "$user_name") )));

            // Execute post
            $result = curl_exec($ch);
            curl_close($ch);
            // echo $result;				

			// echo $cekdata;
			// return 1;
			if ($cekdata == "1") {

				$emailll 		= $this->session->userdata('email_register');
				$passworddd  	= $this->session->userdata('password_register');
				$where = array(
					'email' => $emailll,
					'kata_sandi' => $passworddd 
				);

				$cek = $this->M_login->cl_daftartutor('tbl_user', $where);

				if ($cek == 0) {
					// $load = $this->lang->line('failed');
					// $this->session->set_flashdata('mes_alert','danger');
					// $this->session->set_flashdata('mes_display','block'); 
					// $this->session->set_flashdata('mes_message',$load);	
					// $this->session->set_userdata('message', 'failed');	
					// $this->session->set_userdata('code','121');
					redirect('/');
		           	
				}
				else
				{
					// $load = $this->lang->line('registered');
					// $rets['mes_alert'] ='warning';
					// $rets['mes_display'] ='block';
					// $rets['mes_message'] =$load;
					// // $this->session->set_userdata('message', 'registered');
					// $this->session->set_flashdata('rets',$rets);			
					// redirect('/');
					// $this->session->set_userdata('code','122');
					redirect('/');
				}
			}
			else
			{
				// $load = $this->lang->line('failed');
				// $this->session->set_flashdata('mes_alert','danger');
				// $this->session->set_flashdata('mes_display','block'); 
				// $this->session->set_flashdata('mes_message',$load);
				$this->session->set_userdata('code','106');
				// $this->session->set_userdata('message', 'failed');	
				redirect('/');
			}	
		}

		function EditPasswordKids(){
			// print_r($this->input->post());
			// return null;
				
				$device = $this->input->post('device');
				// $id_user = $this->input->post('id_user');
				// $oldpass = $this->input->post('oldpass_'.$id_user);

				$res = $this->Master_model->Edit_PasswordKids("tbl_user",$this->input->post());
					$this->session->set_flashdata('rets',$res);
					
				if ($device == 'web') {
					

					redirect ('Kids');
				}
				else{

					redirect('About');	
				}
		}

		function saveEditAccountStudent(){
			// print_r($this->input->post());
			// echo $this->session->userdata('parent_id');
			// return false;
			// return null;
			$parent_id = $this->session->userdata('parent_id');
			$res = $this->Master_model->saveEdit_AccountStudent("tbl_user",$this->input->post());

			
			$iduser = $this->session->userdata('id_user');
				$cek_data= $this->db->query("SELECT tu.user_nationality, tu.usertype_id, tu.user_religion, tu.user_address, tu.user_image, tps.jenjang_id, tps.school_id FROM tbl_user as tu INNER JOIN tbl_profile_student as tps WHERE tu.id_user='$iduser' AND tps.student_id='$iduser'")->row_array();
			$this->session->set_flashdata('rets',$res);
			if($cek_data['school_id']==0 && $cek_data['usertype_id']=='student' && $cek_data['jenjang_id']!='15'){
				$this->session->set_userdata('code_cek', '888');				
				redirect('/Profile-School');			
			}
			else{
				$this->session->set_flashdata('mes_alert','success');
				$this->session->set_flashdata('mes_display','block'); 
				$this->session->set_flashdata('mes_message','Berhasil mengubah data diri');
				redirect('About');
			}
			// print_r($res);
			// return false;	

		}

		function saveEditAccountTutor(){
			$res = $this->Master_model->saveEdit_AccountTutor("tbl_user",$this->input->post());

			$this->session->set_flashdata('rets',$res);

		 //print_r($res);
		// $rets['rets'] = $res;

			redirect('/tutor/profile_account');
		}

		function saveEditProfileStudent(){
			$res = $this->Master_model->saveEdit_ProfileStudent("tbl_user",$this->input->post());

			$this->session->set_flashdata('rets', $res);
				$iduser = $this->session->userdata('id_user');
				$cek_data= $this->db->query("SELECT tu.user_nationality, tu.usertype_id, tu.user_religion, tu.user_address, tu.user_image, tps.jenjang_id, tps.school_id FROM tbl_user as tu INNER JOIN tbl_profile_student as tps WHERE tu.id_user='$iduser' AND tps.student_id='$iduser'")->row_array();
				// echo $cek_data['id_desa'];
				if ($cek_data['user_nationality']=="Indonesia") {
					// $this->session->set_userdata('code_cek',"887");
					redirect('About');
					// echo $cek_data['id_desa'];

					
				}
				else if ($cek_data['school_id']=="0" && $cek_data['jenjang_id'] != 15) {
					$this->session->set_userdata('code_cek',"888");
					redirect('Profile-School');
				}
				else if ($cek_data['user_address']==null) {
					$this->session->set_userdata('code_cek',"889");
					redirect('About');
				}

				else{
				$load = $this->lang->line('successfullyupdate');
				$res['mes_alert'] = 'success';
				$res['mes_display'] = 'block';
				$res['mes_message'] = $load;
				$this->session->set_userdata('code_cek',"0");	
				redirect('/About');	
				}

				
		}

		function saveEditSchoolStudent(){
			$res = $this->Master_model->saveEdit_SchoolStudent("tbl_user",$this->input->post());

			$this->session->set_flashdata('rets', $res);
			$this->session->set_userdata('code_cek',"889");	
			redirect('About');

		}

		function saveEditSchoolStudentMobile(){
			$res = $this->Master_model->saveEdit_SchoolStudent("tbl_user",$this->input->post());

			$this->session->set_flashdata('rets', $res);
			$this->session->set_userdata('code_cek',"889");	
			redirect('Profile-Account');

		}

		function saveEditChannel(){
			$res = $this->Master_model->saveEditChannel("master_channel",$this->input->post());

			// $this->session->set_flashdata('rets',$res);

		 //print_r($res);
		// $rets['rets'] = $res;
			
		}

	 	// public function editChannel()
	  //   {
	  //   		$channel_name = $v['channel_name'];
			// 	$channel_description = $v['channel_description'];
			// 	$channel_country_code = $v['channel_country_code'];
			// 	$channel_callnum = $v['channel_callnum'];
			// 	$channel_email = $v['channel_email'];
			// 	$channel_address = $v['channel_address'];
			// 	$channel_logo = $v['channel_logo'];
			// 	$channel_banner = $v['channel_banner'];
			// 	$channel_link = $v['channel_link'];

	  //   	$channel_name 			= $this->input->post('channel_name');
	  //   	$channel_id 			= $this->session->userdata('channel_id');
	  //   	$channel_description 	= $this->input->post('channel_description');
	  //   	$channel_country_code 	= $this->input->post('channel_country_code');
	  //   	$channel_email 			= $this->input->post('channel_email');
	  //   	$channel_callnum 		= $this->input->post('norekeningbank');
	  //   	$channel_address 		= $this->input->post('channel_address');
	  //   	$channel_link			= $this->input->post('metodepembchannel_linkayaran');
	  //   	$channel_banner 		= $channel_id.'.jpg';
	  //   	$channel_logo 			= $channel_id.'.jpg';

	  //   	$where = array(
	  //   		'channel_name' => $channel_name,
	  //   		'channel_id' => $channel_id,
	  //   		'channel_description' => $channel_description,
	  //   		'channel_country_code' => $channel_country_code,
	  //   		'channel_email' => $channel_email,
	  //   		'channel_callnum' => $channel_callnum,
	  //   		'channel_address' => $channel_address,
	  //   		'channel_link' => $channel_link
	  //   	);
	  //   	$edtchannel = $this->Process_model->edttr("log_trf_confirmation", $where);  		
	  //   	$folder 	= './aset/img/channel/logo/';
			// $file_size	= $_FILES['buktibayar']['size'];
			// $max_size	= 2000000;
			// $file_name	= $bukti_tr;

			// if($file_size > $max_size){											
			// 	echo "<script>alert('Ukuran file melebihi batas maximum');</script>";
			// 	redirect('Epocket');
			// }
			// else
			// {
			// 	if (move_uploaded_file($_FILES['buktibayar']['tmp_name'], $folder.$file_name)) {
			// 		$config['image_library'] = 'gd2';
			// 		$config['source_image']	= "./aset/img/buktipembayaran/".$file_name;
			// 		$config['width']	= 250;
			// 		$config['maintain_ratio'] = FALSE;
			// 		$config['height']	= 250;

			// 		$this->image_lib->initialize($config); 

			// 		$this->image_lib->resize();

			// 		$this->session->set_flashdata('mes_alert','success');
			// 		$this->session->set_flashdata('mes_display','block');
			// 		$this->session->set_flashdata('mes_message',"Perubahan data Konfirmasi berhasil, Silahkan tunggu hingga kami selesai mem-verifikasi data Anda");	
			// 		redirect('Epocket');
			// 	}
			// }
	  //   }


		public function test()
		{
			

		}

		public function update_photo()
		{
			$folder 			= './aset/img/user/';
			$parent_id			= $this->session->userdata('parent_id');
			$sessioniduser		= $this->session->userdata('id_user_kids');
			$sessionuserimage 	= $this->session->userdata('user_image_kids');
			// echo $sessioniduser;
			// return false;
			if ($parent_id == null) {
				$sessioniduser		= $this->session->userdata('id_user');
				$sessionuserimage 	= $this->session->userdata('user_image');
			}
			
			// $sessioniduser		= $this->session->userdata('id_user');
			$file_size			= $_FILES['inputFile']['size'];
	        $basename           = $sessioniduser.$this->Rumus->RandomString();
	    	$img_name      		= $basename.'.jpg';
	    	$bukti 				= isset($datas["image"]) ? $datas["image"] : "";
			// if ($sessionuserimage == "empty.jpg") {

				$max_size	= 2000000;
				// $file_name	= $sessioniduser.$this->Rumus->RandomString();
				if($_FILES['inputFile']['size'] > 10000000) { //2 MB (size is also in bytes)						
					$this->session->set_userdata('code','4045');
					if ($this->session->userdata('usertype_id') == "student") {							
						redirect('About');
					}
					else
					{
						redirect('tutor/about');
					}
				}
				else
				{
					$new_file_name = strtolower($_FILES['inputFile']['name']);
					$uploaded = move_uploaded_file($_FILES['inputFile']['tmp_name'], './aset/_temp_images/'.$img_name);	
					/*if($uploaded) {
					  echo 'its good';
					  return true;
					} else {
					  echo 'it failed';
					  echo $_FILES["inputFile"]["error"];
					  return false;
					}*/
					// $image = 'data:image/jpeg;base64,' .str_replace(' ','+',$bukti);

					$updatephoto = $this->db->query("UPDATE tbl_user SET user_image='".base64_encode('user/'.$img_name)."' WHERE id_user='$sessioniduser'");
					$cek_id_user_kids = $this->session->userdata('id_user_kids');
					if ($parent_id == null) {
						$this->session->set_userdata('user_image', CDN_URL.USER_IMAGE_CDN_URL.base64_encode('user/'.$img_name));
					}
					else{
						$this->session->set_userdata('user_image_kids', CDN_URL.USER_IMAGE_CDN_URL.base64_encode('user/'.$img_name));	
					}
					
					
					if ($updatephoto) {
						$config['image_library'] = 'gd2';
						$config['source_image']	= './aset/_temp_images/'.$img_name;
						$config['width']	= 512;
						$config['maintain_ratio'] = FALSE;
						$config['height']	= 512;

						$this->image_lib->initialize($config); 

						$this->image_lib->resize();
						$this->Rumus->sendToCDN('user', $img_name, null, 'aset/_temp_images/'.$img_name );
						
						$this->session->set_flashdata('mes_alert','danger');
						$this->session->set_flashdata('mes_display','block'); 
						$this->session->set_flashdata('mes_message','Berhasil mengupload file');
						$this->session->set_userdata('code','4044');
						if ($this->session->userdata('usertype_id') == "student") {							
							redirect('About');
						}
						else
						{
							redirect('tutor/about');
						}
					}
					else
					{
						if ($this->session->userdata('usertype_id') == "student") {							
							redirect('About');
						}
						else
						{
							redirect('tutor/about');
						}
					}
				}


			// }
			// else
			// {		
			// 	if($_FILES['inputFile']['size'] > 2000000) { //2 MB (size is also in bytes)						
			// 		$this->session->set_userdata('code','4045');
			// 		if ($this->session->userdata('usertype_id') == "student") {							
			// 			redirect('About');
			// 		}
			// 		else
			// 		{
			// 			redirect('tutor/about');
			// 		}
			// 	}
			// 	else
			// 	{													
			// 		if (move_uploaded_file($_FILES['inputFile']['tmp_name'], CDN2_BASEURL, USER_IMAGE_CDN_BASEPATH.'user/', $img_name, $image)) {
			// 			$config['image_library'] = 'gd2';
			// 			$config['source_image']	= CDN2_BASEURL, USER_IMAGE_CDN_BASEPATH.'user/', $img_name, $image;
			// 			$config['width']	= 512;
			// 			$config['maintain_ratio'] = FALSE;
			// 			$config['height']	= 512;

			// 			$this->image_lib->initialize($config); 

			// 			$this->image_lib->resize();

			// 			$this->session->set_flashdata('mes_alert','success');
			// 			$this->session->set_flashdata('mes_display','block');
			// 			$this->session->set_flashdata('mes_message','Berhasil mengupload file');
			// 			$this->session->set_userdata('code','4044');
			// 			if ($this->session->userdata('usertype_id') == "student") {							
			// 				redirect('About');
			// 			}
			// 			else
			// 			{
			// 				redirect('tutor/about');
			// 			}
			// 		}
			// 	}
			// }
		}
		public function update_photo_new($value='')
		{
			$datas = $this->input->post();
			$image_new = $this->input->post('image_new');
			$parent_id			= $this->session->userdata('parent_id');
			$id		= $this->session->userdata('id_user_kids');
			$sessionuserimage 	= $this->session->userdata('user_image_kids');
			// echo $sessioniduser;
			// return false;
			if ($parent_id == null) {
				$id		= $this->session->userdata('id_user');
				$sessionuserimage 	= $this->session->userdata('user_image');
			}
			
			// $id 				= $datas['id_user'];
	        $basename           = $id.$this->Rumus->RandomString();
	    	$img_name      		= $basename.'.jpg';
	    	$bukti 				= isset($datas["image"]) ? $datas["image"] : "";
	    	// $this->db->simple_query("INSERT INTO test(a) VALUES('{$bukti}')");

			// $gambar_base64 = $DATA_DARI_POST['bukti_tr'];
	    	$processadd = $this->db->query("SELECT *FROM tbl_user where id_user = '{$id}'")->row_array();
	    	if ($processadd) {
	            // $image = 'data:image/jpeg;base64,' .str_replace(' ','+',$bukti); //replacing ' ' with '+'               
	                         
	            //Write to disk
	            file_put_contents("aset/_temp_images/".$img_name,file_get_contents($image_new));
	            $updatephoto = $this->db->query("UPDATE tbl_user SET user_image='".base64_encode('user/'.$img_name)."' WHERE id_user='$id'");
	           
	            $this->Rumus->sendToCDN('user', $img_name, null, 'aset/_temp_images/'.$img_name );
	            $this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block'); 
				$this->session->set_flashdata('mes_message','Berhasil mengupload file');
				$this->session->set_userdata('code','4044');
	            $cek_id_user_kids = $this->session->userdata('id_user_kids');
				if ($parent_id == null) {
					$this->session->set_userdata('user_image', CDN_URL.USER_IMAGE_CDN_URL.base64_encode('user/'.$img_name));
				}
				else{
					$this->session->set_userdata('user_image_kids', CDN_URL.USER_IMAGE_CDN_URL.base64_encode('user/'.$img_name));	
				}
	            if ($this->session->userdata('usertype_id') == "student") {							
					redirect('About');
				}
				else
				{
					redirect('tutor/about');
				}
	            return null;
	    		/*if ($processadd['user_image'] == "empty.jpg") {
	    			if($bukti != ''){
						$image = 'data:image/jpeg;base64,' .str_replace(' ','+',$bukti); //replacing ' ' with '+'				
						
						$updatephoto = $this->db->query("UPDATE tbl_user SET user_image='$img_name' WHERE id_user='$id'");
						//Write to disk
						file_put_contents("aset/img/user/".$img_name,file_get_contents($image));
		                $res['status'] = true;
		                // $res['img'] = $image;
						$res['message'] = 'Sukses bro dengan gambar';
						// $this->response($res);
						echo json_encode($res);
						return null;
			            
		        	}
	    		} else {
		    		if($bukti != ''){
						$image = 'data:image/jpeg;base64,' .str_replace(' ','+',$bukti); //replacing ' ' with '+'				
							 
						//Write to disk
						file_put_contents("aset/img/user/".$img_name,file_get_contents($image));
		                $res['status'] = true;
		                // $res['img'] = $image;
						$res['message'] = 'Sukses bro dengan gambar';
						// $this->response($res);
						echo json_encode($res);
						return null;
			            
		        	}
	        	}
				
				$res['status'] = false;
				$res['message'] = 'Gagal bro';		
				echo json_encode($res);
				return null;*/
			}
			else
			{
				$res['status'] = false;
				$res['messag'] = 'Gagal bro';			
				echo json_encode($res);
				return null;
				$this->response($res);
			}	
		}

		public function update_logo()
		{
			$folder 			= './aset/_temp_images/';
			$sessionuserlogo 	= $this->session->userdata('channel_id');
			$sessioniduser		= $this->session->userdata('channel_id')."_l".$this->Rumus->RandomString(15);
			$file_size			= $_FILES['inputFile1']['size'];

			$max_size	= 2000000;
			$file_name	= $sessioniduser.'.jpg';
			if($_FILES['inputFile1']['size'] > 2000000) { //2 MB (size is also in bytes)						
					redirect('Channel/setting');

					$this->session->set_flashdata('mes_alert','danger');
					$this->session->set_flashdata('mes_display','block'); 
					$this->session->set_flashdata('mes_message','Gagal mengupload file');
					$this->session->set_userdata('code','4044');
			}
			else
			{
				move_uploaded_file($_FILES['inputFile1']['tmp_name'], $folder.$file_name);

				$updatelogo = $this->db->query("UPDATE master_channel SET channel_logo='".base64_encode('channel/'.$file_name)."' WHERE channel_id='$sessionuserlogo'");

				if ($updatelogo) {

					$config['image_library'] = 'gd2';
					$config['source_image']	= $folder.$file_name;
					$config['width']	= 512;
					$config['maintain_ratio'] = FALSE;
					$config['height']	= 512;
					$myip = $_SERVER['REMOTE_ADDR'];
					$plogc_id = $this->Rumus->getLogChnTrxID();
					$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$sessionuserlogo','Update logo channel',NULL,'$myip',now())");

					$this->image_lib->initialize($config); 

					$this->image_lib->resize();
					$this->Rumus->sendToCDN('channel', $file_name, null, 'aset/_temp_images/'.$file_name );
					$this->session->set_flashdata('mes_alert','danger');
					$this->session->set_flashdata('mes_display','block'); 
					$this->session->set_flashdata('mes_message','Berhasil mengupload logo');
					$this->session->set_userdata('code','4044');
					redirect('Channel/setting');
				}
				else
				{
					redirect('Channel/setting');

				}
			}
		}
		public function update_logo_new()
		{
			$folder 			= './aset/_temp_images/';
			$image_new = $this->input->post('image_new');
			$sessionuserlogo 	= $this->session->userdata('channel_id');
			$sessioniduser		= $this->session->userdata('channel_id')."_l".$this->Rumus->RandomString(15);
			$file_size			= $_FILES['inputFile1']['size'];

			$max_size	= 2000000;
			$file_name	= $sessioniduser.'.jpg';
			if($_FILES['inputFile1']['size'] > 2000000) { //2 MB (size is also in bytes)						
					redirect('Channel/setting');

					$this->session->set_flashdata('mes_alert','danger');
					$this->session->set_flashdata('mes_display','block'); 
					$this->session->set_flashdata('mes_message','Gagal mengupload file');
					$this->session->set_userdata('code','4044');
			}
			else
			{
				file_put_contents("aset/_temp_images/".$file_name,file_get_contents($image_new));
				// move_uploaded_file($_FILES['inputFile1']['tmp_name'], $folder.$file_name);

				$updatelogo = $this->db->query("UPDATE master_channel SET channel_logo='".base64_encode('channel/'.$file_name)."' WHERE channel_id='$sessionuserlogo'");

				if ($updatelogo) {

					$config['image_library'] = 'gd2';
					$config['source_image']	= $folder.$file_name;
					$config['width']	= 512;
					$config['maintain_ratio'] = FALSE;
					$config['height']	= 512;
					$myip = $_SERVER['REMOTE_ADDR'];
					$plogc_id = $this->Rumus->getLogChnTrxID();
					$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$sessionuserlogo','Update logo channel',NULL,'$myip',now())");

					$this->image_lib->initialize($config); 

					$this->image_lib->resize();
					$this->Rumus->sendToCDN('channel', $file_name, null, 'aset/_temp_images/'.$file_name );
					$this->session->set_flashdata('mes_alert','danger');
					$this->session->set_flashdata('mes_display','block'); 
					$this->session->set_flashdata('mes_message','Berhasil mengupload logo');
					$this->session->set_userdata('code','4044');
					redirect('Channel/setting');
				}
				else
				{
					redirect('Channel/setting');

				}
			}
		}

		public function update_banner()
		{
			
			$banner_new			= $this->post('banner_new');
			$folder 			= './aset/_temp_images/';
			$sessionuserbanner 	= $this->session->userdata('channel_id');
			$sessioniduser		= $this->session->userdata('channel_id')."_b".$this->Rumus->RandomString(15);
			$file_size			= $_FILES['inputFile1']['size'];
				$max_size	= 2000000;
				$file_name	= $sessioniduser.'.jpg';
				if($_FILES['inputBanner']['size'] > 5000000) { //2 MB (size is also in bytes)						
						redirect('Channel/setting');
				}
				else
				{
					// $new_file_name = strtolower($_FILES['inputBanner']['name']);
					// move_uploaded_file($_FILES['inputBanner']['tmp_name'], $folder.$file_name);	
					file_put_contents("aset/_temp_images/".$file_name,file_get_contents($banner_new));

					$updatebanner = $this->db->query("UPDATE master_channel SET channel_banner='".base64_encode('channel/'.$file_name)."' WHERE channel_id='$sessionuserbanner'");
					if ($updatebanner) {

						$config['image_library'] = 'gd2';
						$config['source_image']	= $folder.$file_name;
						$config['width']	= 1386;
						$config['maintain_ratio'] = FALSE;
						$config['height']	= 472;
						// $updatebanner = $this->db->query("UPDATE master_channel SET channel_banner='".base64_encode('channel/'.$file_name)."' WHERE channel_id='$sessionuserbanner'");
						$myip = $_SERVER['REMOTE_ADDR'];
						$plogc_id = $this->Rumus->getLogChnTrxID();
						$this->db->simple_query("INSERT INTO log_channel VALUES('$plogc_id','$sessionuserbanner','Update banner channel',NULL,'$myip',now())");

						$this->image_lib->initialize($config); 

						$this->image_lib->resize();
						$this->Rumus->sendToCDN('channel', $file_name, null, 'aset/_temp_images/'.$file_name );
						$this->session->set_flashdata('mes_alert','danger');
						$this->session->set_flashdata('mes_display','block'); 
						$this->session->set_flashdata('mes_message','Berhasil mengupload banner');
						$this->session->set_userdata('code','4044');
						
						redirect('Channel/setting');
					}
					else
					{
						redirect('Channel/setting');

					}
				}
		}

		public function sizeistobig()
		{
			$this->session->set_userdata('code','4045');
			redirect('/About');
		}

		public function sizeistobigchannel()
		{
			$this->session->set_userdata('code','4145');
			redirect('/channel/setting');
		}

		public function approvalondemand()
		{
			$request_id = $this->input->get('id');
			$updateapprove = $this->db->query("UPDATE tbl_request SET approve='1' WHERE request_id='".$request_id."'");
			if ($updateapprove) {
				echo "<script>alert('berhasil');</script>";
				redirect('tutor/approval_ondemand');
			}
			else
			{
				echo "<script>alert('gagal');</script>";
				redirect('tutor/approval_ondemand');
			}
		}

		public function daftaruser_android()
		{	
			$first_name 	= $this->input->post('first_name');
			$last_name 		= $this->input->post('last_name');
			$user_name 		= $first_name." ".$last_name;
			$email 			= $this->input->post('email');
			$password 		= $this->input->post('password');		
			$jenjang_id 	= $this->input->post('jenjang_id');
			$dob 			= $this->input->post('date_birth');
			$nama_negara	= $this->input->post('nama_negara');
			$countrycode 	= $this->input->post('countrycode');
			$countrycode    = str_replace(" ","+",$countrycode);
			$callnum 		= $this->input->post('callnum');
			$gender 		= $this->input->post('gender');
			$where = array(
				'user_name' => $user_name,
				'first_name' => $first_name,
				'last_name' => $last_name,
				'email' => $email,
				'kata_sandi' => password_hash($password,PASSWORD_DEFAULT),
				'user_countrycode' => $countrycode,
				'user_nationality' => $nama_negara,
				'konfirmasi_ks' => '',
				'tanggal_lahir' => $dob,
				'no_hp_murid' => $callnum,
				'jenis_kelamin' => $gender,
				'jenjang_id' => $jenjang_id
				);

			$res = $this->M_login->daftar_baru("tbl_user",$where);
			$email_tujuan = $res['email'];
			// $email_name = $res['first_name'];
			// $kata_sandi = $res['kata_sandi'];
			$id_user = $res['id_user'];
			$random_key = $res['r_key'];
			$cekcek = $res['cekcek'];


			if ($cekcek == 2) {
				if ($res['key']==2) {
					$load = $this->lang->line('youremail2');
					$array_balikannn = array("message" => $load, "status" => false);	
					echo json_encode($array_balikannn);
					return 0;
				}else{
					$load = $this->lang->line('accountregistered2');
					$array_balikannnn = array("message" => "$email $load", "status" => false);	
					echo json_encode($array_balikannnn);
					return 0;
				}		
			}
			else if ($cekcek == 1) 
			{
				// Open connection
		        $ch = curl_init();

		        // Set the url, number of POST vars, POST data
		        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
		        curl_setopt($ch, CURLOPT_POST, true);
		        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_daftarStudent_android','content' => array("email_tujuan" => "$email_tujuan", "user_name" => "$user_name", "random_key" => "$random_key") )));

		        // Execute post
		        $result = curl_exec($ch);
		        curl_close($ch);
		        // echo $result;


		            if ($result) {
		                  $load = $this->lang->line('successfullyregistered');
						$array_balikann = array("message" => $load, "status" => true);				
						echo json_encode($array_balikann);
						return 1;
		            }else{
		                  $load = $this->lang->line('errorregister');
					$array_balikannn = array("message" => $load, "status" => false);	
					echo json_encode($array_balikannn);
					return 0;    
		            }
			}
		}

		public function daftartutor_android() {
			$first_name 	= $this->input->post('first_name');
			$last_name 		= $this->input->post('last_name');			
			$user_name 		= $first_name." ".$last_name;
			$email 			= $this->input->post('email');
			$password 		= $this->input->post('password');
			$konfirmasi_ps 	= $this->input->post('password');
			$birthdate 		= $this->input->post('date_birth');
			$gender 		= $this->input->post('gender');
			$kode_area 		= $this->input->post('countrycode');
			$nama_negara	= $this->input->post('nama_negara');
			$no_hape 		= $this->input->post('callnum');
			$kodereferral 	= $this->input->post('kode_referral');
			$tutor_callnum 	= $kode_area."".$no_hape;

			$where = array(
				'first_name' => $first_name,
				'last_name' => $last_name,
				'nama_lengkap' => $user_name,
				'email' => $email,
				'kata_sandi' => password_hash($password,PASSWORD_DEFAULT),
				'konfirmasi_ks' => $konfirmasi_ps,
				'tanggal_lahir' => $birthdate,
				'user_nationality' => $nama_negara,
				'user_countrycode' => $kode_area,
				'no_hp_tutor' => $no_hape,
				'jenis_kelamin' => $gender,	
				'referral_id' => $kodereferral
				);

			$res = $this->M_login->daftar_tutor("tbl_user",$where);
			// print_r($res);
			// return false;
			$cekcek = $res['key'];
			// $cekcek =0;

			if ($cekcek != 0) {
				if ($cekcek==2) {
					$load = $this->lang->line('youremail2');
					$array_balikannn = array("message" => $load, "status" => false);	
					echo json_encode($array_balikannn);
					return 0;
				}else{
					$load = $this->lang->line('accountregistered2');
					$array_balikannnn = array("message" => "$email $load", "status" => false);	
					echo json_encode($array_balikannnn);
					return 0;
				}		
			}
			else if ($cekcek == 0) 
			{

				$email_tujuan = $res['email'];			
				$kata_sandi = $password;
				$email_name = $res['first_name'];
				$id_user = $res['id_user'];
				$random_key = $res['r_key']; 
				
				
				

				// Open connection
		        $ch = curl_init();

		        // Set the url, number of POST vars, POST data
		        curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
		        curl_setopt($ch, CURLOPT_POST, true);
		        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_indiclass_daftarTutor_android','content' => array("email_tujuan" => "$email_tujuan", "user_name" => "$user_name", "random_key" => "$random_key") )));

		        // Execute post
		        $result = curl_exec($ch);
		        curl_close($ch);
		        // echo $result;


	            if ($result) {
	                  	$load = $this->lang->line('successfullyregistered');
						$array_balikann = array("message" => $load, "status" => true);				
						echo json_encode($array_balikann);
						return 1;
	            }else{
	                  $load = $this->lang->line('errorregister');
					$array_balikannn = array("message" => "Your email is invalid", "status" => false);	
					echo json_encode($array_balikannn);
					return 0;
	            }
			}
		}

		public function listSubjectChild(){
			$id_user = $this->input->get('child_id');
			$jenjang_id = $this->input->get('jenjang_id');

	        $new_array = array();
			$allsub = $this->db->query("SELECT *FROM master_subject WHERE jenjang_id LIKE '%\"$jenjang_id\"%' OR jenjang_id='$jenjang_id'")->result_array();
			if(!empty($allsub)){
				foreach ($allsub as $row => $v) {
					$alltutor = $this->db->query("SELECT ts.usertype_id, ts.user_name, ts.user_image, mc.nicename as country, tb.*, tb.status as status_tutor FROM tbl_booking as tb  INNER JOIN tbl_user as ts ON tb.id_user=ts.id_user INNER JOIN master_country as mc ON mc.phonecode=ts.user_countrycode WHERE ts.usertype_id='tutor' AND tb.subject_id='$v[subject_id]'")->result_array();
					$new_array[$row] = $allsub[$row];
					$new_array[$row]['tutors'] = array();					

					if(!empty($alltutor)){
						foreach ($alltutor as $row_tutor => $va) { 
							$tutor_id = $va['id_user'];
							$mybooking = $this->db->query("SELECT *FROM tbl_booking WHERE id_user='$id_user'")->result_array();
							$flag = 0;
							if(!empty($mybooking)){
								foreach ($mybooking as $rowbook => $valbook) {
									if ($tutor_id==$valbook['tutor_id'] && $va['subject_id']==$valbook['subject_id']) {
										$flag = 1;
									}
								}
							}
							$alltutor[$row_tutor]['following'] = $flag;
							$new_array[$row]['tutors'][$row_tutor]['tutor_id'] = $tutor_id;
							$new_array[$row]['tutors'][$row_tutor]['tutor_name'] = $va['user_name'];
							$new_array[$row]['tutors'][$row_tutor]['tutor_image'] = $va['user_image'];
							$new_array[$row]['tutors'][$row_tutor]['tutor_country'] = $va['country'];
							$new_array[$row]['tutors'][$row_tutor]['following'] = $flag;
							$new_array[$row]['tutors'][$row_tutor]['status_tutor'] = $va['status_tutor'];

						}
					}
					else
					{
						$rest['status'] 	= false;
						$rest['code']	 	= 1042;
						$rest['data']		= null;
					}
				}			
			}
			else
			{
				$rest['status'] 	= false;
				$rest['code']	 	= 1041;
				$rest['data']		= null;
			}
			$rets['status'] = true;
			$rets['message'] = "Succeed";
			$rest['code']	= 1011;
			$rets['data'] = $new_array;
	        echo json_encode($rets);
		}

		public function get_rekuser(){
			$bank_id = $this->input->get('bank_id');
			$id_user = $this->input->get('id_user') == "" ? $this->session->userdata('id_user') : $this->input->get('id_user');
			$allrek = $this->db->query("SELECT * FROM tbl_rekening WHERE id_user='$id_user' AND bank_id='$bank_id' limit 0,1")->row_array();
			if(!empty($allrek)){
				$rets['status'] = 1;
				$rets['message'] = "Succeed";
				$rets['norek'] = $allrek['nomer_rekening'];
				$rets['nama_akun'] = $allrek['nama_akun'];
			}
			else
			{
				$rets['status'] = 0;
				$rets['message'] = "failed ";			
				$rets['norek'] = null;
				$rets['nama_akun'] = null;
			}
			
	        echo json_encode($rets);
		}

		public function getFeaturedClass()
		{
			$channel_name		= null;
			$user_utc 			= $this->input->post('user_utc');
			$device 			= $this->input->post('device');
			$server_utc         = $this->Rumus->getGMTOffset();
            $interval           = $user_utc - $server_utc;
            $dt 				= new DateTime();       
            $todayInClient		= $dt->format('Y-m-d H:i:s');
   			// $thisBeginDay 		= DateTime::createFromFormat('Y-m-d H:i:s',$todayInClient);
			// $thisBeginDay->modify("+$interval minute");
			// $thisBeginDay 		= $thisBeginDay->format('Y-m-d H:i:s');
            if ($device == "web") {
            	$limit = "8";
            }else{
            	$limit = "4";
            }
			// $data 	= $this->db->query("SELECT tc.class_id,tc.name, tc.description, tc.start_time, tc.finish_time, tc.class_type, tu.user_name, tu.user_image, ms.subject_name, mj.jenjang_name, mj.jenjang_level FROM tbl_class as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN (master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id) ON tc.subject_id=ms.subject_id WHERE tc.finish_time >= '$todayInClient' ORDER BY tc.start_time ASC LIMIT $limit")->result_array();
			$data 	= $this->db->query("SELECT tc.class_id, tc.name, tc.description, tc.start_time, tc.finish_time, tc.class_type, tc.participant, tu.user_name, tu.user_image, ms.subject_name, ms.jenjang_id FROM tbl_class as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN master_subject as ms ON tc.subject_id=ms.subject_id WHERE tc.finish_time >= '$todayInClient' AND tc.class_type != 'private' and tc.class_type !='group' AND (tc.tutor_id!=32 AND tc.tutor_id!=174000 AND tc.tutor_id!=450000 AND tc.tutor_id!=1053000 AND tc.tutor_id!=47) ORDER BY tc.start_time ASC LIMIT $limit")->result_array();
			if (empty($data)) {
				$rets['status'] = false;
				$rets['code'] 	= 404;
				$rets['message'] = null;
				// $rets['data'] = "SELECT * FROM tbl_class WHERE DATE('start_time') = '$datenow'";
			}
			else
			{
				foreach ($data as $row => $va) {
					$statusall = null; 	
					$jenjangnamelevel = array();
					$jenjangid = json_decode($va['jenjang_id'], TRUE);
					if ($jenjangid != NULL) {
						if(is_array($jenjangid)){
							$listclass = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20";
							
							foreach ($jenjangid as $key => $value) {
								$cats = explode(",", $listclass);
									foreach($cats as $cat) {		    
									    if ($cat == $value) {
									    	$statusall = 'Semua Kelas';
									    }
									    else
									    {
									    	$statusall = null;
									    }
									}						
									$jenjang_id[] = $value;
									if ($jenjang_id == $value) {																				
										$data[$row]['jenjang_idd'] = $value;
									}								
								$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$value'")->row_array();

								// $data[$row]['jenjang_level'] = $getAllSubject['jenjang_level'];										
								if ($getAllSubject['jenjang_level']=="0") {
									$jenjangnamelevel[] = $getAllSubject['jenjang_name'];
								}
								else{
									$jenjangnamelevel[] = $getAllSubject['jenjang_name'].' '.$getAllSubject['jenjang_level'];	
								}
								
								$data[$row]['jenjangnamelevel'] = implode(", ", $jenjangnamelevel);
							}
						}
						else
						{
							$getAllSubject = $this->db->query("SELECT jenjang_name, jenjang_level FROM master_jenjang WHERE jenjang_id = '$jenjangid'")->row_array();
							// $data[$row]['jenjang_name'] = $getAllSubject['jenjang_name'];
							// $data[$row]['jenjang_level'] = $getAllSubject['jenjang_level'];
							$jenjangnamelevel[] = $getAllSubject['jenjang_name'].' - '.$getAllSubject['jenjang_level'];	
							$data[$row]['jenjangnamelevel'] = implode(", ", $jenjangnamelevel);			
						}
					}

		            $start_time         = DateTime::createFromFormat ('Y-m-d H:i:s',$va['start_time']);
		            $start_time->modify("+".$interval ." minutes");
		            $start_time         = $start_time->format('Y-m-d H:i:s');
		            $finish_time        = DateTime::createFromFormat ('Y-m-d H:i:s',$va['finish_time']);
		            $finish_time->modify("+".$interval ." minutes");
		            $finish_time        = $finish_time->format('Y-m-d H:i:s');
		            $date          		= date_create($start_time);
		            $datefinish         = date_create($finish_time);
		            $hari          		= date_format($date, 'l');
		            $tanggal            = date_format($date, 'd');
		            $bulan 				= date_format($date, 'F');
		            if ($bulan == 'September'){
		            	$bulan = 'Sept';
		            }else if ($bulan == 'November'){
		            	$bulan = 'Nov';
		            }
		            $tahun 		        = date_format($date, 'Y');
		            $waktu         		= date_format($date, 'H:i');
		            $waktufinish        = date_format($datefinish, 'H:i');
		            $ch 				= curl_init();

					curl_setopt($ch, CURLOPT_URL, 'https://classmiles.com/Tools/get_c/'.$va['class_id']);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					$result 			= curl_exec($ch);
					curl_close($ch);
					$data[$row]['status_all'] = $statusall;
					$link 				= BASE_URL()."class?c=".trim($result);

		            $data[$row]['firstdate'] 	= $tanggal.' '.$bulan.' '.$tahun; 
		            $data[$row]['firsttime'] 	= $waktu;
		            $data[$row]['finishtime'] 	= $waktufinish;
		            $data[$row]['user_image'] 	= CDN_URL.USER_IMAGE_CDN_URL.$va['user_image'];
		            $data[$row]['link'] 		= $link;
		            if ($va['class_type'] == "multicast_paid") {
		            	$harga 	= $this->db->query("SELECT harga_cm FROM tbl_class_price WHERE class_id='$va[class_id]'")->row_array()['harga_cm'];
		            	$data[$row]['harga'] = $harga;
		            }
		            else if ($va['class_type'] == "multicast_channel_paid") {
		            	$listProgram = $this->db->query("SELECT * FROM master_channel_program")->result_array();

		            	$program_id = "";
		            	$channel_id = "";
					    foreach ($listProgram as $key => $value) {        
					        $schedule = $value['schedule'];
					        $json_schedule      = json_decode($schedule,true);
					        $co = 0;
					        foreach ($json_schedule as $keya => $v) {            
					            if($v['class_id'] == $va['class_id']){
					                $co = 1;
					                break;
					            }
					        }
					        if ($co == 1) {					            
					            $program_id = $value['program_id'];
					            $channel_id = $value['channel_id'];
					            break;
					        }
					    }
					    
					    $getPrice = $this->db->query("SELECT price FROM tbl_price_program WHERE program_id = '$program_id'")->row_array()['price'];
					    $getChannelName =$this->db->query("SELECT channel_name FROM master_channel WHERE channel_id = '$channel_id'")->row_array()['channel_name'];
					    $getChannelColor =$this->db->query("SELECT channel_color FROM master_channel WHERE channel_id = '$channel_id'")->row_array()['channel_color'];
					    $data[$row]['channel_name'] = $getChannelName;
					    $data[$row]['channel_color'] = $getChannelColor;
					    if ($getPrice != null) {
					    	$data[$row]['harga'] = $getPrice;
					    }
					    else{
					    	$data[$row]['harga'] = 'Free';
					    }				    
		            }
		            else
		            {
		            	$data[$row]['harga'] = 'Free';
		            }		     
		            $decode = json_decode($va['participant'],true);
					if(isset($decode['participant'])){
						$length = count($decode['participant']);
						$data[$row]['participant_count'] = $length;
					}
					else
					{
						$data[$row]['participant_count'] = 0;
					}       
	        	}
				$rets['status'] = true;
				$rets['code'] 	= 200;
				$rets['message'] = $data;

			}
			echo json_encode($rets);
		}

		public function checkListKids()
		{
			$user_utc 			= $this->input->post('user_utc');
			$device 			= $this->input->post('device');
			$id_user			= $this->session->userdata('id_user');
			$server_utc         = $this->Rumus->getGMTOffset();
            $interval           = $user_utc - $server_utc;
            $dt 				= new DateTime();       
            $todayInClient		= $dt->format('Y-m-d H:i:s');
   			
			$data 	= $this->db->query("SELECT tpk.kid_id, tpk.jenjang_id, tu.user_name, tu.first_name, tu.last_name, tu.user_image from tbl_profile_kid as tpk INNER JOIN tbl_user as tu where tu.id_user = tpk.kid_id and tpk.parent_id='$id_user'")->result_array();


			if (empty($data)) {
				$rets['status'] = false;
				$rets['code'] 	= 404;
				$rets['message'] = "Not found";
			}
			else
			{

				$rets['status'] = true;
				$rets['code'] 	= 200;
				$rets['message'] = $data;

			}
			echo json_encode($rets);
		}

		public function setkodecek()
		{
			$code = $this->input->post('kode_cek');
			$this->session->set_userdata('kode_cek', $code);
			$rets['status'] = true;
			$rets['code'] 	= 200;
			$rets['data'] 	= $code;
			echo json_encode($rets);
			// header("Location:./dummy.html");
		}

		public function testsave()
		{
			file_put_contents("./inc/data.json",$_POST["data"]);
			redirect('first/test');
			// header("Location:./dummy.html");
		}
		public function test2()
		{
			$data = array('username' => 'salahudin', 'first_name' => 'Salah', 'last_name' => 'Udin', 'password' => 'udin123', 'user_birthdate' => '1990-01-02', 'user_birthplace' => 'Bandung', 'user_gender' => 'Laki-laki', 'school_name' => 'SMK 10 Jakarta', 'school_address' => 'Jl. MJ sutoyo');
			$this->Kidsmod->reg_kids($data,'37');
		}
	}