<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajaxer extends CI_Controller {

	public $sesi = array('user_name' => null,'username' => null,'priv_level' => null);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('email');
		$this->load->helper(array('url')); 
	}

	public function getPos($desa_id = null)
	{
		echo $this->Master_model->getPos($desa_id);
	}

	public function getSchoolAddress($d_schoolname = null)
	{
		echo $this->Master_model->getSchoolAddress($d_schoolname);
	}
	public function getSchoolID($d_schoolname = null)
	{
		echo $this->Master_model->getSchoolID($d_schoolname);
	}

	public function getJenjangLevel($value='')
	{

		$term = $this->input->get('term');
		$page  = $this->input->get('page');

		$resCount = 20;

		$offset = ($page - 1) * $resCount;

		$this->db->query("SELECT jenjanglevel_name as id, jenjanglevel_name as text FROM master_jenjanglevel ORDER BY master_jenjanglevel.jenjanglevel_score ASC")->result_array();
		$res['results'] = $this->db->query("SELECT jenjanglevel_name as id, jenjanglevel_name as text FROM master_jenjanglevel WHERE jenjanglevel_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);

	}

	public function getNameuser($value='')
	{
		$term = $this->input->get('term');
		$page  = $this->input->get('page');

		$resCount = 10;

		$offset = ($page - 1) * $resCount;
		// SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id='student'
		// $this->db->query("SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id = 'student'")->result_array();
		$res['results'] = $this->db->query("SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id = 'student' AND user_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);
	}

	public function getJenjangAll($value='')
	{
		$term = $this->input->get('term');
		$page  = $this->input->get('page');

		$resCount = 10;

		$offset = ($page - 1) * $resCount;
		// SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id='student'
		// $this->db->query("SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id = 'student'")->result_array();
		$res['results'] = $this->db->query("SELECT jenjang_id as id, jenjang_name as text, jenjang_level as text2 FROM master_jenjang WHERE jenjang_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);
	}

	public function getStudentName($value='')
	{
		$term = $this->input->get('term');
		$page  = $this->input->get('page');

		$resCount = 10;

		$offset = ($page - 1) * $resCount;
		$channel_id = $this->session->userdata('channel_id') != '' ? $this->session->userdata('channel_id') : "0";
		// SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id='student'
		// $this->db->query("SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id = 'student'")->result_array();
		$res['results'] = $this->db->query("SELECT id_user as id, user_name as text FROM tbl_user WHERE id_user NOT IN (SELECT id_user FROM master_channel_student where channel_id = {$channel_id}) AND usertype_id='student' AND user_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);

	}
	public function getStudentListByChannel($value='')
	{
		$term = $this->input->get('term');
		$page  = $this->input->get('page');

		$resCount = 10;

		$offset = ($page - 1) * $resCount;
		$channel_id = $this->input->get('channel_id') != '' ? $this->input->get('channel_id') : "0";
		// SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id='student'
		// $this->db->query("SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id = 'student'")->result_array();
		$res['results'] = $this->db->query("SELECT id_user as id, user_name as text FROM tbl_user WHERE id_user IN (SELECT id_user FROM master_channel_student where channel_id = {$channel_id}) AND usertype_id='student' AND user_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);
	}
	public function getStudentListByChannelForbidAdded($value='')
	{
		$term = $this->input->get('term');
		$page  = $this->input->get('page');

		$resCount = 10;

		$offset = ($page - 1) * $resCount;
		$channel_id = $this->input->get('channel_id') != '' ? $this->input->get('channel_id') : "0";
		$class_id = $this->session->userdata('idclass') != '' ? $this->session->userdata('idclass') : "0";

		$list_part = $this->db->query("SELECT participant FROM tbl_class WHERE class_id = '$class_id'")->row_array();
		$lar = "";
		if(!empty($list_part)){

			$list_part = json_decode($list_part['participant'], TRUE);
			if(isset($list_part['participant'])){
				foreach ($list_part['participant'] as $key => $value) {
					$lar.= $value['id_user'].",";	
				}
			}

		}
		$lar = rtrim($lar, ','); 
		if($lar == "") {
			$lar = "0";
		}
		// SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id='student'
		// $this->db->query("SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id = 'student'")->result_array();
		$res['results'] = $this->db->query("SELECT id_user as id, user_name as text FROM tbl_user WHERE id_user IN (SELECT id_user FROM master_channel_student where channel_id = {$channel_id} && id_user NOT IN ($lar) ) AND usertype_id='student' AND user_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);	
	}

	public function getMyListChannel($value='')
	{
		$term = $this->input->get('term');
		$page  = $this->input->get('page');

		$resCount = 10;

		$offset = ($page - 1) * $resCount;
		$id_user = $this->session->userdata('id_user');

		$res['results'] = $this->db->query("SELECT mct.channel_id as id, mc.channel_name as text FROM master_channel_tutor as mct INNER JOIN master_channel as mc ON mct.channel_id=mc.channel_id WHERE mct.tutor_id ='$id_user' AND mc.channel_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);
	}

	public function getMyListTutorChannel($value='')
	{
		$term = $this->input->get('term');
		$page  = $this->input->get('page');

		$resCount = 10;

		$offset = ($page - 1) * $resCount;
		$id_user = $this->session->userdata('id_user');
		$channel_id = $this->session->userdata('channel_id');
		
		$res['results'] = $this->db->query("SELECT mct.channel_id as id, tu.user_name as text FROM master_channel_tutor as mct INNER JOIN tbl_user as tu ON mct.tutor_id=tu.id_user WHERE mct.channel_id='$channel_id' AND tu.user_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();

		// $res['results'] = $this->db->query("SELECT mct.channel_id as id, mc.channel_name as text FROM master_channel_tutor as mct INNER JOIN master_channel as mc ON mct.channel_id=mc.channel_id WHERE mct.tutor_id ='$id_user' AND mc.channel_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);

	}

	public function getTutorName ($value='')
	{
		$term = $this->input->get('term');
		$page  = $this->input->get('page');

		$resCount = 10;

		$offset = ($page - 1) * $resCount;
		$channel_id = $this->session->userdata('channel_id');
		// SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id='student'
		// $this->db->query("SELECT id_user as id, user_name as text FROM tbl_user WHERE usertype_id = 'tutor'")->result_array();
		$res['results'] = $this->db->query("SELECT id_user as id, user_name as text FROM tbl_user WHERE id_user NOT IN (SELECT tutor_id FROM master_channel_tutor where channel_id = {$channel_id}) AND usertype_id='tutor' AND user_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);

	}

	public function ajax_indonesia($par)
	{
		$_1 = substr($par, 0, strlen($par) - 3 );

		$term = $this->input->get('term');
		$page  = $this->input->get('page');
		$pfp = $this->input->get('pfp');
		$pfka = $this->input->get('pfka');
		$pfkec = $this->input->get('pfkec');
		$where = "";
		if($par == "kabupaten_id"){
			$where = "AND provinsi_id like '%".$pfp."%'";
		}else if($par == "kecamatan_id"){
			$where = "AND kabupaten_id='".$pfka."'";
		}else if($par == "desa_id"){
			$where = "AND kecamatan_id='".$pfkec."'";
		}
		$resCount = 20;

		$offset = ($page - 1) * $resCount;

		$res['results'] = $this->db->query("SELECT ".$_1."_id as id,".$_1."_name as text FROM master_".$_1." WHERE ".$_1."_name like '%".$term."%' ".$where." limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;			
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);
	}

	public function ajax_indonesia_school($par)
	{
		if ($par != "school_name") {			
			$_1 = substr($par, 7);
		}
		else
		{
			$_1 = $par;
		}

		$term = $this->input->get('term');
		$page  = $this->input->get('page');
		$pfp = $this->input->get('pfp');
		$pfka = $this->input->get('pfka');
		$pfkec = $this->input->get('pfkec');
		// $pfkel = $this->input->get('pfkel');
		$pfname = $this->input->get('pfname');
		$jenjang_id   = $this->session->userdata('jenjang_id');
		$get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$jenjang_id."'")->row_array()['jenjang_name'];
		

		$where = "";
		if($par == "school_kabupaten"){
			$where = "AND provinsi like '%".$pfp."%'";
		}else if($par == "school_kecamatan"){
			$where = "AND kabupaten='".$pfka."'";
		}else if($par == "school_name" && $jenjang_id == null){
			$where = "AND kelurahan='".$pfkel."'";
		}else if($par == "school_name" && $jenjang_id != null){
			$type = substr($get_jenjangtype, 0, 3);
			$where = "AND TYPE LIKE  '".$type."%' AND kecamatan='".$pfkec."'";
		}
		$resCount = 20;

		$offset = ($page - 1) * $resCount;

		$res['results'] = $this->db->query("SELECT ".$_1." as id,".$_1." as text FROM master_school WHERE ".$_1." like '%".$term."%' ".$where." GROUP BY $_1 limit ".$offset.",".$resCount)->result_array();
		// if($par == 'school_name'){
		// 	echo "SELECT ".$_1." as id,".$_1." as text FROM master_school WHERE ".$_1." like '%".$term."%' ".$where." GROUP BY $_1 limit ".$offset.",".$resCount;	
		// 	return false;
		// }
		
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);
	}
	public function ajax_indonesia_school_upd($par)
	{
		if ($par != "upd_school_name") {			
			$_1 = substr($par, 11);
		}
		else if ($par == "upd_school_name"){
			$_1 ="school_name";
		}
		else
		{
			$_1 = $par;
		}

		$term = $this->input->get('term');
		$page  = $this->input->get('page');
		$pfp = $this->input->get('pfp');
		$pfka = $this->input->get('pfka');
		$pfkec = $this->input->get('pfkec');
		// $pfkel = $this->input->get('pfkel');
		$pfname = $this->input->get('pfname');
		$jenjang_id   = $this->input->get('jid');
		$get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$jenjang_id."'")->row_array()['jenjang_name'];
		

		$where = "";
		if($par == "upd_school_kabupaten"){
			$where = "AND provinsi like '%".$pfp."%'";
		}else if($par == "upd_school_kecamatan"){
			$where = "AND kabupaten='".$pfka."'";
		}else if($par == "upd_school_name" && $jenjang_id == null){
			$where = "AND kelurahan='".$pfkel."'";
		}else if($par == "upd_school_name" && $jenjang_id != null){
			$type = substr($get_jenjangtype, 0, 3);
			$where = "AND TYPE LIKE  '".$type."%' AND kecamatan='".$pfkec."'";
		}
		$resCount = 20;

		$offset = ($page - 1) * $resCount;

		$res['results'] = $this->db->query("SELECT ".$_1." as id,".$_1." as text FROM master_school WHERE ".$_1." like '%".$term."%' ".$where." GROUP BY $_1 limit ".$offset.",".$resCount)->result_array();
		// if($par == 'school_name'){
		// 	echo "SELECT ".$_1." as id,".$_1." as text FROM master_school WHERE ".$_1." like '%".$term."%' ".$where." GROUP BY $_1 limit ".$offset.",".$resCount;	
		// 	return false;
		// }
		
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);
	}
	public function ajax_indonesia_school_SD($par)
	{
		if ($par != "school_name") {			
			$_1 = substr($par, 7);
		}
		else
		{
			$_1 = $par;
		}

		$term = $this->input->get('term');
		$page  = $this->input->get('page');
		$pfp = $this->input->get('pfp');
		$pfka = $this->input->get('pfka');
		$pfkec = $this->input->get('pfkec');
		$pfkel = $this->input->get('pfkel');
		$pfname = $this->input->get('pfname');
		$jenjang_id   = $this->session->userdata('jenjang_id'); 
		$get_jenjangtype = $this->db->query("SELECT jenjang_name  FROM master_jenjang where jenjang_id='".$jenjang_id."'")->row_array()['jenjang_name'];
		$where = "";
		if($par == "school_kabupaten"){
			$where = "AND provinsi like '%".$pfp."%'";
		}else if($par == "school_kecamatan"){
			$where = "AND kabupaten='".$pfka."'";
		}else if($par == "school_name"){
			$where = "AND kecamatan='".$pfkec."' AND TYPE ='SD'";
		}
		$resCount = 20;

		$offset = ($page - 1) * $resCount;

		$res['results'] = $this->db->query("SELECT ".$_1." as id,".$_1." as text FROM master_school WHERE ".$_1." like '%".$term."%' ".$where." GROUP BY $_1 limit ".$offset.",".$resCount)->result_array();
		// if($par == 'school_name'){
		// 	echo "SELECT ".$_1." as id,".$_1." as text FROM master_school WHERE ".$_1." like '%".$term."%' ".$where." GROUP BY $_1 limit ".$offset.",".$resCount;	
		// 	return false;
		// }
		
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);
	}

	public function ajax_user_login()
	{

		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
			)
		{
			$dt = $_POST;
			
			$dt['col-display'] = array(
				'us.id_user',
				'us.user_name',
				'hl.logs',
				'hl.imei',
				'hl.datetime'
				);
			$sql  = "SELECT hl.id_user, us.user_name, hl.logs, hl.imei, hl.datetime FROM history_login as hl LEFT JOIN users as us ON hl.id_user=us.id_user";
			$sql1 = $sql;
			$data = $this->db->query($sql);
			$rowCount = $data->num_rows();
			$data->free_result();
        // pengkondisian aksi seperti next, search dan limit
			$columnd = $dt['col-display'];
			$count_c = count($columnd);
        // search
			$search = $dt['search']['value'];
	        /**
	         * Search Global
	         * pencarian global pada pojok kanan atas
	         */
	        $where = '';
	        if ($search != '') {   
	        	for ($i=0; $i < $count_c; $i++) {
	        		$where .= $columnd[$i] .' LIKE "%'. $search .'%"';

	        		if ($i < $count_c - 1) {
	        			$where .= ' OR ';
	        		}
	        	}
	        }
	        if($where != ''){
	        	$where = '( '.$where.' )';
	        }
	        /*if($where == ''){
	        	$where.= ' haji_yearhaji="'.TAHUN_HAJI.'"';
	        }else{
	        	$where.= ' AND haji_yearhaji="'.TAHUN_HAJI.'"';
	        }*/
	        
	        /**
	         * Search Individual Kolom
	         * pencarian dibawah kolom
	         */
	        for ($i=0; $i < $count_c; $i++) { 
	        	$searchCol = $dt['columns'][$i]['search']['value'];
	        	if ($searchCol != '') {
	        		if($where != ''){
	        			$where .= ' AND '. $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';	
	        		}else{
	        			$where .= $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
	        		}
	        		
	        		// break;
	        	}
	        }
	        /**
	         * pengecekan Form pencarian
	         * pencarian aktif jika ada karakter masuk pada kolom pencarian.
	         */
	        if ($where != '') {
	        	$sql .= " WHERE " . $where;

	        }
	        
        // sorting
	        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
	        
	        $list = $this->db->query($sql);
	        $rowCount2 = $list->num_rows();
	        $list->free_result();
        // limit
	        $start  = $dt['start'];
	        $length = $dt['length'];
	        if($length == -1){
	        	$length = $rowCount;
	        }
	        $sql .= " LIMIT {$start}, {$length}";
	        $list = $this->db->query($sql);
        // $vari = $this->db->escape_str(json_encode($sql));
    	// $this->db->query('INSERT INTO fail(fail) values("'.$vari.'")');
	        
	        /*$this->db->query('INSERT INTO fail(fail) values("'.$sql.'")');*/
	        /**
	         * convert to json
	         */
	        $option['draw']            = $dt['draw'];
	        $option['recordsTotal']    = $rowCount;
        // if($sql != $sql1){
	        $option['recordsFiltered'] = $rowCount2;
        // }else{
        	// $option['recordsFiltered'] = $rowCount;
        // }
	        
	        $option['data'] = array();
	        $reverse_column = array("mifare_id","imei","user_name","latitude","longitude","datetime");
	        foreach ($list->result_array() as $row) {
	        	$rows = array();

	        	// for ($i=0; $i < $count_c; $i++) {
	        	$rows[] = $row['id_user'];
	        	$rows[] = $row['user_name'];
	        	$rows[] = $row['logs'];
	        	$rows[] = $row['imei'];
	        	$rows[] = $row['datetime'];
	        	$option['data'][] = $rows;
	        	// }
	        }
        // eksekusi json
	        echo json_encode($option);
	        return null;

	    }
	}
	public function ajax_master_user($kind)
	{

		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
			)
		{
			$dt = $_POST;
			
			$dt['col-display'] = array(
				'id_user',
				'user_name',
				'user_birthdate',
				'user_birthplace',
				'user_email',
				);
			$sql  = "SELECT * FROM users";
			$sql1 = $sql;
			$data = $this->db->query($sql);
			$rowCount = $data->num_rows();
			$data->free_result();
        // pengkondisian aksi seperti next, search dan limit
			$columnd = $dt['col-display'];
			$count_c = count($columnd);
        // search
			$search = $dt['search']['value'];
	        /**
	         * Search Global
	         * pencarian global pada pojok kanan atas
	         */
	        $where = '';
	        if ($search != '') {   
	        	for ($i=0; $i < $count_c; $i++) {
	        		$where .= $columnd[$i] .' LIKE "%'. $search .'%"';

	        		if ($i < $count_c - 1) {
	        			$where .= ' OR ';
	        		}
	        	}
	        }
	        if($where != ''){
	        	$where = '( '.$where.' )';
	        }
	        /*if($where == ''){
	        	$where.= ' haji_yearhaji="'.TAHUN_HAJI.'"';
	        }else{
	        	$where.= ' AND haji_yearhaji="'.TAHUN_HAJI.'"';
	        }*/
	        
	        /**
	         * Search Individual Kolom
	         * pencarian dibawah kolom
	         */
	        for ($i=0; $i < $count_c; $i++) { 
	        	$searchCol = $dt['columns'][$i]['search']['value'];
	        	if ($searchCol != '') {
	        		if($where != ''){
	        			$where .= ' AND '. $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';	
	        		}else{
	        			$where .= $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
	        		}
	        		
	        		// break;
	        	}
	        }
	        /**
	         * pengecekan Form pencarian
	         * pencarian aktif jika ada karakter masuk pada kolom pencarian.
	         */
	        if ($where != '') {
	        	$sql .= " WHERE " . $where." AND id_priviledge='".$kind."'";

	        }else{
	        	$sql .= " WHERE id_priviledge='".$kind."'";	        	
	        }
	        
        // sorting
	        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
	        
	        $list = $this->db->query($sql);
	        $rowCount2 = $list->num_rows();
	        $list->free_result();
        // limit
	        $start  = $dt['start'];
	        $length = $dt['length'];
	        $sql .= " LIMIT {$start}, {$length}";
	        $list = $this->db->query($sql);
        // $vari = $this->db->escape_str(json_encode($sql));
    	// $this->db->query('INSERT INTO fail(fail) values("'.$vari.'")');
	        
	        /*$this->db->query('INSERT INTO fail(fail) values("'.$sql.'")');*/
	        /**
	         * convert to json
	         */
	        $option['draw']            = $dt['draw'];
	        $option['recordsTotal']    = $rowCount;
        // if($sql != $sql1){
	        $option['recordsFiltered'] = $rowCount2;
        // }else{
        	// $option['recordsFiltered'] = $rowCount;
        // }
	        
	        $option['data'] = array();
	        foreach ($list->result_array() as $row) {
	        	$rows = array();

	        	// for ($i=0; $i < $count_c; $i++) {
	        		$rows[] = $row['id_user'];
	        		$rows[] = $row['user_name'];
	        		$rows[] = $row['user_birthplace'].", ".$row['user_birthdate'];
	        		$rows[] = $row['user_email'];
	        		$rows[] = "<div class='col-md-6'><a href='".base_url()."first/edit_user/".$kind."/".$row['id_user']."' class='btns btn btn-info glyphicon glyphicon-pencil'></a></div><div class='col-md-6'><a class='btn btn-danger' href='".base_url()."master/delete_user/".$kind."/".$row['id_user']."' onclick='return delconf();'><i class='glyphicon glyphicon-trash'></i></a></div>";

	        	// }
	        	$option['data'][] = $rows;
	        }
        // eksekusi json
	        echo json_encode($option);
	        return null;

	    }
	}

	public function ajax_get_ondemand_group()
	{
		$rets = array();
		$subject_id = htmlentities($this->input->get('subject_id'));
		$datetime_request = htmlentities($this->input->get('dtrq')." ".$this->input->get('tmrq').":00");
		$durasi = htmlentities($this->input->get('drrq'));
		$user_utc = htmlentities($this->input->get('user_utc'));

		$server_utc = $this->Rumus->getGMTOffset();
		$interval = $user_utc - $server_utc;
		$date_requested = DateTime::createFromFormat ('Y-m-d H:i:s', $datetime_request );
		$date_requested->modify("-".$interval ." minutes");
		$date_requested = $date_requested->format('Y-m-d H:i:s');

		$fn_time = DateTime::createFromFormat('Y-m-d H:i:s', $date_requested);
		$fn_time->modify("+$durasi second");
		$fn_time = $fn_time->format('Y-m-d H:i:s');
		// echo $date_requested." - ".$fn_time;

		// $new_arr = $this->db->query("SELECT tsp.*, ms.jenjang_id, tb.uid as booking_id, ms.subject_name, mj.jenjang_name, mj.jenjang_level,tu.user_name, mc.nicename as country, tu.user_image, tb.status as status_tutor FROM tbl_service_price as tsp INNER JOIN ( master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id ) ON tsp.subject_id=ms.subject_id INNER JOIN ( tbl_user as tu INNER JOIN master_country as mc ON mc.nicename=tu.user_nationality ) ON tsp.tutor_id=tu.id_user INNER JOIN tbl_booking as tb ON tsp.tutor_id=tb.id_user AND tsp.subject_id=tb.subject_id WHERE tsp.subject_id='{$subject_id}' AND tsp.class_type='group' AND tu.status='1'")->result_array();
		$new_arr_baru = $this->db->query("SELECT tsp.*, ms.jenjang_id, tb.uid as booking_id, ms.subject_name,tu.user_name, mc.nicename as country, tu.user_image, tb.status as status_tutor FROM tbl_service_price as tsp INNER JOIN master_subject as ms ON tsp.subject_id=ms.subject_id INNER JOIN ( tbl_user as tu INNER JOIN master_country as mc ON mc.nicename=tu.user_nationality INNER JOIN tbl_profile_tutor as tpt ON tu.id_user=tpt.tutor_id ) ON tsp.tutor_id=tu.id_user INNER JOIN tbl_booking as tb ON tsp.tutor_id=tb.id_user AND tsp.subject_id=tb.subject_id WHERE tsp.subject_id='{$subject_id}' AND tsp.class_type='group' AND tu.status='1' AND tpt.flag='1'")->result_array();
		if (!empty($new_arr_baru)) {	
			$ckck = 1;
			$noTutor = "";
			foreach ($new_arr_baru as $ky => $v) {
				// CHECK JIKA WAKTU TELAH TERISI				
				$tutor_id = $v['tutor_id'];

				//CEK IRISAN
				$cjwtt = $this->db->query("SELECT * FROM tbl_class WHERE ( ( '$date_requested' <= start_time AND '$fn_time' > start_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) OR ( '$date_requested' >= start_time AND '$date_requested' < finish_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) ) AND tutor_id=$tutor_id ")->result_array();

				$validate_from_tbl_req = $this->db->query("SELECT * FROM tbl_request WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND tutor_id=$tutor_id AND approve=2")->result_array();

				$validate_from_tbl_req_grup = $this->db->query("SELECT * FROM tbl_request_grup WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND tutor_id=$tutor_id AND approve=2")->result_array();
				if(!empty($cjwtt) || !empty($validate_from_tbl_req) || !empty($validate_from_tbl_req_grup) ){
					// BENTROK					
					$noTutor = $noTutor."AND tpt.tutor_id!='$tutor_id'";									
				}				
			}

			$new_arr = $this->db->query("SELECT tsp.*, ms.jenjang_id, ms.subject_name,tu.user_name, tu.status, mc.nicename as country, tu.user_image FROM tbl_service_price as tsp INNER JOIN master_subject as ms ON tsp.subject_id=ms.subject_id  INNER JOIN ( tbl_user as tu INNER JOIN master_country as mc ON mc.nicename=tu.user_nationality INNER JOIN tbl_profile_tutor as tpt ON tu.id_user=tpt.tutor_id) ON tsp.tutor_id=tu.id_user WHERE tsp.subject_id='{$subject_id}' AND tsp.class_type='private' AND tu.status='1' AND tpt.flag='1' $noTutor")->result_array();

			foreach ($new_arr as $key => $value) {
				// CHECK JIKA WAKTU TELAH TERISI
				$hargaawal = ($durasi/900)*$value['harga'];
				$hargaakhir = ($durasi/900)*$value['harga'];
				$i = $hargaakhir*0.2;
				if ($i < 5000) {
					$i = 5000;
				}
				$hargaakhir = $i + $hargaakhir;
				$new_arr[$key]['harga_tutor'] = $hargaawal;
				$new_arr[$key]['hargaakhir'] = number_format($hargaakhir, 0, ".", ".");
				$new_arr[$key]['harga_cm'] = $hargaakhir;

				foreach ($new_arr[$key] as $k => $v) {				
					if($k == "user_image"){							
						$new_arr[$key]['user_image'] = CDN_URL.USER_IMAGE_CDN_URL.$v;
						$sbjt = $new_arr[$key]['subject_name'];
					}
					if($k == "harga"){
						$gh = $this->encryption->encrypt($v);
						$base64 = strtr($gh, '+/=', '-_.');
						$new_arr[$key]["gh"] = $base64;
					}
				}
			}

			if ($ckck == 1) {
				$new_arr = array_values($new_arr);

				$rets['status'] = true;
				$rets['data'] = $new_arr;
				$rets['subject_name'] = $sbjt;
				echo json_encode($rets);
			}
			else
			{
				$rets['status'] = false;
				$rets['data'] = null;
				echo json_encode($rets);
			}
		}
		else
		{
			$rets['status'] = false;
			$rets['data'] = array();
			echo json_encode($rets);
		}
	}

	public function ajax_get_ondemand()
	{
		$rets = array();
		$subject_id = htmlentities($this->input->get('subject_id'));
		$datetime_request = htmlentities($this->input->get('dtrq')." ".$this->input->get('tmrq').":00");
		$durasi = htmlentities($this->input->get('drrq'));
		$user_utc = htmlentities($this->input->get('user_utc'));

		$server_utc = $this->Rumus->getGMTOffset();
		$interval = $user_utc - $server_utc;
		$date_requested = DateTime::createFromFormat ('Y-m-d H:i:s',$datetime_request);		
		$date_requested->modify("-".$interval ." minutes");
		$date_requested = $date_requested->format('Y-m-d H:i:s');

		$fn_time = DateTime::createFromFormat('Y-m-d H:i:s', $date_requested);
		$fn_time->modify("+$durasi second");
		$fn_time = $fn_time->format('Y-m-d H:i:s');
		// echo $date_requested." - ".$fn_time;

		// $new_arr = $this->db->query("SELECT tsp.*, ms.jenjang_id, tb.uid as booking_id, ms.subject_name, mj.jenjang_name, mj.jenjang_level,tu.user_name, tu.status, mc.nicename as country, tu.user_image, tb.status as status_tutor FROM tbl_service_price as tsp INNER JOIN ( master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id ) ON tsp.subject_id=ms.subject_id  INNER JOIN ( tbl_user as tu INNER JOIN master_country as mc ON mc.nicename=tu.user_nationality ) ON tsp.tutor_id=tu.id_user INNER JOIN tbl_booking as tb ON tsp.tutor_id=tb.id_user AND tsp.subject_id=tb.subject_id WHERE tsp.subject_id='{$subject_id}' AND tsp.class_type='private' AND tu.status='1'")->result_array();		 
		$new_arr_baru = $this->db->query("SELECT tsp.*, ms.jenjang_id, ms.subject_name,tu.user_name, tu.status, mc.nicename as country, tu.user_image FROM tbl_service_price as tsp INNER JOIN master_subject as ms ON tsp.subject_id=ms.subject_id  INNER JOIN ( tbl_user as tu INNER JOIN master_country as mc ON mc.nicename=tu.user_nationality INNER JOIN tbl_profile_tutor as tpt ON tu.id_user=tpt.tutor_id) ON tsp.tutor_id=tu.id_user WHERE tsp.subject_id='{$subject_id}' AND tsp.class_type='private' AND tu.status='1' AND tpt.flag='1'")->result_array();	
		if (!empty($new_arr_baru)) {		
			$ckck = 1;
			$noTutor = "";
			foreach ($new_arr_baru as $ky => $v) {
				// CHECK JIKA WAKTU TELAH TERISI				
				$tutor_id = $v['tutor_id'];

				//CEK IRISAN
				$cjwtt = $this->db->query("SELECT * FROM tbl_class WHERE ( ( '$date_requested' <= start_time AND '$fn_time' > start_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) OR ( '$date_requested' >= start_time AND '$date_requested' < finish_time AND ( '$fn_time' <= finish_time OR '$fn_time' > finish_time )  ) ) AND tutor_id=$tutor_id ")->result_array();

				$validate_from_tbl_req = $this->db->query("SELECT * FROM tbl_request WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND tutor_id=$tutor_id AND approve=2")->result_array();

				$validate_from_tbl_req_grup = $this->db->query("SELECT * FROM tbl_request_grup WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND tutor_id=$tutor_id AND approve=2")->result_array();
				if(!empty($cjwtt) || !empty($validate_from_tbl_req) || !empty($validate_from_tbl_req_grup) ){
					// BENTROK					
					$noTutor = $noTutor."AND tpt.tutor_id!='$tutor_id'";									
				}
			}

			$new_arr = $this->db->query("SELECT tsp.*, ms.jenjang_id, ms.subject_name,tu.user_name, tu.status, mc.nicename as country, tu.user_image FROM tbl_service_price as tsp INNER JOIN master_subject as ms ON tsp.subject_id=ms.subject_id  INNER JOIN ( tbl_user as tu INNER JOIN master_country as mc ON mc.nicename=tu.user_nationality INNER JOIN tbl_profile_tutor as tpt ON tu.id_user=tpt.tutor_id) ON tsp.tutor_id=tu.id_user WHERE tsp.subject_id='{$subject_id}' AND tsp.class_type='private' AND tu.status='1' AND tpt.flag='1' $noTutor")->result_array();

			foreach ($new_arr as $key => $value) {
				$hargaawal = ($durasi/900)*$value['harga'];
				$hargaakhir = ($durasi/900)*$value['harga'];
				$i = $hargaakhir*0.2;
				if ($i < 5000) {
					$i = 5000;
				}
				$hargaakhir = $i + $hargaakhir;
				// $hargaakhir = ($durasi/900)*$value['harga_cm'];
				$new_arr[$key]['harga_tutor'] = $hargaawal;
				$new_arr[$key]['harga_cm'] = $hargaakhir;
				$new_arr[$key]['hargaakhir'] = number_format($hargaakhir, 0, ".", ".");
								
				foreach ($new_arr[$key] as $k => $v) {								
					if($k == "user_image"){
						$new_arr[$key]['user_image'] = CDN_URL.USER_IMAGE_CDN_URL.$v;
						$sbjt = $new_arr[$key]['subject_name'];
					}
					if($k == "harga"){
						$gh = $this->encryption->encrypt($v);
						$base64 = strtr($gh, '+/=', '-_.');
						$new_arr[$key]["gh"] = $base64;
					}
				}					
				
			}
			if ($ckck == 1) {
				$new_arr = array_values($new_arr);

				$rets['status'] = true;
				$rets['data'] = $new_arr;
				echo json_encode($rets);
			}
			else
			{
				$rets['status'] = false;
				$rets['data'] = null;
				echo json_encode($rets);
			}
			
			/*print_r('<pre>');
			print_r($rets);
			print_r('</pre>');*/
		}
		else
		{
			$rets['status'] = false;
			$rets['data'] = array();
			echo json_encode($rets);
		}
		
	}

	public function ajax_get_approval_ondemand($user_utc='')
	{
		$id_user = $this->session->userdata('id_user');

		$server_utc = $this->Rumus->getGMTOffset();
		$interval   = $user_utc - $server_utc;

		$data = $this->db->query("SELECT tr.*, DATE_ADD(tr.date_requested, INTERVAL $interval MINUTE) as valid_date_requested, tu.user_name, tu.user_image FROM tbl_request as tr LEFT JOIN (tbl_user as tu LEFT JOIN (tbl_profile_student as tps LEFT JOIN master_jenjang as mj ON tps.jenjang_id=mj.jenjang_id) ON tu.id_user=tps.student_id LEFT JOIN tbl_profile_kid as tpk ON tpk.kid_id=tu.id_user) ON tr.id_user_requester=tu.id_user INNER JOIN master_subject as ms ON tr.subject_id=ms.subject_id  WHERE tr.tutor_id='{$id_user}' AND tr.approve=0 ORDER BY tr.request_id DESC")->result_array();

		foreach ($data as $key => $v) {
			$tutorid = $v['tutor_id'];
			$sujectid = $v['subject_id'];
			$getjenjang = $this->db->query("SELECT ms.subject_name, mj.jenjang_level, mj.jenjang_name FROM master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE ms.subject_id='$sujectid'")->row_array();

			$priceprivate = $this->db->query("SELECT harga FROM tbl_service_price WHERE tutor_id='$tutorid' AND class_type='private' AND subject_id='$sujectid'")->row_array()['harga'];
			$seconds = $v['duration_requested'];
		    $hours = floor($seconds / 3600);
		    $mins = floor($seconds / 60 % 60);
		    $secs = floor($seconds % 60);
		    $timeFormat = sprintf('%02d:%02d', $hours, $mins);
		    $data[$key]['subject_name'] = $getjenjang['subject_name'];
		    $data[$key]['jenjang_level'] = $getjenjang['jenjang_level'];
		    $data[$key]['jenjang_name'] = $getjenjang['jenjang_name'];
		    $data[$key]['duration_requested'] = $timeFormat;
		    $hargaakhir = ($v['duration_requested']/900)*$priceprivate;
		    $hargaakhir     = number_format($hargaakhir, 0, ".", ".");
		    $data[$key]['harga'] = $hargaakhir;
		}
		$rets['status'] = true;
		$rets['data'] = $data;
		echo json_encode($rets);

	}

	public function ajax_get_approval_ondemand_group($user_utc='')
	{
		$id_user = $this->session->userdata('id_user');

		$server_utc = $this->Rumus->getGMTOffset();
		$interval   = $user_utc - $server_utc;

		$data = $this->db->query("SELECT trg.*, DATE_ADD(trg.date_requested, INTERVAL $interval MINUTE) as valid_date_requested, tu.user_name, tu.user_image FROM tbl_request_grup as trg INNER JOIN (tbl_user as tu INNER JOIN (tbl_profile_student as tps INNER JOIN master_jenjang as mj ON tps.jenjang_id=mj.jenjang_id) ON tu.id_user=tps.student_id) ON trg.id_user_requester=tu.id_user INNER JOIN master_subject as ms ON trg.subject_id=ms.subject_id WHERE trg.tutor_id='{$id_user}' AND trg.approve=0 AND trg.id_friends NOT LIKE '%\"status\":0%' ORDER BY trg.request_id DESC")->result_array();

		foreach ($data as $key => $v) {
			$subjectid = $v['subject_id'];
			$tutorid = $v['tutor_id'];
			$getjenjang = $this->db->query("SELECT ms.subject_name, mj.jenjang_level, mj.jenjang_name FROM master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id WHERE ms.subject_id='$subjectid'")->row_array();
			$getprice = $this->db->query("SELECT harga FROM tbl_service_price WHERE class_type='group' AND tutor_id='$tutorid' AND subject_id='$subjectid'")->row_array();
			$hargakahir = ($v['duration_requested']/900)*$getprice['harga'];
            $seconds = $v['duration_requested'];
		    $hours = floor($seconds / 3600);
		    $mins = floor($seconds / 60 % 60);
		    $secs = floor($seconds % 60);
		    $timeFormat = sprintf('%02d:%02d', $hours, $mins);
		    $data[$key]['subject_name'] = $getjenjang['subject_name'];
		    $data[$key]['jenjang_level'] = $getjenjang['jenjang_level'];
		    $data[$key]['jenjang_name'] = $getjenjang['jenjang_name'];
		    $data[$key]['duration_requested'] = $timeFormat;
		    $data[$key]['duration'] = $v['duration_requested'];
		    $data[$key]['harga'] = $hargakahir;
		    $data[$key]['hargatampil'] = number_format($hargakahir, 0, ".", ".");
		}
		$rets['status'] = true;
		$rets['data'] = $data;
		echo json_encode($rets);
	}	

	public function ajax_jenjang_name($par='')
	{
		$_1 = substr($par, 0, strlen($par) - 3 );

		$term = $this->input->get('term');
		$page  = $this->input->get('page');
		$where = "";
		$resCount = 20;
		
		$offset = ($page - 1) * $resCount;

		$res['results'] = $this->db->query("SELECT jenjang_name as id, jenjang_name as text FROM master_jenjang WHERE jenjang_name like '%".$term."%' group by jenjang_name limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);
	}
	public function ajax_jenjang_level($par = '')
	{
		$_1 = substr($par, 0, strlen($par) - 3 );

		$term = $this->input->get('term');
		$page  = $this->input->get('page');
		/*$jname = $this->input->get('jname');
		if($jname == ''){
			$jname = 'AAAA';
		}*/
		$where = "";
		$resCount = 20;

		$offset = ($page - 1) * $resCount;

		/*$res['results'] = $this->db->query("SELECT jenjang_id as id , CONCATE(jenjang_name,' ',jenjang_level) as text FROM master_jenjang WHERE jenjang_level like '%".$term."%' AND jenjang_name like '%".$jname."%' limit ".$offset.",".$resCount)->result_array();*/
		$res['results'] = $this->db->query("SELECT jenjang_id as id , CONCAT(jenjang_name,' ',jenjang_level) as text FROM master_jenjang WHERE jenjang_level like '%".$term."%' AND jenjang_name NOT LIKE 'UMUM' and jenjang_name NOT LIKE 'TK' and jenjang_name NOT LIKE 'SD' and curriculum='Indonesia' order by jenjang_level limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);
	}

	public function ajax_jenjang_level_SD($par = '')
	{
		$_1 = substr($par, 0, strlen($par) - 3 );

		$term = $this->input->get('term');
		$page  = $this->input->get('page');
		/*$jname = $this->input->get('jname');
		if($jname == ''){
			$jname = 'AAAA';
		}*/
		$where = "";
		$resCount = 20;

		$offset = ($page - 1) * $resCount;

		/*$res['results'] = $this->db->query("SELECT jenjang_id as id , CONCATE(jenjang_name,' ',jenjang_level) as text FROM master_jenjang WHERE jenjang_level like '%".$term."%' AND jenjang_name like '%".$jname."%' limit ".$offset.",".$resCount)->result_array();*/
		$res['results'] = $this->db->query("SELECT jenjang_id as id , CONCAT(jenjang_name,' ',jenjang_level) as text FROM master_jenjang WHERE jenjang_level like '%".$term."%' and jenjang_name LIKE 'SD' order by jenjang_level limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);
	}
	public function ajax_jenjang_level_ALL($par = '')
	{
		$_1 = substr($par, 0, strlen($par) - 3 );

		$term = $this->input->get('term');
		$page  = $this->input->get('page');
		/*$jname = $this->input->get('jname');
		if($jname == ''){
			$jname = 'AAAA';
		}*/
		$where = "";
		$resCount = 20;

		$offset = ($page - 1) * $resCount;

		/*$res['results'] = $this->db->query("SELECT jenjang_id as id , CONCATE(jenjang_name,' ',jenjang_level) as text FROM master_jenjang WHERE jenjang_level like '%".$term."%' AND jenjang_name like '%".$jname."%' limit ".$offset.",".$resCount)->result_array();*/
		$res['results'] = $this->db->query("SELECT jenjang_id as id , CONCAT(jenjang_name,' ',jenjang_level) as text FROM master_jenjang WHERE jenjang_level like '%".$term."%' and curriculum ='Indonesia'  and jenjang_name NOT LIKE 'TK' and jenjang_name NOT LIKE 'UMUM' order by jenjang_level limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);
	}
	public function ajax_users()
	{
		$term = $this->input->get('term');
		$page  = $this->input->get('page');
		$resCount = 20;

		$offset = ($page - 1) * $resCount;

		$res['results'] = $this->db->query("SELECT id_user as id,user_name as text FROM users WHERE user_name like '%".$term."%' limit ".$offset.",".$resCount)->result_array();
		$total = count($res['results']);
		if($total < $resCount){
			$res['more'] = 0;
		}else{
			$res['more'] = 1;
		}
		
		echo json_encode($res);
	}
		public function ajax_purchase()
	{
		$id_user            = $this->input->get('id_user');           
		$anak               = $this->db->query("SELECT * FROM tbl_profile_kid WHERE parent_id='$id_user'")->result_array();
		$utc 				= $this->input->get('utc');
		$user_utc 			= $this->Rumus->getGMTOffset('minute', $utc);
		$q_anak = "";
		$pagination = $this->input->get('page');
		$last = 5;
		$first = $pagination * 5;

		if(!empty($anak)){
		    foreach ($anak as $key => $value) {
		        $q_anak.= " OR tor.buyer_id='".$value['kid_id']."' ";
		    }
		}
		    
		$getdatainvoice   = $this->db->query("SELECT * FROM tbl_invoice as ti INNER JOIN tbl_order as tor ON ti.order_id=tor.order_id WHERE tor.buyer_id='$id_user' $q_anak ORDER BY ti.created_at DESC LIMIT $first, $last")->result_array();
		if (empty($getdatainvoice)) {
			$res['status'] = false;
			$res['message'] = 'failed empty';
			$res['data'] = null;
			echo json_encode($res);
		}
		else
		{
			foreach ($getdatainvoice as $row => $v) {
                $invoice_id     = $v['invoice_id'];
                // $product_id     = $v['product_id'];
                $order_id       = $v['order_id'];
                $order_status   = $v['order_status'];

                $get_orderdetail = $this->db->query("SELECT tod.*, tr.subject_id as tr_subject_id, tr.tutor_id as tr_tutor_id, tr.approve as tr_approve, tr.topic as tr_topic, trg.subject_id as trg_subject_id, trg.tutor_id as trg_tutor_id, trg.approve as trg_approve, trg.topic as trg_topic, tc.subject_id as trm_subject_id, tc.tutor_id as trm_tutor_id, trm.status as trm_approve, tc.description as trm_topic, tpp.name as program_name, tpp.description as trp_topic, trp.status as trp_approve, tpp.channel_id as trp_channel_id FROM tbl_order_detail as tod LEFT JOIN tbl_request as tr ON tod.product_id=tr.request_id LEFT JOIN tbl_request_grup as trg ON trg.request_id=tod.product_id LEFT JOIN ( tbl_request_multicast as trm INNER JOIN tbl_class as tc ON tc.class_id=trm.class_id ) ON trm.request_id=tod.product_id LEFT JOIN (tbl_request_program as trp INNER JOIN tbl_price_program as tpp ON trp.program_id=tpp.list_id ) ON trp.request_id=tod.product_id WHERE tod.order_id='$order_id'")->result_array(); 
                
                $harga          = $v['total_price'];
                $hargaprice     = number_format($harga, 0, ".", ".");        

                $status = null;
                foreach ($get_orderdetail as $key => $value) {
                    // $subject_id     = $value['tr_subject_id'] != null ? $value['tr_subject_id'] : ( $value['trg_subject_id'] != null ? $value['trg_subject_id'] : $value['trm_subject_id']);
                    // $tutor_id       = $value['tr_tutor_id'] != null ? $value['tr_tutor_id'] : ( $value['trg_tutor_id'] != null ? $value['trg_tutor_id'] : $value['trm_tutor_id']);
                    // $status         = $value['tr_approve'] != null ? $value['tr_approve'] : ( $value['trg_approve'] != null ? $value['trg_approve'] : $value['trm_approve']);
                    $subject_id     = $value['tr_subject_id'] != null ? $value['tr_subject_id'] : ( $value['trg_subject_id'] != null ? $value['trg_subject_id'] : ($value['trm_subject_id'] != null ? $value['trm_subject_id'] : ''));
                    $tutor_id       = $value['tr_tutor_id'] != null ? $value['tr_tutor_id'] : ( $value['trg_tutor_id'] != null ? $value['trg_tutor_id'] : ( $value['trm_tutor_id'] != null ? $value['trm_tutor_id'] : ''));
                    $status         = $value['tr_approve'] != null ? $value['tr_approve'] : ( $value['trg_approve'] != null ? $value['trg_approve'] : ( $value['trm_approve'] != null ? $value['trm_approve'] : $value['trp_approve']));
                    $topic          = $value['tr_topic'] != null ? $value['tr_topic'] : ( $value['trg_topic'] != null ? $value['trg_topic'] : ($value['trm_topic'] != null ? $value['trm_topic'] : $value['trp_topic']));

                    if ($subject_id != '') {
	                    $datasub   = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array();
	                    $subject_name   = $datasub['subject_name'];
	                    // if ($datasub['jenjang_level'] != 0) {
	                    // 	$jenjang_data   = $datasub['jenjang_name'].' '.$datasub['jenjang_level'];
	                    // } else {
	                    // 	$jenjang_data   = $datasub['jenjang_name'];
	                    // }
	                }else
	                {
	                	$subject_name  = $value['program_name'];
                        $jenjang_data  = '';
	                }
	                if ($tutor_id != '') {
	                    $data_tutor     = $this->db->query("SELECT first_name,user_image FROM tbl_user WHERE id_user='$tutor_id'")->row_array();
	                    $tutor_name     = $data_tutor['first_name'];
	                    $tutor_image    = $data_tutor['user_image'];
	                }
	                else
	                {
	                	$channel_id     = $value['trp_channel_id'];
                        $data_channel   = $this->db->query("SELECT channel_name,channel_logo FROM master_channel WHERE channel_id='$channel_id'")->row_array();
                        $tutor_name     = $data_channel['channel_name'];
                        $tutor_image    = $data_channel['channel_logo'];
	                }

                    $get_orderdetail[$key]['subject_name'] 		= $subject_name;
                    $get_orderdetail[$key]['jenjang_name'] 		= null;
                    $get_orderdetail[$key]['tutor_name'] 		= $tutor_name;
                    $get_orderdetail[$key]['tutor_image'] 		= $tutor_image;
                }

                $cekKonfirmasi 		= $this->db->query("SELECT * FROM log_trf_confirmation WHERE trx_id='$order_id'")->row_array();
                if (empty($cekKonfirmasi)) {
                	$getdatainvoice[$row]['trf_confirmation'] 	= true;
                }
                else
                {
                	$getdatainvoice[$row]['trf_confirmation'] 	= false;
                }
                $payment_type       = $v['payment_type'];
	            $UnixCode           = $v['unix_code'];
	            $hargakelas         = $v['total_price']-$UnixCode;
	            $harga_akhir     	= number_format($hargakelas, 0, ".", ".");        
                $server_utcc        = $this->Rumus->getGMTOffset();
                $intervall          = $user_utc - $server_utcc;

                $tanggaltransaksi   = DateTime::createFromFormat ('Y-m-d H:i:s',$v['payment_due']);
	            $tanggaltransaksi->modify("+".$intervall ." minutes");
	            $tanggaltransaksi   = $tanggaltransaksi->format('Y-m-d H:i:s');

                $tanggalcreate   = DateTime::createFromFormat ('Y-m-d H:i:s',$v['created_at']);
	            $tanggalcreate->modify("+".$intervall ." minutes");
	            $tanggalcreate   = $tanggalcreate->format('Y-m-d H:i:s');

	            $dateee             = $tanggaltransaksi;
	            $datetime           = new DateTime($dateee);
	            $datetime->modify('+1 hours');
	            $limitdateee        = $datetime->format('Y-m-d H:i:s');

	            $datelimit          = date_create($limitdateee);
	            $dateelimit         = date_format($datelimit, 'd/m/y');
	            $harilimit          = date_format($datelimit, 'd');
	            $hari               = date_format($datelimit, 'l');
	            $tahunlimit         = date_format($datelimit, 'Y');
	            $waktulimit         = date_format($datelimit, 'H:s');
	                                                    
	            $datelimit          = $dateelimit;
	            $sepparatorlimit    = '/';
	            $partslimit         = explode($sepparatorlimit, $datelimit);
	            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

	            $getdatainvoice[$row]['tanggaltransaksi'] 	= $tanggalcreate;
	            $getdatainvoice[$row]['expireddate']	 	= $tanggaltransaksi;
                $getdatainvoice[$row]['order_detail'] 		= $get_orderdetail;
		        $getdatainvoice[$row]['hargakelas'] 		= $hargakelas;
		        $getdatainvoice[$row]['hargaprice'] 		= $hargaprice;
		        $getdatainvoice[$row]['payment_type'] 		= $payment_type;
            }
		}
		if (count($getdatainvoice) == 5) {
			# code...
			$res['nextpage'] = $pagination + 1;
			$res['lastpage'] = $pagination + 2;
		} else {

			$res['nextpage'] = $pagination + 1;
			$res['lastpage'] = $pagination;
		}
		$res['status'] = true;
		$res['message'] = 'Successfully';
		$res['data'] = $getdatainvoice;
		echo json_encode($res);
	}

	public function ajax_notif_backup()
	{
		$iduser = $this->input->get('id_user');
		$pagination = $this->input->get('page');
		$last = 5;
		$first = $pagination * 5;
		// LIMIT $first, $last
		$querynotif = $this->db->query("SELECT tbl_user.user_name, tbl_user.user_image, tbl_notif.* FROM tbl_user LEFT JOIN tbl_notif ON tbl_user.id_user=tbl_notif.id_user WHERE tbl_notif.id_user = '$iduser' ORDER BY timestamp DESC LIMIT $first, $last")->result_array();

		$new_arr = array();

		if (!empty($querynotif)) {
			foreach ($querynotif as $key => $value) {

				$database = $value['timestamp'];
				$waktusekarang = date('Y/m/d H:i:s');

				$init = strtotime($waktusekarang) - strtotime($database);
				$hours = floor($init / 3600);
				$minutes = floor(($init / 60) % 60);
				$seconds = $init % 60;
				// CASE 1 : JIKA INTERVAL KURANG DR 60 DETIK = JUST NOW
				// CASE 2 : JIKA INTERVAL KURANG DR 60 MENIT = SHOW MINUTES
				if($init < 60){
					$hasil = "baru saja";
				}
				else if ($init >= 60 && $init < 3600){
					$hasil = floor(($init / 60) % 60)." menit yang lalu";
				}
				else if($init >= 3600 && $init < 86400){
					$hasil = floor($init / 3600).' jam yang lalu';
				}			
				else if ($init >= 86400) {
				    $hasil = floor($hours / 24).' hari yang lalu';
				}				
				$querynotif[$key]['interval_time'] = $hasil;
			}

			if (count($querynotif) == 5) {
				# code...
				$new_arr['nextpage'] = $pagination + 1;
				$new_arr['lastpage'] = $pagination + 2;
			} else {

				$new_arr['nextpage'] = $pagination + 1;
				$new_arr['lastpage'] = $pagination;
			}

			$new_arr['status'] = true;
			$new_arr['data'] = $querynotif;
		}
		else{
			$new_arr['status'] = false;
			$new_arr['data'] = $querynotif;
			$new_arr['nextpage'] = $pagination + 1;
			$new_arr['lastpage'] = $pagination;
		}
		echo json_encode($new_arr);
	}

	public function ajax_notif()
	{
		$iduser = $this->input->get('id_user');
		$pagination = $this->input->get('page');
		$last = 5;
		$first = $pagination * 5;
		// LIMIT $first, $last
		$querynotif = $this->db->query("SELECT tbl_user.user_name, tbl_user.user_image, tbl_notif.* FROM tbl_user LEFT JOIN tbl_notif ON tbl_user.id_user=tbl_notif.id_user WHERE tbl_notif.id_user = '$iduser' ORDER BY timestamp DESC LIMIT $first, $last")->result_array();

		$new_arr = array();

		if (!empty($querynotif)) {
			foreach ($querynotif as $key => $value) {
				$new_mess = "";
				$jsonism = json_decode($value['notif'],true);
				switch($jsonism['code']){
					case 32: 
							$new_mess = $this->lang->line('student')." ".$jsonism['student_name']." ".$this->lang->line('ask_class')." ".$this->lang->line('subject_'.$jsonism['subject_id']);
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 33: 
							$new_mess = $this->lang->line('finishverifikasi');
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 34: 
							$new_mess = $this->lang->line('youremail3')." ".$this->lang->line('here')." ".$this->lang->line('emailresend');
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 40: 
							$new_mess = $this->lang->line('tutor')." ".$jsonism['tutor_name']." ".$this->lang->line('has_call_support');
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 41: 
							$new_mess = $this->lang->line('readysupport')." ".$this->lang->line('here')." ".$this->lang->line('starting');
							$querynotif[$key]['notif'] = $new_mess;
							break;	
					case 42: 
							$new_mess = $this->lang->line('tutorsuccessregis');
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 43: 
							$new_mess = $this->lang->line('successverifikasitutor');
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 44: 
							$new_mess = $this->lang->line('successsendapproval');
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 45: 
							$new_mess = $this->lang->line('heyadmin')." ".$jsonism['tutor_name'].", ".$this->lang->line("heyadmin2");
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 46: 
							$new_mess = $this->lang->line('tutornotcoming');
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 110: 
							$new_mess = $this->lang->line('notifpaymentstudent');
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 111: 
							$new_mess = $this->lang->line('heyadmin')." ".$jsonism['tutor_name'].", ".$this->lang->line("paymentmulticast");
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 114: 
							$new_mess = $this->lang->line('successfullydemand');
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 115: 
							$new_mess = $this->lang->line('faileddemand');
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 116: 
							$new_mess = $this->lang->line('waitingpaymentuser1')." ".$jsonism['namauser']." ".$this->lang->line('waitingpaymentuser2');
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 117: 
							$new_mess = $this->lang->line('heyadmin')." ".$jsonism['tutor_name'].", ".$this->lang->line("paymentprivate");
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 118: 
							$new_mess = $this->lang->line('heyadmin')." ".$jsonism['tutor_name'].", ".$this->lang->line("paymentgroup");
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 119: 
							$new_mess = $this->lang->line('alertrejectclass1')." ".$jsonism['tutor_name'].", ".$this->lang->line('alertrejectclass2');
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 212: 
							$new_mess = $this->lang->line('infosucces_groupuser');
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 213: 
							$new_mess = $this->lang->line('infosucces_grouptouser');
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 214: 
							$new_mess = $this->lang->line('infosucces_grouptotutor1')." ".$jsonism['nama_user']." - ".$jsonism['subject_name'].", ".$this->lang->line('infosucces_grouptotutor2')." ".$jsonism['starttime'];
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 215: 
							$new_mess = $this->lang->line('underpaymentnotif');
							$querynotif[$key]['notif'] = $new_mess;
							break;
					case 216: 
							$new_mess = $this->lang->line('alertnotif_complain')." ".$jsonism['student_name']." ".$this->lang->line('alertnotif_complain2')." ".$jsonism['class_name']. " ".$this->lang->line('alertnotif_complain3');
							$querynotif[$key]['notif'] = $new_mess;
							break;
				}


				$database = $value['timestamp'];
				$waktusekarang = date('Y/m/d H:i:s');

				$init = strtotime($waktusekarang) - strtotime($database);
				$hours = floor($init / 3600);
				$minutes = floor(($init / 60) % 60);
				$seconds = $init % 60;
				// CASE 1 : JIKA INTERVAL KURANG DR 60 DETIK = JUST NOW
				// CASE 2 : JIKA INTERVAL KURANG DR 60 MENIT = SHOW MINUTES
				if($init < 60){
					$hasil = "baru saja";
				}
				else if ($init >= 60 && $init < 3600){
					$hasil = floor(($init / 60) % 60)." menit yang lalu";
				}
				else if($init >= 3600 && $init < 86400){
					$hasil = floor($init / 3600).' jam yang lalu';
				}			
				else if ($init >= 86400) {
				    $hasil = floor($hours / 24).' hari yang lalu';
				}				
				$querynotif[$key]['interval_time'] = $hasil;
			}

			if (count($querynotif) == 5) {
				# code...
				$new_arr['nextpage'] = $pagination + 1;
				$new_arr['lastpage'] = $pagination + 2;
			} else {

				$new_arr['nextpage'] = $pagination + 1;
				$new_arr['lastpage'] = $pagination;
			}

			$new_arr['status'] = true;
			$new_arr['data'] = $querynotif;
		}
		else{
			$new_arr['status'] = false;
			$new_arr['data'] = $querynotif;
			$new_arr['nextpage'] = $pagination + 1;
			$new_arr['lastpage'] = $pagination;
		}
		echo json_encode($new_arr);
	}

	public function ajax_notifread()
	{
		$iduser = $this->input->get('id_user_requester');
		$updatenotiff = $this->db->simple_query("UPDATE tbl_notif SET read_status=1  WHERE id_user='{$iduser}'");
		$new_arr = array();
		if ($updatenotiff) {
			$new_arr['status'] = true;
			$new_arr['message'] = 'success'.$iduser;
		}
		else
		{
		$new_arr['status'] = false;
		$new_arr['message'] = 'failed';
		}

		echo json_encode($new_arr);
	}

	public function ajax_count()
	{
		$iduser = $this->input->get('id_user');
        $querycek = $this->db->query("SELECT count(*) as jumlah FROM tbl_notif WHERE id_user='$iduser' AND read_status='0'")->row_array();
        $new_arr = array();
        if ($querycek) {
        	$new_arr['status'] = true;
			$new_arr['data'] = $querycek['jumlah'];
        }
        else
		{
		$new_arr['status'] = false;		
		$new_arr['data'] = $querycek;
		}

		echo json_encode($new_arr);
	}

	public function ajax_notifreadone()
	{
		$iduser = $this->input->get('id_user');
		$notifid = $this->input->get('notif_id');
		$updatenotif = $this->db->simple_query("UPDATE tbl_notif SET read_status=1  WHERE id_user='$iduser' AND notif_id='$notifid'");
		$new_arr = array();
		if ($updatenotif) {
			$new_arr['status'] = true;
			$new_arr['message'] = 'success';
		}
		else
		{
		$new_arr['status'] = false;
		$new_arr['message'] = 'failed';
		}

		echo json_encode($new_arr);
	}

	public function haha($value='')
	{
		$date1=date_create("2013-03-15");
		$date2=date_create("2013-03-17");
		$diff=date_diff($date1,$date2);
		echo $diff->format('%a');
	}


	public function approve_demand_android($request_id='', $template='', $user_utc='', $tutor_id='')
	{
		$request_id = $this->input->get('request_id');
		$template 	= $this->input->get('template');
		$user_utc 	= $this->input->get('user_utc');
		$tutor_id =	$this->input->get('id_user');
		// $exist = $this->db->query("SELECT trq.*, uangsaku.us_id, uangsaku.active_balance FROM tbl_request as trq INNER JOIN uangsaku ON trq.id_user_requester=uangsaku.id_user WHERE trq.tutor_id='{$id_user}' AND trq.request_id='{$request_id}'")->row_array();

		// // $uangsaku = $this->db->query("SELECT * FROM uangsaku WHERE id_user=$id_user")->row_array();
		// if ($this->session_check() == 1) {
			$res = $this->Process_model->approve_demand($request_id,$template,$user_utc,$tutor_id);
			// IF 1 = DEMAND APPROVED
			// ELSE = DEMAND REJECTED
			
			if($res == 1){
				$getdataclass 	= $this->db->query("SELECT ms.subject_name, tr.*, tsp.*, tu.user_name, tu.email FROM tbl_request as tr INNER JOIN tbl_service_price as tsp ON tr.subject_id=tsp.subject_id INNER JOIN tbl_user as tu ON tu.id_user=tr.tutor_id INNER JOIN master_subject as ms ON ms.subject_id=tr.subject_id WHERE tr.request_id='$request_id' AND tsp.class_type='private' AND tsp.tutor_id='$tutor_id'")->row_array();

				$date_requested = DateTime::createFromFormat('Y-m-d H:i:s', $getdataclass['date_requested'] );
                $date_requested = $date_requested->format('Y-m-d H:i:s');

                $duration       = $getdataclass['duration_requested'];

                $fn_time = DateTime::createFromFormat('Y-m-d H:i:s', $date_requested);
                $fn_time->modify("+$duration second");
                $fn_time = $fn_time->format('Y-m-d H:i:s');

                
                // REJECT ALL PENDING REQUEST THAT HAS ARSIRAN TIME
                $to_reject = $this->db->query("SELECT * FROM tbl_request WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND approve=0 ")->result_array();

                foreach ($to_reject as $key => $value) {
                    $rq_id = $value['request_id'];
                    $this->db->simple_query("UPDATE tbl_request SET approve=-2 WHERE request_id='$rq_id'");
                    $this->Process_model->reject_request($rq_id, 'private', 'unavailable');
                }
                $to_reject_group = $this->db->query("SELECT * FROM tbl_request_grup WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND approve=0 ")->result_array();

                foreach ($to_reject_group as $key => $value) {
                    $rq_id = $value['request_id'];
                    $this->db->simple_query("UPDATE tbl_request_grup SET approve=-2 WHERE request_id='$rq_id'");
                    $this->Process_model->reject_request($rq_id, 'group', 'unavailable');
                }

				$iduserrequest 		= $getdataclass['id_user_requester'];
                $duration           = $getdataclass['duration_requested'];
				// $datauser 			= $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
				//CEK IF NOT PARENT 
	            $usertype_id 		= $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['usertype_id'];
                if ($usertype_id == 'student kid') {
                    $getidparent    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$iduserrequest'")->row_array()['parent_id'];
                    $forFcm_id_requester= $getidparent;
                    $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getidparent'")->row_array();
                }
                else
                {
                    $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
                    $forFcm_id_requester= $iduserrequest;
                }
                // if (empty($datauser['email'])) {
                //     $getparentid    = $this->db->query("SELECT * FROM tbl_profile_kid WHERE kid_id='$iduserrequest'")->row_array();
                //     $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getparentid[parent_id]'")->row_array();
                // }
                $hargahitung        = ($duration/900)*$getdataclass['harga_cm'];
				$hargakelas         = $hargahitung;
	            $server_utcc        = $this->Rumus->getGMTOffset();
	            $user_utc 			= $this->Rumus->getGMTOffset('minute',$user_utc);
	            $intervall          = $user_utc - $server_utcc;
	            $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$getdataclass['date_requested']);
	            $tanggal->modify("+".$intervall ." minutes");
	            $tanggal            = $tanggal->format('Y-m-d H:i:s');
	            $datelimit          = date_create($tanggal);
	            $dateelimit         = date_format($datelimit, 'd/m/y');
	            $harilimit          = date_format($datelimit, 'd');
	            $hari               = date_format($datelimit, 'l');
	            $tahunlimit         = date_format($datelimit, 'Y');
	            $waktulimit         = date_format($datelimit, 'H:s');   
	            $datelimit          = $dateelimit;
	            $sepparatorlimit    = '/';
	            $partslimit         = explode($sepparatorlimit, $datelimit);
	            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
	            $seconds 			= $getdataclass['duration_requested'];
			    $hours 				= floor($seconds / 3600);
			    $mins 				= floor($seconds / 60 % 60);
			    $secs 				= floor($seconds % 60);
			    $durationrequested 	= sprintf('%02d:%02d', $hours, $mins);

                //LOG DEMAND
                $myip               = $_SERVER['REMOTE_ADDR'];
                $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$request_id','tutor approve private class','$iduserrequest','private','$myip','111')");
                //END LOG DEMAND

                 ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
	            $json_notif = "";
	            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$forFcm_id_requester}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
	            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
	                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "30","title" => "Classmiles", "text" => "")));
	            }
	            if($json_notif != ""){
	                $headers = array(
	                    'Authorization: key=' . FIREBASE_API_KEY,
	                    'Content-Type: application/json'
	                    );
	                // Open connection
	                $ch = curl_init();

	                // Set the url, number of POST vars, POST data
	                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
	                curl_setopt($ch, CURLOPT_POST, true);
	                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

	                // Execute post
	                $result = curl_exec($ch);
	                curl_close($ch);
	                // echo $result;
	            }
	            ////------------ END -------------------------------------//

                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
                curl_setopt($ch, CURLOPT_POST, true);
                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_approve_demand','content' => array("datauser" => $datauser, "hargakelas" => $hargakelas, "getdataclass" => $getdataclass, "hari" => $hari, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "durationrequested" => $durationrequested) )));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;

				if ($result) {

					//MASUK KE INVOICE
                    $iptInvoice = $this->Process_model->createInvoice($request_id,'private');

                    if ($iptInvoice == 1) {   
	                    $getdataadmin  = $this->db->query("SELECT * FROM tbl_user WHERE usertype_id='admin'")->result_array();
	                    foreach ($getdataadmin as $key => $v) {
	                        $idadmin = $v['id_user'];
	                        $namaadmin = $v['user_name'];
	                        $notif_message = json_encode( array("code" => 117, "tutor_name" => $namaadmin) );                        
	                        $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$idadmin','https://classmiles.com/admin/PaymentPrivate','0')");
	                    }  
	                    $load = 'sukses '.$myip;
						$rets = array('status' => true,'message' => $load);
						echo json_encode($rets);
					} else{
                        
	                    $load = 'gagal Kirim INVOICE';
						$rets = array('status' => false,'message' => $load);
						echo json_encode($rets);
                        
                    }    
				}
                else
                {
                    $load = 'gagal Kirim Email';
					$rets = array('status' => false,'message' => $load);
					echo json_encode($rets);
                }
			}else{
				$load = 'approvalfailed';
				$rets = array('status' => false,'message' => $load);
				echo json_encode($rets);
			}
		// }

		// if(!empty($exist)){
			
		// 	$id_user_requester = $exist['id_user_requester'];
		// 	$balance = $exist['active_balance'];
		// 	$uangsaku_id = $exist['us_id'];
		// 	$subject_id = $exist['subject_id'];
		// 	$price = $this->db->query("SELECT * FROM tbl_service_price WHERE subject_id={$subject_id} AND tutor_id={$id_user} AND class_type='private'")->row_array()['harga'];
		// 	if($balance >= $price){

		// 		// PROCESSING DEMAND TOTAL
		// 		$date_requested = $exist['date_requested'];
		// 		// $time_requested = $exist['time_requested'];
		// 		$duration = $exist['duration_requested'];
		// 		$topic = $exist['topic'];
		// 		$id_user_requester = $exist['id_user_requester'];
				
		// 		$subject_name = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id={$subject_id}")->row_array()['subject_name'];

		// 		$server_utc = $this->Rumus->getGMTOffset();
		// 		$user_utc = $this->Rumus->getGMTOffset('minute',$user_utc);
		// 		$interval   = $user_utc - $server_utc;

		// 		// CREATE NEW CLASS
		// 		$final_st = $date_requested;
		// 		$final_ft = DateTime::createFromFormat('Y-m-d H:i:s', $final_st);
		// 		$final_ft->modify("+$duration second");
		// 		$final_ft = $final_ft->format('Y-m-d H:i:s');

		// 		$participant['visible'] = "private";
		// 		$participant['participant'] = array();
		// 		$participant['participant'][]['id_user'] = $id_user_requester;

		// 		$participant = json_encode($participant);


		// 		$this->db->simple_query("INSERT INTO tbl_class(description, subject_id,name,tutor_id,start_time,finish_time,participant,class_type,template_type) VALUES('{$topic}','{$subject_id}','{$subject_name}','{$id_user}','{$final_st}','{$final_ft}','{$participant}','private','{$template}')");

		// 		$class_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];
		// 		$new_balance = $balance-$price;
		// 		$this->db->simple_query("UPDATE uangsaku SET active_balance=$new_balance WHERE us_id='$uangsaku_id'");
		// 		$trx_id = $this->Rumus->getTrxID();
		// 		$this->db->simple_query("INSERT INTO log_class_transaction VALUES('$trx_id','$id_user_requester','$class_id','$balance','$price','{$new_balance}',now())");

		// 		$this->db->simple_query("UPDATE tbl_request SET approve=1 WHERE request_id='{$request_id}'");
		// 		$simpan_notif = $this->Rumus->create_notif($notif='Permintaan anda telah di setujui oleh tutor. Kelas akan dimulai pada '.$date_requested.'.',$notif_mobile='Permintaan anda telah di setujui oleh tutor. Kelas akan dimulai pada '.$date_requested.'.',$notif_type='single',$id_user=$id_user_requester,$link='https://classmiles.com/first/');
		// 		$load = 'sukses';
		// 		$rets = array('status' => true,'message' => $load);
		// 		echo json_encode($rets);
		// 		return 1;
		// 	}
		// 	$load = 'failed';
		// 	$rets = array('status' => false,'message' => $load);
		// 	echo json_encode($rets);
		// }
	}

	public function approve_demand_group_android($request_id='', $template='', $user_utc='')
	{
		$id_user 	= $this->input->get('id_user');
		$request_id = $this->input->get('request_id');
		$template 	= $this->input->get('template');
		$user_utc 	= $this->input->get('user_utc');
		$tutorid 	= $this->input->get('id_user');
			$res = $this->Process_model->approve_demand_group($request_id,$template,$user_utc, $tutorid);
			
			if($res == 1){
                $getdataclass   = $this->db->query("SELECT ms.subject_name, trg.*, tsp.*, tu.user_name, tu.email FROM tbl_request_grup as trg INNER JOIN tbl_service_price as tsp ON trg.subject_id=tsp.subject_id INNER JOIN tbl_user as tu ON tu.id_user=trg.tutor_id INNER JOIN master_subject as ms ON ms.subject_id=trg.subject_id WHERE trg.request_id='$request_id' AND tsp.class_type='group' AND tsp.tutor_id='$tutorid'")->row_array();

                $date_requested = DateTime::createFromFormat('Y-m-d H:i:s', $getdataclass['date_requested'] );
                $date_requested = $date_requested->format('Y-m-d H:i:s');

                $duration       = $getdataclass['duration_requested'];

                $fn_time = DateTime::createFromFormat('Y-m-d H:i:s', $date_requested);
                $fn_time->modify("+$duration second");
                $fn_time = $fn_time->format('Y-m-d H:i:s');

                
                // REJECT ALL PENDING REQUEST THAT HAS ARSIRAN TIME
                $to_reject = $this->db->query("SELECT * FROM tbl_request WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND approve=0 ")->result_array();

                foreach ($to_reject as $key => $value) {
                    $rq_id = $value['request_id'];
                    $this->db->simple_query("UPDATE tbl_request SET approve=-2 WHERE request_id='$rq_id'");
                    $this->Process_model->reject_request($rq_id, 'private', 'unavailable');
                }
                $to_reject_group = $this->db->query("SELECT * FROM tbl_request_grup WHERE ( ( '$date_requested' <= date_requested AND '$fn_time' > date_requested AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) OR ( '$date_requested' >= date_requested AND '$date_requested' < DATE_ADD(date_requested, INTERVAL duration_requested SECOND) AND ( '$fn_time' <= DATE_ADD(date_requested, INTERVAL duration_requested SECOND) OR '$fn_time' > DATE_ADD(date_requested, INTERVAL duration_requested SECOND) )  ) ) AND approve=0 ")->result_array();

                foreach ($to_reject_group as $key => $value) {
                    $rq_id = $value['request_id'];
                    $this->db->simple_query("UPDATE tbl_request_grup SET approve=-2 WHERE request_id='$rq_id'");
                    $this->Process_model->reject_request($rq_id, 'group', 'unavailable');
                }

                
                $iduserrequest      = $getdataclass['id_user_requester'];
                // $datauser           = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
                $usertype_id 		= $this->db->query("SELECT usertype_id FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['usertype_id'];
                if ($usertype_id == 'student kid') {
                    $getidparent    = $this->db->query("SELECT parent_id FROM tbl_profile_kid WHERE kid_id = '$iduserrequest'")->row_array()['parent_id'];
                    $forFcm_id_requester= $getidparent;
                    $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getidparent'")->row_array();
                }
                else
                {
                    $datauser       = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$iduserrequest'")->row_array();
                    $forFcm_id_requester= $iduserrequest;
                }
	            
                $hargakelas         = number_format($getdataclass['harga_kelas'], 0, ".", ".");
                $server_utcc        = $this->Rumus->getGMTOffset();
                $intervall          = $user_utc - $server_utcc;
                $tanggal            = DateTime::createFromFormat ('Y-m-d H:i:s',$getdataclass['date_requested']);
                $tanggal->modify("+".$intervall ." minutes");
                $tanggal            = $tanggal->format('Y-m-d H:i:s');
                $datelimit          = date_create($tanggal);
                $dateelimit         = date_format($datelimit, 'd/m/y');
                $harilimit          = date_format($datelimit, 'd');
                $hari               = date_format($datelimit, 'l');
                $tahunlimit         = date_format($datelimit, 'Y');
                $waktulimit         = date_format($datelimit, 'H:s');   
                $datelimit          = $dateelimit;
                $sepparatorlimit    = '/';
                $partslimit         = explode($sepparatorlimit, $datelimit);
                $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));
                $seconds            = $getdataclass['duration_requested'];
                $hours              = floor($seconds / 3600);
                $mins               = floor($seconds / 60 % 60);
                $secs               = floor($seconds % 60);
                $durationrequested  = sprintf('%02d:%02d', $hours, $mins);

                //LOG DEMAND
                $myip               = $_SERVER['REMOTE_ADDR'];
                $this->db->query("INSERT INTO log_demand (request_id,ket,id_user,type,ip_from,code_status) VALUES ('$request_id','tutor approve group class','$tutorid','group','$myip','112')");
                //END LOG DEMAND

                 ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
	            $json_notif = "";
	            $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$forFcm_id_requester}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
	            if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
	                $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "30","title" => "Classmiles", "text" => "")));
	            }
	            if($json_notif != ""){
	                $headers = array(
	                    'Authorization: key=' . FIREBASE_API_KEY,
	                    'Content-Type: application/json'
	                    );
	                // Open connection
	                $ch = curl_init();

	                // Set the url, number of POST vars, POST data
	                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
	                curl_setopt($ch, CURLOPT_POST, true);
	                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

	                // Execute post
	                $result = curl_exec($ch);
	                curl_close($ch);
	                // echo $result;
	            }
	            ////------------ END -------------------------------------//
                // Open connection
                $ch = curl_init();

                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, 'https://meetaza.com/katana/tools/receiver_Email');
                curl_setopt($ch, CURLOPT_POST, true);
                // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('template_name' => 'email_approve_demand_group','content' => array("datauser" => $datauser, "getdataclass" => $getdataclass, "hari" => $hari, "harilimit" => $harilimit, "bulanlimit" => $bulanlimit, "tahunlimit" => $tahunlimit, "waktulimit" => $waktulimit, "durationrequested" => $durationrequested, "hargakelas" => $hargakelas ) )));

                // Execute post
                $result = curl_exec($ch);
                curl_close($ch);
                // echo $result;


              	if ($result) {
              		$iptInvoice = $this->Process_model->createInvoice($request_id,'group');

                    if ($iptInvoice == 1) {
	    				$load = $this->lang->line('approvalcomplete');
	    				$json = array("status" => true,"message" => $load);

						echo json_encode($json);
	                    //NOTIF TUTOR
	                    $notif_message = json_encode( array("code" => 116, "namauser" => $datauser['user_name']));                    
	                    $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$tutorid','','0')");

	                    $getdataadmin  = $this->db->query("SELECT * FROM tbl_user WHERE usertype_id='admin'")->result_array();
	                    foreach ($getdataadmin as $key => $v) {
	                        $idadmin = $v['id_user'];
	                        $namaadmin = $v['user_name'];
	                        $notif_message = json_encode( array("code" => 118, "tutor_name" => $namaadmin) );                        
	                        $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$idadmin','https://classmiles.com/admin/PaymentGroup','0')");
	                    }
                    } else {
	                    $load = $this->lang->line('approvalfailed');
	                    $json = array("status" => false,"message" => $load);

						echo json_encode($json);

                    }             
                }
                else
                {
                    $load = $this->lang->line('approvalfailed');
                    $json = array("status" => false,"message" => $load);

					echo json_encode($json);
                }
			}else{
				$load = $this->lang->line('approvalfailed');
				$json = array("status" => false,"message" => $load);

				echo json_encode($json);
			}


		// $exist = $this->db->query("SELECT * FROM tbl_request_grup WHERE tutor_id='{$id_user}' AND request_id='{$request_id}'")->row_array();


		// if(!empty($exist)){
		// 	$subject_id = $exist['subject_id'];
		// 	$id_user_requester = $exist['id_user_requester'];
		// 	$price = $exist['harga_kelas'];
		// 	$topic = $exist['topic'];
		// 	// PROCESSING DEMAND TOTAL
		// 	$enough = true;
  //           $list_student = json_decode($exist['id_friends'],true);
		// 	if($exist['method_pembayaran'] == 'bagi_rata'){
		// 		$total_student = count($list_student['id_friends']);
		// 		foreach ($list_student['id_friends'] as $key => $value) {
		// 			$ids = $value['id_user'];
		// 			$data_uangsaku = $this->db->query("SELECT * FROM uangsaku WHERE id_user='$ids'")->row_array();
		// 			if(empty($data_uangsaku))
		// 				return -1;

		// 			$list_student['id_friends'][$key]['balance'] = $data_uangsaku['active_balance'];
		// 			$list_student['id_friends'][$key]['uangsaku_id'] = $data_uangsaku['us_id'];
		// 			if($data_uangsaku['active_balance'] < $price)
		// 				$enough = false;
		// 		}
		// 		/*$data_uangsaku = $this->db->query("SELECT * FROM uangsaku WHERE id_user='$id_user_requester'")->row_array();
		// 		if(!empty($data_uangsaku)){
		// 			$balance = $data_uangsaku['active_balance'];
		// 			$uangsaku_id = $data_uangsaku['us_id'];
		// 			if($balance < ($price/$total_student))
		// 				$enough = false;
		// 		}*/
		// 	}else{
		// 		$data_uangsaku = $this->db->query("SELECT * FROM uangsaku WHERE id_user='$id_user_requester'")->row_array();
		// 		$balance = 0;
		// 		if(empty($data_uangsaku))
		// 				return -1;
		// 		$balance = $data_uangsaku['active_balance'];
		// 		$uangsaku_id = $data_uangsaku['us_id'];
		// 		if($balance < $price ){
		// 			$enough = false;
		// 		}
		// 	}
			
		// 	if($enough){
		// 		$date_requested = $exist['date_requested'];
		// 		$duration = $exist['duration_requested'];
		// 		$id_user_requester = $exist['id_user_requester'];
				
		// 		$subject_name = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id={$subject_id}")->row_array()['subject_name'];

		// 		$server_utc = $this->Rumus->getGMTOffset();
		// 		$interval   = $user_utc - $server_utc;

		// 		// CREATE NEW CLASS
		// 		$final_st = $date_requested;
		// 		$final_ft = DateTime::createFromFormat('Y-m-d H:i:s', $final_st);
		// 		$final_ft->modify("+$duration second");
		// 		$final_ft = $final_ft->format('Y-m-d H:i:s');

		// 		/*$final_stjam = sprintf('%02d',$time_requested/3600);
		// 		$final_stmenit = sprintf('%02d',($time_requested%3600/60));
		// 		$final_st = $date." ".$final_stjam.":".$final_stmenit.":00";

		// 		$final_ftjam = sprintf('%02d',$duration/3600);
		// 		$final_ftmenit = sprintf('%02d',($duration%3600/60));
		// 		$final_ft = $date." ".$final_ftjam.":".$final_ftmenit.":00";*/
				

		// 		$participant['visible'] = "private";

		// 		$participant['participant'] = array();
		// 			// print_r($asd);

		// 		foreach($list_student['id_friends'] as $p) {
					
		// 			$participant['participant'][]['id_user'] = $p['id_user'];
		// 			// $participant['participant'][]['id_user'] = $id_user_requester;
		// 		}
				
		// 		$participant = json_encode($participant);


		// 		// $this->db->simple_query("INSERT INTO tbl_class(subject_id,name,tutor_id,start_time,finish_time,participant) VALUES('{$subject_id}','{$subject_name}','{$id_user}','{$final_st}','{$final_ft}','{$participant}')");
		// 		$this->db->simple_query("INSERT INTO tbl_class(description, subject_id,name,tutor_id,start_time,finish_time,participant,class_type,template_type) VALUES('{$topic}','{$subject_id}','{$subject_name}','{$id_user}','{$final_st}','{$final_ft}','{$participant}','group','{$template}')");

		// 		$class_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];
		// 		if($exist['method_pembayaran'] == 'bagi_rata'){
		// 			$total_student = count($list_student['id_friends']);
		// 			$new_price = $price;
		// 			foreach ($list_student['id_friends'] as $key => $value) {
		// 				$usid = $value['uangsaku_id'];
		// 				$ids = $value['id_user'];
		// 				$last_balance = $value['balance'];
		// 				$new_balance = $last_balance-$new_price;
		// 				$this->db->simple_query("UPDATE uangsaku SET active_balance=$new_balance WHERE us_id='$usid'");
                        
                        
		// 				$trx_id = $this->Rumus->getTrxID();
		// 				$this->db->simple_query("INSERT INTO log_class_transaction VALUES('$trx_id','$ids','$class_id','$last_balance','$new_price','{$new_balance}',now())");
		// 			}
                    
  //                   $fee                    = ($new_price*$total_student)*10/100 >= 5000 ? ($new_price*$total_student)*10/100 : 5000;
  //                   $new_price              = ($new_price*$total_student)-$fee;
  //                   $balance_tutor          = $this->db->query("SELECT * FROM uangsaku WHERE id_user=$id_user")->row_array();
  //                   $balancetutor_active    = $balance_tutor['active_balance'];
  //                   $us_idtutor             = $balance_tutor['us_id'];
  //                   $new_balancetutor       = $balancetutor_active+$new_price;
  //                   $this->db->simple_query("UPDATE uangsaku SET active_balance=$new_balancetutor WHERE us_id='$us_idtutor'");
                        
		// 		}else{
		// 			$new_balance = $balance-$price;
		// 			$this->db->simple_query("UPDATE uangsaku SET active_balance=$new_balance WHERE us_id='$uangsaku_id'");
                    
  //                   $fee1                    = $price*10/100 >= 5000 ? $price*10/100 : 5000;
  //                   $price-=$fee1;
  //                   $balance_tutor1          = $this->db->query("SELECT * FROM uangsaku WHERE id_user=$id_user")->row_array();
  //                   $balancetutor_active1    = $balance_tutor1['active_balance'];
  //                   $us_idtutor1             = $balance_tutor1['us_id'];
  //                   $new_balancetutor1       = $balancetutor_active1+$price;
  //                   $this->db->simple_query("UPDATE uangsaku SET active_balance=$new_balancetutor1 WHERE us_id='$us_idtutor1'");
                    
		// 			$trx_id = $this->Rumus->getTrxID();
		// 			$this->db->simple_query("INSERT INTO log_class_transaction VALUES('$trx_id','$id_user_requester','$class_id','$balance','$price','{$new_balance}',now())");
		// 		}
				

		// 		$this->db->simple_query("UPDATE tbl_request_grup SET approve=1 WHERE request_id='{$request_id}'");
		// 		$simpan_notif = $this->Rumus->create_notif($notif='Permintaan anda telah di setujui oleh tutor. Kelas akan dimulai pada '.$date_requested.'.',$notif_mobile='Permintaan anda telah di setujui oleh tutor. Kelas akan dimulai pada '.$date_requested.'.',$notif_type='single',$id_user=$id_user_requester,$link='https://classmiles.com/first/');
				
		// 		$load = 'sukses';
		// 		$rets = array('status' => true,'message' => $load);
		// 		echo json_encode($rets);
		// 	}
		// 	$load = 'failed';
		// 	$rets = array('status' => false,'message' => $load);
		// 	echo json_encode($rets);
			
		// }
		
		// $load = 'failed';
		// $rets = array('status' => false,'message' => $load);
		// echo json_encode($rets);
	}
	
	public function ajax_lavtimeMe($value='')
	{
		$tutor_id = $this->session->userdata('id_user');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');

		$data = array();
		if(!$tutor_id == ''){
			$data = $this->Rumus->lavtimeMe($tutor_id,$start_date,$end_date);
		}
		
		$res['status'] = true;
		$res['message'] = 'successful';
		$res['data'] = $data;
		echo json_encode($res);
	}

	public function get_order_id()
	{
		$tokenid = $this->Rumus->gen_order_id();
    	if (empty($tokenid))
    	{
    		$res['status'] = false;
			$res['message'] = 'failed';
			$res['data'] = $data;
			echo json_encode($res);
    	}
    	$res['status'] = true;
		$res['message'] = 'successful';
		$res['data'] = $tokenid;
		echo json_encode($res);
	}

	public function loaddata()
	{	
		$arr = array();

		//LOAD CSS
		$a = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/animate.css/animate.min.css";
		$b = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css";	
		$c = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css";
		$d = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css";
		$e = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css";
		$f = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css";
		$g = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/chosen/chosen.min.css";
		$h = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css";
		$i = "https://beta.dev.meetaza.com/classmiles/aset/css/jquery.dataTables.css";
		$j = "https://beta.dev.meetaza.com/classmiles/aset/css/app.min.1.css";
		$k = "https://beta.dev.meetaza.com/classmiles/aset/css/aset/css/app.min.2.css";
		$l = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bootgrid/jquery.bootgrid.min.cs";
		$m = "https://beta.dev.meetaza.com/classmiles/aset/sel2/css/select2.css";
		$n = "https://beta.dev.meetaza.com/classmiles/aset/css/custom.css";
		$o = "https://beta.dev.meetaza.com/classmiles/aset/css/css.css";
		$p = "https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js";
		$q = "https://beta.dev.meetaza.com/classmiles/aset/js/anjeondemand.js";
		$r = "https://beta.dev.meetaza.com/classmiles/aset/js/ng-infinite-scroll.min.js";
		$s = "https://beta.dev.meetaza.com/classmiles/aset/dhtmlxscheduler/dhtmlxscheduler_flat.css";

		//LOAD JS
		$t = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/jquery/dist/jquery.min.js";
		$u = "https://beta.dev.meetaza.com/classmiles/aset/js/jquery-ui.min.js";
		$v = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/bootstrap/dist/js/bootstrap.min.jss";
		$w = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js";
		$x = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js";
		$y = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js";
		$z = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/moment/min/moment.min.js";
		$aa = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js";
		$ab = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/chosen/chosen.jquery.min.js";
		$ac = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js";
		$ad = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bootgrid/jquery.bootgrid.updated.min.js";
		$ae = "https://beta.dev.meetaza.com/classmiles/aset/sel2/js/select2.js";
		$af = "https://beta.dev.meetaza.com/classmiles/aset/js/jquery.dataTables.min.js";
		$ag = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bootstrap-wizard/jquery.bootstrap.wizard.min.js";
		$ah = "https://beta.dev.meetaza.com/classmiles/aset/js/functions.js";

		$ah = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bower_components/Waves/dist/waves.min.js";
		$ai = "https://beta.dev.meetaza.com/classmiles/aset/vendors/fileinput/fileinput.min.js";
		$aj = "https://beta.dev.meetaza.com/classmiles/aset/vendors/input-mask/input-mask.min.js";
		$ak = "https://beta.dev.meetaza.com/classmiles/aset/js/scripts.js";
		$al = "https://beta.dev.meetaza.com/classmiles/aset/js/jquery.smartWizard.js";		
		$am = "https://beta.dev.meetaza.com/classmiles/aset/js/jquery.maskMoney.min.js";
		$an = "https://beta.dev.meetaza.com/classmiles/aset/camerajs/webcam.js";
		$ao = "https://apis.google.com/js/platform.js?onload=onLoad";
		$ap = "https://apis.google.com/js/api:client.js";
		$aq = "https://beta.dev.meetaza.com/classmiles/aset/vendors/bootstrap-growl/bootstrap-growl.min.js";
		$ar = "https://beta.dev.meetaza.com/classmiles/aset/FileAPI/FileAPI.min.js";
		$as = "https://beta.dev.meetaza.com/classmiles/aset/FileAPI/FileAPI.exif.js";
		$at = "https://beta.dev.meetaza.com/classmiles/aset/jquery.fileapi.js";
		$au = "https://beta.dev.meetaza.com/classmiles/aset/js/bootstrap-filestyle.min.js";
		$av = "https://beta.dev.meetaza.com/classmiles/aset/js/custom.min.js";
		$arr = array(
		    array("load" => $a),array("load" => $b),array("load" => $c),array("load" => $d),array("load" => $e),array("load" => $f),array("load" => $g),array("load" => $h),array("load" => $i),array("load" => $j),array("load" => $k),array("load" => $l),array("load" => $m),array("load" => $n),array("load" => $o),array("load" => $p),array("load" => $q),array("load" => $r),array("load" => $s),array("load" => $t)	,array("load" => $u),array("load" => $v),array("load" => $w),array("load" => $x),array("load" => $y),array("load" => $z),array("load" => $aa),array("load" => $ab),array("load" => $ac),array("load" => $ad),array("load" => $ae),array("load" => $af),array("load" => $ag),array("load" => $ah),array("load" => $ai),array("load" => $aj),array("load" => $ak),array("load" => $al),array("load" => $am),array("load" => $an),array("load" => $ao),array("load" => $ap),array("load" => $aq),array("load" => $ar),array("load" => $as),array("load" => $at),array("load" => $au),array("load" => $av)			    
		);
			
		echo json_encode($arr);		
	}

	public function my_ended_class($id_user='')
	{
		$iduser = $this->input->post('id_user');
        $at_least_one = 0;
        $now = date('Y-m-d H:i:s');
        $query_sche = $this->db->query("SELECT tc.class_id, tc.participant, tc.subject_id, tc.name, tc.description, tc.tutor_id, tu.first_name, tu.user_image, tc.start_time, tc.finish_time FROM `tbl_class` as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user where finish_time < '$now' && break_pos=0 && break_state = 0 ORDER BY finish_time ASC")->result_array();
        $all_schedule = array();

        foreach ($query_sche as $key => $value) {
            $participant = json_decode($value['participant'],true);
            $in = 0;
            if($participant['visible'] == "followers"){
                $tutor_id = $value['tutor_id'];
                $subject_id = $value['subject_id'];
                $real = $this->db->query("SELECT * FROM tbl_booking WHERE tutor_id='{$tutor_id}' AND id_user='$iduser' AND subject_id='{$subject_id}'")->row_array();
                if(!empty($real)){
                    $in = 1;
                }

            }
            else if($participant['visible'] == "all"){
                $in = 1;
            }
            else if($participant['visible'] == "private"){
                foreach ($participant['participant'] as $p => $vp) {
                    if($vp['id_user'] == $id_user){
                        $in = 1;
                    }
                }
            }
            if($in == 1){
            	$all_schedule[] = $value;
            }
            
        }
        // return $all_schedule;
        $res['status'] = true;
		$res['message'] = 'successful';
		$res['data'] = $all_schedule;
        echo json_encode($res);
	}

	function unsavebooking(){
		$id_user = $this->input->post('id_user');
		$tutor_id = $this->input->post('tutor_id');
		$subject_id = $this->input->post('subject_id');
		$subject_name = $this->input->post('subject_name');

		$cekdlu = $this->db->query("SELECT * FROM tbl_booking WHERE id_user='{$id_user}' AND subject_id='{$subject_id}' AND tutor_id='{$tutor_id}'")->result_array();
		// $getname = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array();
		if (empty($cekdlu)) {			
			$ress['status'] = true;
			$ress['message'] = "Failed Unfollow";			
	        echo json_encode($ress);
	        return 0;
		}
		else
		{
			$res = $this->db->query("DELETE FROM tbl_booking WHERE id_user='{$id_user}' AND subject_id='{$subject_id}' AND tutor_id='{$tutor_id}'");						
	        $ress['status'] = true;
			$ress['message'] = "Success Unfollow";			
	        echo json_encode($ress);
	        return 1;
		}
		
	}

	function set_code(){
		$list_id =$this->input->post('list_id');
		$class_id = $this->input->post('class_id');
		$this->session->set_userdata('kode_cek',$list_id);
		$this->session->set_userdata('stat_showdetail',$class_id);
		$this->session->set_userdata('code','800');
		$ress['status'] = true;
		$ress['message'] = "Success";			
        echo json_encode($ress);	
	    return 1;
	}

	function unset_codedesc(){		
		$this->session->set_userdata('stat_showdetail',"");		
		$this->session->set_userdata('kode_cek',"");		
		$this->session->set_userdata('ubah_pass',"");	
		$ress['status'] = true;
		$ress['message'] = "Success";			
        echo json_encode($ress);	
	    return 1;
	}
	
}