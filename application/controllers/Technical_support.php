<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Technical_support extends CI_Controller {

	public $sesi = array("lang" => "indonesia");	


	public function index($page = 0)
	{
		if ($this->session_check() == 1) {	
			if($this->session->flashdata('rets')){
				$data['rets'] = $this->session->flashdata('rets');
			}
			$data['sideactive'] = "home";
			$this->load->view('inc/header');
			$this->load->view('technical_support/index', $data);
			// $this->load->view('technical_support/checker');
			return null;
		}
		redirect('/');
	}
	public function check_janus ()
	{
		if ($this->session_check() == 1) {		
			$this->load->view('inc/header');
			$this->load->view('technical_support/index', $data);
		}
		else
		{
			redirect('/');
		}
	}
	public function CheckStatus()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "check_status";
			$this->load->view('inc/header');
			$this->load->view('technical_support/status', $data);
		}
		else
		{
			redirect('/login');
		}
	}

	public function session_check()
	{
		if($this->session->has_userdata('id_user')){
			$new_state = $this->db->query("SELECT status FROM tbl_user where id_user='".$this->session->userdata('id_user')."'")->row_array()['status'];
			if($this->session->userdata('status') != $new_state ){
				$this->session->sess_destroy();
				redirect('/login');
			}
			// $this->sesi['user_name'] = $this->session->userdata('user_name');
			// $this->sesi['username'] = $this->session->userdata('username');
			// $this->sesi['priv_level'] = $this->session->userdata('priv_level');
			if($this->session->userdata('usertype_id') != "technical_support"){
				redirect('/');
			}
			return 1;
		}else{
			return 0;
		}
	}
	

}
