<?php

/**
* 
*/
class Oauth_login extends CI_Controller
{
		public $user = "";

	public function __construct() {
		parent::__construct();

	 	$this->load->library('Mobile_Detect');
        $detect = new Mobile_Detect();
		// Check for any mobile device.
		if ($detect->isMobile()){
			define('MOBILE', 'mobile/');
		}else{
			define('MOBILE', '');
		}

// Load facebook library and pass associative array which contains appId and secret key
		$this->load->library('facebook');

// Get user's login information
		$this->user = $this->facebook->getUser();
	}

// Store user information and send to profile page
	public function index() {
		if ($this->user) {
			$data['user_profile'] = $this->facebook->api('/me/');

// Get logout url of facebook
			$data['logout_url'] = $this->facebook->getLogoutUrl(array('next' => base_url() . '/oauth_login/logout'));

// Send data to profile page
			$this->load->view(MOBILE.'profile', $data);
		} else {

// Store users facebook login url
			$data['login_url'] = $this->facebook->getLoginUrl();
			$this->load->view(MOBILE.'loginface', $data);
		}
	}

// Logout from facebook
	public function logout() {

// Destroy session
		session_destroy();

// Redirect to baseurl
		redirect(base_url());
	}
}
?>