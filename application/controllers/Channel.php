<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Channel extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// $this->load->library('fpdf');
        $this->load->helper(array('Form', 'Cookie', 'String'));
		// Check for any mobile device.
	}

	public function index()
	{
		if($this->session_check() == 1){
			$data['sideactive'] = 'Home';
			$this->load->view('inc/header_adm');
			$this->load->view('adminchannel/index',$data);
			$this->load->view('inc/footer_adm');	
		}
		else{
			redirect('/');
		}		
	}	
	public function Purchase()
	{
		$data['sideactive'] = 'Purchase';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/purchase',$data);
		$this->load->view('inc/footer_adm');
	}

	public function TutorList()
	{
		$data['sideactive'] = 'Tutor';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/list_tutor',$data);
		$this->load->view('inc/footer_adm');
	}

	public function StudentList()
	{
		$data['sideactive'] = 'Student';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/list_student',$data);
		$this->load->view('inc/footer_adm');
	}

	public function StudentListJoin()
	{
		$data['sideactive'] = 'StudentJoin';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/list_joinstudent',$data);
		$this->load->view('inc/footer_adm');
	}

	public function ListClass()
	{
		$data['sideactive'] = 'Listclass';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/list_class',$data);
		$this->load->view('inc/footer_adm');
	}

	public function ListProgram()
	{
		$data['sideactive'] = 'Listprogram';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/list_program',$data);
		$this->load->view('inc/footer_adm');
	}

	public function addProgram()
	{
		$data['sideactive'] = 'Listprogram';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/add_program',$data);
		$this->load->view('inc/footer_adm');
	}

	public function editProgram()
	{
		$data['sideactive'] = 'EditProgram';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/edit_program',$data);
		$this->load->view('inc/footer_adm');
	}

	public function modifyClass()
	{
		$data['sideactive'] = 'Modifyclass';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/modify_class',$data);
		$this->load->view('inc/footer_adm');
	}

	public function Group()
	{
		$data['sideactive'] = 'Group';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/list_group',$data);
		$this->load->view('inc/footer_adm');
	}

	public function EditGroup()
	{
		$data['sideactive'] = 'Group';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/edit_group',$data);
		$this->load->view('inc/footer_adm');
	}

	public function Program()
	{
		$data['sideactive'] = 'Program';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/program',$data);
		$this->load->view('inc/footer_adm');
	}

	public function ReportPoint()
	{
		$data['sideactive'] = 'ReportPoint';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/report_point',$data);
		$this->load->view('inc/footer_adm');
	}
	public function ReportPayment()
	{
		$data['sideactive'] = 'ReportPayment';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/report_payment',$data);
		$this->load->view('inc/footer_adm');
	}

	public function Settings()
	{
		$data['sideactive'] = 'Setting';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/settings',$data);
		$this->load->view('inc/footer_adm');
	}
	public function SettingPackage()
	{
		$data['sideactive'] = 'SettingPackage';
		$this->load->view('inc/header_adm');
		$this->load->view('adminchannel/setting_package',$data);
		$this->load->view('inc/footer_adm');
	}
	
	public function set_package()
	{
		$name_package 	= $this->input->post('name_package');
		$credit_package = $this->input->post('credit_package');
		$price_package 	= $this->input->post('price_package');
		
		$inp = $this->db->query("INSERT INTO master_package (name_package,credit_package,price_package) VALUES ('$name_package','$credit_package','$price_package') ");
		if ($inp) {
			$rest['status'] 	= true;
			$rest['code'] 		= 1;			
			$rest['message'] 	= 'Success';	       
		}
		else
		{
			$rest['status'] 	= false;
			$rest['code'] 		= -1;			
			$rest['message'] 	= 'Failed';
		}
		
		echo json_encode($rest);
	}
	public function session_check()
	{		
		if($this->session->has_userdata('id_user')){		
			return 1;
		}else{
			return 0;
		}
	}

	function Logout(){
		$this->session->sess_destroy();
		redirect('/');
	}
}