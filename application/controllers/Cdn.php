<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdn extends CI_Controller {

	// SSH Host 
    private $ssh_host = 'cdn.classmiles.com'; 
    // SSH Port 
    private $ssh_port = 24; 
    // SSH Server Fingerprint 
    private $ssh_server_fp = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'; 
    // SSH Username 
    private $ssh_auth_user = 'hermi'; 
    // SSH Public Key File 
    private $ssh_auth_pub = '/etc/ssl/cm_ssl/GCP_Public_Key_SSH_05072017.pub'; 
    // SSH Private Key File 
    private $ssh_auth_priv = '/etc/ssl/cm_ssl/GCP_Private_Key_SSH_05072017_converted.ppk'; 

    private $ssh_methods = array(
    'hostkey'=>'ssh-rsa,ssh-dss',
//    'kex' => 'diffie-hellman-group-exchange-sha256',
    'client_to_server' => array(
        'crypt' => 'aes256-ctr,aes192-ctr,aes128-ctr,aes256-cbc,aes192-cbc,aes128-cbc,3des-cbc,blowfish-cbc',
        'comp' => 'none'),
    'server_to_client' => array(
        'crypt' => 'aes256-ctr,aes192-ctr,aes128-ctr,aes256-cbc,aes192-cbc,aes128-cbc,3des-cbc,blowfish-cbc',
        'comp' => 'none'));
    // SSH Private Key Passphrase (null == no passphrase) 
    private $ssh_auth_pass = 'T0l4kB4l4'; 
    // SSH Connection 
    private $connection; 

	public function __construct()
	{
		parent::__construct();
	}
	public function index($imgcontent='')
	{

		/*$w = stream_get_wrappers();
echo 'openssl: ',  extension_loaded  ('openssl') ? 'yes':'no', "<br>";
echo 'http wrapper: ', in_array('http', $w) ? 'yes':'no', "<br>";
echo 'https wrapper: ', in_array('https', $w) ? 'yes':'no', "<br";
echo 'wrappers: ', var_export($w);*/

		// header("Content-Type: image/jpeg");
		// $this->load->helper('file');

		$image_path = 'aset/img/user/32.jpg';

		// $this->output->set_content_type(get_mime_by_extension($image_path));
		// $this->output->set_output(file_get_contents($image_path));
		$code_binary = file_get_contents($image_path);

		// $code_binary = base64_decode($code_base64);
		$image= imagecreatefromstring($code_binary);
		header('Content-Type: image/jpg');
		imagejpeg($image);
		imagedestroy($image);


		// Read image path, convert to base64 encoding
		// $imageData = base64_encode(file_get_contents($image_path));

		// Format the image SRC:  data:{mime};base64,{data};
		// $src = 'data: '.mime_content_type($image_path).';base64,'.$imageData;

		// file_put_contents('aset/img/user/a.jpg', file_get_contents($image_path));
		// echo mime_content_type($image_path);
		// readfile($image_path);



		// $this->load->image("aset/img/user/497000.jpg", 'image/jpeg');
		/*$filename= "aset/3.png"; //<-- specify the image  file
		// readfile($filename);
		// echo filesize($filename);
		// return false;
		if(file_exists($filename)){
		  header('Content-Length: '.filesize($filename)); //<-- sends filesize header
		  header('Content-Type: image/png'); //<-- send mime-type header
		  header('Content-Disposition: inline; filename="'.$filename.'";'); //<-- sends filename header
		  readfile($filename); //<--reads and outputs the file onto the output buffer
		  die(); //<--cleanup
		  exit; //and exit
		}else{
			echo "1";
		}*/
	}
	public function begin()
	{
		
		return '<img src="https://air.classmiles.com/usercontent/3.jpg" />';
	}

	public function request_rejector()
	{
		$now = date('Y-m-d H:i:s', time() - 3600);
		$data = $this->db->query("SELECT * FROM tbl_request WHERE approve=0 && datetime<='$now'")->result_array();
		echo 'private : <pre>';
		print_r($data);
		echo '</pre>';
		foreach ($data as $key => $value) {
			$rid=$value['request_id'];
			$this->db->simple_query("UPDATE tbl_request SET approve=-2 WHERE request_id=$rid");
			$this->Process_model->reject_request($rid, 'private');
		}
		$data = $this->db->query("SELECT * FROM tbl_request_grup WHERE approve=0 && datetime<='$now'")->result_array();
		echo 'group : <pre>';
		print_r($data);
		echo '</pre>';
		foreach ($data as $key => $value) {
			$rid=$value['request_id'];
			$this->db->simple_query("UPDATE tbl_request_grup SET approve=-2 WHERE request_id=$rid");
			$this->Process_model->reject_request($rid, 'group');
		}
	}
	public function morning_checker_notification()
	{
		$fields = array();
		$today = date('Y-m-d');

		// SELECT ALL START TODAY
		$users_arr = array();
		$all_class = $this->db->query("SELECT tc.*, tu.user_name, ms.subject_name FROM tbl_class as tc INNER JOIN tbl_user as tu ON tc.tutor_id=tu.id_user INNER JOIN master_subject as ms ON tc.subject_id=ms.subject_id where start_time like '{$today}%' ORDER BY start_time ASC")->result_array();
		foreach ($all_class as $i => $kelas) {
			// TAKE ALL PARTICIPANT IN THAT CLASS
			$visible = json_decode($kelas['participant'],true);
			$tutor_id = $kelas['tutor_id'];
			$subject_id = $kelas['subject_id'];
			$slap = array();
			// $slap['tutor_name'] = $kelas['user_name'];
			$slap['subject_name'] = $kelas['subject_name'];
			$slap['time'] = substr($kelas['start_time'], 11,8);

			if($visible['visible'] == "followers"){
				$the_user = $this->db->query("SELECT * FROM tbl_booking where tutor_id='{$tutor_id}' AND subject_id='{$subject_id}'")->result_array();
				foreach ($the_user as $k => $value) {
					$id_user = $value['id_user'];
					if(isset($users_arr[$id_user])){
						array_push($users_arr[$id_user], $slap);
					}else{
						$users_arr[$id_user][0] = $slap;
					}
				}
			}else if($visible['visible'] == "private"){
				foreach ($visible['participant'] as $k => $value) {
					$id_user = $value['id_user'];
					if(isset($users_arr[$id_user])){
						array_push($users_arr[$id_user], $slap);
					}else{
						$users_arr[$id_user][0] = $slap;
					}
				}
			}
		}
		foreach ($users_arr as $key => $value) {
			$tot_class = count($users_arr[$key]);

			if($tot_class == 1){
				$fields['notification'] = array("body" => $users_arr[$key][0]['subject_name']."    ".$users_arr[$key][0]['time'], "title" => "Classmiles", "sound" => "Reminder.getRingtone()");
			}else{
				$fields['data']['data'] = array("image" => "", "title" => "Classmiles", "is_background" => false, "message" => $tot_class." classes","payload"=> $users_arr[$key], "timestamp" => date('Y-m-d H:i:s'));
			}
			$this->Rumus->create_notif('Kamu memiliki '.$tot_class.' kelas untuk hari ini',serialize($fields),'single',$key,BASE_URL);
		}
	}

	public function sftp($value='')
	{
		// echo "hai";
		// return false;
		$callbacks = array('disconnect' => function(){
			echo "hai";
		}, 'macerror' => function(){
			echo "hai error";
		}, 'ignore' => function(){
			echo "hai ignore";
		});
		if (!($this->connection = ssh2_connect($this->ssh_host, $this->ssh_port, $this->ssh_methods, $callbacks))) { 
            die('Cannot connect to server'); 
        }

        //AUTH THROUGH PUBKEY
        /*$fingerprint = ssh2_fingerprint($this->connection, SSH2_FINGERPRINT_MD5 | SSH2_FINGERPRINT_HEX); 
        if (strcmp($this->ssh_server_fp, $fingerprint) !== 0) { 
            die('Unable to verify server identity!'); 
        } */
        if (!ssh2_auth_pubkey_file($this->connection, $this->ssh_auth_user, $this->ssh_auth_pub, $this->ssh_auth_priv, $this->ssh_auth_pass)) { 
            die('Autentication rejected by server'); 
        }
        //AUTH THROUGH PLAIN PASSWORD
        /*if (!ssh2_auth_password($this->connection, $this->ssh_auth_user, $this->ssh_auth_pass)) {
  			die('Autentication rejected by server'); 
		}*/
		echo "authenticated<br>";

		/*if (!($stream = ssh2_exec($this->connection, 'ls -l'))) { 
            throw new Exception('SSH command failed'); 
        } 
        stream_set_blocking($stream, true); 
        $data = ""; 
        while ($buf = fread($stream, 4096)) {
            $data .= $buf; 
        } 
        fclose($stream); 
        print_r($data);*/

	}
	public function gstore($value='')
	{
		echo $this->Rumus->sendToCDN(CDN2_URL,USER_IMAGE_CDN_URL,'ketawa.jpg', null, 'aset/img/user/1014000.jpg');
	}

}
