<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class First extends CI_Controller {

	public $sesi = array("lang" => "indonesia");

	public function __construct()
	{
		parent::__construct();
        $this->load_lang($this->session->userdata('lang'));
        $this->load->library('recaptcha');
		$this->session->set_userdata('color','blue'); 
		$this->load->library('email');
		// $this->load->library('fpdf');
        $this->load->helper(array('Form', 'Cookie', 'String'));
        $isMobile = false;

        $this->load->library('Mobile_Detect');
        $detect = new Mobile_Detect();
		// Check for any mobile device.
		if ($detect->isMobile()){
			$this->isMobile = true;
			if (base_url() == 'https://classmiles.com/') {
				// redirect('https://m.classmiles.com');
			}else{

			}
			define('MOBILE', 'mobile/');

		}else{
			define('MOBILE', '');
		}
	}

	public function index($page = 0)
	{				
		// ambil cookie
        $cookie = get_cookie('CMKEY');        
        if (!empty($cookie)) {
        	$row = $this->M_login->get_by_cookie($cookie);
            if ($row) {
            	
                $where = array(
					'email' => $row['email']
					// 'kata_sandi' => $row['password']
				);

                // echo "ini username ".$row['email'];
                // echo "<br>";
                // echo "ini password ".$row['password'];
                // return false;
				$cek = $this->M_login->cek_cokeis("tbl_user",$where);
				
				if (empty($cek)) {
					redirect('/');
				}				
				$data['sideactive'] = "home";
				$data['page'] = $page;
				$this->load->view(MOBILE.'inc/header');
				$this->load->view(MOBILE.'index',$data);
            } 
            else 
            {
                redirect('/');
            }
        } 
        else 
        {
			if($this->session->userdata('usertype_id') == "tutor"){
				redirect('/tutor');
			}
			if($this->session->userdata('usertype_id') == "student kid"){
				redirect('/Kids');
			}
			if($this->session->userdata('usertype_id') == "admin"){
				redirect('/admin');
			}
			if($this->session->userdata('usertype_id') == "technical_support"){
				redirect('/technical_support');
			}
			if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid"){

				$data['sideactive'] = "home";
				$data['page'] = $page;
				$this->load->view(MOBILE.'inc/header');
				$this->load->view(MOBILE.'index',$data);
			}else{
				// echo MOBILE;

				// $this->session->set_userdata('lang','indonesia');
				$this->load->view(MOBILE.'home/header');
				$this->load->view(MOBILE.'home/index');					
			}
		}
		
	}

	public function Class()
	{
		// echo "a";
		// return false;
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'class/index');
	}

	public function c()
	{
		$this->load->view(MOBILE."login_channel");
	}

	public function Camera()
	{
		// echo "a";
		// return false;
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'camera/index');
	}

	public function mobile()
	{
		if ($this->isMobile){
			$this->load->view(MOBILE.'home/header');
			$this->load->view(MOBILE.'home/mobile');
		}else{
			redirect('/');
		}
	}

	public function web()
	{
		// $this->load->view(MOBILE.'home/header');
		$this->load->view(MOBILE.'home/web');
	}		

	public function syarat_ketentuan()
	{
		$this->load->view(MOBILE.'home/header');
		$this->load->view(MOBILE.'home/syarat-kententuan');
	}

	public function cobacalendar()
	{		
		$this->load->view(MOBILE.'cobacalendar');
	}

	public function subjects()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid"){
			$data['sideactive'] = "subject";
			$data['allsub'] = $this->Master_model->getSubject();
			// $data['getTutor'] = $this->Master_model->
			$data['alltutor'] = $this->Master_model->getMyTutor();
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'subjects',$data);
		}
		else{
			redirect('/');
		}

		/*if($this->session->userdata('lang')){
			$this->load_lang($this->session->userdata('lang'));
		}else{
			$this->load_lang('indonesia');
		}
		echo $this->lang->line('home');*/
	}
	public function all_subject()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid"){
			$data['sideactive'] = "all_subject";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'all_subject',$data);			
		}
		else{
			redirect('/');
		}
		/*if($this->session->userdata('lang')){
			$this->load_lang($this->session->userdata('lang'));
		}else{
			$this->load_lang('indonesia');
		}
		echo $this->lang->line('home');*/
	}

	public function ondemand()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid"){
			$data['sideactive'] = "ondemand";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'on_demand',$data);
		}
		else{
			redirect('/');
		}
	}

	public function data_ondemand()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid"){
			$data['sideactive'] = "ondemand";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'data_ondemand',$data);
		}
		else{
			redirect('/');
		}
	}	

	public function classhistory()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid"){
			$data['sideactive'] = "classhistory";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'replay/classhistory',$data);
		}
		else{
			redirect('/');
		}
	}

	public function uangsaku()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid"){
			$data['sideactive'] = "uangsaku";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'uangsaku',$data);
		}
		else{
			redirect('/');
		}
	}

	public function Epocket()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid") {
			$data['sideactive'] = "daftartransaksi";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/epocket', $data);
		}
		else
		{
			redirect('/');
		}
	}

	public function Accountlist()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid") {
			$data['sideactive'] = "accountlist";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/accountlist', $data);
		}
		else
		{
			redirect('/');
		}
	}
	
	public function editbank()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid") {
			$data['sideactive'] = "accountlist";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/editbank', $data);
		}
		else
		{
			redirect('/');
		}
	}

	public function Topup()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid") {
			$data['sideactive'] = "topup";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/topup', $data);
		}
		else
		{
			redirect('/');
		}
	}

	public function Complain()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid"){
			$data['sideactive'] = "complain";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'inc/complain',$data);
		}
		else{
			redirect('/');
		}
	}

	public function Transfer()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid") {
			$data['sideactive'] = "topup";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/transfer', $data);
		}
		else
		{
			redirect('/');
		}
	}

	public function Cara_topup()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid") {
			$data['sideactive'] = "caratopup";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/caratopup', $data);
		}
		else
		{
			redirect('/');
		}
	}

	public function Konfirmasi()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid") {
			$data['sideactive'] = "daftartransaksi";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/konfirmasi', $data);
		}
		else
		{
			redirect('/');
		}
	}

	public function Details()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid") {
			$data['sideactive'] = "daftartransaksi";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'topup/details', $data);
		}
		else
		{
			redirect('/');
		}
	}

	public function sukses()
	{
		$data['sideactive'] = "topup";
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'topup/sukses',$data);
	}

	public function failed()
	{
		$data['sideactive'] = "topup";
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'topup/failed',$data);
	}

	public function ondemandpalsu()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid"){
			$data['sideactive'] = "ondemand";
			$this->load->view(MOBILE.'inc/cssjs_load');
			$this->load->view(MOBILE.'ondemand_palsu',$data);
		}
		else{
			redirect('/');
		}
	}
	public function history()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid"){
			$data['sideactive'] = "history";
			$data['dataimage'] = $this->Master_model->ambildataphoto();
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'history',$data);
		}
		else{
			redirect('/');
		}
		/*if($this->session->userdata('lang')){
			$this->load_lang($this->session->userdata('lang'));
		}else{
			$this->load_lang('indonesia');
		}
		echo $this->lang->line('home');*/
	}

	public function Reputation()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid"){
			$data['sideactive'] = "reputation";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'review',$data);
		}
		else{
			redirect('/');
		}
	}

	public function KidsRegister()
	{
		if($this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "mykids_registrasion";
			$this->load->view(MOBILE.'inc/header');
			$data['alluserprofile'] = $this->Master_model->getTblUserAndProfile();
			$this->load->view(MOBILE.'kids/register',$data);
		}
		else{
			redirect('/');
		}
	}

	public function profile()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid"){
			if ($this->session->flashdata('rets')) {
				$data = $this->session->flashdata('rets');
			}
			$data['sideactive'] = "profile";
			$this->load->view(MOBILE.'inc/header');
			$data['alluserprofile'] = $this->Master_model->getTblUserAndProfile();
			$data['dataimage'] = $this->Master_model->ambildataphoto();		
			$this->load->view(MOBILE.'profile',$data);
		}
		else{
			redirect('/');
		}		
	}

	public function about()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid" ){
			$data['sideactive'] = "profile";
			$data['profile'] = $this->Master_model->getTblUserAndProfile();
			$data['dataimage'] = $this->Master_model->ambildataphoto();
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'about',$data);
		}
		else{
			redirect('/');
		}
		/*if($this->session->userdata('lang')){
			$this->load_lang($this->session->userdata('lang'));
		}else{
			$this->load_lang('indonesia');
		}
		echo $this->lang->line('home');*/
	}
	public function profile_account()
	{
		if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid"){

			if ($this->session->flashdata('rets')) {
				$data = $this->session->flashdata('rets');
			}

			$data['sideactive'] = "profile_account";		
			$data['alluser'] = $this->Master_model->getTblUser();
			$data['dataimage'] = $this->Master_model->ambildataphoto();
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'profile_account',$data);
		}
		else{
			redirect('/');
		}
		/*if($this->session->userdata('lang')){
			$this->load_lang($this->session->userdata('lang'));
		}else{
			$this->load_lang('indonesia');
		}
		echo $this->lang->line('home');*/
	}
	public function profile_school()
	{
		if($this->session_check() == 1 && ($this->session->userdata('usertype_id') != "student kid" || $this->session->userdata('usertype_id') == 'student')){
			if($this->session_check() == 1){
				if ($this->session->flashdata('rets')) {
					$data = $this->session->flashdata('rets');
				}
				$data['sideactive'] = "profile";
				$this->load->view(MOBILE.'inc/header');
				$data['alluserprofile'] = $this->Master_model->getTblUserAndProfile();
				$data['dataimage'] = $this->Master_model->ambildataphoto();		
				$this->load->view(MOBILE.'profile_school',$data);
			}
			else{
				echo "asdfsdf";
			}
		}
		/*if($this->session->userdata('lang')){
			$this->load_lang($this->session->userdata('lang'));
		}else{
			$this->load_lang('indonesia');
		}
		echo $this->lang->line('home');*/
	}

	function ambil_data(){

		$modul=$this->input->post('modul');
		$id=$this->input->post('id');

		if($modul=="kabupaten"){
			echo $this->Master_model->kabupaten($id);
		}
		else if($modul=="kecamatan"){
			echo $this->Master_model->kecamatan($id);
		}
		else if($modul=="kelurahan"){
			echo $this->Master_model->kelurahan($id);
		}
	}

	function getlevels(){		
		$name= $this->input->post('name');
		echo $this->Master_model->levels($name);		
	}

	function getlevelsregister(){		
		$name= $this->input->post('name');
		echo $this->Master_model->levelsregister($name);		
	}
	
	public function profile_forgot()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid") {
			$data['sideactive'] = "profile_forgot";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'profile_forgot',$data);
		}
		else{
			redirect('/');
		}
	}

	public function geo_location()
	{
		$this->load->view('inc/geoplugin_activation');
	}

	public function DetailPayment()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid") {
			$data['sideactive'] = "home";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'order_Transfer',$data);
		}
		else{
			redirect('/');
		}
	}

	public function ConfirmPayment()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid") {
			$data['sideactive'] = "home";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'order_Konfirmasi',$data);
		}
		else{
			redirect('/');
		}
	}

	public function DetailOrder()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid") {
			$data['sideactive'] = "home";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'order_Details',$data);
		}
		else{
			redirect('/');
		}
	}
	public function register_email()
	{			
		if ($this->session_check() != 1) {	
			$data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; background-attachment: fixed; height: 100% width:100%;" background="'.base_url().'aset/img/blur/img3.jpg"';
			$this->load->view(MOBILE.'inc/header',$data);
			$new['1'] = null;
			if($this->session->flashdata('google_data')){
				$new['google_data'] = $this->session->flashdata('google_data');
			}
			if($this->session->userdata('lang') == 'indonesia'){
				$this->session->set_userdata('lang','indonesia');
			}
			else
			{
				$this->session->set_userdata('lang','english');
			}
			$this->load->view(MOBILE.'register_email',$new);
		}
		else
		{
			redirect('/');
		}
	}

	public function newPassword()
	{	
		$r_link = $this->input->get('randomLink');
		$email_user = $this->input->get('email');
		$cek_valid = $this->db->query("SELECT *FROM gen_link WHERE random_link='$r_link' AND email_user='$email_user'")->row_array();

		if ($cek_valid == null) {
			$load = $this->lang->line('linkexpired');
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message',$load);
			redirect('/');
		}else{
			$data['sideactive'] = "profile_account";
		 	$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'newpassword',$data);
		}

			
	}

	// public function login_mobile()
	// {
	// 	if ($this->session_check() != 1) {	
	// 	$data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; background-attachment: fixed; height: 100%;" background="'.base_url().'aset/img/blur/img3.jpg"';
	// 	$this->load->view(MOBILE.'inc/header', $data);
	// 	$this->load->view(MOBILE.'login_mb');	
	// 	}else{
	// 		redirect('/');
	// 	}	
	// }

	public function login_web()
	{
		// if ($this->session_check() != 1) {	
		// $data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; background-attachment: fixed; height: 100%;" background="'.base_url().'aset/img/blur/img3.jpg"';
		// if($this->session->userdata('lang') == 'indonesia'){
		// 		$this->session->set_userdata('lang','indonesia');
		// 	}
		// 	else
		// 	{
		// 		$this->session->set_userdata('lang','english');
		// 	}
		// $this->load->view(MOBILE.'inc/header', $data);
		// $this->load->view(MOBILE.'login_wb');	
		// }else{
			redirect('/');
		// }	
	}

	public function login()
	{
		// if ($this->session_check() != 1) {	
		// 	$data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; height: 100%;" background="'.base_url().'aset/img/blur/img3.jpg"';
		// 	if($this->session->userdata('lang') == 'indonesia'){
		// 		$this->session->set_userdata('lang','indonesia');
		// 	}
		// 	else
		// 	{
		// 		$this->session->set_userdata('lang','english');
		// 	}			
		// 	$this->load->view(MOBILE.'inc/header', $data);
		// 	$this->load->view(MOBILE.'login');	
		// }else{
			redirect('/');
		// }	
	}

	public function verifikasi($r_key){
		$detect = new Mobile_Detect();

		// Check for any mobile device.
		if ($detect->isMobile()){
			// redirect(BASE_URL().'first/verifikasi/'.$r_key);
			$res = $this->M_login->getAuth($r_key);
			if (empty($res)) {
				// $this->load->view(MOBILE.'inc/header');
				// $this->load->view(MOBILE.'s_ver');
				// $this->session->set_userdata("code", "1088");
				// redirect('/');
				$data['data'] = 1;
				$this->load->view(MOBILE.'home/header');
				$this->load->view(MOBILE.'home/s_ver',$data);
			}else{
				$upd = $this->db->simple_query("UPDATE tbl_user SET status='1' WHERE id_user='$res[id_user]'");
				if ($upd) {
					$del = $this->db->simple_query("DELETE FROM gen_auth WHERE id_user='$res[id_user]'");
					// $simpan_notif = $this->db->simple_query("INSERT INTO tbl_notif (notif,notif_type,id_user,link,status) VALUES ('Yeay verifikasi telah berhasil, silahkan pilih mata pelajaran dan guru untuk memulai kelas private.','general','$userid','https://beta.dev.meetaza.com/classmiles/first/subjects','0')");

					// $simpan_notif = $this->Rumus->create_notif($notif='Yeay verifikasi telah berhasil, silahkan pilih mata pelajaran dan guru untuk memulai kelas private.',$notif_mobile='Yeay verifikasi telah berhasil, silahkan pilih mata pelajaran dan guru untuk memulai kelas private.',$notif_type='single',$id_user=$userid,$link='https://classmiles.com/first/subjects');

					//TAMBAH NOTIF
					$notif_message = json_encode( array("code" => 34));
					$link = BASE_URL()."/Subjects";
					$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$userid','$link','0')");
					
					$data['data'] = 900;
					$this->load->view(MOBILE.'home/header');
					$this->load->view(MOBILE.'home/s_ver',$data);
				}
			}
		}
		else{
			$res = $this->M_login->getAuth($r_key);
			if (empty($res)) {
				// $this->load->view(MOBILE.'inc/header');
				// $this->load->view(MOBILE.'s_ver');
				$this->session->set_userdata("code", "1088");
				redirect('/');
			}else{
				$res = $this->M_login->getUser($res['id_user']);
				$userid = $res['id_user'];			
				if (empty($res)) {
					redirect('/');
				}else{
					$upd = $this->db->simple_query("UPDATE tbl_user SET status='1' WHERE id_user='$res[id_user]'");
					if ($upd) {
						$del = $this->db->simple_query("DELETE FROM gen_auth WHERE id_user='$res[id_user]'");
						// $simpan_notif = $this->db->simple_query("INSERT INTO tbl_notif (notif,notif_type,id_user,link,status) VALUES ('Yeay verifikasi telah berhasil, silahkan pilih mata pelajaran dan guru untuk memulai kelas private.','general','$userid','https://beta.dev.meetaza.com/classmiles/first/subjects','0')");

						// $simpan_notif = $this->Rumus->create_notif($notif='Yeay verifikasi telah berhasil, silahkan pilih mata pelajaran dan guru untuk memulai kelas private.',$notif_mobile='Yeay verifikasi telah berhasil, silahkan pilih mata pelajaran dan guru untuk memulai kelas private.',$notif_type='single',$id_user=$userid,$link='https://classmiles.com/first/subjects');

						//TAMBAH NOTIF
						$notif_message = json_encode( array("code" => 34));
						$link = BASE_URL()."/Subjects";
						$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$userid','$link','0')");
						
						$this->session->set_userdata("code", "107");				
						$this->load->view(MOBILE.'inc/header');
						$this->load->view(MOBILE.'s_ver');
					}
				}
			}
		}

	}

	public function verifikasiTutor($r_key){
		$detect = new Mobile_Detect();

		// Check for any mobile device.
		if ($detect->isMobile()){
			// redirect('https://m.classmiles.com/first/verifikasi/'.$r_key);
			$res = $this->M_login->getAuth($r_key);
			if (empty($res)) {
				$data['data'] = 1;
				$this->load->view(MOBILE.'home/header');
				$this->load->view(MOBILE.'home/s_ver',$data);				
			}else{
				$upd = $this->db->simple_query("UPDATE tbl_user SET status='3' WHERE id_user='$res[id_user]'");
				$userid = $res['id_user'];
				if ($upd) {
					$del = $this->db->simple_query("DELETE FROM gen_auth WHERE id_user='$res[id_user]'");
					// $simpan_notif = $this->db->simple_query("INSERT INTO tbl_notif (notif,notif_type,id_user,link,status) VALUES ('Verifikasi telah berhasil, Silahkan lengkapi data diri anda agar kami bisa segera memproses.','complete profile','$userid','https://beta.dev.meetaza.com/classmiles/tutor/approval','0')");

					//NOTIF
					$notif_message = json_encode( array("code" => 43));	
					$link = BASE_URL()."/tutor/approval";
					$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$userid','$link','0')");

					// $simpan_notif = $this->Rumus->create_notif($notif='Verifikasi telah berhasil, Silahkan lengkapi data diri anda agar kami bisa segera memproses.',$notif_mobile='Verifikasi telah berhasil, Silahkan lengkapi data diri anda agar kami bisa segera memproses.',$notif_type='single',$id_user=$userid,$link='https://classmiles.com/tutor/approval');
					$data['data'] = 901;
					$this->load->view(MOBILE.'home/header');
					$this->load->view(MOBILE.'home/s_ver',$data);		
				}
			}
		}
		else{
			$res = $this->M_login->getAuth($r_key);
			if (empty($res)) {				
				$this->session->set_userdata("code", "108");
				redirect('/');
			}else{						
				$upd = $this->db->simple_query("UPDATE tbl_user SET status='3' WHERE id_user='$res[id_user]'");
				$userid = $res['id_user'];
				if ($upd) {
					$del = $this->db->simple_query("DELETE FROM gen_auth WHERE id_user='$res[id_user]'");
					// $simpan_notif = $this->db->simple_query("INSERT INTO tbl_notif (notif,notif_type,id_user,link,status) VALUES ('Verifikasi telah berhasil, Silahkan lengkapi data diri anda agar kami bisa segera memproses.','complete profile','$userid','https://beta.dev.meetaza.com/classmiles/tutor/approval','0')");

					//NOTIF
					$notif_message = json_encode( array("code" => 43));	
					$link = "https://classmiles.com/tutor/approval";
					$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$id_user','$link','0')");

					// $simpan_notif = $this->Rumus->create_notif($notif='Verifikasi telah berhasil, Silahkan lengkapi data diri anda agar kami bisa segera memproses.',$notif_mobile='Verifikasi telah berhasil, Silahkan lengkapi data diri anda agar kami bisa segera memproses.',$notif_type='single',$id_user=$userid,$link='https://classmiles.com/tutor/approval');
					$this->session->set_userdata("code", "110");
					$load = $this->lang->line('activationsuccess');
					$this->session->set_flashdata('mes_alert','success');
					$this->session->set_flashdata('mes_display','block');
					$this->session->set_flashdata('mes_message',$load);						
					redirect('/');
				}
			}

			// }
		}
	}

	function gen_session(){

			$digit = 32;
			$karakter = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			srand((double)microtime()*1000000);
			$i = 0;
			$pass = "";
			while ($i <= $digit-1)
			{
			$num = rand() % 32;
			$tmp = substr($karakter,$num,1);
			$pass = $pass.$tmp;
			$i++;
			}
			echo $pass;

		// 	$where = array(
		// 			'random_key' => $pass
		// 		);

		// $cek = $this->M_login->saveRandKey("gen_auth", $where);
		// $this->session->set_flashdata('cek', $cek);	


	}
	
	public function register()
	{		
		if(isset($_GET['email']) || isset($_GET['type']))
		{
			if (!isset($_GET['type'])) {
				$email 	= $_GET['email'];
				$data['typeregister'] = "";
			}
			else if (!isset($_GET['email'])) {
				$typee 	= $_GET['type'];
				$data['typeregister'] = $typee;
			}
			else
			{
				$email 	= $_GET['email'];
				$typee 	= $_GET['type'];
				$data['typeregister'] = $typee;
			}

			$dataJenjangname = $this->Master_model->getDataJenjangname();
			if($this->session->flashdata('form_cache') != null){
				$data['form_cache'] = $this->session->flashdata('form_cache');
			}
			$data['jenjang_name'] = $dataJenjangname;
			$data['levels']=$this->Master_model->jenjangnameregister();
			$this->load->view(MOBILE.'inc/header', $data);
			$data['email'] = $email;
			
			$this->load->view(MOBILE.'register_thirdparty',$data);	
		}
		else
		{
			$dataJenjangname = $this->Master_model->getDataJenjangname();
			if($this->session->flashdata('form_cache') != null){
				$data['form_cache'] = $this->session->flashdata('form_cache');
			}
			$data['jenjang_name'] = $dataJenjangname;
			$data['levels']=$this->Master_model->jenjangnameregister();
			$this->load->view(MOBILE.'inc/header', $data);
			$this->load->view(MOBILE.'register');
		}

	}
	public function register_mobile()
	{		
		// $data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; background-attachment: fixed; height: 100%;" background="'.base_url().'aset/img/blur/img3.jpg"';
		$dataJenjangname = $this->Master_model->getDataJenjangname();


		if($this->session->flashdata('form_cache') != null){
			$data['form_cache'] = $this->session->flashdata('form_cache');
		}
		$data['jenjang_name'] = $dataJenjangname;
		$data['levels']=$this->Master_model->jenjangnameregister();
		// if($this->session->userdata('lang') == 'indonesia'){
		// 	$this->session->set_userdata('lang','indonesia');
		// }
		// else
		// {
		// 	$this->session->set_userdata('lang','english');
		// }

		$this->load->view(MOBILE.'inc/header', $data);
		$this->load->view(MOBILE.'register_m');

	}

	public function tutor_register(){
		// $data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; background-attachment: fixed; height: 100%;" background="'.base_url().'aset/img/blur/img3.jpg"';
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'register_tutor');
	}

	public function forgot()
	{
		$data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; background-attachment: fixed; height: 100%;" background="'.base_url().'aset/img/blur/img3.jpg"';
		if($this->session->userdata('lang') == 'indonesia'){
			$this->session->set_userdata('lang','indonesia');
		}
		else
		{
			$this->session->set_userdata('lang','english');
		}
		$this->load->view(MOBILE.'inc/header', $data);
		$this->load->view(MOBILE.'forgot.php');
	}

	public function resendEmail()
	{
		$data['sat'] = 'style="background-size: cover; background-repeat: no-repeat; background-attachment: fixed; height: 100%;" background="'.base_url().'aset/img/blur/img3.jpg"';
		$this->load->view(MOBILE.'inc/header', $data);
		$this->load->view(MOBILE.'resendEmail.php');
	}	

	public function session_check()
	{
		if($this->session->has_userdata('id_user')){
			$new_state = $this->db->query("SELECT status FROM tbl_user where id_user='".$this->session->userdata('id_user')."'")->row_array()['status'];
			if($this->session->userdata('status') != $new_state ){
				$this->session->sess_destroy();
				redirect('/');
			}
			// $this->sesi['user_name'] = $this->session->userdata('user_name');
			// $this->sesi['username'] = $this->session->userdata('username');
			// $this->sesi['priv_level'] = $this->session->userdata('priv_level');
			if($this->session->userdata('usertype_id') != "student" && $this->session->userdata('usertype_id') != "student kid"){
				redirect('/');
			}
			// if($this->session->userdata('usertype_id') != "student kid"){
			// 	redirect('/');
			// }
			return 1;
		}else{
			return 0;
		}
	}

	public function validationCheck(){

	}

	public function load_lang($lang = '')
	{
		$this->lang->load('umum',$lang);
	}
	public function set_lang($value='')
	{	
		if($value == ""){
			$this->session->set_userdata('lang','indonesia');
		}else{
			$this->session->set_userdata('lang',$value);
		}		
		
		return null;
	}

	public function mails(){
		$this->load->view(MOBILE.'mail_layout_reg');
	}
	public function createSession(){
		//echo $this->M_login->createSess($this->session->userdata('id_user'), $this->session->userdata('email')); 
	}

	public function getTutorProfile($id_user='')
	{
		$data = $this->db->query("SELECT tu.*, tpt.self_description, tpt.education_background, tpt.teaching_experience FROM tbl_user as tu INNER JOIN tbl_profile_tutor as tpt ON tu.id_user=tpt.tutor_id WHERE tu.id_user='".$id_user."'")->row_array();
		if($data['education_background'] != ''){
			$data['education_background'] = unserialize($data['education_background']);
		}
		if($data['teaching_experience'] != ''){
			$data['teaching_experience'] = unserialize($data['teaching_experience']);
		}
		echo json_encode($data);
		return null;
	}

	public function approveTheTutor()
	{
		$id_user 	= $this->input->get('id_user');
		$text 		= $this->input->get('text');
		$data 		= $this->db->simple_query("UPDATE tbl_user SET status=1 WHERE id_user='".$id_user."'");
		// $simpan_notif = $this->db->simple_query("INSERT INTO tbl_notif (notif,notif_type,id_user,link,status) VALUES ('Yeay, anda telah di terima sebagai guru di classmiles. Pilih mata pelajaran dan mulai membuat kelas anda.','Approval Tutor','$id_user','','0')");
		if ($data) {
			$upd 		= $this->db->query("UPDATE tbl_profile_tutor SET conclusion='".$text."' WHERE tutor_id='".$id_user."'");
			$selectname	= $this->db->query("SELECT * FROM tbl_user WHERE id_user='$id_user'")->row_array();
			$usernamee	= $selectname['user_name'];
			$emailll	= $selectname['email'];
			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' =>'info@classmiles.com',
				'smtp_pass' => 'kerjadiCLASSMILES',	
				'mailtype' => 'html',	
				// 'wordwrap' => true,
				'charset' => 'iso-8859-1'
				);

			$this->load->library('email', $config);
			$this->email->initialize($config);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('info@classmiles.com', 'Classmiles');
			$this->email->to($emailll);
			$this->email->subject('Classmiles - Account Success Accept');
			// $message = "Please click the link below to verify and activate your account.";
			// $message .= "Klik link http://beta.dev.meetaza.com/classmiles/first/verifikasi/".$random_key;
			//$this->email->message('Klik link http://beta.dev.meetaza.com/classmiles/first/verifikasi/'.$random_key);
			$templateverifikasi = 
			"
			<div style='height:100%;margin:0;padding:0;width:100%;background-color:#fafafa'>
			    <center>
			        <table align='center' border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' style='border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#fafafa'>
			            <tbody>
			                <tr>
			                    <td align='center' valign='top' style='height:100%;margin:0;padding:10px;width:100%;border-top:0'>
			                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='border-collapse:collapse;border:0;max-width:600px!important'>
			                            <tbody>
			                                <tr>
			                                    <td valign='top' style='background:#ffffff none no-repeat center/cover;background-color:#ffffff;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:0'>
			                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
			                                            <tbody>
			                                                <tr>
			                                                    <td valign='top'>
			                                                        <table align='left' width='100%' border='0' cellpadding='0' cellspacing='0' style='min-width:100%;border-collapse:collapse'>
			                                                            <tbody>
			                                                                <tr>
			                                                                    <td valign='top' style='text-align:center'>
			                                                                        <img align='center' alt='' src='".CDN_URL.STATIC_IMAGE_CDN_URL."maillogo_classmiles.png' width='100%' style='max-width:700px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none'>
			                                                                    </td>
			                                                                </tr>
			                                                            </tbody>
			                                                        </table>
			                                                    </td>
			                                                </tr>
			                                            </tbody>
			                                        </table>
			                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
			                                            <tbody>
			                                                <tr>
			                                                    <td valign='top' style='padding-top:9px'>
			                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse' width='100%'>
			                                                            <tbody>
			                                                                <tr>
			                                                                    <td valign='top' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left'>
			                                                                        <p style='margin:10px 0;padding:0;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left'>
			                                                                            Hello ".$usernamee.", <br><br>
			                                                                            Thank you for signing up at Classmiles.com.
			                                                                            <br><br>
			                                                                            Yeay you have been approved as a tutor in Classmiles. <br>
			                                                                            <br>
			                                                                            Please do not reply this email.
			                                                                            <br>
			                                                                            <b><i>Enjoy the new experience on learning and share it with others!</i></b>
			                                                                            <br>
			                                                                        </p>
			                                                                    </td>
			                                                                </tr>
			                                                            </tbody>
			                                                        </table>
			                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse; margin-top: 15px;' width='100%'>
			                                                            <tbody>
			                                                                <tr>
			                                                                    <td valign='top' class='m_2872471405169675079mcnTextContent' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
			                                                                        <em>Thank you,</em><br>
			                                                                        <em>Classmiles Team</em>
			                                                                    </td>
			                                                                </tr>
			                                                            </tbody>
			                                                        </table>
			                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse; margin-top: 15px;' width='100%'>
			                                                            <tbody>
			                                                                <tr>
			                                                                    <td valign='top' class='m_2872471405169675079mcnTextContent' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
			                                                                        <hr>
			                                                                    </td>
			                                                                </tr>
			                                                            </tbody>
			                                                        </table>
			                                                    </td>
			                                                </tr>
			                                            </tbody>
			                                        </table>
			                                    </td>
			                                </tr>                                
			                                <tr>
			                                    <td valign='top' style='background:#ffffff none no-repeat center/cover;background-color:#ffffff;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;'>                                        
			                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
			                                            <tbody>
			                                                <tr>
			                                                    <td valign='top' style='padding-top:9px'>
			                                                        <table align='left' style='max-width:100%;min-width:100%;border-collapse:collapse;' width='100%'>
			                                                            <tbody>
			                                                                <tr>
			                                                                    <td valign='top' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left'>
			                                                                        <p style='margin:10px 0;padding:0;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left'>
			                                                                            Yang Terhormat ".$usernamee.",<br><br> 
			                                                                            Terima kasih telah mendaftar di Classmiles.com
			                                                                            <br><br>
			                                                                            Yeay kamu telah disetujui menjadi tutor di Classmiles. <br>
			                                                                            <br>
			                                                                            Please do not reply this email.
			                                                                            <br>
			                                                                            <b><i>Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!</i></b>
			                                                                            <br>
			                                                                        </p>
			                                                                    </td>
			                                                                </tr>
			                                                            </tbody>
			                                                        </table>
			                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse; margin-top: 15px;' width='100%'>
			                                                            <tbody>
			                                                                <tr>
			                                                                    <td valign='top' class='m_2872471405169675079mcnTextContent' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
			                                                                        <em>Terima kasih,</em><br>
			                                                                        <em>Tim Classmiles</em><br><br>
			                                                                    </td>
			                                                                </tr>
			                                                            </tbody>
			                                                        </table>
			                                                    </td>
			                                                </tr>
			                                            </tbody>
			                                        </table>
			                                    </td>
			                                </tr>				                                
			                                <tr>
			                                    <td valign='top' style='background:#fafafa none no-repeat center/cover;background-color:#fafafa;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:9px'>
			                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
			                                            <tbody>
			                                                <tr>
			                                                    <td valign='top' style='padding-top:9px'>
			                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse' width='100%'>
			                                                            <tbody>
			                                                                <tr>
			                                                                    <td valign='top' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
			                                                                        <em>Copyright &copy; 2017 Classmiles, All rights reserved.</em><br>                                                                    
			                                                                    </td>
			                                                                </tr>
			                                                            </tbody>
			                                                        </table>
			                                                    </td>
			                                                </tr>
			                                            </tbody>
			                                        </table>
			                                    </td>
			                                </tr>
			                            </tbody>
			                        </table>
			                    </td>
			                </tr>
			            </tbody>
			        </table>
			    </center>
			</div>
			";
			/**/				
			$this->email->message($templateverifikasi);

			if ($this->email->send()) 
			{			
				$emailkedua = "hermawan@meetaza.com";
				$this->load->library('email', $config);
				$this->email->initialize($config);
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");
				$this->email->from('info@classmiles.com', 'Info Classmiles');
				$this->email->to($emailkedua);
				$this->email->subject($usernamee." (".$emailll.") telah disetujui menjadi Tutor Classmiles");	

				$templateverifikasi = 
				"
				<div style='height:100%;margin:0;padding:0;width:100%;background-color:#fafafa'>
				    <center>
				        <table align='center' border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' style='border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#fafafa'>
				            <tbody>
				                <tr>
				                    <td align='center' valign='top' style='height:100%;margin:0;padding:10px;width:100%;border-top:0'>
				                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='border-collapse:collapse;border:0;max-width:600px!important'>
				                            <tbody>
				                                <tr>
				                                    <td valign='top' style='background:#ffffff none no-repeat center/cover;background-color:#ffffff;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:0'>
				                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
				                                            <tbody>
				                                                <tr>
				                                                    <td valign='top'>
				                                                        <table align='left' width='100%' border='0' cellpadding='0' cellspacing='0' style='min-width:100%;border-collapse:collapse'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' style='text-align:center'>
				                                                                        <img align='center' alt='' src='".CDN_URL.STATIC_IMAGE_CDN_URL."maillogo_classmiles.png' width='100%' style='max-width:700px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none'>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                    </td>
				                                                </tr>
				                                            </tbody>
				                                        </table>
				                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
				                                            <tbody>
				                                                <tr>
				                                                    <td valign='top' style='padding-top:9px'>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left'>
				                                                                        <p style='margin:10px 0;padding:0;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left'>
				                                                                            Hello,<br><br>
				                                                                            ".$usernamee." (".$emailll.") telah disetujui menjadi Tutor Classmiles.
				                                                                            <br>
				                                                                        </p>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse; margin-top: 15px;' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' class='m_2872471405169675079mcnTextContent' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
				                                                                        <em>Thank you,</em><br>
				                                                                        <em>Classmiles Team</em>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse; margin-top: 15px;' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' class='m_2872471405169675079mcnTextContent' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
				                                                                        <hr>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                    </td>
				                                                </tr>
				                                            </tbody>
				                                        </table>
				                                    </td>
				                                </tr>
				                                <tr>
				                                    <td valign='top' style='background:#fafafa none no-repeat center/cover;background-color:#fafafa;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:9px'>
				                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
				                                            <tbody>
				                                                <tr>
				                                                    <td valign='top' style='padding-top:9px'>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
				                                                                        <em>Copyright &copy; 2017 Classmiles, All rights reserved.</em><br>                                                                    
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                    </td>
				                                                </tr>
				                                            </tbody>
				                                        </table>
				                                    </td>
				                                </tr>
				                            </tbody>
				                        </table>
				                    </td>
				                </tr>
				            </tbody>
				        </table>
				    </center>
				</div>
				";	
				$this->email->message($templateverifikasi);

				if ($this->email->send()) 
				{

					$load = $this->lang->line('approvesuccess');
					$rets['mes_alert'] ='success';
					$rets['mes_display'] ='block';
					$rets['mes_message'] =$load;
					// $this->session->set_userdata('message', 'tutorregistered');
					$this->session->set_flashdata('rets',$rets);			
					$simpan_notif = $this->Rumus->create_notif($notif='Yeay, anda telah di terima sebagai guru di classmiles. Pilih mata pelajaran dan mulai membuat kelas anda.',$notif_mobile='Yeay, anda telah di terima sebagai guru di classmiles. Pilih mata pelajaran dan mulai membuat kelas.',$notif_type='single',$id_user=$id_user,$link='https://classmiles.com/tutor/choose');
					
					redirect('admin/tutor_approval');
				}
				else
				{
					redirect('admin/tutor_approval');
				}
			}				
			else
			{
				redirect('admin/tutor_approval');
			}
		}
		else
		{
			
			redirect('admin/tutor_approval');
		}	
		return null;

	}
	public function declineTheTutor()
	{
		$id_user 		= $this->input->get("id_user");
		$text 			= $this->input->get('text');
		$data 			= $this->db->simple_query("UPDATE tbl_user SET status=-1 WHERE id_user='".$id_user."'");
		$upd 			= $this->db->query("UPDATE tbl_profile_tutor SET conclusion='".$text."' WHERE tutor_id='".$id_user."'");
		$simpan_notif 	= $this->Rumus->create_notif($notif='Maaf permintaan anda tidak diterima, anda belum memenuhi syarat sebagai tutor di Classmiles.',$notif_mobile='Maaf permintaan anda tidak diterima, anda belum memenuhi syarat sebagai tutor di Classmiles.',$notif_type='single',$id_user=$id_user,$link='');
		return null;
	}
	public function test_m()
	{
		$a = $this->Master_model->getAllKelurahan('1702041002','1702041');
		print_r($a);
	}

	public function expicture()
	{
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'ex_picture');
	}

	public function imgcrop()
	{
		// $this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'imgselect/index');
	}

	public function post_profile($act = ''){
    	 //print_R($post);die;
		// echo $act;
		// return null;
    	 switch($act) {
    	  case 'save' :
    		$this->Master_model->saveAvatarTmp();
    	  break;
    	  default:
    		$this->Master_model->changeAvatar();
    		
    	 }
	}

	public function test()
	{			
		$this->load->view(MOBILE."classroom_config/header");
		$this->load->view(MOBILE."classroom_view/test");		
	}
	public function test2()
	{	
		// $this->load->view(MOBILE."inc/header");
		// // print_r($this->session->userdata('form_cache'));
		// $this->load->view(MOBILE."inc/test2");
		$folder = $this->Rumus->classDrive_listFolders('32');
		$listfile = $this->Rumus->classDrive_listImages('32','Live Stream malming/');	
		echo json_encode($listfile);
	}

	public function test3()
	{	
		// print_r($this->session->userdata('form_cache'));
		$data['sideactive'] = "home";
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE."inc/test4", $data);
		
	}

	public function me()
	{	
		// print_r($this->session->userdata('form_cache'));
		$data['sideactive'] = "home";
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE."inc/me");
		
	}

	public function test4()
	{	
		// print_r($this->session->userdata('form_cache'));
		// $data['sideactive'] = "home";
		// $request_id = "71";
		// $type = "private";
		// $this->Process_model->reject_request($request_id, 'private');
		// $data['data'] = "0";
		// $data['allsub'] = $this->Master_model->getSubject();
		// $this->load->view(MOBILE.'home/header');
		$this->load->view(MOBILE."login_channel");
		
	}

	public function s_ver()
	{
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'s_ver');
	}

    public function charge()
    {
    	// print_r('<pre>');
    	// print_r($this->input->post());
    	// print_r('</pre>');
    	// return null;

    	// $mobile = $this->input->post('mobile');
  //   	echo $mobile;
  //   	return false;
    	$transaction_details = array(
		  'order_id' => rand(),
		  'gross_amount' => '', // no decimal allowed for creditcard
		);

		// Optional
		$customer_details = array(
		  'first_name'    => $this->session->userdata('user_name'),
		  'email'         => $this->session->userdata('email'),
		  'phone'         => $this->session->userdata('no_hp')
		  // 'billing_address'  => $billing_address,
		  // 'shipping_address' => $shipping_address
		);

		// Fill transaction details
		$transaction = array(
		  'transaction_details' => $transaction_details,
		  'customer_details' => $customer_details,
		);
		$snapToken = $this->midtrans->getSnapToken($transaction);
		error_log($snapToken);
		$data['snap'] = $snapToken;
    	$this->load->view(MOBILE.'topup/test', $data);
    }

    public function classroom()
    {
    	// $this->load->view(MOBILE.'inc/header');
    	// $this->load->view(MOBILE.'classroom_view/classroom');
    	$this->load->view(MOBILE."classroom_config/header");
		$this->load->view(MOBILE."classroom_view/test");
    }

    public function testclassroom()
    {
    	$this->load->view(MOBILE.'inc/header');
    	$this->load->view(MOBILE.'classroom/main');
    }

    public function testclassroomm()
    {
    	$this->load->view(MOBILE.'inc/header');
    	$this->load->view(MOBILE.'classroom/test2');
    }

    public function load(){
    	$this->load->view(MOBILE.'classroom/test3');
    }

    public function android()
    {	
    	$this->load->view(MOBILE."classroom_config/header");
    	$this->load->view(MOBILE."classroom_view/classandro");
    }

    public function fbtest()
    {	
    	$this->load->view(MOBILE."inc/header");
    	$this->load->view(MOBILE."classroom/fbtest");
    }

    public function testsubject()
    {
    	if($this->session_check() == 1 && $this->session->userdata('usertype_id') != "student kid"){
			$data['sideactive'] = "subject";
			$data['allsub'] = $this->Master_model->getSubject();
			// $data['getTutor'] = $this->Master_model->
			$data['alltutor'] = $this->Master_model->getMyTutor();
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'new_subject',$data);
		}
		else{
			redirect('/');
		}
    }

    public function cobakirim()
    {
    	$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_port' => 465,
				'smtp_user' =>'anonymousramdhani@gmail.com',
				'smtp_pass' => '',	
				'mailtype' => 'html',	
				// 'wordwrap' => true,
				'charset' => 'iso-8859-1'
				);

			$this->load->library('email', $config);
			$this->email->initialize($config);
			$this->email->set_mailtype("html");
			$this->email->set_newline("\r\n");
			$this->email->from('anonymousramdhani@gmail.com', 'Haji Support');
			$this->email->to("ramdhani.ti@gmail.com");
			$this->email->subject('Proses pembayaran Anda belum selesai');
			// $message = "Please click the link below to verify and activate your account.";
			// $message .= "Klik link http://beta.dev.meetaza.com/classmiles/first/verifikasi/".$random_key;
			//$this->email->message('Klik link http://beta.dev.meetaza.com/classmiles/first/verifikasi/'.$random_key);
			$templateverifikasi = 
			"
			<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8'>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
  <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js'></script>
  <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>  
<style type='text/css'>

  html,body { height: 100%; margin: 0px; padding: 0px; }
  #container{ margin-left: 2%; margin-right: 2%; width: 96%; -webkit-box-sizing: border-box; /* Safari/Chrome, WebKit */
  -moz-box-sizing: border-box;    /* Firefox, Gecko */
  box-sizing: border-box;         /* Opera/IE 8+ */}
  #judulhuji{ font-size: 18px; font-family: 'Helvetica'; background-color:white; height:14%; padding-top:2%; padding-bottom:2%; }

  #info1{ height: 55px; background-color: #E0E0E0; padding-top: 10px; width: 100%; }
  #info2{ margin-bottom: 10px; margin-top: 10px; padding-left: 5%; font-family: Arial,sans-serif,'Open sans'; color: #757575; font-size: 100%; float: left; height: auto;}
  #info3{ text-align: right; margin-right: 40px; margin-bottom: 10px; margin-top: 10px; font-family: Arial,sans-serif,'Open sans'; color: #757575; font-size: 100%; float:right; height: auto;}
  #sukses1{ height: 55px; background-color: #ff9800; padding-top: 10px; width: 100%; }
  #sukses2{ width: 100%; margin-bottom: 10px; margin-top: 10px; font-family: Arial,sans-serif,'Open sans'; font-size: 110%; height: auto; color:white;}
  
  #headerjudul { background-color:white; width: 100%; padding-top:2%; height:auto; padding-bottom: 2%;}
  #header1 { font-size:16px; color: #03A9F4; width: 100%; padding-left: 5%; padding-top: 1%; font-family: Arial,sans-serif,'Open sans';}
  #header2 { font-size:15px; color: #616161; width: 100%; padding-left: 5%; font-family: Arial,sans-serif,'Open sans';}
  #header3 { font-size:12px; color: #616161; width: 100%; padding-left: 5%; font-family: Arial,sans-serif,'Open sans'; }
  #rincianheader { background-color:white; height:auto; width: 100%; margin-top: -1%;}

  #headerjudul2 { background-color:white; width: 100%; padding-top:3%; height: auto;}  
  
  #isi1,#isi2,#isi3{ text-align: left; font-size: 14px; color: #616161; font-family: Arial,sans-serif,'Open sans'; line-height: 20px; padding-left: 5%;}
  #isi11,#isi22,#isi33{ text-align: right; font-size: 14px; color: #616161; font-family: Arial,sans-serif,'Open sans'; line-height: 20px; padding-right: 5%; float: right; }
  #perhatianheader { background-color:white; width: 100%; padding-top:1%; padding-bottom:1%; height:auto;}
  #perhatian{ text-align: left; font-size: 12px; color: #757575; font-family: 'Helvetica'; line-height: 20px; padding-right: 5%; float: left; padding-left: 5%; }
  #informasiheader { background-color:white; width: 100%; padding-top:3%; padding-bottom:1%; height:auto;}
  #informasi { font-size:15px; color: #616161; padding-left:5%; padding-right:5%; padding-bottom: 1%; padding-top: 1%; font-family: Arial,sans-serif,'Open sans'; line-height: 25px; width: 100%; }
  #informasi1{ text-align: left; font-size: 13px; padding-top: 1%; color: #757575; font-family: 'Helvetica'; line-height: 20px; padding-right: 1%; float: left; padding-left: 8%; }
  #informasi2{ text-align: left; font-size: 13px; padding-top: 2.5%; color: #757575; font-family: 'Helvetica'; line-height: 20px; padding-right: 1%; float: left; }
  #informasi3{ text-align: left; font-size: 13px; padding-top: 2.5%; color: #757575; font-family: 'Helvetica'; line-height: 20px; padding-right: 1%; float: left; }
  #tempatinformasi{ width: 60%; }
  #alert { background-color:white; width: 100%; padding-top:2%; height:auto;}
  #footer1 { font-size:12px; color: #9E9E9E; font-family: Arial,sans-serif,'Open sans'; line-height: 25px; width: 100%; }
  #footer2 { font-size:12px; color: #9E9E9E; padding-bottom: 2%; font-family: Arial,sans-serif,'Open sans'; line-height: 25px; width: 100%; }
  #footer3 { font-size:12px; color: #9E9E9E; padding-bottom: 4%; font-family: Arial,sans-serif,'Open sans'; line-height: 25px; width: 100%; }

  @media screen and (max-width: 768px){
      #info1{ height: 60px; background-color: #E0E0E0; padding-top: 10px; width: 100%; box-sizing: border-box; }
      #info2{ width: 48%; margin-bottom: 10px; margin-top: 10px; padding-left: 40px; font-family: Arial,sans-serif,'Open sans'; color: #757575; font-size: 90%; float: left; height: auto;}
      #info3{ width: 48%; text-align: right; margin-bottom: 10px; margin-top: 10px; font-family: Arial,sans-serif,'Open sans'; color: #757575; font-size: 90%; float:left; height: auto;}
      #headerjudul { background-color:white; width: 100%; padding-top:2%; padding-bottom:1%; height:auto;}
      #header1 { font-size:13px; color: #03A9F4; width: 100%; padding-left:5%; font-family: Arial,sans-serif,'Open sans';}
      #header2 { font-size:12px; color: #616161; width: 100%; padding-left: 5%; padding-top: 1%; font-family: Arial,sans-serif,'Open sans'; box-sizing: border-box; }
      #isi1,#isi2,#isi3{ text-align: left; font-size: 12px; color: white; font-family: 'Helvetica'; line-height: 20px; padding-left: 5%;}
      #isi11,#isi22,#isi33{ text-align: right; font-size: 12px; color: white; font-family: 'Helvetica'; line-height: 20px; padding-right: 5%; float: right; }
  }

</style>
</head>
<body style='background-color: #f2f3f4;'>
    
    <div id='container'>
        <div id='judulhuji'>
          <img style='padding-left: 5%;' src='https://classmiles.com/aset/img/logohuji.png' width='220px' height='50px'/>
          <br><br>
          <label style='width: 40%; padding-top: 0%; padding-left: 5%; font-size: 30px; font-family: sans-serif; width: 100%;'>Rp 40.090</label>          
        </div>  
        <div id='info1'>
            <div id='info2'>No Pesanan : 32839329</div>
            <div id='info3'>Pesanan Tanggal 4 April 2017</div>          
        </div>
        <div id='sukses1'>
            <div id='sukses2'><center>PESANAN DIBATALKAN.</center></div>
        </div>      
        <div id='headerjudul'>
            <div id='header1'>Yang terhormat, Ramdhani</div>
            <div id='header2'>Pesanan Anda telah dibatalkan. Karena telah melewati batas waktu transfer.</div>           
        </div>
        <div id='rincianheader'>
            <div style='height: auto; width: 100%; padding-top: 0%; padding-bottom: 2%;'>
              <label id='isi1'>Nama Travel </label><label id='isi11'>Ramdhani</label><br>
              <label id='isi2'>Code </label><label id='isi22'>40</label><br>
              <label id='isi2'>Harga </label><label id='isi22'>Rp. 1.000</label><br>
              <hr style='margin-left: 5%; margin-right: 5%;'>
              <label id='isi3'>Kode Unik </label><label id='isi33'>Rp 090</label><br>
              <label id='isi3'>Total </label><label id='isi33'>Rp 40.090</label><br>
            </div>
        </div>   
        <div id='alert'>
            <div id='header2'>Silakan hubungi Kami jika memiliki pertanyaan terkait pesanan Anda.</div>
        </div>              
        <div id='headerjudul2'>
          <center>
            <label id='footer1'><hr style='margin-left: 5%; margin-right: 5%; margin-bottom: 1%;'>Email ini dibuat secara otomatis. Mohon tidak mengirim balasan ke email</label><br>
            <label id='footer2'>Terima kasih</label><hr style='margin-left: 5%; margin-right: 5%;'><br>
            <label id='footer3'>Copyright 2017 Huji Indonesia</label>
          </center>
        </div>        
    </div> 
</body>
</html>
			"
			;
			/**/				
			$this->email->message($templateverifikasi);

			if ($this->email->send()) 
			{													
				redirect('first');
			}				
    }

    public function player()
    {
    	$this->load->view(MOBILE."inc/player");
    }


   function add(){
     // retrieve the album and add to the data array
       $data['log_trf_confirmation'] = $this->Process_model->getnorek();
       $this->load->view(MOBILE.'topup/konfirmasi, $data');
      }

      function getnorekk(){
      	$trfc = $this->input->post('trfc_id');
      	$norek = $this->getnorek($trfc);
      	$data = "<option value=''>--Pilih--</option>";
      	foreach ($norek as $mp) {

      		$data = "<option value='$mp[norekid]'>$mp[norekno]</option>/n";
      	}
      	echo $data;

      }     


      function addn(){
     // retrieve the album and add to the data array
       $data['log_trf_confirmation'] = $this->Process_model->getnorek();
       $this->load->view(MOBILE.'topup/konfirmasi, $data');
      }

      function getnorekkn(){
      	$trfc = $this->input->post('trfc_idn');
      	$norek = $this->getnorek($trfcn);
      	$data = "<option value=''>--Pilih--</option>";
      	foreach ($norekn as $mp) {

      		$data = "<option value='$mp[norekidn]'>$mp[noreknon]</option>/n";
      	}
      	echo $data;

      }     


       function getid(){
     // retrieve the album and add to the data array
       $data['log_trf_confirmation'] = $this->Process_model->getnorek();
       $this->load->view(MOBILE.'topup/konfirmasi, $data');
      }

      function getidn(){
      	$trfc = $this->input->post('trfc_idn');
      	$norek = $this->getnorek($trfcn);
      	$data = "<option value=''>--Pilih--</option>";
      	foreach ($idn as $mp) {

      		$data = "<option value='$mp[idn]'>$mp[idnn]</option>/n";
      	}
      	echo $data;

      }     

	//LEARN ANGULAR
	public function directive()
	{
		$this->load->view(MOBILE.'inc/directive');		
	}


	//Paypal
	public function do_purchase(){
		$email = $this->session->userdata('email');
		$nominal = $this->input->post('nominalsave');
		$config['business']					= $email;
		$config['cpp_header_image']			= '';
		$config['return']					= base_url().'first/sukses';
		$config['cancel_return']			= base_url().'first/failed';
		$config['notify_url']				= 'process_payment.php';
		$config['production']				= TRUE;
		$config['invoice']					= '';

		$this->load->library('paypal',$config);
		$this->paypal->add('Topup Saldo Classmiles',$nominal);

		$this->paypal->pay();

	}
	public function notify_payment(){
		$received_data = print_r($this->input->post(),TRUE);
		echo"<pre>".$received_data."<pre>";

	}
	public function cancel_payment(){

	}

	public function cobapdf(){
	   // define('FPDF_FONTPATH',$this->config->item('fonts_path'));
        $this->load->library('fpdf');
   $this->fpdf->FPDF("L","cm","A4");
   // kita set marginnya dimulai dari kiri, atas, kanan. jika tidak diset, defaultnya 1 cm
   $this->fpdf->SetMargins(1,1,1);
   /* AliasNbPages() merupakan fungsi untuk menampilkan total halaman
   di footer, nanti kita akan membuat page number dengan format : number page / total page
   */
   $this->fpdf->AliasNbPages();
   // AddPage merupakan fungsi untuk membuat halaman baru
	$this->fpdf->AddPage();
  // Setting Font : String Family, String Style, Font size
  	$this->fpdf->SetFont('Times','B',12);
  	$this->fpdf->Cell(30,0.7,'Laporan Produk dhani Jasa Jawa Barat',0,'C','C');
  	$this->fpdf->Ln();
	$this->fpdf->Cell(30,0.7,'Periode Bulan Agustus',0,'C','C');
 	$this->fpdf->Line(1,3.5,30,3.5);
 	$this->fpdf->Output("laporan.pdf","I");
	}
}
