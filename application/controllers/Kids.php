<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kids extends CI_Controller {

	public $sesi = array("lang" => "indonesia");

	public function __construct()
	{
		parent::__construct();
        // $this->load_lang($this->session->userdata('lang'));
        $this->load->library('recaptcha');
		$this->session->set_userdata('color','blue'); 
		$this->load->library('email');
		// $this->load->library('fpdf');
        $this->load->helper(array('Form', 'Cookie', 'String'));
         $this->load->library('Mobile_Detect');
        $detect = new Mobile_Detect();
		// Check for any mobile device.
		if ($detect->isMobile()){
			define('MOBILE', 'mobile/');
		}else{
			define('MOBILE', '');
		}
	}

	public function index($page = 0)
	{
		// ambil cookie
        $cookie = get_cookie('CMKEY');

        if (!empty($cookie)) {
        	$row = $this->M_login->get_by_cookie($cookie);
            if ($row) {
            	
                $where = array(
					'email' => $row['email']
					// 'kata_sandi' => $row['password']
				);

				$cek = $this->M_login->cek_cokeis("tbl_user",$where);
				
				if (empty($cek)) {
					redirect('/');
				}				
				$data['sideactive'] = "mykids";
				$data['page'] = $page;
				$this->load->view(MOBILE.'inc/header');
				$this->load->view(MOBILE.'kids/index',$data);
            } 
            else 
            {
                redirect('/');
            }
        } 
        else 
        {
			/*if($this->session->userdata('usertype_id') == "tutor"){
				redirect('/tutor');
			}
			if($this->session->userdata('usertype_id') == "admin"){
				redirect('/admin');
			}
			if($this->session->userdata('usertype_id') == "technical_support"){
				redirect('/technical_support');
			}*/
			if($this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
				
				// if($this->session_check() == 1){
					$data['sideactive'] = "mykids";
					$data['page'] = $page;
					$this->load->view(MOBILE.'inc/header');
					$this->load->view(MOBILE.'kids/index',$data);
				// }else{
				// 	$this->load->view(MOBILE.'home/header');
				// 	$this->load->view(MOBILE.'home/index');					
				// }
				
			}
			else{
				redirect('/');
			}
		}
	}

	public function Registration()
	{
		if($this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "mykids_registrasion";
			$this->load->view(MOBILE.'inc/header');
			$data['alluserprofile'] = $this->Master_model->getTblUserAndProfile();
			$this->load->view(MOBILE.'kids/register',$data);
		}
		else{
			redirect('/');
		}
	}

	public function Subject()
	{
		if($this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "mykids_subject";
			// $data['allsub'] = $this->Master_model->getSubject();
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'kids/subjects',$data);
		}
		else{
			redirect('/');
		}
	}

	public function Ondemand()
	{
		if($this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "mykids_ondemand";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'kids/on_demand',$data);
		}
		else{
			redirect('/');
		}
	}

	public function Dataondemand()
	{
		if($this->session->userdata('usertype_id') == "student kid" || $this->session->userdata('usertype_id') == "student"){
			$data['sideactive'] = "mykids_dataondemand";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'kids/data_ondemand',$data);
		}
		else{
			redirect('/');
		}
	}

	public function registerkids(){
		$id_user 			= $this->session->userdata('id_user');
		$email 				= $this->input->post('email');
		$username 			= $this->input->post('username');
		$first_name 		= $this->input->post('first_name');
		$last_name 			= $this->input->post('last_name');
		$password 			= $this->input->post('password');
		$user_birthdate 	= $this->input->post('tahun_lahir').'-'.$this->input->post('bulan_lahir').'-'.$this->input->post('tanggal_lahir');
		$user_birthplace 	= $this->input->post('user_birthplace');
		$user_gender 		= $this->input->post('user_gender');

		$school_name 		= $this->input->post('school_name');
		$school_address	 	= $this->input->post('school_address');
		$jenjang_id 		= $this->input->post('jenjang_id');

		$where = array(
			'email'			=> $email,
			'username' 		=> $username,
			'first_name' 	=> $first_name,
			'last_name' 	=> $last_name,
			'password' 		=> $password,
			'user_birthdate'=> $user_birthdate,
			'user_birthplace'=> $user_birthplace,
			'user_gender' 	=> $user_gender,
			'school_name' 	=> $school_name,
			'school_address'=> $school_address,
			'jenjang_id' 	=> $jenjang_id
		);
		$reg 				= $this->Kidsmod->reg_kids($where,$id_user);
		if ($reg == 0) {
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message', 'Maaf, gagal mendaftar silahkan coba lagi');
			redirect('Kids/Registration');
		}
		else
		{
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message', 'Pendaftaran anak berhasil.');
			redirect('Kids/Registration');
		}
	}
	public function registerkidsMobile(){
		$id_user 			= $this->session->userdata('id_user');
		$username 			= $this->input->post('username');
		$first_name 		= $this->input->post('first_name');
		$last_name 			= $this->input->post('last_name');
		$password 			= $this->input->post('password');
		$user_birthdate 	= $this->input->post('tahun_lahir').'-'.$this->input->post('bulan_lahir').'-'.$this->input->post('tanggal_lahir');
		$user_birthplace 	= $this->input->post('user_birthplace');
		$user_gender 		= $this->input->post('user_gender');

		$school_name 		= $this->input->post('school_name');
		$school_address	 	= $this->input->post('school_address');
		$jenjang_id 		= $this->input->post('jenjang_id');

		$where = array(
			'username' 		=> $username,
			'first_name' 	=> $first_name,
			'last_name' 	=> $last_name,
			'password' 		=> $password,
			'user_birthdate'=> $user_birthdate,
			'user_birthplace'=> $user_birthplace,
			'user_gender' 	=> $user_gender,
			'school_name' 	=> $school_name,
			'school_address'=> $school_address,
			'jenjang_id' 	=> $jenjang_id
		);
		$reg 				= $this->Kidsmod->reg_kids($where,$id_user);
		if ($reg == 0) {
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message', 'Maaf, gagal mendaftar silahkan coba lagi');
			$this->session->set_userdata('code', '342');
			redirect('Kids/Registration');
		}
		else
		{
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message', 'Pendaftaran anak berhasil.');
			// echo 'Pendaftaran anak berhasil.';
			// return false;
			$this->session->set_userdata('code', '343');
			redirect('Kids/Registration');
		}
	}

	public function session_check()
	{
		if($this->session->has_userdata('id_user')){
			$new_state = $this->db->query("SELECT status FROM tbl_user where id_user='".$this->session->userdata('id_user')."'")->row_array()['status'];
			if($this->session->userdata('status') != $new_state ){
				$this->session->sess_destroy();
				redirect('/');
			}
			// $this->sesi['user_name'] = $this->session->userdata('user_name');
			// $this->sesi['username'] = $this->session->userdata('username');
			// $this->sesi['priv_level'] = $this->session->userdata('priv_level');
			if($this->session->userdata('usertype_id') != "student kid" || $this->session->userdata('usertype_id') != "student"){
				redirect('/');
			}
			return 1;
		}else{
			return 0;
		}
	}

	public function load_lang($lang = '')
	{
		$this->lang->load('umum',$lang);
	}

	public function set_lang($value='')
	{	
		if($value == ""){
			$this->session->set_userdata('lang','indonesia');
		}else{
			$this->session->set_userdata('lang',$value);
		}		
		
		return null;
	}

}