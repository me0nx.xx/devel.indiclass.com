<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $sesi = array("lang" => "indonesia");	

	public function __construct()
	{
		parent::__construct();
        $this->load_lang($this->session->userdata('lang'));        
        $this->load->database();
		$this->load->helper(array('url'));       
		// $this->load->model('Master_model'); 
	}

	public function index()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "tutor_approval" || $usertype_id == "admin" || $usertype_id == "assessor" || $usertype_id == "evaluator") {		
				$data['sideactive'] = "homeadmin";
				$this->load->view('inc/header');
				$this->load->view('admin/index', $data);
			}
			else
			{
				redirect('/admin');
				
			}
		}
		else{
			redirect('/login');
		}
	}
	public function resendEmail()
	{
		if ($this->session_check() == 1) {		
			$data['sideactive'] = "resend_email";
			$this->load->view('inc/header');
			$this->load->view('admin/resend_email', $data);
		}
		else{
			redirect('/login');
		}
	}

	public function tutor_approval()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "tutor_approval" || $usertype_id == "assessor") {							
				$data['sideactive'] = "approval_tutor";
				$data['profile'] = $this->Master_model->getTblUserAndProfileTutor();
				$data['alldatatutor'] = $this->Master_model->getPendingApproval();
				$this->load->view('inc/header');
				$this->load->view('admin/tutor_approval', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else{
			redirect('/login');
		}
	}
	public function rescheduleClass()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "reschedule";
				$data['profile'] = $this->Master_model->getTblUserAndProfileTutor();
				$data['alldatatutor'] = $this->Master_model->getAllTutor();
				$this->load->view('inc/header');
				$this->load->view('admin/rescheduleClass', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else{
			redirect('/login');
		}
	}

	public function allTutor()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "alltutor";
				$data['profile'] = $this->Master_model->getTblUserAndProfileTutor();
				$data['alldatatutor'] = $this->Master_model->getAllTutor();
				$this->load->view('inc/header');
				$this->load->view('admin/alltutor', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else{
			redirect('/login');
		}
	}

	public function ApprovalKuota()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "kuotakelas";
				$this->load->view('inc/header');
				$this->load->view('admin/kuotakelas', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else{
			redirect('/login');
		}
	}

	public function kuotaKelas()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "tambahkuotakelas";
				$this->load->view('inc/header');
				$this->load->view('admin/tambah_kuotakelas', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else{
			redirect('/login');
		}
	}
	
	public function pendingApproval(){

	}

	public function Engine()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "enggineadmin";
				$this->load->view('inc/header');
				$this->load->view('admin/engine', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function Data_transaction()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "datauangsaku";
				$this->load->view('inc/header');
				$this->load->view('admin/data_uangsaku', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}	

	public function Confrim_transaction()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "confrimuangsaku";
				$this->load->view('inc/header');
				$this->load->view('admin/confrim_uangsaku', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function Support()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "support";
				$this->load->view('inc/header');
				$this->load->view('admin/support', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function Register()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "registerchannel";
				$this->load->view('inc/header');
				$this->load->view('admin/registerchannel', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function PaymentMulticast()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "paymentmulticast";
				$this->load->view('inc/header');
				$this->load->view('admin/payment_multicast', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function PaymentPrivate()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "paymentprivate";
				$this->load->view('inc/header');
				$this->load->view('admin/payment_private', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function PaymentGroup()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "paymentgroup";
				$this->load->view('inc/header');
				$this->load->view('admin/payment_group', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function ReferralEdit()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "addreferral";
				$this->load->view('inc/header');
				$this->load->view('admin/edit_referral', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function addReferral()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "addreferral";
				$this->load->view('inc/header');
				$this->load->view('admin/add_referral', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function useReferral()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "usereferral";
				$this->load->view('inc/header');
				$this->load->view('admin/use_referral', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function Subjectadd()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "addsubject";
				$this->load->view('inc/header');
				$this->load->view('admin/add_subject', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function SubjectEdit()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "addsubject";
				$this->load->view('inc/header');
				$this->load->view('admin/edit_subject', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function EditChannel()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "registerchannel";
				$this->load->view('inc/header');
				$this->load->view('admin/EditChannel', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function PointChannel()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "pointchannel";
				$this->load->view('inc/header');
				$this->load->view('admin/PointChannel', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}
	public function QuotaChannel()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "quotachannel";
				$this->load->view('inc/header');
				$this->load->view('admin/QuotaChannel', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function UsePointChannel()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "usepointchannel";
				$this->load->view('inc/header');
				$this->load->view('admin/UsePointChannel', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function ActivityChannel()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "activitychannel";
				$this->load->view('inc/header');
				$this->load->view('admin/activityChannel', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}
	
	public function Announcement()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "admin") {
				$data['sideactive'] = "announcement";
				$this->load->view('inc/header');
				$this->load->view('admin/announ', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function getSubjectTutor()
    {
    	$tutor_id 	= $this->input->post('tutor_id');
        $getBooking = $this->db->query("SELECT tb.*, ms.subject_name, mj.jenjang_name, mj.jenjang_level FROM tbl_booking as tb INNER JOIN (master_subject as ms INNER JOIN master_jenjang as mj ON ms.jenjang_id=mj.jenjang_id) ON tb.subject_id=ms.subject_id WHERE tb.id_user='$tutor_id' ORDER BY datetime DESC")->result_array();
        if (empty($getBooking)) {
        	$json = array("status" => -1,"message" => "Gagal", "statuz" => false);
        }
        else
        {	        
	        $json = array("status" => 1,"message" => "Berhasil bro", "data" => $getBooking, "statuz" => true);	        
        }
        echo json_encode($json);
    }

    public function Evaluator()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "evaluator") {
				$data['sideactive'] = "evaluator_tutor";
				$data['alldatatutor'] = $this->Master_model->getAllTutor();
				$this->load->view('inc/header');
				$this->load->view('admin/Evaluator', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function ViewEvaluator()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "evaluator") {
				$data['sideactive'] = "evaluator_tutor";
				$data['alldatatutor'] = $this->Master_model->getAllTutor();
				$this->load->view('inc/header');
				$this->load->view('admin/ViewEvaluator', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function Assessor()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "assessor") {
				$data['sideactive'] = "assessor";				
				$this->load->view('inc/header');
				$this->load->view('admin/Assessor', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function UnAssessor()
	{
		if ($this->session_check() == 1) {
			$usertype_id 	= $this->session->userdata('usertype_id');
			if ($usertype_id == "assessor") {
				$data['sideactive'] = "unassessor";				
				$this->load->view('inc/header');
				$this->load->view('admin/UnAssessor', $data);
			}
			else
			{
				redirect('/admin');
			}
		}
		else
		{
			redirect('/login');
		}
	}

	public function load_lang($lang = '')
	{
		$this->lang->load('umum',$lang);
	}

	public function session_check()
	{
		if($this->session->has_userdata('id_user')){
			// $this->sesi['user_name'] = $this->session->userdata('user_name');
			// $this->sesi['username'] = $this->session->userdata('username');
			// $this->sesi['priv_level'] = $this->session->userdata('priv_level');
			return 1;
		}else{
			return 0;
		}
	}

	public function approve_support()
	{
		$idrequest = $this->input->post('idr');
		// $idtutor = $_GET['idttr'];
		// $idrequest = $this->input->post('idrequest');
		// $idtutor = $this->input->post('idttr');
		// $tutorname = $this->db->query("SELECT * FROM tbl_user WHERE id_user='$idtutor'")->row_array();
		$request_data = $this->db->query("SELECT tsup.*, tu.user_name FROM tbl_support as tsup INNER JOIN tbl_user as tu ON tsup.id_tutor=tu.id_user WHERE tsup.requestid='$idrequest';")->row_array();
		if(!empty($request_data)){
			$name = $request_data['user_name'];
			$idtutor = $request_data['id_tutor'];

			$update = $this->db->query("UPDATE tbl_support SET status=1 WHERE requestid='$idrequest'");
			if ($update) {
				$notif_message = json_encode( array("code" => 41, "tutor_name" => $name) );
				$base64 = $this->encryption->encrypt(json_encode(array("tutor_id" => $idtutor, "requestid"=> $idrequest, "iat" => time())));
				$enc = strtr($base64, '+/=', '-_.');
				$link = base_url()."master/room_tester/".$enc;
				$addnotifadmin = $this->db->query("INSERT INTO tbl_notif 
					(notif, notif_type, id_user, link, read_status) VALUES 
					('$notif_message','single','$idtutor','$link','0')");

				$addlinktoadmin = $this->db->query("UPDATE tbl_support SET link='$link' WHERE requestid='$idrequest'");
				// $this->session->set_userdata('checksupport', 'penuh');
				$data['requestid'] = $idrequest;
				$data['status'] = true;
				echo json_encode($data);
				// redirect('admin/support');
			}
			else
			{
				$data['requestid'] = $idrequest;
				$data['status'] = false;
				echo json_encode($data);
				return false;
			}
		}
	}
	public function approve_vts($value='')
	{
		$booking_id = $this->input->get('booking_id');
		// $id_user = $this->input->get('id_user');
		// $subject_id = $this->input->get('subject_id');

		$exist = $this->db->query("SELECT * FROM tbl_booking WHERE uid='$booking_id'")->row_array();
		if(empty($exist)){
			$data['status'] = false;
			$data['message'] = 'data not found';
		}else{
			if($this->db->simple_query("UPDATE tbl_booking SET status='verified' WHERE uid='$booking_id'") )
			{
				$user_tutor = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='".$exist['id_user']."'")->row_array();
				$subject = $this->db->query("SELECT * FROM master_subject WHERE subject_id='".$exist['subject_id']."'")->row_array();
				$booked = $this->db->query("SELECT * FROM tbl_booking WHERE subject_id='".$exist['subject_id']."' AND id_user='".$exist['id_user']."' ")->row_array();
				$selectnew = $this->db->query("SELECT * FROM master_jenjang WHERE jenjang_id='".$subject['jenjang_id']."'")->row_array();
	 
				$subject_name = $subject['subject_name'];
				// $subject_name = "Matematika";
				$user_nametutor = $user_tutor['user_name'];
				$user_email 	= $user_tutor['email'];
				$config = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'ssl://smtp.googlemail.com',
					'smtp_port' => 465,
					'smtp_user' =>'info@classmiles.com',
					'smtp_pass' => 'kerjadiCLASSMILES',	
					'mailtype' => 'html',	
					// 'wordwrap' => true,
					'charset' => 'iso-8859-1'
				);

				$emailkedua = "hermawan@meetaza.com";
				$emailketiga = "support@classmiles.com";
				// $emailketiga = "farid@cleva.id";
				// $emailkedua = "faridsetiawan96@gmail.com";
				$this->load->library('email', $config);
				$this->email->initialize($config);
				$this->email->set_mailtype("html");
				$this->email->set_newline("\r\n");
				$this->email->from('info@classmiles.com', 'Classmiles');
				$this->email->to($emailkedua." , ". $emailketiga);
				if ($selectnew['jenjang_level'] == "0") {
					$this->email->subject($user_nametutor." (".$user_email.") telah disetujui mengajar ".$subject_name." - ".$jenjangname[] = $selectnew['jenjang_name']." di Classmiles oleh Admin");		
				}
				else{
					$this->email->subject($user_nametutor." (".$user_email.") telah disetujui mengajar ".$subject_name." ".$jenjanglevel[] = $selectnew['jenjang_level']." - ".$jenjangname[] = $selectnew['jenjang_name']." di Classmiles oleh Admin");		
				}
				$templateverifikasi = 
				"
				<div style='height:100%;margin:0;padding:0;width:100%;background-color:#fafafa'>
				    <center>
				        <table align='center' border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' style='border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#fafafa'>
				            <tbody>
				                <tr>
				                    <td align='center' valign='top' style='height:100%;margin:0;padding:10px;width:100%;border-top:0'>
				                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='border-collapse:collapse;border:0;max-width:600px!important'>
				                            <tbody>
				                                <tr>
				                                    <td valign='top' style='background:#ffffff none no-repeat center/cover;background-color:#ffffff;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:0'>
				                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
				                                            <tbody>
				                                                <tr>
				                                                    <td valign='top'>
				                                                        <table align='left' width='100%' border='0' cellpadding='0' cellspacing='0' style='min-width:100%;border-collapse:collapse'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' style='text-align:center'>
				                                                                        <img align='center' alt='' src='".CDN_URL.STATIC_IMAGE_CDN_URL."maillogo_classmiles.png' width='100%' style='max-width:700px;padding-bottom:0;display:inline!important;vertical-align:bottom;border:0;height:auto;outline:none;text-decoration:none'>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                    </td>
				                                                </tr>
				                                            </tbody>
				                                        </table>
				                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
				                                            <tbody>
				                                                <tr>
				                                                    <td valign='top' style='padding-top:9px'>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left'>
				                                                                        <p style='margin:10px 0;padding:0;color:#202020;font-family:Helvetica;font-size:16px;line-height:150%;text-align:left'>
				                                                                            Hello,<br><br>
				                                                                            ".$user_nametutor." (".$user_email.") <b style='color: green'>telah disetujui </b> mengajar ".$subject_name." - ".$jenjangname[] = $selectnew['jenjang_name']."  di Classmiles oleh Admin.
				                                                                            <br>
				                                                                        </p>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse; margin-top: 15px;' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' class='m_2872471405169675079mcnTextContent' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
				                                                                        <em>Thank you,</em><br>
				                                                                        <em>Classmiles Team</em>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse; margin-top: 15px;' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' class='m_2872471405169675079mcnTextContent' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
				                                                                        <hr>
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                    </td>
				                                                </tr>
				                                            </tbody>
				                                        </table>
				                                    </td>
				                                </tr>
				                                <tr>
				                                    <td valign='top' style='background:#fafafa none no-repeat center/cover;background-color:#fafafa;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:9px'>
				                                        <table border='0' cellpadding='0' cellspacing='0' width='100%' style='min-width:100%;border-collapse:collapse'>
				                                            <tbody>
				                                                <tr>
				                                                    <td valign='top' style='padding-top:9px'>
				                                                        <table align='left' border='0' cellpadding='0' cellspacing='0' style='max-width:100%;min-width:100%;border-collapse:collapse' width='100%'>
				                                                            <tbody>
				                                                                <tr>
				                                                                    <td valign='top' style='padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:center'>
				                                                                        <em>Copyright &copy; 2017 Classmiles, All rights reserved.</em><br>                                                                    
				                                                                    </td>
				                                                                </tr>
				                                                            </tbody>
				                                                        </table>
				                                                    </td>
				                                                </tr>
				                                            </tbody>
				                                        </table>
				                                    </td>
				                                </tr>
				                            </tbody>
				                        </table>
				                    </td>
				                </tr>
				            </tbody>
				        </table>
				    </center>
				</div>
				";	
				$this->email->message($templateverifikasi);

				if ($this->email->send()) 
				{
					$data['status'] = true;
					$data['message'] = 'sucess';	
				}
				else
				{
					$data['status'] = false;
					$data['message'] = 'failure';	
				}
			}
			else
			{
				$data['status'] = false;
				$data['message'] = 'failure';	

			}
		}
		
		echo json_encode($data);
	}

	public function reject_vts($value='')
	{
		$booking_id = $this->input->get('booking_id');

		$exist = $this->db->query("SELECT * FROM tbl_booking WHERE uid='$booking_id'")->row_array();
		if(empty($exist)){
			$data['status'] = false;
			$data['message'] = 'data not found';
		}else{
			if($this->db->simple_query("UPDATE tbl_booking SET status='unverified' WHERE uid='$booking_id'") )
			{
				$data['status'] = true;
				$data['message'] = 'success';
			}
			else
			{
				$data['status'] = false;
				$data['message'] = 'failure';	
			}
		}
		
		echo json_encode($data);
	}
	
	public function checked()
	{
		$idrequest = $this->input->get('idr');
		$querycek = $this->db->query("SELECT * FROM tbl_support WHERE requestid='$idrequest'")->row_array();
		if ($querycek['status'] == 2) {
			$data['status'] = 1;
			$data['link'] = $querycek['link'];
			$data['idrequest'] = $idrequest;
			$data['message'] = 'sucess';
		}
		else
		{
			$data['status'] = 0;
			$data['idrequest'] = $idrequest;
			$data['message'] = 'Tutor tidak menjawab.';	
		}

		echo json_encode($data);
	}

	public function sendaction()
	{
		$idrequest = $this->input->get('idr');
		// $idtutor   = $_GET['idttr'];
		$gantistatus = $this->db->query("UPDATE tbl_support SET status='-1' WHERE requestid='$idrequest'");
		if ($gantistatus) {
			$notif_message = json_encode( array("code" => 46));
			$addnotifadmin = $this->db->query("INSERT INTO tbl_notif (notif, notif_type, id_user, link, read_status) VALUES ('$notif_message','single','$idtutor','','0')");

			// $deleteidrequest = $this->db->query("DELETE FROM tbl_support WHERE requestid='$idrequest'");

			$data['status'] = 1;
		}
		else
		{
			$data['status'] = 0;
		}
		echo json_encode($data);
	}

	public function editannoun()
	{
		if ($this->session_check() == 1) {
			$data['sideactive'] = "announcement";
			$this->load->view('inc/header');
			$this->load->view('admin/edit_announ', $data);
		}
		else
		{
			redirect('/login');
		}
	}

	public function cobacoba(){
		$folder 			= './aset/img/user/';
		// $file_sizeweb		= $_FILES['iconweb']['size'];
		$file_sizeandroid	= $_FILES['iconandroid']['size'];
		$max_size			= 2000000;
		// $file_nameweb		= $subject_name.'.png';
		$file_nameandroid	= 'd_android'.'.png';

		if($file_sizeandroid > $max_size){								
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Ukuran file melebihi batas maximum 2MB');
			redirect('admin/Subjectadd');
		}
		echo $file_sizeandroid;
		echo "<br>";
		echo $max_size;
		if ($file_sizeandroid >= $max_size) {
			echo "terlalu besar";
		}
		else
		{
			echo "lanjut bro";
		}
		return false;
	}

	public function add_subject()
	{
		$subject_name 			= $this->input->post('subject_name');
		$jenjang_id 			= json_encode($this->input->post('jenjang_id'));
		$subject_name_indonesia = $this->input->post('subject_name_indonesia');
		$subject_name_inggris 	= $this->input->post('subject_name_inggris');
		// $iconweb 			= $subject_name.'.png';
		$iconandroid 		= str_replace(' ', '_', $subject_name).'_android'.'.png';
		$where = array(
			'subject_name' => $subject_name,
			'jenjang_id' => $jenjang_id,
			'subject_name_indonesia' => $subject_name_indonesia,
			'subject_name_inggris' => $subject_name_inggris,
			'icon' => $iconandroid 
		);

		$savesubject = $this->Process_model->addsubject("master_subject", $where);

		if ($savesubject == 0) {
			$load = 'Maaf, gagal menambah Subject. Mohon periksa kembali';
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message',$load);
			redirect('admin/Subjectadd');
		}
		else if ($savesubject == 3) {
			$load = 'Maaf, gagal menambah Subject. Subject sudah ada.';
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message',$load);
			redirect('admin/Subjectadd');
		}
		else
		{
			$folder 			= './aset/_temp_images/';
			// $file_sizeweb		= $_FILES['iconweb']['size'];
			$file_sizeandroid	= $_FILES['iconandroid']['size'];
			$max_size			= 2000000;
			// $file_nameweb		= $subject_name.'.png';
			$file_nameandroid	= str_replace(' ', '_', $subject_name).'_android'.'.png';

			if($file_sizeandroid > $max_size){								
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message','Ukuran file melebihi batas maximum 2MB');
				redirect('admin/Subjectadd');
			}
			else
			{
				if (move_uploaded_file($_FILES['iconandroid']['tmp_name'], $folder.$file_nameandroid)) {

					$config['image_library'] = 'gd2';
					// $config['source_image']	= "./aset/img/class_icon/".$file_nameweb;
					$config['source_image']	= $folder.$file_nameandroid;
					$config['width']	= 250;
					$config['maintain_ratio'] = FALSE;
					$config['height']	= 250;

					$this->image_lib->initialize($config); 

					$this->image_lib->resize();
					$this->Rumus->sendToCDN('class_icon', $file_nameandroid, null, 'aset/_temp_images/'.$file_nameandroid );

					$this->session->set_flashdata('mes_alert','success');
					$this->session->set_flashdata('mes_display','block');
					$this->session->set_flashdata('mes_message',"Subject Berhasil di Tambah");	
					redirect('admin/Subjectadd');
				}
			}
		}
	}

	public function edtsubject()
	{
		$subject_id 	= $this->input->post('subject_id');
    	$subject_name 	= $this->input->post('subject_name');
    	$jenjang_id 	= json_encode($this->input->post('jenjang_id'));
    	
    	$subject_name_indonesia = $this->input->post('subject_name_indonesia');
		$subject_name_inggris 	= $this->input->post('subject_name_inggris');
    	$fotoicon   	= $this->input->post('fotoiconlama');

    	$where = array(
    		'subject_id' => $subject_id,
    		'subject_name' => $subject_name,
    		'jenjang_id' => $jenjang_id,
    		'subject_name_indonesia' => $subject_name_indonesia,
			'subject_name_inggris' => $subject_name_inggris,
    		'fotoicon' => $fotoicon
    	);

    	$edtsubject = $this->Process_model->edtsubject("master_subject", $where);

    	if ($edtsubject == 0) {
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message',"Gagal mengubah data Subject");
			redirect('admin/Subjectadd');
    	}
    	else if ($edtsubject == 1)
    	{    		
    		if ($_FILES['fotoiconbaru']['name'] == "") {
    			$this->session->set_flashdata('mes_alert','success');
				$this->session->set_flashdata('mes_display','block');
				$this->session->set_flashdata('mes_message',"Perubahan data berhasil.");	
				redirect('admin/Subjectadd');
    		}
    		else
    		{
		  //   	$folder 	= './aset/img/class_icon/';
				// $file_size	= $_FILES['fotoiconbaru']['size'];
				// $max_size	= 2000000;
				// $file_name	= $fotoicon;

				// if($file_size > $max_size){								
				// 	$this->session->set_flashdata('mes_alert','danger');
				// 	$this->session->set_flashdata('mes_display','block');
				// 	$this->session->set_flashdata('mes_message','Ukuran file melebihi batas maximum 2MB');
				// 	redirect('admin/Subjectadd');
				// }
				// else
				// {
				// 	if (move_uploaded_file($_FILES['fotoiconbaru']['tmp_name'], $folder.$file_name)) {
				// 		$config['image_library'] = 'gd2';
				// 		$config['source_image']	= "./aset/img/class_icon/".$file_name;
				// 		$config['width']	= 250;
				// 		$config['maintain_ratio'] = FALSE;
				// 		$config['height']	= 250;

				// 		$this->image_lib->initialize($config); 

				// 		$this->image_lib->resize();

				// 		$this->session->set_flashdata('mes_alert','success');
				// 		$this->session->set_flashdata('mes_display','block');
				// 		$this->session->set_flashdata('mes_message',"Perubahan data Subject berhasil.");	
				// 		redirect('admin/Subjectadd');
				// 	}
				// }
				$folder 			= './aset/_temp_images/';
				// $file_sizeweb		= $_FILES['iconweb']['size'];
				$file_sizeandroid	= $_FILES['fotoiconbaru']['size'];
				$max_size			= 2000000;
				// $file_nameweb		= $subject_name.'.png';
				$file_nameandroid	= $fotoicon;

				if($file_sizeandroid > $max_size){								
					$this->session->set_flashdata('mes_alert','danger');
					$this->session->set_flashdata('mes_display','block');
					$this->session->set_flashdata('mes_message','Ukuran file melebihi batas maximum 2MB');
					redirect('admin/Subjectadd');
				}
				else
				{
					if (move_uploaded_file($_FILES['fotoiconbaru']['tmp_name'], $folder.$file_nameandroid)) {

						$config['image_library'] = 'gd2';
						// $config['source_image']	= "./aset/img/class_icon/".$file_nameweb;
						$config['source_image']	= $folder.$file_nameandroid;
						$config['width']	= 250;
						$config['maintain_ratio'] = FALSE;
						$config['height']	= 250;

						$this->image_lib->initialize($config); 

						$this->image_lib->resize();
						$this->Rumus->sendToCDN('class_icon', $file_nameandroid, null, 'aset/_temp_images/'.$file_nameandroid );

						$this->session->set_flashdata('mes_alert','success');
						$this->session->set_flashdata('mes_display','block');
						$this->session->set_flashdata('mes_message',"Subject Berhasil di Ubah");	
						redirect('admin/Subjectadd');
					}
				}

			}
		}

	}

	public function add_referral()
	{
		$referral_code 		= $this->input->post('referral_code');
		$start_datetime 	= $this->input->post('start_datetime');
		$finish_datetime 	= $this->input->post('finish_datetime');
		$description 		= $this->input->post('description');
		$type 				= $this->input->post('type');

		$where = array(
    		'referral_code' => $referral_code,
    		'start_datetime' => $start_datetime,
    		'finish_datetime' => $finish_datetime,
    		'description' => $description,
			'type' => $type
    	);

    	$addreferral = $this->Process_model->addreferral($where);

    	if ($addreferral == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message',"Referral Berhasil di Tambah");	
			redirect('admin/addReferral');
    	}
    	else
    	{
    		$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message',"Gagal menambah Referral");
			redirect('admin/addReferral');
    	}
	}

	public function edtreferral()
	{
		$referral_id 		= $this->input->post('referral_id');
		$referral_code 		= $this->input->post('referral_code');
		$start_datetime 	= $this->input->post('start_datetime');
		$finish_datetime 	= $this->input->post('finish_datetime');
		$description 		= $this->input->post('description');
		$type 				= $this->input->post('type');

    	$where = array(
    		'referral_id' => $referral_id,
    		'referral_code' => $referral_code,
    		'start_datetime' => $start_datetime,
    		'end_datetime' => $finish_datetime,
    		'description' => $description,
			'type' => $type
    	);

    	$edtsubject = $this->Process_model->edtreferral($where);

    	if ($edtsubject == 1) {
			
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message',"Perubahan data Referral berhasil.");	
			redirect('admin/addReferral');
    	}
    	else
    	{    		
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message',"Gagal mengubah data Referral");
			redirect('admin/addReferral');		
		}
	}

	public function deletereferral()
	{
		$rtp = $this->input->post('rtp');
		$where = array(
			'referral_id' => $rtp
		);

		$delete = $this->Process_model->referraldelete($where);
		if ($delete == 1) {
			$this->session->set_flashdata('mes_alert','success');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Berhasil menghapus Referral');
			// redirect("Channel/student_channel");
		}
		else
		{
			$this->session->set_flashdata('mes_alert','danger');
			$this->session->set_flashdata('mes_display','block');
			$this->session->set_flashdata('mes_message','Gagal menghapus Referral');
			// redirect("Channel/student_channel");
		}
	}
}
