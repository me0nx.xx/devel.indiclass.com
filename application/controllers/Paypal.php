<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paypal extends CI_Controller 
{
     function  __construct(){
        parent::__construct();
        $this->load->library('paypal_lib');
        $this->load->model('product');
        $this->load->library('email');
     }
     
    function success(){
        // Get the transaction data
        $paypalInfo = $this->input->get();
        
        $data['item_number'] = $paypalInfo['item_number']; 
        $data['txn_id'] = $paypalInfo["tx"];
        $data['payment_amt'] = $paypalInfo["amt"];
        $data['currency_code'] = $paypalInfo["cc"];
        $data['status'] = $paypalInfo["st"];

        $id_user        = $paypalInfo['cm'];
        $item_number    = $paypalInfo['item_number'];
        $txn_id         = $paypalInfo["tx"];
        $payment_amt    = $paypalInfo["amt"];
        $currency_code  = $paypalInfo["cc"];
        $status         = $paypalInfo["st"];
        $em             = $paypalInfo["item_name"];
        $product_id     = preg_replace("/[^0-9]/", '', $em);
        if ($status == "Completed") {
            $this->db->query("UPDATE tbl_order SET order_status=1 WHERE order_id='$product_id'");
            $updatemethod = $this->db->query("UPDATE tbl_invoice SET payment_type='Paypal' WHERE order_id='$product_id'");

            $insert = $this->db->query("INSERT INTO payments (user_id, product_id, txn_id, payment_gross, currency_code, payer_email, payment_status) VALUES ('$id_user','$product_id','$txn_id','$payment_amt','$currency_code','$em','$status')");
            if ($insert) {
                
                $cekors = $this->db->query("SELECT order_status FROM tbl_order WHERE order_id = '$product_id' ")->row_array()['order_status'];
                if ($cekors == 1) {
                            
                    $request_id      = $this->db->query("SELECT product_id FROM tbl_order_detail WHERE order_id = '$product_id'")->row_array()['product_id'];
                    $sub_productid  = substr($request_id,0,1); 
                    $type           = $sub_productid == '7' ? 'private' : ( $sub_productid == '8' ? 'group': 'multicast' );

                    $get_datadetail = $this->db->query("SELECT * FROM tbl_order as tor INNER JOIN tbl_invoice as ti ON tor.order_id=ti.order_id WHERE tor.order_id='$product_id'")->row_array();
                    $invoice_id     = $get_datadetail['invoice_id'];
                    $total_price    = $get_datadetail['total_price'];
                    $UnixCode       = $get_datadetail['unix_code'];
                    $hargatotal     = $total_price+$UnixCode;
                    
                    if ($type == 'private') {
                        $tutor_id       = $this->db->query("SELECT tutor_id FROM tbl_request WHERE request_id = '$request_id'")->row_array()['tutor_id'];
                        $q = $this->db->query("SELECT * FROM tbl_request as tr INNER JOIN tbl_service_price as tsp ON tr.subject_id=tsp.subject_id WHERE tsp.class_type='private' AND tsp.tutor_id='$tutor_id' AND tr.request_id='$request_id'")->row_array();
                        $id_user            = $q['id_user_requester'];
                        $subject_id         = $q['subject_id'];
                        $subject_name       = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
                        $topic              = $q['topic'];
                        $tutorname          = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutor_id'")->row_array()['user_name'];
                        $hargakelas         = number_format($q['harga_cm'], 0, ".", ".");
                        
                        $uangtutor          = $this->db->query("SELECT * FROM uangsaku WHERE id_user=$tutor_id")->row_array();  
                        $balance_tutor      = $uangtutor['active_balance'];
                        $uangsaku_idtutor   = $uangtutor['us_id'];
                        $durasi             = $q['duration_requested'];
                        $price              = $q['harga'];
                        $hargaakhir         = ($durasi/900)*$price;
                        $pricecm            = $q['harga_cm'];                
                        $template           = $q['template'];
                        $new_balancetutor   = $balance_tutor+$hargaakhir;
                        $this->db->simple_query("UPDATE uangsaku SET active_balance=$new_balancetutor WHERE us_id='$uangsaku_idtutor'");

                        // CREATE NEW CLASS
                        $duration = $q['duration_requested'];
                        $start_st = $q['date_requested'];
                        $final_st = $q['date_requested'];
                        $final_ft = DateTime::createFromFormat('Y-m-d H:i:s', $final_st);
                        $final_ft->modify("+$duration second");
                        $final_ft = $final_ft->format('Y-m-d H:i:s');

                        $participant['visible'] = "private";
                        $participant['participant'] = array();
                        $participant['participant'][]['id_user'] = $id_user;

                        $participant = json_encode($participant);
                        $this->db->simple_query("INSERT INTO tbl_class(subject_id,name,description,tutor_id,start_time,finish_time,participant,class_type,template_type) VALUES('{$subject_id}','{$subject_name}','{$topic}','{$tutor_id}','{$start_st}','{$final_ft}','{$participant}','private','{$template}')");
                        $class_id            = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];

                        $this->db->simple_query("INSERT INTO tbl_class_rating(class_id, id_user, class_ack) VALUES('$class_id','$id_user',-1)");

                        $getdatauser        = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$id_user'")->row_array();
                        if (empty($getdatauser['email'])) {
                            $getparentid    = $this->db->query("SELECT * FROM tbl_profile_kid WHERE kid_id='$id_user'")->row_array();
                            $getdatauser    = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$getparentid[parent_id]'")->row_array();
                        }
                        $emailuser          = $getdatauser['email'];
                        $usernameuser       = $getdatauser['user_name'];
                        
                        $user_utc           = $this->session->userdata('user_utc');
                        $server_utcc        = $this->Rumus->getGMTOffset();
                        $intervall          = $user_utc - $server_utcc;
                        $starttime          = DateTime::createFromFormat ('Y-m-d H:i:s',$start_st);
                        $starttime->modify("+".$intervall ." minutes");
                        $starttime          = $starttime->format('Y-m-d H:i:s');
                        $finishtime         = DateTime::createFromFormat ('Y-m-d H:i:s',$final_ft);
                        $finishtime->modify("+".$intervall ." minutes");
                        $finishtime         = $finishtime->format('Y-m-d H:i:s');
                        
                        $datelimit          = date_create($starttime);
                        $dateelimit         = date_format($datelimit, 'd/m/y');
                        $harilimit          = date_format($datelimit, 'd');
                        $hari               = date_format($datelimit, 'l');
                        $tahunlimit         = date_format($datelimit, 'Y');
                        $waktulimit         = date_format($datelimit, 'H:i');   
                        $datelimit          = $dateelimit;
                        $sepparatorlimit    = '/';
                        $partslimit         = explode($sepparatorlimit, $datelimit);
                        $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

                        $datelimitt          = date_create($finishtime);
                        $dateelimitt         = date_format($datelimitt, 'd/m/y');
                        $harilimitt          = date_format($datelimitt, 'd');
                        $harit               = date_format($datelimitt, 'l');
                        $tahunlimitt         = date_format($datelimitt, 'Y');
                        $waktulimitt         = date_format($datelimitt, 'H:i');   
                        $datelimitt          = $dateelimitt;
                        $sepparatorlimitt    = '/';
                        $partslimitt         = explode($sepparatorlimitt, $datelimitt);
                        $bulanlimitt         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

                        $seconds            = $duration;
                        $hours              = floor($seconds / 3600);
                        $mins               = floor($seconds / 60 % 60);
                        $secs               = floor($seconds % 60);
                        $durationrequested  = sprintf('%02d:%02d', $hours, $mins);
                        
                        $tanggalkonfirm     = date("Y-m-d H:i:s");
                        $this->db->simple_query("UPDATE tbl_order_detail SET class_id='$class_id' WHERE order_id='$product_id'");
                        $this->db->simple_query("UPDATE tbl_order SET order_status=1, approval_time='$tanggalkonfirm' WHERE order_id='$product_id'");

                        $config1 = Array(
                            'protocol' => 'smtp',
                            'smtp_host' => 'ssl://smtp.googlemail.com',
                            'smtp_port' => 465,
                            'smtp_user' =>'finance@meetaza.com',
                            'smtp_pass' => 'xmbmpuwuxlzmcjsl',  
                            'mailtype' => 'html',   
                            'charset' => 'iso-8859-1'
                        );

                        $this->load->library('email', $config1);
                        $this->email->initialize($config1);
                        $this->email->set_mailtype("html");
                        $this->email->set_newline("\r\n");
                        $this->email->from('finance@meetaza.com', 'Classmiles');
                        $this->email->to($emailuser);
                        $this->email->subject('Classmiles - Private Class Success');
                        $templateToStudent = 
                            "
                            <div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
                                <table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
                                        <tbody>
                                          <tr>        
                                              <td style='padding-left:10px;padding-right:10px'>
                                                  <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:700px' width='100%'>
                                                      <tbody>
                                                        <tr>
                                                            <td style='text-align:center;padding-top:3%'>
                                                                <a href='https://classmiles.com/' style='text-decoration:none' target='_blank'>
                                                                    <img src='".CDN_URL.STATIC_IMAGE_CDN_URL."maillogo_classmiles.png' style='display:block;margin:0;border:0;max-width:700px' width='100%'>
                                                                </a>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
                                                                <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style='padding-right:5%' width='72%'>
                                                                                <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
                                                                                    Dear ".$usernameuser.",
                                                                                </h1>
                                                                                <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
                                                                                    Pembayaran kelas tambahan anda BERHASIL kami terima. Kelas sudah bisa dimulai pada :   
                                                                                </p>                  
                                                                                <div style='border-bottom:2px solid #eeeeee;'>&nbsp;</div>
                                                                                <br>                         
                                                                                <p>                                                      
                                                                                    <table style='width:100%; border: 1px solid gray;
                                                                                        border-collapse: collapse; color: #6C7A89;'>
                                                                                        <tr>
                                                                                            <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>TUTOR</th>      
                                                                                            <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>MATA PELAJARAN</th>
                                                                                            <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>TOPIK</th>
                                                                                            <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>START CLASS</th> 
                                                                                            <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>FINISH CLASS</th>
                                                                                            <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>DURASI KELAS</th>                                                                                        
                                                                                        </tr> 
                                                                                        <tr>
                                                                                            <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$tutorname."</td>
                                                                                            <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$subject_name."</td>
                                                                                            <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$topic."</td>
                                                                                            <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.", Pukul ".$waktulimit."</td>
                                                                                            <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$harit.', '.$harilimitt.' '. $bulanlimitt. ' '.$tahunlimitt.", Pukul ".$waktulimitt."</td>
                                                                                            <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$durationrequested."</td>                                                                                        
                                                                                        </tr>                                                                                   
                                                                                    </table>
                                                                                </p>                                                    
                                                                            </td>
                                                                        </tr>                                                          
                                                                        <tr>
                                                                            <td style='background-color:#ffffff'>
                                                                                <tbody>                                                                          
                                                                                    <tr>
                                                                                        <td style='padding-bottom:20px;line-height:20px'>
                                                                                            <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align='center' style='padding-right:5%' width='100%'>
                                                                                              <p style='color:gray;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
                                                                                              Diharapkan jangan membalas email ini.<br>
                                                                                              Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
                                                                                              <br>Terima kasih,                                           
                                                                                              <br>
                                                                                                Tim Classmiles
                                                                                              </p>                                                        
                                                                                        </td>                                                   
                                                                                    </tr>
                                                                                </tbody>                                               
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
                                                                <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
                                                                    
                                                                </tbody></table>
                                                                <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
                                                                    
                                                                </tbody></table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                  </table>
                                              </td>
                                          </tr>
                                    </tbody>
                                </table>
                            </div>
                            ";
                        /**/                
                        $this->email->message($templateToStudent);          

                        if ($this->email->send()) 
                        {
                            return 1;                            
                        }
                        else
                        {
                            return 0;
                        }

                    }
                    else if($type == "group")
                    {
                        $tutor_id       = $this->db->query("SELECT tutor_id FROM tbl_request_grup WHERE request_id = '$request_id'")->row_array()['tutor_id'];
                        $update = $this->db->query("UPDATE tbl_request_grup SET approve=1 WHERE request_id='$request_id'");
                        if ($update) {                            
                            $q = $this->db->query("SELECT * FROM tbl_request_grup as tr INNER JOIN tbl_service_price as tsp ON tr.subject_id=tsp.subject_id WHERE tsp.class_type='group' AND tsp.tutor_id='$tutor_id' AND tr.request_id='$request_id'")->row_array();
                            $id_user            = $q['id_user_requester'];
                            $subject_id         = $q['subject_id'];
                            $tutorid            = $q['tutor_id'];
                            $subject_name       = $this->db->query("SELECT subject_name FROM master_subject WHERE subject_id='$subject_id'")->row_array()['subject_name'];
                            $topic              = $q['topic'];
                            $tutorname          = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$tutorid'")->row_array()['user_name'];
                            $hargakelas         = number_format($q['harga_cm'], 0, ".", ".");            
                            $list_student       = json_decode($q['id_friends'],true);
                            $uangtutor          = $this->db->query("SELECT * FROM uangsaku WHERE id_user='$tutorid' ")->row_array();
                            $balance_tutor      = $uangtutor['active_balance'];
                            $uangsaku_idtutor   = $uangtutor['us_id'];
                            $durasi             = $q['duration_requested'];
                            $price              = $q['harga'];
                            $hargaakhir         = ($durasi/900)*$price;
                            $pricecm            = $q['harga_cm'];                
                            $template           = $q['template'];
                            $price              = ($q['duration_requested']/900)*$price;
                            $new_balancetutor   = $balance_tutor+$hargaakhir;
                            $this->db->simple_query("UPDATE uangsaku SET active_balance=$new_balancetutor WHERE us_id='$uangsaku_idtutor'");

                            // CREATE NEW CLASS
                            $duration           = $q['duration_requested'];
                            $start_st           = $q['date_requested'];
                            $final_st           = $q['date_requested'];
                            $final_ft           = DateTime::createFromFormat('Y-m-d H:i:s', $final_st);
                            $final_ft->modify("+$duration second");
                            $final_ft           = $final_ft->format('Y-m-d H:i:s');

                            $participant['visible'] = "private";
                            $participant['participant'] = array();                   
                            foreach($list_student['id_friends'] as $p) {
                                $participant['participant'][]['id_user'] = $p['id_user'];           
                            }
                            
                            $rating_participant = $participant;
                            $participant = json_encode($participant);
                            $this->db->simple_query("INSERT INTO tbl_class(subject_id,name,description,tutor_id,start_time,finish_time,participant,class_type,template_type) VALUES('{$subject_id}','{$subject_name}','{$topic}','{$tutor_id}','{$start_st}','{$final_ft}','{$participant}','group','{$template}')");
                            $class_id = $this->db->query("SELECT LAST_INSERT_ID()")->row_array()['LAST_INSERT_ID()'];

                            foreach ($rating_participant['participant'] as $key => $value) {
                                $std_id = $value['id_user'];
                                $this->db->simple_query("INSERT INTO tbl_class_rating(class_id, id_user, class_ack) VALUES('$class_id','$std_id',-1)");

                                ////------------ BUAT NGIRIM EVENT FIREBASE --------------//
                                $json_notif = "";
                                $fcm_token = $this->db->query("SELECT fcm_token FROM master_fcm where id_user='{$std_id}' && LEFT(fcm_token,6) != 'kosong'")->row_array();
                                if(!empty($fcm_token) && $fcm_token['fcm_token'] != ''){
                                    $json_notif = array("to" => $fcm_token['fcm_token'], "data" => array("data" => array("code" => "10","title" => "Classmiles", "text" => "")));
                                }
                                if($json_notif != ""){
                                    $headers = array(
                                        'Authorization: key=' . FIREBASE_API_KEY,
                                        'Content-Type: application/json'
                                        );
                                    // Open connection
                                    $ch = curl_init();

                                    // Set the url, number of POST vars, POST data
                                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                                    curl_setopt($ch, CURLOPT_POST, true);
                                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_notif));

                                    // Execute post
                                    $result = curl_exec($ch);
                                    curl_close($ch);
                                    // echo $result;
                                }
                                ////------------ END -------------------------------------//
                            }

                            $getdatauser        = $this->db->query("SELECT user_name,email FROM tbl_user WHERE id_user='$id_user'")->row_array();
                            $emailuser          = $getdatauser['email'];
                            $usernameuser       = $getdatauser['user_name'];

                            $user_utc           = $this->session->userdata('user_utc');
                            $server_utcc        = $this->Rumus->getGMTOffset();
                            $intervall          = $user_utc - $server_utcc;
                            $starttime          = DateTime::createFromFormat ('Y-m-d H:i:s',$start_st);
                            $starttime->modify("+".$intervall ." minutes");
                            $starttime          = $starttime->format('Y-m-d H:i:s');
                            $finishtime         = DateTime::createFromFormat ('Y-m-d H:i:s',$final_ft);
                            $finishtime->modify("+".$intervall ." minutes");
                            $finishtime         = $finishtime->format('Y-m-d H:i:s');

                            $datelimit          = date_create($starttime);
                            $dateelimit         = date_format($datelimit, 'd/m/y');
                            $harilimit          = date_format($datelimit, 'd');
                            $hari               = date_format($datelimit, 'l');
                            $tahunlimit         = date_format($datelimit, 'Y');
                            $waktulimit         = date_format($datelimit, 'H:i');   
                            $datelimit          = $dateelimit;
                            $sepparatorlimit    = '/';
                            $partslimit         = explode($sepparatorlimit, $datelimit);
                            $bulanlimit         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

                            $datelimitt          = date_create($finishtime);
                            $dateelimitt         = date_format($datelimitt, 'd/m/y');
                            $harilimitt          = date_format($datelimitt, 'd');
                            $harit               = date_format($datelimitt, 'l');
                            $tahunlimitt         = date_format($datelimitt, 'Y');
                            $waktulimitt         = date_format($datelimitt, 'H:i');   
                            $datelimitt          = $dateelimitt;
                            $sepparatorlimitt    = '/';
                            $partslimitt         = explode($sepparatorlimitt, $datelimitt);
                            $bulanlimitt         = date("F", mktime(0, 0, 0, $partslimit[1], $partslimit[2], $partslimit[0]));

                            $seconds            = $duration;
                            $hours              = floor($seconds / 3600);
                            $mins               = floor($seconds / 60 % 60);
                            $secs               = floor($seconds % 60);
                            $durationrequested  = sprintf('%02d:%02d', $hours, $mins); 

                            $tanggalkonfirm     = date("Y-m-d H:i:s");
                            $this->db->simple_query("UPDATE tbl_order_detail SET class_id='$class_id' WHERE order_id='$product_id'");            
                            $this->db->simple_query("UPDATE tbl_order SET approval_time='$tanggalkonfirm' WHERE order_id='$product_id'");

                            $config1 = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'ssl://smtp.googlemail.com',
                                'smtp_port' => 465,
                                'smtp_user' =>'finance@meetaza.com',
                                'smtp_pass' => 'xmbmpuwuxlzmcjsl', 
                                'mailtype' => 'html',   
                                'charset' => 'iso-8859-1'
                            );

                            $this->load->library('email', $config1);
                            $this->email->initialize($config1);
                            $this->email->set_mailtype("html");
                            $this->email->set_newline("\r\n");
                            $this->email->from('finance@meetaza.com', 'Classmiles');
                            $this->email->to($emailuser);
                            $this->email->subject('Classmiles - Group Class Success');
                            $templateToStudent = 
                                "
                                <div bgcolor='#f3f3f3' style='margin:5px 0 0 0;padding:0;background-color:#f3f3f3'>
                                    <table bgcolor='#f3f3f3' border='0' cellpadding='0' cellspacing='0' style='color: #f3f3f3;font-family:'Museo Sans Rounded',Museo Sans Rounded,'Museo Sans',Museo Sans,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;line-height:20px;border-collapse:callapse;border-spacing:0;margin:0 auto;' width='100%'>
                                        <tbody>
                                              <tr>        
                                                  <td style='padding-left:10px;padding-right:10px'>
                                                      <table align='center' border='0' cellpadding='0' cellspacing='0' style='width:100%;margin:0 auto;max-width:700px' width='100%'>
                                                          <tbody>
                                                            <tr>
                                                                <td style='text-align:center;padding-top:3%'>
                                                                    <a href='https://classmiles.com/' style='text-decoration:none' target='_blank'>
                                                                        <img src='".CDN_URL.STATIC_IMAGE_CDN_URL."maillogo_classmiles.png' style='display:block;margin:0;border:0;max-width:700px' width='100%'>
                                                                    </a>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td bgcolor='#ffffff' style='background-color:#ffffff;padding:9%'>
                                                                    <table border='0' cellpadding='0' cellspacing='0' style='width:100%;padding-bottom:20px'>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style='padding-right:5%' width='72%'>
                                                                                    <h1 style='font-size:18px;font-weight:700;color:#404040;line-height:24px;margin:0 0 15px 0'>
                                                                                        Dear ".$usernameuser.",
                                                                                    </h1>
                                                                                    <p style='color:#888;font-size:15px;font-weight:normal;line-height:24px;margin:0'>
                                                                                        Pembayaran kelas kelompok anda BERHASIL kami terima. Kelas sudah bisa dimulai pada :   
                                                                                    </p>                  
                                                                                    <div style='border-bottom:2px solid #eeeeee;'>&nbsp;</div>
                                                                                    <br>                         
                                                                                    <p>                                                      
                                                                                        <table style='width:100%; border: 1px solid gray;
                                                                                            border-collapse: collapse; color: #6C7A89;'>
                                                                                            <tr>
                                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>TUTOR</th>      
                                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>MATA PELAJARAN</th>
                                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>TOPIK</th>
                                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>START CLASS</th> 
                                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>FINISH CLASS</th>
                                                                                                <th style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>DURASI KELAS</th>                                                                                        
                                                                                            </tr> 
                                                                                            <tr>
                                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$tutorname."</td>
                                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$subject_name."</td>
                                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$topic."</td>
                                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$hari.', '.$harilimit.' '. $bulanlimit. ' '.$tahunlimit.", Pukul ".$waktulimit."</td>
                                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$harit.', '.$harilimitt.' '. $bulanlimitt. ' '.$tahunlimitt.", Pukul ".$waktulimitt."</td>
                                                                                                <td style='border: 1px solid gray; padding: 15px;border-collapse: collapse;'>".$durationrequested."</td>                                                                                        
                                                                                            </tr>                                                                                   
                                                                                        </table>
                                                                                    </p>                                                    
                                                                                </td>
                                                                            </tr>                                                          
                                                                            <tr>
                                                                                <td style='background-color:#ffffff'>
                                                                                    <tbody>                                                                          
                                                                                        <tr>
                                                                                            <td style='padding-bottom:20px;line-height:20px'>
                                                                                                <div style='border-bottom:2px solid #eeeeee'>&nbsp;</div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align='center' style='padding-right:5%' width='100%'>
                                                                                                  <p style='color:gray;font-size:10px;font-weight:normal;line-height:20px;margin:0 top:150px; text-align: center;'>
                                                                                                  Diharapkan jangan membalas email ini.<br>
                                                                                                  Nikmati pengalaman baru dalam belajar dan sebarkan ke kerabat Anda!
                                                                                                  <br>Terima kasih,                                           
                                                                                                  <br>
                                                                                                    Tim Classmiles
                                                                                                  </p>                                                        
                                                                                            </td>                                                   
                                                                                        </tr>
                                                                                    </tbody>                                               
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td border='0' cellpadding='0' cellspacing='0' style='padding:20px 0' width='100%'>
                                                                    <table align='center' border='0' cellpadding='0' cellspacing='0' style='color:#808080;font-size:14px;line-height:18px;border-collapse:collapse' width='100%'>
                                                                        
                                                                    </tbody></table>
                                                                    <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:collapse' width='100%'>
                                                                        
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                      </table>
                                                  </td>
                                              </tr>
                                        </tbody>
                                    </table>

                                </div>
                                ";
                            /**/                
                            $this->email->message($templateToStudent);          

                            if ($this->email->send()) 
                            {
                                // return 1;
                            }
                            else
                            {
                                // return 0;
                            }
                        }
                    }

                    $iduserrequest      = $this->db->query("SELECT buyer_id FROM tbl_order WHERE order_id='$product_id'")->row_array()['buyer_id'];

                    //KIRIM EMAIL SUKSES CREATE CLASS

                    // $namauser           = $this->db->query("SELECT user_name FROM tbl_user WHERE id_user='$iduserrequest'")->row_array()['user_name'];

                }  
            }                              

        }

        // Pass the transaction data to view
        $data['sideactive'] = 'success_payment';
        $this->load->view('inc/header', $data);
        $this->load->view('content/success', $data);
        // $this->load->view('inc/footer');
    }
     
     function cancel(){
        // Load payment failed view
        $data['sideactive'] = 'cancel_payment';
        $this->load->view('inc/header');
        $this->load->view('content/cancel', $data);
        // $this->load->view('inc/footer');
     }
     
     function ipn(){
        // Paypal return transaction details array
        $paypalInfo = $this->input->post();

        $data['user_id']        = $paypalInfo['custom'];
        $data['product_id']     = $paypalInfo["item_number"];
        $data['txn_id']         = $paypalInfo["txn_id"];
        $data['payment_gross']  = $paypalInfo["mc_gross"];
        $data['currency_code']  = $paypalInfo["mc_currency"];
        $data['payer_email']    = $paypalInfo["payer_email"];
        $data['payment_status'] = $paypalInfo["payment_status"];

        $paypalURL = $this->paypal_lib->paypal_url;
        $result     = $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
        
        // Check whether the payment is verified
        if(preg_match("/VERIFIED/i",$result)){
            // Insert the transaction data into the database
            $this->product->insertTransaction($data);
        }
    }
}