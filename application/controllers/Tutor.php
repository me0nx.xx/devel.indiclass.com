<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tutor extends CI_Controller {

	public $sesi = array("lang" => "indonesia");	

	public function __construct()
	{
		parent::__construct();
        $this->load_lang($this->session->userdata('lang'));        
        $this->load->database();
		$this->load->helper(array('url'));       
		// $this->load->model('Master_model'); 
		 $this->load->library('Mobile_Detect');
        $detect = new Mobile_Detect();
		// Check for any mobile device.
		if ($detect->isMobile()){
			define('MOBILE', 'mobile/');
		}else{
			define('MOBILE', '');
		}
	}

	public function index($page = 0)
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor"  ) {	
			if($this->session->flashdata('rets')){
				$data['rets'] = $this->session->flashdata('rets');
			}
			if($this->session->userdata('status') == -1){
				$load = $this->lang->line('registrationfailed');
				$this->session->set_flashdata('mes_alert','danger');
				$this->session->set_flashdata('mes_display','block'); 
				$this->session->set_flashdata('mes_message',$load);	
			}
			$data['sideactive'] = "hometutor";
			$data['page'] = $page;
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/index', $data);
			return null;
		}
		redirect('/');
	}
	public function lngsngmasuk()
	{
		$data['sideactive'] = "hometutor";
		$data['page'] = $page;
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'tutor/index', $data);
		return null;
	}

	public function availability()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {	
			if($this->session->flashdata('rets')){
				$data = $this->session->flashdata('rets');
			}
			$data['sideactive'] = "setavailabilitytime";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/availability', $data);
			return null;
		}
		redirect('/');
	}

	public function availabilitytime()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {	
			if($this->session->flashdata('rets')){
				$data = $this->session->flashdata('rets');
			}
			$data['sideactive'] = "availabilitytime";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/availabilitytime', $data);
			return null;
		}
		redirect('/');
	}

	public function availabilitysubd()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {	
			if($this->session->flashdata('rets')){
				$data = $this->session->flashdata('rets');
			}
			$data['sideactive'] = "availabilitysubd";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/availabilitysubd', $data);
			return null;
		}
		redirect('/');
	}

	public function availabilitypalsu()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {	
			if($this->session->flashdata('rets')){
				$data = $this->session->flashdata('rets');
			}
			$data['sideactive'] = "setavailabilitytime";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/availabilitypalsu', $data);
			return null;
		}
		redirect('/');
	}

	public function setavailability()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "setavailabilitytime";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/setavailability', $data);
			return null;
		}
		redirect('/');
	}

	public function approval_ondemand()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "approvalondemand";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/approvalondemand', $data);
			return null;
		}
		redirect('/');
	}

	public function approval_ondemand_group()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "approvalondemandgroup";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/approvalondemandgroup', $data);
			return null;
		}
		redirect('/');
	}

	public function choose()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "choosingsubjecttutor";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/choose', $data);
			return null;
		}
		redirect('/');
	}

	public function subject()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "subjectadd_choose";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/subjectadd_choose', $data);
			return null;
		}
		redirect('/');
	}

	public function set_class()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "freeclass";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/set_class', $data);
			return null;
		}
		redirect('/');
	}

	public function free()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "freeclass";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/free', $data);
			return null;
		}
		redirect('/');
	}

	public function paid()
	{
		if (isset($_GET['c'])) {
			$c 			= $_GET['c'];        
			$this->session->set_userdata('code',$c);
		}
				
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "paidclass";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/paid', $data);
			return null;
		}
		redirect('/');
	}

	public function BuyClassCredit()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "buy_class_credit";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/buy_class_credit', $data);
			return null;
		}
		redirect('/');
	}

	public function RequestTrialClass()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "request_trial_class";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/request_trial_class', $data);
			return null;
		}
		redirect('/');
	}

	public function privatechannel()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "privatechannel";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/privatechannel', $data);
			return null;
		}
		redirect('/');
	}

	public function groupchannel()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "groupchannel";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/groupchannel', $data);
			return null;
		}
		redirect('/');
	}

	public function multicastchannel()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "multicastchannel";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/multicastchannel', $data);
			return null;
		}
		redirect('/');
	}

	public function ClassChannel()
	{
		// if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
		// 	$data['sideactive'] = "listclasschannel";
		// 	$this->load->view(MOBILE.'inc/header');
		// 	$this->load->view(MOBILE.'tutor/listclass_channel', $data);
		// 	return null;
		// }
		redirect('/');
	}
	
	public function about()
	{
		/*echo "waksss";
		print_r($this->session->userdata());
		print_r("<a href='https://devel.classmiles.com/logout'>asd</a>");
		if($this->session_check() == 1){
			echo "logged  in";
		}else{
			redirect('/');
		}
		return false;*/
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "profiletutor";
			$data['profile'] = $this->Master_model->getTblUserAndProfileTutor();
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/about', $data);
			return null;
		}
		redirect('/');
	}

	public function profile()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "profiletutor";
			$data['alluserprofile'] = $this->Master_model->getTblUserAndProfileTutor();
			/*print_r('<pre>');
			print_r($data['alluserprofile']);
			print_r('</pre>');*/
		
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/profile', $data);
			return null;
		}
		redirect('/');
	}

	public function profile_account()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			if($this->session->flashdata('rets')){
				$data['rets'] = $this->session->flashdata('rets');
			}
			$data['sideactive'] = "profiletutor";
			$data['provinsi']=$this->Master_model->provinsi();
			$data['kabupaten']=$this->Master_model->getAllKabupaten();
			$data['kecamatan']=$this->Master_model->getAllKecamatan();
			$data['kelurahan']=$this->Master_model->getAllKelurahan();
			$data['levels']=$this->Master_model->jenjangname();
			$data['alldata'] = $this->Master_model->getTblUserAndProfileTutor();
			$this->load->view(MOBILE.'inc/header');

			$this->load->view(MOBILE.'tutor/profile_account', $data);
			return null;
		}
		redirect('/');
	}

	public function profile_forgot()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "profiletutor";
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/profile_forgot', $data);
			return null;
		}
		redirect('/');
	}

	public function Approval()
	{
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			if($this->session->userdata('status') == 3){
				$data['sideactive'] = "profileapproval";
				$data['provinsi']=$this->Master_model->provinsi();
				$data['levels']=$this->Master_model->jenjangnamelevel();
				$this->load->view(MOBILE.'inc/header');
				$this->load->view(MOBILE.'tutor/approval', $data);
				return null;
			}	
		}
		redirect('/');
	}

	public function ReportTeaching()
	{	
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "reportteaching";				
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/report_teaching', $data);
			return null;			
		}
		redirect('/');
	}

	public function Quiz()
	{	
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "listquiz";				
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/list_quiz', $data);
			return null;			
		}
		redirect('/');
	}

	public function EditQuiz()
	{	
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "listquiz";				
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/edit_quiz', $data);
			return null;			
		}
		redirect('/');
	}

	public function QuestionAdd()
	{	
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "question_add";				
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/question_add', $data);
			return null;			
		}
		redirect('/');
	}

	public function QuestionAddArchive()
	{	
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "question_add_archive";				
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/question_add_archive', $data);
			return null;			
		}
		redirect('/');
	}

	public function QuestionArchive()
	{	
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "list_quiz_archive";				
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/question_archive_view', $data);
			return null;			
		}
		redirect('/');
	}

	public function QuestionView()
	{	
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "question_view";				
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/question_view', $data);
			return null;			
		}
		redirect('/');
	}

	public function List_QuizResult()
	{	
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "student_quizview";				
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/list_quizresult', $data);
			return null;			
		}
		redirect('/');
	}

	public function QuizResultStudent()
	{	
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "student_quizview";				
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/student_quizview', $data);
			return null;			
		}
		redirect('/');
	}

	public function Description()
	{
		$data['sideactive'] = "profileapproval";
		$data['profile'] = $this->Master_model->getTblUserAndProfileTutor();
		// $data['alluserprofile'] = $this->Master_model->getTblUserAndProfileTutor();
		$this->load->view(MOBILE.'inc/header');
		$this->load->view(MOBILE.'tutor/profile_desc', $data);
		return null;
	}

	public function Complain()
	{	
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "complain";				
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'inc/complain', $data);
			return null;			
		}
		redirect('/');
	}

	public function FileManager()
	{	
		if ($this->session_check() == 1 && $this->session->userdata('usertype_id') == "tutor") {
			$data['sideactive'] = "filemanager";				
			$this->load->view(MOBILE.'inc/header');
			$this->load->view(MOBILE.'tutor/file_manager', $data);
			return null;			
		}
		redirect('/');
	}

	function ambil_data(){

		$modul=$this->input->post('modul');
		$id=$this->input->post('id');

		if($modul=="kabupaten"){
			echo $this->Master_model->kabupaten($id);
		}
		else if($modul=="kecamatan"){
			echo $this->Master_model->kecamatan($id);
		}
		else if($modul=="kelurahan"){
			echo $this->Master_model->kelurahan($id);
		}
	}

	public function load_lang($lang = '')
	{
		$this->lang->load('umum',$lang);
	}

	public function session_check()
	{
		if($this->session->has_userdata('id_user')){
			$new_state = $this->db->query("SELECT status FROM tbl_user where id_user='".$this->session->userdata('id_user')."'")->row_array()['status'];
			if($this->session->userdata('status') != $new_state ){
				$this->session->sess_destroy();
				redirect('/login');
			}
			// $this->sesi['user_name'] = $this->session->userdata('user_name');
			// $this->sesi['username'] = $this->session->userdata('username');
			// $this->sesi['priv_level'] = $this->session->userdata('priv_level');
			if($this->session->userdata('usertype_id') != "tutor"){
				redirect('/logout');
			}
			return 1;
		}else{
			return 0;
		}
	}
	

}
