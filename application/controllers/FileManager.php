<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FileManager extends CI_Controller {

	public $sesi = array("lang" => "indonesia");

	public function __construct()
	{
		parent::__construct();
        $this->load_lang($this->session->userdata('lang'));
        $this->load->library('recaptcha');
		$this->session->set_userdata('color','blue'); 
		$this->load->library('email');
		$this->load->helper(array('url'));
		// $this->load->library('fpdf');
        $this->load->helper(array('Form', 'Cookie', 'String'));
        $isMobile = false;

        $this->load->library('Mobile_Detect');
        $detect = new Mobile_Detect();
		// Check for any mobile device.
		if ($detect->isMobile()){
			$this->isMobile = true;
			if (base_url() == 'https://classmiles.com/') {
				// redirect('https://m.classmiles.com');
			}else{

			}
			define('MOBILE', 'mobile/');

		}else{
			define('MOBILE', '');
		}
		header("Access-Control-Allow-Origin: *");	
	}
	public function check_path($value='')
	{
		echo APPPATH;
	}

	public function listFolder_unauth($tutor_id='')
	{
		$listfolder = $this->Rumus->classDrive_listFolders($tutor_id);
		if (empty($listfolder)) {
			$res['status']		= 0;
			$res['list_folder'] = null;	
		}
		else{
			$res['status']		= 1;
			$res['list_folder'] = $listfolder;
		}
		
		echo json_encode($res);
	}
	public function listFile_unauth($tutor_id='')
	{
		$folder_select = $this->input->post('folder');
		$list_file = $this->Rumus->classDrive_listImages($tutor_id, $folder_select);
		if (empty($list_file)) {
			$res['status']		= 0;
			$res['list_file'] = null;
		}
		else
		{
			$res['status']		= 1;
			$res['list_file'] = $list_file;
		}

		echo json_encode($res);
	}
	public function listFolder($tutor_id='')
	{
		$tutor_id = $this->session->userdata('id_user');
		$listfolder = $this->Rumus->classDrive_listFolders($tutor_id);
		if (empty($listfolder)) {
			$res['status']		= 0;
			$res['list_folder'] = null;	
		}
		else{
			$res['status']		= 1;
			$res['list_folder'] = $listfolder;
		}
		
		echo json_encode($res);

	}

	public function createFolder()
	{
		$tutor_id = $this->session->userdata('id_user');
		$namefolder = $this->input->post('namefolder');
		$create = $this->Rumus->classDrive_createFolder($tutor_id,$namefolder);
		
		$res['status']		= 1;
		$res['create_folder'] = $return;

		echo json_encode($res);
	}

	public function deleteFolder()
	{
		$tutor_id = $this->session->userdata('id_user');
		$namefolder = $this->input->post('namefolder');
		$delete = $this->Rumus->classDrive_deleteFolder($tutor_id,$namefolder);
		
		$res['status']		= 1;
		$res['create_folder'] = $delete;

		echo json_encode($res);
	}

	public function list_file()
	{
		$tutor_id = $this->session->userdata('id_user');
		$folder_select = $this->input->post('folder');
		$list_file = $this->Rumus->classDrive_listImages($tutor_id, $folder_select);
		if (empty($list_file)) {
			$res['status']		= 0;
			$res['list_file'] = null;
		}
		else
		{
			$res['status']		= 1;
			$res['list_file'] = $list_file;
		}

		echo json_encode($res);
	}

	public function saveTempImage()
    {        
    	$tutor_id = $this->session->userdata('id_user');
        $img_name = $this->input->post('imgname');
        $imgfile = $this->input->post('imgfile');
        $folder = $this->input->post('folder');        

        file_put_contents('./aset/_temp_filemanager/'.$img_name,file_get_contents($imgfile));
        
        $this->Rumus->classDrive_uploadImage($tutor_id, $folder, $img_name, '', APPPATH.'../aset/_temp_filemanager/'.$img_name);
        // $this->session->set_userdata('user_image_temp', CDN_URL.USER_IMAGE_CDN_URL.base64_encode('banksoal/'.$img_name));

        unlink(APPPATH.'../aset/_temp_filemanager/'.$img_name);
        $res['status']		= 1;		
        echo json_encode($res);
    }

    public function deleteImage()
	{
		$tutor_id = $this->session->userdata('id_user');
		$namefolder = $this->input->post('folder');
		$filename = $this->input->post('filename');

		$delete = $this->Rumus->classDrive_deleteImage($tutor_id, $namefolder, $filename);
		
		$res['status']		= 1;
		$res['create_folder'] = $delete;

		echo json_encode($res);
	}

	public function load_lang($lang = '')
	{
		$this->lang->load('umum',$lang);
	}

	public function set_lang($value='')
	{	
		if($value == ""){
			$this->session->set_userdata('lang','indonesia');
		}else{
			$this->session->set_userdata('lang',$value);
		}			
		return null;
	}
}
