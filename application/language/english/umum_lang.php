<?php
	/*
	English
	*/
	//BULAN

	$lang['jan']					= 'January';
	$lang['feb']					= 'February';
	$lang['mar']					= 'March';
	$lang['apr']					= 'April';
	$lang['mei']					= 'May';
	$lang['jun']					= 'June';
	$lang['jul']					= 'July';
	$lang['ags']					= 'August';
	$lang['sep']					= 'September';
	$lang['okt']					= 'October';
	$lang['nov']					= 'November';
	$lang['des']					= 'December';

	//HARI
	$lang['Sun']					= 'Sunday';
	$lang['Mon']					= 'Monday';
	$lang['Tue']					= 'Tuesday';
	$lang['Wed']					= 'Wednesday';
	$lang['Thu']					= 'Thursday';
	$lang['Fri']					= 'Friday';
	$lang['Sat']					= 'Saturday';

	$lang['buyforkid']				= 'Buy this package for';
	$lang['pilihakun']				= 'Choose Account';
	$lang['textbelipaket']			= 'Beli paket ini untuk :';
	$lang['program_reguler']		= 'Reguler Program';
	$lang['text_program']		= 'Or join below programs from Indiclass partners:';
	$lang['daftarakun']				= 'Account List';
	$lang['tambahanak']				= 'Add Kids Account';
	$lang['hello']					= 'Hello,';
	$lang['birth']					= 'Place & date of birth';
	$lang['tiga_langkah']			= 'Online learning with 3 easy steps';
	$lang['pilih_tutor']			= 'Select Tutor';
	$lang['atur_waktu']				= 'Set the Time';
	$lang['masuk_kelas']			= 'Enter Class';
	$lang['daftar_disini']			= 'Register Here Now';
	$lang['kami_siap']				= 'We are ready to serve you';
	$lang['setiap_hari_kerja']		= 'Every working days';


    // $lang['banner2']				= 'Direct online interaction with the tutor of your choice wherever he/she is.';
    $lang['banner3']				= 'Flexible timing, set the schedule according to your availability.';
    // $lang['banner4']				= 'Discuss directly with the tutor as closely as possible to discuss with your parents.';
    // $lang['banner5']				= 'Available features private tutoring with teachers who are varied, fun and very experienced.';
    $lang['banner6']				= 'Distance is no longer a barrier. Group learning become much easier. Can be from anywhere.';


    // Class Credit
    $lang['classcredit']			= 'We will send you and an invoice via email. Kindly proceed with the payment.';
	$lang['btnclasscredit']			= 'Ask For Invoice';

	// TAMBAHAN
	$lang['invitedby']				= 'Invited by';
	$lang['classinvitation']		= 'Class Invitation';
	$lang['extraclass']				= 'Extra Class';
	$lang['createextraclass']		= 'Create New Class';
	$lang['createnewclass']			= 'Create New Class';
	$lang['free']					= 'Free';

	$lang['joined']					= 'Participant';


	$lang['waitapprovedme']			= 'Waiting for your approval';
	$lang['waitapprovedtutor']		= 'Waiting for tutor approval';
	$lang['waitapprovedfriend']		= 'Waiting for firends approval';
	$lang['waitforpayment']			= 'Waiting for payment';
	$lang['paymentsuccess']			= 'Payment Successful';
	$lang['paymentfailed']			= 'Payment Failed';
	$lang['waitingunderpayment']	= 'Waiting Underpayment';
	$lang['rejectedbytutor']		= 'Rejected by tutor';
	$lang['classexpired']			= 'This class expired';
	$lang['tutornotavailable']		= 'Tutor not available';
	$lang['confirmedbytutor']		= 'Confirmed by tutor';

	$lang['completeprofiletitle']	= 'Complete Your Profil';
	$lang['completeschooltitle']	= 'Complete Your School Data';
	$lang['alertcompleteschool']	= 'Please complete your school data below';
	$lang['alertcompleteaddress']	= 'Please complete your address data below';
	$lang['alertcompletedata']		= 'Please complete your personal data below to access full features in Indiclass';
	$lang['alertnotif_complain']	= 'Hi Tutor, Student';
	$lang['alertnotif_complain2']	= 'complain the class';
	$lang['alertnotif_complain3']	= 'please discuss in chatting feature.';
	
	$lang['yearsold']				= "years old";
	$lang['typeaddress']			= "Type your address here...";
	$lang['livenow']				= 'Live Now';
	$lang['comingup']				= 'Coming Up';
	$lang['missed']					= 'Passed';
	$lang['purchasecomfirmation']	= 'Purchase Confirmation';
	$lang['konfirmasibelikelas']	= 'Do you want to buy this class ?';
	$lang['konfirmasigabungkelas']	= 'Do you want to join this class ?';
	$lang['berbagilinkkelas']		= 'Share link your Class';

	$lang['join']					= 'Join';
	$lang['groupclass']				= 'Group Class';
	$lang['privateclass']			= 'Private Class';

	$lang['detailprivate']			= 'Only you and tutor in class';
	$lang['detailgroup']			= 'Up to 4 students in class';
	$lang['emailkosong']			= 'Please input at least 1 email of your friend!';
	$lang['emailnotfound']			= 'Sorry there are emails that are not listed in Indiclass. Please repeat again';
	$lang['emailown']				= 'Sorry you can not ask yourself';
	$lang['topikbelajar']			= 'Topics: ';
	$lang['isitopikanda']			= 'Fill topics you want to learn ...';
	$lang['topikkosong']			= 'Please fill in the lesson topic first!';
	$lang['friendinvitation']		= 'Friend Invitation';
	$lang['durasi']					= 'Duration';
	$lang['tanggaldurasi']			= 'Date of request';
	$lang['belumsetuju']			= 'Not Yet Approved';
	$lang['sudahsetuju']			= 'Already Approved';
	$lang['bayarsendiri']			= 'Own';
	$lang['bayarbersama']			= 'Together';
	$lang['waitapproved']			= 'Awaiting approval from the tutor';
	$lang['temangroup']				= 'Friends of the group';
	$lang['diajakoleh']				= 'Invited by';
	$lang['terima']					= 'Accept';
	$lang['tolak']					= 'Decline';
	$lang['kelasapprove']			= 'Class request has been approved by the tutor. Class schedule already on ';
	$lang['waitingfriend']			= 'Awaiting Friend Approval';
	$lang['waitingtutor']			= 'Request already received by tutor, wait until our tutor receive it.';
	$lang['menit']					= 'Minutes';
	$lang['jam']					= 'Hours';

	$lang['lihatkelaskids']			= "Viewing the Kid's Class Schedule";
	$lang['pilihkids']				= "please select your child to see their schedule";
	$lang['jadwalkelas']			= "Class Schedule";
	$lang['kidsselected']			= "Child selected";
	$lang['pilihyanglain']			= "Choose another";
	$lang['registerkid']			= "Register your child";
	$lang['typeusername']			= "Type username here...";
	$lang['typepassword']			= "Type password here...";
	$lang['typefirstname']			= "Type first name here...";
	$lang['typelastname']			= "Type last name here...";
	$lang['typebirthplace']			= "Type birthplace here...";
	$lang['typeschoolname']			= "Type school name here...";
	$lang['typeschooladdress']		= "Type school address here...";
	$lang['aksesloginkid']			= "Access to login Child";
	$lang['register']				= "Register";

	$lang['lihatpelajarankids']		= "Viewing the Kid's Subject";
	$lang['pilihpelajaran']			= "please select your child to see their subject";
	$lang['pilihanak']				= "please select your child";
	$lang['mengatur']				= "Set the schedules";
	$lang['atur']					= "Set";

	$lang['masukkelas']				= "Enter";
	$lang['kelasberakhir']			= "Ended";
	$lang['belikelas']				= "Buy";
	$lang['aturkelas']				= "Set Class";
	$lang['kelaspribadi']			= "Personal Class";
	$lang['kelaschannel']			= "Channel Class";
	$lang['gratis']					= "Open Class";
	$lang['berbayar']				= "Paid Class";
	$lang['buy_class_credit']		= "Buy class credit";
	$lang['request_trial_class']	= "Request trial class";
	$lang['private']				= "Private";
	$lang['group']					= "Group";
	$lang['invitation']				= "Invitation";
	$lang['nodata_ondemand']		= "Please click '+' below";
	$lang['nodata_group']			= "to add a group class request";
	$lang['nodata_private']			= "to add a private class request";
	$lang['nodata_invitation']		= "No data Invitation";
	$lang['noorder_class']			= "No order class";
	$lang['noinvoice']				= "You haven't purchased any class";
	$lang['keranjangorder']			= "Order Basket";
	$lang['listinvoice']			= "List Purchase";
	$lang['listdetailinvoice']		= "click for detail";


	$lang['typeyournumberid']			= "Type Your ID Number here...";
	$lang['typeyourbirthplace']			= "Type Your Birthplace here...";
	$lang['chooseyourreligion']			= "Choose Your Religion here...";
	$lang['typeyouraddress']			= "Type Your address here...";
	$lang['chooseyourpropinsi']			= "Choose Your Province  here...";
	$lang['chooseyourkabupaten']		= "Choose Your City  here...";
	$lang['chooseyourkecamatan']		= "Choose Your Subdistrict  here...";
	$lang['chooseyourkelurahan']		= "Choose Your Village  here...";







	/* LOGIN */

	$lang['wlogin'] 				= 'Welcome to ';
	$lang['notaccountlogin'] 		= 'No account yet? Please register';
	$lang['here'] 					= 'here';
	$lang['login'] 					= 'Login';
	$lang['forgotpassword'] 		= 'Forgot Password ?';
	$lang['description_loginfooter']= 'Indiclass provides online learning service. It contains many virtual classrooms for students i.e. elementary school (grade 5-6), junior high school (grade 7-9) and senior high school (grade 10-12). Indiclass also provides general course such as English, Japanese, Arab, Cooking, Yoga, Dancing, etc. Indiclass enables interactive communication between student and teacher as well as among students. Please sign up to enjoy our free sessions we provide everyday.';
	$lang['withfacebook'] 			= 'Login with Facebook';
	$lang['withgoogle'] 			= 'Login with Google';
	$lang['remember']				= 'Remember me';

	// FORGOT 
	$lang['lupasandi'] 				= 'Forgot Password';
	$lang['title1'] 				= 'Enter your email address below and';
	$lang['title2'] 				= 'we will send you instructions on';
	$lang['title3'] 				= 'how to change your password';
	$lang['inputemail'] 			= 'Input your email';
	$lang['sendemail'] 				= 'Send Email';
	$lang['backlogin'] 				= 'Back to Home';

	// REGISTER

	$lang['detail_ortu']			= 'To register a grade 1-6 student, parent of the student must register themselves first in Indiclass by choosing this option. After signing up, the parent can add their children who are grade 1-6 students through My Kids menu and will get user name and password for each child registered. Child then can access the class through website or Indiclass Kids apps.';
	$lang['detail_siswa']			= 'Grade 7-12 student';
	$lang['detail_umum']			= 'College student, Professional, etc';
	$lang['detail_tutor']			= 'If you are teacher, trainer, tutor, coach or someone wants to TEACH in Indiclass, click this button to register.';
	$lang['tutor']					= 'TUTOR';
	$lang['pilihlevel']				= 'Register according to your age, level of education or job.';
   	$lang['levelpelajar']			= '(Grade 7-12, University, General)';
   	$lang['leveltutor']				= '(Teacher, Trainer)';
   	$lang['pelajar']				= 'STUDENT';			
	$lang['signup_tutor']			= 'Tutor Registration';
   	$lang['levelsiswa']				= '(Grade 7 to 12)';
   	$lang['levelumum']				= '(University, Professional, etc)';
	$lang['ortu']					= "PARENT of Grade 1-6 student";
	$lang['levelortu']				= "(Parents / Student Guardian)";
	$lang['smp-sma']				= "Grade 7 - 12 Student";

   	$lang['or']						= 'Or';
   	$lang['sebagai']				= 'REGISTER AS';
   	$lang['pendaftaransiswa']		= 'STUDENT REGISTRATION';
	$lang['daftar_siswa']			= 'STUDENT';
	$lang['daftar_umum']			= 'GENERAL';
	$lang['signup'] 				= 'REGISTRATION';
	$lang['sstudent']				= 'STUDENT';
	$lang['student'] 				= 'Student';
	$lang['nonstudent'] 			= 'GENERAL';
	$lang['datebirth'] 				= 'Date';
	$lang['monthbirth'] 			= 'Month';
	$lang['yearbirth'] 				= 'Year';
	$lang['male'] 					= 'Male';
	$lang['women'] 					= 'Female';
	$lang['countrycode'] 			= 'Country code';
	$lang['alreadyaccount'] 		= 'Already have an account? Login click';
	$lang['orsignup'] 				= 'Or you can sign up through';
	$lang['signupfacebook'] 		= 'Sign up with Facebook';
	$lang['signupgoogle'] 			= 'Sign up with Google';

	$lang['invalidfirstname'] 		= 'Please fill in First Name';
	$lang['invalidlastname'] 		= 'Please fill in Last Name';
	$lang['invalidpassword'] 		= 'Please fill in Password';
	$lang['invalidconfirmpassword'] = 'Please fill in Confrim Password';
	$lang['invalidtanggallahir'] 	= 'Please fill in Date of birth';
	$lang['invalidbulanlahir'] 		= 'Please fill in Month of birth';
	$lang['invalidtahunlahir'] 		= 'Please fill in Year of birth';
	$lang['invalidnohape'] 			= 'Please fill in Phone Number';
	$lang['invalidjenjanglevel']	= 'Please Select Level';
	$lang['invalidemail'] 			= 'Invalid email address';

	$lang['btnsignup']				= 'Sign up';
	$lang['createpassword'] 		= 'Create Password';
	$lang['laststep']				= 'The final step, please complete your details in the column below for the security and convenience of transacting next.';

	$lang['syarat']					= 'I have read and agree to our';	
	$lang['syarat1']			 	= 'Terms and Conditions';
	$lang['syarat2']				= 'and Privacy Policy of Indiclass.';

	$lang['activationsuccess']		= 'Account activation is successful,';
	$lang['activationsuccess2']		= 'please continue to login.';
	$lang['activationtutor']		= 'please complete your personal data, education history and teaching experience for approval request to be Indiclass tutor.';
	$lang['sayaadalah']				= 'I am a';
   	$lang['pilihpekerjaan']			= 'Select Occupation';
   	$lang['pesanregistertutor1']	= 'If you are teacher, trainer, tutor, coach or someone wants to TEACH in Indiclass, please proceed to register.';
   	$lang['pesanregistertutor2']	= 'If you are student or professional wants to LEARN in Indiclass, kindly register as STUDENT.';
   	$lang['pesanumurumum']			= 'Are you sure you have selected the correct Date of Birth or Occupation?? We identify that your age is not in the general age range.';	
   	$lang['pesanumursiswa']			= 'Are you sure you have selected the correct Date of Birth or Occupation? We identify that your age is not in the student age range.';


	$lang['kewarganegaraan']		= 'Nationality';
	$lang['kodereferral']			= 'Referral Code';
	$lang['tidakpunyareferral']		= 'Do not have referral code';
	$lang['referralsalah']			= 'Your referral code is wrong';
	$lang['referralbenar']			= 'Your referral code is correct';
	$lang['lanjutkan']				= 'proceed';
	$lang['akunsudahada']			= 'Account Already Listed';
	$lang['akungoogle']				= 'Your Google account is already registered, please use another account';
	$lang['akunfacebook']			= 'Your Facebook account is already registered, please use another account';


	//FITUR TUTOR
	$lang['fitur_tutor1']			= 'Want to be a teacher? or hard to find a place for teaching?';
	$lang['fitur_tutor2']			= "Let's";
	$lang['fitur_tutor3']			= "Register Here";
	$lang['fiturtutor1'] 			= 'Anytime';
	$lang['isi_fitur1']				= 'Morning, afternoon or evening is no longer an obstacle for you to be able to teach. You are free to set the time.';
	$lang['fiturtutor2']			= 'Anywhere';
	$lang['isi_fitur2'] 			= 'Indiclass encourages teacher/tutor to be able to teach from wherever you are. ';
	$lang['fiturtutor3'] 			= 'Name Your Own Price        ';
	$lang['isi_fitur3'] 			= 'Like having your own tuition agency, you are free to determine the fee. Earn infinite income by joining Indiclass.';
	$lang['fiturtutor4'] 			= 'Time and energy efficiency';
	$lang['isi_fitur4']				= 'No longer need to spend transport expenses and energy to meet face to face with students. Use your precious time to teach remotely in Indiclass.';

	//Syarat dan Ketentuan
	$lang['headersyarat']			= 'Terms and Conditions';
	$lang['sebagai_siswa']			= 'As a Student';
	$lang['sebagai_tutor']			= 'As a Tutor';

	$lang['isi_siswa']				= 'Indiclass.com is a site that connects students, students and communities with the right teachers to help students and learners in the classroom learn new skills / knowledge, gain additional help outside the school / campus, and develop certain skills. This www.Indiclass.com website is managed by PT. Meetaza Prawira Media. By accessing and using the Site www.Indiclass.com and registering as a Student or Class Participant at www.Indiclass.com, you understand and agree to be bound by and subject to all applicable rules at Indiclass.com. Indiclass reserves the right to modify the terms at any time without prior notice, which you are legally bound by these terms if you use the website and / or related services. Any changes to the terms will be effective immediately upon being published on this page, with an updated effective date. Make sure you return to this page periodically to make sure you understand the latest version of the Indiclass provisions.';

	$lang['isi_tutor']				= 'Indiclass.com is a site that connects students, students and communities with the right teachers to help students and learners in the classroom learn new skills / knowledge, gain additional help outside the school / campus, and develop certain skills. This www.Indiclass.com website is managed by PT. Meetaza Prawira Media . By accessing and using the Site www.Indiclass.com and registering as a Tutor at www.Indiclass.com, you understand and agree to be bound by and subject to all applicable rules at Indiclass.com. Indiclass.com reserves the right to modify the terms at any time without prior notice, which you are legally bound by these terms if you use the website and / or related services. Any changes to the terms will be effective immediately upon being published on this page, with an updated effective date. Make sure you return to this page periodically to make sure you understand the latest version of Indiclass.com terms.';

	$lang['sk_daftarsiswa']			= '1. Registration';
	$lang['sk_daftarsiswa1']		= 'For registration to be a Student, you must complete all biodata completeness without exception and include your real identity.';
	$lang['sk_daftarsiswa2']		= 'You must specify the full name (not the alias name).';
	$lang['sk_daftarsiswa3']		= 'The listed phone number is an active number, so Indiclass.com may contact at any time as necessary.';
	$lang['sk_daftarsiswa4']		= 'Choose classes according to age, level education and skills.';
	$lang['sk_daftarsiswa5']		= 'Indiclass.com reserves the right to refuse your registration application as a Disciple for obvious reasons.';

	$lang['sk_akunsiswa']			= '2. Student Account';
	$lang['sk_akunsiswa1']			= 'You are responsible for maintaining the confidentiality of accounts and passwords and restricting access to your computer.';
	$lang['sk_akunsiswa2']			= 'You agree to be responsible for everything that goes on using your personal account and password.';
	$lang['sk_akunsiswa3']			= 'You agree to immediately notify Indiclass.com of any account abuse or other security breaches related to your account.';
	$lang['sk_akunsiswa4']			= 'Indiclass.com shall not be liable for any loss or damage arising from failure to comply with these terms.';
	$lang['sk_akunsiswa5']			= 'Indiclass.com reserves the right to refuse service or terminate the account.';

	$lang['sk_profilsiswa']			= '3. Student Profile';
	$lang['sk_profilsiswa1']		= 'You agree to fill out all personal information as accurately as possible.';
	$lang['sk_profilsiswa2']		= 'You, and not Indiclass.com, are solely responsible for all materials you provide on Indiclass.com.';

	$lang['sk_caritutor']			= '4. Search Tutor';
	$lang['sk_caritutor1']			= 'You understand that all information about the Tutor is provided by a third party that is outside the control of Indiclass.com.';
	$lang['sk_caritutor2']			= 'We are not responsible for any loss caused by third party information.';
	$lang['sk_caritutor3']			= 'Before entering a private class or a public class, participants and students are required   Follow Tutor lessons to be selected as a teacher or giver of material   lesson';
	$lang['sk_caritutor4']			= 'After following the lessons from the Tutor that has been selected, students will get   Information if the Tutor provides both public and private classes';
	$lang['sk_caritutor5']			= 'The date and time of the private course you choose on the search form is necessary   Get approval from Tutor or teaching staff. If you have not   Get approval from the Tutor or Teaching Staff you have selected. You   Can search for Tutor   Or other teachers after cancellation on   Private course request.';
	$lang['sk_caritutor6']			= 'If you have not changed or canceled an agreed private class   By the Tutor or the teaching staff, you will be considered following a private class   Has been approved by the Tutor or the teaching staff.';

	$lang['sk_bayarsiswa']			= '5. Payment';
	$lang['sk_bayarsiswa1']			= 'You agree to pay for the course package according to the time set by Indiclass.com.';
	$lang['sk_bayarsiswa2']			= 'If you do not pay within 2x24 hours, the booking is considered canceled.';
	$lang['sk_bayarsiswa3']			= 'Indiclass.com does not guarantee return of payment in case of cancellation of study.';
	$lang['sk_bayarsiswa4']			= 'If the class member resigns or can not continue learning until the class is over, then the money already paid can not taken again.';



	$lang['sk_perilakusiswa']		= '6. Student Behavior';
	$lang['sk_perilakusiswa1']		= 'Students who are late to pay for the package will have an impact on the cancellation of the enrollment.';
	$lang['sk_perilakusiswa2']		= 'Students must maintain ethics and courtesy while studying with Tutor so that the teaching and learning atmosphere becomes conducive and comfortable.';
	$lang['sk_perilakusiswa3']		= 'Students and any users are prohibited from violating any applicable law, the rights of others, intellectual property rights, and others, and the rules governed by this Agreement.';
	$lang['sk_perilakusiswa4']		= 'Students and any users of the service are prohibited from providing false, inaccurate, misleading, defamatory, immoral, pornographic, discriminatory or racist information.';
	$lang['sk_perilakusiswa5']		= 'Students and any users are prohibited from spreading spam, things that are not   Immoral, or large electronic messages, serial messages.';
	$lang['sk_perilakusiswa6']		= 'Students and any users service and viral or viral spread   Other similar technologists who may damage and / or harm the Site,   Affiliates, Providers, and other Users.';
	$lang['sk_perilakusiswa7']		= 'Students and any users are prohibited from entering or moving features    On the Site is no exception without our knowledge and consent.';
	$lang['sk_perilakusiswa8']		= 'Students and any users are prohibited from storing, replicating, modifying, or otherwise   Disseminate the content and features of the Site, including the way services, content, copyrights   And intellectual property contained on the Site.';
	$lang['sk_perilakusiswa9']		= 'Students and any users of the service are prohibited from taking or collecting   Information from other users, including email addresses, without your knowledge   Other users.';



	$lang['sk_informasisiswa']		= '7. Use of Information';
	$lang['sk_informasisiswa1']		= 'The use of Tutor account information, ambassadors and students for the benefit of Indiclass.com and Indonesian Future Leaders is permitted on a permanent basis by agreement of Indiclass.com without going through intermediaries and all matters relating to information are the rights of Indiclass.com fully.';
	$lang['sk_informasisiswa2']		= 'By participating in this Indiclass, it means that registrants have read and agree to the terms and conditions that apply. Indiclass entitled to use the data and information you provide.';
	$lang['sk_informasisiswa3']		= 'We reserve the right to restrict or deny access, or provide different access to be able to open the Site and its features to each User, or to replace any of the features or include new features without prior notice. Each User is aware that if the Site can not be used in whole or in part for any reason, any attempt or activity of any User may be impaired. Each User hereby agrees that for any reason exempts Us from any form of liability to the User or to any third party in the event that the person concerned may not use the Site (whether due to any interruption, restricted access, modification of features or exclusion of certain features or due other reason); Or if communications or transmissions are delayed, fail or can not take place; Or if there is a loss (directly, indirectly) due to the use or non-use of the Site or any of the features thereof.';
	$lang['sk_informasisiswa4']		= 'The User hereby acknowledges and agrees that the use of this Site is the responsibility and risk of the User and therefore the User hereby releases or exempts us from and against all or any other form of liability for loss of use of the Site';





	$lang['sk_daftartutor']			= '1. Registration';
	$lang['sk_daftartutor1']		= 'To register into a Tutor, you are required to complete all of your personal information, including personal data, short profile, teaching experience, and proof of qualification for a particular subject. All information entered should be accurate and correct information';
	$lang['sk_daftartutor2']		= 'You must specify the full name (not the alias name).';
	$lang['sk_daftartutor3']		= 'The specified phone number is an active number, so Indiclass.com may contact at any time as necessary.';
	$lang['sk_daftartutor4']		= 'Choose class corresponding with age, education as well appropriate expertise.';
	$lang['sk_daftartutor5']		= 'Indiclass.com reserves the right to refuse your registration application as a Tutor for obvious reasons.';

	$lang['sk_akuntutor']			= '2. Tutor Account';
	$lang['sk_akuntutor1']			= 'You are responsible for maintaining the confidentiality of accounts and passwords and limiting access to your computer.';
	$lang['sk_akuntutor2']			= 'You agree to be responsible for everything that goes on using your personal account and password.';
	$lang['sk_akuntutor3']			= 'You agree to immediately notify Indiclass.com of any account abuse or other security breaches related to your account';
	$lang['sk_akuntutor4']			= 'Indiclass.com shall not be liable for any loss or damage arising from failure to comply with these terms.';
	$lang['sk_akuntutor5']			= 'Indiclass.com reserves the right to refuse service or terminate the account.';

	$lang['sk_profiltutor']			= '3. Tutor Profile';
	$lang['sk_profiltutor1']		= 'You agree to fill out all personal information as accurately as possible.';
	$lang['sk_profiltutor2']		= 'You, and not Indiclass.com, are solely responsible for all material you provide on Indiclass.com.';

	$lang['sk_bayartutor']			= '4. Payment For Tutor';
	$lang['sk_bayartutor1']			= 'You agree to follow the payment procedures set by Indiclass.com, ie payments will be paid monthly with a cut-off date on the 25th of each month';
	$lang['sk_bayartutor2']			= 'The amount to be you accept is Amount 90% of the total amount cost that agreed upon by Students, cut tax income corresponding regulations.';
	
	$lang['sk_perilakututor']		= '5. Tutor Behavior';
	$lang['sk_perilakututor1']		= 'The tutor should be responsible, be professional and ethical while teaching Students to make the learning environment conducive and comfortable.';
	$lang['sk_perilakututor2']		= 'Tutors should prepare the teaching materials carefully before teaching';
	$lang['sk_perilakututor3']		= 'Tutors must be present on time according to the agreed schedule. If the Tutor faces obstacles to attend, the Tutor must inform the Parents or Students who are taught for obvious reasons at least two days before the teaching time.';
	$lang['sk_perilakututor4']		= 'The tutor agrees to accept sanctions from Indiclass.com if it does not meet all three points above. The form of sanctions will be determined more by Indiclass.com according obvious consideration.';
	$lang['sk_perilakututor5']		= 'Tutor and every Users service banned spread spam, things are not adultery, or message electronic numbering large, the message continued.';
	$lang['sk_perilakututor6']		= 'Tutor and every Users Service banned distribute viruses or Whole technologist similar to Damaging and / or Disadvantageous site, affiliates, providers, and Users more.';
	$lang['sk_perilakututor7']		= 'Tutor and every Users service banned enter or move Features on Site no Exception without To your knowledge and approval of Us.';
	$lang['sk_perilakututor8']		= 'Tutor and every Users service banned store, reproduce, modify, or spread Content and Features Site, including way services, content, rights create   and intellectual contained on Site.';
	$lang['sk_perilakututor9']		= 'Tutor and every Users service banned take or Collect information from Users He was in, including email address, without To your knowledge other users.';


	$lang['sk_informasitutor']		= '6. Use of Information';
	$lang['sk_informasitutor1']		= "The use of Tutor, Ambassador and Student account information for Indiclass.com or Indonesian Future Leaders is permitted on a permanent basis by Indiclass.com's consent without going through intermediaries and all matters relating to information are the rights of Indiclass.com fully.";
	$lang['sk_informasitutor2']		= 'With Participate in Indiclass.com This, then Means Registrant Has   read and Approve Requirement and applicable regulations. Indiclass.com   Right   for   using data and the information you provide.';
	$lang['sk_informasitutor3']		= 'We reserve the right to restrict or deny access, or provide different access to be able to open the Site and its features to each User, or to replace any of the features or include new features without prior notice. Each User is aware that if the Site can not be used in whole or in part for any reason, any attempt or activity of any User may be impaired. Each User hereby agrees that for any reason exempts Us from any form of liability to the User or to any third party in the event that the person concerned may not use the Site (whether due to any interruption, restricted access, modification of features or exclusion of certain features or due other reason); Or if communications or transmissions are delayed, fail or can not take place; Or if there is a loss (directly, indirectly) due to the use or non-use of the Site or any of the features thereof.';
	$lang['sk_informasitutor4']		= 'Users with this knowing and Approve that use Site this   Is Responsibility Answer and Risk from Users and by kare na that   Users with this Let go or We liberate from and to   all or all form Accountability loss on use Site.';



	//Main Menu
	$lang['myclass'] 				= 'My Class';
	$lang['home'] 					= 'Home';
	$lang['subject'] 				= 'Subject';
	$lang['allsub'] 				= 'All Subject'; 
	$lang['ondemand'] 				= 'Extra Class';
	$lang['history'] 				= 'History';
	$lang['profil'] 				= 'Profile';
	$lang['schedule'] 				= 'Schedule';
	$lang['viewprofile'] 			= 'View Profile';
	$lang['privacysetting'] 		= 'Privacy Settings';
	$lang['settings'] 				= 'Settings';
	$lang['logout'] 				= 'Logout';
	$lang['togglefullscreen'] 		= 'Toggle Fullscreen';
	$lang['no_schedule'] 			= "There is no schedule this week";
	$lang['no_class'] 				= "No Class";
	$lang['no_program']				= "No available program/offer for you right now";
	$lang['nothing_here'] 			= "There is nothing here, your account has not been approved";
	$lang['no_subjects'] 			= "There is no subject for your educational level";
	$lang['no_subjectskids']		= "There is no lesson for your Child's education level";
	$lang['no_tutor_here']			= "No Tutors in Lessons ";
	$lang['checkdemand'] 			= "By pressing SELECT button then the class request is automatically delivered to the Tutor concerned. If the Tutor receives your request, then you will receive the Email payment details of the class you have requested.";
	$lang['competency']				= 'Competency';
	$lang['notification']			= 'Notification';
	$lang['error']					= 'Error!';
	$lang['alreadyonline'] 			= "You can only enter one classroom in the same time";
	$lang['alreadyactivation'] 		= "Your email has been activated previously. If you forget your password, kindly click Forgot Password link below.";

	//Footer
	$lang['reports'] 				= 'Reports';
	$lang['support'] 				= 'Support';
	$lang['contact'] 				= 'Contact';
	$lang['copyright'] 				= 'Copyright';

	//Page Index
	$lang['title_pg_index'] 		= 'Home';

	//Page Subject
	$lang['title_pg_subject'] 		= 'Subjects';
	$lang['follow'] 				= 'Follow';
	$lang['unfollow']				= 'Unfollow';
	$lang['followed'] 				= 'Followed';
	$lang['no_tutor'] 				= 'Tutor are not available in these subjects';
	$lang['tutorname']				= 'Tutor Name';
	$lang['transactionlist']		= 'Details';
	$lang['yourrequest']			= 'Your Request';
	$lang['accountlist']			= 'Account list';
	$lang['paymentconfirmation']    = 'Payment Confirmation';
	$lang['underpaymentnotif']    	= 'There is less payment, please check your email for more details.';

	//Page OnDemand
	$lang['findfriends']			= 'Add Friends via email';
	$lang['titleinvite']			= 'Invite your Group Friends';
	$lang['note']					= 'Notes';
	$lang['note1']					= 'Minimal Invitation 1 Person';
	$lang['note2']					= 'Invite Maximum 3 People';
	$lang['note3']					= 'Make sure you have enough Balance';
	$lang['note4']					= 'Make sure your friends have enough Balance';
	$lang['note5']					= 'We will deduct your balance temporarily';
	$lang['note6']					= 'Maximum of your friends approve your call 2 Hours';
	$lang['searchduration'] 		= 'Search Duration';
	$lang['titlesearchpriv']		= 'Search Tutor - Private Class';
	$lang['titlesearchgrup']		= 'Search Tutor - Group Class';
	$lang['titleresult']			= 'Tutor Search Results';
	$lang['alertfirst']				= 'Tutor search results will appear here';
	$lang['demandprice']			= 'Price';
	$lang['demandname']				= 'Tutor';	
	$lang['demandtime']				= 'Time';	
	$lang['demanddate']				= 'Date';
	$lang['demandpayown']			= 'Paid by 1 student';
	$lang['demandpayshare']			= 'Split Equally';
	$lang['alertsubjectnull']		= 'Please select subjects !';
	$lang['alertdatenull']			= 'Please choose date !';
	$lang['alertdurationnull']		= 'Please select duration !';

	//Page All Subject
	$lang['title_pg_allsub'] 		= 'All Subject';
	$lang['select_class'] 			= 'Grade Level';
	$lang['select_level'] 			= 'Select Level Educational';
	$lang['status'] 				= 'Status';
	$lang['button_search'] 			= 'Search';

	//Page Profile
	$lang['noage'] 					= 'No Age';
	$lang['nogender'] 				= 'No Gender';
	$lang['noreligion'] 			= 'No Religion';
	$lang['noaddress'] 				= 'No Address';
	$lang['noemail'] 				= 'No Email';
	$lang['nonumber'] 				= 'No Number';
	$lang['nofamilynumber'] 		= 'No Family Number';
	$lang['nobirthday']				= 'No Birthdate';
	$lang['nobirthplace']			= 'No Birth Place';
	$lang['nousername']				= 'No Username';
	$lang['uploadcompleted'] 		= 'Upload Completed...';
	$lang['uploadphoto'] 			= 'Upload your photo';
	$lang['upload'] 				= 'Upload';
	$lang['tab_about'] 				= 'About';
	$lang['tab_account'] 			= 'Account';
	$lang['tab_cp'] 				= 'Change Password';
	$lang['tab_pr'] 				= 'Profile';
	$lang['tab_ps'] 				= 'School';
	$lang['school_jenjang'] 		= 'School level';
	$lang['school_name'] 			= 'School name';
	$lang['school_address'] 		= 'School address';
	$lang['upd_profpic'] 			= 'Update Profile Picture';
	$lang['summary']	 			= 'Summary';
	$lang['basic_info'] 			= 'Basic Information';
	$lang['contact_info'] 			= 'Contact Information';
	$lang['contact_left_side'] 		= 'Contact';
	$lang['totcon'] 				= 'Total Connection';
	$lang['edit'] 					= 'Edit';
	$lang['username'] 				= 'Username';
	$lang['gender'] 				= 'Gender';
	$lang['placeofbirth'] 			= 'Place of Birth';
	$lang['birthday'] 				= 'Birthdate';
	$lang['religion'] 				= 'Religion';
	$lang['full_name'] 				= 'Full Name';
	$lang['first_name'] 			= 'First Name';
	$lang['last_name'] 				= 'Last Name';
	$lang['rel_stat'] 				= 'Relationship Status';
	$lang['mobile_phone'] 			= 'Mobile Phone';
	$lang['email_address'] 			= 'Email Address';
	$lang['select_religion'] 		= 'Select Religion';
	$lang['postalcode'] 			= 'ZIP Code';
	$lang['age'] 					= 'Age';
	$lang['phonefamily'] 			= 'Family Number';
	$lang['selectprovinsi'] 		= 'Select Province';
	$lang['selectkabupaten'] 		= 'Select Regency / City';
	$lang['selectkecamatan'] 		= 'Select Subdistrict';
	$lang['selectkelurahan'] 		= 'Select Village';
	$lang['password'] 				= 'Password';	
	$lang['passwordnotmatch'] 		= 'Passwords does not match';
	$lang['allsubject']				= 'All Subject';

	// TUTOR
	$lang['next'] 					= 'Next';
	$lang['previous'] 				= 'Previous';

	//Khusus Approval Tutor 
	$lang['select_religionn'] 		= 'Religion';
	$lang['numberid']				= "ID Number ";
	$lang['selectnumberid']			= "Driver's License / Passport";
	$lang['selectprovinsii']	 	= 'Province';
	$lang['selectkabupatenn'] 		= 'Regency / City';
	$lang['selectkecamatann'] 		= 'Subdistrict';
	$lang['selectkelurahann'] 		= 'Village';
	$lang['selectgrade'] 			= 'Grade level';
	$lang['selectlevel'] 			= 'Level educational';
	$lang['inputtahun']				= 'Input the correct year';
	$lang['next']					= 'Next';
	$lang['back']					= 'Back';
	$lang['submitapproval']			= 'Submit for approval';
	$lang['not_graduated']			= 'Not graduated yet';
	$lang['fotowajah']				= 'Please upload a photo of yourself which clearly shows your face';

	//Khusus AGAMA

	$lang['Islam'] 					= 'Moslem';
	$lang['Protestan'] 				= 'Christian Protestant';
	$lang['Katolik'] 				= 'Catholic';
	$lang['Hindu'] 					= 'Hindu';
	$lang['Buddha'] 				= 'Buddha';
	$lang['Konghucu'] 				= 'Confucianism';

	//On Account Tab
	$lang['select_gender'] 			= 'Select Gender';
	$lang['home_address'] 			= 'Home Address';
	$lang['old_pass'] 				= 'Old Password';
	$lang['new_pass'] 				= 'New Password';
	$lang['confirm_pass']	 		= 'Confirm Password';
	$lang['button_save'] 			= 'Save';
	$lang['button_cancel'] 			= 'Cancel';

	//profile tutor
	$lang['educational_level'] 		= 'Level';
	$lang['educational_competence'] = 'Competence';
	$lang['educational_institution']= 'Institution';
	$lang['institution_address'] 	= 'Institution Address';
	$lang['graduation_year'] 		= 'Graduation Year';
	$lang['description_experience'] = 'Major / Subject';
	$lang['year_experience'] 		= 'Period';
	$lang['place_experience'] 		= 'Name Institution';
	$lang['information_experience'] = 'Additional Information';
	$lang['fromyear'] 				= 'From Year';
	$lang['from'] 					= 'From';
	$lang['toyear'] 				= 'To Year';
	$lang['to'] 					= 'To';
	$lang['latbelpend'] 			= 'Educational Background';
	$lang['pengalaman_mengajar']	= 'Teaching Experience';
	$lang['selfdescription'] 		= 'Self Description';
	$lang['nodescription']			= 'No Description';

	//Main Menu Tutor
	$lang['hometutor'] 				= 'Dashboard';
	$lang['setavailabilitytime']	= 'Set Availability Time';
	$lang['selectsubjecttutor'] 	= 'Select Subject';
	$lang['approvalondemand']		= 'Approval Ondemand';
	$lang['select'] 				= 'Select';
	$lang['selected'] 				= 'Selected';
	$lang['profiltutor'] 			= 'Profile';
	$lang['description_tutor'] 		= 'Description Tutor';
	$lang['report']					= 'Report';
	$lang['reportteaching']			= 'Tutor Report';


	//Main Menu Admin
	$lang['homeadminn'] 			= 'Dashboard';
	$lang['approval_tutor'] 		= 'Approval ( Tutor )';
	$lang['verify_subject'] 		= 'Verify ( Tutor Subject )';
	$lang['unverify_subject'] 		= 'UnVerify ( Tutor Subject )';
	$lang['schedule_engine'] 		= 'Schedule Engine';
	$lang['buttonapproval'] 		= 'Submit Approval';
	$lang['approval'] 				= 'Complete Profile';
	$lang['approve'] 				= 'Approve';
	$lang['decline'] 				= 'Decline';
	$lang['adminsupport'] 			= 'Bantuan';
	$lang['adminaddsubject'] 		= 'Add subject';
	$lang['adminreferralcode'] 		= 'Referral code';
	$lang['adminaddreferral'] 		= 'Add referral';
	$lang['adminusereferral'] 		= 'Use referral';
	$lang['adminpayment'] 			= 'Payment';
	$lang['adminclassmulticast'] 	= 'Class multicast';
	$lang['adminclassprivate'] 		= 'Class private';
	$lang['adminclassgroup'] 		= 'Class group';
	$lang['adminchannel'] 			= 'Channel';
	$lang['adminregisterchannel'] 	= 'Register channel';
	$lang['adminpointchannel'] 		= 'Point channel';
	$lang['adminusepointchannel'] 	= 'Use point channel';
	$lang['adminactivitychannel'] 	= 'Activity channel';
	$lang['adminpocketmoney'] 		= 'Pocket money';
	$lang['admindatatransaction'] 	= 'Data transaction';
	$lang['adminconfirmtransaction']= 'Confirm transaction';
	$lang['adminannouncement']		= 'Announcement';
	$lang['adminalltutor']			= 'All Tutor';
	$lang['tambahadminkuotakelas']	= 'Request Class Quota';
	$lang['adminkuotakelas']		= 'List Class Quota';

	//choosesubjectutor
	$lang['ikutisubject'] 			= 'Choose';
	$lang['tidakikutisubject'] 		= 'unChoose';

	//else
	$lang['subdisc'] 				= "Sub Discussion";

	//quizmenu
	$lang['sidequiz']				= "Quiz";
	$lang['quizlist']				= "List Quiz";

	//choosee
	$lang['confrimdemand'] 			= 'Confirm Class';
	$lang['confrimpayment'] 		= 'Detail Order';
	$lang['canceldemand'] 			= 'Cancel';
	$lang['ikutidemand'] 			= 'Choose';
	$lang['diikutidemand'] 			= 'Choosed';
	$lang['nohistory'] 				= 'No History';
	$lang['duration'] 				= 'Duration';
	$lang['searchtime'] 			= 'Search Time';
	$lang['searchdate'] 			= 'Search Date';
	$lang['cancel'] 				= 'Cancel';
	$lang['nodata'] 				= 'No Data';
	$lang['loading'] 				= 'Please wait...';

		//resend Email
	$lang['resendaccount']			= 'Resend Account Activation';
	$lang['enteremail']				= "Enter your email address below and we'll resend";
	$lang['accountactivation']		= 'the account activation email';

		//approval tutor
	$lang['photo']					= 'Profile Photo';
	$lang['photoupload']			= 'Upload Photo';
	$lang['usecamera']				= 'Use Camera';
	$lang['capture']				= 'Capture';
	$lang['retakecapture']			= 'Retake Capture';
	$lang['tambahdata']				= 'Add';
	$lang['hapusdata']				= 'Delete';

		//aprovalondemand
	$lang['norequest'] 				= 'No request';
	$lang['areyousure']			 	= 'Are you sure';
	$lang['yes']					= 'Yes';
	$lang['no']						= 'No';
	$lang['aksi']					= 'Action';
	$lang['kelas']					= 'Class';

	$lang['uangsaku']				= 'E-Pocket';
	$lang['chooselanguage']			= 'Choose language';
	$lang['passed']					= 'Passed';

	//SUBJECT_NAME
	$lang['subject_4'] 				= "Bahasa";
	$lang['subject_5'] 				= "Bahasa";
	$lang['subject_6'] 				= "Math";
	$lang['subject_7'] 				= "Math";
	$lang['subject_8'] 				= "English";
	$lang['subject_9'] 				= "English";
	$lang['subject_10'] 			= "Science";
	$lang['subject_11'] 			= "Social Science";
	$lang['subject_12'] 			= "Sport";
	$lang['subject_13'] 			= "Citizen Education";
	$lang['subject_15'] 			= "Computer";
	$lang['subject_408'] 			= "Finance";
	$lang['subject_16'] 			= "Cooking Class";
	$lang['subject_404'] 			= "Room Tester";

	//REPLAY
	$lang['replay']					= "Replay";
	$lang['replaytitle']			= "Play videos anywhere and anytime";

	// ------------------------- KHUSUS ALERT-------------------------------- //

	// ini untuk aksi login
	$lang['ceksandi']				= 'Your passwords do not match';
	$lang['wrongpassword']			= 'Wrong email or password';
	$lang['checkemail'] 			= 'Please check your email to verify this account';
	$lang['completeprofile']		= 'Please complete your profile for tutor registration approval';
	$lang['requestsent']			= 'Your request has been sent, please wait for approval from Indiclass Team. Most probably, we will try to contact you through email or your phone number. Thank you.';
	$lang['youremail'] 				= 'Your email is previously registered, please check your email for account activation or click to resend the email here';
	$lang['youremail2'] 			= 'Your email is previously already registered, please check your email for account activation';
	$lang['youremail3']				= 'Please check your email for account activation or click to resend the email ';

	//ini untuk daftarbaru
	$lang['previously1']			= 'Your email is previously registered, please check your email for account activation or click';
	$lang['previously2']			= 'to resend the email';
	$lang['accountregistered']  	= 'account has been registered and activated previously, click';
	$lang['accountregistered2']  	= 'account has been registered and activated previously';
	$lang['successfullyregistered']	= 'Successfully registered, please check your email for account activation';
	$lang['captcha'] 			 	= 'Please check your reCaptcha';
	$lang['errorregister']			= 'Error register';
	$lang['successfullyregis']		= 'Succesfully Registered';
	$lang['checkemail']				= 'Please, check your email to verify this account';
	$lang['completetutorapproval']	= 'Please, complete your profile for tutor registration approval';
	$lang['linkexpired'] 			= 'Link already expired';
	$lang['approvesuccess']			= 'Approve success';

	//ini untuk resendemail
	$lang['successfullysent']		= 'Successfully sent, please check your email for account activation process.';
	$lang['failed']					= 'Failed';

	//ini untuk daftartutor	
	$lang['emailresend']			= 'to resend the email';	
	$lang['tologin']				= 'to login';
	$lang['tutorregistered']		= 'Successfully registered, please check your email for verification process.';
	$lang['tutoraccountregistered'] = 'Email has been registered and activated before, please use another email to register';

	//ini untuk aksi ubah password
	$lang['failedpassword'] 		= 'Failed, confirm password doesn\'t match';
	$lang['oldpasswordfailed']		= 'Failed, Your old password was wrong';
	$lang['successfullypassword']   = 'Your password successfully changed';
	$lang['successfullyselecting']  = 'Successfully selecting tutor and subjects';
	$lang['unfollowsuccessfully']   = 'Successfully unfollow';
	$lang['followedtutor'] 			= 'You have followed the tutor and its subjects';
	$lang['failedselect']			= 'Failed to select tutor and subjects';

	//ini untuk choosesub
	$lang['successfullychoose'] 	= 'Successfully choose subjects';
	$lang['failedchoosen'] 			= 'Failed, you have chosen that subject';
	$lang['erroroccured'] 			= 'Error Occured !';

	//ini untuk unchoosesub
	$lang['successfullyunfollow'] 	= 'Successfully unfollow subjects';
	$lang['failedunchoose']			= 'Failed, you have unchoose that subject';

	//ini untuk sendlinkweb+android
	$lang['failedsendemail'] 		= 'Email failed to send, your email is not registered';
	$lang['sentlink']				= 'Email successfully sent, please check your email';

	//ini untuk aksiforgot
	$lang['failedchange'] 			= 'Your password change has failed';
	$lang['registered']			 	= 'Successfully registered!';

	$lang['logsuccess'] 			= 'Log Out Success';
	$lang['settingdone']			= 'Setting Done';
	$lang['enterclass']				= 'Enter Class';
	$lang['successfullyupdate']		= 'Succesfully update your account';
	$lang['failedupdate']			= 'Failed to update your account';
	$lang['successfullydemand']		= 'Your class request is pending approval from the Tutor. and Payments wait until our tutor receives your class.';
	$lang['successaddtocart']		= 'Class successfully entered into Shopping Cart';
	$lang['faileddemand'] 			= 'Demand request failed';
	$lang['approvalcomplete']		= 'Approval Complete!';
	$lang['approvalfailed']			= 'Approval Failed!';
	$lang['saved']					= 'Saved.';
	$lang['successfullyprofile']	= 'Profile Succesfully Updated.';
	$lang['titledemandconfrim']		= 'Class Purchase';

	// ini untuk tutor
	$lang['registrationfailed']		= 'Tutor registration failed. Please contact us.';
	$lang['step']					= 'Step';
	$lang['account']				= 'Account';
	$lang['settime']				= 'Set Time';

	//ini untuk admin
	$lang['welcomeadmin']			= 'Welcome admin ';
	$lang['noempty']				= 'No empty';
	$lang['multigrade'] 			= 'Multi Grades';

	// ini untuk log_avtime
	$lang['sukses_delete']				= 'Successfully Deleted.';

	// ---------------------------END ALERT--------------------------------- //



	// ------------------------- START RIWAYAT ATAU LOG USER ------------------------------ //

	$lang['choosesubject']			= 'Choosing subjects';
	$lang['classenter']				= 'Enter Class';	

	// ------------------------- END RIWAYAT ATAU LOG USER -------------------------------- //


	// ------------------------- START UANG SAKU ------------------------------ //

	$lang['menu1']					= 'Detail';
	$lang['menu2']					= 'List Rekening';
	$lang['menu3']					= 'Top up';
	$lang['menu4']					= 'How to Top Up';

	$lang['yourbalance']			= 'Your balance';
	$lang['usebalance']				= 'Balance use';
	$lang['detailusages']			= 'Details Usages';

	// ISI PENGGUNAAN SALDO
	$lang['tanggal']				= 'Date';
	$lang['keterangan'] 			= 'Information';
	$lang['debet']					= 'Debet';
	$lang['kredit']					= 'Credit';
	$lang['saldoawal']				= 'Beginning Balance';
	$lang['saldoakhir']				= 'Ending Balance';
	$lang['detailtopup']			= 'Details Top Up';
	$lang['jumlah']					= 'Amount';
	$lang['metodepembayaran']		= 'Payment Method';
	$lang['notransaction']			= 'No transactions Top up';
	$lang['pendingtransaction']		= 'Waiting for payment';
	$lang['suksestransaction']		= 'Payment successful';
	$lang['ubahkonfirmasi']			= 'Change Confirmation';
	$lang['tidakadadata']			= 'No data found';
	$lang['konfirmasi']				= 'Confirmation';
	$lang['deskripsitopup']			= 'Fill your balance for purchases quickly and easily.';
	$lang['pembayaran']				= 'Payment';
	$lang['nominalinput']			= 'Input Nominal';
	$lang['pilihpayment']			= 'Select a payment method you want to use';

	$lang['transferbank']			= 'Bank Transfer';
	$lang['kartukredit']			= 'Credit Bank';
	$lang['selanjutnya']			= 'Next';
	$lang['transaksi']				= 'Transaction';
	$lang['segera']					= 'Immediately transfer the payment to add balance before :';
	$lang['segerabayar']			= 'Immediately transfer the payment before :';
	$lang['expiredtext']			= 'Before the deadline below :';
	$lang['amountpayment']			= 'Transfer payment immediately:';
	$lang['bedatransfer']			= 'Transfer rate differences will hinder the process of verification';
	$lang['konfirmasipembayaran']	= 'Payment Confirmation';
	$lang['konfirmasiunderpayment']	= 'Confirm underpayment';
	$lang['pembayaranmelalui']		= 'Payment can be made to one of the following accounts:';
	$lang['notes']					= 'Notes';
	$lang['notesdes']				= "Top up your balance considered CANCEL if until o'clock";
	$lang['notesorder']				= "Your class considered CANCEL if until o'clock";
	$lang['hari']					= 'Day';
	$lang['notpaid']				= '(2 × 12 hours) is not paid';
	$lang['notpaidorder']			= '(1x1 hours) tidak dibayar.';
	$lang['kembali']				= 'Back';
	$lang['notif_waitingpayment'] 	= 'Waiting for your payment';
	$lang['notif_successpayment'] 	= 'Payment completed';
	$lang['notif_classexpired'] 	= 'Payment has been overdue';
	$lang['notif_classexpired2']	= 'If you have made payment, please make payment confirmation above. And then contact customer service or email to finance@meetaza.com with your full name, email / username and proof of transfer.';
	$lang['notif_underpayment'] 	= 'Waiting for lack of payment';

	//KONFIRMASI
	$lang['tujuantransfer']			= 'Transfer destination';
	$lang['pilih']					= 'Select';
	$lang['jumlahtransfer']			= 'Transfer amount';
	$lang['inputjumlah']			= 'Enter transfer amount';
	$lang['jumlahharus']			= 'Amount that should be paid ';
	$lang['fromrekening']			= 'From rekening';
	$lang['masukanrekening']		= 'Enter your account number';
	$lang['namadirekening']			= 'The name on the rekening';
	$lang['denganmetode']			= 'With methods';
	$lang['pilihmetode']			= 'Select method of transfer'; 
	$lang['buktipembayaran']		= 'Payment proof';
	$lang['peringatan']				= '( No need to be filled if the evidence previously it is correct )';
	$lang['totalpayment']			= 'Total Payment';
	$lang['besarpayment']			= 'Sebesar';

	//NOTIF
	$lang['student'] 				= "Student";
	$lang['ask_class'] 				= "ask for extra class of class";
	$lang['has_call_support'] 		= "has requested to call support.";
	$lang['readysupport'] 			= "call support is ready to help you, click ";
	$lang['starting'] 				= "to start.";
	$lang['finishverifikasi'] 		= "Yeay verification has been successful, please choose subjects and teachers to start a private class.";
	$lang['tutorsuccessregis'] 		= "Successfully registered, please check your email for verification process.";
	$lang['successverifikasitutor'] = "Verification has been successful, please fill in your details so we can immediately process.";
	$lang['successsendapproval'] 	= "Your request has been sent, please wait for approval from Tim Indiclass. thanks.";	

	$lang['heyadmin'] 				= "Hey admin";
	$lang['heyadmin2'] 				= "There is a new Tutor registration, please check the menu approval.";
	$lang['tutornotcoming'] 		= 'Please contact our support back because you do not answer our calls.';
	$lang['paymentmulticast'] 		= 'There is a new transaction in the multicast transaction menu. Immediately do the examination.';
	$lang['paymentprivate'] 		= 'There is a new transaction in the private transaction menu. Immediately do the examination.';
	$lang['paymentgroup'] 			= 'There is a new transaction in the group transaction menu. Immediately do the examination.';
	$lang['notifpaymentstudent']	= 'Thank you for purchasing a class in Indiclass. Payment details can be clicked here.';
	$lang['alertdibawahwaktu'] 		= "You already have class in the time range you are setting.Please, select different time or duration.";
	$lang['alertlimakali']			= "Sorry, Before getting approval from our side, you are only allowed to make class 5 times.";
	$lang['alertalreadyclass'] 		= "You already have class in the time range you are setting.";

	$lang['yakinkeluar']			= "Are you sure want to quit the Classroom?";
	$lang['ubahcamera']				= "Change Video Devices";

	$lang['infosucces_groupuser'] 	= "Payment of your group class SUCCESS we received. Classes can start ";
	$lang['infosucces_grouptouser'] = "Yeay your group class is SUCCESSFULLY made. Classes can start.";
	$lang['infosucces_grouptotutor1']= "Class";
	$lang['infosucces_grouptotutor2']= "successfully made and the money has gone into your balance. Class will start at";
	$lang['waitingpaymentuser1']	= "You have successfully approved the student group class request";
	$lang['waitingpaymentuser2']	= "and wait for the payment by the student.";
	$lang['alertrejectclass1']		= "We're sorry, the group's invite class request has been DENIED by the tutor, ";
	$lang['alertrejectclass2']		= "you can still search for other tutors in the Extra class menu";


	// -------------------------- END UANG SAKU ------------------------------- //

	// -------------------------- LANDING PAGE  ------------------------------- // 
	$lang['navbartop1']				= "SIGN UP";
	$lang['navbartop2']				= "LOGIN";

	$lang['banneratas1'] 			= "Comprehensive interactive online learning features as well as a wide selection of tutors or teachers.";
	$lang['banneratas2']			= "Available on :";

	$lang['bannert1'] 				= "Delivers a very interactive learning atmosphere just like in a real classroom.";
	$lang['bannert2'] 				= "Makes learning material so much easier to understand as using a real whiteboard.";
	$lang['bannert3'] 				= "Tutor can easily sharing his/her screen display to make it easier for lesson delivery.";
	$lang['bannert4'] 				= "Students can ask face-to-face with tutor online.";
	$lang['bannert5'] 				= "Allows realtime chat among participants.";

	$lang['bannerb1'] 				= "Enter classes not only must be morning ...! In Indiclass you are free to set your own time for the class you want to follow, adjust to the time you have, anytime.";
	$lang['bannerb2']				= "In Indiclass you are free to ask, discuss with the reliable and fun tutors about lessons that you have not understood, as close to discussing with friends or even your parents.";
	$lang['bannerb3']				= "For those who want private lessons, Indiclass also provides that service with teachers who are varied, fun and certainly very experienced.";
	$lang['bannerb4']				= "Now the distance is no longer an obstacle for you who want to learn the group, invite your friends to follow the same class in Indiclass. Study in groups is now easier to do.";

	$lang['bannerdapat'] 			= "Accessible ";
	$lang['bannerdimana']			= "Anywhere "; 
	$lang['dan']					= " and ";
	$lang['bannerkapan']			= "Anytime";
	$lang['bannerunduh']			= "Download the Indiclass app right now.";

	$lang['bannertentang'] 			= "About Site";
	$lang['bannersyarat']			= "Terms and Conditions";
	$lang['bannerbawah1']			= "Indiclass provides interactive online learning services with many choices of virtual classrooms.";
	$lang['bannerbawah2']			= "Indiclass enable interactive communication between teacher and students as well as among students.";




	// Khusus Channel

	$lang['settingchannel']			= "Setting Channel";
	$lang['klik']					= "Click";
	$lang['kechannel']				= "to go to your channel page";
	$lang['welcomechannel']			= "Welcome to Channel, ";
	$lang['channelname']			= "Channel Name";
	$lang['channeldescription']		= "Channel Description";
	$lang['channelphone']			= "Channel Contact";
	$lang['channeladdress']			= "Channel Address";
	$lang['channellink']			= "Channel Link";
	$lang['channelabout']			= "About Channel";
	$lang['channelemail']			= "Email Channel";
	$lang['changelogo']				= "Change Channel Logo";
	$lang['changebanner']			= "Change Banner Logo";
	$lang['maxsizelogo']			= "Maximal File 2 MB.";
	$lang['sizelogo']				= "Size of logo must be 512 x 512 pixels or bigger.";
	$lang['maxsizebanner']			= "Maximal File 5 MB.";
	$lang['sizebanner']				= "Size of logo must be 1386 x 462 pixels or bigger.";
	$lang['successchangedata']		= "Successfully Changing Data";
	$lang['failedchangedata']		= "The channel link name already exists, please replace it with another";
	$lang['successupload']			= "Failed to upload file";
	$lang['failedupload']			= "Success to upload file";
	$lang['loginfirst']				= "Please login first before purchasing the package";

	// Khusus KIDS

	$lang['mykids']					= "My Kids";
	$lang['mykids_registrasion']	= "Registration";
	$lang['mykids_class']			= "Class";
	$lang['mykids_subject']			= "Subject";
	$lang['mykids_demand']			= "Extra Class";
	$lang['mykids_ondemand']		= "Data Extra Class";