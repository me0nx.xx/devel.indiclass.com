<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	/*
	Indonesia
	*/

	/* LOGIN */
	
	$lang['wlogin'] 				= 'Selamat datang di ';
	$lang['notaccountlogin'] 		= 'Belum punya akun? Silahkan daftar';
	$lang['here'] 					= 'disini';
	$lang['login'] 					= 'Masuk';
	$lang['forgotpassword'] 		= 'Lupa Password ?';
	$lang['description_loginfooter']= 'Classmiles menyediakan layanan pembelajaran online. Ini berisi banyak ruang kelas virtual untuk siswa yaitu SD (kelas 5-6), SMP (kelas 7-9) dan SMA (kelas 10-12). Classmiles juga menyediakan kursus umum seperti bahasa Inggris, Jepang, Arab, Cooking, Yoga, Tari, dll Classmiles memungkinkan komunikasi interaktif antara guru dan siswa serta antara siswa. Silakan mendaftar untuk menikmati sesi gratis kami kami sediakan sehari-hari.';
	$lang['withfacebook'] 			= 'Masuk dengan Facebook';
	$lang['withgoogle'] 			= 'Masuk dengan Google';
	$lang['remember']				= 'Ingat saya';

	// FORGOT 
	$lang['lupasandi'] 				= 'Lupa Kata sandi';	
	$lang['title1'] 				= 'Masukkan alamat email Anda di bawah ini ';
	$lang['title2'] 				= 'dan kami akan mengirimkan email petunjuk ';
	$lang['title3'] 				= 'tentang cara mengubah kata sandi Anda';
	$lang['inputemail'] 			= 'Masukkan email Anda';
	$lang['sendemail'] 				= 'Kirim Email';
	$lang['backlogin'] 				= 'Kembali ke login';

	// REGISTER

	$lang['sandisalah']				= 'Kata Sandi tidak cocok';
	$lang['sebagai']				= 'Daftar Sebagai :';
	$lang['signup'] 				= 'Pendaftaran';
	$lang['or']						= 'or';
	$lang['student'] 				= 'Pelajar';
	$lang['nonstudent'] 			= 'Umum';
	$lang['datebirth'] 				= 'Tanggal lahir';
	$lang['monthbirth'] 			= 'Bulan lahir';
	$lang['yearbirth'] 				= 'Tahun lahir';
	$lang['male'] 					= 'Pria';
	$lang['women'] 					= 'Wanita';
	$lang['countrycode'] 			= 'Kode negara';
	$lang['alreadyaccount'] 		= 'Sudah punya akun? Masuk';
	$lang['orsignup'] 				= 'Atau Anda dapat mendaftar melalui';
	$lang['signupfacebook'] 		= 'Mendaftar dengan Facebook';
	$lang['signupgoogle'] 			= 'Mendaftar dengan Google';
	$lang['invalidemail'] 			= 'Alamat Email salah';
	$lang['btnsignup']				= 'Daftar';
	$lang['btnsignups']				= 'Daftar Sebagai';		
	$lang['createpassword'] 		= 'Membuat Password';
	$lang['laststep']				= 'Langkah terakhir, mohon lengkapi data diri Anda pada kolom di bawah ini untuk keamanan dan kenyamanan bertransaksi selanjutnya.';
	$lang['syarat']					= 'Saya telah membaca dan menyetujui';
	$lang['syarat1']			 	= 'Syarat dan Ketentuan';
	$lang['syarat2']				= 'dan Kebijakan Privasi Classmiles.';
	$lang['activationsuccess']		= 'Aktivasi akun sukses';

	// MAIN MENU
	$lang['home'] 					= 'Kelas Saya';
	$lang['subject'] 				= 'Mata Pelajaran';
	$lang['allsub'] 				= 'Semua Mapel'; 
	$lang['ondemand'] 				= 'Kelas Tambahan';
	$lang['history'] 				= 'Riwayat';
	$lang['profil'] 				= 'Data Diri';
	$lang['schedule'] 				= 'Jadwal';
	$lang['viewprofile'] 			= 'Lihat Data Diri';
	$lang['privacysetting'] 		= 'Pengaturan Privasi';
	$lang['settings'] 				= 'Pengaturan';
	$lang['logout'] 				= 'Keluar';
	$lang['togglefullscreen'] 		= 'Layar Penuh';
	$lang['no_schedule'] 			= "Tidak ada jadwal untuk minggu ini";
	$lang['no_class'] 				= "Tidak ada kelas";
	$lang['nothing_here'] 			= "Tidak ada apa-apa di sini, akun Anda belum di setujui";
	$lang['no_subjects'] 			= "Tidak ada mata pelajaran untuk jenjang pendidikan Anda";
	$lang['checkdemand'] 			= "Apakah Anda yakin ingin memilih tutor ini ???";
	$lang['allsubject']				= 'Semua Pelajaran';
	$lang['competency']				= 'Kompetensi';
	$lang['notification']			= 'Notifikasi';
	$lang['error']					= 'Kesalahan!';

	//Footer
	$lang['reports'] 				= 'Laporan';
	$lang['support'] 				= 'Dukungan';
	$lang['contact'] 				= 'Kontak';
	$lang['copyright'] 				= 'Hak Cipta';

	//Page Index
	$lang['title_pg_index'] 		= 'Beranda';

	//Page Subject
	$lang['title_pg_subject'] 		= 'Mata Pelajaran';
	$lang['follow'] 				= 'Ikuti';
	$lang['unfollow']				= 'Berhenti ikuti';
	$lang['followed'] 				= 'Diikuti';
	$lang['no_tutor'] 				= 'Tidak ada tutor di mata pelajaran ini';
	$lang['tutorname']				= 'Nama Guru';
	$lang['transactionlist']		= 'Detail';
	$lang['accountlist']			= 'Daftar Rekening';
	$lang['paymentconfirmation']    = 'Konfirmasi Pembayaran';

	//Page All Subject
	$lang['title_pg_allsub'] 		= 'Semua Mapel';
	$lang['select_class'] 			= 'Pilih Tingkatan Kelas';
	$lang['select_level'] 			= 'Pilih Jenjang Pendidikan';
	$lang['status'] 				= 'Status';
	$lang['button_search'] 			= 'Cari';

	//Page Profile
	$lang['noage'] 					= 'Tidak ada umur';
	$lang['nogender'] 				= 'Tidak ada Jenis Kelamin';
	$lang['noreligion'] 			= 'Tidak ada Agama';
	$lang['noaddress'] 				= 'Tidak ada Alamat';
	$lang['noemail'] 				= 'Tidak ada Email';
	$lang['nonumber'] 				= 'Tidak ada Nomor Handphone';
	$lang['nofamilynumber'] 		= 'Tidak ada Nomor Keluarga';
	$lang['nobirthday']				= 'Tidak ada Ulang tahun';
	$lang['nobirthplace']			= 'Tidak ada Tempat lahir';
	$lang['nousername']				= 'Tidak ada Nama';
	$lang['uploadcompleted'] 		= 'Unggah Selesai...';
	$lang['uploadphoto'] 			= 'Unggah Foto Anda';
	$lang['upload'] 				= 'Unggah';

	$lang['tab_about'] 				= 'Tentang';
	$lang['tab_account'] 			= 'Akun';
	$lang['tab_cp'] 				= 'Ubah Kata Sandi';
	$lang['tab_pr'] 				= 'Data Diri';
	$lang['school_jenjang'] 		= 'Tingkatan Sekolah';
	$lang['upd_profpic'] 			= 'Ganti Foto Profil';
	$lang['summary'] 				= 'Ringkasan';
	$lang['basic_info'] 			= 'Informasi Dasar';
	$lang['contact_info'] 			= 'Informasi Kontak';
	$lang['contact_left_side'] 		= 'Kontak';
	$lang['totcon'] 				= 'Jumlah Koneksi';
	$lang['edit'] 					= 'Sunting';
	$lang['username'] 				= 'Nama Pengguna';
	$lang['first_name'] 			= 'Nama Depan';
	$lang['last_name'] 				= 'Nama Belakang';
	$lang['full_name'] 				= 'Nama Lengkap';
	$lang['gender'] 				= 'Jenis kelamin';
	$lang['placeofbirth'] 			= 'Tempat Lahir';
	$lang['birthday'] 				= 'Tanggal Lahir';
	$lang['religion'] 				= 'Agama';
	$lang['rel_stat'] 				= 'Status Hubungan';
	$lang['mobile_phone'] 			= 'Nomor Ponsel';
	$lang['email_address'] 			= 'Alamat Email';
	$lang['select_religion'] 		= 'Pilih Agama';
	$lang['postalcode'] 			= 'Kode Pos';
	$lang['age'] 					= 'Umur';
	$lang['phonefamily'] 			= 'Nomor Orang Tua';	
	$lang['selectprovinsi'] 		= 'Propinsi';
	$lang['selectkabupaten'] 		= 'Pilih Kabupaten/Kota';
	$lang['selectkecamatan'] 		= 'Pilih Kecamatan';
	$lang['selectkelurahan'] 		= 'Pilih Kelurahan/Desa';
	$lang['password'] 				= 'Kata Sandi';	
	$lang['passwordnotmatch'] 		= 'Kata Sandi tidak cocok!';

	// TUTOR
	$lang['next'] 					= 'Selanjutnya';
	$lang['previous'] 				= 'Sebelumnya';

	//Khusus Approval Tutor 
	$lang['select_religionn'] 		= 'Agama';
	$lang['selectprovinsii'] 		= 'Provinsi';
	$lang['selectkabupatenn'] 		= 'Kabupaten/Kota';
	$lang['selectkecamatann'] 		= 'Kecamatan';
	$lang['selectkelurahann'] 		= 'Kelurahan';
	$lang['selectgrade'] 			= 'Tingkatan Kelas';
	$lang['selectlevel'] 			= 'Tingkat Pendidikan';

	//On Account Tab
	$lang['select_gender'] 			= 'Jenis kelamin';
	$lang['homeaddress'] 			= 'Alamat';
	$lang['old_pass'] 				= 'Kata Sandi Lama';
	$lang['new_pass'] 				= 'Kata Sandi Baru';
	$lang['confirm_pass'] 			= 'Konfirmasi Kata Sandi Baru';
	$lang['button_save'] 			= 'Simpan';
	$lang['button_cancel']			= 'Batal';

	//profile tutor
	$lang['educational_level'] 		= 'Tingkat';
	$lang['educational_competence'] = 'Kompetensi';
	$lang['educational_institution']= 'Lembaga';
	$lang['institution_address'] 	= 'Alamat Institusi';
	$lang['graduation_year'] 		= 'Tahun Pendidikan';
	$lang['description_experience'] = 'Deskripsi';
	$lang['year_experience'] 		= 'Periode';
	$lang['place_experience'] 		= 'Tempat';
	$lang['information_experience'] = ' Informasi Tambahan';
	$lang['fromyear'] 				= 'Dari Tahun';
	$lang['from'] 					= 'Dari';
	$lang['toyear'] 				= 'Sampai Tahun';
	$lang['to'] 					= 'Sampai';
	$lang['pengalaman_mengajar'] 	= 'Pengalaman Mengajar';
	$lang['latbelpend'] 			= 'Latar Belakang Pendidikan';
	$lang['selfdescription'] 		= 'Deskripsi Diri';
	$lang['nodescription']			= 'Tidak ada Deskripsi';

	//Main Menu Tutor
	$lang['hometutor'] 				= 'Dashboard';
	$lang['setavailabilitytime'] 	= 'Atur Ketersediaan Waktu';
	$lang['selectsubjecttutor'] 	= 'Pilih Mata Pelajaran';
	$lang['approvalondemand'] 		= 'Persetujuan Kelas Tambahan';
	$lang['select'] 				= 'Pilih';
	$lang['selected'] 				= 'Telah dipilih';
	$lang['profiltutor'] 			= 'Data Diri';
	$lang['description_tutor'] 		= 'Deskripsi Guru';

	//Main Menu Admin
	$lang['homeadminn'] 			= 'Dashboard';
	$lang['aktivasiuser'] 			= 'Persetujuan Pendaftaran Tutor';
	$lang['schedule_engine'] 		= 'Jadwal';
	$lang['buttonapproval'] 		= 'Kirim Persetujuan';
	$lang['approval'] 				= 'Lengkapi Profil';
	$lang['approve'] 				= 'Setuju';
	$lang['decline'] 				= 'Tolak';

	//choosesubjectutor
	$lang['ikutisubject'] 			= 'Pilih';
	$lang['tidakikutisubject'] 		= 'Berhenti Pilih';

	//else
	$lang['subdisc'] 				= "Sub Bahasan";

	//choosee
	$lang['ikutidemand'] 			= 'Pilih';
	$lang['diikutidemand'] 			= 'Dipilih';
	$lang['nohistory'] 				= 'Tidak ada Riwayat ';
	$lang['duration'] 				= 'Durasi';
	$lang['searchtime'] 			= 'Cari Waktu';
	$lang['searchdate'] 			= 'Cari Tanggal';
	$lang['cancel'] 				= 'Batal';
	$lang['nodata'] 				= 'Tidak ada data';
	$lang['loading']        		= 'Mohon tunggu...';

	//resend Email
	$lang['resendaccount']			= 'Aktivasi Akun Kirim Ulang';
	$lang['enteremail']				= "Masukkan alamat email Anda di bawah ini dan kami akan mengirim ulang";
	$lang['accountactivation']		= 'email aktivasi akun';

	//approval tutor
	$lang['numberid']				= 'Nomor ID (KTP / SIM / PASPOR)';
	$lang['photo']				 	= 'Foto Profil';
	$lang['photoupload']			= 'Unggah Foto';
	$lang['usecamera']				= 'Gunakan kamera';
	$lang['capture']				= 'Ambil Foto';
	$lang['retakecapture']			= 'Ambil ulang Foto';

	//aprovalondemand
	$lang['norequest'] 				= 'Tidak ada permintaan';
	$lang['areyousure']			 	= 'Apakah Anda yakin';
	$lang['yes']					= 'Ya';
	$lang['no']						= 'Tidak';
	$lang['aksi']					= 'Aksi';
	$lang['kelas']					= 'Kelas';

	$lang['uangsaku']				= 'Uang Saku';
	$lang['chooselanguage']			= 'Pilih Bahasa';
	$lang['passed']					= 'Telah berlalu';
	
	//SUBJECT_NAME
	$lang['subject_4'] 				= "Bahasa Indonesia";
	$lang['subject_5'] 				= "Bahasa Indonesia";
	$lang['subject_6'] 				= "Matematika";
	$lang['subject_7'] 				= "Matematika";
	$lang['subject_8'] 				= "Bahasa Inggris";
	$lang['subject_9'] 				= "Bahasa Inggris";
	$lang['subject_10'] 			= "IPA (Ilmu Pengetahuan Alam)";
	$lang['subject_11'] 			= "IPS (Ilmu Pengetahuan Sosial)";
	$lang['subject_12'] 			= "Olahraga";
	$lang['subject_13'] 			= "PKn";
	$lang['subject_15'] 			= "Komputer";
	$lang['subject_16'] 			= "Kelas Memasak";
	$lang['subject_404'] 			= "Room Tester";

	//REPLAY
	$lang['replay']					= "Putar Ulang";
	$lang['replaytitle']			= "Memutar video dimana saja dan kapan saja."; 

	// ------------------------- KHUSUS ALERT-------------------------------- //

	// ini untuk aksi login
	$lang['ceksandi']				= 'Kata Sandi tidak cocok';
	$lang['wrongpassword']			= 'Email atau password Anda salah';
	$lang['checkemail'] 			= 'Silakan Periksa email Anda untuk memverifikasi akun';
	$lang['completeprofile']		= 'Silakan melengkapi profil Anda untuk persetujuan pendaftaran guru';
	$lang['requestsent']			= 'Permintaan Anda telah dikirim, silakan menunggu persetujuan dari Tim Classmiles. Paling mungkin, kami akan mencoba untuk menghubungi Anda melalui email atau nomor telepon Anda. Terima kasih.';
	$lang['youremail']				= 'Email Anda sebelumnya terdaftar, silahkan cek email Anda untuk aktivasi akun atau klik';
	$lang['youremail2'] 			= 'Email Anda sebelumnya telah terdaftar, silahkan cek email Anda untuk aktivasi akun';
	$lang['youremail3']				= 'Silahkan cek email Anda untuk aktivasi akun atau klik';

	//ini untuk daftarbaru
	$lang['previously1']			= 'Email Anda sebelumnya terdaftar, silahkan cek email Anda untuk aktivasi akun atau klik';
	$lang['previously2']			= 'untuk mengirim ulang email';
	$lang['accountregistered']  	= 'akun telah terdaftar dan diaktifkan sebelumnya, klik';
	$lang['accountregistered2']  	= 'akun telah terdaftar dan diaktifkan sebelumnya';
	$lang['successfullyregistered']	= 'Berhasil terdaftar, silahkan cek email Anda untuk aktivasi akun';
	$lang['captcha'] 			 	= 'Silakan periksa CAPTCHA Anda';
	$lang['errorregister']			= 'Kesalahan mendaftar';
	$lang['successfullyregis']		= 'Berhasil mendaftar';
	$lang['checkemail']				= 'Silakan, periksa email Anda untuk memverifikasi akun ini';
	$lang['completetutorapproval']	= 'Silakan, melengkapi profil Anda untuk persetujuan pendaftaran guru';
	$lang['linkexpired'] 			= 'Link sudah kedaluwarsa';
	$lang['approvesuccess']			= 'Setujui sukses';

	//ini untuk resendemail
	$lang['successfullysent']		= 'Berhasil dikirim, silahkan cek email Anda untuk proses aktivasi akun.';
	$lang['failed']					= 'Gagal';

	//ini untuk daftartutor
	$lang['previouslyregistered']	= 'Email Anda sebelumnya terdaftar, silahkan cek email Anda untuk aktivasi akun atau klik';
	$lang['emailresend']			= 'untuk mengirim ulang email';
	$lang['tutoraccountregistered'] = 'akun telah terdaftar dan diaktifkan sebelumnya, klik';
	$lang['tologin']				= 'untuk login';
	$lang['tutorregistered']		= 'Berhasil terdaftar, silahkan cek email Anda untuk proses verifikasi.';

	//ini untuk aksi ubah password
	$lang['failedpassword'] 		= 'Gagal, konfirmasi password tidak cocok';
	$lang['oldpasswordfailed']		= 'Gagal, password lama Anda salah';
	$lang['successfullypassword']   = 'Sandi Anda berhasil diubah';
	$lang['successfullyselecting']  = 'Berhasil memilih guru dan mata pelajaran';
	$lang['unfollowsuccessfully']   = 'Berhasil berhenti mengikuti';
	$lang['failedselect']			= 'Gagal untuk memilih guru dan mata pelajaran';

	//ini untuk choosesub
	$lang['successfullychoose'] 	= 'Berhasil memilih mata pelajaran';
	$lang['failedchoosen'] 			= 'Gagal, Anda telah memilih mata pelajaran tersebut';
	$lang['erroroccured'] 			= 'Terjadi kesalahan !';

	//ini untuk unchoosesub
	$lang['successfullyunfollow'] 	= 'Berhasil berhenti mengikuti pelajaran';
	$lang['failedunchoose']			= 'Gagal, Anda memiliki pelajaran yang sudah terjadwal';

	//ini untuk sendlinkweb+android
	$lang['failedsendemail'] 		= 'Gagal mengirim, email Anda tidak terdaftar';
	$lang['sentlink']				= 'Email berhasil dikirim, silahkan cek email Anda';

	//ini untuk aksiforgot
	$lang['failedchange'] 			= 'Gagal mengubah password Anda';
	$lang['registered']			 	= 'Berhasil terdaftar!';

	$lang['logsuccess'] 			= 'Berhasil keluar';
	$lang['settingdone']			= 'Pengaturan selesai';
	$lang['enterclass']				= 'Masuk Kelas';
	$lang['successfullyupdate']		= 'Sukses perbarui data Anda';
	$lang['failedupdate']			= 'Gagal perbarui data Anda';
	$lang['successfullydemand']		= 'Permintaan Berhasil Diminta';
	$lang['faileddemand'] 			= 'Permintaan permintaan gagal';
	$lang['approvalcomplete']		= 'Persetujuan lengkap!';
	$lang['approvalfailed']			= 'Persetujuan gagal!';
	$lang['saved']					= 'Disimpan.';
	$lang['successfullyprofile']	= 'Profil Berhasil Diperbarui.';

	// ini untuk tutor
	$lang['registrationfailed']		= 'Pendaftaran guru gagal. Silahkan hubungi kami.';
	$lang['step']					= 'Langkah';
	$lang['account']				= 'Akun';
	$lang['settime']				= 'Atur Waktu';
	$lang['noempty']				= 'Tidak boleh kosong';

	//ini untuk admin
	$lang['welcomeadmin']			= 'Selamat datang admin ';
	
	// ini untuk log_avtime
	$lang['sukses_delete']				= 'Berhasil menghapus.';

	// ---------------------------END ALERT--------------------------------- //

	// ------------------------- START RIWAYAT ATAU LOG USER ------------------------------ //

	$lang['choosesubject']			= 'Memilih mata pelajaran';
	$lang['classenter']				= 'Masuk Kelas';

	// ------------------------- END RIWAYAT ATAU LOG USER -------------------------------- //


	// ------------------------- START UANG SAKU ------------------------------ //

	$lang['menu1']					= 'Detail';
	$lang['menu2']					= 'Daftar Rekening';
	$lang['menu3']					= 'Top Up';
	$lang['menu4']					= 'Cara Top Up';

	// MENU DETAIL
	$lang['yourbalance']			= 'Saldo anda';
	$lang['usebalance']				= 'Penggunaan Saldo';
	$lang['detailusages']			= 'Detail Penggunaan';

	// DETAIL
	$lang['tanggal']				= 'Tanggal';
	$lang['keterangan'] 			= 'Keterangan';
	$lang['debet']					= 'Debit';
	$lang['kredit']					= 'Kredit';
	$lang['saldoakhir']				= 'Saldo Akhir';
	$lang['detailtopup']			= 'Rincian Top Up';
	$lang['jumlah']					= 'Jumlah';
	$lang['metodepembayaran']		= 'Metode Pembayaran';
	$lang['notransaction']			= 'Tidak ada Transaksi Top up';
	$lang['pendingtransaction']		= 'Menunggu pembayaran';
	$lang['suksestransaction']		= 'Pembayaran sukses';
	$lang['ubahkonfirmasi']			= 'Ubah Konfirmasi';
	$lang['tidakadadata']			= 'Tidak ada data';
	$lang['konfirmasi']				= 'Konfirmasi';
	$lang['deskripsitopup']			= 'Isi saldo anda untuk pembelian yang cepat dan mudah.';
	$lang['pembayaran']				= 'Pembayaran';
	$lang['nominalinput']			= 'Masukan Nominal';
	$lang['pilihpayment']			= 'Pilih metode pembayaran yang ingin digunakan';

	$lang['transferbank']			= 'Transfer Bank';
	$lang['kartukredit']			= 'Kartu Kredit';
	$lang['selanjutnya']			= 'Lanjut';
	$lang['transaksi']				= 'Transaksi';
	$lang['segera']					= 'Segera melakukan transfer pembayaran untuk tambah saldo sebelum :';
	$lang['bedatransfer']			= 'Perbedaan nilai transfer akan menghambat proses verifikasi';
	$lang['konfirmasipembayaran']	= 'Konfirmasi Pembayaran';
	$lang['pembayaranmelalui']		= 'Pembayaran dapat dilakukan ke salah satu rekening berikut:';
	$lang['notes']					= 'Catatan';
	$lang['notedes']				= 'Top up saldo Anda dianggap BATAL jika sampai pukul';
	$lang['hari']					= 'Hari';
	$lang['notpaid']				= '(2×12 jam) tidak dibayar.';
	$lang['kembali']				= 'Kembali';

	//KONFIRMASI
	$lang['tujuantransfer']			= 'Tujuan Transfer';
	$lang['pilih']					= 'Pilih';
	$lang['jumlahtransfer']			= 'Sejumlah';
	$lang['inputjumlah']			= 'Masukan jumlah transfer';
	$lang['jumlahharus']			= 'Jumlah yang harus dibayar ';
	$lang['fromrekening']			= 'Dari Rekening';
	$lang['masukanrekening']		= 'Masukan nomer rekening anda';
	$lang['namadirekening']			= 'Nama yang tertera di rekening';
	$lang['denganmetode']			= 'Dengan metode';
	$lang['pilihmetode']			= 'Pilih metode transfer';
	$lang['buktipembayaran']		= 'Bukti Pembayaran';
	$lang['peringatan']				= '( Tidak usah di isi jika bukti sebelumnya sudah benar )';
	$lang['totalpayment']			= 'Jumlah Pembayaran';


	//NOTIF
	$lang['student'] 				= "Siswa";
	$lang['ask_class'] 				= "meminta kelas tambahan untuk kelas";
	$lang['has_call_support'] 		= "telah melakukan request untuk melakukan panggilan support";
	$lang['readysupport'] 			= "call support sudah siap untuk membantu anda, klik ";
	$lang['starting'] 				= "untuk memulai.";
	$lang['finishverifikasi'] 		= "Yeay verifikasi telah berhasil, silahkan pilih mata pelajaran dan guru untuk memulai kelas private.";
	$lang['tutorsuccessregis'] 		= "Berhasil terdaftar, silahkan cek email Anda untuk proses verifikasi.";
	$lang['successverifikasitutor'] = "Verifikasi telah berhasil, Silahkan lengkapi data diri anda agar kami bisa segera memproses.";
	$lang['successsendapproval'] 	= "Permintaan Anda telah dikirim, silakan tunggu persetujuan dari Tim Classmiles. Terima kasih.";

	$lang['heyadmin'] 				= "Hai admin";
	$lang['heyadmin2'] 				= "Terdapat Tutor yang baru registrasi, silahkan cek di menu persetujuan.";
	$lang['tutornotcoming'] 		= 'Mohon untuk menghubungi support kami kembali dikarenakan anda tidak menjawab panggilan kami.';

	$lang['yakinkeluar']			= "Apakah anda yakin ingin keluar kelas?";
	$lang['ubahcamera']				= "Ubah perangkat video";

	// -------------------------- END UANG SAKU ------------------------------- //