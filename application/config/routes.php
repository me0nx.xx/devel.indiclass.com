<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']= 'First';
$route['login'] 			= "/First/login";
$route['logout'] 			= "/master/logout";
$route['c'] 				= "/First/c";
$route['ClassHistory'] 		= "/replay/classreplay";
$route['classroom2']		= "Classroom";
$route['landing'] 			= "/First/landing";
$route['syarat-ketentuan'] 	= "/First/syarat_ketentuan";
// $route['Classroom'] 		= "/Classroom";
$route['web'] 				= "/First/web";
$route['mobile'] 			= "/First/mobile";

//MENGHAPUS FIRST
$route['MyClass'] 			= "/First/MyClass";
$route['Subjects'] 			= "/First/subjects";
$route['Ondemand'] 			= "/First/ondemand";
$route['Dataondemand'] 		= "/First/data_ondemand";
// $route['Class'] 			= "/First/data_ondemand";
$route['Epocket'] 			= "/First/Epocket";
$route['Accountlist'] 		= "/First/Accountlist";
$route['EditBank'] 			= "/First/editbank";
$route['Topup'] 			= "/First/Topup";
$route['Transfer'] 			= "/First/Transfer";
$route['Konfirmasi'] 		= "/First/Konfirmasi";
$route['Details'] 			= "/First/Details";
$route['Sukses'] 			= "/First/sukses";
$route['Cara-Topup'] 		= "/First/Cara_topup";
$route['History'] 			= "/First/history";
$route['About'] 			= "/First/about";
$route['Profile-Account'] 	= "/First/profile_account";
$route['Profile-School'] 	= "/First/profile_school";
$route['UpdateProfile'] 	= "/First/profile_picture";
$route['School-Kids'] 		= "/First/school_kids";
$route['Profile'] 			= "/First/profile";
$route['SetProfile'] 			= "/First/set_profile";
$route['Register'] 			= "/First/register";
$route['RegisterTutor'] 	= "/First/tutor_register";
$route['RegisterEmail'] 	= "/First/register_email";
$route['class'] 			= "/First/Class";
$route['DetailPayment'] 	= "/First/DetailPayment";
$route['DetailOrder'] 		= "/First/DetailOrder";
$route['ConfirmPayment']	= "/First/ConfirmPayment";
$route['channel/(:any)']	= "/Channel/channel/$1";
$route['Operator']    		= "/Operator/index";
$route['Finance']    		= "/Finance/index";
$route['Quiz']    			= "/Quiz/Quiz";
$route['QuizLMS']    		= "/Quiz/QuizLMS";
$route['Courses']			= "/Quiz/Courses";
$route['Start']				= "/Quiz/Start";
$route['Reputation']		= "/First/Reputation";
$route['Complain']			= "/First/Complain";
$route['KidsRegister']		= "/First/KidsRegister";

//CHANNEL

$route['set_lang/(:any)'] 	= "first/set_lang/$1";
$route['404_override'] 		= '';
$route['V1.0.0'] 			= 'Rest/main';
$route['V1.0.0/(:any)'] 	= 'Rest/$1';
$route['V1.0.0/(:any)/(:any)'] = 'Rest/$1/$2';
$route['V1.0.0/(:any)/(:any)/(:any)'] = 'Rest/$1/$2/$3';
$route['V1.0.0/(:any)/(:any)/(:any)/(:any)'] = 'Rest/$1/$2/$3/$4';
$route['V1.0.0/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Rest/$1/$2/$3/$4/$5';
$route['V1.0.0/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Rest/$1/$2/$3/$4/$5/$6';
$route['V1.0.0/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Rest/$1/$2/$3/$4/$5/$6/$7';
$route['V1.0.0/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Rest/$1/$2/$3/$4/$5/$6/$7/$8';
$route['V1.0.0/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Rest/$1/$2/$3/$4/$5/$6/$7/$8/$9';
$route['V1.0.0/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Rest/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10';
$route['V1.0.0/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Rest/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11';
$route['V1.0.0/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Rest/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12';
$route['V1.0.0/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Rest/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12/$13';
// $route['api/v1'] = 'Kidsv1/main';
// $route['api/v1/(:any)'] = 'Kidsv1/$1';
// $route['api/v1/(:any)/(:any)'] = 'Kidsv1/$1/$2';
// $route['api/v1/(:any)/(:any)/(:any)'] = 'Kidsv1/$1/$2/$3';
// $route['api/v1/(:any)/(:any)/(:any)/(:any)'] = 'Kidsv1/$1/$2/$3/$4';
$route['api'] = 'Restv1';
//$route['api/'] = 'Restv1';
$route['api/v1'] = 'Restv1/index';
$route['api/v1/(:any)'] = 'Restv1/$1';
$route['api/v1/(:any)/(:any)'] = 'Restv1/$1/$2';
$route['api/v1/(:any)/(:any)/(:any)'] = 'Restv1/$1/$2/$3';
$route['api/v1/(:any)/(:any)/(:any)/(:any)'] = 'Restv1/$1/$2/$3/$4';
$route['api/v1/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Restv1/$1/$2/$3/$4/$5';
$route['api/v1/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Restv1/$1/$2/$3/$4/$5/$6';
$route['api/v1/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Restv1/$1/$2/$3/$4/$5/$6/$7';
$route['api/v1/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Restv1/$1/$2/$3/$4/$5/$6/$7/$8';
$route['api/v1/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Restv1/$1/$2/$3/$4/$5/$6/$7/$8/$9';
$route['api/v1/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Restv1/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10';
$route['api/v1/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Restv1/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11';
$route['api/v1/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Restv1/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12';
$route['api/v1/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Restv1/$1/$2/$3/$4/$5/$6/$7/$8/$9/$10/$11/$12/$13';
$route['approve_support'] = 'Admin/approve_support';
$route['translate_uri_dashes'] = FALSE;
