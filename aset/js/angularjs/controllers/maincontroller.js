app.controller('MainController',['$scope', function($scope){
  	// $scope.title='Belajar AngularJs';
  	$scope.title=10;

  	// $scope.book={
  	// 	title: 'Belajar AngularJS Bersama Ramdhani',
  	// 	author: 'Ramdhani',
  	// 	price: 8000,
  	// 	tanggal: new Date('2017','03','09')
  	// };
  	$scope.books=[
  	{
  		title: 'Belajar AngularJS Bersama Ramdhani',
  		author: 'Ramdhani',
  		price: 80000,
  		tanggal: new Date('2017','03','09'),
  		likes: 0
  	},
  	{
  		title: 'Belajar CodeIgniter',
  		author: 'Me',
  		price: 65000,
  		tanggal: new Date('2017','03','01'),
  		likes: 0
  	},
  	{
  		title: 'Belajar Github',
  		author: 'We',
  		price: 90000,
  		tanggal: new Date('2017','02','03'),
  		likes: 0
  	}];

  	$scope.logToConsole=function(index){
  		var book = $scope.books[index];
  		console.log(book);
  	};

  	$scope.likes=function(index){
  		$scope.books[index].likes+=1;
  	};

  	$scope.log=function(){
  		console.log($scope.title);
  	}

}]);