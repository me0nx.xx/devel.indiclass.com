
	var chatData = [];
    var transactions = {};
    var textroom = null;
    var chatParticipants = {};
    var chatReady = false;
    var classidcp = "";
    var idusercp = "";
    var displaynamecp = "";

    // ===========================================================================
    // ========================= CHAT FUNCTION DISINI ============================
    // ===========================================================================
 	function attachChatPlugin(Room, IdUser, Display) {
 		classidcp = Room;
 		idusercp = IdUser;
 		displaynamecp = Display;
      // Attach to echo test plugin
      console.warn("INIT CHAT "+JanusConfigService.pluginChatRoom);
      janus.attach(
        {
          plugin: JanusConfigService.pluginChatRoom,
          success: function (pluginHandle) {
            $('#details').remove();
            textroom = pluginHandle;
            Janus.log("Plugin attached! (" + textroom.getPlugin() + ", id=" + textroom.getId() + ")");
            // Setup the DataChannel
            var body = { "request": "setup", "room": 787488 };
            Janus.debug("Sending message (" + JSON.stringify(body) + ")");
            textroom.send({ "message": body });
          },
          error: function (error) {
            addNewLog(error, "ERROR");
          },
          webrtcState: function (on) {
            Janus.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
          },
          onmessage: function (msg, jsep) {
            Janus.debug(" ::: Got a message :::");
            Janus.debug(JSON.stringify(msg));
            if (msg["error"] !== undefined && msg["error"] !== null) {
              addNewLog(msg["error"], "ERROR");
            }
            if (jsep !== undefined && jsep !== null) {
              // Answer
              textroom.createAnswer(
                {
                  jsep: jsep,
                  media: { audio: false, video: false, data: true },	// We only use datachannels
                  success: function (jsep) {
                    var body = { "request": "ack" };
                    textroom.send({ "message": body, "jsep": jsep });
                  },
                  error: function (error) {
                    addNewLog(error, "ERROR");
                  }
                });
            }
          },
          ondataopen: function (data) {
            Janus.log("The DataChannel is available!");
            // Prompt for a display name to join the default room
            createRoomChat();
          },
          ondata: function (data) {
            Janus.debug("We got data from the DataChannel! " + data);
            handleMessage(data);
          },
          oncleanup: function () {
            Janus.log(" ::: Got a cleanup notification :::");
            $('#datasend').attr('disabled', true);
          }
        }
      );
    }

    function createRoomChat() {

      var transaction = randomString(12);
      var register = {
        textroom: "create",
        transaction: transaction,
        room: classidcp,
        description: "",
        is_private: false,
        pin: "",
        post: "",
        secret: "",
        permanent: false
      };
      textroom.data({
        text: JSON.stringify(register),
        success: function (resp) {
          registerUsernameChat();
        }
      });
    }

    function registerUsernameChat() {
      var transaction = randomString(12);
      var register = {
        textroom: "join",
        transaction: transaction,
        room: classidcp,
        username: "userId" + idusercp,
        display: displaynamecp
      };

      transactions[transaction] = function (response) {
        if (response["textroom"] === "error") {
          // Something went wrong
          addNewLog(response["error"], "ERROR");
          return;
        }
        // Any participants already in?
        if (response.participants && response.participants.length > 0) {
          for (var i in response.participants) {
            var p = response.participants[i];
            var username = p.display ? p.display : p.username;
            chatParticipants[p.username] = username;
            appendMessage(p.username, new Date(), false, "JOIN", "joined");
            // $('#chatroom').get(0).scrollTop = $('#chatroom').get(0).scrollHeight;
          }
        }
      };
      textroom.data({
        text: JSON.stringify(register),
        error: function (reason) {
          addNewLog(reason, "ERROR");
        },
        success: function () { chatReady = true; }
      });
    }

    function sendDataChat() {
      var inputText = $("#chatInput").val();
      if (inputText) {
        var message = {
          textroom: "message",
          transaction: randomString(12),
          room: classid,
          text: inputText
        };
        textroom.data({
          text: JSON.stringify(message),
          error: function (reason) { addNewLog(reason, "ERROR"); },
          success: function () { $("#chatInput").val(null); }
        });
      }
    }

    // Helper to format times
    function getDate(jsonDate) {
      var when = new Date();
      if (jsonDate) {
        when = new Date(Date.parse(jsonDate));
      }
      return when;
    }

    function handleMessage(data) {
      var json = JSON.parse(data);
      var transaction = json["transaction"];
      if (transactions[transaction]) {
        // Someone was waiting for this
        transactions[transaction](json);
        delete transactions[transaction];
        return;
      }
      var what = json["textroom"];

      // ================ Incoming message: public or private? ============
      if (what === "message") {
        var msg = json["text"];
        msg = msg.replace(new RegExp('<', 'g'), '&lt');
        msg = msg.replace(new RegExp('>', 'g'), '&gt');
        appendMessage(json["from"], json["date"], json["whisper"], "CHAT", msg);
      } // ====================== Somebody joined =========================
      else if (what === "join") {
        var username = json["display"] ? json["display"] : json["username"];
        chatParticipants[json["username"]] = username;
        appendMessage(json["username"], new Date(), false, "JOIN", "joined");
      } // ====================== Somebody left ===========================
      else if (what === "leave") {
        var username = json["username"];
        appendMessage(username, new Date(), false, "LEAVE", "left");
      } // ================= Room was destroyed, goodbye! =================
      else if (what === "destroyed") {
        addNewLog("The room has been destroyed!", "ERROR");
      }

      // $('#chatroom').get(0).scrollTop = $('#chatroom').get(0).scrollHeight;
    }

    function appendMessage(participant, dateTime, isPrivate, type, msg) {
      
        var newMessage = {};
        newMessage.from = participant;
        newMessage.datetime = dateTime;
        newMessage.isPrivate = isPrivate;
        newMessage.type = type;
        newMessage.message = msg;
        chatData.splice(0, 0, newMessage);

        var newArchive = {};
        newArchive.sender_name = participant;
        newArchive.text = msg;
        newArchive.datetime = dateTime;
        // archiveChat.push(newArchive);

        // if (pageType == "video") {
        //   var divChat = document.getElementById('chatBoxScroll');
        //   divChat.scrollTop = divChat.scrollHeight;
        // }
    }

    // Just an helper to generate random string
    function randomString(len, charSet) {
      charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var randomString = '';
      for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz, randomPoz + 1);
      }
      return randomString;
    }

    function addNewLog(text, type){
      console.log(text+" : "+type)
    }

    function checkEnter(field, event) {
      var theCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
      if(theCode == 13) {
        if(field.id == 'datasend')
          sendDataChat();
        return false;
      } else {
        return true;
      }
    }
