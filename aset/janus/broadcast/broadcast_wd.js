// var server = "wss://c2.classmiles.com:8989";
var server = null;
var janus = null;
var sendrequest = null;
var sendrequestListener = null;
var started = null;
var opaqueId = "videoroomtest-"+Janus.randomString(12);
var mypvtid = null;
var listUserActive = null;
var listDevices = [];
var raisehandlist = {};

var idtutor = null;
var iduser  = null;
var displayname = null;
var tutorname = null;
var firstname = null;
var token = null;
var classid = null;
var usertype = null;
var classname = null;
var class_description = null;
var currentDeviceId = null;
var tutorin = null;
var st_tutor = 0;
var startTime = null;
var endTime = null;
var endTimeclass = null;
var cektangan = 0;
var vartutor = 0;
var privateid = null;

//KHUSUS CHAT
var chatData = [];
var transactions = {};
var textroom = null;
var chatParticipants = {};
var chatReady = false;
var classidcp = "";
var idusercp = "";
var displaynamecp = "";
var archiveChats = [];

var d = new Date();
var month = d.getMonth()+1;
var day = d.getDate();

var feeds = [];
var bitrateTimer = [];
var iceServer = [];
var listVideo = null;
var iceServers = [];
var asdf = "Awal";
var checkdate = ((''+day).length<2 ? '0' : '')+ day + '/' + ((''+month<2 ? '0' : '') +  month + '/'+ d.getFullYear());

var link = "https://classmiles.com/";
var timer = 0;
var aaa = 0;
var parameter = true;

var temp_break_state = -1;
var temp_play_pos = -1;
var break_state = 0; 
var play_pos = 0;
var page_Type = null; 
var wktu = 0;
var tempatsimpancek = null;
var st_break = 0;
var geo_location = [];
var session = null;
var lebarlayar = null;
var janusscreen = null;
var rhands = false;
var checkchat = 0;

$(document).ready(function(){

  function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
  }

  $.get("https://ipinfo.io/json", function (response) {
    geo_location = {city:response.city,region:response.region,country:response.country};       
  }, "jsonp");

  lebarlayar = $(window).width(); 
  session = window.localStorage.getItem('session'); 
      
  if (session == null) {
      session = getURLParameter("access_session");  
      console.warn('Test Token Null = ' + session);
  }

  if (lebarlayar < 900 ) 
  {
      location.href='https://id.classmiles.com/classroom/broadcast_wd?access_session='+session;
      page_Type = "android";    
      $("#loader").css('display', 'none');
      var text = 'Classmiles ...';
      var customElement   = $("<div>", {
          id      : "countdown",
          css     : {
              "font-size"   : "20px",
              "margin-top"  : "130px",
              "color"       : "#ffffff"
          },
          text    : text
      });
      $.LoadingOverlay("show", {
          color   : "#008080",
          fade    : true,
          size    : "50%",
          zIndex  : 9999,
          image   : link+"aset/img/baru/Loadingcmm.gif",
          custom  : customElement
      });  
  } 
  else 
  {
      page_Type = "web";
  }  
  
  setTimeout(function(){
      $("#loader").css('display', 'none');
      getpollingdata(parameter);
  },3000);

  setInterval(function(){
      $("#loader").css('display', 'none');
      $.ajax({
          url: 'https://classmiles.com/Rest/list_video',
          type: 'GET',
          data: {
            session: session
          },
          success: function(data)
          { 
              if (data['list'].length < 0) 
              {
                  listVideo = data['list'][play_pos]['video_source'];            
              }
              else
              {
                listVideo = "../aset/video/classmiles.mp4";
              }     
          }
      });         
      getpollingdata(parameter);
  },10000);

  setInterval(function(){
      if (play_pos == 3) 
      {
          st_tutor = 3;
          $("#tempatwaktu").css('display','block');
          wktu = 1;
          clearInterval(getpollingdata);        
          generate();
      }
      if (cektangan == 1) {
          $("#tanganbiasa").css('display','none');
          $("#tanganmerah").css('display','block');
      }   
      else
      {
          $("#tanganmerah").css('display','none');
          $("#tanganbiasa").css('display','block');
      }
  },1000);

  $("#pindah2").css('display','block');
  $("#pindah3").css('display','block');
  $("#pindah2").click(function(){   
    if ($("#vdio").height() != 100 && $("#whboard").height() != 100) {      
      $("#pindah2").css('display','none');
      $("#pindah1").css('display','block');
      $("#pindah3").css('display','block');
    }
  });
  $("#myvideostudent").click(function(){    
    if ($("#vdio").height() != 100 && $("#whboard").height() != 100) {      
      $("#pindah2").css('display','none');
      $("#pindah1").css('display','block');
      $("#pindah3").css('display','block');
    }
  });
  $("#pindah3").click(function(){   
    if ($("#sshare").height() != 100 && $("#whboard").height() != 100) {      
      $("#pindah3").css('display','none');
      $("#pindah2").css('display','block');
      $("#pindah1").css('display','block');
    }
  });
  $("#pindah1").click(function(){   
    if ($("#vdio").height() != 100 && $("#whboard").height() != 100) {      
      $("#pindah1").css('display','none');
      $("#pindah2").css('display','block');
      $("#pindah3").css('display','block');
    }
  });

  setInterval(function(){     
      getraisehand();        
  },1000);

  function getraisehand()
  {   
      $.ajax({
      url: 'https://classmiles.com/Rest/get_token',
      type: 'POST',
      data: {
      session: session
      },
      success: function(data)
      {                     
        if (data['error']['code'] == '200' && data['response']['token'] != null) {
          // ===== SET RAISE HAND DOME ======//
          raisehandlist = [];
          if (data['student_rhand']) {
            // console.warn("INI DATA RAISE HAND "+data['student_rhand']);
            raisehandlist = data['student_rhand'];
            // console.warn(raisehandlist);
          }
          if (raisehandlist.length > 0) {
            $("#totalraisehand").css("display",'block');
            $("#totalraisehand").text(raisehandlist.length);
          }
          else
          {
            $("#totalraisehand").text("");
            $("#totalraisehand").css("display",'none');
          }
          // console.warn("aaaa "+raisehandlist);
        }
      }
      });  
  }

  // ============= TUTOR LOGIN ==============================
  function getpollingdata(firsttime)
  {
    var user_utc = new Date().getTimezoneOffset();
    user_utc = -1 * user_utc;
    // alert("E");
    $.ajax({
      url: 'https://classmiles.com/Rest/get_token',
      type: 'POST',
      data: {
        session: session,
        user_utc: user_utc
      },
      success: function(data)
      {                     
          if (data['error']['code'] == '200' && data['response']['token'] != null) {
              if (server == null) 
              {
                  server = "wss://"+data['response']['janus']+":8989";
              }
              try
              {            
                  window.localStorage=LocalStorage;    
              }
              catch(e){                  
              }

              //then use your localStorage as usual
              localStorage.setItem('janus',data['response']['janus']);
              janusscreen = data['response']['janus'];
              console.warn(janusscreen + " Local Android "+ localStorage.getItem('janus'));

              usertype = data['response']['usertype'];
              $("#usertypelogin").val(usertype);          
              displayname = data['response']['first_name'];
              classname = data['response']['class_name'];
              class_description = data['response']['class_description'];
              firstname = data['response']['first_name'];
              tutorname = data['response']['nama_tutor'];

              startTime = data['response']['start_time'];
              endTime = data['response']['finish_time'];
              endTimeclass = data['response']['end_time'];

                  // if (displayname != null) {
              $("#classname").text(classname);
              $("#subclass").text(class_description);
              $("#datenow").text("Class will end at "+endTimeclass);
              $("#nametutor").text(tutorname);

              $("#classnamee").text(classname);
              $("#subclasss").text(class_description);
              $("#datenoww").text("Class will end at "+endTimeclass);
              $("#nametutorr").text(tutorname);
                  // }                                    
              GogoLayar();
              var heightchat = $("#chat").height()-270;
              $("#chatBoxScroll").height(heightchat);

              // ===== !SET DOM COMPONENT ======//
              if (archiveChats.length > 0 && data['response']['usertype'] == "tutor") {
                  archiveChat(archiveChats);              
              }              
              if (data['room_state']['break_state'] == "" && data['room_state']['play_pos'] == "") 
              {
                  console.warn("Tidak ada Break state");
              }

              break_state = data['room_state']['break_state'];
              // break_state = 0;
              // console.warn(break_state);         
              play_pos = data['room_state']['play_pos'];
              // console.warn(play_pos);  
              // $("#myvideo").attr('muted','true');
              token = data['response']['token'];
              privateid = 1;

              classid = parseInt(data['response']['class_id']);                 
              idtutor = parseInt(data['response']['id_tutor']);
              iduser = parseInt(data['response']['id_user']); 

              if (data['response']['raise_hand'] == 2 && sendrequest != null && !rhands) {
              	 console.log('Do Pbulish');
      			     publishOwnFeed(currentDeviceId);
      			     rhands = true;
                  $("#tempatraise").css('display','none');
                  $("#tempatraisee").css('display','none');
                  $("#tempathangup").css('display','block');            
                  $("#tempathangupp").css('display','block');                
              }
              
              if (rhands == false) {
                  $("#tempathangup").css('display','none');            
                  $("#tempathangupp").css('display','none');
                  $("#tempatraise").css('display','block');
                  $("#tempatraisee").css('display','block');
              }
              // alert(vartutor);
              console.warn("SAMPE");
              console.warn(break_state);
              tempatsimpancek = $("#myvideostudent").attr('src');
              if (break_state == 1) {
                  $.LoadingOverlay("hide");
                  $("#myvideo").attr('muted','false');
                  // console.warn(temp_break_state);
                  // console.warn("IKLAN CUY");

                  if (temp_break_state != break_state) {
                      temp_break_state = break_state;

                    $.ajax({
                        url: 'https://classmiles.com/Rest/list_video',
                        type: 'GET',
                        data: {
                          session: session
                        },
                        success: function(data)
                        {                         
                            if (data['list'].length < 0) 
                            {                   
                                if (vartutor == 0) 
                                {
                                    listVideo = data['list'][play_pos]['video_source'];                   
                                    // console.warn("AAAAAAAAAAAAAAAAAAAA");
                                    if (usertype == "student") {
                                        if (page_Type == "web") 
                                        {                               
                                          $("#myvideostudent").attr('src',listVideo);
                                          $("#myvideostudent").attr('loop','true');
                                        }
                                        else
                                        {
                                          $("#playerse").attr('src',listVideo);
                                          $("#playerse").attr('loop','true');
                                        }
                                    }
                                    if (usertype == "tutor") {                                
                                        $("#myvideo").attr('src',listVideo);
                                        $("#myvideo").attr('loop','true');
                                    }
                                }
                            }
                            else
                            {
                                // console.warn("TIDAK ADA VIDEO SOURCE");
                                if (vartutor == 0) 
                                {
                                  var listVideoo = "../aset/video/classmiles.mp4";
                                  if (usertype == "student") {
                                      if (page_Type == "web") 
                                      {                                               
                                          $("#myvideostudent").attr('src',listVideoo);
                                          $("#myvideostudent").attr('loop','true');
                                      }
                                      else
                                      {
                                          $("#playerse").attr('src',listVideoo);
                                          $("#playerse").attr('loop','true');
                                      }
                                  }
                                  else if(usertype == "tutor"){
                                      $("#myvideo").attr('src',listVideoo);
                                      $("#myvideo").attr('loop','true');
                                  }
                                }
                            }                      
                        }
                    });
                  }
                  return null;
                  // parameter = true;
                  console.warn("GK MASUK WOY");
              }
              else
              {     
                  console.warn("WOY");
                  console.warn('ini temp'+temp_break_state);
                  console.warn(break_state);
                  if (temp_break_state != break_state) {
                      console.warn(break_state);
                      console.warn("BBBBBBBBBBBBBBBB");
                      temp_break_state = break_state;
                      // console.warn("UDAH MASUK LAGI");
                      if (janus == null) {                                        
                          tunner(parameter);                                            
                          $("#screensharestudent").attr("src", "https://classmiles.com/screen_share/#access_session="+session+"&janus="+janusscreen+"&page="+page_Type);
                          $("#screensharestudenttt").attr("src", "https://classmiles.com/screen_share/#access_session="+session+"&janus="+janusscreen+"&page="+page_Type);
                          $("#wbcard").attr("src", "https://"+data['response']['janus']+"/wb/"+classid+"/session/"+session);
                          $("#wbcardd").attr("src", "https://"+data['response']['janus']+"/wb/"+classid+"/session/"+session);
                          $("#wbcarddd").attr("src", "https://"+data['response']['janus']+"/wb/"+classid+"/session/"+session);

                          $("#screensharestudent").css("width", "100%");
                          $("#screensharestudent").css("height", "100%");
                          $("#screensharestudenttt").css("width", "100%");
                          $("#screensharestudenttt").css("height", "100%");                   
                          parameter = false;                        
                      }                        
                  }                                                      
              }
          } 
          else if (data['error']['code'] == '504') {
              // console.warn("MASUK ELSE IF 504");
              if (page_Type == "android") {
                // Android("404 not found");
                // Android("504");
              } 
              else 
              {
                location.href='https://classmiles.com';
              }
          }
          else if (data['error']['code'] == '404') {
              if (page_Type == "android") {
                  Android("404");
              } 
              else {
                  location.href='https://classmiles.com';
              }
          }   
          else
          {
              if (page_Type == "android") {
                  Android();
              } 
              else 
              {
                  // console.warn("SAATNYA DESTROY TOKEN");
                  destroyToken();
                  // clearInterval(timer);
              }
          }        
      }
    });
} 

function Android(isi)
{
  if (isi) {
      AndroidFunction.ErrorMessage(isi);
  } else {
      AndroidFunction.FinishClass();
  }
}

function GogoLayar() {
    // alert(lebarlayar +"|"+usertype);
    if (lebarlayar < 900 ) {
            
        // page_Type = "android";
        console.warn(session);

        if (usertype == "student") 
        {               
            console.warn("INI STUDENT LOH");
            $(".tampilanweb").css('display','none');
            $("#header").css('display','none');     
            $("#chat").css('display','none');     
            $(".tampilanandrotutor").css('display','none');
            $(".tampilanandro").css('display','block');            
        } 
        else
        {
            console.warn("INI TUTOR LOH");
            $(".tampilanweb").css('display','none');
            $("#header").css('display','none');     
            $("#chat").css('display','none');
            $(".tampilanandro").css('display','none');
            $(".tampilanandrotutor").css('display','block');
            var a = $(window).height();
            var b = $(window).width(); //UNTUK HEIGHT
            // var b = $(window).height() / 0.65;
            var c = b * 0.5625;
            var d = a - c;
            var e = a / 0.5625; //UNTUK WIDTH LANDSCAPE             
            if (a > b * 0.5625) {
              console.warn("INIMASUK KE RATIO");
              $("#playerse_tutor").css('height', c);
              $("#playerse_tutor").css('width', b);
            }
            else
            {               
              console.warn("ENGGA MASUK BRO");
              $("#playerse_tutor").css('width', e)
              $("#playerse_tutor").css('height', a);
              console.warn("WIDTH_a"+e);
              console.warn("HEIGHT_a"+a);
            }               
        }
  }
  else
  {
    // page_Type = "web";
    console.warn("INI WEB LOH");
    if (usertype == "tutor") {
      console.warn("TUTOR");
      $("#tempatstudent").css('display', 'none');
      $("#usertypestudent").css('display', 'none');                   
      $("#tempattutor").css('display', 'block');
      $("#usertypetutor").css('display', 'block');

      // session = window.localStorage.getItem('session');
      // var lebarnih = $(".asd").height() /100*75;
      // var tingginih = $(".asd").height() /100*75;             
      // var a = $(".asd").height() /100*75;
      // var b = $(".asd").width() /100*75; //UNTUK HEIGHT
      // // var b = $(window).height() / 0.65;
      // var c = b * 0.5625;
      // var d = a - c;
      // var e = a / 0.5625; //UNTUK WIDTH LANDSCAPE
      // if (a > b * 0.5625) {                       
      //   $("#myvideo").css('height', c);
      //   $("#myvideo").css('width', b);
      // }
      // else
      // {               
      //   $("#myvideo").css('width', e);
      //   $("#myvideo").css('height', a);
      // }  
    }       
    else if(usertype == "student")
    {
      console.warn("STUDENT");
      $("#tempattutor").css('display', 'none');
      $("#usertypetutor").css('display', 'none');
      $("#tempatstudent").css('display', 'block');
      $("#usertypestudent").css('display', 'block'); 

      // var lebarnih = $(window).height() /100*75;
      // var tingginih = $(window).height() /100*75;              
      // var a = $(window).height();
      // var b = $(window).width(); //UNTUK HEIGHT
      // // var b = $(window).height() / 0.65;
      // var c = b * 0.5625;
      // var d = a - c;
      // var e = a / 0.5625; //UNTUK WIDTH LANDSCAPE
      // if (a > b * 0.5625) {                       
      //  $("#myvideostudent").css('height', c);
      //  $("#myvideostudent").css('width', b);
      // }
      // else
      // {               
      //  $("#myvideostudent").css('width', e)
      //  $("#myvideostudent").css('height', a);
      // }                     
    }
    
    $(".tampilanandro").css('display','none');
    $(".tampilanweb").css('display','block');
    $("#chat").css('display','block');
    $("#header").css('display','block');   
   }
  }
  

  $("#clearinterval").click(function(){
      clearInterval(getpollingdata);
      $("#tempatwaktu").css('display','block');
        // console.warn("SUKSES CLEAR");
        generate();
      });

      var seconds = 60; // Berapa detik waktu mundurnya
      function generate() { // Nama fungsi countdownnya   
        if(getpollingdata) {
          clearInterval(getpollingdata);
        }
        // console.warn("GENERATE");  

        var id;
        id = setInterval(function () {
          if (seconds < 1) {
            clearInterval(id);
            wktu = 0;
            // Perintah yang akan dijalankan
            // apabila waktu sudah habis
            $("#tempatwaktu").css('display','none');
            destroyToken();

          } else {      
            document.getElementById('countdown').innerHTML = --seconds;
          }
        }, 1000);       
  }


  function tunner()
  {
    $.ajax({
      url: 'https://classmiles.com/Rest/turner_stuner',
      type: 'GET',
      data: {
        session: session
      },
      success: function(data)
      {              
        var dataServer = data.response;
        var responseIceServer = dataServer.list_stun.concat(dataServer.list_turn);
        iceServers = [];

        var config = {};
        config.audioCodec = dataServer.audiocodec;
        config.videoCodec = dataServer.videocodec;
        config.maxBitRate = dataServer.maxbitrate;
        config.resolution = dataServer.resolution;

        window.localStorage.setItem("videoConfig", config);

        if (config.videoCodec === 'lowres') {
              // Small resolution, 4:3
              config.height = 240;
              config.width = 320;
            } else if (config.videoCodec === 'lowres-16:9') {
              // Small resolution, 16:9
              config.height = 180;
              config.width = 320;
            } else if (config.videoCodec === 'hires' || config.videoCodec === 'hires-16:9') {
              // High resolution is only 16:9
              config.height = 720;
              config.width = 1280;
              if (navigator.mozGetUserMedia) {
                var firefoxVer = parseInt(window.navigator.userAgent.match(/Firefox\/(.*)/)[1], 10);
                if (firefoxVer < 38) {
                  config.height = 480;
                  config.width = 640;
                }
              }
            } else if (config.videoCodec === 'stdres') {
              // Normal resolution, 4:3
              config.height = 480;
              config.width = 640;
            } else if (config.videoCodec === 'stdres-16:9') {
              // Normal resolution, 16:9
              config.height = 360;
              config.width = 640;
            } else {
              config.height = 480;
              config.width = 640;
            }

            window.localStorage.setItem('videoConfig', JSON.stringify(config));

            for (var i = 0; i < responseIceServer.length; i++) {
              var server = {};
              server.urls = responseIceServer[i].url;
              server.credential = responseIceServer[i].credential;
              server.username = responseIceServer[i].username;
              iceServers.push(server);

            }
            // console.warn(iceServers);
            start(iceServers);
          }
        });
  }


// ============= JANUS START ==============================
function start(inidingin){
  Janus.init({debug: "all", callback: function(){
      // $("#start").click(function(){

        if(started)
          return;
        started = true;

        // Make sure the browser supports WebRTC
        if(!Janus.isWebrtcSupported()) {
          bootbox.alert("No WebRTC support... ");
          return;
        }

        janus = new Janus(
        {
            server: server,
            iceServers: inidingin,
            token: token,
            success: function()
            {
                attachPublisher();
                attachChatPlugin(classid, iduser, displayname);
              // attachVideoCall();
              
            },
            error: function(error) {
              Janus.error(error);
              if (page_Type == "android") {
                  if (error.includes("missing secret/token")) {
                      $.ajax({
                          url: link+'Rest/recreate_token',
                          type: 'POST',
                          data: {
                              token: token,
                              janus: window.localStorage.getItem('janus')
                          },
                          success: function(data)
                          {
                              console.warn("BERHASIL BRO");
                              window.location.reload();
                          }
                      });
                  }
                  else
                  {
                      Android();
                  }                
              } else {
                  if (error.includes("missing secret/token")) {
                      $.ajax({
                          url: link+'Rest/recreate_token',
                          type: 'POST',
                          data: {
                              token: token,
                              janus: window.localStorage.getItem('janus')
                          },
                          success: function(data)
                          {
                              console.warn("BERHASIL BRO");
                              window.location.reload();
                          }
                      });
                  }
                  else
                  {
                      bootbox.alert(error, function() {
                        if (page_Type == "android") {
                          // Android();
                        }else{
                          window.location.reload();
                        }
                      });
                  }
              }
            }
        }
        )
      }});
} 

// ____________________________ JANUS ____________________________________

function getListParticipants()
{
  var display="";
  var jsonRequest = {
    "request": "listparticipants",
    "room": classid,
    "description": "",
    "is_private": true
  };
  sendrequest.send({
    "message": jsonRequest,
    success: function (resp) {
      listUserActive = resp.participants;
      var a = JSON.stringify(listUserActive);
          // console.warn("INI LIST USERACTIOVE"+a);
          $("#totalstudent").text(listUserActive.length+" people attending now");
          $("#totalstudentt").text(listUserActive.length+" people attending now");

          var as=$(listUserActive).filter(function (i,n){return n.publisher==='true'});
        // console.warn("AS = " + as);
        if (as.length > 0) {
          // console.warn("Ada Tutor Mang");
          vartutor = 1;
          st_tutor = 1;
          st_break = 1;
          // $("#myvideostudent").attr("src",'');
        } else {
          $.LoadingOverlay("hide");
          // console.warn("Tidak ada tutor Mang");
          vartutor = 0;
          st_break = 0;         
          if (st_tutor == 0) 
          {           
            if (listVideo == null) {
              if (page_Type == "web") {
                if (usertype == "student") 
                {
                  // console.warn("MASUK WEB VIDEO IKLAN TIDAK KOSONG");
                  $("#myvideostudent").attr("src",'../aset/video/classmiles.mp4');                 
                  $("#myvideostudent").attr('loop','true');
                  st_tutor = 1;
                }
              } else {
                // console.warn("MASUK ANDROID VIDEO IKLAN TIDAK KOSONG");
                $("#playerse").attr('src','../aset/video/classmiles.mp4');
                // $("#playerse").attr('muted','true');
                $("#playerse").attr('loop','true');
                st_tutor = 1;
              }
            }
            else
            {
              // console.warn("MASUK LIST VIDEO IKLAN TIDAK KOSONG");
              if (page_Type == "web") {
                if (usertype == "student") 
                {
                  // console.warn("MASUK WEB VIDEO IKLAN TIDAK KOSONG");
                  $("#myvideostudent").attr("src",listVideo);                 
                  $("#myvideostudent").attr('loop','true');
                  st_tutor = 1;
                }
              } else {
                // console.warn("MASUK ANDROID VIDEO IKLAN TIDAK KOSONG");
                $("#playerse").attr('src',listVideo);
                // $("#playerse").attr('muted','true');
                $("#playerse").attr('loop','true');
                st_tutor = 1;
              }
            }
          }
          return null;

        }
        return null;

        $("#listuser").val(display);          
      }
    });
}

function getDevice() {
    // console.warn("get Devices");
    Janus.listDevices(function (devices) {
      var deviceVideo = [];
      var deviceAudio = [];

      for (var i = 0; i < devices.length; i++) {
        if (devices[i].kind == "videoinput") {
          deviceVideo.push(devices[i]);
        } else if (devices[i].kind == "audioinput") {
          deviceAudio.push(devices[i]);
        }
      }
      $("#buttonlist").empty();
      listDevices.video = deviceVideo;
      listDevices.audio = deviceAudio;

    });
  }

function attachPublisher()
{
	janus.attach(
	{
		plugin: "janus.plugin.videoroom",
		opaqueId: opaqueId,
		success: function(pluginHandle) {
			$('#details').remove();
			sendrequest = pluginHandle;
      $("#classname").text(classname);
      $("#subclass").text(class_description);
      $("#datenow").text("Class will end at "+endTimeclass);
      $("#nametutor").text(tutorname);

      $("#classnamee").text(classname);
      $("#subclasss").text(class_description);
      $("#datenoww").text("Class will end at "+endTimeclass);
      $("#nametutorr").text(tutorname);
			// Janus.log("Plugin attached! (" + sendrequest.getPlugin() + ", id=" + sendrequest.getId() + ")");
			// Janus.log("  -- This is a publisher/manager");
			// Prepare the username registration
			createroom();   
			getListParticipants();            
			getDevice();     
			var timer2 = setInterval(function() {
		        getListParticipants();
		    }, 1000);									
			// var register = { "request": "join", "room": classid, "ptype": "publisher", "display": displayname, "id":iduser };
			// sendrequest.send({"message": register});							
		},
		error: function(error) {
			Janus.error("  -- Error attaching plugin...", error);
			bootbox.alert("Error attaching plugin... " + error);
		},
		consentDialog: function(on) {
			Janus.debug("Consent dialog should be " + (on ? "on" : "off") + " now");			
		},
		mediaState: function(medium, on) {
			// Janus.log("Janus " + (on ? "started" : "stopped") + " receiving our " + medium);
		},
		webrtcState: function(on) {
			// Janus.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
			$("#videolocal").parent().parent().unblock();
			$("#videoremote1").parent().parent().unblock();
		},
		ondataopen: function(data) {
			Janus.log("The DataChannel is available! lis");
		},
		ondata: function(data) {
			Janus.debug("We got data from the DataChannel! lis" + data);
			if (usertype == "student") {
				doHangup();
			}
		},
		onmessage: function(msg, jsep) {
  			Janus.debug(" ::: Got a message (publisher) :::");
  			Janus.debug(JSON.stringify(msg));
  			var event = msg["videoroom"];
  			Janus.debug("Event: " + event);
  			if(event != undefined && event != null) {
    				if(event === "joined") {
    					// Publisher/manager created, negotiate WebRTC and attach to existing feeds, if any
    					myid = msg["id"];
    					mypvtid = msg["private_id"];
    					Janus.log("Successfully joined room " + msg["room"] + " with ID " + myid);
    					if (idtutor == myid) 
    				    {
    				    	console.warn("Tutor publish");
    				    	publishOwnFeed(currentDeviceId);
    				    }
    					// Any new feed to attach to?
    					if(msg["publishers"] !== undefined && msg["publishers"] !== null) {
    						var list = msg["publishers"];
    						Janus.debug("Got a list of available publishers/feeds:");
    						Janus.debug(list);
    						for(var f in list) {
    							var id = list[f]["id"];
    							var display = list[f]["display"];
    							Janus.debug("  >> [" + id + "] " + display);
    							attachListener(id, display);
    						}
    					}
    				} else if(event === "destroyed") {
    					// The room has been destroyed
    					Janus.warn("The room has been destroyed!");
    					bootbox.alert("The room has been destroyed", function() {
    						window.location.reload();
    					});
    				} else if(event === "event") {
    					// Any new feed to attach to?
    					if(msg["publishers"] !== undefined && msg["publishers"] !== null) {
    						var list = msg["publishers"];
    						Janus.debug("Got a list of available publishers/feeds:");
    						Janus.debug(list);
    						for(var f in list) {
    							var id = list[f]["id"];
    							var display = list[f]["display"];
    							Janus.debug("  >> [" + id + "] " + display);
    							attachListener(id, display);
    						}
    					} else if(msg["leaving"] !== undefined && msg["leaving"] !== null) {
    						// One of the publishers has gone away?
    						var leaving = msg["leaving"];
    						// Janus.log("Publisher left: " + leaving);
    						var remoteFeed = null;
    						for(var i=1; i<6; i++) {
    							if(feeds[i] != null && feeds[i] != undefined && feeds[i].rfid == leaving) {
    								remoteFeed = feeds[i];
    								break;
    							}
    						}
    						if (leaving == idtutor) {
    							if(sendrequest != null) {
    								// console.warn("HILANG");
    								Janus.debug("Feed  (" + sendrequest.rfdisplay + ") has left the room, detaching");
    								$('#remote').empty().hide();
    								$('#videoremote').empty();
    								feeds[sendrequest.rfindex] = null;
    								// alert(usertype);
    								// alert(usertype);			
    								$.ajax({
    									url: 'https://classmiles.com/Rest/unbreak',
    									type: 'GET',
    									data: {
    										session: session
    									},
    									success: function(data)
    									{							
    										// console.warn("BERHASIL UNBREAK");

    										// var aa = $("#tempatx").attr("title").remove();
    										// a.text("Play");
    										st_tutor = 1;
    										publishOwnFeed(true);

    									}
    								});					
    								if (usertype == "student") {
    									if (listVideo != null) 
    									{
    										// videonih = data['list'][play_pos]['video_source'];
    										// alert(dataa['list']['video_source']);
    										// alert(page_Type + " publish leave");
    										if (page_Type == "android") {                      
		                      	$("#playerse").attr('src',listVideo);
		                        // $("#playerse").attr('muted','true');
		                        $("#playerse").attr('loop','true');
                            alert('a');
		                    } else {
		                        $("#myvideostudent").attr('src',listVideo);
		                        // $("#myvideostudent").attr('muted','true');
		                        $("#myvideostudent").attr('loop','true');
		                    }     								
    									}
    									else
    									{
    										// console.warn("TUTOR LEAVING");													
    										if (page_Type == "web") {                       
    					                        $("#playerse").attr('src','../aset/video/classmiles.mp4');
    					                        $("#playerse").attr('loop','true');
    					                    }
    					                    else
    					                    {                        
    					                        $("#myvideostudent").attr('src','../aset/video/classmiles.mp4');
    					                        $("#myvideostudent").attr('loop','true');
    					                    }
    									}
    								}
    								
    									return null;
    								// $("#myvideo").css('width','90%');
    							}
    						}
    						if (aaa == 1) {
    							if(remoteFeed != null) {
    								Janus.debug("Feed " + remoteFeed.rfid + " (" + remoteFeed.rfdisplay + ") has left the room, detaching");
    								$("#videoBoxLocal").empty();
    								$("#videoBoxRemote").empty();
    								$(".tutuprhh").css('display','none');        
    								$("#raisehandclick").css('display','block');
    								$("#videoBoxRemote").css('display','none');
    								$(".tutuprhh").css('display','none');        
    								$("#raisehandclick").css('display','block');
    								$("#tanganmerah").css('display','none');
    								$("#tanganbiasa").css('display','block');
    								cektangan = 0;
    								aaa = 0;
    								// feeds[remoteFeed.rfindex] = null;
    								// remoteFeed.detach();
    							}
    						}
    					} else if(msg["unpublished"] !== undefined && msg["unpublished"] !== null) {
    						// One of the publishers has unpublished?
    						var unpublished = msg["unpublished"];
    						// Janus.log("Publisher left: " + unpublished);
    						if(unpublished === 'ok') {
    							// That's us
    							sendrequest.hangup();
    							return;
    						}
    						var remoteFeed = null;
    						for(var i=1; i<6; i++) {
    							if(feeds[i] != null && feeds[i] != undefined && feeds[i].rfid == unpublished) {
    								remoteFeed = feeds[i];
    								break;
    							}
    						}
    						if(remoteFeed != null) {
    							Janus.debug("Feed " + remoteFeed.rfid + " (" + remoteFeed.rfdisplay + ") has left the room, detaching");
    							$('#remote'+remoteFeed.rfindex).empty().hide();
    							$('#videoremote'+remoteFeed.rfindex).empty();
    							console.warn("SISWA CLEAR");
    			        $("#videoBoxLocal").empty();
    							$("#videoBoxRemote").empty();
    							$(".tutuprh").css('display','none');        
    							$("#bukakotakraise").css('display','block');
    							$("#videoBoxRemote").css('display','none');
    							$(".tutuprhh").css('display','none');        
    							$("#raisehandclick").css('display','block');
    							$("#tanganmerah").css('display','none');
    							$("#tanganbiasa").css('display','block');
    							cektangan = 0;			                
    							feeds[remoteFeed.rfindex] = null;
    							// remoteFeed.detach();
    						} 
    					} else if(msg["error"] !== undefined && msg["error"] !== null) {
    						bootbox.alert(msg["error"]);
    					}
    				}
  			}
  			if(jsep !== undefined && jsep !== null) {
    				Janus.debug("Handling SDP as well...");
    				Janus.debug(jsep);
    				sendrequest.handleRemoteJsep({jsep: jsep});
  			}
		},
		onlocalstream: function(stream) {
			Janus.debug(" ::: Got a local stream :::");
			mystream = stream;
			Janus.debug(JSON.stringify(stream));
			$('#publisher').removeClass('hide').html(displayname).show();
	        
	        console.warn('aku masuk nih mas bro');
	        // $('#myvideo2').attr('src', URL.createObjectURL(stream));
	        console.warn(URL.createObjectURL(stream));
	        if (page_Type=="android") 
	        {
	        	  var orientation = $("#orientation").val();
              if (orientation == "potrait") {                      
                  $('#videoBoxLocal').empty();
                  $('#videoBoxLocal').append('<video id="videoCallLocal" style="z-index: 1; position: absolute; margin-bottom: 2%; margin-right:-4%; height: 120px; width: 150px; bottom: 0; right: 0;" autoplay data-video-aspect-ratio="16:9"></video>');
                  $("#tempatraisehand").css('display','block');
                  $("#tempatraise").css('display','none');
                  $("#tempatraisee").css('display','none');
                  $("#tempathangup").css('display','block');
                  $("#tempathangupp").css('display','block');
              }   
              else
              {                                       
                  $('#videoBoxLocal').empty();     
                  $('#videoBoxLocal').append('<video id="videoCallLocal" style="width:150px; height:120px;" autoplay data-video-aspect-ratio="16:9"></video>');
                  $("#tempatraise").css('display','none');
                  $("#tempatraisee").css('display','none');
                  $("#tempathangup").css('display','block');
                  $("#tempathangupp").css('display','block');
              }                                                                 
              if (usertype=="tutor") {                                                                     
                $('#playerse_tutor').attr('src', URL.createObjectURL(stream));
              }
              $('#videoCallLocal').attr('src', URL.createObjectURL(stream));
	        }
	        else
	        {
	        	if (usertype=="student") {
		          	// $('#playerse').attr('src', URL.createObjectURL(stream));
		          	console.log("Masuk student");
                $("#videoBoxLocal").empty();
		            $('#videoBoxLocal').append('<video id="videoCallLocal" style="z-index: 1; position: absolute; margin-bottom: 7%; margin-right:7%; height:250px; width: 250px; bottom: 0; right: 0;" autoplay></video>');
		            $("#raisehandclick").css('display','none');
		          	$('#videoCallLocal').attr('src', URL.createObjectURL(stream));
		     		    $(".tutuprhh").css('display','block');       
		        }
		        else
		        {
		        	  $('#myvideo').attr('src', URL.createObjectURL(stream));	          
		        }
	        }

		},
		onremotestream: function(stream) {
			// The publisher stream is sendonly, we don't expect anything here
		},
		oncleanup: function() {
			// Janus.log(" ::: Got a cleanup notification: we are unpublished now :::");
			mystream = null;
			$('#videolocal').html('<button id="publish" class="btn btn-primary">Publish</button>');
			$('#publish').click(function() { publishOwnFeed(true); });
			$("#videolocal").parent().parent().unblock();
		}
	});
}

function createroom()
{
    // console.warn("CREATEROOM");
    var jsonRequest = {
      "request": "create",
      "room": classid,
      "ptype": "publisher",
      "description": "",
      "publishers": 2, //hanya 1 orang sebagai publisher
      "bitrate": 256000,
      "audiocodec": "opus",
      "videocodec": "vp9",
      "record": true,
      "rec_dir": "/home/videos/" + classid,
      "is_private": true
    };  
        
    sendrequest.send({ 
        "message": jsonRequest,
        success: function(resp){
            // console.warn("Berhasil Create ",  resp);
            joinroomNumber();         
        } 
    });
}

function joinroomNumber()
{
    var jsonRequest = {
        "request": "join",
        "room": classid,
        "ptype": "publisher",
        "display": displayname,
        "id": iduser          
    };
    sendrequest.send({
       "message": jsonRequest
    });
    // Masuk ke event Join
} 

function attachThisuserJoin(id, publishers)
{
    // console.warn(idtutor);
    // console.warn(iduser);    
    if (idtutor == id) 
    {
        publishOwnFeed(currentDeviceId);
    }
    eventPublisherJoin(publishers);
}

function eventPublisherJoin(publishers)
{
    // console.warn("EVENTPUBLISHERJOIN");
    if (publishers) 
    {
        for (var f in publishers) {
            var id = publishers[f]["id"];
            var display = publishers[f]["display"];
            attachListener(id, display);          
        }
    }
}

function publishOwnFeed(deviceId)
{
    var videoConfig = JSON.parse(window.localStorage.getItem('videoConfig'));
    // if (currentDeviceId != null) {
    //  deviceId = currentDeviceId;
    // }
    var jsonRequest = {};
    // console.warn("== DEVICES ==" + deviceId);
    if (deviceId) {
        jsonRequest = {
            "audioRecv": false,
            "videoRecv": false,
            "audioSend": true,
            "videoSend": true,
            "video":{"deviceId": deviceId, "width": "1024", "height": "702"},
            "data": true
        };
         // console.warn("== CHECK ==" + deviceId);
    } 
    else {
        jsonRequest = {
            "audioRecv": false,
            "videoRecv": false,
            "audioSend": true,
            "videoSend": true,
            "video": videoConfig.defaultResolution,
            "data": true
        };
    }
    sendrequest.createOffer({
        media: jsonRequest,
        trickle: true,
        success: function(jsep)
        {
            sendPublish(jsep);
        },
        error: function(error)
        {
            setTimeout(function(){
                $.LoadingOverlay("hide");
            },2000);         
            // console.log("error bego", error);
        }
    });
}

function sendPublish(jsep)
{
    $.LoadingOverlay("hide");
    var jsonRequest = {};
    jsonRequest.request = "configure";
    jsonRequest.audio = true;
    jsonRequest.video = true;
    jsonRequest.bitrate = 256000;
    jsonRequest.audiocodec = "opus";
    jsonRequest.videocodec = "vp9";
    sendrequest.send({ "message": jsonRequest, "jsep": jsep});    
}

function attachListener(id, display) {
  	// A new feed has been published, create a new plugin handle and attach to it as a listener
  	var remoteFeed = null;
  	janus.attach(
    {
  			plugin: "janus.plugin.videoroom",
  			opaqueId: opaqueId,
  			success: function(pluginHandle) {
    				remoteFeed = pluginHandle;
    				// Janus.log("Plugin attached! (" + remoteFeed.getPlugin() + ", id=" + remoteFeed.getId() + ")");
    				// Janus.log("  -- This is a subscriber");
    				// We wait for the plugin to send us an offer
    				var listen = { "request": "join", "room": classid, "ptype": "listener", "feed": id, "private_id": mypvtid, "id":iduser };
    				remoteFeed.send({"message": listen});
    				$("#classname").text(classname);
            $("#subclass").text(class_description);
            $("#datenow").text("Class will end at "+endTimeclass);
            $("#nametutor").text(tutorname);

            $("#classnamee").text(classname);
            $("#subclasss").text(class_description);
            $("#datenoww").text("Class will end at "+endTimeclass);
            $("#nametutorr").text(tutorname);             
    				if (usertype == "tutor") {
      					// console.warn("TUTOR");
      					$("#usertypestudent").css('display', 'none'); 
      					$("#tempatstudent").css('display', 'none');
      					$("#usertypetutor").css('display', 'block');                    
      					$("#tempattutor").css('display', 'block');
    				}       
    				else if(usertype == "student")
    				{
      					// console.warn("STUDENT");
      					$("#usertypetutor").css('display', 'none');
      					$("#tempattutor").css('display', 'none');
      					$("#usertypestudent").css('display', 'block');   
      					$("#tempatstudent").css('display', 'block');                  
    				}
  			},
  			error: function(error) {
    				Janus.error("  -- Error attaching plugin...", error);
    				bootbox.alert("Error attaching plugin... " + error);
  			},
  			onmessage: function(msg, jsep) {
    				Janus.debug(" ::: Got a message (listener) :::");
    				Janus.debug(JSON.stringify(msg));
    				var event = msg["videoroom"];
    				Janus.debug("Event: " + event);
    				if(event != undefined && event != null) {
      					if(event === "attached") {
        						// Subscriber created and attached
        						for(var i=1;i<6;i++) {
          							if(feeds[i] === undefined || feeds[i] === null) {
          								feeds[i] = remoteFeed;
          								remoteFeed.rfindex = i;
          								break;
          							}
        						}
        						// listeners += 1;
        						remoteFeed.rfid = msg["id"];
        						remoteFeed.rfdisplay = msg["display"];
        						// if(remoteFeed.spinner === undefined || remoteFeed.spinner === null) {
        						// 	var target = document.getElementById('videoremote'+remoteFeed.rfindex);
        						// 	remoteFeed.spinner = new Spinner({top:100}).spin(target);
        						// } else {
        						// 	remoteFeed.spinner.spin();
        						// }
        						// Janus.log("Successfully attached to feed " + remoteFeed.rfid + " (" + remoteFeed.rfdisplay + ") in room " + msg["room"]);
        						// $('#remote'+remoteFeed.rfindex).removeClass('hide').html(remoteFeed.rfdisplay).show();
      					} 
                else if(msg["error"] !== undefined && msg["error"] !== null) {
      						  // bootbox.alert(msg["error"]);
      					} 
                else 
                {
      						  // What has just happened?
      					}
  				  }
  				  if(jsep !== undefined && jsep !== null) {
      					Janus.debug("Handling SDP as well...");
      					Janus.debug(jsep);
      					// Answer and attach
      					remoteFeed.createAnswer(
      					{
      							jsep: jsep,
      							// Add data:true here if you want to subscribe to datachannels as well
      							// (obviously only works if the publisher offered them in the first place)
      							media: { audioSend: false, videoSend: false, data: true },	// We want recvonly audio/video
      							success: function(jsep) {
        								Janus.debug("Got SDP!");
        								Janus.debug(jsep);
        								var body = { "request": "start", "room": classid };
        								remoteFeed.send({"message": body, "jsep": jsep});
      							},
      							error: function(error) {
        								Janus.error("WebRTC error:", error);
        								bootbox.alert("WebRTC error... " + JSON.stringify(error));
      							}
      					});
      			}
  			},
  			ondataopen: function(data) {
  				  Janus.log("The DataChannel is available! listener ");
  			},
  			ondata: function(data) {
    				Janus.debug("We got data from the DataChannel! listener " + data);
    				console.warn(rhands);
    				if (usertype == "student" && rhands) {			
                doHangup();
    				}
  			},
  			webrtcState: function(on) {
  				  // Janus.log("Janus says this WebRTC PeerConnection (feed #" + remoteFeed.rfindex + ") is " + (on ? "up" : "down") + " now");
            $("#myvideostudent").parent().parent().unblock();
  			},
  			onlocalstream: function(stream) {
  				  // The subscriber stream is recvonly, we don't expect anything here
  			},
  			onremotestream: function(stream) {
    				Janus.debug("Remote feed #" + remoteFeed.rfindex);				
    				Janus.debug(JSON.stringify(stream));    
    				if (page_Type == "web") 
    				{
      					console.warn("masuk cong");
      					console.warn(id);
      					console.warn("tutor"+idtutor);
      					// $('#myvideostudent').attr('src', URL.createObjectURL(stream));
      					if (id == idtutor) {
      						  $('#myvideostudent').attr('src', URL.createObjectURL(stream));
      					} 
                else 
                {
        						console.warn("masuk conggg");
        						if (usertype == "tutor") {
          							console.warn("Raise Hand");
          							$("#videoBoxRemote").empty();
          							$('#videoBoxRemote').append('<video id="videoCallRemote" style="z-index: 1; position: absolute; margin-bottom: 7%; margin-right:7%; height: 250px; width: 250px; bottom: 0; right: 0;" autoplay></video>');  
          							$('#videoCallRemote').attr('src', URL.createObjectURL(stream));
          							$("#bukakotakraise").css('display','none');
          							$("#videoBoxRemote").css('display','block');             
          							$(".tutuprh").css('display','block');
        						} 
                    else 
                    {					
          							console.warn("masuk student lain");
          							$("#videoBoxLocal").empty();
          							$('#videoBoxLocal').append('<video id="videoCallLocal" style="z-index: 1; position: absolute; margin-bottom: 7%; margin-right:7%; height: 250px; width: 250px; bottom: 0; right: 0;" autoplay></video>');
          							$('#videoCallLocal').attr('src', URL.createObjectURL(stream));
        						}
      					}
      					console.log(stream);
    				} 
            else {
      					if (usertype=="student") 
      					{
      						$('#playerse').attr('src', URL.createObjectURL(stream));
      					}
      					else
      					{
      						$('#playerse_tutor').attr('src', URL.createObjectURL(stream));  
      					}

                var orientation = $("#orientation").val();
                if (orientation == "potrait") {  
                    $("#tempatraisehand").css('display','block');
                    $("#tempatraise").css('display','none');
                    $("#tempatraisee").css('display','none');
                    $("#tempathangup").css('display','block');
                    $("#tempathangupp").css('display','block');
                }
                else
                {                  
                    $("#tempatraise").css('display','none');
                    $("#tempatraisee").css('display','none');
                    $("#tempathangup").css('display','block');
                    $("#tempathangupp").css('display','block');
                }

      					setTimeout(function(){
      						  $.LoadingOverlay("hide");
      					},3000); 
  				  }
  			},
  			oncleanup: function() {
  				Janus.log(" ::: Got a cleanup notification (remote feed " + id + ") :::");  				
  			}
		});
}


function answerRequest(jsep, remoteFeed) {
    var jsonRequest = {
      "audioRecv": true,
      "videoRecv": true,
      "audioSend": false,
      "videoSend": false,
      "video": {"deviceId": currentDeviceId, "width": "1024", "height": "702"},
      "data": true
    };
    // Answer
    sendrequestListener.createAnswer({
        jsep: jsep,
        media: jsonRequest,
        trickle: true,
        success: function (jsep) {
            attachRequest(jsep, sendrequest);
        },
        error: function (error) {
            // $scope.addNewLog("Error Answer : " + error, "ERROR");
        }
    });
}

function attachRequest(jsep, sendrequest) {
    var jsonRequest = jsonRequest = {
        "request": "start",
        "room": classid
    };
    sendrequestListener.send({ "message": jsonRequest, "jsep": jsep });
}


// ============= JANUS CHATROOM ==============================
function getDataChat(){
    console.warn("Start "+startTime);
    console.warn("End "+endTime);
    console.warn("Class "+classid);
    $.ajax({
        url: 'https://classmiles.com/Rest/get_chat',
        type: 'POST',
        data: {
            dateawal : startTime,
            dateakhir : endTime,
            class_id : classid
        },
        success: function(response)
        { 
            // console.warn(response);
            if (response.data.length > 0) {
                for (var i = 0; i < response.data.length;i++) {
                //    {         
                 //     $("#chatBoxScroll").append('<div class="listview" style="width: 100%; word-wrap: break-word; background-color:rgba(187, 222, 251, 0.3); margin-bottom:2px;" id="chatBoxScroll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title" id="panel8"><b>'+ chatParticipants[response.data[i]["sender_name"]] +' - Tutor </b></div><small style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ response.data[i]["text"] +'</small></div></div></a></div>');            
                  //     $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;
                //    } else {
                 //     $("#chatBoxScroll").append('<div class="listview" style="width: 100%; word-wrap: break-word;" id="chatBoxScroll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title" id="panel8"><b>'+ chatParticipants[response.data[i]["sender_name"]] +'</b></div><small style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ response.data[i]["text"] +'</small></div></div></a></div>');           
                  //     $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;
                  // }
                    console.warn("Chat data : " +response);
                    if (page_Type == "android") {
                        console.warn("Andro Chat");
                        if(response.data[i]["sender_name"].slice(6,response.data[i]["sender_name"].length) == idtutor)
                        {         
                          $("#chatAndroid").append('<div class="chating" background-color:rgba(187, 222, 251, 0.3)><label>'+ response.data[i]["sender_name"] +'<br><small style="word-wrap: break-word;">'+ response.data[i]["text"] +'</small></label></div>');            
                          $('#chatAndroid').get(0).scrollTop = $('#chatAndroid').get(0).scrollHeight;
                        } else {
                          $("#chatAndroid").append('<div class="chating"><label>'+ response.data[i]["sender_name"] +'<br><small style="word-wrap: break-word;">'+ response.data[i]["text"] +'</small></label></div>');            
                          $('#chatAndroid').get(0).scrollTop = $('#chatAndroid').get(0).scrollHeight;
                        }
                    } 
                    else 
                    {
                        if(response.data[i]["sender_name"].slice(6,response.data[i]["sender_name"].length) == idtutor)
                        {         
                          $("#chatBoxScroll").append('<div class="listview" style="width: 100%; word-wrap: break-word; background-color:rgba(236, 236, 236, 0.3); margin-bottom:2px;" id="chatBoxScroll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title c-white" id="panel8"><b>'+ response.data[i]["sender_name"] +' - Tutor </b></div><small class="c-white" style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ response.data[i]["text"] +'</small></div></div></a></div>');            
                          $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;                   
                        } else {
                          $("#chatBoxScroll").append('<div class="listview" style="width: 100%; word-wrap: break-word; background-color:rgba(236, 236, 236, 0.3); margin-bottom:2px;" id="chatBoxScroll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title c-white" id="panel8"><b>'+ response.data[i]["sender_name"] +'</b></div><small class="c-white" style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ response.data[i]["text"] +'</small></div></div></a></div>');           
                          $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;                   
                        }
                    }
                }
            }
        }
    });
}

setInterval(function(){
    if (checkchat == 0) {
        if (usertype == "tutor") 
        {
            $(".iconchattutor").attr("src","../aset/img/baru/chatnotif/chat icon-03.png");
        }
        else
        {
            $(".iconchatstudent").attr("src","../aset/img/baru/chatnotif/chat icon-03.png");
        }
    }       
    else
    {
        if (usertype == "tutor") 
        {
            var lebarchat = parseInt($("#tempattutor").css('width'));
            var layarlebar = parseInt(lebarlayar);

            if (lebarchat < layarlebar) {
                // $("#iconchattutor").empty();
                // $("#iconchattutor").append("<img src='../aset/img/baru/chatnotif/chat icon-03.png' style='height:23px; width:23px; margin-top: 7px;'/>");
                $(".iconchattutor").attr("src","../aset/img/baru/chatnotif/chat icon-03.png");
            } 
            else
            {
                // alert('ada chat masuk tutor');                    
                $(".iconchattutor").attr("src","../aset/img/baru/chatnotif/chat icon notif-02.png");
                // $("#iconchattutor").append("<img src='../aset/img/baru/chatnotif/chat icon notif-03.png' style='height:23px; width:23px; margin-top: 7px;'/>");
            }               
        }
        else
        {
            var lebarchats = parseInt($("#tempatstudent").css('width'));
            var layarlebars = parseInt(lebarlayar);

            if (lebarchats < layarlebars) {
                // $("#iconchatstudent").empty();
                // $("#iconchatstudent").append("<img src='../aset/img/baru/chatnotif/chat icon-03.png' style='height:23px; width:23px; margin-top: 7px;'/>");
                $(".iconchatstudent").attr("src","../aset/img/baru/chatnotif/chat icon-03.png");
            }
            else
            {
                // alert('ada chat masuk');
                // $("#iconchatstudent").empty();
                // $("#iconchatstudent").append("<img src='../aset/img/baru/chatnotif/chat icon notif-03.png' style='height:23px; width:23px; margin-top: 7px;'/>");
                $(".iconchatstudent").attr("src","../aset/img/baru/chatnotif/chat icon notif-02.png");
            }
            // console.warn('chat student');
        }
    }
},1000);

function attachChatPlugin(Room, IdUser, Display) {
  classidcp = Room;
  idusercp = IdUser;
  displaynamecp = Display;
      // Attach to echo test plugin
      console.warn("INIT CHAT "+JanusConfigService.pluginChatRoom);
      janus.attach(
      {
        plugin: JanusConfigService.pluginChatRoom,
        success: function (pluginHandle) {
            // $('#details').remove();
            textroom = pluginHandle;
            // Janus.log("Plugin attached! (" + textroom.getPlugin() + ", id=" + textroom.getId() + ")");
            // Setup the DataChannel
            var body = { "request": "setup", "room": classid };
            Janus.debug("Sending message (" + JSON.stringify(body) + ")");
            textroom.send({ "message": body });
          },
          error: function (error) {
            addNewLog(error, "ERROR");
          },
          webrtcState: function (on) {
            // Janus.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
          },
          onmessage: function (msg, jsep) {
            Janus.debug(" ::: Got a message :::");
            Janus.debug(JSON.stringify(msg));
            if (msg["error"] !== undefined && msg["error"] !== null) {
              addNewLog(msg["error"], "ERROR");
            }
            if (jsep !== undefined && jsep !== null) {
              // Answer
              textroom.createAnswer(
              {
                jsep: jsep,
                  media: { audio: false, video: false, data: true },  // We only use datachannels
                  success: function (jsep) {
                    var body = { "request": "ack" };
                    textroom.send({ "message": body, "jsep": jsep });
                  },
                  error: function (error) {
                    addNewLog(error, "ERROR");
                  }
                });
            }
          },
          ondataopen: function (data) {
            // Janus.log("The DataChannel is available!");
            // Prompt for a display name to join the default room
            checkchat = 0;
            createRoomChat();
          },
          ondata: function (data) {
            Janus.debug("We got data from the DataChannel! " + data);
            handleMessage(data);
            if (usertype == "tutor") 
            {
                checkchat = 1;
            }
            else
            {
                checkchat = 1;              
            }
          },
          oncleanup: function () {
            // Janus.log(" ::: Got a cleanup notification :::");
            $('#datasend').attr('disabled', true);
            checkchat = 0;
          }
        }
        );
    }

    function createRoomChat() {

      var transaction = randomString(12);
      var register = {
        textroom: "create",
        transaction: transaction,
        room: classid,
        description: "",
        is_private: false,
        pin: "",
        post: "",
        secret: "",
        permanent: false
      };
      textroom.data({
        text: JSON.stringify(register),
        success: function (resp) {
          registerUsernameChat();
        }
      });
    }

    function registerUsernameChat() {
      var transaction = randomString(12);
      var register = {
        textroom: "join",
        transaction: transaction,
        room: classid,
        username: "userId" + iduser,
        display: displayname
      };

      transactions[transaction] = function (response) {
        if (response["textroom"] === "error") {
          // Something went wrong
          addNewLog(response["error"], "ERROR");
          return;
        }
        // Any participants already in?
        if (response.participants && response.participants.length > 0) {
          for (var i in response.participants) {
            var p = response.participants[i];
            var username = p.display ? p.display : p.username;
            chatParticipants[p.username] = username;
            // appendMessage(p.username, new Date(), false, "JOIN", "joined");
            // $('#chatroom').get(0).scrollTop = $('#chatroom').get(0).scrollHeight;
          }
        }
      };
      textroom.data({
        text: JSON.stringify(register),
        error: function (reason) {
          addNewLog(reason, "ERROR");
        },
        success: function () { 
          chatReady = true;
          console.warn("ShowDataChat");
          getDataChat(); 
        }
      });
    }

    function sendDataChat() {
      var inputText = $("#chatInput").val();
      var inputTextt = $("#chatInputt").val();
      if (inputText) {
        var message = {
          textroom: "message",
          transaction: randomString(12),
          room: classid,
          text: inputText
        };
        textroom.data({
          text: JSON.stringify(message),
          error: function (reason) { addNewLog(reason, "ERROR"); },
          success: function () { $("#chatInput").val(null); }
        });
      }
      if (inputTextt) {
        var message = {
          textroom: "message",
          transaction: randomString(12),
          room: classid,
          text: inputTextt
        };
        textroom.data({
          text: JSON.stringify(message),
          error: function (reason) { addNewLog(reason, "ERROR"); },
          success: function () { $("#chatInputt").val(null); }
        });
      }
    }

    function sendDataChatAndroid() {
      var inputText = $("#chatInputAndroid").val();
      if (inputText) {
        var message = {
          textroom: "message",
          transaction: randomString(12),
          room: classid,
          text: inputText
        };
        textroom.data({
          text: JSON.stringify(message),
          error: function (reason) { addNewLog(reason, "ERROR"); },
          success: function () { $("#chatInputAndroid").val(null); }
        });
      }
    }

    function getDate(jsonDate) {
      var when = new Date();
      if (jsonDate) {
        when = new Date(Date.parse(jsonDate));
      }
      return when;
    }

    function handleMessage(data) {
      var json = JSON.parse(data);
      var transaction = json["transaction"];
      if (transactions[transaction]) {
        // Someone was waiting for this
        transactions[transaction](json);
        delete transactions[transaction];
        return;
      }
      var what = json["textroom"];

      // ================ Incoming message: public or private? ============
      if (what === "message") {
        var msg = json["text"];
        msg = msg.replace(new RegExp('<', 'g'), '&lt');
        msg = msg.replace(new RegExp('>', 'g'), '&gt');
        appendMessage(json["from"], json["date"], json["whisper"], "CHAT", msg);
      } // ====================== Somebody joined =========================
      else if (what === "join") {
        var username = json["display"] ? json["display"] : json["username"];
        chatParticipants[json["username"]] = username;
        // appendMessage(json["username"], json["date"], false, "JOIN", username+" joined");
      } // ====================== Somebody left ===========================
      else if (what === "leave") {
        var username = json["username"];
        // appendMessage(username, json["date"], false, "LEAVE", username+" left");
      } // ================= Room was destroyed, goodbye! =================
      else if (what === "destroyed") {
        addNewLog("The room has been destroyed!", "ERROR");
      }

      // $('#chatroom').get(0).scrollTop = $('#chatroom').get(0).scrollHeight;
    }

    function appendMessage(participant, dateTime, isPrivate, type, msg) {

      var newMessage = {};
      newMessage.from = participant;
      newMessage.datetime = dateTime;
      newMessage.isPrivate = isPrivate;
      newMessage.type = type;
      newMessage.message = msg;
      chatData.splice(0, 0, newMessage);

      var newArchive = {};
      newArchive.sender_name = chatParticipants[participant];
      newArchive.text = msg;
      newArchive.datetime = dateTime;
      archiveChats.push(newArchive);

        // if (pageType == "video") {
        //   var divChat = document.getElementById('chatBoxScroll');
        //   divChat.scrollTop = divChat.scrollHeight;
        // }
        if (newMessage.type == "CHAT") {
          if (page_Type == "web") {
            if(participant.slice(6,participant.length) == idtutor)
            {         
              $("#chatBoxScroll").append('<div class="listview" style="width: 100%; word-wrap: break-word; background-color:rgba(236, 236, 236, 0.3); margin-bottom:2px;" id="chatBoxScroll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title c-white" id="panel8"><b>'+ chatParticipants[participant] +' - Tutor </b></div><small class="c-white" style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ newMessage.message +'</small></div></div></a></div>');            
              $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;
              
            } else {
              $("#chatBoxScroll").append('<div class="listview" style="width: 100%; word-wrap: break-word; background-color:rgba(236, 236, 236, 0.3); margin-bottom:2px;" id="chatBoxScroll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title c-white" id="panel8"><b>'+ chatParticipants[participant] +'</b></div><small class="c-white" style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ newMessage.message +'</small></div></div></a></div>');           
              $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;
              
            }
          } else {
            if(participant.slice(6,participant.length) == idtutor)
            {         
              $("#chatAndroid").append('<div class="chating" background-color:rgba(187, 222, 251, 0.3)><label>'+ chatParticipants[participant] +'<br><small style="word-wrap: break-word;">'+ newMessage.message +'</small></label></div>');            
              $('#chatAndroid').get(0).scrollTop = $('#chatAndroid').get(0).scrollHeight;
            } else {
              $("#chatAndroid").append('<div class="chating"><label>'+ chatParticipants[participant] +'<br><small style="word-wrap: break-word;">'+ newMessage.message +'</small></label></div>');            
              $('#chatAndroid').get(0).scrollTop = $('#chatAndroid').get(0).scrollHeight;
            }
          }
        }
        // if(newMessage.type == "JOIN"){
        //  $("#chatBoxScroll").append('<div class="listview" ><a class="lv-item" href="#"><div class="media"><div class="pull-left p-relative"><img class="lv-img-sm" src="../../aset/img/user/empty.jpg" alt=""></div><div class="media-body"><div class="lv-title c-green" style="margin-left:7px; margin-top:3px;">' + chatParticipants[participant] + ' joined</div></div></div></a></div>');
        //  // $("#chatBoxScroll").append('<p style="color:green;"><b>' + chatParticipants[participant] + ' joined</b></p>');
         //  $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;
        // }
        // if(newMessage.type == "LEAVE"){
        //  $("#chatBoxScroll").append('<div class="listview" ><a class="lv-item" href="#"><div class="media"><div class="pull-left p-relative"><img class="lv-img-sm" src="../../aset/img/user/empty.jpg" alt=""></div><div class="media-body"><div class="lv-title c-red" style="margin-left:7px; margin-top:3px;">' + chatParticipants[participant] + ' left</div></div></div></a></div>');
        //  // $("#chatBoxScroll").append('<p style="color:red;"><b>' + chatParticipants[participant] + ' left</b></p>');
         //  $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;
        // }
      }

      function randomString(len, charSet) {
        charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var randomString = '';
        for (var i = 0; i < len; i++) {
          var randomPoz = Math.floor(Math.random() * charSet.length);
          randomString += charSet.substring(randomPoz, randomPoz + 1);
        }
        return randomString;
      }

      function archiveChat(dataChat){
        var param = {
          "class_id": classid,
          "chat_arr": dataChat
        };
        // console.warn(JSON.stringify(param));
        // console.warn(param);
        // var param = JSON.stringify(param);
        $.ajax({
          url: 'https://classmiles.com/Rest/rec_chat',
          type: 'POST',
          data: param,
          success: function(data)
          { 


        // if (play_pos == 1) {
          // $.ajax({
          //  url: 'https://classmiles.com/Rest/list_video',
          //  type: 'GET',
          //  data: {
          //    session: session
          //  },
          //  success: function(data)
          //  {                         
              // console.warn(data);    
              archiveChats = [];
          //  }
          // });
        // }

      }
    });
      }

      function addNewLog(text, type){
      // console.log(text+" : "+type)
    }

    $('#chatInput').keypress(function(){
      var theCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
      if(theCode == 13) {
        sendDataChat();
        return false;
      } else {
        return true;
      }
    });

    $('#chatInputt').keypress(function(){
      var theCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
      if(theCode == 13) {
        sendDataChat();
        return false;
      } else {
        return true;
      }
    });

    $('#chatInputAndroid').keypress(function(){
      var theCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
      if(theCode == 13) {
        sendDataChatAndroid();
        return false;
      } else {
        return true;
      }
    });

    // ============= END JANUS CHATROOM ==============================




  // ============= EVENT CLICK ============================== 

  var klik;
  var b;
  // $("#chat-trigger").click(function(){   
  //  $("#chat").css('left','0px');   
  //  $("#chat").css('display','block');
  //  $("#chat").css('z-index','100');
  // });

  $("#klikchat").click(function(){    

  });

  $(document.body).click(function(e) {
    var f = $("#tempatstudent").width() / $('#tempatstudent').parent().width() * 100;  
    var g = $("#tempattutor").width() / $('#tempattutor').parent().width() * 100;
    e = e || window.event;
    e = e.target;
    var myClass = $(this).attr("class");

    if (e.id == "klikchat" || e.id == "chat-trigger") {
      // alert(f);
      checkchat = 0; 
      if (f == 100) {         
        $("#tempatstudent").css('width','80%');       
      }
      if (g == 100) {
        $("#tempattutor").css('width','80%');
      }
      else
      {
        $("#tempatstudent").css('width','100%');
        $("#tempattutor").css('width','100%');
      }
    }       
    else if(e.id == "mCSB_1" || e.id == "mCSB_1_container" || e.id == "mCSB_1_scrollbar_vertical" || e.id == "chat" || e.id == "chat" || e.id == "panel1" || e.id == "panel2" || e.id == "chatBoxScroll" || e.id == "chatBoxScroll" || e.id == "panel3" || e.id == "panel4" || e.id == "chatInput" || e.id == "panel5" || e.id == "panel6" || e.id == "panel7" || e.id == "panel8" || e.id == "panel9")
    {
      checkchat = 0; 
      $("#tempatstudent").css('width','80%');
      $("#tempattutor").css('width','80%');
    }
    else
    {
      checkchat = 0; 
      $("#tempatstudent").css('width','100%');
      $("#tempattutor").css('width','100%');
    }
  });

  if ($("#chatInput").is(":focus")) {
      checkchat == 0;
  }

  $("#devicedetect").click(function(){
    getListDevice();
  });

  $("#whiteboardsetting").click(function(){
    $("#whiteboardlist").modal("show");
  });

  $("#devicedetects").click(function(){
    console.log("Andro Set");
    getListDevices();
  });
  
  function getListDevice() {
    var listdevicee = "";
    var iddevice = "";
    Janus.listDevices(function (devices) {
      var deviceVideo = [];
      var deviceAudio = [];

      for (var i = 0; i < devices.length; i++) {
        if (devices[i].kind == "videoinput") {
          deviceVideo.push(devices[i]);
        } else if (devices[i].kind == "audioinput") {
          deviceAudio.push(devices[i]);
        }
      }

      $("#buttonlist").empty();
      listDevices.video = deviceVideo;
      listDevices.audio = deviceAudio;

      noid = 1;
      for(var f in listDevices.video) {         
        listdevicee = listDevices.video[f]["label"];
        iddevice = listDevices.video[f]["deviceId"];
        console.warn("INI LIST DEVICE "+listdevicee+" INI ID DEVICE "+iddevice);
        $("#buttonlist").append("<button class='b btn btn-default btn-block btn-sm' style='margin:10px; padding:5px;' id='iddevice"+noid+"' ><label class='iniclick' id='listdevice"+noid+"' ></label></button>");      
        $("#iddevice"+noid+"").attr("cm-device",iddevice);
        $("#listdevice"+noid+"").text(listdevicee);
        noid++;
      }     
      
      $("#deviceConfigurationModal").modal("show");

    });
  }

  function getListDevices() {
    var listdevicee = "";
    var iddevice = "";
    Janus.listDevices(function (devices) {
      var deviceVideo = [];
      var deviceAudio = [];

      for (var i = 0; i < devices.length; i++) {
        if (devices[i].kind == "videoinput") {
          deviceVideo.push(devices[i]);
        } else if (devices[i].kind == "audioinput") {
          deviceAudio.push(devices[i]);
        }
      }

      $("#buttonlists").empty();
      listDevices.video = deviceVideo;
      listDevices.audio = deviceAudio;

      noid = 1;
      for(var f in listDevices.video) {         
        listdevicee = listDevices.video[f]["label"];
        iddevice = listDevices.video[f]["deviceId"];
        console.warn("INI LIST DEVICE "+listdevicee+" INI ID DEVICE "+iddevice);
        $("#buttonlists").append("<button class='b btn btn-default btn-block btn-sm' style='margin:10px; padding:5px;' id='iddevice"+noid+"' ><label class='iniclick' id='listdevice"+noid+"' ></label></button>");     
        $("#iddevice"+noid+"").attr("cm-device",iddevice);
        $("#listdevice"+noid+"").text(listdevicee);
        noid++;
      }     
      
      $("#deviceConfigurationModals").modal("show");

    });
  }

  $('#buttonlist').on('click', '.b', function(){
    var id_device = $(this).attr("cm-device");
    changeDeviceConfiguration(id_device);
  });

  function changeDeviceConfiguration(deviceId) 
  {
    console.warn("== CHECK ==" + deviceId+" ini id lama "+currentDeviceId);
    if (deviceId) {
     currentDeviceId = deviceId;
     if (usertype == "tutor") {
      sendrequest.hangup();
      sendrequest.detach();
      sendrequest = "READY";
      asdf = "Ganti";
      attachPublisher();
    }
  }

  $("#deviceConfigurationModal").modal("hide");
}

$("#keluarcobaaa").click(function(){    
    $("#classkeluar").modal("show");
});

$("#keluarcobaa").click(function(){   
    $("#classkeluar").modal("show");
});

$("#iddevice1").click(function(){
    var id_device = $(this).attr("cm-device");
});

$('#mute').click(toggleMute);

function toggleMute() {   
  var muted = sendrequest.isAudioMuted();
    // Janus.log((muted ? "Unmuting" : "Muting") + " local stream...");
    if(muted)
    {
      sendrequest.unmuteAudio();      
    }
    else
    {
      sendrequest.muteAudio();      
    }
    muted = sendrequest.isAudioMuted();
    $('#mute').html(muted ? "<i class='tm-icon zmdi zmdi-volume-off' title='Audio Off'></i>" : "<i class='tm-icon zmdi zmdi-volume-up' title='Audio On'></i>");
}

$('#unpublish').click(unpublishOwnFeed);

function unpublishOwnFeed() {
    // 
    var muted = sendrequest.isVideoMuted();
    // Janus.log((muted ? "Unmuting" : "Muting") + " local stream...");
    if(muted)
        sendrequest.unmuteVideo();
    else
        sendrequest.muteVideo();
    muted = sendrequest.isVideoMuted();
    $('#unpublish').html(muted ? "<i class='tm-icon zmdi zmdi-eye-off' title='Video Off'></i>" : "<i class='tm-icon zmdi zmdi-eye' title='Video On'></i>");
}

$("#keluar").click(function(){
    st_tutor = 0;
    destroyToken();
}); 

function matikanstreamsiswa(){
  	$("#videoBoxLocal").empty();
  	$("#videoBoxRemote").empty();
  	$(".tutuprhh").css('display','none');        
    $("#raisehandclick").css('display','block');
    $("#videoBoxRemote").css('display','none');
    $(".tutuprhh").css('display','none');        
    $("#raisehandclick").css('display','block');
    $("#tanganmerah").css('display','none');
    $("#tanganbiasa").css('display','block');
    $("#tempathangup").css('display','none');            
    $("#tempathangupp").css('display','none');
    $("#tempatraise").css('display','block');
    $("#tempatraisee").css('display','block');
    cektangan = 0;
    var unpublishh = { "request": "unpublish" };
    sendrequest.send({"message": unpublishh});  
}

$('#pausee').click(matikanstreamsaya);

function matikanstreamsaya() {
    $("#pausee").css('display','none');   
    // var aa = $("#tempatx").attr("title").remove();
    // a.text("Break");
    
    var unpublishh = { "request": "unpublish" };
    sendrequest.send({"message": unpublishh});

    $.ajax({
        url: 'https://classmiles.com/Rest/break',
        type: 'GET',
        data: {
          session: session
        },
        success: function(data)
        { 
            st_break = 1;              
            $("#myvideo").attr('muted','false');
            // $("#play").remove('attr','id');
            $("#playboongan").css('display','block');
            setTimeout(function(){
              $("#playboongan").css('display','none');
              $("#play").css('display','block');
            },10000); 
            notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Silahkan Tunggu 10 detik untuk memulai kelas kembali");
            if (vartutor == 0 && st_break == 1) 
            {           
              if (usertype == "student") 
              {   
                $("#myvideostudent").attr('src', listVideo);
                $("#myvideostudent").attr('loop','true');
              }
            }
            else
            {
                $("#playerse").attr('src', listVideo);
                $("#playerse").attr('loop','true');
            }              
        }
    });  
}

$('#play').click(nyalakanstream);

function nyalakanstream()
{
    // alert('a');
    $("#play").css('display','none');
    $("#playboongan").css('display','none'); 
    $("#pausee").css('display','block');
    $("#myvideo").attr("src",'');
    $("#myvideostudent").attr("src",'');
    $.ajax({
        url: 'https://classmiles.com/Rest/unbreak',
        type: 'GET',
        data: {
          session: session
        },
        success: function(data)
        {             
          st_break = 0;
          $("#myvideo").attr('muted','true');
          // console.warn("BERHASIL UNBREAK");

          // var aa = $("#tempatx").attr("title").remove();
          // a.text("Play");
          publishOwnFeed(true);
        }
    });
}

  function destroyToken()
  {
    window.localStorage.removeItem("session");
    window.localStorage.removeItem("type_page");
    $.ajax({
        url: 'https://classmiles.com/Rest/destroy_token', 
        type: 'POST',
        data : {
          token : token                    
        },
        success: function(data) {
                // janus.destroy();
                // sendrequest.detach();
                if (page_Type == "android") {
                  Android();
                } else {
                  location.href='https://www.classmiles.com/';
                }
              }
    });
  }

  function destroyRaiseHand(){
      $.ajax({
        url: 'https://classmiles.com/Rest/raise_hand_destroy/',
        type: 'GET',
        data: {
          session: session
        },
        success: function(data)
        {
          console.warn("SUKSES destroy REQUEST");
    		  rhands = false;
        }
      });
  }

function callraiseHand(session_id){
    $.ajax({
      url: 'https://classmiles.com/Rest/raise_hand_call/',
      type: 'GET',
      data: {
        session: session_id
      },
      success: function(data)
      {
        console.warn("SUKSES call REQUEST");
      }
    });
}

  $("#hangups").click(function(){    
    $("#kotakmenulist").modal("hide");
    $("#callingModal").modal("hide");


    // console.warn("Do hangups");
    if (page_Type == "android") {
        $("#videoBoxLocal").empty();
        $("#tempathangup").css('display','none');            
        $("#tempathangupp").css('display','none');
        $("#tempatraise").css('display','block');
        $("#tempatraisee").css('display','block');			
    }
    else
    {
        $(".tutuprh").css('display','none');        
        $("#bukakotakraise").css('display','block');
        $(".tutuprhh").css('display','none');        
        $("#raisehandclick").css('display','block');
        $("#videoBoxRemote").css('display','none');
        $("#tanganmerah").css('display','none');
        $("#tanganbiasa").css('display','block');
        cektangan = 0;
    }

    doHangup();
  });

  $("#hangupsss").click(function(){

    $("#kotakmenulist").modal("hide");
    $("#callingModal").modal("hide");
    // $("#videoCallModall").modal("hide");
    // $("#tempatraisehand").empty();
    if (page_Type == "android") {
      $("#videoBoxLocal").empty();
        $("#tempathangup").css('display','none');            
        $("#tempathangupp").css('display','none');
        $("#tempatraise").css('display','block');
        $("#tempatraisee").css('display','block');
    }
    else
    {     
        $(".tutuprh").css('display','none');        
        $("#bukakotakraise").css('display','block');
        $(".tutuprhh").css('display','none');        
        $("#raisehandclick").css('display','block');
        $("#videoBoxRemote").css('display','none');
        $("#tanganmerah").css('display','none');
        $("#tanganbiasa").css('display','block');
        cektangan = 0;
    }
    
    doHangup();
  });
    
    $("#hangupssss").click(function(){
    	  aaa = 1;
        $("#kotakmenulist").modal("hide");
        $("#callingModal").modal("hide");
        // $("#videoCallModall").modal("hide");
        // $("#tempatraisehand").empty();
        if (page_Type == "android") {
            $("#videoBoxLocal").empty();
            $("#tempathangup").css('display','none');            
            $("#tempathangupp").css('display','none');
            $("#tempatraise").css('display','block');
            $("#tempatraisee").css('display','block');
        }
        else
        {     
            $(".tutuprh").css('display','none');        
            $("#bukakotakraise").css('display','block');
            $(".tutuprhh").css('display','none');        
            $("#raisehandclick").css('display','block');
            $("#videoBoxRemote").css('display','none');
            $("#tanganmerah").css('display','none');
            $("#tanganbiasa").css('display','block');
            cektangan = 0;
        }

        doHangup();
    });

  $("#hangupss").click(function(){

    $("#kotakmenulist").modal("hide");
    $("#callingModal").modal("hide");

    $("#videoBoxRemote").css('display','none');
    $(".tutuprh").css('display','none');        
    $("#bukakotakraise").css('display','block');
    $(".tutuprhh").css('display','none');        
    $("#raisehandclick").css('display','block');

    // console.warn("Do hangups");
    $("#tanganmerah").css('display','none');
    $("#tanganbiasa").css('display','block');
    cektangan = 0;
    doHangup();
  });

  $("#raisehandclick").click(function(){
    $.ajax({
      url: 'https://classmiles.com/Rest/raise_hand/',
      type: 'GET',
      data: {
        session: session
      },
      success: function(data)
      {
        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Silahkan Tunggu hingga Tutor menjawab Raise hand anda");
        // console.warn("SUKSES SEND REQUEST");
        // alert("test");
        cektangan = 1;
      }
    });
  });

  $("#raisehandclickAndroid").click(function(){
    $.ajax({
      url: 'https://classmiles.com/Rest/raise_hand/',
      type: 'GET',
      data: {
        session: session
      },
      success: function(data)
      {
        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Silahkan Tunggu hingga Tutor menjawab Raise hand anda");
      // console.warn("SUKSES SEND REQUEST");
      // alert("test");
      $("#tanganbiasa").css('display','none');
      $("#tanganmerah").css('display','block');
      }
    });
  });

  $("#raisehandclickAndroidd").click(function(){
    $.ajax({
      url: 'https://classmiles.com/Rest/raise_hand/',
      type: 'GET',
      data: {
        session: session
      },
      success: function(data)
      {
        notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Silahkan Tunggu hingga Tutor menjawab Raise hand anda");
      // console.warn("SUKSES SEND REQUEST");
      // alert("test");
      $("#tanganbiasa").css('display','none');
      $("#tanganmerah").css('display','block');
      }
    });
  });

  $("#showchat").click(function(){


    if ($("#papanchat").css('display') == "block") {
      // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Chat disembunyikan.");
      $("#papanchat").css('display', 'none');
    } else{
      // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Chat ditampilkan.");
      $("#papanchat").css('display', 'block');
    }  
    console.log("Diklik");
  // console.warn("SUKSES SEND REQUEST");
  // alert("test");
  });

  $("#showchatt").click(function(){


    if ($("#papanchatt").css('display') == "block") {
      // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Chat disembunyikan.");
      $("#papanchatt").css('display', 'none');
    } else{
      // notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Chat ditampilkan.");
      $("#papanchatt").css('display', 'block');
    }  
    console.log("Diklik");
  // console.warn("SUKSES SEND REQUEST");
  // alert("test");
  });

  $("#bukakotakraise").click(function(){
    // alert('a');
    $("#kotakraisehand").empty();
    var id_userraise = {};
    var usernameraise = {};
    var sessionraise = {};
    // console.warn("LIST RAISEHAND");    
    // $("#kotakraisehand").append("<div class='col-md-4 m-t-15'><div class='pull-left'>test</div></div><div class='col-md-8 m-t-15'><div class='pull-right'><a id='1'><img src='../../aset/img/iconvideo.png'></a></div></div>");    
    // for(var f in raisehandlist.length) {
      if (raisehandlist.length > 0) {
        noid = 1;         
        for (var f = 0; f < raisehandlist.length; f++) {
          id_userraise = raisehandlist[f]["id_user"];
          usernameraise = raisehandlist[f]["user_name"];  
          sessionraise = raisehandlist[f]["session_id"];
          $("#kotakraisehand").append("<button class='c btn btn-info btn-block btn-sm' session_id='"+raisehandlist[f]['session_id']+"' iduser='"+raisehandlist[f]['id_user']+"' username='"+raisehandlist[f]['user_name']+"' style='margin:10px; padding:5px; cursor:pointer;' ><i class='zmdi zmdi-phone'></i>     <label class='iniclick' >"+ raisehandlist[f]['user_name'] +"</label></button>");          
        // $("#kotakraisehand").append("<div class='col-md-5 m-t-15'><div class='pull-left'>"+ raisehandlist[f]['user_name'] +"</div></div><div class='col-md-7'><div class='pull-right'><button id='jawabraise' class='c' iduser='"+raisehandlist[f]['id_user']+"' username='"+raisehandlist[f]['user_name']+"' style='cursor:pointer;' src='../../aset/img/iconvideo.png'>asdfasdf</button></div></div>");            
      }            
      $("#kotakmenulist").modal("show");
    }
    else
    {
      // alert("Tidak ada data");
      // bootbox.alert("Tidak ada data");     
            // swal("Tidak ada data");            
            $("#tidakadadata").modal("show");
          }

        });

  $("#tutupalert").click(function(){
    $("#tidakadadata").modal("hide");
  });

  $("#tutup").click(function(){
    $("#whiteboardlist").modal("hide");
  });



  $('#kotakraisehand').on('click', '.c', function(){
    var iduser = $(this).attr("iduser");
    var username = $(this).attr("username");
    var session_id = $(this).attr("session_id");
    $("#kotakmenulist").modal("hide");
    cektangan = 1;
    
    // checkBeforeCall(iduser, username);
    callraiseHand(session_id);

  });

  $("#openscreenshare").click(function(){
    $(this).attr("href",'https://classmiles.com/screen_share/#access_session='+session);
  });

  // ============= END EVENT CLICK ==============================



  var videoCall = null;
  var vc_user1 = {};
  var vc_user2 = {};

    // ===========================================================================
    // ========================= VIDEO CALL FUNCTION DISINI ======================
    // ===========================================================================

    function attachVideoCall () {
      // Attach to echo test plugin
      janus.attach(
      {
        plugin: "janus.plugin.videocall",
        success: function (pluginHandle) {
            videoCall = pluginHandle;
            vc_user1.id = "rm" + classid + "vc" + iduser + "|" + randomString(10);

            vc_user1.username = displayname;
            registerUserForCall();
            $("#kotakmenulist").modal("hide");
            $("#callingModal").modal("hide");
            $("#videoBoxRemote").css('display','none');
            $(".tutuprh").css('display','none');        
            $("#bukakotakraise").css('display','block');
            $(".tutuprhh").css('display','none');        
            $("#raisehandclick").css('display','block');
            // $("#tempatraisehand").css('display','none');
            $("#videoBoxLocal").empty();

            // Janus.log("Plugin attached! (" + videoCall.getPlugin() + ", id=" + videoCall.getId() + ")");
            // console.warn("AttachVideoCall");
          },
          error: function (error) {
            addNewLog("Error VideoCall : " + error, "ERROR");
          },
          consentDialog: function (on) {
            Janus.debug("Consent dialog should be " + (on ? "on" : "off") + " now");
            // if (on) {
            //   // Darken screen and show hint
            //   $.blockUI({
            //     message: '<div><img src="up_arrow.png"/></div>',
            //     css: {
            //       border: 'none',
            //       padding: '15px',
            //       backgroundColor: 'transparent',
            //       color: '#aaa',
            //       top: '10px',
            //       left: (navigator.mozGetUserMedia ? '-100px' : '300px')
            //     }
            //   });
            // } else {
            //   // Restore screen
            //   $.unblockUI();
            // }
          },
          mediaState: function (medium, on) {
            // Janus.log("Janus " + (on ? "started" : "stopped") + " receiving our " + medium);
          },
          webrtcState: function (on) {
            // Janus.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
            // $("#videoleft").parent().unblock();
          },
          onmessage: function (msg, jsep) {
            Janus.debug(" ::: Got a message :::");
            Janus.debug(JSON.stringify(msg));
            var result = msg["result"];
            if (result !== null && result !== undefined) {
              if (result["list"] !== undefined && result["list"] !== null) {
                var list = result["list"];
                Janus.debug("Got a list of registered peers:");
                Janus.debug(list);
                var isExist = false;
                for (var mp in list) {
                  if (vc_user2.id == list[mp].split("|", 1)) {
                    doCall(list[mp]);
                    isExist = true;
                    break;
                  }
                }

                if (!isExist) {
                  bootbox.alert(vc_user2.username + " is Offline");
                }
              } else if (result["event"] !== undefined && result["event"] !== null) {
                var event = result["event"];
                if (event === 'registered') {
                  vc_user1.id = result["username"];
                  // Janus.log("Successfully registered as " + vc_user1.username + "!");
                } else if (event === 'calling') {
                  // Janus.log("Waiting for the peer to answer...");
                  // TODO Any ringtone?
                } else if (event === 'incomingcall') {
                  // Janus.log("Incoming call from " + result["username"] + "!");
                  vc_user2.id = result["username"];
                  // TODO Enable buttons to answer
                  // bootbox.confirm("Want to answer ?", function (result) {
                  // Telah selesai diproses, ubah status raise hand
                  //   destroyRaiseHand();

                  //   if (result) {
                  //     doAccept(jsep);
                  //   } else {
                  //     doHangup();
                  //   }
                  // });

                  doAccept(jsep);

                } 
                else if (event === 'accepted') {
                    var peer = result["username"];
                    $("#callingModal").modal("hide");
                    if (page_Type == "android") {
						var orientation = $("#orientation").val();
						if (orientation == "potrait") {  
						  	$("#tempatraisehand").css('display','block');
						  	$("#tempatraise").css('display','none');
                            $("#tempatraisee").css('display','none');
                            $("#tempathangup").css('display','block');
                            $("#tempathangupp").css('display','block');
						}
						else
						{                  
						  	$("#tempatraise").css('display','none');
                            $("#tempatraisee").css('display','none');
                            $("#tempathangup").css('display','block');
                            $("#tempathangupp").css('display','block');
						}
					}
					else
					{
						$(".tutuprh").css('display','block');        
						$("#bukakotakraise").css('display','none');
                        $(".tutuprhh").css('display','block');        
                        $("#raisehandclick").css('display','none');
						$("#videoBoxRemote").css('display','block'); 						
				    }
                  
                    if (peer === null || peer === undefined) {
                        // Janus.log("Call started!");
                    } 
                    else 
                    {
                        // Janus.log(peer + " accepted the call!");
                        vc_user2.id = peer;
                    }
                    // TODO Video call can start
                    if (jsep !== null && jsep !== undefined)
                        videoCall.handleRemoteJsep({ jsep: jsep });
                } 
                else if (event === 'hangup') {
                     Janus.log("Call hung up by " + result["username"] + " (" + result["reason"] + ")!");
                    // TODO Reset status
                    cektangan = 0;
                    videoCall.hangup();
                    clearVideoCall();
                }
              }
            } else {
              // FIXME Error?
              var error = msg["error"];
              addNewLog("Error VideoCall : " + error, "ERROR");
              // TODO Reset status
              videoCall.hangup();
            }
          },
          onlocalstream: function (stream) {
            Janus.debug(" ::: Got a local stream :::");
            if ($('#videoCallLocal').length === 0) {
                if (page_Type == "android") {                    
                    var orientation = $("#orientation").val();
                    if (orientation == "potrait") {                      
                        $('#videoBoxLocal').empty();
                        $('#videoBoxLocal').append('<video id="videoCallLocal" style="z-index: 1; position: absolute; margin-bottom: 2%; margin-right:-4%; height: 120px; width: 150px; bottom: 0; right: 0;" autoplay data-video-aspect-ratio="16:9"></video>');
                    }   
                    else
                    {                                       
                        $('#videoBoxLocal').empty();     
                        $('#videoBoxLocal').append('<video id="videoCallLocal" style="width:150px; height:120px;" autoplay data-video-aspect-ratio="16:9"></video>');
                    }                                                           
                }
                else
                {
                    
                    $('#videoBoxLocal').append('<video id="videoCallLocal" style="z-index: 1; position: absolute; margin-bottom: 2%; margin-right:5%; height: 400px; width: 400px; bottom: 0; right: 0;" autoplay></video>');
                }
            }

            $("#videoCallLocal").bind("playing", function () {
                if (adapter.browserDetails.browser == "firefox") {
                  // Firefox Stable has a bug: width and height are not immediately available after a playing
                  setTimeout(function () {
                    var width = $("#videoCallLocal").get(0).videoWidth;
                    var height = $("#videoCallLocal").get(0).videoHeight;
                  }, 2000);
                }
            });
            Janus.attachMediaStream($('#videoCallLocal').get(0), stream);
          },
          onremotestream: function (stream) {
            Janus.debug(" ::: Got a remote stream :::");
            if ($('#videoCallRemote').length === 0) {                
                if (page_Type == "android") {
                    var orientation = $("#orientation").val();
                    if (orientation == "potrait") {
                        $('#videoBoxRemote').empty(); 
                        $('#videoBoxRemote').append('<video controls id="videoCallRemote" class="video" style="z-index: 1; position: absolute; margin-bottom: 2%; margin-right:-4%; height: 120px; width: 150px; bottom: 0; right: 0;" autoplay></video>');   
                    }
                    else
                    {
                        $('#videoBoxRemote').empty(); 
                        $('#videoBoxRemote').append('<video controls id="videoCallRemote" class="video" style="width:190px; height:170px;" autoplay></video>');
                    }
                }
                else
                { 
                    $('#videoBoxRemote').append('<video id="videoCallRemote" style="z-index: 1; position: absolute; margin: 2%; height: 100px; width: 100px; bottom: 0; right: 0;" autoplay></video>');                    
                }
            }

            $("#videoCallRemote").bind("playing", function () {
              if (adapter.browserDetails.browser == "firefox") {
                // Firefox Stable has a bug: width and height are not immediately available after a playing
                setTimeout(function () {
                  var width = $("#videoCallRemote").get(0).videoWidth;
                  var height = $("#videoCallRemote").get(0).videoHeight;
                }, 2000);
              }
            });
            Janus.attachMediaStream($('#videoCallRemote').get(0), stream);     
          },
          
          ondataopen: function(data) {
            // Janus.log("The DataChannel is available!");
            // $('#videos').removeClass('hide').show();
            // $('#datasend').removeAttr('disabled');
          },
          ondata: function(data) {
            Janus.debug("We got data from the DataChannel! " + data);
            // $('#datarecv').val(data);
            if (data == "GoHangup" && usertype != "tutor") {
              // console.log("GoHangup ke student");
              doHangup();
            }
          },
          oncleanup: function () {
            // Janus.log(" ::: Got a cleanup notification :::");
            $('#videoCallLocal').remove();
            $('#videoCallRemote').remove();

            $("#kotakmenulist").modal("hide");
            $("#callingModal").modal("hide");
            $("#videoBoxRemote").css('display','none');
            $(".tutuprh").css('display','none');        
            $("#bukakotakraise").css('display','block');
            $(".tutuprhh").css('display','none');        
            $("#raisehandclick").css('display','block');
            // $("#tempatraisehand").css('display','none');
            $("#videoBoxLocal").empty();
            $("#tempathangup").css('display','none');            
            $("#tempathangupp").css('display','none');
            $("#tempatraise").css('display','block');
            $("#tempatraisee").css('display','block');

    //        var hangup = { "request": "hangup" };
    //        videoCall.send({ "message": hangup });
    //        videoCall.hangup();
            clearVideoCall();
          }
        });
    };

function checkBeforeCall (id, name) {
  vc_user2.id = "rm" + classid + "vc" + id;
  vc_user2.username = name;
  videoCall.send({ "message": { "request": "list" } });
};

function getJsonOffer () {
  var videoConfig = JSON.parse(window.localStorage.getItem('videoConfig'));
  var jsonRequest = {};
      // console.warn(currentDeviceId + " JSON OFFER");
      if (currentDeviceId) {
        jsonRequest = {
          "video": { "deviceId": currentDeviceId, "width": videoConfig.width, "height": videoConfig.height },
          // "video": {"deviceId": currentDeviceId, "width": "1024", "height": "702"},
          "data": false
        };
      } else {
        jsonRequest = {
          "video": videoConfig.defaultResolution,
          "data": false
        };
      };

      return jsonRequest;
    };

    function  doCall(x) {
      var jsonOffer = getJsonOffer();
      // Call this user
      videoCall.createOffer(
      {
          // By default, it's sendrecv for audio and video...
          // media: { data: true }, // ... let's negotiate data channels as well
          media: jsonOffer,
          success: function (jsep) {
            Janus.debug("Got SDP!");
            Janus.debug(jsep);
            // disableVideoRoomTutor();
            $("#callingModal").modal("show");
            $("#messageCalling").html("Memanggil " + vc_user2.username + " . . . ");
            var body = { "request": "call", "username": x };
            videoCall.send({ "message": body, "jsep": jsep });
          },
          error: function (error) {
            addNewLog("Error VideoCall : " + error, "ERROR");
          }
        });
    };

    function registerUserForCall() {
      var register = { "request": "register", "username": vc_user1.id, "token": token };
      videoCall.send({ "message": register });
    };

    function doAccept(jsep) {
      var jsonAccept = getJsonOffer();

      videoCall.createAnswer(
      {
        jsep: jsep,
          // No media provided: by default, it's sendrecv for audio and video
          // media: { data: true },  // Let's negotiate data channels as well
          media: jsonAccept,
          success: function (jsep) {
            Janus.debug("Got SDP!");
            Janus.debug(jsep);
            var body = { "request": "accept" };
            videoCall.send({ "message": body, "jsep": jsep });
          },
          error: function (error) {
            addNewLog("Error AcceptCall : " + JSON.stringify(error), "ERROR");
            doHangup();
          }
        });
    };

  function doHangup() {
      // if (usertype == "tutor") {
      //  sendData();
      // } else {
      // var hangup = { "request": "hangup" };
      // videoCall.send({ "message": hangup });
      // videoCall.hangup();
      // videoCall.detach();
      // clearVideoCall();
      // }
      if (usertype == "student") {
      	 matikanstreamsiswa();
      	 destroyRaiseHand();
      } 
      else
      {
      	 sendDataC();    	
      }
  };

  function clearVideoCall() {
    // videoCall = null;      
    $('#videoCallLocal').remove();
    $('#videoCallRemote').remove();

    $("#kotakmenulist").modal("hide");
    $("#callingModal").modal("hide");
    $("#videoBoxRemote").css('display','none');
    // $("#tempatraisehand").css('display','none');
    $("#videoBoxLocal").empty();
    
    vc_user1 = {};
    vc_user2 = {};
    // videoCall = null;
    // enableVideoRoomTutor();
    // attachVideoCall();

    if (usertype == "student") {
      destroyRaiseHand();
    }
  };

  function enableVideoRoomTutor(){
    sendrequest.unmuteAudio();
    sendrequest.unmuteVideo();
  };

  function sendDataC() {
    var data = "GoHangup";
    if(data === "") {
      bootbox.alert('Insert a message to send on the DataChannel to your peer');
      return;
    }
    sendrequest.data({
		text: data,
		error: function(reason) { 
			bootbox.alert(reason); 
		},
		success: function() { 
			console.log("Sukes Hangup from tutor"); 
		}
    });
  };
  // $("#chatInput").maxlength();
 //    $("textarea").bind("update.maxlength", function(event, element, lastLength, length, maxLength, left){
 //    });

});
