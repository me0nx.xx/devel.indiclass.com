var server = "wss://c2.classmiles.com:8989";
var janus = null;
var sendrequest = null;
var sendrequestjsep = null;
var started = null;
var opaqueId = "videoroomtest-"+Janus.randomString(12);
var mypvtid = null;
var listUserActive = null;
var listDevices = [];
var raisehandlist = {};

var idtutor = null;
var iduser  = null;
var displayname = null;
var tutorname = null;
var firstname = null;
var token = null;
var classid = null;
var usertype = null;
var classname = null;
var currentDeviceId = null;
var tutorin = null;
var st_tutor = 0;
var startTime = null;
var endTime = null;
var cektangan = 0;
var vartutor = 0;
var privateid = null;

//KHUSUS CHAT
var chatData = [];
var transactions = {};
var textroom = null;
var chatParticipants = {};
var chatReady = false;
var classidcp = "";
var idusercp = "";
var displaynamecp = "";
var archiveChats = [];

var d = new Date();
var month = d.getMonth()+1;
var day = d.getDate();

var feeds = [];
var bitrateTimer = [];
var iceServer = [];
var listVideo = null;
var iceServers = [];
// [
// 	{        
// 	"url": "turn:m1.classmiles.com",
// 	"credential": "tyh4g92chiwugahrj7hqwt8978sdiguf",
// 	"username": "c1397542364"
// 	},
// 	{
// 	"url": "stun:m1.classmiles.com",
// 	"credential": "tyh4g92chiwugahrj7hqwt8978sdiguf",
// 	"username": "c1397542364"
// 	}
// ];

var checkdate = ((''+day).length<2 ? '0' : '')+ day + '/' + ((''+month<2 ? '0' : '') +  month + '/'+ d.getFullYear());
var session = window.localStorage.getItem('session');
var link = "https://classmiles.com/";
var timer = 0;
var parameter = true;

var temp_break_state = -1;
var temp_play_pos = -1;
var break_state = 0; 
var play_pos = 0;
var page_Type = null; 
var wktu = 0;
var tempatsimpancek = null;
var st_break = 0;
var geo_location = [];
$(document).ready(function(){
	function getURLParameter(name) {
	  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
	}

	$.get("https://ipinfo.io/json", function (response) {
      // $("#ip").html("IP: " + response.ip);
      // $("#address").html("Location: " + response.city + ", " + response.region);
      // $("#details").html(JSON.stringify(response, null, 4));
       geo_location = {city:response.city,region:response.region,country:response.country};       
       // geo_location = json_encode(JSON_geo);       
      // geo_location = "city: "+response.city+" region: "+response.region+" country: "+response.country;
  	}, "jsonp");
	// $ipAddress = get_data("http://ipinfo.io/{$_SERVER['REMOTE_ADDR']}/country");

      // $("#ip").html("IP Server: " + $ipAddress);

	var lebarlayar = $(window).width();	
	console.warn('Test Token = ' + session);


	if (session == null) {
		session = getURLParameter("access_session");	
		console.warn('Test Token Null = ' + session);

	}
	
    if (lebarlayar < 900 ) {
    	page_Type = "android";
    	$(".tampilanweb").css('display','none');
        $("#header").css('display','none');    	
        $("#chat").css('display','none');
        $(".tampilanandro").css('display','block');
        var a = $(window).height();
	    var b = $(window).width(); //UNTUK HEIGHT
	    // var b = $(window).height() / 0.65;
	    var c = b * 0.5625;
	    var d = a - c;
	    var e = a / 0.5625; //UNTUK WIDTH LANDSCAPE
	    if (a > b * 0.5625) {                       
	        $("#player").css('height', c);
	        $("#player").css('width', b);

	        $(".papanwhiteboard").css('height','70vh');
	        $(".papanscreen").css('height','70vh');
	    }
	    else
	    {               
	        $("#player").css('width', e)
	        $("#player").css('height', a);
	    }
    }
    else
    {
    	var a = $(window).height();
	    var b = $(window).width(); //UNTUK HEIGHT
	    // var b = $(window).height() / 0.65;
	    var c = b * 0.5625;
	    var d = a - c;
	    var e = a / 0.5625; //UNTUK WIDTH LANDSCAPE
    	page_Type = "web";
        $(".tampilanandro").css('display','none');
        $(".tampilanweb").css('display','block');
        $("#chat").css('display','block');
        $("#header").css('display','block');      
    }

	setTimeout(function(){
		$("#loader").css('display', 'none');
		getpollingdata(parameter);
	},3000);
	setInterval(function(){
		$.ajax({
			url: 'https://classmiles.com/Rest/list_video',
			type: 'GET',
			data: {
				session: session
			},
			success: function(data)
			{	
				
				if (data['list'].length < 0) 
				{
					listVideo = data['list'][play_pos]['video_source'];
					// alert(listVideo);
				}
				else
				{
					listVideo = "../aset/video/aqua.mp4";
				}			
			}
		});		
		
		getpollingdata(parameter);
	},10000);
	setInterval(function(){
		if (play_pos == 3) 
		{
			st_tutor = 3;
			$("#tempatwaktu").css('display','block');
			wktu = 1;
			clearInterval(getpollingdata);
			console.warn("SUKSES CLEAR");
			generate();
		}
		if (cektangan == 1) {
			$("#tanganbiasa").css('display','none');
			$("#tanganmerah").css('display','block');
		}		
		else
		{
			$("#tanganmerah").css('display','none');
			$("#tanganbiasa").css('display','block');
		}
	},1000);
	

	var heightchat = $("#chat").height()-270;
	// alert(heightchat-100);
	// $("#chatBoxScroll").height(heightchat);

	// alert($("#tempatchat").height());
	// alert($("#whboard").height());
	$("#pindah2").css('display','block');
	$("#pindah3").css('display','block');
	$("#pindah2").click(function(){		
		if ($("#vdio").height() != 100 && $("#whboard").height() != 100) {			
			$("#pindah2").css('display','none');
			$("#pindah1").css('display','block');
			$("#pindah3").css('display','block');
		}
	});
	$("#myvideostudent").click(function(){		
		if ($("#vdio").height() != 100 && $("#whboard").height() != 100) {			
			$("#pindah2").css('display','none');
			$("#pindah1").css('display','block');
			$("#pindah3").css('display','block');
		}
	});
	$("#pindah3").click(function(){		
		if ($("#sshare").height() != 100 && $("#whboard").height() != 100) {			
			$("#pindah3").css('display','none');
			$("#pindah2").css('display','block');
			$("#pindah1").css('display','block');
		}
	});
	$("#pindah1").click(function(){		
		if ($("#vdio").height() != 100 && $("#whboard").height() != 100) {			
			$("#pindah1").css('display','none');
			$("#pindah2").css('display','block');
			$("#pindah3").css('display','block');
		}
	});

	// ============= TUTOR LOGIN ==============================
	function getpollingdata(firsttime)
	{
		$.ajax({
			url: 'https://classmiles.com/Rest/get_token',
			type: 'POST',
			data: {
				session: session
			},
			success: function(data)
			{							
				if (data['error']['code'] == '200' && data['response']['token'] != null) {
				// 	var a = JSON.stringify(geo_location); 
				// alert(a);			
					// =====SET DOM COMPONENT ======//
					usertype = data['response']['usertype'];					
					displayname = data['response']['first_name'];
					classname = data['response']['class_name'];
			        firstname = data['response']['first_name'];
			        tutorname = data['response']['nama_tutor'];

			        startTime = data['response']['start_time'];
			        endTime = data['response']['finish_time'];

			        // if (displayname != null) {
			        	$("#classname").text(classname);
				        $("#datenow").text(checkdate);
				        $("#nametutor").text(tutorname);
			        // }			        

			        if (data['response']['usertype'] == "tutor") {
	                	console.warn("TUTOR");
	                	$("#tempatstudent").css('display', 'none');
	                	$("#usertypestudent").css('display', 'none');	                	
	                	$("#tempattutor").css('display', 'block');
	                	$("#usertypetutor").css('display', 'block');	                	                	                	
	                }       
	                else if(data['response']['usertype'] == "student")
	                {
	                	console.warn("STUDENT");
	                	$("#tempattutor").css('display', 'none');
	                	$("#usertypetutor").css('display', 'none');
	                	$("#tempatstudent").css('display', 'block');
	                	$("#usertypestudent").css('display', 'block');	                	
	                }
	                // ===== !SET DOM COMPONENT ======//
	                if (archiveChats.length > 0 && data['response']['usertype'] == "tutor") {
						archiveChat(archiveChats);
						console.warn("Save Chats");
					}
					// alert(data['room_state']['break_state']);				
					if (data['room_state']['break_state'] == "" && data['room_state']['play_pos'] == "") 
					{
						console.warn("Tidak ada Break state");
					}

					break_state = data['room_state']['break_state'];
					console.warn(break_state);					
					play_pos = data['room_state']['play_pos'];
					console.warn(play_pos);	
					// $("#myvideo").attr('muted','true');
					token = data['response']['token'];
					privateid = 1;

	                classid = parseInt(data['response']['class_id']);	                
	                idtutor = parseInt(data['response']['id_tutor']);
	                iduser = parseInt(data['response']['id_user']);	
					// alert(vartutor);

					tempatsimpancek = $("#myvideostudent").attr('src');
					if (break_state == 1 ) {
						$("#myvideo").attr('muted','false');
						console.warn(temp_break_state);
						console.warn("IKLAN CUY");
												
						if (temp_break_state != break_state) {
							temp_break_state = break_state;

							$.ajax({
								url: 'https://classmiles.com/Rest/list_video',
								type: 'GET',
								data: {
									session: session
								},
								success: function(data)
								{													
									if (data['list'].length < 0) 
									{										
										if (vartutor == 0) 
										{
											listVideo = data['list'][play_pos]['video_source'];										
											console.warn("AAAAAAAAAAAAAAAAAAAA");
											if (usertype == "student") {
												if (page_Type == "web") 
												{																
													$("#myvideostudent").attr('src',listVideo);
													$("#myvideostudent").attr('loop','true');
												}
												else
												{
													$("#player").attr('src',listVideo);
													$("#player").attr('loop','true');
												}
											}
											if (usertype == "tutor") {																
												$("#myvideo").attr('src',listVideo);
												$("#myvideo").attr('loop','true');
											}
										}
									}
									else
									{
										console.warn("TIDAK ADA VIDEO SOURCE");
										if (vartutor == 0) 
										{
											var listVideoo = "../aset/video/aqua.mp4";
											if (usertype == "student") {
												if (page_Type == "web") 
												{	
													// $("#myvideostudent").attr('src','');												
													$("#myvideostudent").attr('src',listVideoo);
													$("#myvideostudent").attr('loop','true');
												}
												else
												{
													$("#player").attr('src',listVideoo);
													$("#player").attr('loop','true');
												}
											}
											else if(usertype == "tutor"){
												$("#myvideo").attr('src',listVideoo);
												$("#myvideo").attr('loop','true');
											}
										}
									}
								}
							});
						}
						return null;
																		
						// parameter = true;
						
					}
					else
					{					
						if (temp_break_state != break_state) {
							console.warn(break_state);
							console.warn("BBBBBBBBBBBBBBBB");
							temp_break_state = break_state;
							console.warn("UDAH MASUK LAGI");
							if (janus == null) {	                		                	

								if (privateid == 2) {
									$("#privatetutorlocal").css('display','block');								
									checkBeforeCall(iduser, displayname);
								}
								else
								{
									tunner(parameter);
				                	$("#screensharestudent").attr("src", "https://classmiles.com/screen_share/#access_session="+session);
				                	$("#screensharestudenttt").attr("src", "https://classmiles.com/screen_share/#access_session="+session);
				                	$("#wbcard").attr("src", "https://c2.classmiles.com/wb/"+classid+"/session/"+session);
				                	$("#wbcardd").attr("src", "https://c2.classmiles.com/wb/"+classid+"/session/"+session);
				                	$("#wbcarddd").attr("src", "https://c2.classmiles.com/wb/"+classid+"/session/"+session);
				                	$("#screensharestudent").css("width", "100%");
				                	$("#screensharestudent").css("height", "100%");
				                	$("#screensharestudenttt").css("width", "100%");
				                	$("#screensharestudenttt").css("height", "100%");	                	
				                	parameter = false;
				                	// $("#nametutor").text(displayname);	
				                	console.warn("SUDAH MASUK JANGAN MASUK LAGI");
			                	}	
			                }
			                
						}
					}					
					// else
					// {
					// 	tempbreakstate = null;
					// }

						// alert("sudah di bawah");
					

			         
			        // alert(temp_break_state); 
		        	console.warn("ADA DATA");		                
	                	                		                

	                 // ===== SET RAISE HAND DOME ======//
	                raisehandlist = [];
	                if (data['student_rhand']) {
		                // console.warn("INI DATA RAISE HAND "+data['student_rhand']);
		                raisehandlist = data['student_rhand'];
		                console.warn(raisehandlist);
	            	}
	            	
	            	if (raisehandlist.length > 0) {
	            		$("#totalraisehand").css("display",'block');
	            		$("#totalraisehand").text(raisehandlist.length);
	            	}
	            	else
	            	{
	            		$("#totalraisehand").text("");
	            		$("#totalraisehand").css("display",'none');
	            	}
	            	// ===== !SET RAISE HAND DOME ======//				
	                				
		        } 
		        else if (data['error']['code'] == '504') {
		        	console.warn("MASUK ELSE IF 504");
					// setInterval(function(){
					// 	getpollingdata(parameter);
					// },2000);	
					// timer;
		        }
		        else if (data['error']['code'] == '404') {
		        	console.warn("MASUK ELSE IF 404");
					// setInterval(function(){
					// 	getpollingdata(parameter);
					// },2000);	
					destroyToken();
		        	// clearInterval(timer);
		        }   
		        else
		        {
		        	console.warn("SAATNYA DESTROY TOKEN");
		        	destroyToken();
		        	// clearInterval(timer);
		        }
		         
			}
		});
	}	

	$("#clearinterval").click(function(){
		clearInterval(getpollingdata);
		$("#tempatwaktu").css('display','block');
		console.warn("SUKSES CLEAR");
		generate();
	});

    var seconds = 60; // Berapa detik waktu mundurnya
	function generate() { // Nama fungsi countdownnya
		if(getpollingdata) {
	        clearInterval(getpollingdata);
	    }
		console.warn("GENERATE");	
				
		var id;
		id = setInterval(function () {
		if (seconds < 1){
		clearInterval(id);
		wktu = 0;
		// Perintah yang akan dijalankan
		// apabila waktu sudah habis
		$("#tempatwaktu").css('display','none');
		destroyToken();

		}else {			
			document.getElementById('countdown').innerHTML = --seconds;
		}
		}, 1000);			
		
	}
	//]]>

	function publishOwnFeed(deviceId)
	{
      	var videoConfig = JSON.parse(window.localStorage.getItem('videoConfig'));
        // if (currentDeviceId != null) {
        // 	deviceId = currentDeviceId;
        // }
        var jsonRequest = {};
        console.warn("== DEVICES ==" + deviceId);
        if (deviceId) {
          jsonRequest = {
            "audioRecv": false,
            "videoRecv": false,
            "audioSend": true,
            "videoSend": true,
            "video":{"deviceId": deviceId, "width": "1024", "height": "702"},
            "data": false
          };
          console.warn("== CHECK ==" + deviceId);
        } else {
          jsonRequest = {
            "audioRecv": false,
            "videoRecv": false,
            "audioSend": true,
            "videoSend": true,
            "video": videoConfig.defaultResolution,
            "data": false
          };
        }
        sendrequest.createOffer
        ({
        	media: jsonRequest,
        	trickle: true,
        	success: function(jsep)
        	{
        		sendPublish(jsep);
        	},
        	error: function(error)
        	{
        		console.log("error bego", error);
        	}
        });

	}

	function sendPublish(jsep)
	{
		var jsonRequest = {};
		jsonRequest.request = "configure";
		jsonRequest.audio = true;
		jsonRequest.video = true;
		jsonRequest.bitrate = 256000;
		jsonRequest.audiocodec = "opus";
		jsonRequest.videocodec = "vp9";
		sendrequest.send({ "message": jsonRequest, "jsep": jsep});		
	}

	function tunner()
	{
		$.ajax({
			url: 'https://classmiles.com/Rest/turner_stuner',
			type: 'GET',
			data: {
				session: session
			},
			success: function(data)
			{				       
				var dataServer = data.response;
		        var responseIceServer = dataServer.list_stun.concat(dataServer.list_turn);
		        iceServers = [];

		        var config = {};
		        config.audioCodec = dataServer.audiocodec;
		        config.videoCodec = dataServer.videocodec;
		        config.maxBitRate = dataServer.maxbitrate;
		        config.resolution = dataServer.resolution;

		        window.localStorage.setItem("videoConfig", config);

		        if (config.videoCodec === 'lowres') {
		          // Small resolution, 4:3
		          config.height = 240;
		          config.width = 320;
		        } else if (config.videoCodec === 'lowres-16:9') {
		          // Small resolution, 16:9
		          config.height = 180;
		          config.width = 320;
		        } else if (config.videoCodec === 'hires' || config.videoCodec === 'hires-16:9') {
		          // High resolution is only 16:9
		          config.height = 720;
		          config.width = 1280;
		          if (navigator.mozGetUserMedia) {
		            var firefoxVer = parseInt(window.navigator.userAgent.match(/Firefox\/(.*)/)[1], 10);
		            if (firefoxVer < 38) {
		              config.height = 480;
		              config.width = 640;
		            }
		          }
		        } else if (config.videoCodec === 'stdres') {
		          // Normal resolution, 4:3
		          config.height = 480;
		          config.width = 640;
		        } else if (config.videoCodec === 'stdres-16:9') {
		          // Normal resolution, 16:9
		          config.height = 360;
		          config.width = 640;
		        } else {
		          config.height = 480;
		          config.width = 640;
		        }

		        window.localStorage.setItem('videoConfig', JSON.stringify(config));

		        for (var i = 0; i < responseIceServer.length; i++) {
		          var server = {};
		          server.urls = responseIceServer[i].url;
		          server.credential = responseIceServer[i].credential;
		          server.username = responseIceServer[i].username;
		          iceServers.push(server);
		        }
		        start(iceServers);
			}
		});
	}

	function createroom()
	{
		console.warn("CREATEROOM");
		var jsonRequest = {
          "request": "create",
          "room": classid,
          "ptype": "publisher",
          "description": "",
          "publishers": 1, //hanya 1 orang sebagai publisher
          "bitrate": 256000,
          "audiocodec": "opus",
          "videocodec": "vp9",
          "record": false,
          // "rec_dir": "/home/videos/" + userData.roomNumber,
          "is_private": true
        };	
        sendrequest.send(
        { 
        	"message": jsonRequest,
        	success: function(resp){
			console.warn("Berhasil Create ",  resp);
        		joinroomNumber();        	
		}	
        });
	}

	function joinroomNumber()
	{
		var jsonRequest = {
          "request": "join",
          "room": classid,
          "ptype": "publisher",
          "display": displayname,
          "id": iduser          
        };
        sendrequest.send({
        	"message": jsonRequest
        });
	}	
	// ============= END TUTOR LOGIN ==============================





	// ============= STUDENT LOGIN ==============================
	function attachListener(id, display)
	{
		console.warn("ATTACHLISTENER");
		janus.attach
		({
			plugin: "janus.plugin.videoroom",
			opaqueId: opaqueId,
			success: function(pluginHandle){
				sendrequest = pluginHandle;
				var listen = { "request": "join", "room": classid, "ptype": "listener", "feed": id, "private_id": mypvtid };
				sendrequest.send({ "message": listen });
				$("#classname").text(classname);
				$("#datenow").text(checkdate);						
				if (usertype == "tutor") {
					console.warn("TUTOR");
                	$("#usertypestudent").css('display', 'none');	
                	$("#tempatstudent").css('display', 'none');
                	$("#usertypetutor").css('display', 'block');	                	
                	$("#tempattutor").css('display', 'block');
                }       
                else if(usertype == "student")
                {
                	console.warn("STUDENT");
                	$("#usertypetutor").css('display', 'none');
                	$("#tempattutor").css('display', 'none');
                	$("#usertypestudent").css('display', 'block');	 
                	$("#tempatstudent").css('display', 'block');                	
                }
			},
			error: function (error) {
	          console.warn("Error Listener : " + error, "ERROR");
	        },
	        onmessage: function(msg, jsep){
	        	var event = msg["videoroom"];
				Janus.debug("Event: " + event);
				if(event != undefined && event != null) {
					if(event === "attached") {				
						// if(sendrequest.spinner === undefined || sendrequest.spinner === null) {
						// 	var target = document.getElementById('myvideostudent');
						// 	sendrequest.spinner = new Spinner({top:100}).spin(target);
						// } else {
						// 	sendrequest.spinner.spin();
						// }

						Janus.log("Successfully attached to feed  (" + sendrequest.rfdisplay + ") in room " + msg["room"]);
						$('#publisher').removeClass('hide').html(sendrequest.rfdisplay).show();
					} else if(msg["error"] !== undefined && msg["error"] !== null) {
						bootbox.alert(msg["error"]);
					} else {	
						// What has just happened?
					}
				}
				if(jsep !== undefined && jsep !== null) {
					answerRequest(jsep, sendrequest);
				}

	        },
	        webrtcState: function(on) {
				Janus.log("Janus says this WebRTC PeerConnection (feed #) is " + (on ? "up" : "down") + " now");
				$("#myvideostudent").parent().parent().unblock();
			},
			onlocalstream: function(stream) {
				// The subscriber stream is recvonly, we don't expect anything here
			},
	        onremotestream: function(stream){
	        	mystream = stream;
				Janus.debug(JSON.stringify(stream));				
				$("#mute").css('display','none');
				$("#unpublish").css('display','none');
				if (page_Type == "web") 
				{
					$('#myvideostudent').attr('src', URL.createObjectURL(stream));
				}
				else
				{
					$('#player').attr('src', URL.createObjectURL(stream));
				}
				// $("#myvideostudent").parent().parent().block({
				// 	message: '<b>Publishing...</b>',
				// 	css: {
				// 		border: 'none',
				// 		backgroundColor: 'transparent',
				// 		color: 'white'
				// 	}
				// });
				var videoTracks = stream.getVideoTracks();
				if(videoTracks === null || videoTracks === undefined || videoTracks.length === 0) {
					// No webcam
					$('#myvideostudent').hide();
					$('#videolocal').append(
						'<div class="no-video-container">' +
							'<i class="fa fa-video-camera fa-5 no-video-icon" style="height: 100%;"></i>' +
							'<span class="no-video-text" style="font-size: 16px;">No webcam available</span>' +
						'</div>');
				}
	        },
	        oncleanup: function() {
				Janus.log(" ::: Got a cleanup notification (remote feed " + id + ") :::");
				// if(sendrequest.spinner !== undefined && sendrequest.spinner !== null)
				// 	sendrequest.spinner.stop();
				// sendrequest.spinner = null;
				$('#waitingvideo'+sendrequest.rfindex).remove();
				$('#curbitrate'+sendrequest.rfindex).remove();
				$('#curres'+sendrequest.rfindex).remove();
				if(bitrateTimer[sendrequest.rfindex] !== null && bitrateTimer[sendrequest.rfindex] !== null) 
					clearInterval(bitrateTimer[sendrequest.rfindex]);
				bitrateTimer[sendrequest.rfindex] = null;
			}

		})
	}

	function attachThisuserJoin(id, publishers)
	{
		console.warn(idtutor);
		console.warn(iduser);
		if (idtutor == id) 
		{
			publishOwnFeed(currentDeviceId);
		}
		eventPublisherJoin(publishers);
	}

	function eventPublisherJoin(publishers)
	{
		console.warn("EVENTPUBLISHERJOIN");
		if (publishers) 
		{
			for (var f in publishers) {
	          	var id = publishers[f]["id"];
	          	var display = publishers[f]["display"];
	          	attachListener(id, display);
	        }
		}
	}
	// ============= END STUDENT LOGIN ==============================






	// ============= JANUS START ==============================
	function start(inidingin){
		Janus.init({debug: "all", callback: function(){
			// $("#start").click(function(){

				if(started)
					return;
				started = true;
				$(this).attr('disabled', true).unbind('click');
				// Make sure the browser supports WebRTC
				if(!Janus.isWebrtcSupported()) {
					bootbox.alert("No WebRTC support... ");
					return;
				}

				janus = new Janus(
					{
						server: server,
						iceServer: inidingin,
						token: token,
						success: function()
						{
							attachPublisher();
							attachChatPlugin(classid, iduser, displayname);
							attachVideoCall();
						},
						error: function(error) {
							Janus.error(error);
							bootbox.alert(error, function() {
								window.location.reload();
							});
						}
					}
				)
		}});
	}	

	function attachPublisher() {
		janus.attach(
		{								
			plugin: "janus.plugin.videoroom",
			opaqueId: opaqueId,
			success: function(pluginHandle)
			{
				sendrequest = pluginHandle;
				sendrequestjsep = pluginHandle;
				$("#classname").text(classname);
				
				console.warn(displayname);			
			    getListParticipants();  
			    getDevice();			    
				var timer2 = setInterval(function() {
			        getListParticipants();                
			    }, 5000);								    
				createroom();
			},
			error: function(error)
			{
				console.log("error bro");
			},
			consentDialog : function(on)
			{
				console.log("consentDialog");						
			},
			mediaState : function(medium, on)
			{
				console.log("mediaState");
			},
			webrtcState: function(medium, on)
			{
				$("#videolocal").parent().parent().unblock();
			},
			onmessage: function(msg, jsep)
			{
				console.log("onmessage");
				console.debug(JSON.stringify(msg));

				var event = msg['videoroom'];
				if (event == "joined") 
				{
					attachThisuserJoin(msg['id'],msg['publishers']);
				}
				else if(event == "destroyed")
				{
					Janus.warn("The room has been destroyed!");
					bootbox.alert("The room has been destroyed", function() {
						window.location.reload('https://classmiles.com/');
					});
				}
				else if(event == "event")
				{
					if (msg["publishers"] !== undefined && msg["publishers"] !== null) {
						var list = msg["publishers"];
						Janus.debug("Got a list of available publishers/feeds:");
						Janus.debug(list);
						for(var f in list) {
							var id = list[f]["id"];
							var display = list[f]["display"];
							Janus.debug("  >> [" + id + "] " + display);
							attachListener(id, display)
						}
					}
					else if(msg["leaving"] !== undefined && msg["leaving"] !== null) 
					{
						// One of the publishers has gone away?
						var leaving = msg["leaving"];
						Janus.log("Publisher left: " + leaving);
						// var remoteFeed = null;
						for(var i=1; i<6; i++) {
							if(feeds[i] != null && feeds[i] != undefined && feeds[i].rfid == leaving) {
								sendrequestjsep = feeds[i];
								break;
							}
						}

						// alert(leaving);
						// alert(idtutor);
						if (leaving == idtutor) {
							if(sendrequestjsep != null) {
								console.warn("HILANG");
								Janus.debug("Feed  (" + sendrequestjsep.rfdisplay + ") has left the room, detaching");
								$('#remote').empty().hide();
								$('#videoremote').empty();
								feeds[sendrequestjsep.rfindex] = null;
								// alert(usertype);
								// alert(usertype);			
								$.ajax({
									url: 'https://classmiles.com/Rest/unbreak',
									type: 'GET',
									data: {
										session: session
									},
									success: function(data)
									{							
										console.warn("BERHASIL UNBREAK");

										// var aa = $("#tempatx").attr("title").remove();
										// a.text("Play");
										st_tutor = 1;
										publishOwnFeed(true);

									}
								});					
								if (usertype == "student") {
									if (listVideo != null) 
									{
										// videonih = data['list'][play_pos]['video_source'];
										// alert(dataa['list']['video_source']);
										// alert(page_Type + " publish leave");
										if (page_Type == "web") {
											$("#myvideostudent").attr('src',listVideo);
											// $("#myvideostudent").attr('muted','true');
											$("#myvideostudent").attr('loop','true');
										} else {
											$("#player").attr('src',listVideo);
											// $("#player").attr('muted','true');
											$("#player").attr('loop','true');
										}									
									}
									else
									{
										console.warn("TUTOR LEAVING");													
										$("#myvideostudent").attr('src','../aset/video/juki.mp4');
									}
								}
								// $('#myvideo').attr('src', link+'/aset/video/line.mp4');
								// $('#myvideo').attr('muted');
								// if (tutorin == null) {
								// 	if (st_tutor == 0) 
								// 	{
								// 		st_tutor = 1;
										// alert('asdfklad');
										// $.ajax({
										// 	url: 'https://classmiles.com/Rest/list_video',
										// 	type: 'GET',
										// 	data: {
										// 		session: session
										// 	},
										// 	success: function(data)
										// 	{	
												// console.warn("TIDAK ADA TUTOR");						
												// if (data['list'].length > 0) 
												// {
													// videonih = data['list'][play_pos]['video_source'];
													// alert(dataa['list']['video_source']);
													// alert(page_Type + " leave");
													// if (page_Type == "web") {
													// 	$("#myvideostudent").attr('src',listVideo);
													// 	// $("#myvideostudent").attr('muted','true');
													// 	$("#myvideostudent").attr('loop','true');
													// } else {
													// 	$("#player").attr('src',listVideo);
													// 	// $("#player").attr('muted','true');
													// 	$("#player").attr('loop','true');

													// }
												// }
												// else
												// {
												// 	console.warn("TUTOR LEAVING");
												// 	$("#myvideostudent").attr('src','../aset/video/juki.mp4');													
												// }	
												// videonih = data['list'][play_pos-1]['video_source'];											
												// // alert(data['list'][0]['video_source']);
												// // alert(dataa['list']['video_source']);							
												// $("#myvideostudent").attr('src',videonih);
												// $("#myvideostudent").attr('muted','true');
												// $("#myvideostudent").attr('loop','true');									
											// }
										// });
								// 	}
									return null;
								// }
								// else
								// {
								// 	st_tutor = 0;
								// }
								$("#myvideo").css('width','90%');
								// $("#myvideo").css('height','300px');
								// sendrequest.detach();
							}
						}
					}
					else if(msg["unpublished"] !== undefined && msg["unpublished"] !== null) {
						// One of the publishers has unpublished?
						var unpublished = msg["unpublished"];
						Janus.log("Publisher left: " + unpublished);
						// if (usertype == "tutor") {
						// 	$('#myvideo').attr('src', '../../aset/video/bigbunny.mp4');							
						// }
						// if(usertype == "student"){
						// 	$('#myvideostudent').attr('src', '../../aset/video/juki.mp4');							
						// }
						// if (tutorin == null) {
						// 	if (st_tutor == 0) 
						// 	{
						// 		st_tutor = 1;
						// 		alert('asdfklad');
						// 		$.ajax({
						// 			url: 'https://classmiles.com/Rest/list_video',
						// 			type: 'GET',
						// 			data: {
						// 				session: session
						// 			},
						// 			success: function(data)
						// 			{	
										// console.warn("TIDAK ADA TUTOR");	

										// if (usertype == "tutor") 
										// {				
											// if (listVideo != null) 
											// {
											// 	// videonih = data['list'][play_pos]['video_source'];
											// 	// alert(dataa['list']['video_source']);
											// 	// alert(page_Type + " publish leave");
											// 	if (page_Type == "web") {
											// 		$("#myvideostudent").attr('src',listVideo);
											// 		// $("#myvideostudent").attr('muted','true');
											// 		$("#myvideostudent").attr('loop','true');
											// 	} else {
											// 		$("#player").attr('src',listVideo);
											// 		// $("#player").attr('muted','true');
											// 		$("#player").attr('loop','true');
											// 	}									
											// }
											// else
											// {
											// 	console.warn("TUTOR LEAVING");													
											// 	$("#myvideostudent").attr('src','../aset/video/juki.mp4');
											// }
										// }	
										
										// videonih = data['list'][play_pos]['video_source'];
										// 	$("#myvideostudent").attr('src',videonih);
										// 	$("#myvideostudent").attr('muted','true');
										// 	$("#myvideostudent").attr('loop','true');																				
							// 		}
							// 	});
							// }
							return null;						
						if(unpublished === 'ok') {
							// That's us							
							sendrequestjsep.hangup();
							return;
						}
						var sendrequest = null;
						for(var i=1; i<6; i++) {
							if(feeds[i] != null && feeds[i] != undefined && feeds[i].rfid == unpublished) {
								sendrequest = feeds[i];
								break;
							}
						}
						if (leaving == idtutor) {
						if(sendrequest != null) {
								Janus.debug("Feed (" + sendrequest.rfdisplay + ") has left the room, detaching");
								$('#remote').empty();
								$('#videoremote').empty();
								feeds[sendrequest.rfindex] = null;
								// sendrequest.detach();															
							}
						}
					} else if(msg["error"] !== undefined && msg["error"] !== null) {
						// bootbox.alert(msg["error"]);
						notify('top','center','fa fa-check','info','animated fadeInDown','animated fadeOut',"You probably have been entering this classroom from other devices");
						// bootbox.alert('You probably have been entering this classroom from other devices');
					}
				}									

				if(jsep !== undefined && jsep !== null) {
					Janus.debug("Handling SDP as well...");
					Janus.debug(jsep);
					sendrequestjsep.handleRemoteJsep({jsep: jsep});
				}

			},
			onlocalstream: function(stream)
			{
				mystream = stream;
				Janus.debug(JSON.stringify(stream));
				$('#publisher').removeClass('hide').html(displayname).show();
				$('#myvideo').attr('src', URL.createObjectURL(stream));
				$("#videolocal").parent().parent().block({
					message: '<b>Publishing...</b>',
					css: {
						border: 'none',
						backgroundColor: 'transparent',
						color: 'white'
					}
				});
				var videoTracks = stream.getVideoTracks();
				if(videoTracks === null || videoTracks === undefined || videoTracks.length === 0) {
				}
			},
			oncleanup: function() {
				Janus.log(" ::: Got a cleanup notification: we are unpublished now :::");
				mystream = null;
			},
			error: function(error)
			{
				Janus.error(error);
				bootbox.alert(error, function() {
					window.location.reload();
				});
			},
			destroyed: function()
			{
				window.location.reload();
			}
		})
	}

	function getListParticipants()
	{
		var display="";
		var jsonRequest = {
          "request": "listparticipants",
          "room": classid,
          "description": "",
          "is_private": true
        };
		sendrequestjsep.send({
			"message": jsonRequest,
			success: function (resp) {
			  	listUserActive = resp.participants;
			  	var a = JSON.stringify(listUserActive);
			  	console.warn("INI LIST USERACTIOVE"+a);
			  	$("#totalstudent").text(listUserActive.length+" people attending now");
			  	$("#totalstudentt").text(listUserActive.length+" people attending now");
				
				var as=$(listUserActive).filter(function (i,n){return n.publisher==='true'});
				// console.warn("AS = " + as);
				if (as.length > 0) {
					console.warn("Ada Tutor Mang");
					vartutor = 1;
					st_tutor = 1;
					st_break = 1;
					// $("#myvideostudent").attr("src",'');
				} else {
					console.warn("Tidak ada tutor Mang");
					vartutor = 0;
					st_break = 0;					
					if (st_tutor == 0) 
					{						
						if (listVideo == null) {
							if (page_Type == "web") {
								if (usertype == "student") 
								{
									console.warn("MASUK WEB VIDEO IKLAN TIDAK KOSONG");
									$("#myvideostudent").attr("src",'../aset/video/cocacola1.mp4');									
									$("#myvideostudent").attr('loop','true');
									st_tutor = 1;
								}
							} else {
								console.warn("MASUK ANDROID VIDEO IKLAN TIDAK KOSONG");
								$("#player").attr('src',listVideo);
								// $("#player").attr('muted','true');
								$("#player").attr('loop','true');
							}
						}
						else
						{
							console.warn("MASUK LIST VIDEO IKLAN TIDAK KOSONG");
							if (usertype == "student") 
							{
								$("#myvideostudent").attr("src",listVideo);
								st_tutor = 1;	
							}
						}
					}
					return null;

				}
				return null;

				// for (var i=0;i<as.length;i++)
  		// 		{
				// 	alert(as[i].display +"         "+as[i].publisher)
				// }

			  	// for(var f in listUserActive) {	
			  	// for (var f = 0; f < listUserActive.length; f++) {
			  	// 	console.log("data ke " + f);
			  	// 	console.warn("DATA : " + listUserActive[f]['publisher']);
			  	// 	if (listUserActive[f]['publisher'] == "true") 
			  	// 	{			  			
			  	// 		console.warn("UDAH ADA TUTOR");
			  	// 	}
			  	// 	else
			  	// 	{
			  	// 		console.warn("gk ada tutor");
			  	// 	}
			  		// if (listUserActive[f]['publisher']) {
			  		// 	console.log("Tutor sudah hadir");
			  		// 	return;
			  		// }			else {
			  		// 	console.log("Gk ada tutor");
			  		// }	
					// display += listUserActive[f]["display"]+"\n";
					// console.warn('INI CEK TRUE ATAU FALSE '+listUserActive[f]['publisher']);

					// 	console.warn("MASUK MASUK");	
					// 	console.warn("ADA TUTOR");			
					// 	tutorin = listUserActive[f]['display'];											
					// 	// $("#nametutor").text(listUserActive[f]['display']);
					// 	// alert(listUserActive[f]['publisher']);
					// 	if (usertype == "tutor") /
					// 	{
							
					// 	}
					// 	else
					// 	{
							
					// 	}

					// 	return null;
				// }
			  	
					$("#listuser").val(display);					
			}
		});
	}

	function getDevice() {
		console.warn("get Devices");
		Janus.listDevices(function (devices) {
	        var deviceVideo = [];
	        var deviceAudio = [];

	        for (var i = 0; i < devices.length; i++) {
	          if (devices[i].kind == "videoinput") {
	            deviceVideo.push(devices[i]);
	          } else if (devices[i].kind == "audioinput") {
	            deviceAudio.push(devices[i]);
	          }
	        }
	        $("#buttonlist").empty();
	        listDevices.video = deviceVideo;
	        listDevices.audio = deviceAudio;

	    });
	}	
	// ============= END JANUS START ==============================






	// ============= FUNCTION FUNTION ==============================
    function answerRequest(jsep, remoteFeed) {
      var jsonRequest = {
            "audioRecv": true,
            "videoRecv": true,
            "audioSend": false,
            "videoSend": false,
            "video": {"deviceId": currentDeviceId, "width": "1024", "height": "702"},
            "data": false
        };
      // Answer
      sendrequest.createAnswer({
        jsep: jsep,
        media: jsonRequest,
        trickle: true,
        success: function (jsep) {
          attachRequest(jsep, sendrequest);
        },
        error: function (error) {
          // $scope.addNewLog("Error Answer : " + error, "ERROR");
        }
      });
    }

    function attachRequest(jsep, sendrequest) {
      var jsonRequest = jsonRequest = {
          "request": "start",
          "room": classid
        };
      sendrequest.send({ "message": jsonRequest, "jsep": jsep });
    }

	function eventAttached(msg, sendrequest)
	{
		var event = msg['videoroom'];
		if (event) {
			if (event == "attached") {
				sendrequest.rf_id = msg['id'];
				sendrequest.rf_display = msg['display'];
				sendrequest.rf_videobox = "small";
				sendrequest.rf_type = "remote";
				if (sendrequest.rf_id === idtutor) {
					sendrequest.rf_userType = "tutor";
				}
				else{
					sendrequest.rf_userType = "student";
				}

				feeds.push(sendrequest);
			}
			else if(msg['error'] !== undefined && msg['error'] !== null){
				console.warn(msg['error']);
			}
		}
	}
	// ============= END FUNCTION FUNTION ==============================






	// ============= JANUS CHATROOM ==============================
	function getDataChat(){
		console.warn("Start "+startTime);
		console.warn("End "+endTime);
		console.warn("Class "+classid);
        $.ajax({
			url: 'https://classmiles.com/Rest/get_chat',
			type: 'POST',
			data: {
				dateawal : startTime,
				dateakhir : endTime,
				class_id : classid
			},
			success: function(response)
			{	
				console.warn(response);
				if (response.data.length > 0) {
					for (var i = 0; i < response.data.length;i++) {
							
			      //   	{        	
				     //    	$("#chatBoxScroll").append('<div class="listview" style="width: 100%; word-wrap: break-word; background-color:rgba(187, 222, 251, 0.3); margin-bottom:2px;" id="chatBoxScroll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title" id="panel8"><b>'+ chatParticipants[response.data[i]["sender_name"]] +' - Tutor </b></div><small style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ response.data[i]["text"] +'</small></div></div></a></div>');		        
					    //     $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;
			      //   	} else {
				     //    	$("#chatBoxScroll").append('<div class="listview" style="width: 100%; word-wrap: break-word;" id="chatBoxScroll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title" id="panel8"><b>'+ chatParticipants[response.data[i]["sender_name"]] +'</b></div><small style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ response.data[i]["text"] +'</small></div></div></a></div>');		        
					    //     $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;
				    	// }

				    	if (page_Type == "web") {
							if(response.data[i]["sender_name"].slice(6,response.data[i]["sender_name"].length) == idtutor)
				        	{        	
					        	$("#chatBoxScroll").append('<div class="listview" style="width: 100%; word-wrap: break-word; background-color:rgba(236, 236, 236, 0.3); margin-bottom:2px;" id="chatBoxScroll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title" id="panel8"><b>'+ response.data[i]["sender_name"] +' - Tutor </b></div><small style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ response.data[i]["text"] +'</small></div></div></a></div>');		        
						        $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;
						        $("#chatBoxScrolll").append('<div class="listview" style="width: 100%; word-wrap: break-word; background-color:rgba(236, 236, 236, 0.3); margin-bottom:2px;" id="chatBoxScrolll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title" id="panel8"><b>'+ response.data[i]["sender_name"] +' - Tutor </b></div><small style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ response.data[i]["text"] +'</small></div></div></a></div>');		        
						        $('#chatBoxScrolll').get(0).scrollTop = $('#chatBoxScrolll').get(0).scrollHeight;
				        	} else {
					        	$("#chatBoxScroll").append('<div class="listview" style="width: 100%; word-wrap: break-word; background-color:rgba(236, 236, 236, 0.3); margin-bottom:2px;" id="chatBoxScroll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title" id="panel8"><b>'+ response.data[i]["sender_name"] +'</b></div><small style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ response.data[i]["text"] +'</small></div></div></a></div>');		        
						        $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;
						        $("#chatBoxScrolll").append('<div class="listview" style="width: 100%; word-wrap: break-word; background-color:rgba(236, 236, 236, 0.3); margin-bottom:2px;" id="chatBoxScrolll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title" id="panel8"><b>'+ response.data[i]["sender_name"] +'</b></div><small style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ response.data[i]["text"] +'</small></div></div></a></div>');		        
						        $('#chatBoxScrolll').get(0).scrollTop = $('#chatBoxScrolll').get(0).scrollHeight;
					    	}
					    } else {
							if(response.data[i]["sender_name"].slice(6,response.data[i]["sender_name"].length) == idtutor)
				        	{        	
					        	$("#chatAndroid").append('<div class="chating" background-color:rgba(187, 222, 251, 0.3)><label>'+ response.data[i]["sender_name"] +'<br><small style="word-wrap: break-word;">'+ response.data[i]["text"] +'</small></label></div>');		        
						        $('#chatAndroid').get(0).scrollTop = $('#chatAndroid').get(0).scrollHeight;
				        	} else {
					        	$("#chatAndroid").append('<div class="chating"><label>'+ response.data[i]["sender_name"] +'<br><small style="word-wrap: break-word;">'+ response.data[i]["text"] +'</small></label></div>');		        
						        $('#chatAndroid').get(0).scrollTop = $('#chatAndroid').get(0).scrollHeight;
					    	}
					    }
					}
		    	}

			}
		});
	}

	function attachChatPlugin(Room, IdUser, Display) {
 		classidcp = Room;
 		idusercp = IdUser;
 		displaynamecp = Display;
      // Attach to echo test plugin
      console.warn("INIT CHAT "+JanusConfigService.pluginChatRoom);
      janus.attach(
        {
          plugin: JanusConfigService.pluginChatRoom,
          success: function (pluginHandle) {
            // $('#details').remove();
            textroom = pluginHandle;
            Janus.log("Plugin attached! (" + textroom.getPlugin() + ", id=" + textroom.getId() + ")");
            // Setup the DataChannel
            var body = { "request": "setup", "room": classid };
            Janus.debug("Sending message (" + JSON.stringify(body) + ")");
            textroom.send({ "message": body });
          },
          error: function (error) {
            addNewLog(error, "ERROR");
          },
          webrtcState: function (on) {
            Janus.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
          },
          onmessage: function (msg, jsep) {
            Janus.debug(" ::: Got a message :::");
            Janus.debug(JSON.stringify(msg));
            if (msg["error"] !== undefined && msg["error"] !== null) {
              addNewLog(msg["error"], "ERROR");
            }
            if (jsep !== undefined && jsep !== null) {
              // Answer
              textroom.createAnswer(
                {
                  jsep: jsep,
                  media: { audio: false, video: false, data: true },	// We only use datachannels
                  success: function (jsep) {
                    var body = { "request": "ack" };
                    textroom.send({ "message": body, "jsep": jsep });
                  },
                  error: function (error) {
                    addNewLog(error, "ERROR");
                  }
                });
            }
          },
          ondataopen: function (data) {
            Janus.log("The DataChannel is available!");
            // Prompt for a display name to join the default room
            createRoomChat();
          },
          ondata: function (data) {
            Janus.debug("We got data from the DataChannel! " + data);
            handleMessage(data);
          },
          oncleanup: function () {
            Janus.log(" ::: Got a cleanup notification :::");
            $('#datasend').attr('disabled', true);
          }
        }
      );
    }
	
    function createRoomChat() {

      var transaction = randomString(12);
      var register = {
        textroom: "create",
        transaction: transaction,
        room: classid,
        description: "",
        is_private: false,
        pin: "",
        post: "",
        secret: "",
        permanent: false
      };
      textroom.data({
        text: JSON.stringify(register),
        success: function (resp) {
          registerUsernameChat();
        }
      });
    }

    function registerUsernameChat() {
      var transaction = randomString(12);
      var register = {
        textroom: "join",
        transaction: transaction,
        room: classid,
        username: "userId" + iduser,
        display: displayname
      };

      transactions[transaction] = function (response) {
        if (response["textroom"] === "error") {
          // Something went wrong
          addNewLog(response["error"], "ERROR");
          return;
        }
        // Any participants already in?
        if (response.participants && response.participants.length > 0) {
          for (var i in response.participants) {
            var p = response.participants[i];
            var username = p.display ? p.display : p.username;
            chatParticipants[p.username] = username;
            // appendMessage(p.username, new Date(), false, "JOIN", "joined");
            // $('#chatroom').get(0).scrollTop = $('#chatroom').get(0).scrollHeight;
          }
        }
      };
      textroom.data({
        text: JSON.stringify(register),
        error: function (reason) {
          addNewLog(reason, "ERROR");
        },
        success: function () { 
        	chatReady = true;
      		getDataChat(); 
      	}
      });
    }

    function sendDataChat() {
      var inputText = $("#chatInput").val();
      var inputTextt = $("#chatInputt").val();
      if (inputText) {
        var message = {
          textroom: "message",
          transaction: randomString(12),
          room: classid,
          text: inputText
        };
        textroom.data({
          text: JSON.stringify(message),
          error: function (reason) { addNewLog(reason, "ERROR"); },
          success: function () { $("#chatInput").val(null); }
        });
      }
      if (inputTextt) {
        var message = {
          textroom: "message",
          transaction: randomString(12),
          room: classid,
          text: inputTextt
        };
        textroom.data({
          text: JSON.stringify(message),
          error: function (reason) { addNewLog(reason, "ERROR"); },
          success: function () { $("#chatInputt").val(null); }
        });
      }
    }
   
    function sendDataChatAndroid() {
      var inputText = $("#chatInputAndroid").val();
      if (inputText) {
        var message = {
          textroom: "message",
          transaction: randomString(12),
          room: classid,
          text: inputText
        };
        textroom.data({
          text: JSON.stringify(message),
          error: function (reason) { addNewLog(reason, "ERROR"); },
          success: function () { $("#chatInputAndroid").val(null); }
        });
      }
    }

    function getDate(jsonDate) {
      var when = new Date();
      if (jsonDate) {
        when = new Date(Date.parse(jsonDate));
      }
      return when;
    }

    function handleMessage(data) {
      var json = JSON.parse(data);
      var transaction = json["transaction"];
      if (transactions[transaction]) {
        // Someone was waiting for this
        transactions[transaction](json);
        delete transactions[transaction];
        return;
      }
      var what = json["textroom"];

      // ================ Incoming message: public or private? ============
      if (what === "message") {
        var msg = json["text"];
        msg = msg.replace(new RegExp('<', 'g'), '&lt');
        msg = msg.replace(new RegExp('>', 'g'), '&gt');
        appendMessage(json["from"], json["date"], json["whisper"], "CHAT", msg);
      } // ====================== Somebody joined =========================
      else if (what === "join") {
        var username = json["display"] ? json["display"] : json["username"];
        chatParticipants[json["username"]] = username;
        // appendMessage(json["username"], json["date"], false, "JOIN", username+" joined");
      } // ====================== Somebody left ===========================
      else if (what === "leave") {
        var username = json["username"];
        // appendMessage(username, json["date"], false, "LEAVE", username+" left");
      } // ================= Room was destroyed, goodbye! =================
      else if (what === "destroyed") {
        addNewLog("The room has been destroyed!", "ERROR");
      }

      // $('#chatroom').get(0).scrollTop = $('#chatroom').get(0).scrollHeight;
    }

    function appendMessage(participant, dateTime, isPrivate, type, msg) {
      
        var newMessage = {};
        newMessage.from = participant;
        newMessage.datetime = dateTime;
        newMessage.isPrivate = isPrivate;
        newMessage.type = type;
        newMessage.message = msg;
        chatData.splice(0, 0, newMessage);

        var newArchive = {};
        newArchive.sender_name = chatParticipants[participant];
        newArchive.text = msg;
        newArchive.datetime = dateTime;
        archiveChats.push(newArchive);

        // if (pageType == "video") {
        //   var divChat = document.getElementById('chatBoxScroll');
        //   divChat.scrollTop = divChat.scrollHeight;
        // }
        if (newMessage.type == "CHAT") {
        	if (page_Type == "web") {
	        	if(participant.slice(6,participant.length) == idtutor)
	        	{        	
		        	$("#chatBoxScroll").append('<div class="listview" style="width: 100%; word-wrap: break-word; background-color:rgba(236, 236, 236, 0.3); margin-bottom:2px;" id="chatBoxScroll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title" id="panel8"><b>'+ chatParticipants[participant] +' - Tutor </b></div><small style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ newMessage.message +'</small></div></div></a></div>');		        
			        $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;
			        $("#chatBoxScrolll").append('<div class="listview" style="width: 100%; word-wrap: break-word; background-color:rgba(236, 236, 236, 0.3); margin-bottom:2px;" id="chatBoxScrolll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title" id="panel8"><b>'+ chatParticipants[participant] +' - Tutor </b></div><small style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ newMessage.message +'</small></div></div></a></div>');		        
			        $('#chatBoxScrolll').get(0).scrollTop = $('#chatBoxScrolll').get(0).scrollHeight;
	        	} else {
		        	$("#chatBoxScroll").append('<div class="listview" style="width: 100%; word-wrap: break-word; background-color:rgba(236, 236, 236, 0.3); margin-bottom:2px;" id="chatBoxScroll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title" id="panel8"><b>'+ chatParticipants[participant] +'</b></div><small style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ newMessage.message +'</small></div></div></a></div>');		        
			        $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;
			        $("#chatBoxScrolll").append('<div class="listview" style="width: 100%; word-wrap: break-word; background-color:rgba(236, 236, 236, 0.3); margin-bottom:2px;" id="chatBoxScrolll"><a class="lv-item" id="panel5" href="#"><div class="media" id="panel6"><div style="word-wrap: break-word; width: 100%;" id="panel7"><div class="lv-title" id="panel8"><b>'+ chatParticipants[participant] +'</b></div><small style="width: 100%; word-wrap: break-word; color:grey;" id="panel9">'+ newMessage.message +'</small></div></div></a></div>');		        
			        $('#chatBoxScrolll').get(0).scrollTop = $('#chatBoxScrolll').get(0).scrollHeight;
		    	}
		    } else {
		    	if(participant.slice(6,participant.length) == idtutor)
	        	{        	
		        	$("#chatAndroid").append('<div class="chating" background-color:rgba(187, 222, 251, 0.3)><label>'+ chatParticipants[participant] +'<br><small style="word-wrap: break-word;">'+ newMessage.message +'</small></label></div>');		        
			        $('#chatAndroid').get(0).scrollTop = $('#chatAndroid').get(0).scrollHeight;
	        	} else {
		        	$("#chatAndroid").append('<div class="chating"><label>'+ chatParticipants[participant] +'<br><small style="word-wrap: break-word;">'+ newMessage.message +'</small></label></div>');		        
			        $('#chatAndroid').get(0).scrollTop = $('#chatAndroid').get(0).scrollHeight;
		    	}
		    }
        }
        // if(newMessage.type == "JOIN"){
        // 	$("#chatBoxScroll").append('<div class="listview" ><a class="lv-item" href="#"><div class="media"><div class="pull-left p-relative"><img class="lv-img-sm" src="../../aset/img/user/empty.jpg" alt=""></div><div class="media-body"><div class="lv-title c-green" style="margin-left:7px; margin-top:3px;">' + chatParticipants[participant] + ' joined</div></div></div></a></div>');
        // 	// $("#chatBoxScroll").append('<p style="color:green;"><b>' + chatParticipants[participant] + ' joined</b></p>');
	       //  $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;
        // }
        // if(newMessage.type == "LEAVE"){
        // 	$("#chatBoxScroll").append('<div class="listview" ><a class="lv-item" href="#"><div class="media"><div class="pull-left p-relative"><img class="lv-img-sm" src="../../aset/img/user/empty.jpg" alt=""></div><div class="media-body"><div class="lv-title c-red" style="margin-left:7px; margin-top:3px;">' + chatParticipants[participant] + ' left</div></div></div></a></div>');
        // 	// $("#chatBoxScroll").append('<p style="color:red;"><b>' + chatParticipants[participant] + ' left</b></p>');
	       //  $('#chatBoxScroll').get(0).scrollTop = $('#chatBoxScroll').get(0).scrollHeight;
        // }
    }
    
    function randomString(len, charSet) {
      charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var randomString = '';
      for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz, randomPoz + 1);
      }
      return randomString;
    }

    function archiveChat(dataChat){
    	var param = {
          "class_id": classid,
          "chat_arr": dataChat
        };
        console.warn(JSON.stringify(param));
        console.warn(param);
        // var param = JSON.stringify(param);
        $.ajax({
			url: 'https://classmiles.com/Rest/rec_chat',
			type: 'POST',
			data: param,
			success: function(data)
			{	


				// if (play_pos == 1) {
					// $.ajax({
					// 	url: 'https://classmiles.com/Rest/list_video',
					// 	type: 'GET',
					// 	data: {
					// 		session: session
					// 	},
					// 	success: function(data)
					// 	{													
							console.warn(data);		
							archiveChats = [];
					// 	}
					// });
				// }

			}
		});
    }

    function addNewLog(text, type){
      console.log(text+" : "+type)
    }

    $('#chatInput').keypress(function(){
    	var theCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
		if(theCode == 13) {
		  sendDataChat();
		return false;
		} else {
		return true;
		}
    });

    $('#chatInputt').keypress(function(){
    	var theCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
		if(theCode == 13) {
		  sendDataChat();
		return false;
		} else {
		return true;
		}
    });

	$('#chatInputAndroid').keypress(function(){
    	var theCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
		if(theCode == 13) {
		  sendDataChatAndroid();
		return false;
		} else {
		return true;
		}
    });

    // ============= END JANUS CHATROOM ==============================



	
	// ============= EVENT CLICK ==============================	

	var klik;
	var b;
	// $("#chat-trigger").click(function(){		
	// 	$("#chat").css('left','0px');		
	// 	$("#chat").css('display','block');
	// 	$("#chat").css('z-index','100');
	// });

	$("#klikchat").click(function(){		
		
	});

	// $( document.body ).click(function(e) {
	// 	var f = $("#tempatstudent").width() / $('#tempatstudent').parent().width() * 100;  
	// 	var g = $("#tempattutor").width() / $('#tempattutor').parent().width() * 100;
	// 	e = e || window.event;
	// 	e = e.target;
	// 	var myClass = $(this).attr("class");

	// 	if (e.id == "klikchat" || e.id == "chat-trigger") {
	// 		// alert(f);
	//    		if (f == 100) {   			
	//    			$("#tempatstudent").css('width','80%');	   		
	//    		}
	//    		if (g == 100) {
	//    			$("#tempattutor").css('width','80%');
	//    		}
	//    		else
	//    		{
	//    			$("#tempatstudent").css('width','100%');
	//    			$("#tempattutor").css('width','100%');
	//    		}
 //   		}   		
 //   		else if(e.id == "mCSB_1" || e.id == "mCSB_1_container" || e.id == "mCSB_1_scrollbar_vertical" || e.id == "chat" || e.id == "chat" || e.id == "panel1" || e.id == "panel2" || e.id == "chatBoxScroll" || e.id == "chatBoxScroll" || e.id == "panel3" || e.id == "panel4" || e.id == "chatInput" || e.id == "panel5" || e.id == "panel6" || e.id == "panel7" || e.id == "panel8" || e.id == "panel9")
 //   		{
 //   			$("#tempatstudent").css('width','80%');
 //   			$("#tempattutor").css('width','80%');
 //   		}
 //   		else
 //   		{
 //   			$("#tempatstudent").css('width','100%');
 //   			$("#tempattutor").css('width','100%');
 //   		}
	// });

	$("#devicedetect").click(function(){
		getListDevice();
	});

	function getListDevice() {
		var listdevicee = "";
		var iddevice = "";
		Janus.listDevices(function (devices) {
	        var deviceVideo = [];
	        var deviceAudio = [];

	        for (var i = 0; i < devices.length; i++) {
	          if (devices[i].kind == "videoinput") {
	            deviceVideo.push(devices[i]);
	          } else if (devices[i].kind == "audioinput") {
	            deviceAudio.push(devices[i]);
	          }
	        }

	        $("#buttonlist").empty();
	        listDevices.video = deviceVideo;
	        listDevices.audio = deviceAudio;

	        noid = 1;
	        for(var f in listDevices.video) {					
				listdevicee = listDevices.video[f]["label"];
				iddevice = listDevices.video[f]["deviceId"];

				$("#buttonlist").append("<button class='b btn btn-default btn-block btn-sm' style='margin:10px; padding:5px;' id='iddevice"+noid+"' ><label class='iniclick' id='listdevice"+noid+"' ></label></button>");			
				$("#iddevice"+noid+"").attr("cm-device",iddevice);
				$("#listdevice"+noid+"").text(listdevicee);
				noid++;
			}			
			
	        $("#deviceConfigurationModal").modal("show");


	    });
	}

	$('#buttonlist').on('click', '.b', function(){
		var id_device = $(this).attr("cm-device");
		changeDeviceConfiguration(id_device);
	});

	function changeDeviceConfiguration(deviceId) 
	{
        console.warn("== CHECK ==" + deviceId);
		if (deviceId) {
	       currentDeviceId = deviceId;
	        if (usertype == "tutor") {
	          	sendrequest.hangup();
	         	sendrequest.detach();
	          	sendrequest = "READY";
	          	attachPublisher();
	        }
	    }

	    $("#deviceConfigurationModal").modal("hide");
	}

	$("#keluarcobaaa").click(function(){		
		$("#classkeluar").modal("show");
	});

	$("#keluarcobaa").click(function(){		
		$("#classkeluar").modal("show");
	});

	$("#iddevice1").click(function(){
		var id_device = $(this).attr("cm-device");
	});

	$('#mute').click(toggleMute);
	function toggleMute() {
		var muted = sendrequestjsep.isAudioMuted();
		Janus.log((muted ? "Unmuting" : "Muting") + " local stream...");
		if(muted)
			sendrequestjsep.unmuteAudio();
		else
			sendrequestjsep.muteAudio();
		muted = sendrequestjsep.isAudioMuted();
		$('#mute').html(muted ? "<i class='tm-icon zmdi zmdi-volume-off' title='Audio Off'></i>" : "<i class='tm-icon zmdi zmdi-volume-up' title='Audio On'></i>");
	}

	$('#unpublish').click(unpublishOwnFeed);
	function unpublishOwnFeed() {
		// 
		var muted = sendrequestjsep.isVideoMuted();
		Janus.log((muted ? "Unmuting" : "Muting") + " local stream...");
		if(muted)
			sendrequestjsep.unmuteVideo();
		else
			sendrequestjsep.muteVideo();
		muted = sendrequestjsep.isVideoMuted();
		$('#unpublish').html(muted ? "<i class='tm-icon zmdi zmdi-eye-off' title='Video Off'></i>" : "<i class='tm-icon zmdi zmdi-eye' title='Video On'></i>");
	}

	$("#keluar").click(function(){
		destroyToken();
	});	

	$('#pause').click(matikanstreamsaya);
	function matikanstreamsaya() {
		$("#pause").css('display','none');		
		// var aa = $("#tempatx").attr("title").remove();
		// a.text("Break");
		
		var unpublishh = { "request": "unpublish" };
		sendrequest.send({"message": unpublishh});

		$.ajax({
			url: 'https://classmiles.com/Rest/break',
			type: 'GET',
			data: {
				session: session
			},
			success: function(data)
			{	
				st_break = 1;
				// if (play_pos == 1) {
					// $.ajax({
					// 	url: 'https://classmiles.com/Rest/list_video',
					// 	type: 'GET',
					// 	data: {
					// 		session: session
					// 	},
					// 	success: function(data)
					// 	{										
							console.warn("BERHASIL BREAK");
							$("#myvideo").attr('muted','false');
							// $("#play").remove('attr','id');
							$("#playboongan").css('display','block');
							setTimeout(function(){
								$("#playboongan").css('display','none');
								$("#play").css('display','block');
							},10000); 
							notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Silahkan Tunggu 10 detik untuk memulai kelas kembali");
							if (vartutor == 0 && st_break == 1) 
							{						
								if (usertype == "student") 
								{		
									$("#myvideostudent").attr('src', listVideo);
									$("#myvideostudent").attr('loop','true');
								}
							}
							else
							{

							}
							// $("#myvideostudent").attr('muted','false');
							// $("#myvideostudent").attr('src', '');
							// if (page_Type == "web") {
							// 	$("#myvideostudent").attr('src', listVideo);
								// $("#myvideostudent").attr('muted','true');
							// 	$("#myvideostudent").attr('loop','true');									
							// } else {
							// 	$("#player").attr('src', listVideo);
							// 	// $("#player").attr('muted','true');
							// 	$("#player").attr('loop','true');
							// }
					// 	}
					// });
				// }

			}
		});
		
	}

	$('#play').click(nyalakanstream);

	function nyalakanstream()
	{
		// alert('a');
		$("#play").css('display','none');
		$("#playboongan").css('display','none'); 
		$("#pause").css('display','block');
		$("#myvideo").attr("src",'');
		$("#myvideostudent").attr("src",'');
		$.ajax({
			url: 'https://classmiles.com/Rest/unbreak',
			type: 'GET',
			data: {
				session: session
			},
			success: function(data)
			{							
				st_break = 0;
				$("#myvideo").attr('muted','true');
				console.warn("BERHASIL UNBREAK");

				// var aa = $("#tempatx").attr("title").remove();
				// a.text("Play");
				publishOwnFeed(true);
			}
		});
		 
	}

	function destroyToken()
	{
		window.localStorage.removeItem("session");
		window.localStorage.removeItem("type_page");
		$.ajax({
	        url: 'https://classmiles.com/Rest/destroy_token', 
	        type: 'POST',
	        data : {
	            token : token                    
	        },
	        success: function(data) {
	            // janus.destroy();
	            // sendrequestjsep.detach();
	        	location.href='https://classmiles.com/first';
	        }
	    });
	}

	function destroyRaiseHand(){
		$.ajax({
			url: 'https://classmiles.com/Rest/raise_hand_destroy/',
			type: 'GET',
			data: {
				session: session
			},
			success: function(data)
			{
				console.warn("SUKSES destroy REQUEST");
			}
		});
	}

	$("#hangups").click(function(){

        $("#kotakmenulist").modal("hide");
        $("#callingModal").modal("hide");
        $("#videoCallModall").modal("hide");
        
		console.warn("Do hangups");
		
		doHangup();
	});

	$("#hangupss").click(function(){

        $("#kotakmenulist").modal("hide");
        $("#callingModal").modal("hide");
        $("#videoCallModall").modal("hide");
        
		console.warn("Do hangups");
		$("#tanganmerah").css('display','none');
		$("#tanganbiasa").css('display','block');
		cektangan = 0;
		doHangup();
	});

	$("#raisehandclick").click(function(){
		$.ajax({
			url: 'https://classmiles.com/Rest/raise_hand/',
			type: 'GET',
			data: {
				session: session
			},
			success: function(data)
			{
				notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Silahkan Tunggu hingga Tutor menjawab Raise hand anda");
				console.warn("SUKSES SEND REQUEST");
				// alert("test");
				cektangan = 1;
			}
		});
	});

	$("#raisehandclickAndroid").click(function(){
		$.ajax({
			url: 'https://classmiles.com/Rest/raise_hand/',
			type: 'GET',
			data: {
				session: session
			},
			success: function(data)
			{
				notify('top','right','fa fa-check','success','animated fadeInDown','animated fadeOut',"Silahkan Tunggu hingga Tutor menjawab Raise hand anda");
				console.warn("SUKSES SEND REQUEST");
				// alert("test");
				$("#tanganbiasa").css('display','none');
				$("#tanganmerah").css('display','block');
			}
		});
	});

	$("#bukakotakraise").click(function(){
		// alert('a');
		$("#kotakraisehand").empty();
		var id_userraise = {};
		var usernameraise = {};
		console.warn("LIST RAISEHAND");		
		// $("#kotakraisehand").append("<div class='col-md-4 m-t-15'><div class='pull-left'>test</div></div><div class='col-md-8 m-t-15'><div class='pull-right'><a id='1'><img src='../../aset/img/iconvideo.png'></a></div></div>");		
		// for(var f in raisehandlist.length) {
		if (raisehandlist.length > 0) {
			noid = 1;					
			for (var f = 0; f < raisehandlist.length; f++) {
				id_userraise = raisehandlist[f]["id_user"];
				usernameraise = raisehandlist[f]["user_name"];	
				$("#kotakraisehand").append("<button class='c btn btn-info btn-block btn-sm' iduser='"+raisehandlist[f]['id_user']+"' username='"+raisehandlist[f]['user_name']+"' style='margin:10px; padding:5px; cursor:pointer;' ><i class='zmdi zmdi-phone'></i>     <label class='iniclick' >"+ raisehandlist[f]['user_name'] +"</label></button>");					
				// $("#kotakraisehand").append("<div class='col-md-5 m-t-15'><div class='pull-left'>"+ raisehandlist[f]['user_name'] +"</div></div><div class='col-md-7'><div class='pull-right'><button id='jawabraise' class='c' iduser='"+raisehandlist[f]['id_user']+"' username='"+raisehandlist[f]['user_name']+"' style='cursor:pointer;' src='../../aset/img/iconvideo.png'>asdfasdf</button></div></div>");						
			}			       
			$("#kotakmenulist").modal("show");
		}
		else
		{
			// alert("Tidak ada data");
			// bootbox.alert("Tidak ada data");			
            // swal("Tidak ada data");            
            $("#tidakadadata").modal("show");
		}
		
	});

	$("#tutupalert").click(function(){
		$("#tidakadadata").modal("hide");
	});

	$('#kotakraisehand').on('click', '.c', function(){
		var iduser = $(this).attr("iduser");
		var username = $(this).attr("username");
        $("#kotakmenulist").modal("hide");
        cektangan = 1;
		
		checkBeforeCall(iduser, username);

	});

	$("#openscreenshare").click(function(){
		$(this).attr("href",'https://classmiles.com/screen_share/#access_session='+session);
	});

	// ============= END EVENT CLICK ==============================



	var videoCall = null;
    var vc_user1 = {};
    var vc_user2 = {};

    // ===========================================================================
    // ========================= VIDEO CALL FUNCTION DISINI ======================
    // ===========================================================================

    function attachVideoCall () {
      // Attach to echo test plugin
      janus.attach(
        {
          plugin: "janus.plugin.videocall",
          success: function (pluginHandle) {
            videoCall = pluginHandle;
            vc_user1.id = "rm" + classid + "vc" + iduser + "|" + randomString(10);

            vc_user1.username = displayname;
            registerUserForCall();
            $("#kotakmenulist").modal("hide");
            $("#callingModal").modal("hide");
            $("#videoCallModall").modal("hide");
            
            Janus.log("Plugin attached! (" + videoCall.getPlugin() + ", id=" + videoCall.getId() + ")");
            console.warn("AttachVideoCall");
          },
          error: function (error) {
            addNewLog("Error VideoCall : " + error, "ERROR");
          },
          consentDialog: function (on) {
            Janus.debug("Consent dialog should be " + (on ? "on" : "off") + " now");
            // if (on) {
            //   // Darken screen and show hint
            //   $.blockUI({
            //     message: '<div><img src="up_arrow.png"/></div>',
            //     css: {
            //       border: 'none',
            //       padding: '15px',
            //       backgroundColor: 'transparent',
            //       color: '#aaa',
            //       top: '10px',
            //       left: (navigator.mozGetUserMedia ? '-100px' : '300px')
            //     }
            //   });
            // } else {
            //   // Restore screen
            //   $.unblockUI();
            // }
          },
          mediaState: function (medium, on) {
            Janus.log("Janus " + (on ? "started" : "stopped") + " receiving our " + medium);
          },
          webrtcState: function (on) {
            Janus.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
            // $("#videoleft").parent().unblock();
          },
          onmessage: function (msg, jsep) {
            Janus.debug(" ::: Got a message :::");
            Janus.debug(JSON.stringify(msg));
            var result = msg["result"];
            if (result !== null && result !== undefined) {
              if (result["list"] !== undefined && result["list"] !== null) {
                var list = result["list"];
                Janus.debug("Got a list of registered peers:");
                Janus.debug(list);
                var isExist = false;
                for (var mp in list) {
                  if (vc_user2.id == list[mp].split("|", 1)) {
                    doCall(list[mp]);
                    isExist = true;
                    break;
                  }
                }

                if (!isExist) {
                  bootbox.alert(vc_user2.username + " is Offline");
                }
              } else if (result["event"] !== undefined && result["event"] !== null) {
                var event = result["event"];
                if (event === 'registered') {
                  vc_user1.id = result["username"];
                  Janus.log("Successfully registered as " + vc_user1.username + "!");
                } else if (event === 'calling') {
                  Janus.log("Waiting for the peer to answer...");
                  // TODO Any ringtone?
                } else if (event === 'incomingcall') {
                  Janus.log("Incoming call from " + result["username"] + "!");
                  vc_user2.id = result["username"];
                  // TODO Enable buttons to answer
                  // bootbox.confirm("Want to answer ?", function (result) {
                  // Telah selesai diproses, ubah status raise hand
                  //   destroyRaiseHand();

                  //   if (result) {
                  //     doAccept(jsep);
                  //   } else {
                  //     doHangup();
                  //   }
                  // });

                  doAccept(jsep);

                } else if (event === 'accepted') {
                  var peer = result["username"];
                  $("#callingModal").modal("hide");
                  $("#videoCallModall").modal("show");
                  if (peer === null || peer === undefined) {
                    Janus.log("Call started!");
                  } else {
                    Janus.log(peer + " accepted the call!");
                    vc_user2.id = peer;
                  }
                  // TODO Video call can start
                  if (jsep !== null && jsep !== undefined)
                    videoCall.handleRemoteJsep({ jsep: jsep });
                } else if (event === 'hangup') {
                  Janus.log("Call hung up by " + result["username"] + " (" + result["reason"] + ")!");
                  // TODO Reset status
                  cektangan = 0;
                  videoCall.hangup();
                  clearVideoCall();
                }
              }
            } else {
              // FIXME Error?
              var error = msg["error"];
              addNewLog("Error VideoCall : " + error, "ERROR");
              // TODO Reset status
              videoCall.hangup();
            }
          },
          onlocalstream: function (stream) {
            Janus.debug(" ::: Got a local stream :::");
            if ($('#videoCallLocal').length === 0) {
              $('#videoBoxLocal').append('<video id="videoCallLocal" style="width:80%; height:350px;" autoplay></video>');
            }

            $("#videoCallLocal").bind("playing", function () {
              if (adapter.browserDetails.browser == "firefox") {
                // Firefox Stable has a bug: width and height are not immediately available after a playing
                setTimeout(function () {
                  var width = $("#videoCallLocal").get(0).videoWidth;
                  var height = $("#videoCallLocal").get(0).videoHeight;
                }, 2000);
              }
            });
            Janus.attachMediaStream($('#videoCallLocal').get(0), stream);
          },
          onremotestream: function (stream) {
            Janus.debug(" ::: Got a remote stream :::");
            if ($('#videoCallRemote').length === 0) {
              $('#videoBoxRemote').append('<video id="videoCallRemote" style="width:80%; height:350px;" autoplay></video>');
            }

            $("#videoCallRemote").bind("playing", function () {
              if (adapter.browserDetails.browser == "firefox") {
                // Firefox Stable has a bug: width and height are not immediately available after a playing
                setTimeout(function () {
                  var width = $("#videoCallRemote").get(0).videoWidth;
                  var height = $("#videoCallRemote").get(0).videoHeight;
                }, 2000);
              }
            });
           	Janus.attachMediaStream($('#videoCallRemote').get(0), stream);
          },
	        
	        ondataopen: function(data) {
				Janus.log("The DataChannel is available!");
				// $('#videos').removeClass('hide').show();
				// $('#datasend').removeAttr('disabled');
			},
			ondata: function(data) {
				Janus.debug("We got data from the DataChannel! " + data);
				// $('#datarecv').val(data);
				if (data == "GoHangup" && usertype != "tutor") {
					console.log("GoHangup ke student");
					doHangup();
				}
			},
			oncleanup: function () {
				Janus.log(" ::: Got a cleanup notification :::");
				$('#videoCallLocal').remove();
				$('#videoCallRemote').remove();

				$("#kotakmenulist").modal("hide");
				$("#callingModal").modal("hide");
				$("#videoCallModall").modal("hide");

				// clearVideoCall();
			}
        });
    };

    function checkBeforeCall (id, name) {
      vc_user2.id = "rm" + classid + "vc" + id;
      vc_user2.username = name;
      videoCall.send({ "message": { "request": "list" } });
    };

    function getJsonOffer () {
      var videoConfig = JSON.parse(window.localStorage.getItem('videoConfig'));
      var jsonRequest = {};
      console.warn(currentDeviceId + " JSON OFFER");
      if (currentDeviceId) {
        jsonRequest = {
          "video": { "deviceId": currentDeviceId, "width": videoConfig.width, "height": videoConfig.height },
          // "video": {"deviceId": currentDeviceId, "width": "1024", "height": "702"},
          "data": false
        };
      } else {
        jsonRequest = {
          "video": videoConfig.defaultResolution,
          "data": false
        };
      };

      return jsonRequest;
    };

    function  doCall(x) {
      var jsonOffer = getJsonOffer();
      // Call this user
      videoCall.createOffer(
        {
          // By default, it's sendrecv for audio and video...
          // media: { data: true }, // ... let's negotiate data channels as well
          media: jsonOffer,
          success: function (jsep) {
            Janus.debug("Got SDP!");
            Janus.debug(jsep);
            // disableVideoRoomTutor();
            $("#callingModal").modal("show");
            $("#messageCalling").html("Memanggil " + vc_user2.username + " . . . ");
            var body = { "request": "call", "username": x };
            videoCall.send({ "message": body, "jsep": jsep });
          },
          error: function (error) {
            addNewLog("Error VideoCall : " + error, "ERROR");
          }
        });
    };

    function registerUserForCall() {
      var register = { "request": "register", "username": vc_user1.id, "token": token };
      videoCall.send({ "message": register });
    };

    function doAccept(jsep) {
      var jsonAccept = getJsonOffer();

      videoCall.createAnswer(
        {
          jsep: jsep,
          // No media provided: by default, it's sendrecv for audio and video
          // media: { data: true },  // Let's negotiate data channels as well
          media: jsonAccept,
          success: function (jsep) {
            Janus.debug("Got SDP!");
            Janus.debug(jsep);
            var body = { "request": "accept" };
            videoCall.send({ "message": body, "jsep": jsep });
          },
          error: function (error) {
            addNewLog("Error AcceptCall : " + JSON.stringify(error), "ERROR");
            doHangup();
          }
        });
    };

    function doHangup() {
    	// if (usertype == "tutor") {
    	// 	sendData();
    	// } else {
			var hangup = { "request": "hangup" };
			videoCall.send({ "message": hangup });
			videoCall.hangup();
			// videoCall.detach();
			clearVideoCall();
		// }
    };

    function clearVideoCall() {
		// videoCall = null;      
		$('#videoCallLocal').remove();
		$('#videoCallRemote').remove();

		$("#kotakmenulist").modal("hide");
		$("#callingModal").modal("hide");
		$("#videoCallModall").modal("hide");
		
		vc_user1 = {};
		vc_user2 = {};
		// videoCall = null;
		// enableVideoRoomTutor();
		// attachVideoCall();

		if (usertype == "student") {
			destroyRaiseHand();
		}
  	};

	function enableVideoRoomTutor(){
		sendrequestjsep.unmuteAudio();
		sendrequestjsep.unmuteVideo();
	};

	function sendData() {
		var data = "GoHangup";
		if(data === "") {
			bootbox.alert('Insert a message to send on the DataChannel to your peer');
			return;
		}
		videoCall.data({
			text: data,
			error: function(reason) { bootbox.alert(reason); },
			success: function() { console.log("Sukes Hangup from tutor"); },
		});
	};
	// $("#chatInput").maxlength();
 //    $("textarea").bind("update.maxlength", function(event, element, lastLength, length, maxLength, left){
 //        console.log(event, element, lastLength, length, maxLength, left);
 //    });

});
