FROM telkomindonesia/alpine:php-7.1-apache-novol

WORKDIR /var/www/data/html

COPY . .

User root 

RUN mkdir -p /var/www/data/html/application/sessions/ \
	&& chmod 0700 /var/www/data/html/application/sessions/ \
    && chown -R user:www-data /var/www/data/html/application/sessions/
    